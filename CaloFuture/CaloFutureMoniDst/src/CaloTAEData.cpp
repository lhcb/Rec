/*****************************************************************************\
 * * (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "Event/ODIN.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Relations/RelationWeighted1D.h"

using Input = LHCb::Event::Calo::Digits;

namespace LHCb::Calo::Algorithms {

  /**
   *   This algorithm builds an nTuple for TAE analysis offline only if the boolean
   *   m_writeTuple is set to true.
   *
   *   In order to write the nTuple correctly, the option:
   *     options.ntuple_file = <file_name.root>
   *   needs to be specified in the options file when executing the algorithm.
   *
   *   It stores data only for 6 cells: 2 inner, 2 middle, 2 outer.
   *   The nTuple contains:
   *     - isTAE: boolean, true if event is stored in TAE mode.
   *     - TAEventIndex: integer, idex of the TAE window of the digits.
   *     - TAEventCentral: boolean, true if TAEventIndex is 0 (central wondow of the TAE).
   *     - TAEventNumber: integer, event number the digits belong to.
   *     - bxID: bunch id the digits are expected to be.
   *     - digitE: vector of digit energies.
   *     - digitI: vector of digit cellIDs.
   *     - digitA: vector of digit area.
   *
   *   @author  Nuria Valls Canudas  nuria.valls.canudas@cern.ch
   *   @date    17/05/2022
   */
  class CaloTAEData : public LHCb::Algorithm::Consumer<void( const Input&, const ODIN& ),
                                                       Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

  public:
    using base_type = LHCb::Algorithm::Consumer<void( const Input&, const ODIN& ),
                                                Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>>;
    using KeyValue  = typename base_type::KeyValue;

    CaloTAEData( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "Input", CaloFutureAlgUtils::CaloFutureDigitLocation( name ) },
                      KeyValue{ "ODINLocation", ODINLocation::Default } } ) {}

    void operator()( const Input& digits, const ODIN& odin ) const override {

      if ( m_writeTuple.value() ) {

        std::vector<float> cell_list{ 11109.0, 11098.0, 7077.0, 7065.0, 2853.0, 2842.0 };
        auto               tuple = this->nTuple( "TAEData" );

        std::vector<double> digitE;
        std::vector<int>    digitI, digitA;

        for ( const auto& digit : digits ) {
          if ( digit.adc() < 100 ) continue;
          if ( std::find( cell_list.begin(), cell_list.end(), digit.cellID().index() ) != cell_list.end() ) {
            digitE.push_back( digit.energy() );
            digitA.push_back( digit.cellID().area() );
            digitI.push_back( digit.cellID().index() );
          }
        }

        if ( !digitE.empty() ) {

          auto sc = tuple->column( "isTAE", odin.isTAE() );
          sc      = tuple->column( "TAEventIndex", odin.timeAlignmentEventIndex() );
          sc      = tuple->column( "TAEventFirst", odin.timeAlignmentEventFirst() );
          sc      = tuple->column( "TAEventCentral", odin.timeAlignmentEventCentral() );
          sc      = tuple->column( "TAEEventNumber", odin.eventNumber() );
          sc      = tuple->column( "bxID", odin.bunchId() );
          sc      = tuple->farray( "digitE", digitE, "ND", 10 );
          sc      = tuple->farray( "digitI", digitI, "ND", 10 );
          sc      = tuple->farray( "digitA", digitA, "ND", 10 );

          sc = tuple->write();
        }
      }
    }

  private:
    Gaudi::Property<bool> m_writeTuple{ this, "writeTuple", false };
  };
  DECLARE_COMPONENT_WITH_ID( CaloTAEData, "CaloTAEData" )
} // namespace LHCb::Calo::Algorithms
