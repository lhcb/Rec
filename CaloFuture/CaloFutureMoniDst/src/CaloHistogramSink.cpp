/***********************************************************************************\
* (c) Copyright 1998-2022 CERN for the benefit of the LHCb and ATLAS collaborations *
*                                                                                   *
* This software is distributed under the terms of the Apache version 2 licence,     *
* copied verbatim in the file "LICENSE".                                            *
*                                                                                   *
* In applying this licence, CERN does not waive the privileges and immunities       *
* granted to it by virtue of its status as an Intergovernmental Organization        *
* or submit itself to any jurisdiction.                                             *
\***********************************************************************************/

#include <CaloFutureUtils/CellIDHistogramUtils.h>

#include <Gaudi/Histograming/Sink/Base.h>
#include <Gaudi/Histograming/Sink/Utils.h>

namespace GHSink = Gaudi::Histograming::Sink;

namespace LHCb::Calo::Histograming::Sink {

  using namespace std::string_literals;

  struct Root : public GHSink::Base {
    using Base::Base;
    template <int ND>
    struct saveProfileRootHistoWrapper {
      saveProfileRootHistoWrapper( Root& root ) : m_root( root ) {}
      void operator()( TFile& file, std::string dir, std::string name, nlohmann::json const& j ) {
        return saveProfileRootHisto<ND>( file, dir, name, j, m_root.m_convertProfiles.value() );
      }
      Root& m_root;
    };
    HistoRegistry const registry = {
        { { "histogram:CellIDHistogram"s, 1 }, &GHSink::saveRootHisto<Traits1D<HistoFlavour::Regular>> },
        { { "histogram:WeightedCellIDHistogram"s, 1 }, &GHSink::saveRootHisto<Traits1D<HistoFlavour::Regular>> },
        { { "histogram:ProfileCellIDHistogram"s, 1 }, saveProfileRootHistoWrapper<1>( *this ) },
        { { "histogram:WeightedProfileCellIDHistogram"s, 1 }, saveProfileRootHistoWrapper<1>( *this ) },
        { { "histogram:CellIDHistogram"s, 2 }, &GHSink::saveRootHisto<Traits2D<HistoFlavour::Regular>> },
        { { "histogram:WeightedCellIDHistogram"s, 2 }, &GHSink::saveRootHisto<Traits2D<HistoFlavour::Regular>> },
        { { "histogram:ProfileCellIDHistogram"s, 2 }, saveProfileRootHistoWrapper<2>( *this ) },
        { { "histogram:WeightedProfileCellIDHistogram"s, 2 }, saveProfileRootHistoWrapper<2>( *this ) } };
    StatusCode initialize() override {
      return Base::initialize().andThen( [&] {
        for ( auto& [id, func] : registry ) { registerHandler( id, func ); }
      } );
    }
    Gaudi::Property<bool> m_convertProfiles{
        this, "ConvertProfileHistos", true,
        "If true, no profile histos are saved. These are converted to a pair of regular histograms, one plotting the "
        "mean and the other the rms of the original data" };
  };

  DECLARE_COMPONENT( Root )
} // namespace LHCb::Calo::Histograming::Sink
