/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellIDHistogram.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/Histogram.h>

namespace LHCb::Calo {

  /**
   *  The algorithm for trivial monitoring of "CaloFutureCluster" containers.
   *  The algorithm produces 8 histograms:
   *
   *  <ol>
   *  <li> @p CaloCluster multiplicity                    </li>
   *  <li> @p CaloCluster size (number of cells)          </li>
   *  <li> @p CaloCluster energy distribution             </li>
   *  <li> @p CaloCluster transverse energy distribution  </li>
   *  <li> @p CaloCluster x-distribution                  </li>
   *  <li> @p CaloCluster y-distribution                  </li>
   *  <li> @p CaloCluster x vs y-distribution             </li>
   *  </ol>
   *
   *  Histograms reside in the directory @p /stat/"Name" , where
   *  @ "Name" is the name of the algorithm
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date   02/11/2001
   */

  using Input = Event::Calo::v2::Clusters;

  template <Detector::Calo::CellCode::Index calo>
  class ClusterMonitor final : public Algorithm::Consumer<void( const Input& )> {
  public:
    StatusCode initialize() override;
    void       operator()( const Input& ) const override;

    ClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "Input", CaloFutureAlgUtils::CaloFutureClusterLocation( name ) } } ) {}

  private:
    mutable Gaudi::Accumulators::Histogram<1>         m_histoNClusters{ this, "nClusters" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoNDigits{ this, "nDigits" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoNDigits_forE{ this, "nDigits_forE" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoEnergy{ this, "energy" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoET{ this, "ET" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoX{ this, "x" };
    mutable Gaudi::Accumulators::Histogram<1>         m_histoY{ this, "y" };
    mutable Gaudi::Accumulators::Histogram<2>         m_histoXY{ this, "x-y" };
    mutable Gaudi::Accumulators::WeightedHistogram<2> m_histoXY_eW{ this, "x-y-eW" };
    mutable CellIDHistogram<calo>                     m_position2D{ this, "position2D" };
    mutable WeightedCellIDHistogram<calo>             m_position2D_eW{ this, "position2D-eW" };
    mutable Gaudi::Accumulators::StatCounter<>        m_clusters{ this, "# clusters over threshold" };

  private:
    Gaudi::Property<Gaudi::Histo1DDef> m_multHist{ this, "HistoMultiplicity", Gaudi::Histo1DDef( 0., 2000., 100 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_sizeHist{ this, "HistoSize", Gaudi::Histo1DDef( 0., 25., 25 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_energyHist{
        this, "HistoEnergy", Gaudi::Histo1DDef( 0. * Gaudi::Units::GeV, 250. * Gaudi::Units::GeV, 100 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_etHist{
        this, "HistoEt", Gaudi::Histo1DDef( 0. * Gaudi::Units::GeV, 15. * Gaudi::Units::GeV, 100 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_xHist{
        this, "HistoX", Gaudi::Histo1DDef( -4 * Gaudi::Units::meter, +4 * Gaudi::Units::meter, 50 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_yHist{
        this, "HistoY", Gaudi::Histo1DDef( -4 * Gaudi::Units::meter, +4 * Gaudi::Units::meter, 50 ) };
    Gaudi::Property<float> m_eFilter{ this, "EnergyFilter", -100. };
    Gaudi::Property<float> m_etFilter{ this, "EtFilter", -100. };
  };

  DECLARE_COMPONENT_WITH_ID( ClusterMonitor<Detector::Calo::CellCode::Index::EcalCalo>, "CaloClusterECALMonitor" )
  DECLARE_COMPONENT_WITH_ID( ClusterMonitor<Detector::Calo::CellCode::Index::HcalCalo>, "CaloClusterHCALMonitor" )

} // namespace LHCb::Calo

template <LHCb::Detector::Calo::CellCode::Index calo>
StatusCode LHCb::Calo::ClusterMonitor<calo>::initialize() {
  return Consumer::initialize().andThen( [&] {
    // define range of new histograms from properties
    using Axis1D = Gaudi::Accumulators::Axis<double>;
    m_histoNClusters.setTitle( "# of Clusters " + inputLocation() );
    m_histoNClusters.setAxis<0>( Axis1D{ m_multHist } );
    m_histoNDigits.setTitle( "Cluster digit multiplicity " + inputLocation() );
    m_histoNDigits.setAxis<0>( Axis1D{ m_sizeHist } );
    m_histoNDigits_forE.setTitle( "Cluster digit used for Energy multiplicity " + inputLocation() );
    m_histoNDigits_forE.setAxis<0>( Axis1D{ m_sizeHist } );
    m_histoEnergy.setTitle( "Cluster Energy " + inputLocation() );
    m_histoEnergy.setAxis<0>( Axis1D{ m_energyHist } );
    m_histoET.setTitle( "Cluster Et " + inputLocation() );
    m_histoET.setAxis<0>( Axis1D{ m_etHist } );
    m_histoX.setTitle( "Cluster x " + inputLocation() );
    m_histoX.setAxis<0>( Axis1D{ m_xHist } );
    m_histoY.setTitle( "Cluster y " + inputLocation() );
    m_histoY.setAxis<0>( Axis1D{ m_yHist } );
    m_histoXY.setTitle( "Cluster barycenter position x vs y " + inputLocation() );
    m_histoXY.setAxis<0>( Axis1D{ m_xHist } );
    m_histoXY.setAxis<1>( Axis1D{ m_yHist } );
    m_histoXY_eW.setTitle( "Energy-weighted cluster barycenter position x vs y " + inputLocation() );
    m_histoXY_eW.setAxis<0>( Axis1D{ m_xHist } );
    m_histoXY_eW.setAxis<1>( Axis1D{ m_yHist } );
    m_position2D.setTitle( "Cluster position 2Dview " + inputLocation() );
    m_position2D_eW.setTitle( "Cluster position 2Dview " + inputLocation() );
  } );
}

template <LHCb::Detector::Calo::CellCode::Index calo>
void LHCb::Calo::ClusterMonitor<calo>::operator()( const Input& clusters ) const {

  if ( clusters.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty cluster in " << inputLocation() << endmsg;
    return;
  }

  // for multiplicity histogram
  uint nClusters = 0;

  for ( const auto& cluster : clusters.scalar() ) {
    const double e  = cluster.energy();
    const double x  = cluster.position().x();
    const double y  = cluster.position().y();
    const double z  = cluster.position().z();
    const double et = e * sqrt( x * x + y * y ) / sqrt( x * x + y * y + z * z );
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    ++nClusters;
    const auto  id      = cluster.cellID();
    const auto& entries = cluster.entries();
    ++m_histoNDigits[entries.size().cast()];
    ++m_histoEnergy[e];
    ++m_histoET[et];
    ++m_histoX[x];
    ++m_histoY[y];
    ++m_histoXY[{ x, y }];
    m_histoXY_eW[{ x, y }] += e;

    int iuse = std::count_if( entries.begin(), entries.end(),
                              []( const auto& e ) { return e.status().test( CaloDigitStatus::Mask::UseForEnergy ); } );
    ++m_histoNDigits_forE[iuse];

    // TODO: use thread-safe histos. Need variable binning.
    ++m_position2D[id];
    m_position2D_eW[id] += e;
  }
  ++m_histoNClusters[nClusters];
  m_clusters += nClusters;
}
