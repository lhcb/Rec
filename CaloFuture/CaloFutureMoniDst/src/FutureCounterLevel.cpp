/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "IFutureCounterLevel.h" // Interface

//-----------------------------------------------------------------------------
// Implementation file for class : FutureCounterLevel
//
// 2016-08-13 : Olivier Deschamps
//-----------------------------------------------------------------------------

/** @class FutureCounterLevel FutureCounterLevel.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2016-08-13
 */
class FutureCounterLevel final : public extends<GaudiTool, IFutureCounterLevel> {
  Gaudi::Property<int> m_clevel{ this, "SetLevel", 1, "quiet mode is the default" };

public:
  using extends::extends;
  bool isQuiet() const override { return m_clevel > 0; };
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( FutureCounterLevel )

//=============================================================================
