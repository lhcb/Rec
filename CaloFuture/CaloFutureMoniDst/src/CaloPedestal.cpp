/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellIDHistogram.h"
#include "Core/FloatComparison.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"
#include "LHCbAlgs/Consumer.h"
#include "TH2.h"
#include "TProfile2D.h"

#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Accumulators/HistogramArray.h>

#include "fmt/format.h"

namespace LHCb::Calo {

  template <Detector::Calo::CellCode::Index calo>
  class Pedestal final : public Algorithm::Consumer<void( const Event::Calo::Digits&, const LHCb::ODIN& )> {
  public:
    StatusCode initialize() override;
    void       operator()( const Event::Calo::Digits&, const LHCb::ODIN& ) const override;

    Pedestal( const std::string& name, ISvcLocator* pSvcLocator );

  private:
    mutable CellIDHistogram<calo, 2, Gaudi::Accumulators::atomicity::full, double> m_ADC1Hist{
        this, fmt::format( "{}Cells/subch1", toString( calo ) ) };
    mutable CellIDHistogram<calo, 2, Gaudi::Accumulators::atomicity::full, double> m_ADC2Hist{
        this, fmt::format( "{}Cells/subch2", toString( calo ) ) };

    Gaudi::Property<Gaudi::Histo1DDef> m_adcHist{ this, "HistoAdc", Gaudi::Histo1DDef( 0., 4096., 4096 ) };
    Gaudi::Property<float>             m_adcFilter{ this, "ADCFilter", -100. };
  };

  DECLARE_COMPONENT_WITH_ID( Pedestal<Detector::Calo::CellCode::Index::EcalCalo>, "CaloECALPedestal" )
  DECLARE_COMPONENT_WITH_ID( Pedestal<Detector::Calo::CellCode::Index::HcalCalo>, "CaloHCALPedestal" )

} // namespace LHCb::Calo

template <LHCb::Detector::Calo::CellCode::Index calo>
LHCb::Calo::Pedestal<calo>::Pedestal( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "Input", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( name ) },
                  KeyValue{ "ODINLocation", LHCb::ODINLocation::Default } } ) {}

template <LHCb::Detector::Calo::CellCode::Index calo>
StatusCode LHCb::Calo::Pedestal<calo>::initialize() {
  return Consumer::initialize().andThen( [&] {
    info() << " digits from " << inputLocation() << endmsg;
    m_ADC1Hist.setTitle( fmt::format( "{} channel, per cell", toString( calo ) ) );
    m_ADC1Hist.template setAxis<0>( Gaudi::Accumulators::Axis<double>{ m_adcHist } );
    m_ADC2Hist.setTitle( fmt::format( "{} channel, per cell", toString( calo ) ) );
    m_ADC2Hist.template setAxis<0>( Gaudi::Accumulators::Axis<double>{ m_adcHist } );
  } );
}

template <LHCb::Detector::Calo::CellCode::Index calo>
void LHCb::Calo::Pedestal<calo>::operator()( const Event::Calo::Digits& digits, const LHCb::ODIN& odin ) const {
  if ( digits.empty() && msgLevel( MSG::DEBUG ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
  const auto bcID = odin.bunchId();
  for ( const auto& digit : digits ) {
    const auto id  = digit.cellID();
    const auto adc = digit.adc();
    if ( digit.adc() < m_adcFilter ) continue;
    if ( bcID % 2 == 0 ) {
      ++m_ADC1Hist[{ id, adc }];
    } else {
      ++m_ADC2Hist[{ id, adc }];
    }
  }
  return;
}
