/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellIDHistogram.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"
#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/Histogram.h>

#include <mutex>

namespace LHCb::Calo {

  /**
   *  The algorithm for trivial monitoring of "CaloDigit" containers.
   *  The algorithm produces the following histograms:
   *   1. CaloDigit multiplicity
   *   2. CaloDigit ocupancy 2D plot per area
   *   3. CaloDigit energy 2D plot per area
   *  The same set of histograms, but with cut on Et (or E), is produced if specified
   *
   *  Histograms reside in the directory @p /stat/"Name" , where
   *  @p "Name" is the name of the algorithm
   *
   *  @author Konstantin Belous Konstantin.Beloous@itep.ru
   *  @date   21/06/2007
   */

  using Input = Event::Calo::Digits;

  template <Detector::Calo::CellCode::Index calo>
  class DigitMonitor final
      : public LHCb::Algorithm::Consumer<void( const Input&, const LHCb::ODIN&, const DeCalorimeter& ),
                                         DetDesc::usesConditions<DeCalorimeter>> {
  public:
    StatusCode initialize() override;
    void       operator()( const Input&, const LHCb::ODIN&, const DeCalorimeter& ) const override;

    DigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  private:
    std::string m_detData{};

    mutable Gaudi::Accumulators::Histogram<1>  m_histoMultiplicity{ this, "Multiplicity" };
    mutable Gaudi::Accumulators::Histogram<1>  m_histoDigitX{ this, "DigitX" };
    mutable Gaudi::Accumulators::Histogram<1>  m_histoDigitY{ this, "DigitY" };
    mutable Gaudi::Accumulators::Histogram<2>  m_histoDigitXY{ this, "DigitXY" };
    mutable Gaudi::Accumulators::Histogram<1>  m_histoET{ this, "ET" };
    mutable Gaudi::Accumulators::Histogram<1>  m_histoADC{ this, "ADC" };
    mutable Gaudi::Accumulators::StatCounter<> m_digits{ this, "# digits over threshold" };
    mutable WeightedCellIDHistogram<calo>      m_histoAccEnergy{ this, "_7" };
    mutable CellIDHistogram<calo>              m_histoAccDigits{ this, "_8" };
    mutable WeightedCellIDHistogram<calo>      m_histoAccADCs{ this, "_9" };

    Gaudi::Property<std::vector<int>> m_Calib_BXIDsA{ this, "Calib_BXIDsA" };
    Gaudi::Property<std::vector<int>> m_Calib_BXIDsB{ this, "Calib_BXIDsB" };

    Gaudi::Property<Gaudi::Histo1DDef> m_multHist{ this, "HistoMultiplicity", Gaudi::Histo1DDef( 0., 2000., 100 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_etHist{
        this, "HistoEt", Gaudi::Histo1DDef( 0. * Gaudi::Units::GeV, 15. * Gaudi::Units::GeV, 100 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_xHist{
        this, "HistoX", Gaudi::Histo1DDef( -4 * Gaudi::Units::meter, +4 * Gaudi::Units::meter, 50 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_yHist{
        this, "HistoY", Gaudi::Histo1DDef( -4 * Gaudi::Units::meter, +4 * Gaudi::Units::meter, 50 ) };
    Gaudi::Property<Gaudi::Histo1DDef> m_adcHist{ this, "HistoAdc", Gaudi::Histo1DDef( 0., 4096., 4096 ) };

    Gaudi::Property<float> m_eFilter{ this, "EnergyFilter", -100. };
    Gaudi::Property<float> m_etFilter{ this, "EtFilter", -100. };
    Gaudi::Property<float> m_adcFilter{ this, "ADCFilter", -100. };
  };

  DECLARE_COMPONENT_WITH_ID( DigitMonitor<Detector::Calo::CellCode::Index::EcalCalo>, "CaloDigitECALMonitor" )
  DECLARE_COMPONENT_WITH_ID( DigitMonitor<Detector::Calo::CellCode::Index::HcalCalo>, "CaloDigitHCALMonitor" )

} // namespace LHCb::Calo

template <LHCb::Detector::Calo::CellCode::Index calo>
LHCb::Calo::DigitMonitor<calo>::DigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "Input", CaloFutureAlgUtils::CaloFutureDigitLocation( toString( calo ) ) },
                  KeyValue{ "ODINLocation", ODINLocation::Default },
                  KeyValue{ "Detector", Calo::Utilities::DeCaloFutureLocation( toString( calo ) ) } } )
    , m_detData{ toString( calo ) } {}

template <LHCb::Detector::Calo::CellCode::Index calo>
StatusCode LHCb::Calo::DigitMonitor<calo>::initialize() {
  return Consumer::initialize().andThen( [&] {
    info() << m_detData << " digits from " << inputLocation() << endmsg;
    // book physics histograms so they are always created and found by Monet
    m_histoAccEnergy.setTitle( fmt::format( "{} Accumulated Energy", m_detData ) );
    m_histoAccDigits.setTitle( fmt::format( "{} Accumulated Digits filled", m_detData ) );
    m_histoAccADCs.setTitle( fmt::format( "{} Accumulated ADCs", m_detData ) );
    // define range of new histograms from properties
    using Axis1D = Gaudi::Accumulators::Axis<double>;
    m_histoMultiplicity.setTitle( "Number of digits" );
    m_histoMultiplicity.setAxis<0>( Axis1D{ m_multHist } );
    m_histoDigitX.setTitle( "Digit X" );
    m_histoDigitX.setAxis<0>( Axis1D{ m_xHist } );
    m_histoDigitY.setTitle( "Digit Y" );
    m_histoDigitY.setAxis<0>( Axis1D{ m_yHist } );
    m_histoDigitXY.setTitle( "Digit position x vs y" );
    m_histoDigitXY.setAxis<0>( Axis1D{ m_xHist } );
    m_histoDigitXY.setAxis<1>( Axis1D{ m_yHist } );
    m_histoET.setTitle( "ET" );
    m_histoET.setAxis<0>( Axis1D{ m_etHist } );
    m_histoADC.setTitle( "ADC" );
    m_histoADC.setAxis<0>( Axis1D{ m_adcHist } );
  } );
}

template <LHCb::Detector::Calo::CellCode::Index calo>
void LHCb::Calo::DigitMonitor<calo>::operator()( const Input& digits, const LHCb::ODIN& odin,
                                                 const DeCalorimeter& caloDet ) const {
  if ( digits.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Found empty container in " << inputLocation() << endmsg; }
    return;
  }

  // count digits NB: digits.size() has a fixed value no matter the state of the detector
  uint ndigits = 0;

  for ( const auto& digit : digits ) {
    const auto id  = digit.cellID();
    const auto e   = digit.energy();
    const auto adc = digit.adc();
    const auto et  = e * caloDet.cellSine( id );
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    if ( adc < m_adcFilter ) continue;
    const double x = caloDet.cellCenter( id ).X();
    const double y = caloDet.cellCenter( id ).Y();
    if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing ) {
      ++ndigits;
      ++m_histoDigitX[x];
      ++m_histoDigitY[y];
      ++m_histoDigitXY[{ x, y }];
    }
    m_histoAccEnergy[id] += e;
    ++m_histoAccDigits[id];
    m_histoAccADCs[id] += adc;
    ++m_histoET[et];
    ++m_histoADC[adc];
  }
  m_digits += ndigits;
  if ( odin.bunchCrossingType() == LHCb::ODINImplementation::v7::ODIN::BXTypes::BeamCrossing )
    ++m_histoMultiplicity[ndigits];
  return;
}
