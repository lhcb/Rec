/***************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellIDHistogram.h"
#include "Event/CaloDigits_v2.h"
#include "Event/ODIN.h"
#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators/HistogramArray.h"
#include "Gaudi/Accumulators/StaticHistogram.h"

#include <type_traits>

#include <fmt/format.h>
#include <fmt/ostream.h>

namespace LHCb::Calo {

  /**
   *   The algorithm for trivial monitoring of the "CaloDigits" as a function of
   *   the bunch crossing ID.
   *   The algorithm produces the following histograms:
   *    1. CaloDigit Energy per BXID
   *    2. CaloDigit ADCs per BXID
   *    3. Accumulated adc deposits per cell and per TAE window,
   *       for the two subchannels of the cells
   *    4. One histogram per crate, acumulating adc deposits per BXID
   *   The same set of histograms, but with cut on ADC is produced if specified.
   *
   *   Histograms reside in the directory @p /stat/"Name" , where
   *   @p "Name" is the name of the algorithm
   *
   *   @author Nuria Valls Canudas nuria.valls.canudas@cern.ch
   *   @date   25/11/2021
   */
  template <Detector::Calo::CellCode::Index calo>
  class TimeAlignment final
      : public Gaudi::Functional::Consumer<void( const Event::Calo::Digits&, const ODIN&, const DeCalorimeter& ),
                                           DetDesc::usesConditions<DeCalorimeter>> {
  public:
    void operator()( const Event::Calo::Digits&, const ODIN&, const DeCalorimeter& ) const override;
    TimeAlignment( const std::string& name, ISvcLocator* pSvcLocator );

  private:
    Gaudi::Property<std::vector<int>> m_TAE_BXIDs{ this, "TAE_BXIDs" };
    Gaudi::Property<float>            m_adcFilter{ this, "ADCFilter", -100. };

    mutable Gaudi::Accumulators::StaticHistogram<2> m_ta_e{
        this, "TA_E", "Energy per BX", { 3600, 0, 3600 }, { 124, 0, 250. * Gaudi::Units::GeV } };
    mutable Gaudi::Accumulators::StaticHistogram<2> m_ta_adc{
        this, "TA_ADC", "ADC per BX", { 3600, 0, 3600 }, { 4096, 0, 4096 } };
    mutable Gaudi::Accumulators::StaticHistogram<1> m_tae_bx{ this, "TAE BXIDs", "TAE BXIDs", { 4000, 0, 4000 } };

    struct FormatCrateHistTitle {
      auto operator()( size_t n ) { return fmt::format( "TAE_crate{}", n + DeCalorimeter::minCrate<calo> ); }
    };

    // We create one histogram per crate for tae
    mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, DeCalorimeter::nbCrates<calo>>
        m_tae_crate{ this, FormatCrateHistTitle{}, FormatCrateHistTitle{}, { 3600, 0, 3600 }, { 3600, 0, 3600 } };
    // 2D histogram, where the first dimension is the CellID and the second the tae window value
    mutable StaticWeightedCellIDHistogram<calo, 2, Gaudi::Accumulators::atomicity::full, double> m_tae_window{
        this,
        fmt::format( "{}Cells", toString( calo ) ),
        fmt::format( "{} tae window per channel", toString( calo ) ),
        { 11, -5, 6 } };
  };

  DECLARE_COMPONENT_WITH_ID( TimeAlignment<Detector::Calo::CellCode::Index::EcalCalo>, "CaloECALTimeAlignment" )
  DECLARE_COMPONENT_WITH_ID( TimeAlignment<Detector::Calo::CellCode::Index::HcalCalo>, "CaloHCALTimeAlignment" )

} // namespace LHCb::Calo

template <LHCb::Detector::Calo::CellCode::Index calo>
LHCb::Calo::TimeAlignment<calo>::TimeAlignment( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "Input", CaloFutureAlgUtils::CaloFutureDigitLocation( name ) },
                  KeyValue{ "ODINLocation", ODINLocation::Default },
                  KeyValue{ "Detector", LHCb::Calo::Utilities::DeCaloFutureLocation( name ) } } ) {}

template <LHCb::Detector::Calo::CellCode::Index calo>
void LHCb::Calo::TimeAlignment<calo>::operator()( const Event::Calo::Digits& digits, const LHCb::ODIN& odin,
                                                  const DeCalorimeter& caloDet ) const {
  if ( digits.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
    return;
  }

  const auto bcID = odin.bunchId();
  for ( const auto& digit : digits ) {
    if ( digit.adc() < m_adcFilter ) continue;
    ++m_ta_e[{ bcID, digit.energy() }];
    ++m_ta_adc[{ bcID, digit.adc() }];

    // Fill per crate histograms (TAE mode)
    const auto crate = caloDet.cardCrate( caloDet.cardNumber( digit.cellID() ) );
    ++m_tae_crate[crate - DeCalorimeter::minCrate<calo>][{ bcID, digit.adc() }];

    // Fill cell by cell histograms (TAE mode)
    int TAEwindow = -9999;
    for ( auto TAEbx : m_TAE_BXIDs ) {
      if ( ( bcID < TAEbx + 5 ) & ( bcID > TAEbx - 5 ) ) { TAEwindow = bcID - TAEbx; }
      ++m_tae_bx[TAEbx];
    }
    if ( TAEwindow > -9999 ) { m_tae_window[{ digit.cellID(), TAEwindow }] += digit.adc(); }
  }
}
