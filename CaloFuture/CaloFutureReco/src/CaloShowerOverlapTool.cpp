/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Core/FloatComparison.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloHypoTool.h"
#include "ICaloShowerOverlapTool.h" // Interface
#include "boost/container/flat_map.hpp"
#include <functional>
#include <utility>

/** @class CaloFutureShowerOverlapTool CaloFutureShowerOverlapTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */

namespace {
  std::string correctionType( const std::string& name ) {
    const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos )
      return "SplitPhoton";
    return "Photon";
  }

  Gaudi::LorentzVector to_p4( Gaudi::XYZPointF loc, float e ) {
    auto dir = ( loc - Gaudi::XYZPointF{ 0, 0, 0 } ).Unit();
    return { e * dir.x(), e * dir.y(), e * dir.z(), e };
  }
} // namespace

namespace LHCb::Calo::Tools {

  class ShowerOverlap final : public extends<GaudiTool, Interfaces::IShowerOverlap> {

  public:
    using extends::extends;

    // if niter < 0, stop if the tolerance criteria is satisfied (see definition),
    // perform maximum -niter iterations
    void process( const DeCalorimeter& calo,
                  LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c1,
                  LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c2,
                  IGeometryInfo const& geometry, int niter, propagateInitialWeights, applyCorrections ) const override;

  private:
    void evaluate( const DeCalorimeter& calo,
                   LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c,
                   IGeometryInfo const& geometry, bool hypoCorrection ) const;

    Gaudi::Property<unsigned int> m_minSize{ this, "ClusterMinSize", 2 };
    Gaudi::Property<std::string>  m_pcond{ this, "Profile",
#ifdef USE_DD4HEP
                                          "/world/DownstreamRegion/Ecal:PhotonShowerProfile"
#else
                                          "Conditions/Reco/Calo/PhotonShowerProfile"
#endif
    };
    ToolHandle<Interfaces::IProcessHypos> m_stool{
        this, "CaloFutureSCorrection", "CaloFutureSCorrection/" + correctionType( name() ) + "SCorrection" };
    ToolHandle<LHCb::Calo::Interfaces::IProcessHypos> m_ltool{
        this, "CaloFutureLCorrection", "CaloFutureLCorrection/" + correctionType( name() ) + "LCorrection" };
    ToolHandle<Correction::Base> m_shape{ this, "CaloFutureCorrectionBase", "CaloFutureCorrectionBase/ShowerProfile" };
    mutable Gaudi::Accumulators::Counter<> m_skippedSize{ this, "Overlap correction skipped due to cluster size" };
    mutable Gaudi::Accumulators::Counter<> m_skippedEnergy{ this, "Overlap correction skipped due to cluster energy" };
    mutable Gaudi::Accumulators::Counter<> m_positionFailed{ this, "Cluster position failed" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_lcorr_error{ this, "LCorrection could not be evaluated" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_scorr_error{ this, "SCorrection could not be evaluated" };
  };

  namespace {

    using WeightMap = boost::container::flat_map<LHCb::Detector::Calo::CellID, float>;

    float showerFraction( Correction::Parameters const& baseParams, Correction::Base const& shape, float d3d,
                          unsigned int area ) {
      LHCb::Detector::Calo::CellID cellID( Detector::Calo::CellCode::Index::EcalCalo, area, 0, 0 ); // fake cell
      return std::clamp( shape.getCorrection( baseParams, Correction::Type::profile, cellID, d3d ).value_or( 0. ), 0.f,
                         1.f );
    }

    [[gnu::always_inline]] inline float
    fraction( Correction::Parameters const& baseParams, Correction::Base const& shape, DeCalorimeter const& det,
              LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cluster,
              LHCb::Detector::Calo::CellID cellID, float ed, int area ) {

      auto xc   = cluster.position().x();
      auto yc   = cluster.position().y();
      auto zc   = cluster.position().z();
      auto xd   = det.cellX( cellID );
      auto yd   = det.cellY( cellID );
      auto zd   = ( xc * xc + yc * yc + zc * zc - xc * xd - yc * yd ) / zc;
      auto d3d2 = (float)( ( xd - xc ) * ( xd - xc ) + ( yd - yc ) * ( yd - yc ) + ( zd - zc ) * ( zd - zc ) );
      auto size = det.cellSize( cellID );
      auto d3d  = LHCb::Math::Approx::approx_sqrt( d3d2 ) / size;
      auto f    = showerFraction( baseParams, shape, d3d, area );
      auto ec   = f * cluster.e();
      return ( ed > ec ) ? ( ed - ec ) / ed : 0.;
    }

    template <typename Map, typename Predicate>
    void erase_if( Map& map, Predicate&& predicate ) {
      auto i = map.begin();
      while ( i != map.end() ) {
        if ( std::invoke( predicate, std::as_const( *i ) ) )
          i = map.erase( i );
        else
          ++i;
      }
    }

    template <typename Map>
    Map makeWeights(
        LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl1,
        LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl2 ) {
      Map weights;
      weights.reserve( cl1.entries().size().cast() + cl2.entries().size().cast() );
      for ( const auto i1 : cl1.entries() ) {
        [[maybe_unused]] auto [it, inserted] = weights.try_emplace( i1.cellID(), i1.fraction() );
        assert( inserted );
      }
      for ( const auto i2 : cl2.entries() ) {
        auto [it, inserted] = weights.try_emplace( i2.cellID(), i2.fraction() );
        if ( !inserted ) it->second += i2.fraction();
      }
      // check
      erase_if( weights, []( const auto& p ) { return p.second == 1.; } );
      return weights;
    }

    template <typename GetWeight, typename GetFraction, typename Evaluate>
    [[gnu::always_inline, gnu::flatten]] inline void
    subtract( LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl1,
              LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl2,
              const GetWeight& getWeight, const GetFraction& getFraction, const Evaluate& evaluate,
              IGeometryInfo const& ) {

      auto skip = []( auto status ) {
        return status.noneOf( { CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForPosition } );
      };

      // cluster1  -> cluster2 spillover
      for ( auto i2 : cl2.entries() ) {
        if ( skip( i2.status() ) ) continue;
        i2.setFraction( getFraction( cl1, i2, 1 ) * getWeight( i2.cellID() ) );
      }

      // re-evaluate cluster2 accordingly
      evaluate( cl2 );
      if ( cl2.e() < 0 ) return; // skip negative energy "clusters"

      // cluster2  -> cluster1 spillover
      for ( auto i1 : cl1.entries() ) {
        if ( skip( i1.status() ) ) continue;
        auto initialWeight = getWeight( i1.cellID() );
        i1.setFraction( getFraction( cl2, i1, 2 ) * initialWeight );
        constexpr float eps = 1.e-4;
        // normalize the sum of partial weights in case of  shared cells
        for ( auto i2 : cl2.entries() ) {
          if ( !( i2.cellID() == i1.cellID() ) || skip( i2.status() ) ) continue;
          auto f1  = i1.fraction();
          auto f2  = i2.fraction();
          auto sum = f1 + f2;
          if ( std::abs( sum - initialWeight ) > eps ) {
            if ( sum < initialWeight && essentiallyZero( f2 ) ) {
              i2.setFraction( initialWeight - f1 );
            } else if ( sum < initialWeight && essentiallyZero( f1 ) ) {
              i1.setFraction( initialWeight - f2 );
            } else {
              i1.setFraction( initialWeight * f1 / ( f1 + f2 ) );
              i2.setFraction( initialWeight * f2 / ( f1 + f2 ) );
            }
          }
        }
      }

      // reevaluate  cluster1 & 2 accordingly
      evaluate( cl1 );
      evaluate( cl2 );
    }
  } // namespace

  void ShowerOverlap::process(
      const DeCalorimeter&                                                                              calo,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl1,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl2,
      IGeometryInfo const& geometry, int niter, propagateInitialWeights propagateInitialWeights,
      applyCorrections applyCorrections ) const {

    if ( cl1.entries().size().cast() < (int)m_minSize || cl2.entries().size().cast() < (int)m_minSize ) {
      ++m_skippedSize;
      return; // skip small clusters
    }

    // settings for stopping criterias
    // typical energy resolution (LHCb note 2003 091, page 5): sigma_E/E = (10.2+-0.3)%/sqrt(E) convoluted with
    // (1.6+-0.1)%
    auto resolution_e = []( float e ) {
      constexpr float part2_squared       = 1.6 * 1.6;
      constexpr float part1_squared       = 10.2 * 10.2 * 1000.0;
      const float     relative_resolution = LHCb::Math::Approx::approx_sqrt( part1_squared / e + part2_squared );
      return relative_resolution / 100.0 * e;
    };
    // typical transversal resolution (LHCb note 2003 091, page 5): 1.5mm, 3mm and 7mm for the inner, middle and outer
    // part of Ecal, respectively (∼ 4%, 5% and 6% of the cell size)
    constexpr float resolution_transversal_all[3] = { 7.0, 3.0, 1.5 };
    // distance in transversal plane
    auto distance = []( const float x1, const float x2, const float y1, const float y2 ) {
      const float dx = x2 - x1;
      const float dy = y2 - y1;
      return LHCb::Math::Approx::approx_sqrt( dx * dx + dy * dy );
    };
    // tolerance (as fraction of typical resolution)
    constexpr float tolerance = 0.01; // very safe, 1% of typical resolution
    // store energies and cluster positions at each iteration to determine if the algorithm could be stopped
    auto last_e_cl1 = cl1.e();
    auto last_e_cl2 = cl2.e();
    auto last_x_cl1 = cl1.position().x();
    auto last_x_cl2 = cl2.position().x();
    auto last_y_cl1 = cl1.position().y();
    auto last_y_cl2 = cl2.position().y();
    // currently do not check z: seems that it changes sizably only at 1st iteration, and can't find typical resolution
    // values
    // auto last_z_cl1 = cl1.position().z();
    // auto last_z_cl2 = cl2.position().z();
    // temporary stuff
    // static int niter_tot = 0;
    // static int ncalls = 0;

    // 0 - evaluate parameters (optionally applying photon hypo corrections for the position)
    evaluate( calo, cl1, geometry, bool( applyCorrections ) );
    evaluate( calo, cl2, geometry, bool( applyCorrections ) );

    if ( cl1.e() <= 0. || cl2.e() <= 0 ) {
      ++m_skippedEnergy;
      return;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << " ======== Shower Overlap =======" << endmsg;
      verbose() << " CL1/CL2 : " << cl1.e() << " " << cl2.e() << " " << cl1.e() + cl2.e() << endmsg;
      verbose() << " seed    : " << cl1.cellID() << " " << cl2.cellID() << endmsg;
      verbose() << " area    : " << cl1.cellID().area() << " " << cl2.cellID().area() << endmsg;
      verbose() << " position  : " << cl1.position() << " / " << cl2.position() << endmsg;
      verbose() << " >> E  : " << cl1.e() << " / " << cl2.e() << endmsg;
    }

    // get parameters from base class, needed to call getCorrection*
    Correction::Parameters const& baseParams = m_shape->getParameters();

    const auto evaluate_ = [this, applyCorrections, &geometry, &calo]( auto& cluster ) {
      return this->evaluate( calo, cluster, geometry, bool( applyCorrections ) );
    };
    const auto weight_ = [weights = ( propagateInitialWeights ? makeWeights<WeightMap>( cl1, cl2 ) : WeightMap{} )](
                             LHCb::Detector::Calo::CellID id ) {
      if ( weights.empty() ) return 1.;
      auto it = weights.find( id );
      return it != weights.end() ? it->second : 1.;
    };
    const auto fraction_ =
        [shape = std::cref( *m_shape ), det = std::cref( calo ), a1 = cl1.cellID().area(), a2 = cl2.cellID().area(),
         &baseParams](
            LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c,
            const auto& entry, int flag ) {
          return fraction( baseParams, shape, det, c, entry.cellID(), entry.energy(), flag == 1 ? a1 : a2 );
        };
    // 1 - determine the energy fractions of each entry
    // if niter < 0, stop if the tolerance criteria is satisfied (see definition),
    // perform maximum -niter iterations
    for ( int iter = 0; iter < abs( niter ); ++iter ) {

      subtract( cl1, cl2, weight_, fraction_, evaluate_, geometry );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " ------ iter = " << iter << endmsg;
        verbose() << " >> CL1/CL2 : " << cl1.e() << " " << cl2.e() << "  " << cl1.e() + cl2.e() << endmsg;
        verbose() << " >> position  : " << cl1.position() << " / " << cl2.position() << endmsg;
        verbose() << " >> E  : " << cl1.e() << " / " << cl2.e() << endmsg;
        auto m = ( to_p4( cl1.position(), cl1.e() ) + to_p4( cl2.position(), cl2.e() ) ).mass();
        verbose() << " >> Mass : " << m << endmsg;
      }

      if ( niter < 0 ) {
        if ( distance( cl1.position().x(), last_x_cl1, cl1.position().y(), last_y_cl1 ) <
                 ( tolerance * resolution_transversal_all[cl1.cellID().area()] ) &&
             distance( cl2.position().x(), last_x_cl2, cl2.position().y(), last_y_cl2 ) <
                 ( tolerance * resolution_transversal_all[cl2.cellID().area()] ) &&
             fabs( cl1.e() - last_e_cl1 ) < ( tolerance * resolution_e( cl1.e() ) ) &&
             fabs( cl2.e() - last_e_cl2 ) < ( tolerance * resolution_e( cl2.e() ) ) ) {
          // niter_tot += iter + 1;
          // ncalls++;
          // always() << "stopped after iteration " << iter << " (average " << 1.0 * niter_tot / ncalls <<  ")" <<
          // endmsg;
          break;
        }
        // else if (iter == (abs(niter) - 1))
        //{
        // niter_tot += abs(niter);
        // always() << "stopped after iteration " << iter << " (average " << 1.0 * niter_tot / ncalls <<  ")" << endmsg;
        // ncalls++;
        //}
        last_e_cl1 = cl1.e();
        last_e_cl2 = cl2.e();
        last_x_cl1 = cl1.position().x();
        last_x_cl2 = cl2.position().x();
        last_y_cl1 = cl1.position().y();
        last_y_cl2 = cl2.position().y();
        // last_z_cl1 = cl1.position().z();
        // last_z_cl2 = cl2.position().z();
      }
    }
    // 3 - reset cluster-like parameters
    evaluate( calo, cl1, geometry, false );
    evaluate( calo, cl2, geometry, false );
  }

  void ShowerOverlap::evaluate(
      const DeCalorimeter&                                                                              calo,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cluster,
      IGeometryInfo const& geometry, bool hypoCorrection ) const {

    // 0 - reset z-position of cluster
    LHCb::ClusterFunctors::ZPosition zPosition( &calo );
    cluster.template field<LHCb::Event::Calo::v2::ClusterTag::Position>().setZ( zPosition( cluster ) );

    // 1 - 3x3 energy and energy-weighted barycenter
    auto ret = LHCb::Calo::Functor::calculateClusterEXY( cluster.entries(), &calo );
    if ( ret ) {
      cluster.setEnergy( ret->Etot );
      cluster.setPosition( { ret->x, ret->y, cluster.position().Z() } );
    } else {
      ++m_positionFailed;
    }

    if ( cluster.e() < 0 ) return; // skip correction for negative energy "clusters"
    if ( !hypoCorrection ) return; // do not apply 'photon' hypo correction

    // 2 - apply 'photon hypothesis' corrections

    // Apply transversal corrections and then Apply longitudinal correction
    m_stool->process( calo, LHCb::Event::Calo::Hypotheses::Type::Photon, cluster, geometry )
        .orElse( [&] { ++m_scorr_error; } )
        .andThen(
            [&] { return m_ltool->process( calo, LHCb::Event::Calo::Hypotheses::Type::Photon, cluster, geometry ); } )
        .orElse( [&] { ++m_lcorr_error; } )
        .ignore();
  }
} // namespace LHCb::Calo::Tools

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::Tools::ShowerOverlap, "CaloFutureShowerOverlapTool" )
