/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/StatusCode.h"

namespace LHCb::Calo::Reco {
  enum class ErrorCode : StatusCode::code_t {
    WRONG_NUMBER_OF_PARAMETERS_SIGMOID = 10,
    WRONG_NUMBER_OF_PARAMETERS_SSHAPE,
    WRONG_NUMBER_OF_PARAMETERS_SHOWERPROFILE,
    WRONG_NUMBER_OF_PARAMETERS_SINUSIODAL,
    NO_TOOLS,
    NO_CLUSTER,
    NO_SEED_CELL,
    NO_SEED_DIGIT,
    NO_CLUSTER,
    WRONG_NUMBER_OF_PARAMETERS,
    CANNOT_ACCESS_DB,
    TAGGER_NOT_FOUND,
    CANNOT_OBTAIN_ENERGY_MASK_FROM_DB,
    CANNOT_OBTAIN_POSITION_MASK_FROM_DB
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "LHCb::Calo::Reco::Errors"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<ErrorCode>( code ) ) {
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS_SIGMOID:
        return "The power sigmoid function must have 4 parameters";
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS_SSHAPE:
        return "The Sshape function must have 1 parameter";
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS_SHOWERPROFILE:
        return "The ShowerProfile function must have 10 parameters";
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS_SINUSIODAL:
        return "The Sinusoidal function must have 1 parameter";
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS_POWERSIGMOID:
        return "The power sigmoid function must have 4 parameters";
      case ErrorCode::NO_TOOLS:
        return "Empty list of tools";
      case ErrorCode::NO_CLUSTER:
        return "CaloCluster* points to NULL -> no correction applied";
      case ErrorCode::NO_SEED_CELL:
        return "The seed cell is not found -> no correction applied";
      case ErrorCode::NO_SEED_DIGIT:
        return "Seed digit points to NULL -> no correction applied";
      case ErrorCode::NO_CLUSTER:
        return "CaloCluster* points to NULL!";
      case ErrorCode::WRONG_NUMBER_OF_PARAMETERS:
        return "Parameters vector exceeds the number of known parameters";
      case ErrorCode::CANNOT_ACCESS_DB:
        return "Cannot access DB";
      case ErrorCode::TAGGER_NOT_FOUND:
        return "Tagger not found";
      case ErrorCode::CANNOT_OBTAIN_ENERGY_MASK_FROM_DB:
        return "Cannot update energy mask from DB";
      case ErrorCode::CANNOT_OBTAIN_POSITION_MASK_FROM_DB:
        return "Cannot update position mask from DB";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace LHCb::Calo::Reco
STATUSCODE_ENUM_DECL( LHCb::Calo::Reco::ErrorCode )
STATUSCODE_ENUM_IMPL( LHCb::Calo::Reco::ErrorCode, LHCb::Calo::Reco::ErrorCategory )
