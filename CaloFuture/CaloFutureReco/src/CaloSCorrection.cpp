/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloCorrectionBase.h"
#include "Core/FloatComparison.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloHypoTool.h"
#include <string>

/** @class CaloFutureSCorrection CaloFutureSCorrection.h
 *  @file
 *  Implementation file for class : CaloFutureSCorrection
 *
 *  @date 2003-03-10
 *  @author Deschamps Olivier
 *
 *  Adam Szabelski
 *  date 2019-10-15
 *
 */
namespace {
  void updateCovariance(
      float dXhy_dXcl, float dYhy_dYcl,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>& cluster ) {

    // cov.m packing in double array[5] following ROOT::Math::SMatrix<double,3,3>::Array()
    // for row/column indices (X:0, Y:1, E:2), see comments in CaloFutureECorrection::process()

    // cov1 = (J * cov0 * J^T) for the special case of diagonal Jacobian for (X,Y,E) -> (X1=X1(X), Y1=Y1(Y), E1=E)
    // c1[5] remains unchanged (energy is not changed by S-correction)

    auto                                                  cov = cluster.covariance();
    LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3> covariance;

    covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) =
        cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) * dXhy_dXcl * dXhy_dXcl;
    covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ) =
        cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::Y ) * dXhy_dXcl * dYhy_dYcl;
    covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) =
        cov( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) * dYhy_dYcl * dYhy_dYcl;
    covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ) =
        cov( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::E ) * dXhy_dXcl;
    covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ) =
        cov( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::E ) * dYhy_dYcl;
    covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E ) =
        cov( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E );

    cluster.setCovariance( covariance );
  }
} // namespace

namespace LHCb::Calo {

  class SCorrection : public extends<LHCb::Calo::Correction::Base, LHCb::Calo::Interfaces::IProcessHypos> {
  public:
    SCorrection( const std::string& type, const std::string& name, const IInterface* parent );

    using LHCb::Calo::Interfaces::IProcessHypos::process;
    StatusCode
    process( const DeCalorimeter& calo, LHCb::Event::Calo::Hypotheses::Type,
             LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>&,
             IGeometryInfo const& geometry ) const override;

  private:
    struct SCorrInputParams {
      LHCb::Detector::Calo::CellID cellID;
      Gaudi::XYZPoint              seedPos;
      float                        x;
      float                        y;
      float                        z;
    };

    struct SCorrResults {
      float xCor;
      float yCor;
      float dXhy_dXcl;
      float dYhy_dYcl;
    };

    /// calculate corrected CaloHypo position depending on CaloCluster position
    SCorrResults calculateSCorrections( Correction::Parameters const&, DeCalorimeter const&,
                                        SCorrInputParams const& ) const;

    void debugDerivativesCalculation( Correction::Parameters const&, DeCalorimeter const&, SCorrInputParams const&,
                                      SCorrResults const& ) const;

  private:
    std::array<float, 4> compute( const DeCalorimeter& calo, SCorrInputParams params ) const {
      /** here all information is available
       *
       *  ( ) Ecal energy in 3x3     :   ( not used )
       *  ( ) Prs and Spd energies   :   ( not available )
       *  (3) weighted barycenter    :    xBar, yBar
       *  ( ) Zone/Area in Ecal      :    area        ( not used )
       *  (5) SEED digit             :    seed   (NOT FOR SPLITPHOTONS !!)
       *  (6) CellID of seed digit   :    cellID
       *  (7) Position of seed cell  :    seedPos
       */

      // get parameters from base class, needed to call getCorrection*
      Correction::Parameters const& baseParams = getParameters();

      SCorrResults results = calculateSCorrections( baseParams, calo, params );
      float        xCor    = results.xCor;
      float        yCor    = results.yCor;

      float dXhy_dXcl = results.dXhy_dXcl;
      float dYhy_dYcl = results.dYhy_dYcl;

      // protection against unphysical d(Xhypo)/d(Xcluster) == 0 or d(Yhypo)/d(Ycluster) == 0
      if ( fabs( dXhy_dXcl ) < 1e-10 ) {
        warning() << "unphysical d(Xhypo)/d(Xcluster) = " << dXhy_dXcl << " reset to 1 as if Xhypo = Xcluster"
                  << endmsg;
        dXhy_dXcl = 1.;
      }
      if ( fabs( dYhy_dYcl ) < 1e-10 ) {
        warning() << "unphysical d(Yhypo)/d(Ycluster) = " << dYhy_dYcl << " reset to 1 as if Yhypo = Ycluster"
                  << endmsg;
        dYhy_dYcl = 1.;
      }

      if ( msgLevel( MSG::DEBUG ) && m_correctCovariance )
        debugDerivativesCalculation( baseParams, calo, params, results );

      return { xCor, yCor, dXhy_dXcl, dYhy_dYcl };
    }
  };

  DECLARE_COMPONENT_WITH_ID( SCorrection, "CaloFutureSCorrection" )

  SCorrection::SCorrection( const std::string& type, const std::string& name, const IInterface* parent )
      : extends( type, name, parent ) {

    // define conditionName
    const std::string uName( LHCb::CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "ELECTRON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:ElectronSCorrection"
#else
                   "Conditions/Reco/Calo/ElectronSCorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:SplitPhotonSCorrection"
#else
                   "Conditions/Reco/Calo/SplitPhotonSCorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "PHOTON" ) ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:PhotonSCorrection"
#else
                   "Conditions/Reco/Calo/PhotonSCorrection"
#endif
                   )
          .ignore();
    }
  }
  // ============================================================================

  StatusCode SCorrection::process(
      const DeCalorimeter& calo, LHCb::Event::Calo::Hypotheses::Type hypo,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>& cluster,
      IGeometryInfo const& ) const {
    auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo );
    if ( m_hypos.end() == h ) return Error( "Invalid hypothesis!", StatusCode::SUCCESS );

    if ( cluster.e() < 0. ) { return StatusCode::SUCCESS; }
    const auto& entries = cluster.entries();
    auto        iseed =
        LHCb::ClusterFunctors::locateDigit( entries.begin(), entries.end(), LHCb::CaloDigitStatus::Mask::SeedCell );
    if ( entries.end() == iseed ) {
      Warning( "The seed cell is not found -> no correction applied", StatusCode::SUCCESS ).ignore();
      return StatusCode::FAILURE;
    }

    const auto&                         position = cluster.position();
    const float                         xBar     = position.x();
    const float                         yBar     = position.y();
    const float                         z        = position.z();
    const LHCb::Detector::Calo::CellID& cellID   = cluster.cellID();
    auto                                seedPos  = calo.cellCenter( cellID );

    auto [xCor, yCor, dXhy_dXcl, dYhy_dYcl] = compute( calo, { cellID, seedPos, xBar, yBar, z } );

    cluster.setPosition( { xCor, yCor, z } );
    if ( m_correctCovariance ) updateCovariance( dXhy_dXcl, dYhy_dYcl, cluster );

    return StatusCode::SUCCESS;
  }

  // ============================================================================

  SCorrection::SCorrResults SCorrection::calculateSCorrections( Correction::Parameters const& baseParams,
                                                                DeCalorimeter const&          calo,
                                                                SCorrInputParams const&       params ) const {
    SCorrResults                        results{ 0, 0, 0, 0 };
    const LHCb::Detector::Calo::CellID& cellID  = params.cellID;
    const Gaudi::XYZPoint&              seedPos = params.seedPos;
    const float&                        z       = params.z;
    float                               xBar    = params.x;
    float                               yBar    = params.y;

    float CellSize = calo.cellSize( cellID );
    float Asx      = -( xBar - seedPos.x() ) / CellSize;
    float Asy      = -( yBar - seedPos.y() ) / CellSize;

    // Sshape correction :
    auto AsxCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::shapeX, cellID, Asx )
                         .value_or( Correction::Result{ Asx, 1. } );
    Asx                  = AsxCorDer.value; // Asx1
    const auto DshapeX   = AsxCorDer.derivative;
    auto       AsyCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::shapeY, cellID, Asy )
                         .value_or( Correction::Result{ Asy, 1. } );
    Asy                = AsyCorDer.value; // Asy1
    const auto DshapeY = AsyCorDer.derivative;

    // Angular correction (if any) [ NEW  - inserted between Sshape and residual correction ]
    const float xs              = seedPos.x() - Asx * CellSize; // xscor
    const float ys              = seedPos.y() - Asy * CellSize; // yscor
    const float thx             = LHCb::Math::fast_atan2( xs, (float)z );
    const float thy             = LHCb::Math::fast_atan2( ys, (float)z );
    const auto [daX, DangularX] = getCorrectionAndDerivative( baseParams, Correction::Type::angularX, cellID, thx )
                                      .value_or( Correction::Result{ 0., 0. } );
    const auto [daY, DangularY] = getCorrectionAndDerivative( baseParams, Correction::Type::angularY, cellID, thy )
                                      .value_or( Correction::Result{ 0., 0. } );
    Asx -= daX;
    Asy -= daY;

    // residual correction (if any):
    auto dcXCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::residual, cellID, Asx )
                         .value_or( Correction::Result{ 0., 0. } );
    auto dcX = dcXCorDer.value;
    if ( essentiallyZero( dcX ) ) {
      // check X-specific correction
      dcXCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::residualX, cellID, Asx )
                      .value_or( Correction::Result{ 0., 0. } );
      dcX = dcXCorDer.value;
    }
    const auto DresidualX = dcXCorDer.derivative;
    auto       dcYCorDer  = getCorrectionAndDerivative( baseParams, Correction::Type::residual, cellID, Asy )
                         .value_or( Correction::Result{ 0., 0. } );
    auto dcY = dcYCorDer.value;
    if ( essentiallyZero( dcY ) ) {
      // check Y-specific correction
      dcYCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::residualY, cellID, Asy )
                      .value_or( Correction::Result{ 0., 0. } );
      dcY = dcYCorDer.value;
    }
    const auto DresidualY = dcYCorDer.derivative;
    Asx -= dcX;
    Asy -= dcY;

    // left/right - up/down asymmetries correction (if any) :
    const auto [ddcX, DasymX] =
        getCorrectionAndDerivative( baseParams, ( xBar < 0 ) ? Correction::Type::asymM : Correction::Type::asymP,
                                    cellID, Asx )
            .value_or( Correction::Result{ 0., 0. } );
    const auto [ddcY, DasymY] =
        getCorrectionAndDerivative( baseParams, ( yBar < 0 ) ? Correction::Type::asymM : Correction::Type::asymP,
                                    cellID, Asy )
            .value_or( Correction::Result{ 0., 0. } );
    Asx += ddcX; // Asx4
    Asy += ddcY; // Asy4

    // Recompute position and fill CaloFuturePosition

    results.xCor = seedPos.x() - Asx * CellSize;
    results.yCor = seedPos.y() - Asy * CellSize;

    /* DG,20140714: derivative calculation for  d(Xhypo)/d(Xcluster)
     *
     * Asx0 =-(xBar - seedPos.x)/CellSize; // xBar = Xcluster
     * Asx1 = shapeX(Asx0)
     * xs   = seedPos.x - Asx1*CellSize
     * thx  = atan(xs/z); // in principle, this brings in an implicit dependence on cluster E, but it's logarithmic so
     * let's neglect it daX  = angular(thx) Asx2 = Asx1 - daX dcX  = residual(Asx2) != 0 ? residual(Asx2) :
     * residualX(Asx2); // add an auxiliary bool residualX_flag Asx3 = Asx2 - dcX ddcX = asym(Asx3) Asx4 = Asx3 + ddcX =
     * Asx Xhypo= xCor(Asx4)  = seedPos.x - Asx4*CellSize
     *
     * d(Xhypo)/d(Xcluster) = d(xCor)/d(Asx4) * product[ d(Asx%i)/d(Asx%{i-1}), for i=1..4 ] * d(Asx0)/d(Xcluster)
     *
     * d(xCor)/d(Asx4)      =-CellSize
     * d(Asx0)/d(Xcluster)  = d(Asx0)/d(xBar)     = -1/CellSize
     * d(Asx1)/d(Asx0)      = DshapeX(Asx0)
     * d(thx)/d(Asx1)       = d(thx)/d(xs) * d(xs)/d(Asx1) =-CellSize/(1+(xc/z)**2)*(1/z)
     * d(xs)/d(Asx1)        =-CellSize
     * d(Asx2)/d(Asx1)      = 1 - d(daX)/d(Asx1)  = 1 - Dangular(thx)*d(thx)/d(Asx1) = 1 +
     * Dangular(thx)*CellSize/z/(1+(xs/z)**2) d(Asx3)/d(Asx2)      = 1 - d(dcX)/d(Asx2)  = 1 - ( residual(Asx2) != 0 ?
     * Dresidual(Asx2) : DresidualX(Asx2) ) residualX_flag       = residual(Asx2) != 0 ? false : true d(Asx4)/d(Asx3) =
     * 1
     * + d(ddcX)/d(Asx3) = 1 + Dasym(Asx3)
     *
     *
     * d(Xhypo)/d(Xcluster) = (1 + Dasym(Asx3)) * (1 - (resudualX_flag ? DresidualX(Asx2) : Dresidual(Asx2)))
     *                       *(1 + Dangular(thx)*CellSize/z/(1+(xs/z)**2)) * DshapeX(Asx0)
     */

    if ( m_correctCovariance ) {
      // calculation of the analytic derivatives:
      // NB: printouts comparing analytic calculations with numeric derivatives which are commented-out below
      // are useful for debugging in case of changes in the correction function code
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "---------- analytic derivatives of individual S-correction functions ---------------" << endmsg;

      float tx = xs / z;
      float ty = ys / z;

      results.dXhy_dXcl =
          ( 1. + DasymX ) * ( 1. - DresidualX ) * ( 1. + DangularX * CellSize / ( z * ( 1. + tx * tx ) ) ) * DshapeX;
      results.dYhy_dYcl =
          ( 1. + DasymY ) * ( 1. - DresidualY ) * ( 1. + DangularY * CellSize / ( z * ( 1. + ty * ty ) ) ) * DshapeY;
    }
    return results;
  }

  void SCorrection::debugDerivativesCalculation( Correction::Parameters const& baseParams, DeCalorimeter const& calo,
                                                 SCorrInputParams const& inParams,
                                                 SCorrResults const&     outParams ) const {
    const float d_rel( 1.e-5 ); // dx ~ 0.1 mm for numeric derivative calculation

    debug() << " ---------- calculation of numeric derivative dXhypo/dXcluster follows -----------" << endmsg;
    SCorrInputParams inParams1( inParams );
    inParams1.x     = inParams1.x * ( 1 + d_rel );
    inParams1.y     = inParams1.y;
    auto outParams1 = calculateSCorrections( baseParams, calo, inParams1 );

    debug() << " ---------- calculation of numeric derivative dYhypo/dYcluster follows -----------" << endmsg;
    SCorrInputParams inParams2( inParams );
    inParams2.x     = inParams2.x;
    inParams2.y     = inParams2.y * ( 1 + d_rel );
    auto outParams2 = calculateSCorrections( baseParams, calo, inParams2 );

    float xCor_x = outParams1.xCor;
    float yCor_x = outParams1.yCor;

    float xCor_y = outParams2.xCor;
    float yCor_y = outParams2.yCor;

    float xBar = inParams.x;
    float yBar = inParams.y;

    float xCor      = outParams.xCor;
    float yCor      = outParams.yCor;
    float dXhy_dXcl = outParams.dXhy_dXcl;
    float dYhy_dYcl = outParams.dYhy_dYcl;

    const float dn_xCor_dx = ( xCor_x - xCor ) / xBar / d_rel;
    const float dn_yCor_dx = ( yCor_x - yCor ) / xBar / d_rel; // sanity test, should be 0
    const float dn_xCor_dy = ( xCor_y - xCor ) / yBar / d_rel; // sanity test, should be 0
    const float dn_yCor_dy = ( yCor_y - yCor ) / yBar / d_rel;

    if ( std::abs( ( dXhy_dXcl - dn_xCor_dx ) / dXhy_dXcl ) > 0.02 ||
         std::abs( ( dYhy_dYcl - dn_yCor_dy ) / dYhy_dYcl ) > 0.02 || std::abs( dn_yCor_dx ) > 1e-8 ||
         std::abs( dn_xCor_dy ) > 1e-7 )
      debug() << " SCorrection numerically-calculated Jacobian differs (by > 2%) from analytically-calculated one"
              << endmsg;

    debug() << "================== Jacobian elements ============= " << endmsg;
    debug() << "  semi-analytic dXhy_dXcl = " << dXhy_dXcl << " numeric dn_xCor_dx = " << dn_xCor_dx
            << " dn_xCor_dy = " << dn_xCor_dy << endmsg;
    debug() << "  semi-analytic dYhy_dYcl = " << dYhy_dYcl << " numeric dn_yCor_dy = " << dn_yCor_dy
            << " dn_yCor_dx = " << dn_yCor_dx << endmsg;
  }
} // namespace LHCb::Calo
