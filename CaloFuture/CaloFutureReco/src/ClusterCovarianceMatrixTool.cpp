/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ClusterCovarianceMatrixTool.h"

#include <yaml-cpp/yaml.h>

/** @file
 *
 *  Implementation file for class FutureClusterCovarianceMatrixTool
 *
 *  @date 02/11/2001
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  modified 02/07/2014 by O. Deschamps
 */

namespace LHCb::Calo {
  DECLARE_COMPONENT_WITH_ID( ClusterCovarianceMatrixTool, "FutureClusterCovarianceMatrixTool" )

  CovarianceMatrixTool::detail::ParameterMap
  ClusterCovarianceMatrixTool::getParams( Correction::Parameters const& baseParams ) {
    LHCb::Calo::CovarianceMatrixTool::detail::ParameterMap parameters;
    for ( unsigned int area = 0; area < 3; ++area ) { // loop over calo area
      const auto& params = m_dbAccessor->getParamVector(
          baseParams, Correction::Type::ClusterCovariance,
          Detector::Calo::CellID{ Detector::Calo::CellCode::Index::EcalCalo, area, 0, 0 } );
      // FIXME: read Calo index from DeCalorimeter
      if ( params.size() > CovarianceMatrixTool::detail::Parameter::Last ) ++m_parExceeded;
      if ( params.size() < CovarianceMatrixTool::detail::Parameter::Last ) {
        throw GaudiException( "ClusterCovarianceMatrixTool::getParams",
                              "Not enough values found in DB for ClusterCovariance", StatusCode::FAILURE );
      }
      for ( auto index : LHCb::Calo::CovarianceMatrixTool::detail::Parameters{} ) {
        parameters[index].push_back( params[index] );
      }
    }
    // info
    info() << " \t ==  Parameters for covariance estimation ==" << endmsg;
    for ( auto index : CovarianceMatrixTool::detail::Parameters{} ) {
      info() << CovarianceMatrixTool::detail::Parameters::name( index ) << " \t : " << parameters[index] << " "
             << CovarianceMatrixTool::detail::Parameters::unit( index ) << endmsg;
    }
    return parameters;
  }

  StatusCode ClusterCovarianceMatrixTool::initialize() {
    return ConditionAccessorHolder::initialize().andThen( [&] {
      // depends on m_conditionName so that calls to m_dbAccessor give proper results
      addConditionDerivation(
          { m_conditionName, m_dbAccessor->getParamConditionPath() }, m_parameters.key(),
          [&]( YAML::Node const&, Correction::Parameters const& baseParams ) { return getParams( baseParams ); } );
      return StatusCode::SUCCESS;
    } );
  }
} // namespace LHCb::Calo
