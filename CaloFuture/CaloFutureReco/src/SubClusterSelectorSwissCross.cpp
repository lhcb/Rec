/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CellSwissCross.h"
#include "SubClusterSelectorBase.h"

// ============================================================================
/** @file FutureSubClusterSelectorSwissCross.cpp
 *
 *  Implementation file for class : FutureSubClusterSelectorSwissCross
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 07/11/2001
 */
// ============================================================================
namespace LHCb::Calo {

  class SubClusterSelectorSwissCross : public SubClusterSelectorBase {
  public:
    using SubClusterSelectorBase::SubClusterSelectorBase;

    // ============================================================================
    /** The main processing method
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    // ============================================================================
    StatusCode
    tag( const DeCalorimeter& calo,
         Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
             entries ) const override {
      return Functor::Cluster::tagTheSubCluster( entries, CellSwissCross{ &calo }, modify(), mask(),
                                                 DigitStatus::Mask::ModifiedBySwissCrossTagger );
    }

    // ============================================================================
    /** The main processing method (untag)
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    // ============================================================================
    StatusCode
    untag( const DeCalorimeter& calo,
           Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
               entries ) const override {
      return Functor::Cluster::untagTheSubCluster( entries, CellSwissCross{ &calo },
                                                   DigitStatus::Mask::ModifiedBySwissCrossTagger );
    }
  };

  DECLARE_COMPONENT_WITH_ID( SubClusterSelectorSwissCross, "SubClusterSelectorSwissCross" )

} // namespace LHCb::Calo
// ============================================================================
// The End
// ============================================================================
