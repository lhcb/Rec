/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Core/FloatComparison.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypo.h"
#include "Event/CaloHypos_v2.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation2D.h"
#include "Relations/RelationWeighted1D.h"
#include "Relations/RelationWeighted2D.h"
#include <deque>

/** @class CaloHypoConverter HypoConverter.h
 *  @author Gerhard Raven
 *  @date   2020-06-10
 */
namespace LHCb::Converters::Calo::EC {

  enum class ErrorCode : StatusCode::code_t {
    INDEX_NOT_UNIQUE = 10,
    INCONSISTENT_DIGIT_ENERGY,
    NO_HYPO_MATCH_FOUND,
    NO_CLUSTER_MATCH_FOUND,
    TOO_MANY_MATCHES,
    UNEXPECTED_SIZE,
    NAME_MUST_CONTAIN_SPLIT,
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "CaloConverters"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<ErrorCode>( code ) ) {
      case ErrorCode::INDEX_NOT_UNIQUE:
        return "makeIndex: Not unique";
      case ErrorCode::INCONSISTENT_DIGIT_ENERGY:
        return "Inconsistent digit energy";
      case ErrorCode::NO_HYPO_MATCH_FOUND:
        return "could not find matching hypothesis";
      case ErrorCode::NO_CLUSTER_MATCH_FOUND:
        return "could not find matching cluster";
      case ErrorCode::TOO_MANY_MATCHES:
        return "too many matches";
      case ErrorCode::UNEXPECTED_SIZE:
        return "Unexpected size";
      case ErrorCode::NAME_MUST_CONTAIN_SPLIT:
        return "SplitPhotonsFromMergedPi0 TES location must contain \"split\"";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace LHCb::Converters::Calo::EC

STATUSCODE_ENUM_DECL( LHCb::Converters::Calo::EC::ErrorCode )
STATUSCODE_ENUM_IMPL( LHCb::Converters::Calo::EC::ErrorCode, LHCb::Converters::Calo::EC::ErrorCategory )

namespace LHCb::Converters::Calo {
  namespace {

    [[gnu::noreturn]] void throw_exception( EC::ErrorCode ec, const char* tag ) {
      auto sc = StatusCode( ec );
      throw GaudiException{ sc.message(), tag, std::move( sc ) };
    }

#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )

    LHCb::CaloCluster::Type toType( LHCb::Event::Calo::Clusters::Type in ) {
      switch ( in ) {
      case LHCb::Event::Calo::Clusters::Type::Invalid:
        return LHCb::CaloCluster::Type::Invalid;
      case LHCb::Event::Calo::Clusters::Type::CellularAutomaton:
        return LHCb::CaloCluster::Type::CellularAutomaton;
      case LHCb::Event::Calo::Clusters::Type::Area3x3:
        return LHCb::CaloCluster::Type::Area3x3;
      case LHCb::Event::Calo::Clusters::Type::Area2x2:
        return LHCb::CaloCluster::Type::Area2x3x3; // FIXME: I suspect a typo in the original CaloCluster code...
      default:
      case LHCb::Event::Calo::Clusters::Type::Undefined:
        return LHCb::CaloCluster::Type::Undefined;
      }
    }

    LHCb::CaloHypo::Hypothesis toHypo( LHCb::Event::Calo::Enum::Hypothesis in ) {
      switch ( in ) {
      case LHCb::Event::Calo::Enum::Hypothesis::Undefined:
        return LHCb::CaloHypo::Hypothesis::Undefined;
      case LHCb::Event::Calo::Enum::Hypothesis::Mip:
        return LHCb::CaloHypo::Hypothesis::Mip;
      case LHCb::Event::Calo::Enum::Hypothesis::Photon:
        return LHCb::CaloHypo::Hypothesis::Photon;
      case LHCb::Event::Calo::Enum::Hypothesis::PhotonFromMergedPi0:
        return LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0;
      case LHCb::Event::Calo::Enum::Hypothesis::BremmstrahlungPhoton:
        return LHCb::CaloHypo::Hypothesis::BremmstrahlungPhoton;
      case LHCb::Event::Calo::Enum::Hypothesis::Pi0Resolved:
        return LHCb::CaloHypo::Hypothesis::Pi0Resolved;
      case LHCb::Event::Calo::Enum::Hypothesis::Pi0Overlapped:
        return LHCb::CaloHypo::Hypothesis::Pi0Overlapped;
      case LHCb::Event::Calo::Enum::Hypothesis::Pi0Merged:
        return LHCb::CaloHypo::Hypothesis::Pi0Merged;
      case LHCb::Event::Calo::Enum::Hypothesis::EmCharged:
        return LHCb::CaloHypo::Hypothesis::EmCharged;
      case LHCb::Event::Calo::Enum::Hypothesis::NeutralHadron:
        return LHCb::CaloHypo::Hypothesis::NeutralHadron;
      case LHCb::Event::Calo::Enum::Hypothesis::ChargedHadron:
        return LHCb::CaloHypo::Hypothesis::ChargedHadron;
      case LHCb::Event::Calo::Enum::Hypothesis::Jet:
        return LHCb::CaloHypo::Hypothesis::Jet;
      default:
        return LHCb::CaloHypo::Hypothesis::Undefined;
      }
    }

    enum struct Policy { requireUnique, takeFirst };

    template <Policy policy, typename KeyedContainer, typename Id = LHCb::Calo::Functor::CellID_t>
    auto makeIndex( KeyedContainer const& container, Id id = {} ) {
      auto index = std::vector<typename KeyedContainer::contained_type const*>( LHCb::Detector::Calo::Index::max() );
      for ( const auto* c : container ) {
        if ( !c ) continue;
        auto cellid = id( *c );
        if ( !cellid ) continue;
        auto& i = index[LHCb::Detector::Calo::Index{ cellid }];
        if constexpr ( policy == Policy::requireUnique ) {
          if ( i ) OOPS( EC::ErrorCode::INDEX_NOT_UNIQUE );
          i = c;
        } else if constexpr ( policy == Policy::takeFirst ) {
          if ( !i ) i = c;
        } else {
          throw std::logic_error( "unknown policy" );
        }
      }
      return index;
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    auto createCaloPosition( LHCb::Event::Calo::Clusters::const_reference<simd, behaviour> cl ) {
      const auto& pos = cl.position();
      return LHCb::CaloPosition{}
          .setZ( pos.z() )
          .setParameters( { pos.x(), pos.y(), cl.e() } )
          .setCovariance( cl.covariance() )
          .setCenter( cl.center() )
          .setSpread( cl.spread() );
    }

    template <typename ClusterContainer, typename DigitContainer>
    const LHCb::CaloCluster*
    convert( LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cl,
             ClusterContainer& clusterContainer, DigitContainer& digitContainer ) {

      auto cluster = std::make_unique<LHCb::CaloCluster>();
      cluster->setType( toType( cl.type() ) );
      cluster->setSeed( cl.cellID() );
      cluster->setPosition( createCaloPosition( cl ) );

      auto& entries = cluster->entries();
      entries.reserve( cl.size() );
      const auto& r = cl.entries();
      std::transform( r.begin(), r.end(), std::back_inserter( entries ), [&]( const auto& e ) {
        assert( static_cast<bool>( e.cellID() ) );
        auto digi = digitContainer( e.cellID() );
        if ( !digi ) {
          auto dig = std::make_unique<LHCb::CaloDigit>( e.cellID(), e.energy() );
          digi     = dig.get();
          digitContainer.insert( dig.release() );
        } else {
          if ( !essentiallyEqual( (float)digi->e(), e.energy() ) ) OOPS( EC::ErrorCode::INCONSISTENT_DIGIT_ENERGY );
        }
        return LHCb::CaloClusterEntry{ digi, e.status(), e.fraction() };
      } );

      auto ret = cluster.get();
      clusterContainer.insert( cluster.release() );
      return ret;
    }

    template <typename HypoContainer, typename ClusterConverter>
    LHCb::CaloHypo* convert_hypo(
        LHCb::Event::Calo::Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> h,
        HypoContainer& outputHypos, ClusterConverter convert_cluster ) {
      // sadly we can't use the cellid as key as it is not unique (mergedpi0s etc..)
      auto hypo = std::make_unique<LHCb::CaloHypo>();
      hypo->setHypothesis( toHypo( h.hypothesis() ) );
      hypo->setPosition( std::make_unique<LHCb::CaloPosition>( createCaloPosition( h.clusters()[0] ) ) );
      // overwrite part of default CaloPosition with the hypo settings...
      hypo->position()->setZ( h.position().z() ).setParameters( { h.position().x(), h.position().y(), h.energy() } );

      for ( const auto& c : h.clusters() ) {
        auto out = convert_cluster( c );
        hypo->addToClusters( out );
        hypo->addToDigits( out->entries() );
      }
      LHCb::CaloHypo* ret = hypo.get();
      outputHypos.insert( hypo.release() );
      return ret;
    }

    template <typename HypoContainer, typename ClusterContainer, typename DigitContainer>
    LHCb::CaloHypo* convert(
        LHCb::Event::Calo::Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> h,
        HypoContainer& outputHypos, ClusterContainer& outputClusters, DigitContainer& outputDigits ) {
      return convert_hypo(
          h, outputHypos,
          [&outputClusters, &outputDigits](
              LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
                  cr ) { return convert( cr, outputClusters, outputDigits ); } );
    }

    auto make_ClusterLookup( const LHCb::CaloCluster::Container& clusters ) {
      return
          [idx = makeIndex<Policy::requireUnique>( clusters )](
              LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::ScatterGather>
                  cr ) {
            auto ptr = idx[LHCb::Detector::Calo::Index{ cr.cellID() }];
            if ( !ptr ) OOPS( EC::ErrorCode::NO_CLUSTER_MATCH_FOUND );
            return ptr;
          };
    }

    auto make_SplitClusterLookup( const LHCb::CaloCluster::Container& clusters ) {

      auto index = []( const LHCb::CaloCluster::Container& c ) {
        // allow for up to three clusters per cellID...
        auto index = std::array<std::array<const LHCb::CaloCluster*, 4>, LHCb::Detector::Calo::Index::max()>{};
        for ( const LHCb::CaloCluster* ptr : c ) {
          auto& ptrs = index[LHCb::Detector::Calo::Index{ ptr->seed() }];
          auto  p    = std::find( ptrs.begin(), ptrs.end(), nullptr );
          if ( p == ptrs.end() ) OOPS( EC::ErrorCode::TOO_MANY_MATCHES );
          *p = ptr;
        }
        return index;
      };

      return
          [idx = index( clusters )](
              LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::ScatterGather>
                  cr ) {
            auto i       = idx[LHCb::Detector::Calo::Index{ cr.cellID() }];
            auto n_match = std::distance( i.begin(), std::find( i.begin(), i.end(), nullptr ) );
            if ( n_match == 1 ) return i.front(); // fast path: single candidate, don't do anything fancy...
            if ( n_match == 0 ) OOPS( EC::ErrorCode::NO_CLUSTER_MATCH_FOUND );

            // we've got multiple candidates -- check first if requiring the same cellIDs gives a unique result
            auto cands   = i;
            auto last    = std::next( cands.begin(), n_match );
            auto matches = std::partition( cands.begin(), last, [r1 = cr.entries()]( const LHCb::CaloCluster* clus ) {
              const auto& r2 = clus->entries();
              return std::equal( r1.begin(), r1.end(), r2.begin(), r2.end(),
                                 []( const auto& e1, const auto& e2 ) { return e1.cellID() == e2.cellID(); } );
            } );

            auto delta = [e = cr.energy()]( const LHCb::CaloCluster* lhs, const LHCb::CaloCluster* rhs ) {
              return std::abs( e - lhs->e() ) < std::abs( e - rhs->e() );
            };

            auto m1 = std::min_element( cands.begin(), matches, delta );
            return *( m1 != matches ? m1 : std::min_element( matches, last, delta ) );
          };
    }

    template <typename ClusterContainer, typename DigitContainer>
    auto make_ClusterConverter( ClusterContainer& outputSplitClusters, DigitContainer& outputDigits ) {
      return [outClusters = std::ref( outputSplitClusters ), outDigits = std::ref( outputDigits )](
                 LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
                     cr ) { return convert( cr, outClusters.get(), outDigits.get() ); };
    }

    template <typename HypoContainer>
    LHCb::CaloHypo* convert(
        LHCb::Event::Calo::Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> h,
        HypoContainer& outputHypos, const LHCb::CaloCluster::Container& clusters ) {
      return convert_hypo( h, outputHypos, make_ClusterLookup( clusters ) );
    }

    template <typename HypoContainer, typename MainClusterConverter, typename SplitClusterConverter>
    LHCb::CaloHypo* convertMergedPi0(
        LHCb::Event::Calo::Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> h,
        HypoContainer& outputHypos, HypoContainer& outputSplitHypos, MainClusterConverter mainClusterConverter,
        SplitClusterConverter splitClusterConverter ) {

      auto make_photon =
          [&splitClusterConverter](
              const LHCb::CaloCluster* leadingCluster,
              LHCb::Event::Calo::Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::ScatterGather>
                  sc ) {
            auto splitCluster = splitClusterConverter( sc );
            auto g            = std::make_unique<LHCb::CaloHypo>();
            g->setHypothesis( LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 );
            g->addToClusters( leadingCluster );
            g->addToClusters( splitCluster );
            // use the original, _uncalibrated_ cluster
            auto pos = std::make_unique<LHCb::CaloPosition>( splitCluster->position() );
            // but overwrite part of default CaloPosition with the E/S/L calibrated values
            pos->setZ( sc.position().z() )
                .setParameters( { sc.position().x(), sc.position().y(), sc.energy() } )
                .setCovariance( sc.covariance() );
            g->setPosition( std::move( pos ) );
            return g;
          };

      auto hypo = std::make_unique<LHCb::CaloHypo>();
      hypo->setHypothesis( toHypo( h.hypothesis() ) );
      // note that we do NOT call hypo->setPosition and friends - to be fully backwards compatible...

      if ( h.clusters().size() != 3 ) OOPS( EC::ErrorCode::UNEXPECTED_SIZE );
      auto mainCluster = mainClusterConverter( h.clusters()[0] );

      hypo->addToClusters( mainCluster );
      hypo->addToDigits( mainCluster->entries() );

      auto g1 = make_photon( mainCluster, h.clusters()[1] );
      auto g2 = make_photon( mainCluster, h.clusters()[2] );

      hypo->addToHypos( g2.get() );
      hypo->addToHypos( g1.get() );

      outputSplitHypos.insert( g1.release() );
      outputSplitHypos.insert( g2.release() );

      LHCb::CaloHypo* ret = hypo.get();
      outputHypos.insert( hypo.release() );
      return ret;
    }

  } // namespace

  namespace Cluster::v1 {
    using Result = std::tuple<LHCb::CaloCluster::Container, LHCb::CaloDigits>;
    struct fromV2 : Algorithm::MultiTransformer<Result( const LHCb::Event::Calo::Clusters& )> {
      fromV2( const std::string& name, ISvcLocator* pSvcLocator )
          : MultiTransformer{ name,
                              pSvcLocator,
                              KeyValue{ "InputClusters", "" },
                              { KeyValue{ "OutputClusters", "" }, KeyValue{ "OutputDigits", "" } } } {}

      Result operator()( const LHCb::Event::Calo::Clusters& clusters ) const override {
        auto result                          = Result{};
        auto& [outputClusters, outputDigits] = result;
        outputClusters.reserve( clusters.size() );
        outputDigits.reserve( clusters.size() * 25 );
        for ( const auto& c : clusters.scalar() ) convert( c, outputClusters, outputDigits );
        return result;
      }
    };

    DECLARE_COMPONENT( fromV2 )
  } // namespace Cluster::v1

  namespace Hypo::v1 {
    using Table = LHCb::Relation2D<LHCb::Detector::Calo::CellID, LHCb::CaloHypo>;

    using Result = std::tuple<LHCb::CaloHypo::Container, Table>;
    struct fromV2 : Algorithm::MultiTransformer<Result( const LHCb::Event::Calo::Hypotheses&,
                                                        const LHCb::CaloCluster::Container& )> {
      fromV2( const std::string& name, ISvcLocator* pSvcLocator )
          : MultiTransformer{ name,
                              pSvcLocator,
                              { KeyValue{ "InputHypos", "" }, KeyValue{ "InputClusters", "" } },
                              { KeyValue{ "OutputHypos", "" }, KeyValue{ "OutputTable", "" } } } {}

      Result operator()( const LHCb::Event::Calo::Hypotheses& hypos,
                         const LHCb::CaloCluster::Container&  clusters ) const override {
        auto result                = Result{};
        auto& [outputHypos, table] = result;
        outputHypos.reserve( hypos.size() );
        table.reserve( hypos.size() );
        for ( const auto& h : hypos.scalar() ) {
          auto hypo = convert( h, outputHypos, clusters );
          table.i_push( h.cellID(), hypo );
        }
        table.i_sort();
        return result;
      }
    };

    DECLARE_COMPONENT( fromV2 )

    namespace MergedPi0 {

      using Result = std::tuple<LHCb::CaloHypo::Container, LHCb::CaloHypo::Container, Table>;
      struct fromV2 : Algorithm::MultiTransformer<Result( const LHCb::Event::Calo::Hypotheses&,
                                                          const LHCb::CaloCluster::Container&,
                                                          const LHCb::CaloCluster::Container& )> {
        fromV2( const std::string& name, ISvcLocator* pSvcLocator )
            : MultiTransformer{
                  name,
                  pSvcLocator,
                  { KeyValue{ "InputHypos", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "OldHypos" ) },
                    KeyValue{ "InputClusters", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "OldClusters" ) },
                    KeyValue{ "InputSplitClusters",
                              LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( "OldSplitClusters" ) } },
                  { KeyValue{ "OutputHypos", LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "OldHypos" ) },
                    KeyValue{ "OutputSplitPhotons",
                              LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "OldSplitPhotonsFromMergedPi0Hypos" ) },
                    KeyValue{ "OutputTable", "Rec/Calo/Hypo2CellID" } } } {}

        Result operator()( const LHCb::Event::Calo::Hypotheses& hypos,
                           const LHCb::CaloCluster::Container&  inputClusters,
                           const LHCb::CaloCluster::Container&  inputSplitClusters ) const override {
          if ( !LHCb::Calo::Utilities::details::contains_ci( outputLocation<1>(), "SPLIT" ) ) {
            error() << outputLocation<1>() << " should contain `split`" << endmsg;
            OOPS( EC::ErrorCode::NAME_MUST_CONTAIN_SPLIT );
          }
          auto result                               = Result{};
          auto& [outputHypos, outputPhotons, table] = result;
          outputHypos.reserve( hypos.size() );
          outputPhotons.reserve( 2 * hypos.size() );
          table.reserve( hypos.size() );
          for ( const auto& h : hypos.scalar() ) {
            auto hypo = convertMergedPi0( h, outputHypos, outputPhotons, make_ClusterLookup( inputClusters ),
                                          make_SplitClusterLookup( inputSplitClusters ) );
            table.i_push( h.cellID(), hypo );
          }
          table.i_sort();
          return result;
        }
      };

      DECLARE_COMPONENT( fromV2 )
    } // namespace MergedPi0
  }   // namespace Hypo::v1

  namespace Cluster2TrackTable::v1 {

    using OutputTable = LHCb::RelationWeighted2D<LHCb::CaloCluster, LHCb::Track, float>;
    using InputTable  = LHCb::RelationWeighted2D<LHCb::Detector::Calo::CellID, LHCb::Track, float>;

    struct fromV2 : Algorithm::Transformer<OutputTable( InputTable const&, LHCb::CaloCluster::Container const& )> {
      fromV2( const std::string& name, ISvcLocator* pSvcLocator )
          : Transformer{ name,
                         pSvcLocator,
                         { KeyValue{ "InputTable", "" }, KeyValue{ "InputClusters", "" } },
                         KeyValue{ "OutputTable", "" } } {}
      OutputTable operator()( InputTable const& in, const LHCb::CaloCluster::Container& clusters ) const override {
        auto out   = OutputTable{ in.relations().size() };
        auto index = makeIndex<Policy::takeFirst>( clusters );
        for ( const auto& r : in.relations() ) {
          // why do we have a key(edContainer) and then not use keys to look up objects?
          // because not _all_ cluster containers have clusters with unique entries (eg. 'splitClusters').
          // So the KeyedContainer<CaloCluster> cannot use the obvious cellID key, and instead it
          // uses an internally assigned int instead. So we cannot lookup by key, and thus have to make our own index
          // ;-( so we cannot just do: auto* c = clusters( r.from() ); and instead we do:
          auto c = index[LHCb::Detector::Calo::Index{ r.from() }];
          if ( !c ) OOPS( EC::ErrorCode::NO_CLUSTER_MATCH_FOUND );
          out.i_push( c, r.to(), r.weight() );
        }
        out.i_sort();
        return out;
      }
    };
    DECLARE_COMPONENT( fromV2 )

  } // namespace Cluster2TrackTable::v1

  namespace Hypo2TrackTable::v1 {

    using OutputTable = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::Track, float>;
    using InputTable  = LHCb::RelationWeighted2D<LHCb::Detector::Calo::CellID, LHCb::Track, float>;

    struct fromV2 : Algorithm::Transformer<OutputTable( InputTable const&, LHCb::CaloHypo::Container const& )> {
      fromV2( const std::string& name, ISvcLocator* pSvcLocator )
          : Transformer{ name,
                         pSvcLocator,
                         { KeyValue{ "InputTable", "" }, KeyValue{ "InputHypotheses", "" } },
                         KeyValue{ "OutputTable", "" } } {}
      OutputTable operator()( InputTable const& in, LHCb::CaloHypo::Container const& hypos ) const override {
        auto out   = OutputTable{ in.relations().size() };
        auto index = makeIndex<Policy::takeFirst>( hypos, []( const LHCb::CaloHypo& h ) {
          return LHCb::Calo::Functor::cellID( LHCb::CaloFutureAlgUtils::ClusterFromHypo( h, false ) );
        } );
        for ( const auto& r : in.relations() ) {
          auto h = index[LHCb::Detector::Calo::Index{ r.from() }];
          if ( !h ) OOPS( EC::ErrorCode::NO_HYPO_MATCH_FOUND );
          out.i_push( h, r.to(), r.weight() );
        }
        out.i_sort();
        return out;
      }
    };
    DECLARE_COMPONENT( fromV2 )
  } // namespace Hypo2TrackTable::v1

  namespace Hypo2TruthTable::v1 {

    using OutputTable = LHCb::RelationWeighted2D<LHCb::CaloHypo, LHCb::MCParticle, float>;
    using Old2NewHypo = Hypo::v1::Table;
    using InputTable  = LHCb::RelationWeighted1D<LHCb::Detector::Calo::CellID, LHCb::MCParticle, float>;

    struct fromV2 : Algorithm::MultiTransformer<std::tuple<OutputTable, LHCb::LinksByKey>(
                        LHCb::CaloHypo::Container const&, Old2NewHypo const&, InputTable const& )> {
      fromV2( const std::string& name, ISvcLocator* pSvcLocator )
          : MultiTransformer{ name,
                              pSvcLocator,
                              { KeyValue{ "InputHypos", "" }, KeyValue{ "Old2New", "" }, KeyValue{ "InputTable", "" } },
                              { KeyValue{ "OutputTable", "" }, KeyValue{ "OutputLinks", "" } } } {}
      std::tuple<OutputTable, LHCb::LinksByKey> operator()( LHCb::CaloHypo::Container const& hypos,
                                                            Old2NewHypo const&               convert,
                                                            InputTable const&                in ) const override {
        auto out =
            std::tuple{ OutputTable{ hypos.size() * 3 },
                        LHCb::LinksByKey{ std::in_place_type<LHCb::CaloHypo>, std::in_place_type<LHCb::MCParticle>,
                                          LHCb::LinksByKey::Order::decreasingWeight } };
        auto& [table, links] = out;
        auto convert_inverse = convert.inverse();
        for ( const auto& h : hypos ) {
          auto id = convert_inverse->relations( h );
          if ( id.size() != 1 ) OOPS( EC::ErrorCode::UNEXPECTED_SIZE );
          for ( const auto& mc : in.relations( id.front().to() ) ) {
            table.i_push( h, mc.to(), mc.weight() );
            links.link( h, mc.to(), mc.weight() );
          }
        }
        table.i_sort();
        return out;
      }
    };
    DECLARE_COMPONENT( fromV2 )

  } // namespace Hypo2TruthTable::v1
} // namespace LHCb::Converters::Calo
