/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SubClusterSelectorTool.h"

#include "fmt/format.h"

#include <yaml-cpp/yaml.h>

//-----------------------------------------------------------------------------
// Implementation file for class : SubClusterSelectorTool
//
// 2014-06-20 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace LHCb::Calo {
  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( SubClusterSelectorTool, "FutureSubClusterSelectorTool" )

  SubClusterSelectorTool::SubClusterSelectorTool( const std::string& type, const std::string& name,
                                                  const IInterface* parent )
      : ConditionAccessorHolder( type, name, parent ) {
    declareInterface<SubClusterSelectorTool>( this );
    // inherit properties from parents
    if ( parent ) {
      if ( const IProperty* prop = dynamic_cast<const IProperty*>( parent ); prop ) {
        if ( prop->hasProperty( "EnergyTags" ) )
          setProperty( prop->getProperty( "EnergyTags" ) ).orThrow( "oops", "oops" );
        if ( prop->hasProperty( "PositionTags" ) )
          setProperty( prop->getProperty( "PositionTags" ) ).orThrow( "oops", "oops" );
      }
    }
    // apply local default setting if not defined by parent
    if ( m_taggerE.empty() ) m_taggerE = std::vector<std::string>( 1, "useDB" );
    if ( m_taggerP.empty() ) m_taggerP = std::vector<std::string>( 1, "useDB" );
  }

  StatusCode SubClusterSelectorTool::initialize() {
    return ConditionAccessorHolder::initialize().andThen( [&] {
      // depends on m_condition so that calls to m_dbAccessor give proper results
      addConditionDerivation(
          { m_condition, m_dbAccessor->getParamConditionPath() }, m_tags.key(),
          [&]( YAML::Node const&, Correction::Parameters const& baseParams ) { return getParams( baseParams ); } );
      return StatusCode::SUCCESS;
    } );
  }

  SubClusterSelectorTool::Tags SubClusterSelectorTool::getParams( Correction::Parameters const& baseParams ) {
    Tags tags;
    // FIXME: get it from DeCalorimeter instead
    static constexpr int narea   = 3;
    auto                 caloidx = LHCb::Detector::Calo::CellCode::Index::EcalCalo;

    // extend tagger per area when needed
    if ( m_taggerE.size() == 1 ) m_taggerE = std::vector<std::string>( narea, m_taggerE[0] );
    if ( m_taggerP.size() == 1 ) m_taggerP = std::vector<std::string>( narea, m_taggerP[0] );
    if ( m_taggerE.size() != narea || m_taggerP.size() != narea ) {
      throw GaudiException( "SubClusterSelectorTool", "You must define the tagger for each calo area",
                            StatusCode::FAILURE );
    }
    using namespace CaloFutureClusterMask;
    std::string sourceE = " (none)";
    std::string sourceP = " (none)";

    for ( unsigned int area = 0; area < narea; ++area ) {
      LHCb::Detector::Calo::CellID id       = LHCb::Detector::Calo::CellID( caloidx, area, 0, 0 );
      std::string                  areaName = id.areaName();

      std::string nameE = m_taggerE[area];
      if ( nameE != "useDB" ) {
        sourceE = " (from options) ";
        std::string taggerE =
            ( m_clusterTaggers.find( nameE ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameE] : "";
        if ( taggerE.empty() ) {
          throw GaudiException( "SubClusterSelectorTool",
                                "Cannot find a  '" + nameE +
                                    "' tagger - You must select or define a known tagging method",
                                StatusCode::FAILURE );
        }
        Interfaces::ISubClusterTag* tE = tool<Interfaces::ISubClusterTag>( taggerE, areaName + "EnergyTagger", this );
        tE->setMask( m_energyStatus );
        tags[0].push_back( tE );
      } else {
        sourceE = " (from DB) ";
        // default is when no DB
        Mask maskE = (Mask)m_dbAccessor->getParameter( baseParams, Correction::Type::EnergyMask, 0, id ).value_or( 0. );
        std::string name    = maskName[maskE];
        std::string taggerE = ( m_clusterTaggers.find( name ) != m_clusterTaggers.end() ) ? m_clusterTaggers[name] : "";
        if ( !taggerE.empty() ) {
          Interfaces::ISubClusterTag* tE =
              tool<Interfaces::ISubClusterTag>( taggerE, id.areaName() + "EnergyTagger", this );
          tE->setMask( m_energyStatus );
          tags[0].push_back( tE );
        } else {
          ++m_energy_update_failed;
        }
      }

      std::string nameP = m_taggerP[area];
      if ( nameP != "useDB" ) {
        sourceP = " (from options) ";
        std::string taggerP =
            ( m_clusterTaggers.find( nameP ) != m_clusterTaggers.end() ) ? m_clusterTaggers[nameP] : "";
        if ( taggerP.empty() ) {
          throw GaudiException( "SubClusterSelectorTool",
                                "Cannot find a  '" + nameP +
                                    "' tagger - You must select or define a known tagging method",
                                StatusCode::FAILURE );
        }
        Interfaces::ISubClusterTag* tP = tool<Interfaces::ISubClusterTag>( taggerP, areaName + "PositionTagger", this );
        tP->setMask( m_positionStatus );
        tags[1].push_back( tP );
      } else {
        sourceP = " (from DB) ";
        // default is 3x3 when no DB
        Mask maskP =
            (Mask)m_dbAccessor->getParameter( baseParams, Correction::Type::PositionMask, 0, id ).value_or( 0. );
        std::string name    = maskName[maskP];
        std::string taggerP = ( m_clusterTaggers.find( name ) != m_clusterTaggers.end() ) ? m_clusterTaggers[name] : "";
        if ( !taggerP.empty() ) {
          Interfaces::ISubClusterTag* tP =
              tool<Interfaces::ISubClusterTag>( taggerP, id.areaName() + "PositionTagger", this );
          tP->setMask( m_positionStatus );
          tags[1].push_back( tP );
        } else
          ++m_position_update_failed;
      }
    }
    info() << " Energy   mask : " << sourceE << endmsg;
    info() << " Position mask : " << sourceP << endmsg;
    return tags;
  }

} // namespace LHCb::Calo
