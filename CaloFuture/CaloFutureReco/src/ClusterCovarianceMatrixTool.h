/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloCorrectionBase.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Core/FloatComparison.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Parsers/CommonParsers.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToStream.h"
#include "boost/container/small_vector.hpp"
#include "fmt/format.h"
#include <functional>
#include <iostream>
#include <vector>

/** @class ClusterCovarianceMatrixTool
 *         ClusterCovarianceMatrixTool.h
 *
 *  Concrete tool for calculation of covariance matrix
 *  for the whole cluster object
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

/** @class CovarianceEstimator CovarianceEstimator.h
 *
 *  simple helper class for estimation of covariance matrix
 *  for CaloCluster object.
 *
 *   Model:   ("M{}" means "expectation", "D{}" means "dispersion")
 *
 *   All cluster quantities could be calculated if one knows the matrix
 *
 *   Cov{i,j} = M{(e(i)-M{e(i)})*(e(j)-M{e(j)})}
 *
 *   where "e(i)" is energy deposited in cell with index "i":
 *
 *   e(i) =  Sg * E(i)  + Gain * ( noise1 + noise2 )
 *
 *   where: "Sg"     - relative gain fluctuations;
 *          "E(i)"   - intrinsic energy (with stochastic flustuations);
 *          "Gain"   - gain for given channel;
 *          "noise1" - incoherent noise;
 *          "noise2" - coherent noise;
 *
 *   This model results in following exporession for covariance matrix:
 *   (diagonal elements)
 *
 *   Cov{i,i} =   D{e(i)} = M{(e(i)-M{e(i)})*(e(i)-M{e(i)})}
 *
 *            =   E(i)      * E(i)      * D{Sg}     +
 *                M{Sg}     * M{Sg}     * D{E(i)}   +
 *                M{Gain}   * M{Gain}   * D{noise1} +
 *                M{Gain}   * M{Gain}   * D{noise2} +
 *                M{noise1} * M{noise1} * M{Gain}   +
 *                M{noise2} * M{noise2} * M{Gain}
 *
 *   for non-diagonal elements (i != j ) one has:
 *
 *   Cov{i,j} =   Cov{j,i} = M{(e(i)-M{e(i)})*(e(j)-M{e(j)})}
 *
 *            =   M{ Gain } * M{ Gain } * D(noise2)
 *
 *   According to the obvious definitions:
 *
 *      "M{ Sg     }"     = 1
 *
 *      "M{ noise1 }" = 0, "mean" value of noise is sero
 *
 *      "M{ noise2 }" = 0, "mean" value of noise is zero
 *
 *
 *   other parameters could be extractes from external parametrisation:
 *
 *      "D{ E(i)   }" = E(i) * "A" * "A" * GeV
 *
 *      "D{ Sg     }" = relative gain sigma squared
 *
 *      "D{ noise1 }" = sigma of incoherent noise squared
 *
 *      "D{ noise2 }" = sigma on   coherent noise squared
 *
 *      "M{ Gain   }" is extracted for each cell from DeCalorimeter object
 *
 *  At next step one is able to calculate calculate intermediate values:
 *
 *     Total cluster energy         "Etot" = Sum_i{ 1.0  * e(i) }
 *
 *     Energy weighted X            "Ex"   = Sum_i{ x(i) * e(i) }
 *
 *     Energy weighted Y            "Ey"   = Sum_i{ y(i) * e(i) }
 *
 *  Since transformation from "e(i)" to ("Etot","Ex","Ey")
 *  id a linear transformation,  the covariance matrix
 *  for ("Etot","Ex","Ey") quantities could be calculated
 *  in an easy and transparent way:
 *
 *  Cov{ Etot, Etot } = Sum_ij { 1.0  * Cov{i,j} * 1.0  }
 *
 *  Cov{ Etot, Ex   } = Sum_ij { x(i) * Cov{i,j} * 1.0  }
 *
 *  Cov{ Ex  , Ex   } = Sum_ij { x(i) * Cov{i,j} * x(i) }
 *
 *  Cov{ Etot, Ey   } = Sum_ij { y(i) * Cov{i,j} * 1.0  }
 *
 *  Cov{ Ex  , Ey   } = Sum_ij { x(i) * Cov{i,j} * y(i) }
 *
 *  Cov{ Ey  , Ey   } = Sum_ij { y(i) * Cov{i,j} * y(i) }
 *
 *  And the last step: we calculate the final quantities:
 *
 *   total energy of cluster:  "Ecl" = Etot
 *
 *   X-position of barycenter: "Xcl" = Ex/Etot
 *
 *   Y-position of barycenter: "Ycl" = Ey/Etot
 *
 *  The calculation of covariance matrix for final values
 *  is a little bit tedious, since transformation from
 *  ("Etot","Ex","Ey") to ("Ecl","Xcl","Ycl") is not linear,
 *  but it could be done using analytical expansion:
 *
 *  Cov{ Ecl , Ecl } =              Cov{ Etot , Etot }
 *
 *  Cov{ Ecl , Xcl } =              Cov{ Etot , Ex   } / Ecl -
 *                            Xcl * Cov{Etot,Etot}/Ecl
 *
 *  Cov{ Xcl , Xcl } =              Cov{ Ex   , Ex   } / Ecl / Ecl +
 *                      Xcl * Xcl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ex   } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ex   } / Ecl / Ecl
 *
 *  Cov{ Ecl , Ycl } =              Cov{ Etot , Ey   } / Ecl -
 *                            Ycl * Cov{ Etot , Etot } / Ecl
 *
 *  Cov{ Xcl , Ycl } =              Cov{ Ex   , Ey   } / Ecl / Ecl +
 *                      Xcl * Ycl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Xcl * Cov{ Etot , Ey   } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ex   } / Ecl / Ecl
 *
 *  Cov{ Ycl , Ycl } =              Cov{ Ey   , Ey   } / Ecl / Ecl +
 *                      Ycl * Ycl * Cov{ Etot , Etot } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ey   } / Ecl / Ecl -
 *                            Ycl * Cov{ Etot , Ey   } / Ecl / Ecl
 *
 *  @author Ivan Belyaev
 *  @date   06/07/2001
 */
namespace LHCb::Calo::CovarianceMatrixTool::detail {
  enum Parameter {
    Stochastic      = 0, // stochastig term     Cov(EE)_i +=  [ S  * sqrt(E_i_GeV) ]^2
    GainError       = 1, // constant term       Cov(EE)_i +=  [ G  * E_i           ]^2
    IncoherentNoise = 2, // noise (inc.) term   Cov(EE)_i +=  [ iN * gain_i        ]^2
    CoherentNoise   = 3, // noise (coh.) term   Cov(EE)_i +=  [ cN * gain_i        ]^2
    ConstantE       = 4, // additional term     Cov(EE) +=  [ cE               ]^2
    ConstantX       = 5, // additional term     Cov(XX) +=  [ cX               ]^2
    ConstantY       = 6, // additional term     Cov(XX) +=  [ cY               ]^2
    Last
  };
  struct Parameters {
    struct Iterator {
      unsigned            i;
      constexpr Parameter operator*() noexcept { return static_cast<Parameter>( i ); }
      constexpr Iterator& operator++() noexcept {
        ++i;
        return *this;
      }
      constexpr friend bool operator!=( Iterator lhs, Iterator rhs ) { return lhs.i != rhs.i; }
      constexpr friend int  operator-( Iterator lhs, Iterator rhs ) {
        return static_cast<int>( lhs.i ) - static_cast<int>( rhs.i );
      }
    };
    constexpr static auto begin() { return Iterator{ 0 }; }
    constexpr static auto end() { return Iterator{ Parameter::Last }; }
    constexpr static auto size() { return Parameter::Last; }

    static const char* name( Parameter p ) {
      constexpr auto names = std::array{ "Stochastic", "GainError", "IncoherentNoise", "CoherentNoise",
                                         "ConstantE",  "ConstantX", "ConstantY" };
      return names[p];
    }
    static const char* unit( Parameter p ) {
      constexpr auto units = std::array{ "Sqrt(GeV)", "", "ADC", "ADC", "MeV", "mm", "mm" };
      return units[p];
    }
  };
} // namespace LHCb::Calo::CovarianceMatrixTool::detail

namespace std {
  template <>
  struct iterator_traits<LHCb::Calo::CovarianceMatrixTool::detail::Parameters::Iterator> {
    using difference_type   = int;
    using iterator_category = std::random_access_iterator_tag;
  };
} // namespace std

namespace LHCb::Calo::CovarianceMatrixTool::detail {
  inline Parameter to_parameter( std::string_view sv ) {
    auto ps = Parameters{};
    return *std::find_if( ps.begin(), ps.end(), [&]( auto p ) { return Parameters::name( p ) == sv; } );
  }

  class ParameterMap {
    std::bitset<Parameters::size()>                     m_present;
    std::array<std::vector<double>, Parameters::size()> m_params; // TODO: replace vector by static_vector<double,
                                                                  // maxareas>

  public:
    [[nodiscard]] bool                       empty() const { return m_present.none(); }
    [[nodiscard]] bool                       contains( Parameter p ) const { return m_present.test( p ); }
    [[nodiscard]] const std::vector<double>& at( Parameter p ) const {
      if ( !contains( p ) )
        throw std::out_of_range{ fmt::format( "Parameter '{}' not present", Parameters::name( p ) ) };
      return m_params[p];
    }
    [[nodiscard]] std::vector<double>& operator[]( Parameter p ) {
      if ( !contains( p ) ) insert( p, {} );
      return m_params[p];
    }
    void insert( Parameter p, std::initializer_list<double> i ) {
      m_params[p].assign( i );
      m_present.set( p );
    }
    friend StatusCode parse( ParameterMap& p, const std::string& s ) {
      std::map<std::string, std::vector<double>> m;
      return Gaudi::Parsers::parse( m, s ).andThen( [&]() -> StatusCode {
        for ( auto& [k, v] : m ) {
          auto idx = to_parameter( k );
          if ( idx == Parameter::Last ) return StatusCode::FAILURE;
          p.m_params[idx]  = std::move( v );
          p.m_present[idx] = true;
        }
        return StatusCode::SUCCESS;
      } );
    }
    friend std::string toString( ParameterMap const& pm ) {
      std::map<std::string, std::vector<double>> m;
      for ( auto p : Parameters{} ) {
        if ( pm.contains( p ) ) m.insert( { Parameters::name( p ), pm.at( p ) } );
      }
      return Gaudi::Utils::toString( m );
    }
    friend std::ostream& operator<<( std::ostream& os, ParameterMap const& pm ) { return os << toString( pm ); }
  };
} // namespace LHCb::Calo::CovarianceMatrixTool::detail

namespace LHCb::Calo {
  class ClusterCovarianceMatrixTool : public LHCb::DetDesc::ConditionAccessorHolder<extends<GaudiTool, IAlgTool>> {
  public:
    using ConditionAccessorHolder::ConditionAccessorHolder;
    StatusCode initialize() override;

    CovarianceMatrixTool::detail::ParameterMap const& getParameters() const { return m_parameters.get(); }

    template <typename ClusterType>
    [[gnu::always_inline]] StatusCode compute( CovarianceMatrixTool::detail::ParameterMap const& params,
                                               DeCalorimeter const& calo, ClusterType cluster ) const {

      /// apply the estimator

      // ignore trivial cases
      if ( cluster.entries().empty() ) return StatusCode::SUCCESS;

      auto               entries = cluster.entries();
      const unsigned int size    = entries.size().cast();

      // auxillary arrays
      boost::container::small_vector<float, 32>  x( size, 0 );    ///< x-position of cell [i]
      boost::container::small_vector<float, 32>  y( size, 0 );    ///< y-position of cell [i]
      boost::container::small_vector<double, 32> gain( size, 0 ); ///< gain of cell[i]

      // calculate intermediate values
      //    eT = sum_i { 1.0  * e(i) }
      //    eX = sum_i { x(i) * e(i) }
      //    eY = sum_i { y(i) * e(i) }
      // and their covariance matrix

      float eTE  = 0;
      float eTP  = 0;
      float eTEP = 0;
      float eX   = 0;
      float eY   = 0;

      const LHCb::Detector::Calo::CellID seedID = cluster.cellID();
      // the matrices:
      auto See   = s2E( params, seedID ); // add constant term to global cov(EE)
      auto Sex   = 0.0f;
      auto Sxx   = s2X( params, seedID ); // cov(XX)_0
      auto Sey   = 0.0f;
      auto Sxy   = 0.0f;
      auto Syy   = s2Y( params, seedID ); // cov(YY)_0
      auto SeeP  = s2E( params, seedID );
      auto SexEP = 0.0f;
      auto SeyEP = 0.0f;
      auto SeeEP = s2E( params, seedID );
      using namespace LHCb::CaloDigitStatus;

      for ( auto&& [i, entry] : LHCb::range::enumerate( entries ) ) {
        /// check the status
        if ( entry.status().anyOf(
                 { LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForPosition } ) )
          entry.addStatus( LHCb::CaloDigitStatus::Mask::UseForCovariance );
        else
          entry.removeStatus( LHCb::CaloDigitStatus::Mask::UseForCovariance );
        if ( !entry.status().test( LHCb::CaloDigitStatus::Mask::UseForCovariance ) ) continue;

        const LHCb::Detector::Calo::CellID id       = entry.cellID();
        const auto                         fraction = entry.fraction();
        const auto                         energy   = entry.energy() * fraction;
        const auto                         e_i      = LHCb::Calo::Functor::truncateEnergy( energy );

        // get cell position
        const Gaudi::XYZPoint& pos = calo.cellCenter( id );
        const auto             x_i = pos.x();
        const auto             y_i = pos.y();

        // intrinsic resolution
        auto s2 = std::abs( energy ) * a2GeV( params, id );
        //  gain fluctuation
        if ( auto s2g = s2gain( params, id ); !essentiallyZero( s2g ) ) s2 += energy * energy * s2g;

        //  noise (both coherent and incoherent)
        double g = 0;
        if ( auto noise = s2noise( params, id ); !essentiallyZero( noise ) ) {
          g = calo.cellGain( id );
          s2 += noise * g * g;
        }

        bool forE  = entry.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
        bool forP  = entry.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition );
        bool forEP = forE && forP;

        if ( forE ) eTE += e_i;
        if ( forP ) {
          eTP += e_i;
          eX += x_i * e_i;
          eY += y_i * e_i;
        }
        if ( forEP ) eTEP += e_i;

        const auto s_ii = s2;

        if ( forE ) See += s_ii;
        if ( forP ) {
          SeeP += s_ii;
          Sxx += x_i * s_ii * x_i;
          Sxy += x_i * s_ii * y_i;
          Syy += y_i * s_ii * y_i;
          Sex += x_i * s_ii;
          Sey += y_i * s_ii;
        }
        if ( forEP ) {
          SeeEP += s_ii;
          SexEP += x_i * s_ii;
          SeyEP += y_i * s_ii;
        }

        // second loop if there exist correlations
        if ( essentiallyZero( s2coherent( params, id ) ) ) { continue; } ///<  CONTINUE
        x[i]    = x_i;
        y[i]    = y_i;
        gain[i] = g;
        for ( auto j = i; j < size; j++ ) {
          auto jt = entries[j];
          if ( !jt.status().test( LHCb::CaloDigitStatus::Mask::UseForCovariance ) ) continue;

          // position of cell "j"
          const auto x_j = x[j];
          const auto y_j = y[j];

          // covariance between cell "i" and "j"
          const auto s_ij = s2coherent( params, id ) * gain[i] * gain[j];

          bool jforE  = jt.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
          bool jforP  = jt.status().test( LHCb::CaloDigitStatus::Mask::UseForPosition );
          bool jforEP = jforE && jforP;
          //
          if ( jforE ) See += 2.0 * s_ij;
          if ( jforP ) {
            SeeP += 2.0 * s_ij;
            Sxx += 2.0 * x_i * s_ij * x_j;
            Sxy += x_i * s_ij * y_j + x_j * s_ij * y_i;
            Syy += 2.0 * y_i * s_ij * y_j;
            Sex += x_i * s_ij + x_j * s_ij;
            Sey += y_i * s_ij + y_j * s_ij;
          }
          if ( jforEP ) {
            SeeEP += 2.0 * s_ij;
            SexEP += x_i * s_ij + x_j * s_ij;
            SeyEP += y_i * s_ij + y_j * s_ij;
          }
        } // end of loop over all digits/diagonal elements
      }   // loop over entries

      // does energy have a reasonable value?
      if ( eTE <= 0 ) cluster.setEnergy( 0 );
      if ( eTP <= 0 ) cluster.setPosition( { 0, 0, cluster.position().z() } );
      if ( eTE <= 0 || eTP <= 0 ) {
        ++m_negative_energy;
        return StatusCode::SUCCESS;
      }

      // The last step: calculate final quantities
      //   Ecl  =  eT
      //   Xcl  =  eX / eT
      //   Ycl  =  eY / eT

      const auto Ecl = eTE;
      const auto Xcl = eX / eTP;
      const auto Ycl = eY / eTP;

      // and their covariance matrix:
      const auto CovEE = See;
      const auto CovXX = ( Sxx + Xcl * Xcl * SeeP - 2.0 * Xcl * Sex ) / eTP / eTP;
      const auto CovYY = ( Syy + Ycl * Ycl * SeeP - 2.0 * Ycl * Sey ) / eTP / eTP;
      const auto CovXY = ( Sxy + Xcl * Ycl * SeeP - Ycl * Sex - Xcl * Sey ) / eTP / eTP;
      const auto CovEY = SeyEP / eTEP - Ycl * SeeEP / eTEP;
      const auto CovEX = SexEP / eTEP - Xcl * SeeEP / eTEP;

      // update cluster patameters
      cluster.setEnergy( Ecl );
      cluster.setPosition( { Xcl, Ycl, cluster.position().z() } );

      // update cluster matrix
      LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3> covariance;
      covariance( LHCb::CaloPosition::Index::X, LHCb::CaloPosition::Index::X ) = CovXX;
      covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::X ) = CovXY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::X ) = CovEX;
      covariance( LHCb::CaloPosition::Index::Y, LHCb::CaloPosition::Index::Y ) = CovYY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::Y ) = CovEY;
      covariance( LHCb::CaloPosition::Index::E, LHCb::CaloPosition::Index::E ) = CovEE;
      cluster.setCovariance( covariance );

      return StatusCode::SUCCESS;
    }

  private:
    CovarianceMatrixTool::detail::ParameterMap getParams( Correction::Parameters const& );

    ConditionAccessor<CovarianceMatrixTool::detail::ParameterMap> m_parameters{ this, name() + "_Parameters" };
    Gaudi::Property<std::string>                                  m_conditionName{ this, "ConditionName",
#ifdef USE_DD4HEP
                                                  "/world/DownstreamRegion/Ecal:EcalCovariance"
#else
                                                  "Conditions/Reco/Calo/EcalCovariance"
#endif
    };
    ToolHandle<Correction::Base> m_dbAccessor = { this, "CorrectionBase", "CaloFutureCorrectionBase/DBAccessor" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_negative_energy{ this, "negative energy cluster" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_parExceeded{
        this, "Parameters vector exceeds the number of known parameters" };

    /** calorimeter resolution (A*A*GeV)
     *  @return A*A*GeV resolution parameter
     */
    [[nodiscard]] double a2GeV( CovarianceMatrixTool::detail::ParameterMap const& params,
                                LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::Stochastic );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()] * Gaudi::Units::GeV;
    }

    /** get dispersion  of relative gain error
     *  @return dispersion of relative gain error
     */
    [[nodiscard]] double s2gain( CovarianceMatrixTool::detail::ParameterMap const& params,
                                 LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::GainError );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    /** get  dispersion of noise (both coherent and incoherent
     *  @return overall noise dispersion
     */
    [[nodiscard]] double s2noise( CovarianceMatrixTool::detail::ParameterMap const& params,
                                  LHCb::Detector::Calo::CellID const                id ) const {
      return s2incoherent( params, id ) + s2coherent( params, id );
    }

    /** get the dispersion of incoherent noise
     *  @return dispersion of incoherent noise
     */
    [[nodiscard]] double s2incoherent( CovarianceMatrixTool::detail::ParameterMap const& params,
                                       LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::IncoherentNoise );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    /**  dispersion of coherent  noise
     *  @return dispersion of coherent noise
     */
    [[nodiscard]] double s2coherent( CovarianceMatrixTool::detail::ParameterMap const& params,
                                     LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::CoherentNoise );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2E( CovarianceMatrixTool::detail::ParameterMap const& params,
                              LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::ConstantE );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2X( CovarianceMatrixTool::detail::ParameterMap const& params,
                              LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::ConstantX );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }

    [[nodiscard]] double s2Y( CovarianceMatrixTool::detail::ParameterMap const& params,
                              LHCb::Detector::Calo::CellID const                id ) const {
      const auto& param = params.at( LHCb::Calo::CovarianceMatrixTool::detail::ConstantY );
      if ( id.area() >= param.size() ) return 0.;
      return param[id.area()] * param[id.area()];
    }
  }; ///< end of class FutureClusterCovarianceMatrixTool
} // namespace LHCb::Calo
