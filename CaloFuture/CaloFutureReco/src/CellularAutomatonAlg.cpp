/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "CaloFutureUtils/CellMatrix.h"
#include "CaloFutureUtils/CellNeighbour.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloKernel/CaloVector.h"
#include "CellSelector.h"
#include "DetDesc/IGeometryInfo.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CellID.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/compose.h"
#include "LHCbAlgs/Transformer.h"
#include "boost/container/static_vector.hpp"
#include <iomanip>
#include <optional>
#include <string>
#include <variant>
#include <vector>

/** @class FutureCellularAutomatonAlg FutureCellularAutomatonAlg.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */

namespace LHCb::Calo {
  namespace {
    // ============================================================================
    constexpr auto usedForE =
        CaloDigitStatus::Status{ CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForCovariance };
    constexpr auto usedForP =
        CaloDigitStatus::Status{ CaloDigitStatus::Mask::UseForPosition, CaloDigitStatus::Mask::UseForCovariance };
    constexpr auto seed =
        ( usedForP | usedForE | CaloDigitStatus::Mask::SeedCell | CaloDigitStatus::Mask::LocalMaximum );
    // ============================================================================

    class CelAutoTaggedCell final {
      // ==========================================================================
      enum class Tag : char { DefaultFlag, Clustered, Edge };

      enum class FlagState : char { NotTagged, Tagged };
      // ==========================================================================
    public:
      // ==========================================================================
      // Constructor
      CelAutoTaggedCell() = default;
      CelAutoTaggedCell( Detector::Calo::CellID id, double energy ) : m_id{ id }, m_energy{ energy } {}
      // ==========================================================================
      // Getters
      LHCb::Detector::Calo::CellID cellID() const { return m_id; }
      double                       e() const { return m_energy; }
      bool isEdge() const { return ( FlagState::Tagged == m_status ) && ( Tag::Edge == m_tag ); }
      bool isClustered() const { return ( FlagState::Tagged == m_status ) && ( Tag::Clustered == m_tag ); }
      LHCb::Detector::Calo::CellID seedForClustered() const { return m_seeds[0]; }
      const auto&                  seeds() const { return m_seeds; }
      size_t                       numberSeeds() const { return m_seeds.size(); }
      bool                         isSeed() const { return m_seeds.size() == 1 && cellID() == m_seeds[0]; }
      bool                         isWithSeed( LHCb::Detector::Calo::CellID seed ) const {
        return m_seeds.end() != std::find( m_seeds.begin(), m_seeds.end(), seed );
      }
      Tag       tag() const { return m_tag; }
      FlagState status() const { return m_status; }
      // ==========================================================================
      // Setters
      void setIsSeed() {
        m_tag    = Tag::Clustered;
        m_status = FlagState::Tagged;
        m_seeds.push_back( cellID() );
      }
      // ==========================================================================
      void setEdge() { m_tag = Tag::Edge; }
      void setClustered() { m_tag = Tag::Clustered; }
      void setStatus() {
        if ( ( Tag::Edge == m_tag ) || ( Tag::Clustered == m_tag ) ) { m_status = FlagState::Tagged; }
      }
      void addSeed( const LHCb::Detector::Calo::CellID& seed ) { m_seeds.push_back( seed ); }
      // ==========================================================================
    private:
      // ==========================================================================
      Detector::Calo::CellID m_id;
      double                 m_energy = 0;
      Tag                    m_tag    = Tag::DefaultFlag;
      FlagState              m_status = FlagState::NotTagged;
      // ==========================================================================
      // Ident.seed(s)
      boost::container::static_vector<LHCb::Detector::Calo::CellID, 12> m_seeds;
      // ==========================================================================
    };

    auto isClustered       = []( const CelAutoTaggedCell* c ) { return c->isClustered(); };
    auto isClusteredOrEdge = []( const CelAutoTaggedCell* c ) { return c->isClustered() || c->isEdge(); };

    auto isWithSeed = []( LHCb::Detector::Calo::CellID id ) {
      return [id]( const CelAutoTaggedCell* c ) { return c->isWithSeed( id ); };
    };

    auto setStatus = []( CelAutoTaggedCell* c ) { c->setStatus(); };

    template <typename Digit, typename Hits>
    bool isLocMax( const Digit& digit, const Hits& hits, const DeCalorimeter& det, double et_cut ) {
      double e  = digit.e();
      auto   et = LHCb::CaloDataFunctor::EnergyTransverse{ &det };
      double eT = et( &digit );
      for ( const auto& i : det.neighborCells( digit.cellID() ) ) {
        auto cell = hits[i];
        if ( !cell ) { continue; }
        if ( cell->e() > e ) { return false; }
        eT += et( cell );
      }
      return eT >= et_cut;
    }

    template <typename Digit, typename Hits>
    bool isLocMax( const Digit& digit, const Hits& hits, const DeCalorimeter& det ) {
      const auto& neighbours = det.neighborCells( digit.cellID() );
      return std::none_of( neighbours.begin(), neighbours.end(), [e = digit.e(), &hits]( const auto& i ) {
        auto cell = hits[i];
        return cell && cell->e() > e;
      } );
    }

    // ============================================================================
    /* Application of rules of tagging on one cell
     *   - No action if no clustered neighbor
     *   - Clustered if only one clustered neighbor
     *   - Edge if more then one clustered neighbor
     */
    // ============================================================================

    template <typename Hits>
    void appliRulesTagger( CelAutoTaggedCell* cell, const Hits& hits, const DeCalorimeter* det, bool release ) {

      // Find in the neighbors cells tagged before, the clustered neighbors cells
      const LHCb::Detector::Calo::CellID&        cellID               = cell->cellID();
      const std::vector<Detector::Calo::CellID>& ns                   = det->neighborCells( cellID );
      bool                                       hasEdgeNeighbor      = false;
      bool                                       hasClusteredNeighbor = false;
      for ( const auto& iN : ns ) {
        const CelAutoTaggedCell* nei = hits[iN];
        if ( !nei ) { continue; }
        //
        if ( nei->isEdge() && release ) {
          hasEdgeNeighbor = true;
          for ( const auto& id : nei->seeds() ) {
            if ( !cell->isWithSeed( id ) ) cell->addSeed( id );
          }
        }
        //
        if ( !nei->isClustered() ) { continue; }
        hasClusteredNeighbor                     = true;
        const LHCb::Detector::Calo::CellID& seed = nei->seedForClustered();
        if ( cell->isWithSeed( seed ) ) { continue; }
        cell->addSeed( seed );
      }

      // Tag or not the cell

      switch ( cell->numberSeeds() ) {
      case 0:
        if ( !release ) break;
        if ( hasEdgeNeighbor && !hasClusteredNeighbor ) cell->setEdge(); //
        break;
      case 1:
        cell->setClustered();
        break;
      default:
        cell->setEdge();
        break;
      }
    }
  } // namespace

  class CellularAutomaton
      : public Algorithm::Transformer<LHCb::Event::Calo::Clusters( const EventContext&, const DeCalorimeter&,
                                                                   const LHCb::Event::Calo::Digits& ),
                                      LHCb::Algorithm::Traits::usesConditions<DeCalorimeter>> {

  public:
    /// Standard constructor
    CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode finalize() override; ///< Algorithm finalization

    LHCb::Event::Calo::Clusters operator()( const EventContext& evtCtx, const DeCalorimeter&,
                                            const LHCb::Event::Calo::Digits& ) const override; ///< Algorithm execution

  private:
    Gaudi::Property<unsigned int>           m_neig_level{ this, "Level", 0 };
    Gaudi::Property<bool>                   m_withET{ this, "withET", false };
    Gaudi::Property<double>                 m_ETcut{ this, "ETcut", -10. * Gaudi::Units::GeV };
    Gaudi::Property<CellSelector::Selector> m_usedE{ this, "CellSelectorForEnergy", CellSelector::Selector::s3x3 };
    Gaudi::Property<CellSelector::Selector> m_usedP{ this, "CellSelectorForPosition", CellSelector::Selector::s3x3 };
    Gaudi::Property<unsigned int>           m_passMax{ this, "MaxIteration", 10 };

    mutable Gaudi::Accumulators::StatCounter<> m_clusters{ this, "# clusters" };
    mutable Gaudi::Accumulators::StatCounter<> m_passes{ this, "# clusterization passes" };
    mutable Gaudi::Accumulators::StatCounter<> m_negative{ this, "Negative energy clusters" };

    mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{ this, "Cluster energy" };
    mutable Gaudi::Accumulators::StatCounter<> m_negativeSeed{ this, "Negative seed energy" };
    mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{ this, "Cluster size" };
  };

  // ============================================================================
  // Declaration of the Algorithm Factory
  // ============================================================================
  DECLARE_COMPONENT_WITH_ID( CellularAutomaton, "FutureCellularAutomatonAlg" )

  // ============================================================================
  // Standard constructor, initializes variables
  // ============================================================================
  CellularAutomaton::CellularAutomaton( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "Detector", Utilities::DeCaloFutureLocation( name ) },
                       KeyValue{ "InputData", Utilities::CaloFutureDigitLocation( name ) } },
                     KeyValue{ "OutputData", Utilities::CaloFutureClusterLocation( name, "EcalRaw" ) } ) {}

  // ============================================================================
  //  Finalize
  // ============================================================================
  StatusCode CellularAutomaton::finalize() {
    info() << "Built <" << m_clusters.mean() << "> cellular automaton clusters/event  with <" << m_passes.mean()
           << "> iterations (min,max)=(" << m_passes.min() << "," << m_passes.max() << ") on average " << endmsg;
    return Transformer::finalize(); // must be called after all other actions
  }

  // ============================================================================
  // Main execution
  // ============================================================================
  Event::Calo::Clusters CellularAutomaton::operator()( const EventContext& evtCtx, const DeCalorimeter& detector,
                                                       const LHCb::Event::Calo::Digits& hits ) const ///< Algorithm
                                                                                                     ///< execution
  {
    Event::Calo::Clusters clusters{ Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) };
    clusters.reserve( hits.size() );
    // cmb: this needs to be done every time in order
    // to allow different detectors in the same algorithm
    // --> to be revisited in a future round
    auto addDigit = [useForE = CellSelector{ &detector, m_usedE.value() },
                     useForP = CellSelector{ &detector, m_usedP.value() }]( auto begin, auto end, auto digits,
                                                                            LHCb::Detector::Calo::CellID  seedID,
                                                                            LHCb::CaloDigitStatus::Status init ) {
      // transform!
      std::for_each( begin, end, [&, seedID = seedID, init = init]( const auto* i ) {
        LHCb::Detector::Calo::CellID cellID = i->cellID();
        auto                         status = init;
        if ( useForE( seedID, cellID ) > 0 ) status |= usedForE;
        if ( useForP( seedID, cellID ) > 0 ) status |= usedForP;

        auto entry = digits.emplace_back();
        entry.setCellID( cellID );
        entry.setEnergy( i->e() );
        entry.setFraction( 1.f );
        entry.setStatus( status );
      } );
    };

    bool                                   releaseBool = false;
    bool                                   useData     = false;
    std::set<LHCb::Detector::Calo::CellID> out_cells;

    /*

     level > 0   ->  All local maxima + neighborhood(level)
     level = 0   ->  All data (default)
    */

    // fill with data if level >0
    std::vector<LHCb::Detector::Calo::CellID> cell_list{};
    cell_list.reserve( LHCb::Detector::Calo::Index::max() );
    if ( m_neig_level > 0 ) {
      useData = true;
      for ( const auto& i : hits ) {
        const std::vector<Detector::Calo::CellID>& neighbors = detector.neighborCells( i.cellID() );
        if ( std::none_of( neighbors.begin(), neighbors.end(), [&, e = i.energy()]( const auto& n ) {
               if ( !detector.valid( n ) ) return false;
               auto dig = hits( n );
               return dig && dig->energy() > e;
             } ) ) {
          cell_list.push_back( i.cellID() );
        }
      }
    }

    // if list of "seed" is not empty
    if ( !cell_list.empty() ) {
      out_cells.insert( cell_list.begin(), cell_list.end() );

      /** find all neighbours for the given set of cells for the givel level
       *  @param cells    (UPDATE) list of cells
       *  @param level    (INPUT)  level
       *  @param detector (INPUT) the detector
       *  @return true if neighbours are added
       */
      LHCb::CaloFutureFunctors::neighbours( out_cells, m_neig_level, &detector );
    }

    size_t                         local_size = cell_list.empty() ? hits.size() : out_cells.size();
    std::vector<CelAutoTaggedCell> local_cells{};
    local_cells.reserve( LHCb::Detector::Calo::Index::max() );

    // Create access direct and sequential on the tagged cells
    /// container to tagged  cells with direct (by CellID key)  access
    CaloVector<CelAutoTaggedCell*> taggedCellsDirect{ nullptr };
    taggedCellsDirect.reserve( local_size );
    taggedCellsDirect.setSize( 14000 );

    /// container to tagged  cells with sequential access
    std::vector<CelAutoTaggedCell*> taggedCellsSeq{};
    taggedCellsSeq.reserve( LHCb::Detector::Calo::Index::max() );

    if ( cell_list.empty() ) { // fill with the data
      for ( const auto& digit : hits ) {
        auto& taggedCell = local_cells.emplace_back( digit.cellID(), digit.energy() );
        taggedCellsDirect.addEntry( &taggedCell, digit.cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    } else { // fill for HLT
      for ( const auto& icell : out_cells ) {
        auto digit = hits( icell );
        if ( !digit ) continue;
        auto& taggedCell = local_cells.emplace_back( digit->cellID(), digit->energy() );
        taggedCellsDirect.addEntry( &taggedCell, digit->cellID() );
        taggedCellsSeq.push_back( &taggedCell );
      }
    }

    // Find and mark the seeds (local maxima)
    if ( useData ) {
      for ( const auto& seed : cell_list ) taggedCellsDirect[seed]->setIsSeed();
    } else if ( m_withET ) {
      for ( const auto& i : taggedCellsSeq ) {
        if ( isLocMax( *i, taggedCellsDirect, detector, m_ETcut.value() ) ) i->setIsSeed();
      }
    } else {
      for ( const auto& i : taggedCellsSeq ) {
        if ( isLocMax( *i, taggedCellsDirect, detector ) ) i->setIsSeed();
      }
    }

    /// Tag the cells which are not seeds
    auto itTagLastSeed = std::stable_partition( taggedCellsSeq.begin(), taggedCellsSeq.end(), isClustered );

    auto         itTagLastClustered = itTagLastSeed;
    auto         itTagFirst         = itTagLastClustered;
    unsigned int nPass              = 0;
    while ( itTagLastClustered != taggedCellsSeq.end() ) {

      // Apply rules tagger for all not tagged cells
      std::for_each( itTagLastClustered, taggedCellsSeq.end(),
                     [&]( auto* t ) { appliRulesTagger( t, taggedCellsDirect, &detector, releaseBool ); } );

      // Valid result
      std::for_each( itTagFirst, taggedCellsSeq.end(), setStatus );

      itTagLastClustered = std::stable_partition( itTagFirst, taggedCellsSeq.end(), isClusteredOrEdge );

      // Test if cells are tagged in this pass
      if ( itTagLastClustered == itTagFirst && releaseBool ) {
        const long number = taggedCellsSeq.end() - itTagLastClustered;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " TAGGING NOT FULL - Remain " << number << " not clustered cells" << endmsg;
        itTagLastClustered = taggedCellsSeq.end();
      }
      if ( itTagLastClustered == itTagFirst )
        releaseBool = true; // try additional passes releasing appliRulesTagger criteria
      nPass++;
      itTagFirst = itTagLastClustered;
      if ( m_passMax > 0 && nPass >= m_passMax ) break;
    }

    itTagLastClustered         = std::stable_partition( itTagLastSeed, taggedCellsSeq.end(), isClustered );
    auto itTagClustered1       = itTagLastSeed;
    auto clusterEnergy_counter = m_clusterEnergy.buffer();
    auto clusterSize_counter   = m_clusterSize.buffer();
    int  nNegative             = 0;
    for ( const auto& itTagSeed :
          LHCb::make_span( taggedCellsSeq ).first( std::distance( taggedCellsSeq.begin(), itTagLastSeed ) ) ) {
      if ( itTagSeed->e() <= 0 ) {
        m_negativeSeed += itTagSeed->e();
        continue; // do not make cluster if seed energy <= 0
      }
      LHCb::Detector::Calo::CellID seedID = itTagSeed->cellID();
      // Do partitions first
      auto itTagClustered2 = std::stable_partition( itTagClustered1, itTagLastClustered, isWithSeed( seedID ) );
      auto itTagLastEdge   = std::stable_partition( itTagLastClustered, taggedCellsSeq.end(), isWithSeed( seedID ) );

      auto cluster = clusters.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

      // first Seed, then Owned, and finally  Shared cells...
      auto entry = cluster.entries().emplace_back();
      entry.setCellID( itTagSeed->cellID() );
      entry.setEnergy( itTagSeed->e() );
      entry.setFraction( 1.f );
      entry.setStatus( seed );

      addDigit( itTagClustered1, itTagClustered2, cluster.entries(), seedID, LHCb::CaloDigitStatus::Mask::OwnedCell );
      addDigit( itTagLastClustered, itTagLastEdge, cluster.entries(), seedID, LHCb::CaloDigitStatus::Mask::SharedCell );

      auto exy = LHCb::CaloDataFunctor::calculateClusterEXY( cluster.entries(), &detector );
      if ( exy ) { //  put cluster to the output
        if ( exy->Etot < 0 ) {
          ++nNegative;
          clusters.resize( clusters.size() - 1 );
          continue; // skip negative E clusters
        }

        cluster.setCellID( seedID );
        cluster.setType( Event::Calo::Clusters::Type::CellularAutomaton );
        cluster.setEnergy( exy->Etot );
        cluster.setPosition( { exy->x, exy->y, (float)detector.cellCenter( seedID ).z() } );

        clusterEnergy_counter += cluster.energy();
        clusterSize_counter += cluster.size();
      }
      itTagClustered1 = itTagClustered2;
    }

    // statistics
    if ( nNegative > 0 ) m_negative += nNegative;
    m_passes += nPass;
    m_clusters += clusters.size();

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Built " << clusters.size() << " cellular automaton clusters  with " << nPass << " iterations"
              << endmsg;
      debug() << " ----------------------- Cluster List : " << endmsg;
      for ( auto c : clusters.scalar() ) {
        debug() << " Cluster seed " << c.cellID() << " energy " << c.e() << " #entries " << c.size() << endmsg;
      }
    }
    return clusters;
  }
} // namespace LHCb::Calo
