/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CellMatrix2x2.h"
#include "CellMatrix3x3.h"
#include "CellSwissCross.h"
#include "Detector/Calo/CaloCellID.h"
#include "Gaudi/Algorithm.h"

/** @class CellSelctor CellSelctor.h
 *
 *
 *  @author Victor Egorychev
 *  @date   2008-04-03
 */

namespace LHCb::Calo {
  namespace {
    const std::string s_3x3        = "3x3";
    const std::string s_2x2        = "2x2";
    const std::string s_SwissCross = "SwissCross";
    const std::string s_Neighbour  = "Neighbour";
    const std::string s_invalid    = "INVALID";

    class CellSelector final : public CellMatrix {
      std::variant<std::monostate, CellMatrix3x3, CellMatrix2x2, CellSwissCross, CellNeighbour> m_selector;

    public:
      enum class Selector { s3x3, s2x2, SwissCross, Neighbour };
      friend StatusCode         parse( Selector&, const std::string& );
      friend const std::string& toString( Selector s ) {
        switch ( s ) {
        case Selector::s3x3:
          return s_3x3;
        case Selector::s2x2:
          return s_2x2;
        case Selector::SwissCross:
          return s_SwissCross;
        case Selector::Neighbour:
          return s_Neighbour;
        }
        return s_invalid;
      }
      friend std::ostream& toStream( Selector s, std::ostream& os ) { return os << std::quoted( toString( s ), '\'' ); }

      CellSelector( const DeCalorimeter* det = nullptr, Selector = Selector{ -1 } );

      double operator()( LHCb::Detector::Calo::CellID seed, LHCb::Detector::Calo::CellID cell ) const {
        return std::visit(
            Gaudi::overload( [&]( const auto& s ) { return s( seed, cell ); }, []( std::monostate ) { return 1.; } ),
            m_selector );
      };
    };

    CellSelector::CellSelector( const DeCalorimeter* det, CellSelector::Selector selector ) : CellMatrix( det ) {
      switch ( selector ) {
      case Selector::s3x3:
        m_selector.emplace<CellMatrix3x3>( det );
        break;
      case Selector::s2x2:
        m_selector.emplace<CellMatrix2x2>( det );
        break;
      case Selector::SwissCross:
        m_selector.emplace<CellSwissCross>( det );
        break;
      case Selector::Neighbour:
        m_selector.emplace<CellNeighbour>( det );
        break;
      default:
        m_selector.emplace<std::monostate>();
        break;
      }
    }

    std::string_view unquote( std::string_view in ) {
      if ( !in.empty() && ( in.front() == '\'' || in.front() == '\"' ) && in.front() == in.back() ) {
        in.remove_prefix( 1 );
        in.remove_suffix( 1 );
      }
      return in;
    }

    StatusCode parse( CellSelector::Selector& s, const std::string& str ) {
      constexpr auto values = std::array{ CellSelector::Selector::s3x3, CellSelector::Selector::s2x2,
                                          CellSelector::Selector::SwissCross, CellSelector::Selector::Neighbour };
      auto           i =
          std::find_if( values.begin(), values.end(), [s = unquote( str )]( auto v ) { return toString( v ) == s; } );
      return i != values.end() ? ( s = *i, StatusCode::SUCCESS ) : StatusCode::FAILURE;
    }
  } // namespace
} // namespace LHCb::Calo
