/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloDigitStatus.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloSubClusterTag.h"

namespace LHCb::Calo {
  class SubClusterSelectorBase : public extends<GaudiTool, Interfaces::ISubClusterTag> {

  public:
    /** Standard Tool Constructor
     *  @param type type of the tool (useless ? )
     *  @param name name of the tool
     *  @param parent the tool parent
     */
    using extends::extends;

    // ============================================================================
    /** The main processing method
     *  @see ICaloFutureSubClusterTag
     *  @see ICaloFutureClusterTool
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    // ============================================================================
    void setMask( LHCb::CaloDigitStatus::Status mask ) override {
      m_mask = mask;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "The default status tag is changed to " << m_mask
                << " -> use for Energy   : " << mask.test( LHCb::CaloDigitStatus::Mask::UseForEnergy )
                << " | for Position : " << mask.test( LHCb::CaloDigitStatus::Mask::UseForPosition )
                << " | for Covariance : " << mask.test( LHCb::CaloDigitStatus::Mask::UseForCovariance ) << endmsg;
    }
    LHCb::CaloDigitStatus::Status mask() const override { return m_mask; };

  protected:
    /**  return  flag to modify the fractions
     *   @return flag to modify the fractions
     */
    bool modify() const { return m_modify; }

  private:
    LHCb::CaloDigitStatus::Status m_mask = LHCb::CaloDigitStatus::Status{
        LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForPosition,
        LHCb::CaloDigitStatus::Mask::UseForCovariance };
    Gaudi::Property<bool> m_modify{ this, "ModifyFractions", false };
  };
} // namespace LHCb::Calo
// ============================================================================
