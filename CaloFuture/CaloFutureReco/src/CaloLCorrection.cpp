/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloHypoTool.h"
#include <cmath>
#include <string>

namespace LHCb::Calo {

  /**
   *  @author Deschamps Olivier
   *  @date   2003-03-10
   *  revised 2010

   *  Adam Szabelski
   *  date 2019-10-15
   */
  class LCorrection : public extends<Correction::Base, Interfaces::IProcessHypos> {
  public:
    LCorrection( const std::string& type, const std::string& name, const IInterface* parent );
    using Interfaces::IProcessHypos::process;
    StatusCode process( const DeCalorimeter& calo, Event::Calo::Hypotheses::Type,
                        Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous>&,
                        IGeometryInfo const& geometry ) const override;

  private:
    Gaudi::XYZPoint m_origin = { 0, 0, 0 };

    template <typename Point>
    std::array<float, 4> compute( const DeCalorimeter& calo, const Point& pos, Detector::Calo::CellID cellID,
                                  float energy ) const {
      int area = cellID.area();
      assert( area >= 0 && area <= 2 ); // TODO: comment assert out
      // Account for the tilt
      const auto   plane  = calo.plane( CaloPlane::Front ); // Ecal Front-Plane
      const auto   normal = plane.Normal();
      const double Hesse  = plane.HesseDistance();

      const float xg = pos.x() - m_origin.X();
      const float yg = pos.y() - m_origin.Y();
      const float z0 = ( -Hesse - normal.X() * pos.x() - normal.Y() * pos.y() ) / normal.Z();
      float       zg = z0 - m_origin.Z();

      // hardcoded inner offset (+7.5 mm)
      if ( area == 2 ) { zg += 7.5; }

      // Uncorrected angle
      float xy_offset  = Math::Approx::approx_sqrt( xg * xg + yg * yg );
      float xyz_offset = Math::Approx::approx_sqrt( xg * xg + yg * yg + zg * zg );
      float tan_theta  = xy_offset / zg;

      // get parameters from base class, needed to call getCorrection*
      Correction::Parameters const& baseParams = getParameters();

      // Corrected angle
      float gamma0 = getCorrection( baseParams, Correction::Type::gamma0, cellID ).value_or( 1 );
      float delta0 = getCorrection( baseParams, Correction::Type::delta0, cellID ).value_or( 1 );

      // NB: gammaP(ePrs = 0) != 0, deltaP(ePrs = 0) != 0 and depend on cellID.area()
      // get gammaP and deltaP parameters (depending on cellID.area() for each cluster
      float gammaP = getCorrection( baseParams, Correction::Type::gammaP, cellID, 0. ).value_or( 1 );
      float deltaP = getCorrection( baseParams, Correction::Type::deltaP, cellID, 0. ).value_or( 1 );
      float g      = gamma0 - gammaP;
      float d      = delta0 + deltaP;

      float      tg_fps    = ( energy > 0.0 ? g * Math::fast_log( energy / Gaudi::Units::GeV ) + d : 0.0 );
      float      temp      = ( 1. + tg_fps / xyz_offset );
      float      cos_theta = temp * Math::fast_rsqrt( tan_theta * tan_theta + temp * temp );
      const auto dz_fps    = cos_theta * tg_fps;
      return { g, d, z0, dz_fps };
    }
  };

  DECLARE_COMPONENT_WITH_ID( LCorrection, "CaloFutureLCorrection" )

  LCorrection::LCorrection( const std::string& type, const std::string& name, const IInterface* parent )
      : extends( type, name, parent ) {
    // define conditionName
    const std::string uName( CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "ELECTRON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:ElectronLCorrection"
#else
                   "Conditions/Reco/Calo/ElectronLCorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:SplitPhotonLCorrection"
#else
                   "Conditions/Reco/Calo/SplitPhotonLCorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "PHOTON" ) ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:PhotonLCorrection"
#else
                   "Conditions/Reco/Calo/PhotonLCorrection"
#endif
                   )
          .ignore();
    }
  }

  StatusCode
  LCorrection::process( const DeCalorimeter& calo, Event::Calo::Hypotheses::Type hypo,
                        Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous>& cluster,
                        IGeometryInfo const& ) const {
    // check the Hypo
    const auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo );
    if ( m_hypos.end() == h ) {
      Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS ).ignore();
      return StatusCode::FAILURE;
    }

    // No correction for negative energy :
    if ( cluster.e() < 0. ) { return StatusCode::SUCCESS; }

    auto [g, d, z0, dz_fps] = compute( calo, cluster.position(), cluster.cellID(), cluster.e() );

    // Recompute Z position and fill CaloPosition
    cluster.template field<Event::Calo::v2::ClusterTag::Position>().setZ( z0 + dz_fps );

    return StatusCode::SUCCESS;
  }
} // namespace LHCb::Calo
