/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
/** @file
 *
 *  Implementation file for class: ClassifyPhotonElectron
 *  The implementation is partially based on previous
 *  SinglePhotonAlg and ElectronAlg codes.
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */
// ============================================================================
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloHypos_v2.h"
#include "Event/RelationTable.h"
#include "Gaudi/Accumulators.h"
#include "ICaloHypoTool.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/RelationWeighted2D.h"
#include <string>

/** @class ClassifyPhotonElectron ClassifyPhotonElectron.h
 *
 *  Classification of electromagnetic clusters into photon and
 *  electron hypothesis according to track matching chi2.
 *  Energy and position corrections are applied to the resulting
 *  calo hypotheses.
 *  Implementation partially based on previous SinglePhotonAlg
 *  and ElectronAlg
 *
 *  @author Carla Marin carla.marin@cern.ch
 *  @date   23/05/2019
 */

namespace LHCb::Calo::Algorithm {
  using namespace LHCb::Event::Calo;

  using TrackMatchTable_v2 = LHCb::Calo::TrackUtils::Clusters2BestTrackMatch;
  using PrimaryVertices    = LHCb::Calo::Interfaces::PrimaryVertices;

  namespace {

    bool apply( const DeCalorimeter& calo, LHCb::Event::Calo::Hypotheses::Type hypo,
                ToolHandleArray<Interfaces::IProcessHypos> const& c, Clusters& clusters, IGeometryInfo const& geometry,
                const PrimaryVertices& vertLoc ) {
      return std::all_of( std::begin( c ), std::end( c ), [&]( const auto& elem ) {
        return elem->process( calo, hypo, clusters, nullptr, geometry, vertLoc ).isSuccess();
      } );
    }

    class Counters {
      Gaudi::Accumulators::StatCounter<> m_dX;
      Gaudi::Accumulators::StatCounter<> m_dY;
      Gaudi::Accumulators::StatCounter<> m_dZ;
      Gaudi::Accumulators::StatCounter<> m_dE;
      Gaudi::Accumulators::StatCounter<> m_E;

    public:
      template <typename Component>
      Counters( Component* parent, std::string_view sv )
          : m_dX{ parent, std::string{ sv }.append( " Delta(X)" ) }
          , m_dY{ parent, std::string{ sv }.append( " Delta(Y)" ) }
          , m_dZ{ parent, std::string{ sv }.append( " Delta(Z)" ) }
          , m_dE{ parent, std::string{ sv }.append( " Delta(E)" ) }
          , m_E{ parent, std::string{ sv }.append( " corrected energy" ) } {}

      auto save_initial_values( Hypotheses const& hypos ) {
        std::vector<std::array<double, 4>> Vect_start{};
        Vect_start.reserve( hypos.size() );
        for ( auto hypo : hypos.scalar() ) {
          const auto& pos = hypo.position();
          Vect_start.push_back( { pos.x(), pos.y(), pos.z(), hypo.energy() } );
        }
        return Vect_start;
      }
      void increment( Hypotheses const& hypos, LHCb::span<std::array<double, 4>> initial_values ) {
        auto buf_dX = m_dX.buffer();
        auto buf_dY = m_dY.buffer();
        auto buf_dZ = m_dZ.buffer();
        auto buf_dE = m_dE.buffer();
        auto buf_E  = m_E.buffer();
        int  i      = 0;
        for ( auto hypo : hypos.scalar() ) {
          auto& [X, Y, Z, E] = initial_values[i++];
          const auto& pos    = hypo.position();
          buf_dX += pos.x() - X;
          buf_dY += pos.y() - Y;
          buf_dZ += pos.z() - Z;
          buf_dE += hypo.energy() - E;
          buf_E += hypo.energy();
        }
      }
    };

  } // namespace

  template <typename TrackMatchTable>
  class ClassifyPhotonElectronBase
      : public LHCb::Algorithm::MultiTransformer<
            std::tuple<Hypotheses, Hypotheses>( const EventContext&, DeCalorimeter const&, Clusters const&,
                                                TrackMatchTable const&, DetectorElement const&,
                                                PrimaryVertices const& ),
            LHCb::Algorithm::Traits::usesConditions<DeCalorimeter, DetectorElement>> {
  public:
    using KeyValue = typename ClassifyPhotonElectronBase<TrackMatchTable>::KeyValue;

    // standard constructor
    ClassifyPhotonElectronBase( const std::string& name, ISvcLocator* pSvc )
        : ClassifyPhotonElectronBase<TrackMatchTable>::MultiTransformer(
              name, pSvc,
              { KeyValue( "Detector", CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) ),
                KeyValue( "InputClusters", CaloClusterLocation::Ecal ), KeyValue( "InputTable", "" ),
                KeyValue( "StandardGeometryTop", LHCb::standard_geometry_top ), KeyValue( "PrimaryVertices", "" ) },
              { KeyValue( "OutputPhotons", HypothesesLocation::Photons ),
                KeyValue( "OutputElectrons", HypothesesLocation::Electrons ) } ) {}

    // returns 2 Hypotheses: photons and electrons respectively
    std::tuple<Hypotheses, Hypotheses> operator()( const EventContext& evtCtx, DeCalorimeter const&, Clusters const&,
                                                   TrackMatchTable const&, DetectorElement const&,
                                                   PrimaryVertices const& ) const override;

  private:
    void printDebugInfo( Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> hypo,
                         bool pass ) const;
    void printDebugInfo( Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cluster,
                         CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const;
    bool validateCluster( Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cluster,
                          CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>& eT ) const {
      int m = cluster.entries().size().cast();
      return ( cluster.e() > m_ecut ) && ( eT( &cluster ) > m_eTcut ) && ( m > m_minDigits ) && ( m < m_maxDigits );
    }

    // Correction tools for now used as they were in
    // SinglePhotonAlg and ElectronAlg
    ToolHandleArray<Interfaces::IProcessHypos> m_correc_photon{
        this, "PhotonCorrection", {}, "List of tools for 'fine-corrections'" };
    ToolHandleArray<Interfaces::IProcessHypos> m_correc_electr{
        this, "ElectronCorrection", {}, "List of tools for 'fine-corrections'" };

    // selection cuts
    Gaudi::Property<float>  m_ecut{ this, "MinEnergy", 0., "Threshold on cluster energy" };
    Gaudi::Property<float>  m_eTcut{ this, "MinET", 0., "Threshold on cluster transverse energy" };
    Gaudi::Property<int>    m_minDigits{ this, "MinDigits", 0, "Threshold on minimum cluster digits" };
    Gaudi::Property<int>    m_maxDigits{ this, "MaxDigits", 9999, "Threshold on maximum cluster digits" };
    Gaudi::Property<double> m_photonEtCut{ this, "PhotonMinEt", 0., "Threshold on photon cluster & hypo ET" };
    Gaudi::Property<double> m_electrEtCut{ this, "ElectrMinEt", 0., "Threshold on electron cluster & hypo ET" };
    Gaudi::Property<float>  m_photonChi2Cut{ this, "PhotonMinChi2", 0.,
                                            "Threshold on minimum photon cluster track match chi2" };
    Gaudi::Property<float>  m_electrChi2Cut{ this, "ElectrMaxChi2", 0.,
                                            "Threshold on maximum electron cluster track match chi2" };

    mutable Gaudi::Accumulators::StatCounter<> m_counterPhotons{ this, "photonHypos" };
    mutable Gaudi::Accumulators::StatCounter<> m_counterElectrs{ this, "electronHypos" };
    mutable Gaudi::Accumulators::Counter<>     m_photons_rejected_after_correction{
        this, "Photons pT-rejected after correction" };
    mutable Gaudi::Accumulators::Counter<> m_electrons_rejected_after_correction{
        this, "Electrons pT-rejected after correction" };

    mutable Counters m_photon_counters{ this, "Photon" };
    mutable Counters m_electron_counters{ this, "Electron" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_errApply{
        this, "Error from Correction Tool - skip the cluster", 3 };
  };

  // specific implementations of ClassifyPhotonElectronBase
  struct ClassifyPhotonElectron final : ClassifyPhotonElectronBase<TrackMatchTable_v2> {
    ClassifyPhotonElectron( std::string const& name, ISvcLocator* pSvc )
        : ClassifyPhotonElectronBase<TrackMatchTable_v2>( name, pSvc ) {
      updateHandleLocation( *this, "InputTable", "" );
    }
  };

  DECLARE_COMPONENT_WITH_ID( ClassifyPhotonElectron, "ClassifyPhotonElectronAlg" )

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  template <typename TrackMatchTable>
  std::tuple<Hypotheses, Hypotheses> ClassifyPhotonElectronBase<TrackMatchTable>::operator()(
      const EventContext& evtCtx, DeCalorimeter const& calo, Clusters const& clusters, TrackMatchTable const& table,
      DetectorElement const& lhcb, PrimaryVertices const& vertLoc ) const {
    // output containers
    auto result = std::tuple<Hypotheses, Hypotheses>{
        Hypotheses{ Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) },
        Hypotheses{ Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) } };
    auto& [photons, electrons] = result;
    photons.reserve( clusters.size() );
    photons.clusters().reserve( clusters.size() );
    electrons.reserve( clusters.size() );
    electrons.clusters().reserve( clusters.size() );

    // used in the loop
    auto eT = CaloDataFunctor::EnergyTransverse{ &calo };

    // lookup of cluster-track matches
    // using view, same indexing as clusters
    auto tableview = table.buildView();

    auto hasTrackMatch = [&]( const auto& cluster, float chi2Cut ) {
      auto view = tableview.scalar()[cluster.indices().cast()];
      return view.hasRelation() ? view.relation().template get<LHCb::Calo::TrackUtils::ClusterMatch>().cast() < chi2Cut
                                : false;
    };

    // loop on clusters
    for ( const auto& cl : clusters.scalar() ) {
      if ( this->msgLevel( MSG::DEBUG ) ) printDebugInfo( cl, eT );
      if ( validateCluster( cl, eT ) ) {
        // 1. check if cluster fullfills photon hypo requirements
        if ( ( Calo::Momentum( cl ).pt() >= m_photonEtCut ) && !hasTrackMatch( cl, m_photonChi2Cut ) ) {
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << " --> Cluster satisfies photon e, et, digits, pt and chi2 requirements" << endmsg;
          }
          photons.emplace_back( Hypotheses::Type::Photon, cl.cellID(), { cl } );
        }

        // 2. check if cluster fullfills electron hypo requirements
        if ( ( Calo::Momentum( cl ).pt() >= m_electrEtCut ) && hasTrackMatch( cl, m_electrChi2Cut ) ) {
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << " --> Cluster satisfies electron e, et, digits, pt and chi2 requirements" << endmsg;
          }
          electrons.emplace_back( Hypotheses::Type::EmCharged, cl.cellID(), { cl } );
        }
      }
    }
    auto photons_start_vector   = m_photon_counters.save_initial_values( photons );
    auto electrons_start_vector = m_electron_counters.save_initial_values( electrons );

    bool corr_photons_ok =
        apply( calo, LHCb::Event::Calo::Hypotheses::Type::Photon, m_correc_photon, photons.clusters(),

               *lhcb.geometry(), vertLoc );
    bool corr_electrons_ok = apply( calo, LHCb::Event::Calo::Hypotheses::Type::EmCharged, m_correc_electr,
                                    electrons.clusters(), *lhcb.geometry(), vertLoc );
    if ( !corr_photons_ok || !corr_electrons_ok ) ++m_errApply;

    m_photon_counters.increment( photons, photons_start_vector );
    m_electron_counters.increment( electrons, electrons_start_vector );

    if ( this->msgLevel( MSG::DEBUG ) ) {
      for ( const auto& hypo : photons.scalar() ) {
        printDebugInfo( hypo, Calo::Momentum( hypo.clusters() ).pt() >= m_photonEtCut );
      }
      for ( const auto& hypo : electrons.scalar() ) {
        printDebugInfo( hypo, Calo::Momentum( hypo.clusters() ).pt() >= m_electrEtCut );
      }
    }

    int photons_size   = photons.size();
    int electrons_size = electrons.size();

    LHCb::Event::make_zip<SIMDWrapper::InstructionSet::Scalar>( photons, photons.clusters() )
        .filterInPlace( [&]( const auto& zip_ph ) {
          auto ph = zip_ph.template as<Hypotheses>();
          return Calo::Momentum( ph.clusters() ).pt() >= m_photonEtCut;
        } );
    for ( auto hypo : photons.scalar() ) { // reindex cluster reference
      hypo.clusters().set( 0, hypo.indices() );
    }
    LHCb::Event::make_zip<SIMDWrapper::InstructionSet::Scalar>( electrons, electrons.clusters() )
        .filterInPlace( [&]( const auto& zip_el ) {
          auto el = zip_el.template as<Hypotheses>();
          return Calo::Momentum( el.clusters() ).pt() >= m_electrEtCut;
        } );
    for ( auto hypo : electrons.scalar() ) { // reindex cluster reference
      hypo.clusters().set( 0, hypo.indices() );
    }

    m_photons_rejected_after_correction += photons_size - photons.size();
    m_electrons_rejected_after_correction += electrons_size - electrons.size();

    // debug info
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << " # of created Photon Hypos is " << photons.size() << endmsg;
      this->debug() << " # of created Electron Hypos is " << electrons.size() << endmsg;
    }

    // counters
    m_counterPhotons += photons.size();
    m_counterElectrs += electrons.size();

    return result;
  }

  template <typename TrackMatchTable>
  void ClassifyPhotonElectronBase<TrackMatchTable>::printDebugInfo(
      Clusters::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> cluster,
      CaloDataFunctor::EnergyTransverse<const DeCalorimeter*>&                             eT ) const {
    this->debug() << "*Variables and cut values:" << endmsg;
    this->debug() << " - e: " << cluster.e() << " " << m_ecut << endmsg;
    this->debug() << " - eT:" << eT( &cluster ) << " " << m_eTcut << endmsg;
    this->debug() << " - pt:" << Calo::Momentum( cluster ).pt() << " " << m_photonEtCut << endmsg;
    this->debug() << " - m: " << cluster.entries().size() << " " << m_minDigits << " " << m_maxDigits << endmsg;
    this->debug() << " - chi2 cut: " << m_photonChi2Cut << endmsg;
  }

  template <typename TrackMatchTable>
  void ClassifyPhotonElectronBase<TrackMatchTable>::printDebugInfo(
      Hypotheses::const_reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> hypo, bool pass ) const {
    this->debug() << " - pt hypo: " << Calo::Momentum( hypo.clusters() ).pt() << endmsg;
    this->debug() << " - pt cut: " << m_eTcut << endmsg;
    if ( !pass ) this->debug() << "DOES NOT PASS!" << endmsg;
  }

} // namespace LHCb::Calo::Algorithm
