/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloCorrectionBase.h"
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "Core/FloatComparison.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "ICaloHypoTool.h"
#include "Kernel/CaloCellIDParser.h"
#include <GaudiKernel/GaudiException.h>
#include <cmath>
#include <map>
#include <string>

/** @file
 *  Implementation file for class : CaloFutureECorrection
 *
 *  @date 2003-03-10
 *  @author Deschamps Olivier
 */

namespace {
  int shiftAs( unsigned int cellIDColOrRow, LHCb::span<const int, 3> shift, unsigned int area ) {
    assert( area < 3 );
    unsigned int colOrRow = cellIDColOrRow - shift[area] + 1;
    // leakage induced by Ecal module frame
    switch ( colOrRow % ( area + 1 ) ) {
    case 1:
      return +1;
    case 0:
      return -1;
    default:
      return 0;
    }
  }
} // namespace

class PileupMap {
  std::array<float, 12000> m_constants;

public:
  PileupMap( const std::string& json_string ) {
    m_constants.fill( std::numeric_limits<float>::quiet_NaN() );
    nlohmann::json pileup_mapJSON = nlohmann::json::parse( json_string );
    for ( auto const& [key, value] : pileup_mapJSON.items() ) {
      using Gaudi::Parsers::parse;
      LHCb::Detector::Calo::CellID id{};
      parse( id, key ).orThrow( "Unknown cell ID in the pile-up correction file" );
      assert( isValid( id ) );
      m_constants[id.index()] = value;
    }
  }

  float at( LHCb::Detector::Calo::CellID id ) const {
    int cellID = id.index();
    if ( std::isnan( m_constants[cellID] ) ) {
      throw std::runtime_error{ "Requested pileup correction for cellID that was not specified in the file" };
    }
    return m_constants[cellID];
  }
};

namespace LHCb::Calo {
  using PrimaryVertices = LHCb::Calo::Interfaces::PrimaryVertices;

  /**
   *  @author Deschamps Olivier
   *
   *  @date   2003-03-10
   */
  class ECorrection : public extends<Correction::Base, Interfaces::IProcessHypos> {

  public:
    ECorrection( const std::string& type, const std::string& name, const IInterface* parent );
    StatusCode initialize() override;
    StatusCode process( const DeCalorimeter&, Event::Calo::Hypotheses::Type,
                        Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous>&,
                        CaloFuture2Track::ICluster2TrackTable2D const* ctable, IGeometryInfo const& geometry,
                        PrimaryVertices const& vertLoc ) const override;
    StatusCode process( const DeCalorimeter&, Event::Calo::Hypotheses::Type,
                        Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous>&,
                        IGeometryInfo const& ) const override {
      throw GaudiException(
          "process( Event::Calo::Hypotheses::Type, Event::Calo::Clusters::Range, IGeometryInfo const&, "
          "const EventContext& ) not implemented",
          name(), StatusCode::FAILURE );
    }

  private:
    // FIXME One should not use ConditionAccessors by hand in a Tool
    // The Conditions should be declared at the algorithm level
    // and be used transparently via the functional framework
    DetDesc::ConditionAccessor<DeCalorimeter>       m_calo{ this, "DeCalo", DeCalorimeterLocation::Ecal };
    DetDesc::ConditionAccessor<Detector::Calo::Map> m_offsets{ this, "CaloMaps", "EcalMaps" };
    ToolHandle<Interfaces::IElectron>               m_caloElectron{ this, "ElectronTool", "CaloFutureElectron" };

    struct ECorrInputParams {
      Detector::Calo::CellID cellID;
      Gaudi::XYZPoint        seedPos;
      float                  x      = 0;
      float                  y      = 0;
      float                  z      = 0;
      float                  eEcal  = 0;
      float                  dtheta = 0;
      unsigned int           area   = 0;
    };

    struct ECorrOutputParams {
      float eCor = 0;
      // output Jacobian elements returned from calcECorrection() to process()
      float dEcor_dXcl = 0;
      float dEcor_dYcl = 0;
      float dEcor_dEcl = 0;

      // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
      float alpha = 0;
      float Asx   = 0;
      float Asy   = 0;
      float aG    = 0;
      float aE    = 0;
      float aB    = 0;
      float aX    = 0;
      float aY    = 0;
      float gT    = 0;
    };

    ECorrOutputParams calcECorrection( Correction::Parameters const& baseParams, const DeCalorimeter& calo,
                                       const ECorrInputParams& params ) const;

    /// debugging necessary in case if any new corrections are added or their sequence is changed!
    void debugDerivativesCalculation( Correction::Parameters const& baseParams, DeCalorimeter const& calo,
                                      ECorrInputParams const& inParams, ECorrOutputParams const& outParams ) const;

  private:
    /**
     * Update the covariance matrix of the calo hypothesis
     * @param dEcor_dXcl jacobian element calculated for x
     * @param dEcor_dycl jacobian element calculated for y
     * @param dEcor_decl jacobian element calculated for energy
     * @param hypo hypothesis to be modified
     **/
    void
    updateCovariance( float dEcor_dXcl, float dEcor_dYcl, float dEcor_dEcl,
                      Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous> ) const;
    /**
     * Update the position of the calo hypothesis
     * @param Ecor corrected energy to be applied
     * @param hypo hypothesis to be modified
     **/
    bool  isNotSeed( const CaloDigit* seed ) const;
    float computeDTheta( const DeCalorimeter&                                                                  calo,
                         Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous> cluster,
                         CaloFuture2Track::ICluster2TrackTable2D const& ctable, IGeometryInfo const& geometry ) const;

    mutable IncCounter m_counterSkippedNegativeEnergyCorrection{ this, "Skip negative energy correction" };

    mutable SCounter m_counterPileupOffset{ this, "Pileup offset" };
    mutable SCounter m_counterPileupSubstractedRatio{ this, "Pileup subtracted ratio" };
    mutable SCounter m_counterPileupScale{ this, "Pileup scale" };

    mutable IncCounter m_counterUnphysical{ this, "Unphysical d(Ehypo)/d(Ecluster)" };

    mutable IncCounter m_counterUnphysicalVariance{ this, "Unphysical variance(Ehypo)" };

    mutable std::array<SCounter, k_numOfCaloAreas> m_countersAlpha = make_counters( this, "<alpha> " );

    ServiceHandle<IFileAccess> m_filesvc{ this, "FileAccess", "ParamFileSvc" };
    std::optional<PileupMap>   m_pileupMap;
    void                       read_pileup_map() {
      auto s = m_filesvc->read( m_pileupMapName.value() );
      if ( !s ) throw std::runtime_error( "No PileUpCorrections found at " + m_pileupMapName.value() );
      m_pileupMap.emplace( *s );
    }

    Gaudi::Property<std::string> m_pileupMapName{ this, "PileupMapFileName", "paramfile://data/PileUpOffset.json",
                                                  [this]( auto& ) {
                                                    if ( m_pileupMap ) read_pileup_map();
                                                  } };
  };

  DECLARE_COMPONENT_WITH_ID( ECorrection, "CaloFutureECorrection" )

  namespace {

    Detector::Calo::Map createMap( const DeCalorimeter& caloDet ) {
      Detector::Calo::Map map;
      // fill the maps from the CaloFuture DetElem
      for ( const auto& c : caloDet.cellParams() ) {
        const auto id = c.cellID();
        if ( !caloDet.valid( id ) || id.isPin() ) continue;
        map[id] = c.pileUpOffset();
      }
      return map;
    }

    float computeOffset( DeCalorimeter const& calo, PileupMap const& pileupMap, Detector::Calo::CellID id, int nPV ) {
      if ( id.calo() != calo.index() ) { throw std::invalid_argument( "Wrong Calorimeter for this instance" ); }
      float pileupParam = pileupMap.at( id );
      return pileupParam * nPV;
    }
  } // namespace

  ECorrection::ECorrection( const std::string& type, const std::string& name, const IInterface* parent )
      : extends( type, name, parent ) {

    // define conditionName
    const std::string uName( CaloFutureAlgUtils::toUpper( name ) );
    if ( uName.find( "ELECTRON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:ElectronECorrection"
#else
                   "Conditions/Reco/Calo/ElectronECorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "MERGED" ) != std::string::npos || uName.find( "SPLITPHOTON" ) != std::string::npos ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:SplitPhotonECorrection"
#else
                   "Conditions/Reco/Calo/SplitPhotonECorrection"
#endif
                   )
          .ignore();
    } else if ( uName.find( "PHOTON" ) ) {
      setProperty( "ConditionName",
#ifdef USE_DD4HEP
                   "/world/DownstreamRegion/Ecal:PhotonECorrection"
#else
                   "Conditions/Reco/Calo/PhotonECorrection"
#endif
                   )
          .ignore();
    }
  }

  StatusCode ECorrection::initialize() {
    return extends::initialize().andThen( [&] {
      this->addSharedConditionDerivation( { m_calo.key() }, m_offsets.key(), &createMap );
      read_pileup_map();
    } );
  }

  StatusCode
  ECorrection::process( const DeCalorimeter& calo, Event::Calo::Hypotheses::Type hypo,
                        Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous>& cluster,
                        CaloFuture2Track::ICluster2TrackTable2D const* ctable, IGeometryInfo const& geometry,
                        PrimaryVertices const& vertLoc ) const {

    auto h = std::find( m_hypos.begin(), m_hypos.end(), hypo );
    if ( m_hypos.end() == h ) { return Error( "Invalid hypothesis -> no correction applied", StatusCode::SUCCESS ); }

    auto counterSkippedNegativeEnergyCorrection = m_counterSkippedNegativeEnergyCorrection.buffer();
    auto counterPileupOffset                    = m_counterPileupOffset.buffer();
    auto counterPileupSubstractedRatio          = m_counterPileupSubstractedRatio.buffer();
    auto counterUnphysical                      = m_counterUnphysical.buffer();

    int pileup_cache = vertLoc.size();
    m_counterPileupScale += pileup_cache;

    if ( cluster.e() < 0. ) {
      ++counterSkippedNegativeEnergyCorrection;
      return StatusCode::SUCCESS;
    }

    // Get position
    const auto& position = cluster.position();
    float       eEcal    = cluster.e();
    const float xBar     = position.x();
    const float yBar     = position.y();

    // Cell ID for seed digit
    Detector::Calo::CellID cellID  = cluster.cellID();
    Gaudi::XYZPoint        seedPos = calo.cellCenter( cellID );

    float dtheta = ( ( Event::Calo::Hypotheses::Type::EmCharged == hypo && ctable )
                         ? computeDTheta( calo, cluster, *ctable, geometry )
                         : 0. );

    // Pileup subtraction at the digit level
    float eEcalPUcorr = 0;
    float offsettot   = 0;
    for ( auto entry : cluster.entries() ) { /// Loop over calocluster::entries
      float offset = computeOffset( calo, *m_pileupMap, entry.cellID(), pileup_cache );
      if ( entry.status().test( CaloDigitStatus::Mask::UseForEnergy ) ) {
        float fraction = entry.fraction();
        eEcalPUcorr += ( entry.energy() - offset ) * fraction;
        offsettot += offset * fraction;
      }
    }
    if ( eEcalPUcorr < 0. ) {
      ++counterSkippedNegativeEnergyCorrection;
      return StatusCode::SUCCESS;
    }
    counterPileupOffset += offsettot;
    counterPileupSubstractedRatio += eEcalPUcorr / eEcal;
    eEcal = eEcalPUcorr;

    /** here all information is available
     *
     *  (1) Ecal energy in 3x3     :    eEcal
     *  ( ) Prs and Spd energies   :    ePrs, eSpd ( not used )
     *  (3) weighted barycenter    :    xBar, yBar
     *  (4) Zone/Area in Ecal      :    area
     *  (5) SEED digit             :    seed    (NO for split!)
     *  (6) CellID of seed digit   :    cellID  (OK for split!)
     *  (7) Position of seed cell  :    seedPos (OK for split!)
     *
     */
    ECorrInputParams params{ cellID, seedPos, xBar, yBar, (float)position.z(), eEcal, dtheta, cellID.area() };

    // get parameters from base class, needed to call getCorrection*
    Correction::Parameters const& baseParams = getParameters();

    /* Calculate corrected energy in a separate function call. Necessary for debugging the Jacobian by calculating
     * numeric derivatives w.r.t. (X, Y, E) in case of any changes in the correction code.
     *
     * Input positions and energies are passed as parameters for ease of numeric derivative calculation,
     * all the other paramers and results of intermediate calculations are shared between the two methods
     * using local ECorrInputParams _params, and [zero-initialized] ECorrOutputParams _results.
     */
    ECorrOutputParams results = calcECorrection( baseParams, calo, params );
    float             eCor    = results.eCor;

    // results of semi-analytic derivative calculation
    const float& dEcor_dXcl = results.dEcor_dXcl;
    const float& dEcor_dYcl = results.dEcor_dYcl;
    float&       dEcor_dEcl = results.dEcor_dEcl;

    // protection against unphysical d(Ehypo)/d(Ecluster) == 0
    if ( fabs( dEcor_dEcl ) < 1e-10 ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "unphysical d(Ehypo)/d(Ecluster) = " << dEcor_dEcl << " reset to 1 as if Ehypo = Ecluster" << endmsg;
      ++counterUnphysical;
      dEcor_dEcl = 1.;
    }

    // debugging necessary in case if any new corrections are added or their sequence is changed!
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( m_correctCovariance ) debugDerivativesCalculation( baseParams, calo, params, results );
    }

    m_countersAlpha.at( cellID.area() ) += results.alpha;

    // update position
    cluster.setEnergy( eCor );

    // ----------------------------------------- apply semi-analytic cov.m. propagation due to the (X,Y,E) corrections
    if ( m_correctCovariance ) { updateCovariance( dEcor_dXcl, dEcor_dYcl, dEcor_dEcl, cluster ); }

    return StatusCode::SUCCESS;
  }

  void ECorrection::updateCovariance(
      float dEcor_dXcl, float dEcor_dYcl, float dEcor_dEcl,
      Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous> cluster ) const {

    auto                                            counterUnphysicalVariance = m_counterUnphysicalVariance.buffer();
    CaloPosition::Covariance                        covariance                = cluster.covariance();
    LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3> covarianceOut;

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "before E-corr. cov.m. = \n" << covariance << endmsg; }

    // index numbering just follows ROOT::Math::SMatrix<double,3,3>::Array() for row/column indices (X:0, Y:1, E:2)
    float c0[6], c1[6];
    /*
     * Indexing following ROOT::Math::SMatrix<double,3,3,ROOT::Math::MatRepSym<double,3> >::Array() :
     *
     * The iterators access the matrix element in the order how they are
     * stored in memory. The C (row-major) convention is used, and in the
     * case of symmetric matrices the iterator spans only the lower diagonal
     * block. For example for a symmetric 3x3 matrices the order of the 6
     * elements \f${a_0,...a_5}\f$ is:
     * \f[
     * M = \left( \begin{array}{ccc}
     *     a_0 & a_1 & a_3  \\
     *     a_1 & a_2  & a_4  \\
     *     a_3 & a_4 & a_5   \end{array} \right)
     * \f]
     */
    c0[0] = covariance( CaloPosition::Index::X,
                        CaloPosition::Index::X ); // arr[0] not relying on CaloFuturePosition::Index::X == 0
    c0[1] = covariance( CaloPosition::Index::X, CaloPosition::Index::Y ); // arr[1]
    c0[2] = covariance( CaloPosition::Index::Y,
                        CaloPosition::Index::Y ); // arr[2] not relying on CaloFuturePosition::Index::Y == 1
    c0[3] = covariance( CaloPosition::Index::X, CaloPosition::Index::E ); // arr[3]
    c0[4] = covariance( CaloPosition::Index::Y, CaloPosition::Index::E ); // arr[4]
    c0[5] = covariance( CaloPosition::Index::E,
                        CaloPosition::Index::E ); // arr[5] not relying on CaloFuturePosition::Index::E == 2

    // cov1 = (J * cov0 * J^T) for the special case of Jacobian for (X,Y,E) -> (X1=X, Y1=Y, E1=E(X,Y,E))
    c1[0]     = c0[0];
    c1[1]     = c0[1];
    c1[2]     = c0[2];
    c1[3]     = c0[0] * dEcor_dXcl + c0[1] * dEcor_dYcl + c0[3] * dEcor_dEcl;
    c1[4]     = c0[1] * dEcor_dXcl + c0[2] * dEcor_dYcl + c0[4] * dEcor_dEcl;
    float tmp = c0[3] * dEcor_dXcl + c0[4] * dEcor_dYcl + c0[5] * dEcor_dEcl;
    c1[5]     = c1[3] * dEcor_dXcl + c1[4] * dEcor_dYcl + tmp * dEcor_dEcl;

    // additional protection against cov.m.(E,E) <= 0 due to numerical effects
    if ( c1[5] < 1.e-10 ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "unphysical variance(Ehypo) = " << c1[5]
                << " reset cov.m.(Ehypo,*) = cov.m.(Ecluster,*) as if Ehypo = Ecluster" << endmsg;
      ++counterUnphysicalVariance;
      c1[5] = c0[5];
      c1[3] = c0[3];
      c1[4] = c0[4];
    }

    // finally update CaloHypo::position()->covariance()
    covarianceOut( CaloPosition::Index::X, CaloPosition::Index::X ) = c1[0]; // cov1(0,0);
    covarianceOut( CaloPosition::Index::Y, CaloPosition::Index::Y ) = c1[2]; // cov1(1,1);
    covarianceOut( CaloPosition::Index::E, CaloPosition::Index::E ) = c1[5]; // cov1(2,2);
    covarianceOut( CaloPosition::Index::X, CaloPosition::Index::Y ) = c1[1]; // cov1(0,1);
    covarianceOut( CaloPosition::Index::X, CaloPosition::Index::E ) = c1[3]; // cov1(0,2);
    covarianceOut( CaloPosition::Index::Y, CaloPosition::Index::E ) = c1[4]; // cov1(1,2);
    cluster.setCovariance( covarianceOut );

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "after E-corr. cov.m. = \n" << covariance << endmsg; }
  }

  ECorrection::ECorrOutputParams ECorrection::calcECorrection( Correction::Parameters const& baseParams,
                                                               DeCalorimeter const&          calo,
                                                               ECorrInputParams const&       _params ) const {
    // local aliases for the input variables passed from process() to calcECorrection()
    const Detector::Calo::CellID& cellID  = _params.cellID;
    const Gaudi::XYZPoint&        seedPos = _params.seedPos;
    const float&                  dtheta  = _params.dtheta;
    const unsigned int&           area    = _params.area;
    float                         xBar    = _params.x;
    float                         yBar    = _params.y;
    float                         eEcal   = _params.eEcal;

    float CellSize = calo.cellSize( cellID );
    float Asx      = ( xBar - seedPos.x() ) / CellSize; // Asx0
    float Asy      = ( yBar - seedPos.y() ) / CellSize; // Asy0

    const float Asx0 = Asx;
    const float Asy0 = Asy;

    float bDist = Math::Approx::approx_sqrt( 2.f * ( Asx * Asx + Asy * Asy ) );

    int signX = shiftAs( cellID.col(), std::array{ 0, 0, 8 }, area );
    int signY = shiftAs( cellID.row(), std::array{ 6, 12, 14 }, area );
    Asx *= signX; // Asx1
    Asy *= signY; // Asy1

    // analytic derivatives of the correction functions
    float DaE( 0 ), DaB( 0 ), DaX( 0 ), DaY( 0 );

    //
    // apply corrections
    // NB: numeric derivative calculation calls and printouts which are commented-out below
    // are useful for debugging in case of changes in the correction function code
    //
    //// aG = const(X,Y,E), no need to calculate derivatives
    float aG = getCorrection( baseParams, Correction::Type::alphaG, cellID ).value_or( 1. ); // global Ecal factor
    //// aE = alphaE(eEcal)
    const auto aECorDer = getCorrectionAndDerivative( baseParams, Correction::Type::alphaE, cellID, eEcal )
                              .value_or( Correction::Result{ 1., 0. } ); // longitudinal leakage
    const auto aE       = aECorDer.value;
    const auto aBCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::alphaB, cellID, bDist )
                              .value_or( Correction::Result{ 1., 0. } ); // lateral leakage
    const auto aB = aBCorDer.value;
    //// aX = alphaX(Asx1)
    const auto aXCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::alphaX, cellID, Asx )
                              .value_or( Correction::Result{ 1., 0. } ); // module frame dead material X-direction
    const auto aX = aXCorDer.value;
    //// aY = alphaY(Asy1)
    const auto aYCorDer = getCorrectionAndDerivative( baseParams, Correction::Type::alphaY, cellID, Asy )
                              .value_or( Correction::Result{ 1., 0. } ); // module frame dead material Y-direction
    const auto aY = aYCorDer.value;
    if ( m_correctCovariance ) {
      DaE = aECorDer.derivative;
      DaB = aBCorDer.derivative;
      DaX = aXCorDer.derivative;
      DaY = aYCorDer.derivative;
    }

    // angular correction
    // assume dtheta to be independent of X,Y,E, although it may still be implicitly a bit dependent on X,Y
    float gT = getCorrection( baseParams, Correction::Type::globalT, cellID, dtheta ).value_or( 1. ); // incidence angle
                                                                                                      // (delta)
    float dT = getCorrection( baseParams, Correction::Type::offsetT, cellID, dtheta ).value_or( 0. ); // incidence angle
                                                                                                      // (delta)

    // Energy offset
    float sinT   = calo.cellSine( cellID );
    float offset = getCorrection( baseParams, Correction::Type::offset, cellID, sinT ).value_or( 0. );

    // Apply Ecal leakage corrections
    float alpha = aG * aE * aB * aX * aY;
    float eCor  = eEcal * alpha * gT + dT + offset;

    /* DG,20190421: derivative calculation simplified by removal of SPD and PRS
     *
     * Asx0  = (Xcluster-seedPos.x)/CellSize
     * bDist = sqrt(2)*sqrt(Asx0**2+Asy0**2)
     * signX = signX(cellID); // const(X,Y,Ecluster)
     * Asx1  = signX*Asx0
     * eEcal = Ecluster - pileup_offset(cellID, eSpd = 0); // => d(eEcal)/d(Ecluster) = 1
     * aG    = alphaG(cellID); // const(X,Y, Ecluster)
     * aE    = alphaE(eEcal)
     * aB    = alphaB(bDist)
     * aX    = alphaX(Asx1)
     * aY    = alphaY(Asy1)
     * gT    = globalT(dtheta); // const(X,Y,Ecluster) although dtheta may indirectly depend on (X,Y)
     * dT    = offsetT(dtheta); // const(X,Y,Ecluster)
     * sinT  = cellSince(cellID); // const(X,Y,Ecluster)
     * offset= offset(cellID, sinT); // const(X,Y,Ecluster) at eSpd = 0
     * gC    = 1; // at eSpd = 0
     *
     * d(Asx0)/dX       = +1/CellSize
     * d(Asx1)/d(Asx0)  = signX
     * d(bDist)/d(Asx0) = sqrt(2)*2*Asx0/2/sqrt(Asx0**2+Asy0**2) = 2*Asx0/bDist; // if bDist != 0, otherwise 0
     *   if bDist=0 <=> (Asx=0,Asy=0), but for any Asy!=0 (if Asx=0 => d(bDist)/d(Asx) = 0)
     *   => for continuity similarly define the same for Asy=0, i.e. if bDist=0 => d(bDist)/d(Asx) = 0
     *
     * d(aB)/dX             = d(aB)/d(bDist)*d(bDist)/d(Asx0)*d(Asx0)/dX = DalphpaB*(2*Asx0/bDist)*(1/CellSize)
     * d(aX)/dX             = d(aX)/d(Asx1)*d(Asx1)/d(Asx0)*d(Asx0)/dX = DalphaX*signX*(1/CellSize)
     * d(eEcal)/d(Ecluster) = 1
     *
     * alpha = aG * aE(eEcal) * aB(bDist) * aX(Asx1) * aY(Asy1);
     * Ehypo = eCor = eEcal * alpha(eEcal, bDist, Asx1, Asy1) * (gC = 1) * gT + dT + offset;
     *
     * d(alpha)/d(eEcal) = (aG*aB*aX*aY) * d(aE)/d(eEcal) = (alpha/aE) * DalphaE; // if aE!=0, otherwise
     * aG*aB*aX*aY*DalphaE
     *
     *
     * d(Ehypo)/d(Ecluster) = gT*(eEcal*d(alpha)/d(eEcal) + alpha) = gT * alpha * (1. + DaE / aE * eEcal)
     * d(Ehypo)/d(Xcluster) = gT*eEcal*d(alpha)/dX = gT*eEcal*aG*aE*aY*(d(aB)/dX*aX+d(aX)/dX*aB)
     *                      = gT * eEcal * aG * aE * aY * (DaB*2.*Asx0/bDist*aX + signX*aB*DaX)/CellSize
     * d(Ehypo)/d(Ycluster) = [ same as for d(Ehypo)/d(Xcluster) with ( X <-> Y ) ]
     * 			  = gT * eEcal * aG * aE * aX * (DaB*2.*Asy0/bDist*aY + signY*aB*DaY)/CellSize
     */

    ECorrection::ECorrOutputParams results;
    if ( m_correctCovariance ) {
      float d_alpha_dE = ( !LHCb::essentiallyZero( aE ) )
                             ? DaE * alpha / aE
                             : DaE * aG * aB * aX * aY; // though in principle, aE should never be 0

      results.dEcor_dEcl = gT * ( alpha + d_alpha_dE * eEcal );

      results.dEcor_dXcl =
          gT * eEcal * aG * aE * aY *
          ( ( LHCb::essentiallyZero( bDist ) ? 0. : DaB * 2. * Asx0 / bDist * aX ) + signX * aB * DaX ) / CellSize;

      results.dEcor_dYcl =
          gT * eEcal * aG * aE * aX *
          ( ( LHCb::essentiallyZero( bDist ) ? 0. : DaB * 2. * Asy0 / bDist * aY ) + signY * aB * DaY ) / CellSize;
    }

    results.alpha = alpha;

    // intermediate variables calculated by calcECorrection() needed for debug printout inside process()
    if ( msgLevel( MSG::DEBUG ) ) {
      results.Asx = Asx; // Asx1
      results.Asy = Asy; // Asy1
      results.aG  = aG;
      results.aE  = aE;
      results.aB  = aB;
      results.aX  = aX;
      results.aY  = aY;
      results.gT  = gT;
    }
    results.eCor = eCor;
    return results;
  }

  void ECorrection::debugDerivativesCalculation( Correction::Parameters const& baseParams, DeCalorimeter const& calo,
                                                 ECorrInputParams const&  inParams,
                                                 ECorrOutputParams const& outParams ) const {
    const float dx_rel( 1e-5 ), dy_rel( 1e-5 ), de_rel( 1e-3 ); // dx,dy ~ few*0.1*mm, de ~ few MeV
    float       xBar       = inParams.x;
    float       yBar       = inParams.y;
    float       eEcal      = inParams.eEcal;
    float       eCor       = outParams.eCor;
    float       dEcor_dXcl = outParams.dEcor_dXcl;
    float       dEcor_dYcl = outParams.dEcor_dYcl;
    float       dEcor_dEcl = outParams.dEcor_dEcl;

    debug() << "\n ------------------------ ECorrection(x+dx, y, e) calculation follows ------------------- " << endmsg;
    ECorrInputParams inParams1( inParams );
    inParams1.x      = inParams1.x * ( 1 + dx_rel );
    auto  outParams1 = calcECorrection( baseParams, calo, inParams1 );
    float eCor_x     = outParams1.eCor;

    debug() << "\n ------------------------ ECorrection(x, y+dy, e) calculation follows ------------------- " << endmsg;
    ECorrInputParams inParams2( inParams );
    inParams2.y      = inParams2.y * ( 1 + dy_rel );
    auto  outParams2 = calcECorrection( baseParams, calo, inParams2 );
    float eCor_y     = outParams2.eCor;

    debug() << "\n ------------------------ ECorrection(e, y, e+de) calculation follows ------------------- " << endmsg;
    ECorrInputParams inParams3( inParams );
    inParams3.eEcal  = inParams2.eEcal * ( 1 + de_rel );
    auto  outParams3 = calcECorrection( baseParams, calo, inParams3 );
    float eCor_e     = outParams3.eCor;

    float dn_eCor_dx = ( eCor_x - eCor ) / xBar / dx_rel;
    float dn_eCor_dy = ( eCor_y - eCor ) / yBar / dy_rel;
    float dn_eCor_de = ( eCor_e - eCor ) / eEcal / de_rel;

    // avoid division in comparison for possible dE/dX == 0 or dE/dY == 0
    if ( fabs( dEcor_dXcl - dn_eCor_dx ) > fabs( dEcor_dXcl ) * 0.1 ||
         fabs( dEcor_dYcl - dn_eCor_dy ) > fabs( dEcor_dYcl ) * 0.1 ||
         fabs( dEcor_dEcl - dn_eCor_de ) > fabs( dEcor_dEcl ) * 0.1 ) {
      debug() << " some CaloFutureECorrection analytically-calculated Jacobian elements differ (by > 10%) from "
                 "numerically-calculated ones! "
              << endmsg;
    }

    debug() << "********** Jacobian elements J(2,*) =" << endmsg;
    debug() << "   semi-analytic dEcor_dXcl = " << dEcor_dXcl << " numeric dn_eCor_dx = " << dn_eCor_dx << endmsg;
    debug() << "   semi-analytic dEcor_dYcl = " << dEcor_dYcl << " numeric dn_eCor_dy = " << dn_eCor_dy << endmsg;
    debug() << "   semi-analytic dEcor_dEcl = " << dEcor_dEcl << " numeric dn_eCor_de = " << dn_eCor_de << endmsg;
  }

  float ECorrection::computeDTheta(
      const DeCalorimeter&                                                                  calo,
      Event::Calo::Clusters::reference<SIMDWrapper::Scalar, Pr::ProxyBehaviour::Contiguous> cluster,
      CaloFuture2Track::ICluster2TrackTable2D const& ctable, IGeometryInfo const& geometry ) const {

    const auto range = ctable.relations( cluster.cellID() );
    if ( range.empty() ) return 0;
    //  incidence angle charged
    const Track* ctrack    = range.front();
    const auto   calostate = m_caloElectron->caloState( calo, *ctrack, geometry );
    const float  incidence =
        ( calostate.location() != State::Location::LocationUnknown ? calostate.momentum().Theta() : 0 );
    auto cMomentum = Momentum( cluster );
    return incidence - cMomentum.momentum().Theta();
  }
} // namespace LHCb::Calo
