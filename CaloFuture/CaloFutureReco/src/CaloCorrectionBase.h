/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Core/FloatComparison.h"
#include "DetDesc/Condition.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloHypos_v2.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/compose.h"
#include "LHCbMath/FastMaths.h"
#include "Relations/IRelationWeighted2D.h"
#include "boost/container/static_vector.hpp"
#include "fmt/format.h"
#include <variant>

static const InterfaceID IID_CaloFutureCorrectionBase( "CaloFutureCorrectionBase", 1,
                                                       0 ); // TODO: probably change IF version (?)

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Calo::Hypotheses::Type>& r, const std::string& s );
} // namespace Gaudi::Parsers

// DO NOT CHANGE THE FUNCTION ORDER FOR BACKWARD COMPATIBILITY WITH EXISTING CONDITIONS DB
namespace LHCb::Calo::Correction {

  meta_enum_class( FunctionType, int, InversPolynomial = 0, Polynomial = 1, ExpPolynomial = 2, ReciprocalPolynomial = 3,
                   Sigmoid = 4, Sshape = 5, ShowerProfile = 6, SshapeMod = 7, Sinusoidal = 8,
                   ParamList       = 9,  // simple parameter access (by area)
                   GlobalParamList = 10, // simple parameter access (ind. of area)
                   Unknown )             // type of correction from DB that is unspecified here. MUST be the last item.

      meta_enum_class( Type, int,
                       // NB: numbering is not continuous due to removed PRS and SPD/Converted_photons -related
                       // parameters for Run 1-2 CondDB compatibility E-Correction parameters
                       alphaG = 0, // global alpha factor
                       alphaE = 1, // alpha(E)
                       alphaB = 2, // alpha(Bary)
                       alphaX = 3, // alpha(Dx)
                       alphaY = 4, // alpha(Dy)
                       alphaP = 5, beta = 6, betaP = 7, betaPR = 8, betaC = 9, betaCP = 10, betaCPR = 11, globalC = 12,
                       globalT           = 13, // global(DeltaTheta) function of incidence angle
                       offsetT           = 14, // offset(DeltaTheta) function of incidence angle
                       offset            = 15, // offset( sinTheta ) energy (or ET ) offset
                       offsetC           = 16,
                       ClusterCovariance = 17, // parameters for cluster covariance estimation
                       // L-Correction parameters
                       gamma0 = 18, delta0 = 19,
                       gammaP = 20, // Prs-related L-correction (non-zero at ePrs = 0)
                       deltaP = 21, // Prs-related L-correction (non-sero at ePrs = 0)
                       // S-correction parameters
                       shapeX = 22, shapeY = 23, residual = 24, residualX = 25, residualY = 26, asymP = 27, asymM = 28,
                       angularX = 29, angularY = 30,
                       // ShowerShape profile
                       profile = 31, profileC = 32,
                       // Cluster masking
                       EnergyMask = 33, PositionMask = 34, Unknown )

          struct Result {
    float value;
    float derivative;
  };

  struct Polynomial {
    static constexpr const char*               name = "Polynomial";
    boost::container::static_vector<float, 10> pars; // FIXME: how do I know that 10 is enough? Seems to depend on the
                                                     // database contents... maybe use small_vector instead???
    Result correction( float var ) const;
  };
  struct InversePolynomial final : private Polynomial {
    InversePolynomial( LHCb::span<const float> p ) : Polynomial{ { p.begin(), p.end() } } {}
    static constexpr const char* name = "InversePolynomial";
    Result                       correction( float var ) const;
  };
  struct ExpPolynomial final : private Polynomial {
    static constexpr const char* name = "ExpPolynomial";
    float                        cached;
    ExpPolynomial( LHCb::span<const float> p )
        : Polynomial{ { p.begin(), p.end() } }, cached{ LHCb::Math::Approx::vapprox_exp( (float)p[0] ) } {}
    Result correction( float var ) const;
  };
  struct ReciprocalPolynomial final : private Polynomial {
    static constexpr const char* name = "ReciprocalPolynomial";
    ReciprocalPolynomial( LHCb::span<const float> p ) : Polynomial{ { p.begin(), p.end() } } {}
    Result correction( float var ) const;
  };
  struct Sigmoid final {
    static constexpr const char* name = "Sigmoid";
    float                        a, b, c, d;
    Sigmoid( LHCb::span<const float, 4> p ) : a{ p[0] }, b{ p[1] }, c{ p[2] }, d{ p[3] } {}
    Result correction( float var ) const;
  };
  struct Sshape {
    static float mysinh( float x ) {
      const auto y = LHCb::Math::Approx::vapprox_exp( -x );
      return 0.5 * ( ( 1.0 / y ) - y );
    }
    static constexpr const char* name  = "Sshape";
    constexpr static float       delta = 0.5;
    float                        b;
    float                        cache;
    Sshape( float b, bool modified = false ) : b{ b }, cache{ !essentiallyZero( b ) ? mysinh( delta / b ) : INFINITY } {
      if ( !modified ) cache = LHCb::Math::Approx::approx_sqrt( 1.f + cache * cache );
    }
    Result correction( float var ) const;
  };
  struct SshapeMod final : Sshape {
    static constexpr const char* name = "SshapeMod";
    SshapeMod( float p ) : Sshape( p, true ) {}
  };
  struct ShowerProfile final {
    static constexpr const char* name = "ShowerProfile";
    std::array<float, 10>        pars;
    ShowerProfile( LHCb::span<const float, 10> p ) { std::copy( p.begin(), p.end(), pars.begin() ); }
    Result correction( float var ) const;
  };
  struct Sinusoidal final {
    static constexpr const char* name = "Sinusoidal";
    float                        A;
    Sinusoidal( float s ) : A{ s } {}
    Result correction( float var ) const;
  };
  struct ParamVector final {
    static constexpr const char* name = "ParamVector";
    std::vector<float>           params;
  };

  using Function   = std::variant<ParamVector, InversePolynomial, Polynomial, ExpPolynomial, ReciprocalPolynomial,
                                Sigmoid, Sshape, ShowerProfile, SshapeMod, Sinusoidal>;
  using Functions  = std::array<Function, 3>; // Inner, middle and Outer
  using Parameters = std::array<Functions, static_cast<int>( Type::Unknown ) + 1>;

  struct VectorOfHypothesesTypes : std::vector<LHCb::Event::Calo::Hypotheses::Type> {
    using std::vector<LHCb::Event::Calo::Hypotheses::Type>::vector;
    inline friend std::ostream& operator<<( std::ostream& os, const VectorOfHypothesesTypes& v ) {
      return GaudiUtils::details::ostream_joiner(
                 os << "[ ", v, ", ",
                 []( std::ostream& s, LHCb::Event::Calo::Hypotheses::Type t ) -> std::ostream& {
                   return s << std::quoted( LHCb::Event::Calo::Enum::toString( t ), '\'' );
                 } )
             << " ]";
    }
  };

  /**
   *  @author Olivier Deschamps
   *  @date   2010-05-07
   */
  class Base : public LHCb::DetDesc::ConditionAccessorHolder<GaudiTool> {

  public:
    static const InterfaceID& interfaceID() { return IID_CaloFutureCorrectionBase; }

    Base( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override;
    StatusCode finalize() override;

    Parameters const& getParameters() const { return m_params.get(); }

    std::optional<Result> getCorrectionAndDerivative( Parameters const& params, const Type type,
                                                      const Detector::Calo::CellID id, const float var = 0. ) const;

    std::optional<float> getCorrection( Parameters const& params, const Type type, const Detector::Calo::CellID id,
                                        const float var = 0. ) const;

    const std::vector<float>& getParamVector( Parameters const& params, Type const type,
                                              Detector::Calo::CellID const id = Detector::Calo::CellID() ) const {
      return std::get<ParamVector>( params[static_cast<int>( type )][id.area()] ).params;
    }

    std::optional<float> getParameter( Parameters const& params, Type type, unsigned int i,
                                       const Detector::Calo::CellID id = Detector::Calo::CellID() ) const {
      const auto& data = getParamVector( params, type, id );
      if ( i >= data.size() ) return std::nullopt;
      return data[i];
    }

  protected:
    using IncCounter = Gaudi::Accumulators::Counter<>;
    using ACounter   = Gaudi::Accumulators::AveragingCounter<>;
    using SCounter   = Gaudi::Accumulators::StatCounter<>;

    static constexpr int k_numOfCaloAreas{ 4 };

    template <typename P, typename COUNTER = SCounter>
    static auto make_counters( P* parent, std::string_view prefix ) {
      using namespace std::string_view_literals;
      return std::apply(
          [parent, prefix]( auto&&... args ) {
            return std::array{ COUNTER{ parent, fmt::format( "{}{}", prefix, args ) }... };
          },
          std::array{ "Outer"sv, "Middle"sv, "Inner"sv, "PinArea"sv } );
    }

    LHCb::ClusterFunctors::ClusterArea       m_area;
    Gaudi::Property<bool>                    m_correctCovariance{ this, "CorrectCovariance", true };
    Gaudi::Property<VectorOfHypothesesTypes> m_hypos{ this,
                                                      "Hypotheses",
                                                      {
                                                          LHCb::Event::Calo::Hypotheses::Type::Photon,
                                                          LHCb::Event::Calo::Hypotheses::Type::PhotonFromMergedPi0,
                                                          LHCb::Event::Calo::Hypotheses::Type::EmCharged,
                                                      },
                                                      "acceptable hypotheses" };

  private:
    Gaudi::Property<std::string> m_conditionName{ this, "ConditionName",
#ifdef USE_DD4HEP
                                                  "/world/DownstreamRegion/Ecal:EcalClusterMasks"
#else
                                                  "Conditions/Reco/Calo/EcalClusterMasks"
#endif
    };
    Gaudi::Property<std::vector<std::string>> m_corrections{ this, "Corrections", { "All" } };

    std::optional<Type> accept( const std::string& name ) const {
      auto b = std::any_of( m_corrections.begin(), m_corrections.end(),
                            [&]( const auto& i ) { return i == name || i == "All"; } );
      if ( !b ) return std::nullopt;
      // check if known type
      Type type;
      if ( parse( type, name ).isSuccess() ) return type;
      warning() << " o Type " << name << " is not known" << endmsg;
      return std::nullopt;
    }

    static const char* correctionName( const Function& cp ) {
      return std::visit( []( const auto& i ) { return i.name; }, cp );
    }

    ConditionAccessor<Parameters> m_params{ this, name() + "-Parameters" };
    void                          constructParams( Functions&, const LHCb::span<const double> );
    Parameters                    constructParams( YAML::Node const& );

  public:
    // required by clients to retrieve internal condition m_param
    // and use it directly when deriving their own conditions
    // The original reason being that one cannot call get of a
    // ConditionAccessor during condition derivation
    std::string getParamConditionPath() { return m_params.key(); }
  };

} // namespace LHCb::Calo::Correction
