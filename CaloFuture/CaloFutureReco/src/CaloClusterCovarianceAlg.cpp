/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "ClusterCovarianceMatrixTool.h"
#include "ClusterSpreadTool.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include "SubClusterSelectorTool.h"

/** @class CaloFutureClusterCovarianceAlg CaloFutureClusterCovarianceAlg.h
 *
 *   Simple algorithm for evaluation of covariance matrix
 *   for CaloFutureCluster object
 *
 *  @see CaloFutureClusterCovarianceAlg
 *  @see ICaloFutureClusterTool
 *  @see ICaloFutureSubClusterTag
 *
 *  @author Vanya Belyaev Ivan Belyaev
 *  @date   04/07/2001
 */

namespace LHCb::Calo::Algorithms {
  class ClusterCovariance
      : public Algorithm::Transformer<Event::Calo::Clusters( const EventContext&, const DeCalorimeter&,
                                                             const Event::Calo::Clusters& ),
                                      LHCb::Algorithm::Traits::usesConditions<DeCalorimeter>> {

  public:
    ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator );
    Event::Calo::Clusters operator()( const EventContext& evtCtx, const DeCalorimeter&,
                                      const Event::Calo::Clusters& ) const override;

  private:
    // tool used for covariance matrix calculation
    ToolHandle<LHCb::Calo::ClusterCovarianceMatrixTool> m_cov{
        this, "CovarianceTool",
        "FutureClusterCovarianceMatrixTool/" + toString( Utilities::CaloIndexFromAlg( name() ) ) + "CovarTool" };
    ToolHandle<SubClusterSelectorTool> m_tagger{ this, "TaggerTool",
                                                 "FutureSubClusterSelectorTool/" +
                                                     toString( Utilities::CaloIndexFromAlg( name() ) ) + "ClusterTag" };
    // tool used for cluster spread estimation
    ToolHandle<LHCb::Calo::ClusterSpreadTool> m_spread{
        this, "SpreadTool",
        "FutureClusterSpreadTool/" + toString( Utilities::CaloIndexFromAlg( name() ) ) + "SpreadTool" };

    // following properties are inherited by the selector tool when defined:
    Gaudi::Property<std::string>              m_tagName{ this, "TaggerName" };
    Gaudi::Property<std::vector<std::string>> m_taggerE{ this, "EnergyTags" };
    Gaudi::Property<std::vector<std::string>> m_taggerP{ this, "PositionTags" };

    // counter
    mutable Gaudi::Accumulators::Counter<> m_clusters{ this, "# clusters" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_cov_error{ this, "Error from cov,    skip cluster " };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_spread_error{ this, "Error from spread, skip cluster " };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_tagger_error{ this, "Error from tagger, skip cluster " };
  };
  DECLARE_COMPONENT_WITH_ID( ClusterCovariance, "CaloFutureClusterCovarianceAlg" )

  ClusterCovariance::ClusterCovariance( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "Detector", Utilities::DeCaloFutureLocation( name ) },
                       KeyValue{ "InputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" ) } },
                     KeyValue{ "OutputData", CaloFutureAlgUtils::CaloFutureClusterLocation( name, "Ecal" ) } ) {}

  [[gnu::flatten]] Event::Calo::Clusters ClusterCovariance::operator()( const EventContext&          evtCtx,
                                                                        const DeCalorimeter&         calo,
                                                                        const Event::Calo::Clusters& clusters ) const {

    // create new container for output clusters
    Event::Calo::Clusters outputClusters{ Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) };
    outputClusters.reserve( clusters.size() );

    // get parameters from m_cov and m_tagger, this is basically a local cache
    // of conditions so that we are not retrieving them n times
    auto const& params = m_cov->getParameters();
    auto const& tags   = m_tagger->getTags();

    // copy clusters to new container, and apply corrections on the way...
    for ( const auto cluster : clusters.scalar() ) {
      outputClusters.copy_back<SIMDWrapper::InstructionSet::Scalar>( cluster );
      auto newcluster = outputClusters.scalar()[outputClusters.size() - 1];

      // == APPLY TAGGER
      if ( m_tagger->tag( tags, calo, newcluster.cellID(), newcluster.entries() ).isFailure() ) {
        ++m_tagger_error;
        outputClusters.resize( outputClusters.size() - 1 );
        continue;
      }

      // == APPLY COVARIANCE ESTIMATOR
      if ( ( *m_cov ).compute( params, calo, newcluster ).isFailure() ) {
        ++m_cov_error;
        outputClusters.resize( outputClusters.size() - 1 );
        continue;
      }

      // == APPLY SPREAD ESTIMATOR
      if ( ( *m_spread ).compute( calo, newcluster ).isFailure() ) {
        ++m_spread_error;
        outputClusters.resize( outputClusters.size() - 1 );
        continue;
      }
    }
    m_clusters += clusters.size();
    return outputClusters;
  }
} // namespace LHCb::Calo::Algorithms
