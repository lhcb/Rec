#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## Confurable for CaloFuturerimeter Reconstruction
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
Configurable for CaloFuturerimeter Reconstruction
"""

from __future__ import print_function

# =============================================================================
from future.utils import raise_

__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
__all__ = (
    "HltCaloFutureRecoConf",
    "OffLineCaloFutureRecoConf",
    "CaloFutureRecoConf",
    "CaloFutureProcessor",
    "CaloFutureLines",
)
# =============================================================================

import logging

from CaloKernel.ConfUtils import (
    addAlgs,
    hltContext,
    printOnDemand,
    prntCmp,
    setTheProperty,
)

##from Configurables            import GlobalRecoConf
from Configurables import CaloFutureDigitConf, CaloFuturePIDsConf, GaudiSequencer
from GaudiKernel.SystemOfUnits import GeV, MeV
from LHCbKernel.Configuration import *

from CaloFutureReco.Reconstruction import (
    classifyClusPhotonElec,
    clusterFutureReco,
    mergedPi0FutureReco,
)

_log = logging.getLogger("CaloFutureReco")

import os


# =============================================================================
## @class CaloFutureRecoConf
#  Configurable for CaloFuturerimeter Reconstruction
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class CaloFutureRecoConf(LHCbConfigurableUser):
    """
    Class/Configurable to define the calorimeter reconstruction

    """

    ## define the slots
    __slots__ = {
        ##
        "Context": "",  # The context to run (default = offline)
        "MeasureTime": False,  # Measure the time for sequencers
        "OutputLevel": INFO,  # The global output level
        ##
        "Sequence": None,  # The sequencer to add the CALOFUTURE reconstruction algorithms to
        "RecList": [
            "Digits",
            "Clusters",
            "Photons",
            "MergedPi0s",
            "SplitPhotons",  # the same action as 'MergedPi0s'
            "Electrons",
        ],  #
        "ForceDigits": True,  # Force digits recontruction to be run with Clusters
        "CreateADCs": False,  # Create CaloFuture-ADCs
        "UseTracks": True,  # Use Tracks as Neutrality Criteria
        "UseTracksE": True,  # Use Tracks as Charge  Criteria
        "UseTracksM": True,  # Use Tracks for MergedPi0s-ID
        "CaloFutureStandalone": False,  # useTrackX = false
        "NeutralID": False,  # Apply neutralID (CaloFuturePIDs is in charge + problems with onDemand to be fixed)
        "EnableRecoOnDemand": False,  # Enable Reco-On-Demand (only for components on RecList)
        "TrackLocation": "",  # Track location (Neutral/Charged cluster selection with UseTrack(E) )
        "ClMatchTrTypes": [],  # Accepted track types for cluster matching (default Long, Downstream , TTracks)
        "ExternalClusters": "",  # Start the reconstruction sequence from an external cluster container (HLT)
        "FastReco": False,  # faster reconstruction (HLT)
        "SkipNeutrals": False,
        "SkipCharged": False,
        "ForceOnDemand": False,  # force DoD for ALL components (incl. those not in RecList)
        "PhotonEt": 50.0 * MeV,  # Min Cluster Et
        "ElectronPt": 50.0 * MeV,  # Min Cluster Et
        "MergedPi0Pt": 1.5 * GeV,
        "ClusterPt": 0.0,
        "MakeExternalClustersWithTag": "",
        "ClusterEnergyMasks": [],
        "ClusterPositionMasks": [],
        "Verbose": False,
        "SetCounters": 1,  # Quiet is the default
        ##
    }
    ## documentation lines
    _propertyDocDct = {
        ##
        "Context": """ The context to run """,
        "MeasureTime": """ Measure the time for sequencers """,
        "OutputLevel": """ The global output level """,
        ##
        "Sequence": """ The sequencer to add the CALOFUTURE reconstruction algorithms to """,
        "RecList": """ The recontruction sketch """,
        "ForceDigits": """ Force digits recontruction to be run with Clusters """,
        "CreateADCs": """ Create CaloFuture-ADCs """,
        "UseTracks": """ Use Tracks as Neutrality criterion """,
        "UseTracksE": """ Use Tracks as Charge criterion """,
        "UseTracksM": """ Use Tracks for MergedPi0s-ID """,
        "CaloFutureStandalone": """ UseTrack = false """,
        "NeutralID": """ Apply neutralID """,
        "EnableRecoOnDemand": """ Enable Reco-On-Demand (for components in RecList) """,
        "TrackLocation": """ TrackLocation (Photon/Electron selection)""",
        "ClMatchTrTypes": """ Accepted track types for cluster matching (default Long, Downstream , TTracks) """,
        "SkipNeutrals": """ Skip Neutral reco components in RecList""",
        "SkipCharged": """ Skip Charged reco components in RecList""",
        "ForceOnDemand": """ force DoD for ALL components""",
        "FastReco": """ Speed-up reconstruction (empty so far)""",
        "ExternalClusters": """ Start the reconstruction sequence from an external cluster container (HLT)""",
        "PhotonEt": """ Photon Hypo Et cut """,
        "ElectronPt": """ Electron Hypo Pt cut """,
        "MergedPi0Pt": """ MergedPi0 Pt cut """,
        "ClusterPt": """ Cluster Pt """,
        "MakeExternalClustersWithTag": """ Build tagged external clusters in the sequence (only if ExternalClusters != '')""",
        "ClusterEnergyMasks": """ Set the cluster mask for energy   evaluation (default : set from DB) """,
        "ClusterPositionMasks": """ Set the cluster mask for position evaluation (default : set from DB) """,
        "Verbose": """ Verbose printout""",
        "SetCounters": """ Counters verbosity""",
        ##
    }

    ## used configurables
    __used_configurables__ = ((CaloFutureDigitConf, None),)

    ## configure processing of Digits
    def digits(self):
        """
        Configure processing of Digits
        """
        digitConf = CaloFutureDigitConf()
        digitConf.OutputLevel = self.getProp("OutputLevel")
        digitConf.EnableDigitsOnDemand = self.getProp("EnableRecoOnDemand")
        digitConf.CreateADCs = self.getProp("CreateADCs")
        digitConf.Verbose = self.getProp("Verbose")
        digitConf.Detectors = ["Ecal", "Hcal"]

        return digitConf.digits()

    ## Configure reconstruction of Ecal Clusters
    def clusters(self):
        """
        Configure reconstruction of Ecal Clusters
        """
        cmp = clusterFutureReco(
            self.getProp("EnableRecoOnDemand"),
            self.getProp("ClusterPt"),
            self.getProp("FastReco"),
            self.getProp("ExternalClusters"),
            self.getProp("MakeExternalClustersWithTag"),
            self.getProp("ClusterEnergyMasks"),
            self.getProp("ClusterPositionMasks"),
            self.getProp("MergedPi0Pt"),
        )
        _log.info("Configured Ecal Clusters Reco : %s " % cmp.name())

        if self.getProp("ForceDigits"):
            ## insert digits into Cluster Sequence
            cmp.Members = [self.digits()] + cmp.Members

        ##
        return cmp

    ## reconstruct photon and electron hypos
    def photonsAndElectrons(self):
        """
        Define the reconstruction of Photons
        """
        uTracks = self.getProp("UseTracks")
        if self.getProp("CaloFutureStandalone"):
            uTracks = False

        cmp = classifyClusPhotonElec(
            self.getProp("EnableRecoOnDemand"),
            uTracks,
            self.getProp("TrackLocation"),
            self.getProp("NeutralID"),
            self.getProp("PhotonEt"),
            self.getProp("ElectronPt"),
            self.getProp("FastReco"),
            self.getProp("ExternalClusters"),
            self.getProp("ClMatchTrTypes"),
        )
        ##
        _log.info(
            "Configured photon and electron hypo reconstruction : %s " % cmp.name()
        )
        return cmp

    ## Configure recontruction of Merged Pi0
    def mergedPi0s(self):
        """
        Configure recontruction of Merged Pi0
        """
        uTracks = self.getProp("UseTracksM")
        if self.getProp("CaloFutureStandalone"):
            uTracks = False

        cmp = mergedPi0FutureReco(
            self.getProp("EnableRecoOnDemand"),
            self.getProp("NeutralID"),
            uTracks,
            self.getProp("MergedPi0Pt"),
            self.getProp("FastReco"),
            self.getProp("ExternalClusters"),
            self.getProp("ClusterEnergyMasks"),
            self.getProp("ClusterPositionMasks"),
        )

        ##
        _log.info("Configured Merged Pi0 Reco : %s " % cmp.name())
        return cmp

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        _log.debug("Configuration is not checked !")

    def setCounterLevel(self):
        # == Counter level
        from Configurables import FutureCounterLevel, ToolSvc

        tsvc = ToolSvc()
        tsvc.addTool(FutureCounterLevel, name="CounterLevel")
        level = self.getProp("SetCounters")
        _log.info("Configurabe : Counter level is %s ", level)
        tsvc.CounterLevel.SetLevel = level

    def printConf(self, verbose=False):
        _log.info("CaloFutureRecoConf : upgrade configuration without Spd/Prs")
        if self.getProp("Verbose") or verbose:
            _log.info(self)

    ## Calorimeter Reconstruction Configuration
    def applyConf(self):
        """
        Calorimeter Reconstruction Configuration
        """

        self.printConf()

        _log.info(
            "Apply CaloFutureRecoConf configuration for %s ", self.getProp("RecList")
        )

        recList = self.getProp("RecList")
        skipNeutrals = self.getProp("SkipNeutrals")
        skipCharged = self.getProp("SkipCharged")
        forceOnDemand = self.getProp("ForceOnDemand")

        seq = []

        # configure all components for DoD
        if forceOnDemand:
            _log.info("Force Data-On-Demand for all components")
            self.setProp("EnableRecoOnDemand", "True")
            self.digits()
            self.clusters()
            self.photonsAndElectrons()
            self.mergedPi0s()

        self.setCounterLevel()

        # add only the requested components to the sequence
        if "Digits" in recList:
            addAlgs(seq, self.digits())
            CaloFutureDigitConf().printConf()
        if "Clusters" in recList:
            addAlgs(seq, self.clusters())
            if "Digits" not in recList:
                CaloFutureDigitConf().printConf()
        if (not skipCharged) or (not skipNeutrals):
            if ("Photons" in recList) or ("Electrons" in recList):
                addAlgs(seq, self.photonsAndElectrons())
        if not skipNeutrals:
            if "MergedPi0s" in recList or "SplitPhotons" in recList:
                addAlgs(seq, self.mergedPi0s())

        setTheProperty(seq, "Context", self.getProp("Context"))
        setTheProperty(seq, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(seq, "OutputLevel", self.getProp("OutputLevel"))

        if self.isPropertySet("Sequence"):
            main = self.getProp("Sequence")
            addAlgs(main, seq)
            _log.info("Configure main CaloFuture Reco Sequence  : %s " % main.name())
            if self.getProp("Verbose"):
                _log.info(prntCmp(main))
        else:
            _log.info("Configure CaloFuturerimeter Reco blocks ")
            if self.getProp("Verbose"):
                _log.info(prntCmp(seq))

        if self.getProp("EnableRecoOnDemand"):
            _log.info("CaloFutureReco onDemand enabled")
            if self.getProp("Verbose"):
                _log.info(printOnDemand())


# =============================================================================
## @class HltCaloFutureRecoConf
#  Configurable for CaloFuturerimeter Reconstruction in Hlt context
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class HltCaloFutureRecoConf(CaloFutureRecoConf):
    """
    Class/Configurable to define the calorimeter reconstruction for Hlt

    """

    __slots__ = {}

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        if not hltContext(self.getProp("Context")):
            raise AttributeError("Invalid context for HltCaloFutureRecoConf")


# =============================================================================
## @class OffLineCaloFutureRecoConf
#  Configurable for CaloFuturerimeter Reconstruction in OffLine context
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class OffLineCaloFutureRecoConf(CaloFutureRecoConf):
    """
    Class/Configurable to define the calorimeter reconstruction for Off-Line

    """

    __slots__ = {}

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        if hltContext(self.getProp("Context")):
            raise AttributeError("Invalid context for OffLineCaloFutureRecoConf")


# =============================================================================
class CaloFutureProcessor(CaloFutureRecoConf, LHCbConfigurableUser):
    """
    Class/Configurable to define the Full calorimeter reconstruction
    """

    ## -- re-use CaloFutureRecoConf and add caloPIDs and ProtoP [double-inheritance fails due to conflicts]

    ## define the additional slots
    __slots__ = {
        "CaloFutureReco": True,  ## process CaloFutureReco part
        "CaloFuturePIDs": True,  ## process CaloFuturePID part
        "EnableOnDemand": False,  ## overwrite EnableRecoOnDemand & EnablePIDsOnDemand
        "ProtoOnDemand": False,
        "NeutralProtoLocation": "",
        "ChargedProtoLocation": "",
        "CaloFutureSequencer": None,
        "ProtoSequencer": None,
        "NeutralProtoSequencer": None,
        "ChargedProtoSequencer": None,
        ## PRIVATE TrackTypes & TrackCuts property (as in GlobalRecoConf) : for ChargedProto creation OnD.
        "TrackTypes": ["Long", "Upstream", "Downstream"],
        "TrackCuts": {
            "Long": {"Chi2Cut": [0, 10]},
            "Upstream": {"Chi2Cut": [0, 10]},
            "Downstream": {"Chi2Cut": [0, 10]},
        },
        ##
        "DataType": "MC09",
        "CaloFuturePIDTrTypes": [],
        "BremPIDTrTypes": [],
        "PIDList": [
            "InAcceptance",
            "Match",
            "Energy",
            "Chi2",
            "DLL",
            "NeutralPID",
        ],  # List of PID fragments to be included (alternative full sequence per technique : [ 'EcalPID', 'BremPID', 'HcalPID','NeutralPID' ] )
    }

    ## used configurables
    __used_configurables__ = ((CaloFuturePIDsConf, None), (CaloFutureDigitConf, None))

    def caloPIDs(self):
        pidConf = CaloFuturePIDsConf("CaloFuturePIDsFor" + self.getName())
        pidConf.Verbose = self.getProp("Verbose")
        pidConf.EnablePIDsOnDemand = self.getProp("EnableOnDemand")
        pidConf.PIDList = self.getProp("PIDList")
        pidConf.TrackLocation = self.getProp("TrackLocation")
        pidConf.ClMatchTrTypes = self.getProp("ClMatchTrTypes")
        pidConf.CaloFuturePIDTrTypes = self.getProp("CaloFuturePIDTrTypes")
        pidConf.BremPIDTrTypes = self.getProp("BremPIDTrTypes")
        pidConf.SkipNeutrals = self.getProp("SkipNeutrals")
        pidConf.SkipCharged = self.getProp("SkipCharged")
        pidConf.FastPID = self.getProp("FastReco")
        pidConf.ExternalClusters = self.getProp("ExternalClusters")
        pidConf.DataType = self.getProp("DataType")
        pidConf.OutputLevel = self.getProp("OutputLevel")
        pidConf.printConf()
        ##
        return pidConf.caloPIDs()

    def caloFutureSequence(self, tracks=""):
        _log.info("TEST:  caloFutureSequence")
        seq = GaudiSequencer(self.getName() + "CaloFutureSeq")
        seq.Members[:] = []
        self.setProp("CaloFutureSequencer", seq)
        if tracks:
            self.setProp("TrackLocation", tracks)
            _log.info(
                "CaloFutureProcessor.caloFutureSequence : update TrackLocation %s: "
                % tracks
            )

        self.applyConf()
        return seq

    def protoSequence(self, tracks="", protoPrefix=""):
        _log.info("TEST:  photonFutureSequence")
        seq = GaudiSequencer(self.getName() + "ProtoPUpdateSeq")
        seq.Members[:] = []
        self.setProp("ProtoSequencer", seq)
        if tracks:
            self.setProp("TrackLocation", tracks)
            _log.info(
                "CaloFutureProcessor.protoSequence : update TrackLocation %s: " % tracks
            )
        if protoPrefix:
            nloc = os.path.join(protoPrefix, "Neutrals")
            cloc = os.path.join(protoPrefix, "Charged")
            self.setProp("ChargedProtoLocation", cloc)
            self.setProp("NeutralProtoLocation", nloc)
            _log.info(
                "CaloFutureProcessor.protoSequence : update protoP Location with prefix %s: "
                % protoPrefix
            )
        self.applyConf()
        return seq

    def chargedProtoSequence(self, tracks="", protoPrefix=""):
        _log.info("TEST:  chargedProtoFutureSequence")
        seq = GaudiSequencer(self.getName() + "ChargedProtoPUpdateSeq")
        seq.Members[:] = []
        self.setProp("ChargedProtoSequencer", seq)
        if tracks:
            self.setProp("TrackLocation", tracks)
            _log.info(
                "CaloFutureProcessor.chargedProtoSequence : update TrackLocation %s: "
                % tracks
            )
        if protoPrefix:
            cloc = protoPrefix
            if not cloc.endswith("Charged"):
                cloc = os.path.join(protoPrefix, "Charged")
            self.setProp("ChargedProtoLocation", cloc)
            _log.info(
                "CaloFutureProcessor.chargedProtoSequence : update protoP Location with prefix %s: "
                % protoPrefix
            )

        self.applyConf()
        return seq

    def neutralProtoSequence(self, tracks="", protoPrefix=""):
        _log.info("TEST:  neutralProtoSequence")
        seq = GaudiSequencer(self.getName() + "NeutralProtoPUpdateSeq")
        seq.Members[:] = []
        self.setProp("NeutralProtoSequencer", seq)
        if tracks:
            self.setProp("TrackLocation", tracks)
            _log.info(
                "CaloFutureProcessor.neutralProtoSequence : update TrackLocation %s: "
                % tracks
            )
        if protoPrefix:
            nloc = protoPrefix
            if not nloc.endswith("Neutrals"):
                nloc = os.path.join(protoPrefix, "Neutrals")
            self.setProp("NeutralProtoLocation", nloc)
            _log.info(
                "CaloFutureProcessor.neutralProtoSequence : update protoP Location with prefix %s: "
                % protoPrefix
            )
        self.applyConf()
        return seq

    def sequence(self, tracks="", protoPrefix=""):
        _log.info("TEST:  sequence")
        seq = GaudiSequencer(self.getName() + "FullSeq")
        seq.Members[:] = []
        self.setProp("Sequence", seq)
        if tracks:
            self.setProp("TrackLocation", tracks)
            _log.info(
                "CaloFutureProcessor.sequence : update TrackLocation %s: " % tracks
            )
        if protoPrefix:
            nloc = os.path.join(protoPrefix, "Neutrals")
            cloc = os.path.join(protoPrefix, "Charged")
            self.setProp("ChargedProtoLocation", cloc)
            self.setProp("NeutralProtoLocation", nloc)
            _log.info(
                "CaloFutureProcessor.sequence : update protoP Location with prefix %s: "
                % protoPrefix
            )

        self.applyConf()
        return seq

    def printConf(self, verbose=False):
        _log.info("CaloFutureProcessor : upgrade configuration without Spd/Prs")
        if self.getProp("Verbose") or verbose:
            _log.info(self)

    def applyConf(self):
        _log.info(
            "Apply CaloFutureProcessor configuration for %s and %s",
            self.getProp("RecList"),
            self.getProp("PIDList"),
        )

        self.printConf()

        knownMasks = ["3x3", "2x2", "SwissCross"]

        for tag in self.getProp("ClusterEnergyMasks"):
            if tag not in knownMasks:
                raise_(
                    AttributeError,
                    "ClusterEnergyMasks contains unknown tag"
                    + tag
                    + " -should be in"
                    + knownMasks,
                )
        for tag in self.getProp("ClusterPositionMasks"):
            if tag not in knownMasks:
                raise_(
                    AttributeError,
                    "PositionEnergyMasks contains unknown tag "
                    + tag
                    + " -should be in"
                    + knownMasks,
                )

        from Configurables import (
            ChargedProtoParticleAddBremInfo,
            ChargedProtoParticleAddCaloInfo,
            ChargedProtoParticleAddCombineDLLs,
            FunctionalChargedProtoParticleMaker,
            GaudiSequencer,
        )

        fullSeq = []

        if self.getName() == "CaloFutureProcessor" and (
            self.getProp("Context") == ""
            or self.getProp("Context") == "CaloFutureProcessor"
        ):
            self.setProp(
                "Context", "Offline"
            )  # default is Offline is neither context nor name is specified

        self.setCounterLevel()
        # overwrite Reco & PID onDemand
        dod = self.getProp("EnableOnDemand")
        self.setProp("EnableRecoOnDemand", dod)

        ## define the calo sequence
        caloSeq = []

        doReco = self.getProp("CaloFutureReco")
        doPIDs = self.getProp("CaloFuturePIDs")
        skipNeutrals = self.getProp("SkipNeutrals")
        skipCharged = self.getProp("SkipCharged")

        # CaloFutureReco sequence
        recoSeq = GaudiSequencer("CaloFutureRecoFor" + self.getName())
        recoSeq.Members[:] = []
        recList = self.getProp("RecList")

        # configure all components by default (DoD)
        forceOnDemand = self.getProp("ForceOnDemand")
        if forceOnDemand:
            self.setProp("EnableOnDemand", "True")
            self.setProp("EnableRecoOnDemand", "True")

        #  add only the requested components to the sequence
        if "Digits" in recList:
            addAlgs(recoSeq, self.digits())
            CaloFutureDigitConf().printConf()
        if "Clusters" in recList:
            addAlgs(recoSeq, self.clusters())
            if "Digits" not in recList:
                CaloFutureDigitConf().printConf()

        if (not skipNeutrals) or (not skipCharged):
            if "Photons" in recList or "Electrons" in recList:
                addAlgs(recoSeq, self.photonsAndElectrons())
        if not skipNeutrals:
            if "MergedPi0s" in recList or "SplitPhotons" in recList:
                addAlgs(recoSeq, self.mergedPi0s())

        pidSeq = self.caloPIDs()

        # update CaloFutureSequence
        if doReco:
            addAlgs(caloSeq, recoSeq)
        if doPIDs:
            addAlgs(caloSeq, pidSeq)

        ## propagate the global properties
        setTheProperty(caloSeq, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(caloSeq, "OutputLevel", self.getProp("OutputLevel"))

        ######## ProtoParticle update ##########
        protoSeq = []
        cProtoSeq = []
        nProtoSeq = []
        #  ProtoParticle locations
        nloc = self.getProp("NeutralProtoLocation")
        cloc = self.getProp("ChargedProtoLocation")

        # ChargedProtoParticle
        if not self.getProp("SkipCharged"):
            # ChargedProtoP Maker on demand (not in any sequencer)  ####
            maker = FunctionalChargedProtoParticleMaker("ChargedProtoMaker", AddInfo=[])

            def addInfo(typ, name):
                t = maker.addTool(typ, name)
                maker.AddInfo += [t]
                return t

            calos = addInfo(ChargedProtoParticleAddCaloInfo, "AddCalo")

            from Configurables import (
                LHCb__Converters__Calo__Hypo__v1__fromV2 as HypoConverter,
            )

            ecal_converter1 = HypoConverter("HypoConverter_" + calos.getName())
            ecal_converter1.InputHypos = "PleaseSetMeCorrectly"
            ecal_converter1.InputClusters = "Rec/Converted/Calo/EcalClusters"
            ecal_converter1.OutputHypos = (
                "/Event/Rec/Converted/" + calos.getName() + "/Hypos"
            )

            from Configurables import (
                LHCb__Converters__Calo__Hypo2TrackTable__v1__fromV2 as HypoTableConverter,
            )

            ecal_converter2 = HypoTableConverter("HypoTableConveter_" + calos.getName())
            ecal_converter2.InputTable = calos.InputElectronMatchLocation
            ecal_converter2.InputHypotheses = ecal_converter1.OutputHypos
            ecal_converter2.OutputTable = (
                "/Event/Rec/Converted/" + calos.getName() + "/HypoTable"
            )

            calos.InputElectronMatchLocation = (
                ecal_converter2.OutputTable
            )  # HypoTrTable2D

            addInfo(ChargedProtoParticleAddBremInfo, "AddBrem")
            addInfo(ChargedProtoParticleAddCombineDLLs, "AddCombineDLLs")

            # protoPMaker settings (from GlobalRecoConf)
            from Configurables import DelegatingTrackSelector, GaudiSequencer
            ## ppConf = GlobalRecoConf('DummyConf',RecoSequencer=GaudiSequencer('DummySeq'))
            ##ttypes = ppConf.getProp('TrackTypes')
            ##tcuts  = ppConf.getProp('TrackCuts')

            ttypes = self.getProp("TrackTypes")
            tcuts = self.getProp("TrackCuts")

            maker.addTool(DelegatingTrackSelector, name="TrackSelector")
            maker.TrackSelector.TrackTypes = ttypes
            from Configurables import TrackSelector

            for type in ttypes:
                maker.TrackSelector.addTool(TrackSelector, name=type)
                ts = getattr(maker.TrackSelector, type)
                ts.TrackTypes = [type]
                if type in tcuts:
                    for name, cut in tcuts[type].items():
                        ts.setProp("Min" + name, cut[0])
                        ts.setProp("Max" + name, cut[1])
            #########################################
            if cloc:
                maker.Output = cloc

            maker.Inputs = [self.getProp("TrackLocation")]

            # Fill the sequence
            cpSeq = GaudiSequencer(self.getName() + "ChargedProtoPCaloFutureUpdateSeq")
            addAlgs(protoSeq, cpSeq)
            addAlgs(cProtoSeq, cpSeq.Members)

        # NeutralProtoParticleProtoP components
        if not self.getProp("SkipNeutrals"):
            from Configurables import FutureNeutralProtoPAlg

            suffix = "For" + self.getName()
            neutral = FutureNeutralProtoPAlg("FutureNeutralProtoPMaker" + suffix)
            # location
            if nloc:
                neutral.ProtoParticleLocation = nloc
            # fill the sequence
            addAlgs(protoSeq, neutral)
            addAlgs(nProtoSeq, neutral)

        ## propagate the global properties
        setTheProperty(protoSeq, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(protoSeq, "OutputLevel", self.getProp("OutputLevel"))

        setTheProperty(nProtoSeq, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(nProtoSeq, "OutputLevel", self.getProp("OutputLevel"))

        setTheProperty(cProtoSeq, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(cProtoSeq, "OutputLevel", self.getProp("OutputLevel"))

        # Full sequence
        addAlgs(fullSeq, caloSeq)
        addAlgs(fullSeq, protoSeq)

        ## define the sequencers
        if self.isPropertySet("Sequence"):
            main = self.getProp("Sequence")
            main.Members[:] = []
            addAlgs(main, fullSeq)
            _log.info("Configure full sequence %s with " % main.name())
            _log.info("    Reco : %s " % self.getProp("RecList"))
            _log.info("    PIDs : %s " % self.getProp("PIDList"))
            _log.info("    and ProtoParticle update")
            if self.getProp("Verbose"):
                _log.info(prntCmp(main))

        if self.isPropertySet("CaloFutureSequencer"):
            calo = self.getProp("CaloFutureSequencer")
            calo.Members[:] = []
            addAlgs(calo, caloSeq)
            _log.info("Configure caloFutureSequencer  : %s " % calo.name())
            if self.getProp("Verbose"):
                _log.info(prntCmp(calo))

        if self.isPropertySet("ProtoSequencer"):
            proto = self.getProp("ProtoSequencer")
            proto.Members[:] = []
            addAlgs(proto, protoSeq)
            _log.info("Configure protoSequencer  : %s " % proto.name())
            if self.getProp("Verbose"):
                _log.info(prntCmp(proto))

        if self.isPropertySet("ChargedProtoSequencer"):
            cproto = self.getProp("ChargedProtoSequencer")
            cproto.Members[:] = []
            addAlgs(cproto, cProtoSeq)
            _log.info("Configure chargedProtoSequencer  : %s " % cproto.name())
            if self.getProp("Verbose"):
                _log.info(prntCmp(cproto))

        if self.isPropertySet("NeutralProtoSequencer"):
            nproto = self.getProp("NeutralProtoSequencer")
            nproto.Members[:] = []
            addAlgs(nproto, nProtoSeq)
            _log.info("Configure neutralProtoSequencer  : %s " % nproto.name())
            if self.getProp("Verbose"):
                _log.info(prntCmp(nproto))

        if self.getProp("EnableOnDemand"):
            _log.info("CaloFutureProcessor onDemand enabled")
            if self.getProp("Verbose"):
                _log.info(printOnDemand())


#####################################
class CaloFutureLines(LHCbConfigurableUser):
    """
    Class/Configurable to define the HLT2 fast reconstruction for high-ET photon, low-ET photon/pi0 & low-ET electrons
    """

    #    __used_configurables__ = [ CaloFutureProcessor ]

    ## define the additional slots
    __slots__ = {
        "TrackLocation": "",  # Track location (Neutral/Charged cluster selection with UseTrack(E) )
        "HighPhoton": True,  ## process the highEt-threshold photon reconstruction
        "LowPhoton": True,  ## process the lowEt-threshold photon reconstruction
        "LowElectron": True,  ## process the LowEt-threshold electron reconstruction
        "EnableOnDemand": False,  ## overwrite EnableRecoOnDemand & EnablePIDsOnDemand
        "HighEt": 2000.0 * MeV,
        "LowEt": 300.0 * MeV,
        "ClusterEtFactor": 1.0,  # pre-cut on cluster Et is factor * Low(High)ET (should be <=1)
        "L0CaloFuture2CaloFuture": True,
        "ClusterizationLevel": 2,  # clusterizationLevel (when L0CaloFuture2CaloFuture == True only)
        "ProtoOnDemand": False,
        "Sequencer": None,
        "OutputLevel": INFO,
        "HighEtProtoPPrefix": "",
        "LowEtProtoPPrefix": "",
    }

    def sequence(self, tracks=""):
        _log.info("TEST:  def sequence")
        seq = GaudiSequencer("CaloFutureLines" + self.getName())
        seq.Members[:] = []
        self.setProp("Sequencer", seq)
        self.setProp("TrackLocation", tracks)
        self.applyConf()
        return seq

    def applyConf(self):
        from Configurables import GaudiSequencer, HltL0CaloFutureCandidates

        # overwrite Reco & PID onDemand
        dod = self.getProp("EnableOnDemand")
        pdod = self.getProp("ProtoOnDemand")
        if dod:
            pdod = dod

        trackLocation = self.getProp("TrackLocation")

        ###
        caloLines = GaudiSequencer("CaloFutureLinesSeq" + self.getName())
        caloLines.Members[:] = []

        if self.getProp("L0CaloFuture2CaloFuture"):
            l0calo2calo = HltL0CaloFutureCandidates("L0CaloFuture2CaloFuture")
            if self.getProp("ClusterizationLevel") > 0:
                level = self.getProp("ClusterizationLevel")
                l0calo2calo.ClusterizationLevel = level
            addAlgs(caloLines, l0calo2calo)
            tagHighP = ""
            tagLowP = ""
            tagLowE = ""
        else:
            tagHighP = "HighPhoton"
            tagLowP = "LowPhoton"
            tagLowE = "LowElectron"

        name = self.getName()
        fac = self.getProp("ClusterEtFactor")

        if self.getProp("HighPhoton"):
            hp = CaloFutureProcessor(
                name + "HighPhoton",
                TrackLocation=trackLocation,
                RecList=["Digits", "Clusters", "Photons"],
                CaloFuturePIDs=False,
                ExternalClusters="/Event/Rec/Calo/HighEtPhotons",
                ClusterPt=self.getProp("HighEt") * fac,
                PhotonEt=self.getProp("HighEt"),
                MakeExternalClustersWithTag=tagHighP,
            )

            addAlgs(caloLines, hp.caloFutureSequence(tracks=trackLocation))

            if self.getProp("HighEtProtoPPrefix") == "":
                hploc = name + "HighPhoton/ProtoP"
            else:
                hploc = self.getProp("HighEtProtoPPrefix")
            addAlgs(
                caloLines,
                hp.neutralProtoSequence(protoPrefix=hploc, tracks=trackLocation),
            )

        if self.getProp("LowPhoton"):
            lp = CaloFutureProcessor(
                name + "LowPhoton",
                TrackLocation=trackLocation,
                RecList=["Digits", "Clusters", "Photons", "MergedPi0s", "SplitPhotons"],
                ExternalClusters="/Event/Rec/Calo/LowEtPhotons",
                CaloFuturePIDs=False,
                ClusterPt=self.getProp("LowEt") * fac,
                PhotonEt=self.getProp("LowEt"),
                MakeExternalClustersWithTag=tagLowP,
            )
            addAlgs(caloLines, lp.caloFutureSequence(tracks=trackLocation))
            if self.getProp("LowEtProtoPPrefix") == "":
                lploc = name + "LowPhoton/ProtoP"
            else:
                lploc = self.getProp("LowEtProtoPPrefix")
            addAlgs(
                caloLines,
                lp.neutralProtoSequence(protoPrefix=lploc, tracks=trackLocation),
            )

        if self.getProp("LowElectron"):
            le = CaloFutureProcessor(
                name + "LowElectron",
                TrackLocation=trackLocation,
                RecList=["Digits", "Clusters", "Electrons"],
                ExternalClusters="/Event/Rec/Calo/LowEtElectrons",
                ClusterPt=self.getProp("LowEt") * fac,
                ElectronPt=self.getProp("LowEt"),
                SkipNeutrals=True,
                ProtoOnDemand=pdod,
                MakeExternalClustersWithTag=tagLowE,
            )
            addAlgs(caloLines, le.caloFutureSequence(tracks=trackLocation))
            if self.getProp("LowEtProtoPPrefix") == "":
                leloc = name + "LowElectron/ProtoP"
            else:
                leloc = self.getProp("LowEtProtoPPrefix")
            addAlgs(
                caloLines,
                le.chargedProtoSequence(protoPrefix=leloc, tracks=trackLocation),
            )

        caloLines.IgnoreFilterPassed = True
        ## propagate the global properties
        if self.isPropertySet("OutputLevel"):
            setTheProperty(caloLines, "OutputLevel", self.getProp("OutputLevel"))

        ## define the sequencers
        if self.isPropertySet("Sequencer"):
            main = self.getProp("Sequencer")
            addAlgs(main, caloLines)


# =============================================================================
if "__main__" == __name__:
    print(__doc__)
    print(__author__)
