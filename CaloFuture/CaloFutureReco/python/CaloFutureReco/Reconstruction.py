#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## The major building blocks of CaloFuturerimeter Reconstruction
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
The major building blocks of CaloFuturerimeter Reconstruction
"""

from __future__ import print_function

# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
__all__ = ("clustersReco", "photonsReco", "mergedPi0sReco")
# =============================================================================
import logging

from CaloKernel.ConfUtils import (
    addAlgs,
    caloOnDemand,
    onDemand,
    prntCmp,
    setTheProperty,
)
from Configurables import GaudiSequencer
from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import GeV, MeV

_log = logging.getLogger("CaloFutureReco")


# =============================================================================
## prepare the digits for the recontruction
def digitsFutureReco(createADC):
    """
    Prepare the digits for the recontruction
    """

    from CaloFutureDAQ.CaloFutureDigits import caloDigits

    _log.warning(
        "CaloFutureReco.digitsFutureReco is deprecated, use CaloFutureDigitConf instead"
    )

    return caloDigits(createADC)


## ============================================================================
## define the recontruction of Ecal clusters
def clusterFutureReco(
    enableRecoOnDemand,
    clusterPt=0.0,
    fastReco=False,
    external="",
    makeTag=False,
    masksE=[],
    masksP=[],
    mergedPi0Pt=2 * GeV,
):
    """
    Define the recontruction of Ecal Clusters
    """

    from Configurables import (
        CaloFutureClusterCovarianceAlg,
        CaloFutureShowerOverlap,
        FutureCellularAutomatonAlg,
    )

    ## Define the sequencer
    seq = GaudiSequencer("ClusterFutureReco")

    seq.Members[:] = []

    clust = FutureCellularAutomatonAlg("FutureEcalClust")

    share = CaloFutureShowerOverlap("FutureEcalShare")
    share.SubClusterSelector = "FutureSubClusterSelectorTool/EcalClusterTag"
    share.SubClusterSelector.ConditionName = (
        "/world/DownstreamRegion/Ecal:EcalClusterMasks"
        if UseDD4Hep
        else "Conditions/Reco/Calo/EcalClusterMasks"
    )
    share.SubClusterSelector.CaloFutureCorrectionBase = (
        "CaloFutureCorrectionBase/DBAccessor"
    )
    share.SubClusterSelector.CaloFutureCorrectionBase.ConditionName = (
        share.EcalClusterTag.ConditionName
    )
    share.OverlapTool = "CaloFutureShowerOverlapTool/PhotonShowerOverlap"
    share.OverlapTool.Profile = (
        "/world/DownstreamRegion/Ecal:PhotonShowerProfile"
        if UseDD4Hep
        else "Conditions/Reco/Calo/PhotonShowerProfile"
    )
    share.OverlapTool.CaloFutureCorrectionBase = (
        "CaloFutureCorrectionBase/ShowerProfile"
    )
    share.OverlapTool.CaloFutureCorrectionBase.ConditionName = share.OverlapTool.Profile

    covar = CaloFutureClusterCovarianceAlg("FutureEcalCovar")
    covar.CovarianceTool = "FutureClusterCovarianceMatrixTool/EcalCovarTool"
    covar.CovarianceTool.ConditionName = (
        "/world/DownstreamRegion/Ecal:EcalCovariance"
        if UseDD4Hep
        else "Conditions/Reco/Calo/EcalCovariance"
    )
    covar.CovarianceTool.CorrectionBase = "CaloFutureCorrectionBase/DBAccessor"
    covar.CovarianceTool.CorrectionBase.ConditionName = (
        covar.CovarianceTool.ConditionName
    )
    covar.TaggerTool = "FutureSubClusterSelectorTool/EcalClusterTag"
    covar.TaggerTool.Profile = (
        "/world/DownstreamRegion/Ecal:EcalClusterMasks"
        if UseDD4Hep
        else "Conditions/Reco/Calo/EcalClusterMasks"
    )
    covar.TaggerTool.CaloFutureCorrectionBase = "CaloFutureCorrectionBase/DBAccessor"
    covar.TaggerTool.CaloFutureCorrectionBase.ConditionName = covar.TaggerTool.Profile

    if masksE != []:
        share.EnergyTags = masksE
        covar.EnergyTags = masksE
    if masksP != []:
        share.PositionTags = masksP
        covar.PositionTags = masksP

    if external != "":  # use non-default clusters
        share.InputData = external
        covar.InputData = external
        if makeTag != "":  # make the non-default clusters
            seq.Members += [clust]
            clust.OutputData = external
    else:
        seq.Members += [clust]  # make default clusters

    if clusterPt > 0:
        clust.ETcut = clusterPt
        clust.withET = True

    from Configurables import (
        LHCb__Converters__Calo__Cluster__v1__fromV2 as ClusterConverter,
    )

    convert = ClusterConverter("CaloClusterConverter")
    convert.InputClusters = covar.OutputData
    convert.OutputDigits = "Rec/Converted/Digits"
    convert.OutputClusters = str(convert.InputClusters).replace("Rec", "Rec/Converted")
    seq.Members += [share, covar, convert]

    ## printout
    _log.debug("Configure Ecal Cluster Reco Seq : %s" % (seq.name()))
    if enableRecoOnDemand:
        _log.info("ClusterFutureReco onDemand")

    ##
    return seq


# ============================================================================
## define the reconstruction of Single Photons and Electrons
def classifyClusPhotonElec(
    enableRecoOnDemand,
    useTracks=True,
    trackLocation="",
    neutralID=True,
    photonEt=50.0 * MeV,
    electronPt=50.0 * MeV,
    fastReco=False,
    external="",
    matchTrTypes=[],
):
    """
    Define reconstruction of Single Photons and Electrons
    """
    from Configurables import (
        CaloFutureECorrection,
        CaloFutureLCorrection,
        CaloFutureSCorrection,
        ClassifyPhotonElectronAlg,
    )

    ## build the sequence
    seq = GaudiSequencer("ClassifyClusters")
    seq.Members = []  # is this needed?

    # 1/ trackMatch from CaloFuturePIDs
    # chi2 tables needed for classification
    # FIX ME: make dependency explicit
    if useTracks:  # false to run calo standalone
        from CaloFuturePIDs.PIDs import trackMatch

        tm = trackMatch(
            enableRecoOnDemand, trackLocation, matchTrTypes, fastReco, external
        )
        addAlgs(seq, tm)

    # 2/ classify clusters in Single Photon and Electron hypotheses
    alg = ClassifyPhotonElectronAlg("ClassifyPhotonElectron")

    if external != "":
        alg.InputData = external

    alg.MinDigits = 2  # skip single-cell clusters
    alg.PhotonMinEt = photonEt
    alg.ElectrMinEt = electronPt
    alg.PhotonMinChi2 = 4
    alg.ElectrMaxChi2 = 25

    ## correction tools : E/S/L
    # tool configuration via condDB only (remove default configuration)
    ecor_phot = alg.addTool(CaloFutureECorrection, "ECorrectionPhoton")
    ecor_phot.ConditionName = (
        "/world/DownstreamRegion/Ecal:PhotonECorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/PhotonECorrection"
    )
    scor_phot = alg.addTool(CaloFutureSCorrection, "SCorrectionPhoton")
    scor_phot.ConditionName = (
        "/world/DownstreamRegion/Ecal:PhotonSCorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/PhotonSCorrection"
    )
    lcor_phot = alg.addTool(CaloFutureLCorrection, "LCorrectionPhoton")
    lcor_phot.ConditionName = (
        "/world/DownstreamRegion/Ecal:PhotonLCorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/PhotonLCorrection"
    )
    alg.PhotonCorrection = [ecor_phot, scor_phot, lcor_phot]

    ecor_ele = alg.addTool(CaloFutureECorrection, "ECorrectionElectron")
    ecor_ele.ConditionName = (
        "/world/DownstreamRegion/Ecal:ElectronECorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/ElectronECorrection"
    )
    scor_ele = alg.addTool(CaloFutureSCorrection, "SCorrectionElectron")
    scor_ele.ConditionName = (
        "/world/DownstreamRegion/Ecal:ElectronSCorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/ElectronSCorrection"
    )
    lcor_ele = alg.addTool(CaloFutureLCorrection, "LCorrectionElectron")
    lcor_ele.ConditionName = (
        "/world/DownstreamRegion/Ecal:ElectronLCorrection"
        if UseDD4Hep
        else "Conditions/Reco/Calo/ElectronLCorrection"
    )
    alg.ElectronCorrection = [ecor_ele, scor_ele, lcor_ele]

    # update the sequence
    addAlgs(seq, alg)

    ## printout
    _log.debug("Configure Classify Clusters Seq : %s" % seq.name())
    if enableRecoOnDemand:
        _log.info("ClassifyClusters onDemand")

    return seq


# =============================================================================
## define the reconstruction of Merged Pi0s Hypos
def mergedPi0FutureReco(
    enableRecoOnDemand,
    neutralID=True,
    useTracks=True,
    mergedPi0Pt=2 * GeV,
    fastReco=False,
    external="",
    masksE=[],
    masksP=[],
):
    """
    Define the recontruction of Merged Pi0s
    """

    from Configurables import (
        CaloFutureECorrection,
        CaloFutureLCorrection,
        CaloFutureMergedPi0,
        CaloFutureSCorrection,
    )

    # build the sequences
    seq = GaudiSequencer("MergedPi0FutureReco")

    ## Merged Pi0
    pi0 = CaloFutureMergedPi0("MergedPi0RecFuture")
    pi0.EcalCovariance = "FutureClusterCovarianceMatrixTool/EcalCovariance"
    pi0.EcalCovariance.ConditionName = (
        "/world/DownstreamRegion/Ecal:EcalCovariance"
        if UseDD4Hep
        else "Conditions/Reco/Calo/EcalCovariance"
    )
    pi0.EcalCovariance.CorrectionBase = "CaloFutureCorrectionBase/DBAccessor"
    pi0.EcalCovariance.CorrectionBase.ConditionName = pi0.EcalCovariance.ConditionName
    pi0.EcalClusterTag = "FutureSubClusterSelectorTool/EcalClusterTag"
    pi0.EcalClusterTag.ConditionName = (
        "/world/DownstreamRegion/Ecal:EcalClusterMasks"
        if UseDD4Hep
        else "Conditions/Reco/Calo/EcalClusterMasks"
    )
    pi0.EcalClusterTag.CaloFutureCorrectionBase = "CaloFutureCorrectionBase/DBAccessor"
    pi0.EcalClusterTag.CaloFutureCorrectionBase.ConditionName = (
        pi0.EcalClusterTag.ConditionName
    )
    pi0.SplitPhotonShowerOverlap = (
        "CaloFutureShowerOverlapTool/SplitPhotonShowerOverlap"
    )
    pi0.SplitPhotonShowerOverlap.Profile = (
        "/world/DownstreamRegion/Ecal:PhotonShowerProfile"
        if UseDD4Hep
        else "Conditions/Reco/Calo/PhotonShowerProfile"
    )
    pi0.SplitPhotonShowerOverlap.CaloFutureCorrectionBase = (
        "CaloFutureCorrectionBase/ShowerProfile"
    )
    pi0.SplitPhotonShowerOverlap.CaloFutureCorrectionBase.ConditionName = (
        pi0.SplitPhotonShowerOverlap.Profile
    )

    if masksE != []:
        pi0.EnergyTags = masksE
    if masksP != []:
        pi0.PositionTags = masksP

    if external != "":
        pi0.InputData = external

    pi0.EtCut = mergedPi0Pt

    ## MergedPi0 sequence

    ## 2/ SplitPhotons
    #    splitg = CaloFutureHypoAlg('PhotonFromMergedRec')
    #    splitg.HypoType = "SplitPhotons";
    #    splitg.PropertiesPrint = False

    ## Pi0 correction tools

    # tool configuration via condDB only (remove default configuration)
    pi0.PhotonTools = [
        pi0.addTool(CaloFutureECorrection, "ECorrectionPi0"),
        pi0.addTool(CaloFutureSCorrection, "SCorrectionPi0"),
        pi0.addTool(CaloFutureLCorrection, "LCorrectionPi0"),
    ]

    from Configurables import (
        LHCb__Converters__Calo__Cluster__v1__fromV2 as ClusterConverter,
    )

    convert = ClusterConverter("CaloSplitClusterConverter")
    convert.InputClusters = pi0.SplitClusters
    convert.OutputClusters = str(convert.InputClusters).replace("Rec", "Rec/Converted")
    convert.OutputDigits = "Rec/Calo/Converted/SplitClusterDigits"

    seq.Members = [pi0, convert]

    ## 3/ (PhotonFrom)MergedID
    if neutralID:
        from CaloFuturePIDs.PIDs import MergedID, PhotonFromMergedID

        idSeq = GaudiSequencer("MergedIDSeq")
        mID = MergedID(enableRecoOnDemand, useTracks)
        pmID = PhotonFromMergedID(enableRecoOnDemand, useTracks)
        idSeq.Members = [mID, pmID]
        addAlgs(seq, idSeq)

    ## printout
    _log.debug("Configure MergedPi0 Reco Seq : %s" % (seq.name()))
    if enableRecoOnDemand:
        _log.info("MergedPi0FutureReco onDemand")

    return seq


# =============================================================================
if "__main__" == __name__:
    print(__doc__)
