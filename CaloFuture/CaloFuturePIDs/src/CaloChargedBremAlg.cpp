/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "CaloChargedInfoUtils.h"
#include "GaudiKernel/IFileAccess.h"
#include "LHCbAlgs/Transformer.h"

/** @class CaloChargedBremAlg CaloChargedBremAlg.h
 *
 *  builds a SoA container of brem info related to tracks needed
 *  to be saved for ChargedBasic (for both momentum recovery and global charged PID)
 *
 */

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Event::Calo;
  using namespace LHCb::Calo::ChargedInfoUtils;

  // main class
  class CaloChargedBremAlg
      : public Algorithm::Transformer<BremInfo( TracksInBrem const&, TracksBremEnergy const&, Tracks2Brems const& )> {
  public:
    // standard constructor
    CaloChargedBremAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    BremInfo operator()( TracksInBrem const&, TracksBremEnergy const&, Tracks2Brems const& ) const override;

  private:
    // bremsstrahlung recovery options
    Gaudi::Property<BremMethod> m_bremmethod{ this, "BremMethod", BremMethod::Mixed, "Brem recovery method" };
    Gaudi::Property<float>      m_bremchi2_max{
        this, "BremChi2Max", 6., "Maximum chi2 of cluster for track to use brem recovery (any 'BremMethod')" };
    Gaudi::Property<float> m_bremchi2_thr{
        this, "BremChi2Threshold", 1.0,
        "Threshold of chi2 of cluster when to switch to track-based brem recovery for 'BremMethod::Mixed'" };

    // DLL parametrization locations
    Gaudi::Property<std::string> m_histo_location{ this, "HistLocation", "",
                                                   "Histogram location for histoSvc/ParamFiles" };
    Gaudi::Property<std::string> m_paramfiles_location{
        this, "ParamFilesLocation", "paramfile://data/CaloPID/DLLs_parametrization_histograms_122022.root",
        "Location of ROOT file for charged calo PID DLL histograms" };

    ServiceHandle<IFileAccess> m_file{ this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents" };
    // DLL histograms
    std::unique_ptr<DLLs_3D const> m_dlls;
  };

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  CaloChargedBremAlg::CaloChargedBremAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     { KeyValue( "InBrem", "" ), KeyValue( "BremEnergy", "" ), KeyValue( "Brems", "" ) },
                     // Output
                     { KeyValue( "Output", "" ) } ) {}

  // ============================================================================
  // Initialization of algorithm / tool
  // ============================================================================
  StatusCode CaloChargedBremAlg::initialize() {
    return Transformer::initialize().andThen( [&] {
      // check brem config
      info() << "brem reco method: " << m_bremmethod.value();
      switch ( m_bremmethod ) {
      case BremMethod::Cluster:
        setProperty( "BremChi2Threshold", m_bremchi2_max.value() ).ignore();
        info() << "; using max match chi2 (decision to add brem) at " << m_bremchi2_max.value() << endmsg;
        break;
      case BremMethod::TrackBased:
        setProperty( "BremChi2Threshold", 0.f ).ignore();
        info() << "; using max match chi2 (decision to add brem) at " << m_bremchi2_max.value() << endmsg;
        break;
      case BremMethod::Mixed:
        assert( m_bremchi2_max >= m_bremchi2_thr );
        info() << "; using max match chi2 (decision to add brem) at " << m_bremchi2_max.value()
               << "; switch to track-based above " << m_bremchi2_thr.value() << endmsg;
        break;
      case BremMethod::LooseTrackBased:
        info() << "; no requirements on match chi2" << endmsg;
        break;
      default:
        throw GaudiException( "no brem reco method given", "LHCb::Event::Calo::v2::BremMethod", StatusCode::FAILURE );
      }
      // get DLL histograms from ParamFiles
      std::string hLoc = m_histo_location.value();
      m_file.retrieve().orThrow( "Could not obtain IFileAccess instance" );
      info() << "getting DLL histograms from " << m_paramfiles_location.value() << endmsg;
      const auto buffer = m_file->read( m_paramfiles_location );
      if ( !buffer ) throw GaudiException( "Failed to obtain histogram file content", __func__, StatusCode::FAILURE );
      m_dlls = std::make_unique<DLLs_3D>( TMemFile::ZeroCopyView_t{ buffer->data(), buffer->size() }, hLoc,
                                          getHistoMap3D() );
    } );
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  BremInfo CaloChargedBremAlg::operator()( TracksInBrem const& inbrem, TracksBremEnergy const& breme,
                                           Tracks2Brems const& brems ) const {
    // retrieve tracks from relation table(s)
    auto tracks    = inbrem.from();
    auto tracktype = tracks->type();

    // declare output
    BremInfo output( tracks->zipIdentifier(), tracks->get_allocator() );
    output.reserve( tracks->size() );

    // build views with respect to tracks (so one can loop or zip properly)
    auto rel_inbrem = inbrem.buildView();
    auto rel_breme  = breme.buildView();
    auto rel_brems  = brems.buildFromView();

    // loop over input tracks
    // note: for now scalar access to all views via track index
    //       as in large zip, ambiguities appear otherwise
    for ( auto const& track : tracks->scalar() ) {
      auto idx = track.indices().cast();
      auto pid = output.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

      // cluster-based brem recovery
      auto brem_info = getBestBrem<BremMatch, BremDeltaX, BremBendingCorr>( rel_brems.scalar()[idx] );
      auto brem_chi2 = brem_info ? brem_info->match.chi2 : -1.f;
      auto brem_dx   = brem_info ? brem_info->dx : 0.f;
      auto brem_bc   = brem_info ? brem_info->bc : 1.f;
      pid.field<ChargedInfoTag::BremHypoID>().set( brem_info ? brem_info->match.cellid.all() : 0 );
      pid.field<ChargedInfoTag::BremHypoMatch>().set( brem_chi2 );
      pid.field<ChargedInfoTag::BremHypoEnergy>().set( brem_info ? brem_info->energy : -1.f );
      pid.field<ChargedInfoTag::BremHypoDeltaX>().set( brem_dx );
      pid.field<ChargedInfoTag::BremBendingCorrection>().set( brem_bc );

      // track-based brem recovery
      auto brem_e_trackbased = getRelationValue<BremEnergy, float>( rel_breme.scalar()[idx] );
      pid.field<ChargedInfoTag::BremTrackBasedEnergy>().set( brem_e_trackbased ? *brem_e_trackbased : -1.f );

      // main brem momentum recovery info (combined info)
      auto brem_e = getBremEnergy( m_bremmethod, brem_info, brem_e_trackbased, m_bremchi2_thr, m_bremchi2_max );
      pid.field<ChargedInfoTag::BremEnergy>().set( brem_e ? *brem_e : 0.f );

      // acceptance
      auto comb_brem_acc = getBremEncoding( rel_inbrem.scalar()[idx].hasRelation().cast(), brem_e > 0.f );
      pid.field<ChargedInfoTag::BremAcceptance>().set( comb_brem_acc );

      // DLL variables, obtained from base variables + DLL hists from ParamFiles
      auto track_logp = -LHCb::Math::fast_log( abs( track.qOverP( SL::FirstMeasurement ).cast() ) );
      auto dlle_brem  = getDLL_3D<DLLType::BremPIDe>( *m_dlls, tracktype, brem_chi2, brem_dx, track_logp );
      pid.field<ChargedInfoTag::BremPIDe>().set( dlle_brem );
    }

    return output;
  }

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::CaloChargedBremAlg, "CaloChargedBremAlg" )
