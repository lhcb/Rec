/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GaudiException.h"
#include "LHCbAlgs/Transformer.h"
#include "Magnet/DeMagnet.h"
#include "SelectiveMatchUtils.h"

#include <yaml-cpp/yaml.h>

/** @class SelectiveElectronMatchAlg SelectiveElectronMatchAlg.h
 *
 *  Matches tracks with local electron hypos in and around calo cell
 *  corresponding to track extrapolation from previous cluster-track
 *  matching.
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveElectronMatchAlg
 *  based on SelectiveTrackMatchAlg and CaloFutureTrackMatch
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Calo::SelectiveMatchUtils;

  using CaloObjects = CaloHypos;
  using OutputData  = Tracks2Electrons;

  // local helper functions for class
  namespace {

    // conditions for x-correction
    class electronXcorrections {
    private:
      std::array<double, 4> alphaPOut;
      std::array<double, 4> alphaNOut;
      std::array<double, 4> alphaPMid;
      std::array<double, 4> alphaNMid;
      std::array<double, 4> alphaPInn;
      std::array<double, 4> alphaNInn;

    public:
      electronXcorrections() = default; // needed by DD4hep even if unused !
      electronXcorrections( YAML::Node const& c )
          : alphaPOut{ c["alphaPOut"].as<std::array<double, 4>>() }
          , alphaNOut{ c["alphaNOut"].as<std::array<double, 4>>() }
          , alphaPMid{ c["alphaPMid"].as<std::array<double, 4>>() }
          , alphaNMid{ c["alphaNMid"].as<std::array<double, 4>>() }
          , alphaPInn{ c["alphaPInn"].as<std::array<double, 4>>() }
          , alphaNInn{ c["alphaNInn"].as<std::array<double, 4>>() } {}

      const std::array<double, 4>& operator()( const int area, const int charge, const int polarity ) const {
        bool qpolarity = charge * polarity > 0;
        switch ( area ) {
        case 0: // Outer  ECAL
          return qpolarity ? alphaPOut : alphaNOut;
        case 1: // Middle ECAL
          return qpolarity ? alphaPMid : alphaNMid;
        case 2: // Inner  ECAL
          return qpolarity ? alphaPInn : alphaNInn;
        default:
          throw GaudiException( "requested impossible Calo area", __func__, StatusCode::FAILURE );
        }
      }
    };

    // to apply x-correction to track state at calo
    void applyXCorrection( double& x, const std::array<double, 4>& alphas, const double& momentum ) {
      if ( !alphas.empty() ) {
        // for expansion in p, 1, 1/p, 1/p^2 (all in GeV)
        double expansion = momentum / Gaudi::Units::GeV;
        double invmom    = 1. / expansion;
        for ( const auto alpha : alphas ) {
          x += alpha * expansion;
          expansion *= invmom;
        }
      }
    }

    // obtain track position and momentum covariance
    void get3DTrackCovariance( Match3D::Matrix& covariance, LHCb::State const& state ) {
      auto const qoverp   = state.qOverP();
      auto const statecov = state.covariance();
      double     f        = ( ( qoverp > 0. ) ? -1. : 1. ) / qoverp / qoverp;
      covariance( 0, 0 )  = statecov( 0, 0 );
      covariance( 0, 1 )  = statecov( 0, 1 );
      covariance( 1, 1 )  = statecov( 1, 1 );
      covariance( 0, 2 )  = statecov( 0, 4 ) * f;
      covariance( 1, 2 )  = statecov( 1, 4 ) * f;
      covariance( 2, 2 )  = statecov( 4, 4 ) * f * f;
    }

  } // namespace

  // main class
  class SelectiveElectronMatchAlg
      : public Algorithm::Transformer<
            OutputData( DeCalorimeter const&, CaloObjects const&, Tracks2Clusters const&, electronXcorrections const&,
                        DeMagnet const& ),
            Algorithm::Traits::usesConditions<DeCalorimeter, electronXcorrections, DeMagnet>> {
  public:
    // standard constructor
    SelectiveElectronMatchAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    OutputData operator()( DeCalorimeter const&, const CaloObjects&, Tracks2Clusters const&,
                           electronXcorrections const&, DeMagnet const& ) const override;

  private:
    // properties
    Gaudi::Property<bool> m_addenergy{ this, "AddEnergy", true, "add energy / momentum measurement in chi2" };
    Gaudi::Property<bool> m_onesided{ this, "OneSidedE", true,
                                      "use only lower sided E/p (to avoid cluster overlap issues)" };
    Gaudi::Property<CaloPlane::Plane> m_caloplane{
        this, "CaloPlane", CaloPlane::ShowerMax,
        "Plane at the calorimeter of where to extrapolate to (ShowerMax for electrons)" };
    Gaudi::Property<float>       m_threshold{ this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value" };
    Gaudi::Property<std::string> m_xcorrectionlocation{ this, "XCorrectionLocation",
#ifdef USE_DD4HEP
                                                        "/world/DownstreamRegion/Ecal:ElectronXCorrection",
#else
                                                        "/dd/Conditions/ParticleID/Calo/ElectronXCorrection",
#endif
                                                        "location in CondDB of x-corrections for electrons" };
    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{ this, "#match failure" };
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{ this, "#links in table" };
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{ this, "#above threshold" };
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{ this, "average chi2" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{ this, "Failed to generate index" };
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveElectronMatchAlg, "SelectiveElectronMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveElectronMatchAlg::SelectiveElectronMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     { KeyValue( "Detector", { CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) } ),
                       KeyValue( "InputHypos", { LHCb::CaloFutureAlgUtils::CaloFutureHypoLocation( "Electrons" ) } ),
                       KeyValue( "InputTracks2Clusters", "" ),
#ifdef USE_DD4HEP
                       KeyValue( "XCorrections", { "/world:AlgorithmSpecific-" + name + "-xcorrections" } ),
#else
                       KeyValue( "XCorrections", { "AlgorithmSpecific-" + name + "-xcorrections" } ),
#endif
                       KeyValue( "Magnet", LHCb::Det::Magnet::det_path ) },
                     // Outputs
                     { KeyValue( "Output", "" ) } ) {
  }

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveElectronMatchAlg::initialize() {
    auto sc = Transformer::initialize().andThen( [&] {
      addConditionDerivation<electronXcorrections( YAML::Node const& )>( { m_xcorrectionlocation.value() },
                                                                         inputLocation<electronXcorrections>() );
    } );
    return sc;
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  OutputData SelectiveElectronMatchAlg::operator()( DeCalorimeter const& calo, CaloObjects const& caloobjects,
                                                    Tracks2Clusters const&      trackswithclusters,
                                                    electronXcorrections const& xcorrection,
                                                    DeMagnet const&             magnet ) const {
    // declare output
    OutputData output_table( trackswithclusters.from(), &caloobjects );
    output_table.reserve( trackswithclusters.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_loc = extrapolation_stateloc( *trackswithclusters.from() );
    if ( !state_loc.has_value() ) {
      throw GaudiException( "Not a valid track type for this calo energy type.", "LHCb::Event::Enum::Track::Type",
                            StatusCode::FAILURE );
    }

    // obtain the right calo objects
    const auto& hypoclusters = caloobjects.clusters();
    auto        hypoindex    = hypoclusters.index();
    if ( !hypoindex || hypoclusters.size() != caloobjects.size() ) {
      ++m_not_unique;
      return output_table;
    }

    // tools for accessing track - cluster relations
    auto trackrelations = trackswithclusters.buildFromView();

    // miscellaneous info
    const int  polarity      = magnet.isDown() ? -1 : 1;
    const auto plane_at_calo = calo.plane( m_caloplane );

    // declare what is needed in the loop
    LHCb::State     state;
    Match3D::Vector track_pos, track_pos_local, calo_pos, diff_pos;
    Match3D::Matrix track_cov, comb_cov;

    // main loop over tracks
    for ( const auto& trackrel : trackrelations.scalar() ) {
      // check if track has a relation
      if ( !trackrel.numRelations().cast() ) continue;

      // get state at calo plane
      auto const track     = trackrel.relation( 0 ).from();
      auto const ref_state = track.state( state_loc.value() );
      if ( !propagateToCaloWithCov( state, ref_state, plane_at_calo ) ) continue;

      // obtain relevant info for matching from track
      track_pos            = { state.x(), state.y(), state.p() };
      track_pos_local( 2 ) = track_pos( 2 );
      get3DTrackCovariance( track_cov, state );

      // obtain electron x-correction
      const auto  area   = getAreaForTrack( state.position(), calo );
      const auto& alphas = xcorrection( area, state.qOverP() > 0 ? 1 : -1, polarity );
      applyXCorrection( track_pos( 0 ), alphas, track_pos( 2 ) );

      // loop over local calo objects and calculate chi2
      for ( int i = 0; i < trackrel.numRelations().hmax( trackrel.loop_mask() ); i++ ) {
        if ( !trackrel.hasRelation( i ).cast() ) continue;
        // find calo obj ('cluster' storing info from hypo)
        auto const caloobj = hypoindex.find( trackrel.relation( i ).to().cellID() );
        if ( caloobj == hypoindex.end() ) continue;

        // adapt track state to calo obj location
        const auto delta_z   = caloobj->position().z() - state.z();
        track_pos_local( 0 ) = track_pos( 0 ) + ( state.tx() * delta_z );
        track_pos_local( 1 ) = track_pos( 1 ) + ( state.ty() * delta_z );

        // obtain calo obj info
        calo_pos = { caloobj->position().x(), caloobj->position().y(), caloobj->energy() };
        comb_cov = track_cov + caloobj->covariance();
        diff_pos = calo_pos - track_pos_local;

        // adapt to options
        if ( !m_addenergy || ( m_onesided && diff_pos( 2 ) > 0. ) ) { diff_pos( 2 ) = 0.; }

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        const auto chi2 = ROOT::Math::Similarity( diff_pos, comb_cov );

        // check if it has proper value
        if ( chi2 > m_threshold ) {
          m_nOverflow += 1;
          continue;
        }

        // only now push proper result
        output_table.emplace_back<SIMDWrapper::InstructionSet::Scalar>().set( track.indices(), caloobj->indices(),
                                                                              chi2 );
      }
    }

    // monitor statistics: number of links and average chi2
    const auto nLinks = output_table.size();
    m_nLinks += nLinks;
    if ( nLinks > 0 ) {
      auto chi2 = m_chi2.buffer();
      for ( auto const& proxy : output_table.scalar() ) { chi2 += proxy.get<ElectronMatch>().cast() / nLinks; }
    }

    return output_table;
  }

} // namespace LHCb::Calo
