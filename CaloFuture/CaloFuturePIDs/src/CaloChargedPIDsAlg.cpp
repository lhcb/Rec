/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "CaloChargedInfoUtils.h"
#include "GaudiKernel/IFileAccess.h"
#include "LHCbAlgs/Transformer.h"

/** @class CaloChargedPIDsAlg CaloChargedPIDsAlg.h
 *
 *  builds a SoA container of calo info related to tracks needed
 *  to be saved for ChargedBasic (in particular for global charged PID)
 *
 */

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Event::Calo;
  using namespace LHCb::Calo::ChargedInfoUtils;

  // main class
  class CaloChargedPIDsAlg : public Algorithm::Transformer<ChargedPID(
                                 TracksInEcal const&, TracksInHcal const&, TracksHcalEnergy const&,
                                 Tracks2Clusters const&, Tracks2Electrons const&, TracksElectronShower const& )> {
  public:
    // standard constructor
    CaloChargedPIDsAlg( const std::string& name, ISvcLocator* pSvc );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    ChargedPID operator()( TracksInEcal const&, TracksInHcal const&, TracksHcalEnergy const&, Tracks2Clusters const&,
                           Tracks2Electrons const&, TracksElectronShower const& ) const override;

  private:
    // DLL parametrization locations
    Gaudi::Property<std::string> m_histo_location{ this, "HistLocation", "",
                                                   "Histogram location for histoSvc/ParamFiles" };
    Gaudi::Property<std::string> m_paramfiles_location{
        this, "ParamFilesLocation", "paramfile://data/CaloPID/DLLs_parametrization_histograms_122022.root",
        "Location of ROOT file for charged calo PID DLL histograms" };

    ServiceHandle<IFileAccess> m_file{ this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents" };
    // DLL histograms
    std::unique_ptr<DLLs_2D const> m_dlls_2D;
    std::unique_ptr<DLLs_3D const> m_dlls_3D;
  };

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  CaloChargedPIDsAlg::CaloChargedPIDsAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     { KeyValue( "InEcal", "" ), KeyValue( "InHcal", "" ), KeyValue( "HcalEnergy", "" ),
                       KeyValue( "Tracks2Clusters", "" ), KeyValue( "Tracks2Electrons", "" ),
                       KeyValue( "ElectronShower", "" ) },
                     // Output
                     { KeyValue( "Output", "" ) } ) {}

  // ============================================================================
  // Initialization of algorithm / tool
  // ============================================================================
  StatusCode CaloChargedPIDsAlg::initialize() {
    return Transformer::initialize().andThen( [&] {
      // get DLL histograms from ParamFiles
      std::string hLoc = m_histo_location.value();
      m_file.retrieve().orThrow( "Could not obtain IFileAccess instance" );
      info() << "getting DLL histograms from " << m_paramfiles_location.value() << endmsg;
      const auto buffer = m_file->read( m_paramfiles_location );
      if ( !buffer ) throw GaudiException( "Failed to obtain histogram file content", __func__, StatusCode::FAILURE );
      auto f    = TMemFile::ZeroCopyView_t{ buffer->data(), buffer->size() };
      m_dlls_2D = std::make_unique<DLLs_2D>( f, hLoc, getHistoMap2D() );
      m_dlls_3D = std::make_unique<DLLs_3D>( f, hLoc, getHistoMap3D() );
    } );
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  ChargedPID CaloChargedPIDsAlg::operator()( TracksInEcal const& inecal, TracksInHcal const& inhcal,
                                             TracksHcalEnergy const& hcale, Tracks2Clusters const& clusters,
                                             Tracks2Electrons const&     tracks2els,
                                             TracksElectronShower const& eshower ) const {
    // retrieve tracks from relation table(s)
    auto tracks    = inecal.from();
    auto tracktype = tracks->type();

    // declare output
    ChargedPID output( tracks->zipIdentifier(), tracks->get_allocator() );
    output.reserve( tracks->size() );

    // build views with respect to tracks (so one can loop or zip properly)
    auto rel_inecal     = inecal.buildView();
    auto rel_inhcal     = inhcal.buildView();
    auto rel_hcale      = hcale.buildView();
    auto rel_clusters   = clusters.buildFromView();
    auto rel_tracks2els = tracks2els.buildFromView();
    auto rel_eshower    = eshower.buildView();

    // loop over input tracks
    // note: for now scalar access to all views via track index
    //       as in large zip, ambiguities appear otherwise
    for ( auto const& track : tracks->scalar() ) {
      auto idx = track.indices().cast();
      auto pid = output.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

      // acceptances encoded in one int
      auto inecal   = rel_inecal.scalar()[idx].hasRelation().cast();
      auto inhcal   = rel_inhcal.scalar()[idx].hasRelation().cast();
      auto comb_acc = LHCb::Event::Calo::getAccEncoding( inecal, inhcal );
      pid.field<ChargedInfoTag::CaloAcceptance>().set( comb_acc );

      // generic cluster
      auto cl_bestmatch = getBestMatch<ClusterMatch>( rel_clusters.scalar()[idx] );
      pid.field<ChargedInfoTag::ClusterID>().set( cl_bestmatch ? cl_bestmatch->cellid.all() : 0 );
      pid.field<ChargedInfoTag::ClusterMatch>().set( cl_bestmatch ? cl_bestmatch->chi2 : -1.f );

      // electron-hypo cluster
      auto el_proxy     = rel_tracks2els.scalar()[idx];
      auto el_bestmatch = getBestMatchWithEnergy<ElectronMatch>( el_proxy );
      pid.field<ChargedInfoTag::ElectronID>().set( el_bestmatch ? el_bestmatch->match.cellid.all() : 0 );
      pid.field<ChargedInfoTag::ElectronMatch>().set( el_bestmatch ? el_bestmatch->match.chi2 : -1.f );
      pid.field<ChargedInfoTag::ElectronEnergy>().set( el_bestmatch ? el_bestmatch->energy : -1.f );

      // track-based hcal info
      auto track_invp = abs( track.qOverP( SL::LastMeasurement ).cast() );
      auto hcale      = getRelationValue<HcalEnergy, float>( rel_hcale.scalar()[idx] );
      auto hcaleop    = hcale ? *hcale * track_invp : -1.f;
      pid.field<ChargedInfoTag::HcalEoP>().set( hcaleop );

      // track-based ecal info
      auto eshower_proxy   = rel_eshower.scalar()[idx];
      auto eshower_eop     = getRelationValue<ElectronShowerEoP, float>( eshower_proxy );
      auto eshower_dll     = getRelationValue<ElectronShowerDLL, float>( eshower_proxy );
      auto eshower_eop_val = eshower_eop ? *eshower_eop : -1.f;
      auto eshower_dll_val = eshower_dll ? *eshower_dll : 0.f;
      pid.field<ChargedInfoTag::ElectronShowerEoP>().set( eshower_eop_val );
      pid.field<ChargedInfoTag::ElectronShowerDLL>().set( eshower_dll_val );

      // DLL variables, obtained from base variables + DLL hists from ParamFiles
      auto track_logp = -LHCb::Math::fast_log( abs( track.qOverP( SL::FirstMeasurement ).cast() ) );
      auto dlle_ecal =
          inecal ? getDLL_3D<DLLType::EcalPIDe>( *m_dlls_3D, tracktype, eshower_dll_val, eshower_eop_val, track_logp )
                 : 0.f;
      auto dlle_hcal = inhcal ? getDLL_2D<DLLType::HcalPIDe>( *m_dlls_2D, tracktype, hcaleop, track_logp ) : 0.f;
      auto dllm_ecal =
          inecal ? getDLL_2D<DLLType::EcalPIDm>( *m_dlls_2D, tracktype, eshower_eop_val, track_logp ) : 0.f;
      auto dllm_hcal = inhcal ? getDLL_2D<DLLType::HcalPIDm>( *m_dlls_2D, tracktype, hcaleop, track_logp ) : 0.f;
      pid.field<ChargedInfoTag::EcalPIDe>().set( dlle_ecal );
      pid.field<ChargedInfoTag::HcalPIDe>().set( dlle_hcal );
      pid.field<ChargedInfoTag::EcalPIDmu>().set( dllm_ecal );
      pid.field<ChargedInfoTag::HcalPIDmu>().set( dllm_hcal );
    }

    return output;
  }

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::CaloChargedPIDsAlg, "CaloChargedPIDsAlg" )
