/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Event/SOACollection.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"

/** @class TrackToCaloEnergyAlg TrackToCaloEnergyAlg.h
 *
 *  Algorithms that calculates energy in calorimeter cells that intersect with a track in calo acceptance (v3):
 *   - TrackToEcalEnergyAlg
 *   - TrackToHcalEnergyAlg
 *
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class TrackToCaloEnergyAlg
 *
 */

namespace LHCb::Calo {

  namespace {

    using namespace LHCb::Calo::TrackUtils;

    // energy of cells from track - cell intersection
    float getEnergy( LHCb::StateVector const& state, CaloDigits const& digits, DeCalorimeter const& calo,
                     float const deltaZ, int const nPlanes ) {
      float energy = 0.f;
      // cellid bookkeeping
      auto prev_cellid = LHCb::Detector::Calo::CellID();
      // scan at different planes in the calo
      auto       position = state.position();
      auto const slopes   = state.slopes();
      for ( int i = 0; i < nPlanes; ++i ) {
        const auto cell = calo.Cell_( position );
        if ( !cell || !cell->valid() ) continue;
        // check if it is the same as last one
        auto cellid = cell->cellID();
        if ( prev_cellid == cellid ) continue;
        // if not, add energy
        const auto digit = digits( cellid );
        if ( digit ) energy += digit->energy();
        // next position
        position += deltaZ * slopes;
        prev_cellid = cellid;
      }
      return energy;
    }

  } // namespace

  // main class
  template <typename TracksInCalo, typename OutputData>
  class TrackToCaloEnergyAlg
      : public Algorithm::Transformer<OutputData( TracksInCalo const&, CaloDigits const&, DeCalorimeter const& ),
                                      Algorithm::Traits::usesConditions<DeCalorimeter>> {
  public:
    // standard constructor
    TrackToCaloEnergyAlg( const std::string& name, ISvcLocator* pSvc );

    // main function/operator
    OutputData operator()( TracksInCalo const&, CaloDigits const&, DeCalorimeter const& ) const override;

  private:
    // properties
    Gaudi::Property<int> m_nplanes{
        this, "nCaloPlanes", 7,
        "Number of calorimeter planes where track state - calo cell intersection if performed." };

    // statistics
    mutable Gaudi::Accumulators::StatCounter<float> m_energy{ this, "energy (calo) associated to track" };
  };

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  template <typename TracksInCalo, typename OutputData>
  TrackToCaloEnergyAlg<TracksInCalo, OutputData>::TrackToCaloEnergyAlg( const std::string& name, ISvcLocator* pSvc )
      : TrackToCaloEnergyAlg<TracksInCalo, OutputData>::Transformer(
            name, pSvc,
            // Inputs
            { typename TrackToCaloEnergyAlg<TracksInCalo, OutputData>::KeyValue( "TracksInCalo", "" ),
              typename TrackToCaloEnergyAlg<TracksInCalo, OutputData>::KeyValue( "Digits", "" ),
              typename TrackToCaloEnergyAlg<TracksInCalo, OutputData>::KeyValue( "Calorimeter", "" ) },
            // Output
            { typename TrackToCaloEnergyAlg<TracksInCalo, OutputData>::KeyValue( "Output", "" ) } ) {}

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  template <typename TracksInCalo, typename OutputData>
  OutputData TrackToCaloEnergyAlg<TracksInCalo, OutputData>::operator()( TracksInCalo const&  tracksincalo,
                                                                         CaloDigits const&    digits,
                                                                         DeCalorimeter const& calo ) const {
    // declare output
    OutputData output_table( tracksincalo.from() );
    output_table.reserve( tracksincalo.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_loc = extrapolation_stateloc( *tracksincalo.from() );
    if ( !state_loc.has_value() ) {
      throw GaudiException( "Not a valid track type for this calo energy type.", "LHCb::Event::Enum::Track::Type",
                            StatusCode::FAILURE );
    }

    // miscellaneous info
    LHCb::StateVector calo_state;
    auto const        calo_front = calo.plane( CaloPlane::Front );
    float const       calo_dz    = calo.zSize() / ( m_nplanes > 1 ? m_nplanes - 1 : m_nplanes.value() );

    // loop over input tracks
    for ( auto const& trackincalo : tracksincalo.scalar() ) {
      auto track     = trackincalo.from();
      auto ref_state = track.state( state_loc.value() );
      if ( !propagateToCalo( calo_state, ref_state, calo_front ) ) continue;
      float energy = getEnergy( calo_state, digits, calo, calo_dz, m_nplanes );
      // save result for this index in tracks
      output_table.add( track, energy );
      // statistics
      m_energy += energy;
    }

    return output_table;
  }

  // ========================= SPECIFIC IMPLEMENTATION ==========================

  // ============================================================================
  // instances of track to calo energy for: ecal, hcal, respectively
  // ============================================================================
  struct TrackToEcalEnergyAlg : TrackToCaloEnergyAlg<TracksInEcal, TracksEcalEnergy> {
    // constructor
    TrackToEcalEnergyAlg( const std::string& name, ISvcLocator* pSvc )
        : TrackToCaloEnergyAlg<TracksInEcal, TracksEcalEnergy>( name, pSvc ) {
      updateHandleLocation( *this, "Calorimeter", CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) );
      updateHandleLocation( *this, "Digits", CaloDigitLocation::Ecal );
    }
  };

  struct TrackToHcalEnergyAlg : TrackToCaloEnergyAlg<TracksInHcal, TracksHcalEnergy> {
    // constructor
    TrackToHcalEnergyAlg( const std::string& name, ISvcLocator* pSvc )
        : TrackToCaloEnergyAlg<TracksInHcal, TracksHcalEnergy>( name, pSvc ) {
      updateHandleLocation( *this, "Calorimeter", CaloFutureAlgUtils::DeCaloFutureLocation( "Hcal" ) );
      updateHandleLocation( *this, "Digits", CaloDigitLocation::Hcal );
      setProperty( "nPlanes", 5 ).ignore();
    }
  };

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::TrackToEcalEnergyAlg, "TrackToEcalEnergyAlg" )
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::TrackToHcalEnergyAlg, "TrackToHcalEnergyAlg" )
