/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#pragma once
#include "AIDA/IHistogram2D.h"
#include "AIDA/IHistogram3D.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Event/CaloChargedInfo.h"
#include "LHCbMath/FastMaths.h"
#include "TH2F.h"
#include "TH3F.h"
#include <cfloat>
#include <tuple>

namespace LHCb::Calo {

  namespace ChargedInfoUtils {

    // helper classes
    namespace {

      using BremMethod = LHCb::Event::Calo::v2::BremMethod;
      using CellID     = LHCb::Detector::Calo::CellID;

      struct Match {
        int    index{ -1 };
        float  chi2{ FLT_MAX };
        CellID cellid{};
      };

      // helper function to obtain obj from relation table with minimum chi2
      template <typename ProxyType, typename Weight>
      inline std::optional<Match> getMinChiSqMatch( ProxyType const& proxy ) {
        auto match = Match();
        for ( int i = 0; i < proxy.numRelations().hmax( proxy.loop_mask() ); i++ ) {
          if ( !proxy.hasRelation( i ).cast() ) continue;
          auto chi2 = proxy.relation( i ).template get<Weight>().cast();
          if ( chi2 < match.chi2 ) {
            match.index = i;
            match.chi2  = chi2;
          }
        }
        if ( match.index == -1 ) return std::nullopt;
        return match;
      }

      struct EnergyMatch {
        Match match;
        float energy;
      };

      struct EnergyDeltaXMatch {
        Match match;
        float energy;
        float dx;
        float bc;
      };

      // helpers for DLL getter
      auto clip = []( float val, auto const* axis, float eps = 1e-5 ) {
        auto val_min = axis->GetBinCenter( 1 ) + eps;
        auto val_max = axis->GetBinCenter( axis->GetNbins() ) - eps;
        return ( val < val_min ) ? val_min : ( ( val > val_max ) ? val_max : val );
      };

    } // namespace

    // value obtainer from 1D relation table
    template <typename RelVType, typename RetType, typename ProxyType>
    inline std::optional<RetType> getRelationValue( ProxyType const& proxy ) {
      return proxy.hasRelation().cast() ? std::optional<RetType>( proxy.relation().template get<RelVType>().cast() )
                                        : std::nullopt;
    }

    // to obtain cluster/hypo with minimum chi2
    template <typename Chi2Type, typename ProxyType>
    inline std::optional<Match> getBestMatch( ProxyType const& proxy ) {
      auto match = getMinChiSqMatch<ProxyType, Chi2Type>( proxy );
      if ( !match.has_value() ) return match;
      auto cluster  = proxy.relation( match->index ).to();
      match->index  = cluster.indices().cast();
      match->cellid = cluster.cellID();
      return match;
    }

    // above but with energy of cluster/hypo as well
    template <typename Chi2Type, typename ProxyType>
    inline std::optional<EnergyMatch> getBestMatchWithEnergy( ProxyType const& proxy ) {
      auto match = getMinChiSqMatch<ProxyType, Chi2Type>( proxy );
      if ( !match.has_value() ) return std::nullopt;
      auto cluster  = proxy.relation( match->index ).to();
      match->index  = cluster.indices().cast();
      match->cellid = cluster.cellID();
      return EnergyMatch{ std::move( *match ), cluster.energy() };
    }

    // above one, but for brem with additional info
    template <typename Chi2Type, typename DeltaXType, typename BendingCorrectionType, typename ProxyType>
    inline std::optional<EnergyDeltaXMatch> getBestBrem( ProxyType const& proxy ) {
      auto match = getMinChiSqMatch<ProxyType, Chi2Type>( proxy );
      if ( !match.has_value() ) return std::nullopt;
      auto relation = proxy.relation( match->index );
      auto cluster  = relation.to();
      match->index  = cluster.indices().cast();
      match->cellid = cluster.cellID();
      return EnergyDeltaXMatch{ std::move( *match ), cluster.energy(), relation.template get<DeltaXType>().cast(),
                                relation.template get<BendingCorrectionType>().cast() };
    }

    // to get brem energy, optimized between track-based and cluster method
    inline std::optional<float> getBremEnergy( BremMethod method, std::optional<EnergyDeltaXMatch> brem,
                                               std::optional<float> brem_trackbased, float threshold_chi2 = 1.,
                                               float max_chi2 = 6. ) {
      bool add_brem = brem.has_value() && ( brem->match.chi2 <= max_chi2 );
      switch ( method ) {
      case BremMethod::TrackBased:
        return ( add_brem && brem_trackbased ) ? std::optional<float>( *brem_trackbased ) : std::nullopt;
      case BremMethod::Mixed:
        return ( add_brem && brem_trackbased )
                   ? std::optional<float>( brem->match.chi2 < threshold_chi2 ? brem->energy : *brem_trackbased )
                   : std::nullopt;
      case BremMethod::Cluster:
        return add_brem ? std::optional<float>( brem->energy ) : std::nullopt;
      case BremMethod::LooseTrackBased:
        return brem_trackbased;
      default:
        return std::nullopt;
      }
    }

    // bookkeeping for DLLs histograms
    using TrackType = LHCb::Event::v3::TrackType;

    enum DLLType { EcalPIDe, EcalPIDm, HcalPIDe, HcalPIDm, BremPIDe };

    struct DLLIndex {
      DLLType     type;
      TrackType   track;
      friend bool operator<( DLLIndex const& lhs, DLLIndex const& rhs ) {
        return std::pair{ lhs.type, lhs.track } < std::pair{ rhs.type, rhs.track };
      }
    };

    // note AIDA only works with THxD, so no HistogramSvc for now
    using DLLs_2D = LHCb::Calo::TrackUtils::HistoStore<DLLIndex, TH2F, AIDA::IHistogram2D>;
    using DLLs_3D = LHCb::Calo::TrackUtils::HistoStore<DLLIndex, TH3F, AIDA::IHistogram3D>;

    // histo type to location map
    inline DLLs_2D::HistoMap getHistoMap2D() {
      return {
          { { DLLType::EcalPIDm, TrackType::Long }, "hist_DLL_EcalPIDmu" },
          { { DLLType::EcalPIDm, TrackType::Downstream }, "hist_DLL_EcalPIDmu" },
          { { DLLType::EcalPIDm, TrackType::Ttrack }, "hist_DLL_EcalPIDmu" },
          { { DLLType::HcalPIDe, TrackType::Long }, "hist_DLL_HcalPIDe" },
          { { DLLType::HcalPIDe, TrackType::Downstream }, "hist_DLL_HcalPIDe" },
          { { DLLType::HcalPIDe, TrackType::Ttrack }, "hist_DLL_HcalPIDe" },
          { { DLLType::HcalPIDm, TrackType::Long }, "hist_DLL_HcalPIDmu" },
          { { DLLType::HcalPIDm, TrackType::Downstream }, "hist_DLL_HcalPIDmu" },
          { { DLLType::HcalPIDm, TrackType::Ttrack }, "hist_DLL_HcalPIDmu" },
      };
    }

    inline DLLs_3D::HistoMap getHistoMap3D() {
      return {
          { { DLLType::EcalPIDe, TrackType::Long }, "hist_DLL_EcalPIDe" },
          { { DLLType::EcalPIDe, TrackType::Downstream }, "hist_DLL_EcalPIDe" },
          { { DLLType::EcalPIDe, TrackType::Ttrack }, "hist_DLL_EcalPIDe" },
          { { DLLType::BremPIDe, TrackType::Long }, "hist_DLL_BremPIDe" },
          { { DLLType::BremPIDe, TrackType::Upstream }, "hist_DLL_BremPIDe" },
          { { DLLType::BremPIDe, TrackType::Downstream }, "hist_DLL_BremPIDe" },
          { { DLLType::BremPIDe, TrackType::Velo }, "hist_DLL_BremPIDe" },
      };
    }

    template <DLLType dtype, typename DLL_t>
    auto getDLL_2D( DLL_t const& dlls, TrackType ttype, float varx, float vary ) {
      auto hist       = dlls.hist( { dtype, ttype } );
      auto is_inrange = ( varx >= hist->GetXaxis()->GetXmin() ) && ( varx < hist->GetXaxis()->GetXmax() );
      varx            = clip( varx, hist->GetXaxis() );
      vary            = clip( vary, hist->GetYaxis() );
      return is_inrange ? hist->Interpolate( varx, vary )
                        : hist->GetBinContent( 0, hist->GetYaxis()->FindFixBin( vary ) );
    }

    template <DLLType dtype, typename DLL_t>
    auto getDLL_3D( DLL_t const& dlls, TrackType ttype, float varx, float vary, float varz ) {
      auto hist       = dlls.hist( { dtype, ttype } );
      auto is_inrange = ( varx >= hist->GetXaxis()->GetXmin() ) && ( varx < hist->GetXaxis()->GetXmax() ) &&
                        ( vary >= hist->GetYaxis()->GetXmin() ) && ( vary < hist->GetYaxis()->GetXmax() );
      varx = clip( varx, hist->GetXaxis() );
      vary = clip( vary, hist->GetYaxis() );
      varz = clip( varz, hist->GetZaxis() );
      return is_inrange ? hist->Interpolate( varx, vary, varz )
                        : hist->GetBinContent( 0, 0, hist->GetZaxis()->FindFixBin( varz ) );
    }

  } // namespace ChargedInfoUtils

} // namespace LHCb::Calo
