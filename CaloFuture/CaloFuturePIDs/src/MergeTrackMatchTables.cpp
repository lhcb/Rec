/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/TrackUtils.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <cfloat>

namespace LHCb::Calo {

  using namespace TrackUtils;

  using InputData  = Tracks2Clusters;
  using OutputData = Clusters2BestTrackMatch;
  using Chi2       = ClusterMatch;

  /**
   *  Merges track-cluster matching tables from different track types into one
   *  where from is cluster, not track (reversed), to be accessed for cluster
   *  indepent of track types
   *
   */
  class MergeTrackMatchTables final
      : public Algorithm::MergingTransformer<OutputData( const Gaudi::Functional::vector_of_const_<InputData>& )> {
  public:
    MergeTrackMatchTables( std::string const& name, ISvcLocator* pSvc );
    OutputData operator()( Gaudi::Functional::vector_of_const_<InputData> const& tables ) const override;
  };

  DECLARE_COMPONENT_WITH_ID( MergeTrackMatchTables, "MergeTrackMatchTables" )

  MergeTrackMatchTables::MergeTrackMatchTables( std::string const& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator,
                            // inputs
                            { "Inputs", {} },
                            // outputs
                            { "Output", "" } ) {}

  OutputData MergeTrackMatchTables::operator()( Gaudi::Functional::vector_of_const_<InputData> const& tables ) const {
    // define output
    auto       clusters = tables.at( 0 ).to();
    OutputData output_table( clusters );
    output_table.reserve( clusters->size() );

    // check tables consistancy of clusters
    for ( int i = 1; i < (int)tables.size(); ++i ) {
      if ( tables[i].to() != tables[i - 1].to() ) {
        throw GaudiException( "Tables do not point to the same cluster container", "LHCb::Event::Calo::Clusters",
                              StatusCode::FAILURE );
      }
    }

    // collect best matching track for cluster
    OutputData chi2s( clusters );
    chi2s.resize( clusters->size() );
    // default values
    for ( auto entry : chi2s.simd() ) entry.field<Chi2>().set( FLT_MAX );
    // best chi2 values
    for ( InputData const& table : tables ) {
      for ( auto const& rel : table.scalar() ) {
        auto chi2       = rel.get<Chi2>().cast();
        auto output_rel = chi2s.scalar()[rel.to().indices().cast()];
        if ( chi2 < output_rel.get<Chi2>().cast() ) output_rel.field<Chi2>().set( chi2 );
      }
    }

    // write to output, only relations for those clusters having a track nearby
    for ( auto chi2 : chi2s.simd() ) {
      auto selection = chi2.get<Chi2>() < SIMDWrapper::best::types::float_v{ FLT_MAX };
      output_table.compress_back( selection && chi2.loop_mask() ).set( chi2.indices(), chi2.get<Chi2>() );
    }

    return output_table;
  }

} // namespace LHCb::Calo
