/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include "SelectiveMatchUtils.h"

/** @class SelectiveBremMatchAlg SelectiveBremMatchAlg.cpp
 *
 *  Matches tracks with local clusters in and around calo cell
 *  corresponding to track linear extrapolation from before magnet for brem recovery.
 *  Sufficient since resolution is about cell size / sqrt(12).
 *
 *  Returns per cluster a matching chi2 and a test statistic with the higher the value,
 *  the more 'first-state like' and the lower, the more 'last-state like', as real
 *  brem tends to come from the first state.
 *
 *  In addition calculates track-based brem energy with ecal digit search based on track extrapolations.
 *
 *  @date   2020-11
 *  @author Maarten VAN VEGHEL
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class SelectiveBremMatchAlg
 *  It takes inspiration from SelectiveTrackMatchAlg
 *
 *  @date   2020-11
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Calo::SelectiveMatchUtils;

  using CaloObjects = CaloHypos;

  using MatchTable  = Tracks2Brems;
  using EnergyTable = TracksBremEnergy;

  using OutputData = std::tuple<MatchTable, EnergyTable>;

  // local helper objects / functions for class
  namespace {
    // scan along x from first to last state for cell IDs
    void scanXRange( std::vector<Detector::Calo::CellID>& cellids, Gaudi::XYZPoint& scanpos, const double dx,
                     const double stepsize, const DeCalorimeter& calo, const int nMaxSteps = 0 ) {
      double xpos   = scanpos.x();
      int    sdx    = ( dx > 0. ) - ( dx < 0. );
      int    nscans = int( std::ceil( std::abs( dx ) / stepsize ) );
      nscans        = ( nMaxSteps > 0 ) ? std::min( nMaxSteps, nscans ) : nscans;
      for ( int i = 0; i <= nscans; i++ ) {
        scanpos.SetX( xpos + sdx * i * stepsize );
        const auto cpar = calo.Cell_( scanpos );
        if ( cpar && cpar->valid() ) cellids.push_back( cpar->cellID() );
      }
      // remove duplicates if there are any, note order is not preserved!
      std::sort( cellids.begin(), cellids.end() );
      cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
    }

    // to get additional cell IDs along state slopes for first state (most likely brem origin state)
    void getTrackBasedEnergyCells( std::vector<Detector::Calo::CellID>& cellids, LHCb::State const& state,
                                   DeCalorimeter const& calo, LHCb::span<const double> zscans ) {
      auto pos = state.position();
      for ( const double zscan : zscans ) {
        // y-tilt can be ignored for this scan
        pos += ( zscan - pos.z() ) * state.slopes();
        const auto cpar = calo.Cell_( pos );
        if ( cpar && cpar->valid() ) cellids.push_back( cpar->cellID() );
      }
      // remove duplicates if there are any, note order is not preserved!
      std::sort( cellids.begin(), cellids.end() );
      cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
    }

    // state locations of first and last state
    std::optional<std::pair<SL, SL>> extrapolation_statelocs_brem( Tracks const& tracks ) {
      auto const state_first_map = std::map<TT, SL>{ { TT::Long, SL::ClosestToBeam },
                                                     { TT::Downstream, SL::FirstMeasurement },
                                                     { TT::Velo, SL::ClosestToBeam },
                                                     { TT::Upstream, SL::ClosestToBeam } };
      auto const state_last_map  = std::map<TT, SL>{ { TT::Long, SL::EndRich1 },
                                                     { TT::Downstream, SL::FirstMeasurement },
                                                     { TT::Velo, SL::LastMeasurement },
                                                     { TT::Upstream, SL::LastMeasurement } };
      auto const trktype         = tracks.type();
      if ( state_first_map.find( trktype ) == state_first_map.end() ) return std::nullopt;
      auto const statelocs = std::pair<SL, SL>{ state_first_map.at( trktype ), state_last_map.at( trktype ) };
      return std::optional<std::pair<SL, SL>>{ statelocs };
    }

    // propagates first state and last-before-magnet state of track to calo plane
    // NB: only update last-state covariance, use for both
    template <typename MyState>
    bool propagateToCaloForBrem( LHCb::StateVector& state_first, LHCb::State& state_last, MyState const& ref_first,
                                 MyState const& ref_last, const Gaudi::Plane3D& plane ) {
      return propagateToCalo<MyState>( state_first, ref_first, plane ) &&
             propagateToCaloWithCov<MyState>( state_last, ref_last, plane );
    }

  } // namespace

  class SelectiveBremMatchAlg
      : public Algorithm::MultiTransformer<OutputData( DeCalorimeter const&, CaloObjects const&, CaloDigits const&,
                                                       TracksInBrem const&, cellSizeCovariances const& ),
                                           Algorithm::Traits::usesConditions<DeCalorimeter, cellSizeCovariances>> {
  public:
    // standard constructor
    SelectiveBremMatchAlg( const std::string&, ISvcLocator* );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    OutputData operator()( DeCalorimeter const&, CaloObjects const&, CaloDigits const&, TracksInBrem const&,
                           cellSizeCovariances const& ) const override;

  private:
    // properties
    int                   m_nmaxelements;
    Gaudi::Property<int>  m_nsquares{ this,
                                     "nNeighborSquares",
                                     1,
                                     [this]( auto& ) { m_nmaxelements = std::pow( 2 * m_nsquares + 1, 2 ); },
                                     Gaudi::Details::Property::ImmediatelyInvokeHandler{ true },
                                     "Number of squares of cells around central cell to scan for clusters" };
    Gaudi::Property<bool> m_usespread{ this, "useSpread", false, "Use spread or cellsize/sqrt(12) for uncertainties" };
    Gaudi::Property<int>  m_nmaxcellsxscan{ this, "nMaxCellsXScan", 8,
                                           "maximum number of cells added in scan counting from first to last state" };
    Gaudi::Property<CaloPlane::Plane> m_caloplane{ this, "CaloPlane", CaloPlane::ShowerMax,
                                                   "Plane at the calorimeter of where to extrapolate to" };
    Gaudi::Property<float>            m_threshold{ this, "MaxChi2Threshold", 10000., "Maximum allowed chi2 value" };

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>          m_nMatchFailure{ this, "#match failure" };
    mutable Gaudi::Accumulators::StatCounter<>          m_nLinks{ this, "#links in table" };
    mutable Gaudi::Accumulators::StatCounter<>          m_nOverflow{ this, "#above threshold" };
    mutable Gaudi::Accumulators::StatCounter<float>     m_chi2{ this, "average chi2" };
    mutable Gaudi::Accumulators::StatCounter<float>     m_etrackbased{ this, "average energy (track based)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_unique{ this, "Failed to generate index" };
  };

  DECLARE_COMPONENT_WITH_ID( SelectiveBremMatchAlg, "SelectiveBremMatchAlg" )

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================

  SelectiveBremMatchAlg::SelectiveBremMatchAlg( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer(
            name, pSvc,
            // Inputs
            { KeyValue( "Detector", { CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) } ),
              KeyValue( "InputHypos", { CaloFutureAlgUtils::CaloFutureHypoLocation( "Photons" ) } ),
              KeyValue( "InputDigits", { CaloDigitLocation::Ecal } ), KeyValue( "TracksInCalo", "" ),
#ifdef USE_DD4HEP
              KeyValue( "cellSizeCovariances", { "/world:AlgorithmSpecific-" + name + "-cellsizecovariances" } ) },
#else
              KeyValue( "cellSizeCovariances", { "AlgorithmSpecific-" + name + "-cellsizecovariances" } ) },
#endif
            // Outputs
            { KeyValue( "OutputMatchTable", {} ), KeyValue( "OutputEnergyTable", {} ) } ) {
  }

  // ============================================================================
  //   Initialization of algorithm / tool
  // ============================================================================
  StatusCode SelectiveBremMatchAlg::initialize() {
    return MultiTransformer::initialize().andThen( [&] {
      addConditionDerivation<cellSizeCovariances( const DeCalorimeter& )>(
          { CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) }, inputLocation<cellSizeCovariances>() );
    } );
  }

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  OutputData SelectiveBremMatchAlg::operator()( DeCalorimeter const& calo, CaloObjects const& caloobjects,
                                                CaloDigits const& digits, TracksInBrem const& tracksincalo,
                                                cellSizeCovariances const& cellsizecovs ) const {
    // output containers
    auto result = std::tuple{ MatchTable( &*tracksincalo.from(), &caloobjects ), EnergyTable( &*tracksincalo.from() ) };
    auto& [trtable, entable] = result;

    // reserve memory
    int sizeMatchTable = 5 * tracksincalo.size();
    trtable.reserve( sizeMatchTable );
    entable.reserve( tracksincalo.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_locs_brem    = extrapolation_statelocs_brem( *tracksincalo.from() );
    auto const state_loc_electron = extrapolation_stateloc( *tracksincalo.from() );
    if ( !state_locs_brem.has_value() ) {
      throw GaudiException( "Not a valid track type for brem recovery.", "LHCb::Event::Enum::Track::Type",
                            StatusCode::FAILURE );
    }

    // obtain the right calo objects
    const auto& hypoclusters = caloobjects.clusters();
    auto        clusters     = hypoclusters.index();
    if ( !clusters || hypoclusters.size() != caloobjects.size() ) {
      ++m_not_unique;
      return result;
    }

    // miscellaneous info
    const auto   plane_at_calo = calo.plane( m_caloplane );
    const auto   other_planes  = std::array{ calo.plane( CaloPlane::Front ), calo.plane( CaloPlane::Back ) };
    const auto   cscovs        = cellsizecovs();
    const double mincellsize   = cellsizecovs.cellsizes()[Detector::Calo::CellCode::CaloArea::Inner];

    // for track-based energy scan, ignoring tilt is precise enough when extrapolating from calo front
    // these are on top of the showermax plane
    const auto z_energy_planes =
        std::array{ -calo.plane( CaloPlane::Front ).HesseDistance(), -calo.plane( CaloPlane::Middle ).HesseDistance(),
                    -calo.plane( CaloPlane::Back ).HesseDistance() };

    // declare what is needed in loop
    Gaudi::XYZPoint   scanposition;
    LHCb::StateVector state_first;
    LHCb::State       state_last;
    Match2D::Vector   track_fpos, calo_pos, diff_pos;
    Match2D::Matrix   track_cov, comb_cov;

    std::vector<Detector::Calo::CellID> cellids, energycellids;
    cellids.reserve( ( m_nmaxelements + 1 ) * m_nmaxcellsxscan );
    energycellids.reserve( 2 * ( z_energy_planes.size() + m_nmaxcellsxscan + 1 ) );

    // main loop over tracks
    for ( const auto& trackincalo : tracksincalo.scalar() ) {
      // propagate the two 'edge' states (first and last before magnet) to calo
      // NB: only using last-state covariance (also only calculated)
      auto track     = trackincalo.from();
      auto ref_first = track.state( state_locs_brem.value().first );
      auto ref_last  = track.state( state_locs_brem.value().second );
      if ( !propagateToCaloForBrem( state_first, state_last, ref_first, ref_last, plane_at_calo ) ) continue;

      // information for bending correction
      std::optional<float> full_dx = std::nullopt;
      if ( state_loc_electron.has_value() ) {
        auto ref_ele = track.state( state_loc_electron.value() );
        full_dx      = { std::abs( state_first.x() - ( ref_ele.x().cast() +
                                                  ref_ele.tx().cast() * ( state_first.z() - ref_ele.z().cast() ) ) ) };
      }

      // look for CellIDs from first to last state
      auto       dx         = state_last.x() - state_first.x();
      const auto cell_first = getClosestCellID( calo, state_first.position(), state_first.slopes(), other_planes );
      const auto cell_last  = getClosestCellID( calo, state_last.position(), state_last.slopes(), other_planes );
      // scan along dx range if relevant (large enough)
      if ( cell_first && cell_first == cell_last ) {
        cellids.assign( 1, cell_first );
      } else {
        cellids.clear();
        const auto stepsize =
            std::max( std::min( calo.cellSize( cell_first ), calo.cellSize( cell_last ) ), mincellsize );
        scanposition = state_first.position();
        scanXRange( cellids, scanposition, dx, stepsize, calo );
      }

      // get energy of closest cells based on track state extrapolation
      energycellids.assign( cellids.begin(), cellids.end() );
      getTrackBasedEnergyCells( energycellids, state_first, calo, z_energy_planes );
      auto ebrem_trackbased =
          accumulate( begin( energycellids ), end( energycellids ), 0., [&]( double e, Detector::Calo::CellID ci ) {
            if ( const auto digit = digits( ci ); digit ) { e += digit->energy(); }
            return ( e > 0. ) ? e : 0.;
          } );
      entable.emplace_back<SIMDWrapper::InstructionSet::Scalar>().set( track.indices(), ebrem_trackbased );

      // add neighbouring cellids for calo obj search
      if ( !getNeighborCellIDs( cellids, calo, m_nsquares ) ) continue;

      // obtain relevant info for matching from track
      // take last state cov, as this should be largest
      track_fpos = { state_first.x(), state_first.y() };
      track_cov  = state_last.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 );

      // loop over local calo objects and calculate chi2
      for ( const auto& caloid : cellids ) {
        // find calo obj ('cluster' storing info from hypo)
        const auto caloobj = clusters.find( caloid );
        if ( caloobj == clusters.end() ) continue;

        // obtain calo obj info
        calo_pos = { caloobj->position().x(), caloobj->position().y() };
        comb_cov = ( m_usespread ? caloobj->spread() : cscovs[caloobj->cellID().area()] ) + track_cov;

        // allow for a window between first and last state before magnet
        // calculate relative position between those states
        // (says roughly where along the track the brem emission is)
        diff_pos   = calo_pos - track_fpos;
        double rdx = ( std::abs( dx ) > 0. ) ? diff_pos( 0 ) / dx : 0.;
        if ( rdx > 0. ) {
          if ( rdx > 1. ) {
            diff_pos( 0 ) -= dx;
            rdx = 1.;
          } else {
            diff_pos( 0 ) = 0.;
          }
        } else {
          rdx = 0.;
        }
        // brem tends to come from first state, non-brem random (uniform)
        // test statistic with the higher the value, the more 'first-state like'
        // and the lower, the more 'last-state like'
        double teststat_dx =
            ( comb_cov( 0, 0 ) > 0. ) ? std::abs( dx ) * ( 0.5 - rdx ) / std::sqrt( comb_cov( 0, 0 ) ) : 0.;

        // calculate chi2
        if ( !comb_cov.Invert() ) {
          m_nMatchFailure += 1;
          continue;
        }
        auto chi2 = ROOT::Math::Similarity( diff_pos, comb_cov );

        // check if it has proper value
        if ( m_threshold < chi2 ) {
          m_nOverflow += 1;
          continue;
        }

        // bending correction
        float bending_correction = full_dx.has_value() ? 1.f - std::abs( dx ) * rdx / full_dx.value() : 1.f;

        // only now push proper results
        auto relation = trtable.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        relation.set( track.indices(), caloobj->indices(), chi2, teststat_dx, bending_correction );
      }
    }

    // monitor statistics: number of links and average chi2 and track-based energy
    auto const nMLinks = trtable.size();
    m_nLinks += nMLinks;
    for ( auto const& proxy : trtable.scalar() ) m_chi2 += proxy.get<BremMatch>().cast() / nMLinks;
    auto const nELinks = entable.size();
    for ( auto const& proxy : entable.scalar() ) m_etrackbased += proxy.get<BremEnergy>().cast() / nELinks;

    return result;
  }

} // namespace LHCb::Calo
