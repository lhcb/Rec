/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#ifndef SELECTIVEMATCHUTILS_H
#define SELECTIVEMATCHUTILS_H

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"
#include <array>

/** @file
 *
 *  Implementation file for helper functions and classes for
 *  track - calo object matching algorithms using selective cell ID
 *  search in calo
 *
 *  @date   2020-09
 *  @author Maarten VAN VEGHEL
 */

namespace LHCb::Calo {

  namespace SelectiveMatchUtils {

    namespace Match2D {
      using Vector = ROOT::Math::SVector<double, 2>;
      using Matrix = ROOT::Math::SMatrix<double, 2, 2, ROOT::Math::MatRepSym<double, 2>>;
    } // namespace Match2D

    namespace Match3D {
      using Vector = ROOT::Math::SVector<double, 3>;
      using Matrix = ROOT::Math::SMatrix<double, 3, 3, ROOT::Math::MatRepSym<double, 3>>;
    } // namespace Match3D

    // simplified covariances based on cell size for outer, middle and inner region
    class cellSizeCovariances {
    private:
      std::array<double, 3>          m_cellsizes = { 0., 0., 0. };
      std::array<Match2D::Matrix, 3> m_covariances;

    public:
      cellSizeCovariances() = default; // needed by DD4hep even if unused !
      // constructor
      cellSizeCovariances( DeCalorimeter const& calo ) {
        // get cell sizes
        for ( unsigned int n = 0; n < 3; n++ ) m_cellsizes[n] = calo.cellSize( n );
        // convert them to covariance matrices
        Match2D::Matrix identity = ROOT::Math::SMatrixIdentity();
        m_covariances            = std::array<Match2D::Matrix, 3>{ ( std::pow( m_cellsizes[0], 2 ) / 12. ) * identity,
                                                                   ( std::pow( m_cellsizes[1], 2 ) / 12. ) * identity,
                                                                   ( std::pow( m_cellsizes[2], 2 ) / 12. ) * identity };
      }

      // access (operator) to covariances
      const std::array<Match2D::Matrix, 3>& operator()() const { return m_covariances; }
      // access to cell sizes
      const std::array<double, 3>& cellsizes() const { return m_cellsizes; }
    };

    // to obtain closest cell id (or not) to track extrapolation
    inline Detector::Calo::CellID getClosestCellID( const DeCalorimeter& calo, const Gaudi::XYZPoint& point,
                                                    const Gaudi::XYZVector&    slopes,
                                                    span<const Gaudi::Plane3D> planes = {} ) {
      // is the point associated to a valid cell?
      auto centercellparam = calo.Cell_( point );
      // if not, scan along slope of track if there is maybe one around?
      if ( !centercellparam || !centercellparam->valid() ) {
        Gaudi::XYZPoint other_point_at_calo;
        double          mu   = 0.0;
        const auto&     line = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>( point, slopes );
        for ( const auto& plane : planes ) {
          if ( !Gaudi::Math::intersection( line, plane, other_point_at_calo, mu ) ) { continue; }
          centercellparam = calo.Cell_( other_point_at_calo );
          if ( centercellparam && centercellparam->valid() ) { break; }
        }
        if ( !centercellparam || !centercellparam->valid() ) { return {}; }
      }
      // return valid cell
      return centercellparam->cellID();
    }

    // to obtain calo cell IDs around track extrapolation to calo
    inline bool getNeighborCellIDs( std::vector<Detector::Calo::CellID>& cellids, const DeCalorimeter& calo,
                                    const unsigned int nSquares ) {
      // add neighbours in squares around center cell
      switch ( nSquares ) {
      case 0:
        return true;
      case 1: {
        int nCells = cellids.size();
        if ( nCells == 1 ) {
          const auto& neighbors = calo.neighborCells( cellids.front() );
          cellids.insert( cellids.end(), neighbors.begin(), neighbors.end() );
        } else {
          for ( auto i = 0; i < nCells; ++i ) {
            const auto& neighbors = calo.neighborCells( cellids.at( i ) );
            cellids.insert( cellids.end(), neighbors.begin(), neighbors.end() );
          }
          // remove duplicates
          std::sort( cellids.begin(), cellids.end() );
          cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
        }
        return true;
      }
      default:
        // add even more if needed
        return CaloFutureFunctors::neighbours( cellids, nSquares, &calo );
      }
    }

  } // namespace SelectiveMatchUtils

} // namespace LHCb::Calo
#endif
