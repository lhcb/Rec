#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## Configurable for CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
Configurable for CaloFuturerimeter PID
"""

# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
# =============================================================================
__all__ = ("HltCaloFuturePIDsConf", "OffLineCaloFuturePIDsConf", "CaloFuturePIDsConf")
# =============================================================================

import logging

from CaloKernel.ConfUtils import addAlgs, printOnDemand, prntCmp, setTheProperty
from Configurables import HistogramSvc
from LHCbKernel.Configuration import *

from CaloFuturePIDs.PIDs import caloPIDs, referencePIDs

_log = logging.getLogger("CaloFuturePIDs")


# =============================================================================
## @class CaloFuturePIDsConf
#  Configurable for CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class CaloFuturePIDsConf(LHCbConfigurableUser):
    """
    Class/Configurable to define the calorimeter PID
    """

    ## define the slots
    __slots__ = {
        ##
        "Context": "",  # The context within which to run
        "MeasureTime": False,  # Measure the time for sequencers
        "OutputLevel": INFO,  # The global output level
        ##
        "Sequence": "",  # The sequencer to add the CALOFUTURE reconstruction algorithms to
        "PIDList": [
            "InAcceptance",
            "Match",
            "Energy",
            "Chi2",
            "DLL",
            "NeutralPID",
        ],  # List of PID fragments to be included (alternative full sequence per technique : [ 'EcalPID', 'BremPID', 'HcalPID', 'NeutralPID' ] )
        "EnablePIDsOnDemand": False,  # enable Reco-On-Demand
        ##
        "DataType": "MC09",  # Data type
        "TrackLocation": "",  # track location to be used (default use CaloFutureAlgUtils default)
        "ClMatchTrTypes": [],  # Track types for Cluster matching (if not set the alg. default is used i.e. Long + Downstream + TTracks)
        "CaloFuturePIDTrTypes": [],  # Track types for CaloFuturePID (if not set the alg. default is used i.e. Long + Downstream + TTracks)
        "BremPIDTrTypes": [],  # Track types for BremPID (if not set the alg. default is used i.e. Long + Upstream + Velo)
        "SkipNeutrals": False,  # skip neutralID (already run in CaloFutureRecoConf by default)
        "SkipCharged": False,  # skip chargedID
        "FastPID": False,  # speed-up PID (lighter sequence)
        "ExternalClusters": "",  # use non-default cluster container
        "Verbose": False,
    }

    ## Configure recontruction of CaloFuture Charged  PIDs
    def caloPIDs(self):
        """
        Configure recontruction of CaloFuture Charged  PIDs
        """
        cmp = caloPIDs(
            self.getProp("EnablePIDsOnDemand"),
            self.getProp("PIDList"),
            self.getProp("TrackLocation"),
            self.getProp("ClMatchTrTypes"),
            self.getProp("CaloFuturePIDTrTypes"),
            self.getProp("BremPIDTrTypes"),
            self.getProp("SkipNeutrals"),
            self.getProp("SkipCharged"),
            self.getProp("FastPID"),
            self.getProp("ExternalClusters"),
            self.getName(),
        )

        referencePIDs(self.getProp("DataType"))

        _log.info("Configured CaloFuture PIDs           : %s " % cmp.name())
        ##
        return cmp

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        _log.debug("CaloFuturePIDsConf: Configuration is not checked!")

    def printConf(self, verbose=False):
        if self.getProp("Verbose") or verbose:
            _log.info(self)

    ## CaloFuturerimeter PID Configuration

    def applyConf(self):
        """
        CaloFuturerimeter PID Configuration
        """

        self.printConf()

        pids = self.caloPIDs()

        setTheProperty(pids, "MeasureTime", self.getProp("MeasureTime"))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(pids, "OutputLevel", self.getProp("OutputLevel"))

        if self.getProp("Sequence"):
            addAlgs(self.Sequence, pids)
            _log.info(
                "Configure main CaloFuture PIDs Sequence  : %s " % self.Sequence.name()
            )
            if self.getProp("Verbose"):
                _log.info(prntCmp(self.Sequence))
        else:
            _log.info("Configure CaloFuturerimeter PIDs blocks ")
            if self.getProp("Verbose"):
                _log.info(prntCmp(pids))

        if self.getProp("EnablePIDsOnDemand"):
            _log.info(printOnDemand())


# =============================================================================
## @class HltCaloFuturePIDsConf
#  Configurable for CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class HltCaloFuturePIDsConf(CaloFuturePIDsConf):
    """
    Class/Configurable to define the calorimeter PID for Hlt
    """

    __slots__ = {}

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        pass


# =============================================================================
## @class OffLineCaloFuturePIDsConf
#  Configurable for CaloFuturerimeter PID
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class OffLineCaloFuturePIDsConf(CaloFuturePIDsConf):
    """
    Class/Configurable to define the calorimeter PID for Off-Line
    """

    __slots__ = {}

    ## Check the configuration
    def checkConfiguration(self):
        """
        Check the configuration
        """
        pass


# =============================================================================
# The END
# =============================================================================
