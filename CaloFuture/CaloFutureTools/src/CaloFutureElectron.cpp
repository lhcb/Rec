/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Core/FloatComparison.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/TrackDefaultParticles.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

/** @class CaloFutureElectron CaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */

namespace {
  // fix name clash with Track2Calo::closestState
  decltype( auto ) closest_state( const LHCb::Track& t, const Gaudi::Plane3D& p ) { return closestState( t, p ); }
} // namespace

namespace LHCb::Calo::Tools {

  class Electron final : public extends<GaudiTool, Interfaces::IElectron> {
  public:
    /// Standard constructor
    using extends::extends;

    Interfaces::hypoPairStruct getElectronBrem( ProtoParticle const& proto ) const override;
    State                      caloState( const DeCalorimeter& detCalo, ProtoParticle const& proto,
                                          IGeometryInfo const& geometry ) const override;
    State caloState( const DeCalorimeter& detCalo, Track const&, IGeometryInfo const& geometry ) const override;
    // override;
    Momentum bremMomentum( ProtoParticle const& proto ) const override;

  private:
    State caloState( const DeCalorimeter& detCalo, Track const& track, CaloPlane::Plane plane, double delta,
                     IGeometryInfo const& geometry ) const;

    // configuration-dependent state
    ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "Extrapolator", "TrackRungeKuttaExtrapolator" };
    Gaudi::Property<float>         m_tolerance{ this, "Tolerance", 0.01 };
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( Electron, "CaloFutureElectron" )

  //=============================================================================
  Interfaces::hypoPairStruct Electron::getElectronBrem( ProtoParticle const& proto ) const {
    if ( !proto.track() ) return { nullptr, nullptr };
    Interfaces::hypoPairStruct hypoPair;
    for ( const CaloHypo* hypo : proto.calo() ) {
      if ( !hypo ) continue;
      switch ( hypo->hypothesis() ) {
      case CaloHypo::Hypothesis::EmCharged:
        hypoPair.electronHypo = hypo;
        break;
      case CaloHypo::Hypothesis::Photon:
        hypoPair.bremHypo = hypo;
        break;
      default:; // nothing;
      }
    }
    if ( !( hypoPair && hypoPair.electronHypo->position() ) ) { // Electron hypo is mandatory - brem. not
      return { nullptr, nullptr };
    }
    return hypoPair;
  }

  //=============================================================================
  State Electron::caloState( const DeCalorimeter& detCalo, ProtoParticle const& proto,
                             IGeometryInfo const& geometry ) const {
    auto hypo = electron( proto );
    if ( !hypo ) return {};
    return caloState( detCalo, *proto.track(), CaloPlane::ShowerMax, 0., geometry );
  }

  //=============================================================================
  State Electron::caloState( const DeCalorimeter& detCalo, Track const& track, IGeometryInfo const& geometry ) const {
    return caloState( detCalo, track, CaloPlane::ShowerMax, 0., geometry );
  }

  //=============================================================================
  Momentum Electron::bremMomentum( ProtoParticle const& proto ) const {
    auto hypoPair = getElectronBrem( proto );
    if ( !hypoPair || hypoPair.bremHypo == nullptr ) return {};
    Gaudi::XYZPoint     point;
    Gaudi::SymMatrix3x3 matrix;
    proto.track()->position( point, matrix );
    return { hypoPair.bremHypo, point, matrix };
  }

  //=============================================================================
  State Electron::caloState( const DeCalorimeter& detCalo, Track const& track, CaloPlane::Plane plane, double delta,
                             IGeometryInfo const& geometry ) const {
    // get caloPlane
    ROOT::Math::Plane3D refPlane = detCalo.plane( plane );
    // propagate state to refPlane
    const Tr::PID pid       = Tr::PID::Pion();
    State         calostate = closest_state( track, refPlane );
    StatusCode    sc        = m_extrapolator->propagate( calostate, refPlane, geometry, m_tolerance, pid );
    if ( sc.isFailure() ) return {};
    if ( essentiallyZero( delta ) ) return calostate;
    Gaudi::XYZVector dir( calostate.tx(), calostate.ty(), 1. );
    auto             z = ( calostate.position() + delta * dir.Unit() ).z();
    // extrapolate to the new point
    sc = m_extrapolator->propagate( calostate, z, geometry, pid );
    if ( sc.isFailure() ) return {};
    return calostate;
  }
} // namespace LHCb::Calo::Tools
