/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloChargedInfo.h"
#include "Event/CaloChargedInfo_v1.h"
#include "Event/RelationTable.h"
#include "Event/Track_v3.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb::Calo {

  using Relations       = LHCb::Event::V3ToV1Mapping<LHCb::Event::v1::Tracks>;
  using RelationsShared = LHCb::Event::V3ToV1Mapping<SharedObjectsContainer<LHCb::Event::v1::Track>>;

  /**
   *  Converts v3-tracks-related calo pid object to v1-tracks-related
   *  objects for use in ProtoParticles
   *
   */
  template <typename InputData, typename OutputData, typename Relations>
  class ChargedPIDConverter
      : public Algorithm::Transformer<typename OutputData::Container( InputData const&, Relations const& )> {
  public:
    using KeyValue = typename ChargedPIDConverter<InputData, OutputData, Relations>::KeyValue;
    ChargedPIDConverter( std::string const& name, ISvcLocator* pSvc )
        : ChargedPIDConverter<InputData, OutputData, Relations>::Transformer(
              name, pSvc, { KeyValue( "ChargedPIDs", "" ), KeyValue( "Relations", "" ) },
              { KeyValue( "Output", "" ) } ) {}

    typename OutputData::Container operator()( InputData const& data, Relations const& relations ) const override {
      // define output
      typename OutputData::Container output;
      output.reserve( data.size() );

      // loop over original v3-tracks-related objects and relations
      for ( auto proxy : data.scalar() ) {
        // copy v3 pid object info to v1 object
        auto* obj = new OutputData{};
        obj->template setParametersFromV3( proxy );
        // add pointer to v1 track from relations
        obj->setIDTrack( relations.get( proxy.indices().cast() ) );
        output.add( obj );
      }

      return output;
    };
  };

  // ============================================================================
  // Specific class definitions
  // ============================================================================
  using ChargedPID_v1 = LHCb::Event::Calo::v1::CaloChargedPID;
  using ChargedPID_v2 = LHCb::Event::Calo::v2::ChargedPID;
  using BremInfo_v1   = LHCb::Event::Calo::v1::BremInfo;
  using BremInfo_v2   = LHCb::Event::Calo::v2::BremInfo;

  namespace v1 {

    // Ecal/Hcal info
    struct ChargedPIDsConverter final : ChargedPIDConverter<ChargedPID_v2, ChargedPID_v1, Relations> {
      ChargedPIDsConverter( std::string const& name, ISvcLocator* pSvc )
          : ChargedPIDConverter<ChargedPID_v2, ChargedPID_v1, Relations>( name, pSvc ) {}
    };

    // Brem info
    struct BremInfosConverter final : ChargedPIDConverter<BremInfo_v2, BremInfo_v1, Relations> {
      BremInfosConverter( std::string const& name, ISvcLocator* pSvc )
          : ChargedPIDConverter<BremInfo_v2, BremInfo_v1, Relations>( name, pSvc ) {}
    };

  } // namespace v1

  namespace v1Shared {

    // Ecal/Hcal info
    struct ChargedPIDsConverter final : ChargedPIDConverter<ChargedPID_v2, ChargedPID_v1, RelationsShared> {
      ChargedPIDsConverter( std::string const& name, ISvcLocator* pSvc )
          : ChargedPIDConverter<ChargedPID_v2, ChargedPID_v1, RelationsShared>( name, pSvc ) {}
    };

    // Brem info
    struct BremInfosConverter final : ChargedPIDConverter<BremInfo_v2, BremInfo_v1, RelationsShared> {
      BremInfosConverter( std::string const& name, ISvcLocator* pSvc )
          : ChargedPIDConverter<BremInfo_v2, BremInfo_v1, RelationsShared>( name, pSvc ) {}
    };

  } // namespace v1Shared

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::v1::ChargedPIDsConverter, "ChargedPIDsConverter_v1" )
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::v1::BremInfosConverter, "BremInfosConverter_v1" )

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::v1Shared::ChargedPIDsConverter, "ChargedPIDsConverter_v1Shared" )
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::v1Shared::BremInfosConverter, "BremInfosConverter_v1Shared" )
