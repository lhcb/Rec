/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFuture2CaloFuture.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h" // Interface
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellNeighbour.h"
#include "Core/FloatComparison.h"
#include "Event/CaloDigits_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"

/** @class CaloFutureHypo2CaloFuture CaloFutureHypo2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2008-09-11
 */
class CaloFutureHypo2CaloFuture final : public extends<CaloFuture2CaloFuture, LHCb::Calo::Interfaces::IHypo2Calo> {
public:
  /// Standard constructor
  using extends::extends;

  // energy
  double energy( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                 const DeCalorimeter& toCalo ) const override;
  double energy( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                 const DeCalorimeter& toCalo ) const override;

  // multiplicity
  int multiplicity( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                    const DeCalorimeter& toCalo ) const override;
  int multiplicity( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                    const DeCalorimeter& toCalo ) const override;

private:
  const LHCb::Event::Calo::Digits& fetch_digits( LHCb::Detector::Calo::CellCode::Index toCalo ) const {
    return *m_handles[toCalo].get();
  }
  // cellIDs
  using CaloFuture2CaloFuture::cellIDs;
  std::vector<LHCb::Event::Calo::Digit> cellIDs( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                                                 const DeCalorimeter& toCalo, const LHCb::Event::Calo::Digits& digits,
                                                 LHCb::Detector::Calo::CellID lineID ) const;
  std::vector<LHCb::Event::Calo::Digit> cellIDs( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                                                 const DeCalorimeter&             toCalo,
                                                 const LHCb::Event::Calo::Digits& digits ) const;

  Gaudi::Property<bool>                                m_seed{ this, "Seed", true };
  Gaudi::Property<bool>                                m_neighb{ this, "AddNeighbors", true };
  Gaudi::Property<bool>                                m_line{ this, "PhotonLine", true };
  Gaudi::Property<bool>                                m_whole{ this, "WholeCluster", false };
  Gaudi::Property<int>                                 m_status{ this, "StatusMask", 0x0 };
  Gaudi::Property<float>                               m_x{ this, "xTolerance", 5. * Gaudi::Units::mm };
  Gaudi::Property<float>                               m_y{ this, "yTolerance", 5. * Gaudi::Units::mm };
  Map<DataObjectReadHandle<LHCb::Event::Calo::Digits>> m_handles{
      std::forward_as_tuple( this, "EcalDigitsLocation", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Ecal" ) ),
      std::forward_as_tuple( this, "HcalDigitsLocation",
                             LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( "Hcal" ) ) };
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFutureHypo2CaloFuture )

double CaloFutureHypo2CaloFuture::energy( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                                          const DeCalorimeter& toCalo ) const {
  const auto& output = cellIDs( fromCalo, fromCluster, toCalo, fetch_digits( toCalo.index() ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto& d ) { return e + d.energy(); } );
}
double CaloFutureHypo2CaloFuture::energy( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                                          const DeCalorimeter& toCalo ) const {
  const auto& output = cellIDs( fromCalo, fromHypo, toCalo, fetch_digits( toCalo.index() ) );
  return std::accumulate( output.begin(), output.end(), 0., []( double e, const auto& d ) { return e + d.energy(); } );
}
int CaloFutureHypo2CaloFuture::multiplicity( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                                             const DeCalorimeter& toCalo ) const {
  return cellIDs( fromCalo, fromCluster, toCalo, fetch_digits( toCalo.index() ) ).size();
}
int CaloFutureHypo2CaloFuture::multiplicity( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                                             const DeCalorimeter& toCalo ) const {
  return cellIDs( fromCalo, fromHypo, toCalo, fetch_digits( toCalo.index() ) ).size();
}

//=============================================================================
std::vector<LHCb::Event::Calo::Digit>
CaloFutureHypo2CaloFuture::cellIDs( const DeCalorimeter& fromCalo, const LHCb::CaloHypo& fromHypo,
                                    const DeCalorimeter& toCalo, const LHCb::Event::Calo::Digits& digits ) const {
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Matching CaloHypo to " << toCalo.index() << " hypo energy = " << fromHypo.e() << endmsg;

  // get the cluster
  const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( fromHypo );

  if ( !cluster ) {
    Error( "No valid cluster!" ).ignore();
    return {};
  }

  LHCb::Detector::Calo::CellID seedID = cluster->seed();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Cluster seed " << seedID << " " << fromCalo.cellCenter( seedID ) << endmsg;

  if ( m_whole ) { return CaloFuture2CaloFuture::cellIDs( fromCalo, *cluster, toCalo, digits ); }

  auto lineID = LHCb::Detector::Calo::CellID();
  if ( m_line && fromHypo.position() ) {
    lineID = intersect( toCalo, { fromHypo.position()->x(), fromHypo.position()->y(), fromHypo.position()->z() } );
  }
  return cellIDs( fromCalo, *cluster, toCalo, digits, lineID );
}

std::vector<LHCb::Event::Calo::Digit> CaloFutureHypo2CaloFuture::cellIDs( const DeCalorimeter&             fromCalo,
                                                                          const LHCb::CaloCluster&         fromCluster,
                                                                          const DeCalorimeter&             toCalo,
                                                                          const LHCb::Event::Calo::Digits& digits,
                                                                          LHCb::Detector::Calo::CellID lineID ) const {
  std::vector<LHCb::Event::Calo::Digit> output;
  LHCb::Detector::Calo::CellID          seedID = fromCluster.seed();

  if ( msgLevel( MSG::DEBUG ) ) debug() << "-----  cluster energy " << fromCluster.e() << " " << seedID << endmsg;
  CellNeighbour neighbour;
  neighbour.setDet( &fromCalo );

  // matching cluster
  for ( const LHCb::CaloClusterEntry& entry : fromCluster.entries() ) {
    LHCb::Detector::Calo::CellID cellID = entry.digit()->cellID();
    if ( !( m_seed && entry.status().test( LHCb::CaloDigitStatus::Mask::SeedCell ) ) &&
         !( m_seed && m_neighb && !LHCb::essentiallyZero( neighbour( seedID, cellID ) ) ) &&
         entry.status().noneOf( LHCb::CaloDigitStatus::Status( m_status.value() ) ) && !( m_whole ) )
      continue;
    std::cout << ">>two " << entry.digit()->cellID() << '\n';
    output = cellIDs_( fromCalo, entry.digit()->cellID(), toCalo, digits, std::move( output ) );
    std::cout << ">>two\n";
    if ( msgLevel( MSG::DEBUG ) )
      debug() << toCalo.index() << ":  digit is selected in front of the cluster : " << cellID << "/" << seedID << " "
              << output.size() << endmsg;
  }
  // photon line
  if ( m_line ) {
    auto point = Gaudi::XYZPoint();
    if ( !lineID ) {
      const auto& point = fromCluster.position();
      lineID            = intersect( toCalo, { point.x(), point.y(), point.z() } );
    }
    if ( lineID ) {
      assert( lineID.calo() == toCalo.index() );
      output = addCell( lineID, digits, std::move( output ) );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << toCalo.index() << " : digit is selected in the photon line : " << lineID << "/" << seedID << " "
                << output.size() << endmsg;
      if ( m_neighb ) {
        for ( const auto& n : toCalo.neighborCells( lineID ) ) {
          assert( n.calo() == toCalo.index() );
          double                halfCell   = toCalo.cellSize( n ) * 0.5;
          const Gaudi::XYZPoint cellCenter = toCalo.cellCenter( n );
          if ( msgLevel( MSG::DEBUG ) )
            debug() << n << " Point : (" << point.X() << "," << point.Y() << " Cell :  ( " << cellCenter.X() << ","
                    << cellCenter.Y() << " size/2  : " << halfCell << " Tolerance : " << m_x << "/" << m_y << endmsg;
          if ( fabs( point.X() - cellCenter.X() ) < ( halfCell + m_x ) &&
               fabs( point.Y() - cellCenter.Y() ) < ( halfCell + m_y ) ) {
            output = addCell( n, digits, std::move( output ) );
            if ( msgLevel( MSG::DEBUG ) )
              debug() << toCalo.index() << " : digit is selected in the photon line neighborhood : " << n << "/"
                      << seedID << " " << output.size() << endmsg;
          }
        }
      }
    }
  }
  return output;
}
