/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Math/Point3Dfwd.h"

#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigits_v2.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Plane3DTypes.h"

#include <vector>

/** @class CaloFuture2CaloFuture CaloFuture2CaloFuture.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2007-05-29
 */

namespace {
  inline std::vector<LHCb::Event::Calo::Digit> addCell( const LHCb::Detector::Calo::CellID&     id,
                                                        const LHCb::Event::Calo::Digits&        input,
                                                        std::vector<LHCb::Event::Calo::Digit>&& output ) {
    if ( output.end() ==
         std::find_if( output.begin(), output.end(), [id]( const auto& d ) { return d.cellID() == id; } ) ) {
      if ( auto digit = input( id ); digit ) output.push_back( *digit );
    }
    return std::move( output );
  }
} // namespace

class CaloFuture2CaloFuture : public GaudiTool {
public:
  /// Standard constructor
  using GaudiTool::GaudiTool;

  LHCb::Detector::Calo::CellID intersect( const DeCalorimeter& calo, Gaudi::XYZPoint ref ) const {
    const Gaudi::XYZVector vec = ( ref - Gaudi::XYZPoint( 0, 0, 0 ) );
    Gaudi::Math::XYZLine   line( ref, vec );
    double                 mu{};
    auto                   point = Gaudi::XYZPoint();
    Gaudi::Math::intersection( line, calo.plane( CaloPlane::ShowerMax ), point, mu );
    return calo.Cell( point );
  }

protected:
  // CellIDs
  std::vector<LHCb::Event::Calo::Digit> cellIDs( const DeCalorimeter& fromCalo, const LHCb::CaloCluster& fromCluster,
                                                 const DeCalorimeter&             toCalo,
                                                 const LHCb::Event::Calo::Digits& digits ) const;
  std::vector<LHCb::Event::Calo::Digit> cellIDs_( const DeCalorimeter&                fromCalo,
                                                  const LHCb::Detector::Calo::CellID& fromId,
                                                  const DeCalorimeter& toCalo, const LHCb::Event::Calo::Digits& digits,
                                                  std::vector<LHCb::Event::Calo::Digit>&& container ) const;

  // Calo Maps
  template <typename T>
  class Map {
    std::array<T, 2> content = {};

  public:
    Map() = default;
    template <typename Args>
    Map( Args&& ecal, Args&& hcal )
        : content{ std::make_from_tuple<T>( std::forward<Args>( ecal ) ),
                   std::make_from_tuple<T>( std::forward<Args>( hcal ) ) } {}
    T& operator[]( LHCb::Detector::Calo::CellCode::Index idx ) {
      switch ( idx ) {
      case LHCb::Detector::Calo::CellCode::Index::EcalCalo:
        return content[0];
      case LHCb::Detector::Calo::CellCode::Index::HcalCalo:
        return content[1];
      default:
        throw std::out_of_range( "CaloFuture2CaloFuture: invalid calorimeter requested" );
      }
    }
    const T& operator[]( LHCb::Detector::Calo::CellCode::Index idx ) const {
      switch ( idx ) {
      case LHCb::Detector::Calo::CellCode::Index::EcalCalo:
        return content[0];
      case LHCb::Detector::Calo::CellCode::Index::HcalCalo:
        return content[1];
      default:
        throw std::out_of_range( "CaloFuture2CaloFuture: invalid calorimeter requested" );
      }
    }
  };

private:
  Gaudi::Property<bool> m_geo{ this, "IdealGeometry", true };
};
