/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFuture2CaloFuture.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloFutureDataFunctor.h"
#include "Detector/Calo/CaloCellCode.h"
// ============================================================================
/** @file
 *  Implementation file for class : CaloFuture2CaloFuture
 *  @date 2007-05-29
 *  @author Olivier Deschamps
 */
// ============================================================================

std::vector<LHCb::Event::Calo::Digit> CaloFuture2CaloFuture::cellIDs( const DeCalorimeter&             fromCalo,
                                                                      const LHCb::CaloCluster&         fromCluster,
                                                                      const DeCalorimeter&             toCalo,
                                                                      const LHCb::Event::Calo::Digits& digits ) const {
  std::vector<LHCb::Event::Calo::Digit> output;
  const auto&                           entries = fromCluster.entries();
  output.reserve( 2 * entries.size() );
  for ( const auto& ent : entries ) {
    if ( const LHCb::CaloDigit* digit = ent.digit(); digit ) {
      output = cellIDs_( fromCalo, digit->cellID(), toCalo, digits, std::move( output ) );
    }
  }
  return output;
}

//=======================================================================================================
std::vector<LHCb::Event::Calo::Digit>
CaloFuture2CaloFuture::cellIDs_( const DeCalorimeter& fromCalo, const LHCb::Detector::Calo::CellID& fromId,
                                 const DeCalorimeter& toCalo, const LHCb::Event::Calo::Digits& digits,
                                 std::vector<LHCb::Event::Calo::Digit>&& container ) const {

  // detector variables
  auto fromIndex = fromCalo.index();
  auto toIndex   = toCalo.index();
  // CellSize reference (outer section)  Warning : factor 2 for Hcal
  auto refSizeFrom = fromCalo.cellParams().front().size();
  auto refSizeTo   = toCalo.cellParams().front().size();
  if ( fromIndex == LHCb::Detector::Calo::CellCode::Index::HcalCalo ) refSizeFrom /= 2.;
  if ( toIndex == LHCb::Detector::Calo::CellCode::Index::HcalCalo ) refSizeTo /= 2.;

  LHCb::Detector::Calo::CellID toId = fromId;

  // ---- Assume ideal geometry : trivial mapping for detectors having the same granularity (Prs/Spd/Ecal)
  if ( ( m_geo && fromIndex == LHCb::Detector::Calo::CellCode::Index::EcalCalo &&
         toIndex == LHCb::Detector::Calo::CellCode::Index::EcalCalo ) ||
       fromIndex == toIndex ) {
    toId.setCalo( toIndex );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Add cell from trivial mapping" << endmsg;
    return addCell( toId, digits, std::move( container ) );
  }

  // ---- Else use the actual geometry to connet detectors
  double          scale  = 1.;
  Gaudi::XYZPoint center = fromCalo.cellCenter( fromId );
  if ( fromIndex != toIndex ) {
    // z-scaling
    scale  = refSizeTo / refSizeFrom;
    center = toCalo.plane( CaloPlane::ShowerMax ).ProjectOntoPlane( fromCalo.cellCenter( fromId ) * scale );
    // connect
    toId = toCalo.Cell( center );
  }
  auto fromSize = fromCalo.cellSize( fromId ) * scale;
  // cell-center is outside 'toCalo' - check corners
  if ( !toId ) {
    for ( int i = 0; i != 2; ++i ) {
      for ( int j = 0; j != 2; ++j ) {
        double x = fromCalo.cellCenter( fromId ).X() + ( i * 2 - 1 ) * fromSize;
        double y = fromCalo.cellCenter( fromId ).Y() + ( j * 2 - 1 ) * fromSize;
        if ( auto cornerId = toCalo.Cell( { x, y, center.Z() } ); cornerId ) toId = cornerId;
      }
    }
  }
  if ( !toId ) return std::move( container );
  int    pad = 1;
  double x0  = center.X();
  double y0  = center.Y();
  if ( fromIndex != toIndex ) {
    double toSize = toCalo.cellSize( toId );
    pad           = std::max( 1, (int)floor( fromSize / toSize + 0.25 ) ); // warning int precision
    x0            = center.X() - ( pad - 1 ) * fromSize / 2. / pad;
    y0            = center.Y() - ( pad - 1 ) * fromSize / 2. / pad;
  }
  for ( int i = 0; i != pad; ++i ) {
    for ( int j = 0; j != pad; ++j ) {
      if ( fromIndex != toIndex )
        toId = toCalo.Cell( { x0 + i * fromSize / pad, y0 + j * fromSize / pad, center.Z() } );
      if ( !toId ) continue;
      assert( toId.calo() == toIndex );
      container = addCell( toId, digits, std::move( container ) );
    }
  }
  return std::move( container );
}
