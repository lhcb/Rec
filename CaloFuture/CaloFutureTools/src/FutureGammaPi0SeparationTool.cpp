/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "IGammaPi0SeparationTool.h"
#include <cmath>

//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0SeparationTool
//
// 2019-03-28 : Miriam Calvo Gomez
//-----------------------------------------------------------------------------

#include "TMV_MLP_inner.C"
#include "TMV_MLP_middle.C"
#include "TMV_MLP_outer.C"

/** @class FutureGammaPi0SeparationTool FutureGammaPi0SeparationTool.h
 *
 *
 *  @author Miriam Calvo Gomez
 *  @date   2021-06-28
 */
namespace LHCb::Calo {
  namespace {
    // TMVA discriminant
    constexpr auto inputVars = std::array<std::string_view, 10>{ "fracE1", "fracE2", "fracE3", "fracE4", "fracEseed",
                                                                 "fracE6", "fracE7", "fracE8", "fracE9", "Et" };

    const ReadMLPOuter  s_reader0{ inputVars };
    const ReadMLPMiddle s_reader1{ inputVars };
    const ReadMLPInner  s_reader2{ inputVars };
  } // namespace

  class GammaPi0Separation : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double>      isPhoton( const LHCb::CaloHypo&                hypo,
                                         const LHCb::Event::Calo::v2::Digits& digits ) const override;
    std::optional<double>      isPhoton( Observables const& observables ) const override;
    std::optional<Observables> observables( const CaloHypo&                      hypo,
                                            const LHCb::Event::Calo::v2::Digits& digits ) const override;

  private:
    Gaudi::Property<float> m_minPt{ this, "MinPt", 2000. }; // there are very few merged pi0 below 2 GeV
  };
  DECLARE_COMPONENT_WITH_ID( GammaPi0Separation, "FutureGammaPi0SeparationTool" )

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode GammaPi0Separation::initialize() {

    return extends::initialize().andThen( [&] {} );
  }

  //=============================================================================
  // Main execution
  //=============================================================================

  std::optional<double> GammaPi0Separation::isPhoton( const LHCb::CaloHypo&                hypo,
                                                      const LHCb::Event::Calo::v2::Digits& digits ) const {
    auto observables_ = observables( hypo, digits );
    if ( !observables_ ) {
      return {};
    } else {
      return isPhoton( *observables_ );
    }
  }

  std::optional<double> GammaPi0Separation::isPhoton( Observables const& observables ) const {
    if ( observables.Et < m_minPt ) return {};
    const auto input = std::array{ observables.fracE1,    observables.fracE2,    observables.fracE3, observables.fracE4,
                                   observables.fracEseed, observables.fracE6,    observables.fracE7, observables.fracE8,
                                   observables.fracE9,    observables.Et / 1000. };
    switch ( observables.area ) {
    case 0:
      return s_reader0.GetMvaValue( input );
    case 1:
      return s_reader1.GetMvaValue( input );
    case 2:
      return s_reader2.GetMvaValue( input );
    default:
      return {};
    }
  }

  std::optional<Interfaces::IGammaPi0Separation::Observables>
  GammaPi0Separation::observables( const LHCb::CaloHypo& hypo, const LHCb::Event::Calo::v2::Digits& digits ) const {

    // compute observables for any Et range, as same inputs are used for IsNotH (low energy photons)
    double Et = LHCb::Calo::Momentum( &hypo ).pt();

    const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo );
    if ( !cluster ) return {};

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Inside IsPhoton observables ------" << endmsg;

    double Ecl{ 0. };
    int    area{ -1 };
    double fracEseed{ 0. };
    double fracE1{ 0. };
    double fracE2{ 0. };
    double fracE3{ 0. };
    double fracE4{ 0. };
    double fracE6{ 0. };
    double fracE7{ 0. };
    double fracE8{ 0. };
    double fracE9{ 0. };

    Detector::Calo::CellID seedID = cluster->seed();
    area                          = seedID.area();

    // Compute 3x3 energy from raw digits, so it's independent from the cluster-shape used for reconstruction (2x2,
    // cross-shape, ...)

    // numeration of the 3x3 matrix used
    //  -----------
    //  |7 | 8  |9 |
    //  -----------
    //  |4 |Seed|6 |
    //  -----------
    //  |1 | 2  |3 |
    //  ------------

    std::array<float, 9> rawEnergyVector{ 0 };
    auto                 rawEnergy = [&rawEnergyVector]( int i, int j ) -> float& {
      return rawEnergyVector[( i + 1 ) * 3 + ( j + 1 )];
    };

    constexpr auto offsets = std::array{ -1, 0, 1 };
    for ( auto col_number : offsets ) {
      for ( auto row_number : offsets ) {
        const auto id = Detector::Calo::CellID{ seedID.calo(), seedID.area(), seedID.row() + row_number,
                                                seedID.col() + col_number };
        if ( !isValid( id ) ) continue;
        if ( auto digit = digits( id ); digit ) {
          rawEnergy( col_number, row_number ) = digit->energy();
          Ecl += digit->energy();
        }
      }
    }

    if ( Ecl > 0. ) {
      fracEseed = rawEnergy( 0, 0 ) / Ecl;
      fracE1    = rawEnergy( -1, -1 ) / Ecl;
      fracE2    = rawEnergy( 0, -1 ) / Ecl;
      fracE3    = rawEnergy( 1, -1 ) / Ecl;
      fracE4    = rawEnergy( -1, 0 ) / Ecl;
      fracE6    = rawEnergy( 1, 0 ) / Ecl;
      fracE7    = rawEnergy( -1, 1 ) / Ecl;
      fracE8    = rawEnergy( 0, 1 ) / Ecl;
      fracE9    = rawEnergy( 1, 1 ) / Ecl;
    }

#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif
    Observables observables{ .fracE1    = fracE1,
                             .fracE2    = fracE2,
                             .fracE3    = fracE3,
                             .fracE4    = fracE4,
                             .fracEseed = fracEseed,
                             .fracE6    = fracE6,
                             .fracE7    = fracE7,
                             .fracE8    = fracE8,
                             .fracE9    = fracE9,
                             .Et        = Et,
                             .Ecl       = Ecl,
                             .area      = area };
#pragma GCC diagnostic pop

    return observables;
  }
} // namespace LHCb::Calo
