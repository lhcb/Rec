/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/GaudiTool.h"
#include "INeutralIDTool.h"
#include "TMV_MLP_H_inner.C"
#include "TMV_MLP_H_middle.C"
#include "TMV_MLP_H_outer.C"

/** @class neutralIDTool neutralIDTool.h
 *
 *
 *  @author Miriam Calvo
 *  @date   2021-07-05
 */
namespace LHCb::Calo {

  namespace {
    constexpr auto inputVarNames = std::array<std::string_view, 9>{ "fracE1", "fracE2", "fracE3", "fracE4", "fracEseed",
                                                                    "fracE6", "fracE7", "fracE8", "fracE9" };

    const ReadMLPHInner  s_reader2{ inputVarNames };
    const ReadMLPHMiddle s_reader1{ inputVarNames };
    const ReadMLPHOuter  s_reader0{ inputVarNames };

  } // namespace

  class NeutralIDTool final : public extends<GaudiTool, Interfaces::INeutralID> {
  public:
    /// Standard constructor
    using extends::extends;

    std::optional<double> isNotH( const LHCb::CaloHypo&                      hypo,
                                  const Interfaces::INeutralID::Observables& v ) const override {
      if ( Momentum( &hypo ).pt() <= m_minPt || !LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo, false ) )
        return std::nullopt;
      return isNotH( v );
    }

    double isNotH( const Interfaces::INeutralID::Observables& v ) const override {
      const auto input =
          std::array{ v.e1frac, v.e2frac, v.e3frac, v.e4frac, v.eseedfrac, v.e6frac, v.e7frac, v.e8frac, v.e9frac };

      switch ( v.area ) {
      case 0:
        return s_reader0.GetMvaValue( input );
      case 1:
        return s_reader1.GetMvaValue( input );
      case 2:
        return s_reader2.GetMvaValue( input );
      default:
        return {};
      }
    };

  private:
    Gaudi::Property<float> m_minPt{ this, "MinPt", 75. };
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( NeutralIDTool, "FutureNeutralIDTool" )

} // namespace LHCb::Calo
