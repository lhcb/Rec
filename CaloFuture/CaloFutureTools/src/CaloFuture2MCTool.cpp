/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureInterfaces/ICaloFuture2MCTool.h" // Interface
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloFutureUtils/CaloFutureMCTools.h"
#include "CaloFutureUtils/CaloFutureParticle.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Core/FloatComparison.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/Incident.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "Linker/LinkedTo.h"
#include "Relations/RelationWeighted1D.h"
#include "boost/algorithm/string.hpp"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFuture2MCTool
//
// 2009-07-27 : Olivier Deschamps
//-----------------------------------------------------------------------------

/** @class CaloFuture2MCTool CaloFuture2MCTool.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2009-07-27
 */
class CaloFuture2MCTool : public extends<GaudiTool, ICaloFuture2MCTool, IIncidentListener> {

public:
  // category enum

  enum MCCategory {
    /* Single cluster/hypo categories */
    UnMatched       = 0,
    Photon          = 1,
    BremStrahlung   = 2,
    Electron        = 3,
    ConvertedPhoton = 4,
    MergedPi0       = 5,
    ChargedHadron   = 6,
    NeutralHadron   = 7,
    Spillover       = 8
  };

  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  //
  ICaloFuture2MCTool*     from( const LHCb::CaloDigit* digit ) override;
  ICaloFuture2MCTool*     from( const LHCb::CaloCluster* cluster ) override;
  ICaloFuture2MCTool*     from( const LHCb::CaloHypo* hypo ) override;
  ICaloFuture2MCTool*     from( const LHCb::ProtoParticle* proto ) override;
  ICaloFuture2MCTool*     from( const LHCb::Particle* particle ) override;
  const LHCb::MCParticle* bestMC() const override;
  const LHCb::MCParticle* maxMC() const override;
  double                  weight( const LHCb::MCParticle* ) const override;
  const LHCb::MCParticle* findMC( LHCb::ParticleID id, double threshold = 0 ) const override;
  const LHCb::MCParticle* findMCOrBest( LHCb::ParticleID id, double threshold = 0 ) const override;
  const LHCb::MCParticle* findMC( std::string name, double threshold = 0 ) const override;
  const LHCb::MCParticle* findMCOrBest( std::string name, double threshold = 0 ) const override;
  double                  quality( const LHCb::MCParticle* ) const override;
  std::string             descriptor() const override;
  bool                    isCaloFuture( LHCb::Particle* particle ) const override {
    return particle && LHCb::Calo::CaloParticle( particle ).isCaloFuture();
  }
  bool isPureNeutralCaloFuture( const LHCb::Particle* particle ) const override { // SHOULD BE IN CALOFUTUREPARTICLE
    return particle && LHCb::Calo::CaloParticle( const_cast<LHCb::Particle*>( particle ) )
                           .isPureNeutralCaloFuture(); // non pure
                                                       // calorimetric
                                                       // object
  }

  /// Inform that a new incident has occurred
  void handle( const Incident& /* inc */ ) override { clear(); }

  // TO BE INTERFACED :
  int                 MCCategory() { return m_category; };
  ICaloFuture2MCTool* fragment( unsigned int i );
  unsigned int        numberOfFragments() { return m_nFrag; }
  // ostream << category
  // clusters()
  // hypos()
  // protos()

private:
  StatusCode process();
  void       addDigit( const LHCb::CaloDigit* digit );
  void       addCluster( const LHCb::CaloCluster* cluster );
  void       addHypo( const LHCb::CaloHypo* hypo );
  void       addProto( const LHCb::ProtoParticle* proto, const LHCb::Particle* parent = NULL );
  void       clear();
  void       mcDigest();
  void       mcTree( const LHCb::MCParticle* part, std::vector<const LHCb::MCParticle*>& tree, std::string& sTree );

  std::vector<const LHCb::CaloDigit*>     m_digits;
  std::vector<const LHCb::CaloCluster*>   m_clusters;
  std::vector<const LHCb::CaloHypo*>      m_hypos;
  std::vector<const LHCb::ProtoParticle*> m_protos;
  std::vector<const LHCb::Particle*>      m_parts;
  LHCb::CaloDigit*                        m_digit   = nullptr;
  LHCb::CaloCluster*                      m_cluster = nullptr;
  LHCb::CaloHypo*                         m_hypo    = nullptr;
  LHCb::ProtoParticle*                    m_proto   = nullptr;
  LHCb::Particle*                         m_part    = nullptr;
  CaloFutureMCTools::CaloFutureMCMap      m_mcMap;
  //
  Gaudi::Property<std::string> m_cluster2MCLoc{ this, "Cluster2MCTable",
                                                "Relations/" + LHCb::CaloClusterLocation::Default,
                                                "Cluster->MC relation table location" };

  Gaudi::Property<std::string> m_digit2MCLoc{ this, "Digit2MCTable", "Relations/" + LHCb::CaloDigitLocation::Default,
                                              "Digit->MC relation table location" };

  LHCb::CaloFuture2MC::IClusterTable*                         m_cluster2MC = nullptr;
  LHCb::CaloFuture2MC::IDigitTable*                           m_digit2MC   = nullptr;
  double                                                      m_sum        = 0.;
  const LHCb::MCParticle*                                     m_maxMC      = nullptr;
  const LHCb::MCParticle*                                     m_bestMC     = nullptr;
  std::map<std::string, std::vector<const LHCb::MCParticle*>> m_treeMap;
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppsvc{ this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc" };

  //
  Gaudi::Property<bool> m_hypo2Cluster{ this, "Hypo2Cluster", false,
                                        "(part->protoP)->hypo->cluster cascade ( or use  hypo->MC linker tables)" };

  Gaudi::Property<bool> m_cluster2Digit{
      this, "Cluster2Digits", false,
      "(part->protoP)->hypo->cluster->digit cascade ( or use cluster->MC relation tables)" };

  Gaudi::Property<bool> m_merged2Split{ this, "Merged2Split", false,
                                        "expert usage (merged->split->cluster - NOT IMPLEMENTED so far)" };

  Gaudi::Property<int> m_sFilter{ this, "DigitStatusFilter",
                                  static_cast<int>( LHCb::CaloDigitStatus::Mask::UseForEnergy ),
                                  "digit filter in case of ..->digit->MC table is used" };

  mutable Gaudi::Accumulators::Counter<> m_nProtoAppearsTwice{ this,
                                                               "ProtoParticle appears twice in the same decay chain" };
  mutable Gaudi::Accumulators::Counter<> m_nHypoAppearsTwice{ this,
                                                              "CaloFutureHypo appears twice in the same decay chain" };
  mutable Gaudi::Accumulators::Counter<> m_nHypo2Cluster{ this, "!! Hypo->Cluster" };
  mutable Gaudi::Accumulators::Counter<> m_nHypo2MC{ this, "CaloFutureHypo->MC matching" };
  mutable Gaudi::Accumulators::Counter<> m_nIllDefHypo{ this, "ill-defined CaloFutureHypo" };

  int                                  m_category = 0;
  int                                  m_depth    = -1;
  unsigned int                         m_nFrag    = 0;
  PublicToolHandle<ICaloFuture2MCTool> m_tool     = {
      this, "MCTool", "CaloFuture2MCTool/CaloFutureFragment2MCTool" }; // CAUTION : THE TOOL CANNOT BE PRIVATE !!
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( CaloFuture2MCTool )

//=============================================================================

StatusCode CaloFuture2MCTool::initialize() {
  StatusCode sc = base_class::initialize();
  if ( sc.isFailure() ) return Error( "Failed to initialize", sc );

  // incidentSvc
  IIncidentSvc* inc = incSvc();
  if ( 0 != inc ) inc->addListener( this, IncidentType::BeginEvent );

  // init
  m_digit      = nullptr;
  m_cluster    = nullptr;
  m_hypo       = nullptr;
  m_proto      = nullptr;
  m_part       = nullptr;
  m_digit2MC   = nullptr;
  m_cluster2MC = nullptr;
  m_category   = 0;
  m_depth      = -1;
  // m_hypo2MC = nullptr;
  // get fragment tool and propagate properties -- FIXME: changing properties of a _public_ tool is not very nice...
  m_tool.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //@TODO: dynamic_cast to IProperty... remove IProperty from ICaloFuture2MCTool...
  m_tool->setProperty( "Cluster2MCTable", m_cluster2MCLoc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_tool->setProperty( "Digit2MCTable", m_digit2MCLoc ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_tool->setProperty( "Cluster2Digits", Gaudi::Utils::toString( m_cluster2Digit.value() ) )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_tool->setProperty( "Hypo2Cluster", Gaudi::Utils::toString( m_hypo2Cluster.value() ) )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_tool->setProperty( "Merged2Split", Gaudi::Utils::toString( m_merged2Split.value() ) )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  m_tool->setProperty( "DigitStatusFilter", Gaudi::Utils::toString( m_sFilter.value() ) )
      .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

  m_sum    = 0.;
  m_maxMC  = nullptr;
  m_bestMC = nullptr;

  //
  if ( m_hypo2Cluster ) {
    Warning( " ... Hypo->Cluster reference is used in CaloFuture2MC  ", StatusCode::SUCCESS ).ignore();
    Warning( " ... CaloFutureCluster  will be re-processed (require full DST)", StatusCode::SUCCESS ).ignore();
    Warning( " ... assume an identical reconstruction version  ", StatusCode::SUCCESS ).ignore();
  }
  //
  clear();
  return StatusCode::SUCCESS;
}

/*-------------------------- from Part to MC  ------------------------*/
// associate single particle
ICaloFuture2MCTool* CaloFuture2MCTool::from( const LHCb::Particle* part ) {
  if ( part == m_part ) return this; // process only if needed

  clear();
  m_depth = 4; // particle level
  if ( !part ) return this;

  // check the particle is full calorimetric (using caloParticle functionalities)
  auto cPart = LHCb::Calo::CaloParticle( (LHCb::Particle*)part );
  if ( !cPart.isCaloFuture() ) {
    Warning( "Cannot associate non-pure calorimetric particle to MC" ).ignore();
    return this;
  }

  // register final state particles
  m_parts = cPart.caloEndTree();
  m_part  = const_cast<LHCb::Particle*>( part );
  if ( m_parts.empty() ) m_parts.push_back( part );

  // particle->protoparticle cascade (mandatory)
  m_depth = 3; // protoparticle level
  for ( const auto& fs : m_parts ) {
    const LHCb::ProtoParticle* proto = fs->proto();
    if ( !proto ) {
      Warning( "ProtoParticle point to NULL (should not)" ).ignore();
      continue;
    }
    addProto( proto, fs );
  }
  StatusCode sc = process();
  if ( sc.isFailure() ) Warning( "Processing CaloFuture2MCTool from Particle failed" ).ignore();
  return this;
}

/*-------------------------- from Proto to MC  ------------------------*/
// Accumulate protos->hypo()s [->clusters]
void CaloFuture2MCTool::addProto( const LHCb::ProtoParticle* proto, const LHCb::Particle* parent ) {
  if ( !proto ) return;

  // register proto if not yet done
  bool ok =
      std::none_of( m_protos.begin(), m_protos.end(), [&]( const LHCb::ProtoParticle* p ) { return p == proto; } );
  if ( !ok ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "ProtoParticle appears twice in the same decay chain" << endmsg;
    ++m_nProtoAppearsTwice;
    return;
  }
  m_protos.push_back( proto );

  // proto->hypo cascade (mandatory)
  m_depth           = 2; // hypo level
  const auto& hypos = proto->calo();
  if ( hypos.empty() ) return;

  // special treatment for Bremstrahlung electrons
  bool charged = ( proto->track() ); /* charged part->proto */
  if ( !charged ) {
    for ( const auto& hypo : hypos ) addHypo( hypo );
  } else {
    bool brem = ( parent && ( std::abs( parent->momentum().P() - proto->track()->firstState().p() ) >
                              1E-4 ) ); /* bremstrahlung corrected particle */
    for ( const auto& hypo : hypos ) {
      if ( hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::EmCharged ||
           ( brem && hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::Photon ) )
        addHypo( hypo );
    }
  }
}

// associate single protoParticle
ICaloFuture2MCTool* CaloFuture2MCTool::from( const LHCb::ProtoParticle* proto ) {
  if ( proto == m_proto ) return this; // process only if needed
  clear();
  m_depth = 3; // proto level
  if ( !proto ) return this;
  m_proto = const_cast<LHCb::ProtoParticle*>( proto );
  addProto( proto );
  StatusCode sc = process();
  if ( sc.isFailure() ) Warning( "Processing CaloFuture2MCTool from ProtoParticle failed" ).ignore();
  return this;
}

/*-------------------------- from Hypo to MC  ------------------------*/
// Accumulate hypos [->clusters]
void CaloFuture2MCTool::addHypo( const LHCb::CaloHypo* hypo ) {
  if ( !hypo ) return;
  // register hypo if not yet done
  bool ok = std::none_of( m_hypos.begin(), m_hypos.end(), [&]( const auto& h ) { return h == hypo; } );
  if ( !ok ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "CaloFutureHypo appears twice in the same decay chain" << endmsg;
    ++m_nHypoAppearsTwice;
    return;
  }
  m_hypos.push_back( hypo );
  m_depth = 2;

  // ----- hypo->MC association :
  if ( !m_hypo2Cluster ) {
    // 2 - get the relevant linker
    std::string loc = ( hypo->parent() && hypo->parent()->registry() ) ? hypo->parent()->registry()->identifier() : "";
    auto        links = SmartDataPtr<LHCb::LinksByKey>{ evtSvc(), LHCb::LinksByKey::linkerName( loc ) };
    if ( !links ) {
      Warning( "No Hypo2MC link at '" + loc + "' ", StatusCode::SUCCESS, 1 ).ignore();
      Warning( " ... try using Hypo->Cluster reference  ", StatusCode::SUCCESS, 1 ).ignore();
      Warning( " ... CaloFutureCluster  will be re-processed (require full DST)", StatusCode::SUCCESS, 1 ).ignore();
      Warning( " ... assume an identical reconstruction version  ", StatusCode::SUCCESS, 1 ).ignore();
      ++m_nHypo2Cluster;
      m_hypo2Cluster = true;
    } // ===> force hypo->cluster cascade
    else {
      // - built (particle,weight) map
      for ( auto const& [p, w] : LinkedTo<LHCb::MCParticle>{ links }.weightedRange( hypo ) ) m_mcMap[&p] += w;
      if ( hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged )
        m_sum += LHCb::Calo::Momentum( hypo ).e();
      else
        m_sum += hypo->e();
      ++m_nHypo2MC;
    }
  }

  // hypo->cluster->MC cascade if requested/needed
  if ( m_hypo2Cluster ) {
    m_depth                                          = 1; // cluster level
    const SmartRefVector<LHCb::CaloCluster> clusters = hypo->clusters();
    if ( clusters.empty() ) return;
    const LHCb::CaloCluster* cluster =
        ( clusters.size() > 1 && hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
            ? *( clusters.begin() + 1 )
            : *( clusters.begin() ); //@ToDO use cluster::Type (when defined)
    if ( !cluster ) return;
    if ( clusters.size() != 1 && hypo->hypothesis() != LHCb::CaloHypo::Hypothesis::PhotonFromMergedPi0 )
      ++m_nIllDefHypo;
    addCluster( cluster );
  }
  return;
}
// associate single hypo
ICaloFuture2MCTool* CaloFuture2MCTool::from( const LHCb::CaloHypo* hypo ) {
  if ( hypo == m_hypo ) return this; // process only if needed
  clear();
  m_depth = 2; // hypo level
  if ( !hypo ) return this;
  m_hypo = const_cast<LHCb::CaloHypo*>( hypo );
  // special case for MergedPi0
  if ( hypo->hypothesis() == LHCb::CaloHypo::Hypothesis::Pi0Merged && m_merged2Split ) {
    const auto& hyps = hypo->hypos();
    if ( hyps.empty() ) return this;
    addHypo( hyps.front() );               // splitPhoton1
    addHypo( *std::next( hyps.begin() ) ); // splitPhoton2
  } else {
    addHypo( hypo ); // mother CaloFutureHypo
  }
  StatusCode sc = process();
  if ( sc.isFailure() ) Warning( "Processing CaloFuture2MCTool from CaloFutureHypo failed" ).ignore();
  return this;
}

/*-------------------------- from Cluster to MC  ------------------------*/
// Accumulate clusters [->digits]
void CaloFuture2MCTool::addCluster( const LHCb::CaloCluster* ) {
  error() << " CaloFuture2MCTool::addCluster -- no longer available" << endmsg;
}
// associate single cluster
ICaloFuture2MCTool* CaloFuture2MCTool::from( const LHCb::CaloCluster* cluster ) {
  if ( cluster == m_cluster ) return this; // process only if needed
  clear();
  m_depth = 1; // cluster level
  if ( !cluster ) return this;
  m_cluster = (LHCb::CaloCluster*)cluster;
  //
  addCluster( cluster );
  //
  StatusCode sc = process();
  if ( sc.isFailure() ) Warning( "Processing CaloFuture2MCTool from CaloFutureCluster failed" ).ignore();
  return this;
}

/*-------------------------- from Digit to MC  ------------------------*/
void CaloFuture2MCTool::addDigit( const LHCb::CaloDigit* ) {
  error() << " CaloFuture2MCTool::addDigit -- no longer available" << endmsg;
}

// associate single digit
ICaloFuture2MCTool* CaloFuture2MCTool::from( const LHCb::CaloDigit* digit ) {
  if ( digit == m_digit ) return this; // process only if needed
  clear();
  m_depth = 0; // digit level
  if ( !digit ) return this;
  m_digit = const_cast<LHCb::CaloDigit*>( digit );
  m_digits.push_back( digit );
  StatusCode sc = process();
  if ( sc.isFailure() ) Warning( "Processing CaloFuture2MCTool from CaloDigit failed" ).ignore();
  return this;
}

/*-------------------------- Generic processing ------------------------*/
StatusCode CaloFuture2MCTool::process() {
  mcDigest();
  verbose() << " Processing CaloFuture2MCTool " << std::endl << descriptor() << endmsg;
  return StatusCode::SUCCESS;
}

void CaloFuture2MCTool::clear() {
  m_mcMap.clear();
  m_treeMap.clear();
  m_digits.clear();
  m_clusters.clear();
  m_hypos.clear();
  m_protos.clear();
  m_parts.clear();
  m_nFrag      = 0;
  m_sum        = 0.;
  m_maxMC      = nullptr;
  m_bestMC     = nullptr;
  m_digit      = nullptr;
  m_cluster    = nullptr;
  m_hypo       = nullptr;
  m_proto      = nullptr;
  m_part       = nullptr;
  m_digit2MC   = nullptr;
  m_cluster2MC = nullptr;
  m_category   = 0;
  m_depth      = -1;
}

void CaloFuture2MCTool::mcDigest() {

  double mcMax  = 0.;
  double mcBest = 0.;
  m_maxMC       = nullptr;
  m_bestMC      = nullptr;

  if ( m_sum <= 0 ) return;
  // loop over contributing particle :
  for ( const auto& imap : m_mcMap ) {
    const LHCb::MCParticle* mcPart = imap.first;
    double                  w      = weight( mcPart );
    double                  q      = quality( mcPart );
    double                  m      = mcPart->momentum().M();
    // the most contributing MCParticle (with smallest mass when several MCPart with same weight)
    if ( w >= mcMax ) {
      bool ok = true;
      if ( m_maxMC && LHCb::essentiallyEqual( w, mcMax ) && m > m_maxMC->momentum().M() ) ok = false;
      if ( ok ) {
        mcMax   = w;
        m_maxMC = mcPart;
      }
    }
    // the best matching MCParticle
    if ( q >= mcBest ) {
      mcBest   = q;
      m_bestMC = mcPart;
    }

  } // end loop over MCParticles
  // build MC tree
  // 1- get related MC particle (seed) without any descendant listed in the related mcParticles
  auto hasProd = [&]( const LHCb::MCParticle* mcp ) {
    const auto& vertices = mcp->endVertices();
    return std::any_of( vertices.begin(), vertices.end(), [&]( const auto& v ) {
      return std::any_of( v->products().begin(), v->products().end(),
                          [&]( const auto& p ) { return m_mcMap.find( p ) != m_mcMap.end(); } );
    } );
  };
  std::vector<const LHCb::MCParticle*> seeds;
  for ( const auto& imap : m_mcMap ) {
    const LHCb::MCParticle* mcPart = imap.first;
    int                     _pID   = mcPart->particleID().abspid();
    // include electron with brems and converted photons as seeds
    if ( _pID == 11 || _pID == 22 || !hasProd( mcPart ) ) seeds.push_back( mcPart );
  }

  // 2- build the seed upstream tree
  for ( const LHCb::MCParticle* seed : seeds ) {
    std::vector<const LHCb::MCParticle*> tree;
    std::string                          sTree;
    mcTree( seed, tree, sTree );
    std::stringstream ss;
    ss << format( " %6.1f %% from : ", weight( seed ) * 100. ) << sTree << " ( "
       << format( " %6.1f %% of the MC particle energy contributing",
                  LHCb::essentiallyZero( weight( seed ) ) ? 0 : quality( seed ) / weight( seed ) * 100. )
       << " )";
    m_treeMap[ss.str()] = std::move( tree );
  }
}

void CaloFuture2MCTool::mcTree( const LHCb::MCParticle* part, std::vector<const LHCb::MCParticle*>& tree,
                                std::string& sTree ) {
  if ( !part ) return;
  tree.push_back( part );
  const LHCb::ParticleProperty* prop = m_ppsvc->find( part->particleID() );
  sTree                              = ( prop ? prop->name() : "??" ) + sTree;
  if ( part->mother() ) {
    sTree = " -> " + sTree;
    mcTree( part->mother(), tree, sTree );
  }
}

double CaloFuture2MCTool::weight( const LHCb::MCParticle* part ) const {
  return ( part && m_sum > 0 ) ? m_mcMap[part] / m_sum : 0.;
}

double CaloFuture2MCTool::quality( const LHCb::MCParticle* part ) const {
  return ( part && !LHCb::essentiallyZero( part->momentum().E() ) )
             ? weight( part ) * m_mcMap[part] / part->momentum().E()
             : 0.;
}

std::string CaloFuture2MCTool::descriptor() const {
  std::stringstream ss;
  ss << "\n     ---------- CaloFuture MC contribution ";
  if ( m_part ) ss << "to particle (pid = " << m_part->particleID().pid() << ")\n";
  if ( m_parts.size() > 1 ) ss << " -> to " << m_parts.size() << " particle(s) -------- \n";
  if ( !m_protos.empty() ) ss << "to " << m_protos.size() << " protoParticle(s) -------- \n";
  if ( !m_hypos.empty() ) ss << "to " << m_hypos.size() << " hypo(s) -------- \n";
  if ( !m_digits.empty() ) ss << "to " << m_digits.size() << " digit(s) -------- \n";
  if ( !m_clusters.empty() ) ss << "to " << m_clusters.size() << " cluster(s) ------- \n";
  ss << "     ---- Total calo energy deposit : " << m_sum << " MeV \n";
  for ( const auto& im : m_treeMap ) ss << "        -- " << im.first << '\n';

  if ( bestMC() ) {
    const LHCb::ParticleProperty* prop = m_ppsvc->find( bestMC()->particleID() );
    std::string                   p    = ( prop ? prop->name() : "??" );
    ss << "      --> Best matching MCParticle : [" << p << "] ==  (Quality/Weight : " << quality( bestMC() ) << " / "
       << weight( bestMC() ) << ")\n";
  }

  if ( maxMC() ) {
    const LHCb::ParticleProperty* prop = m_ppsvc->find( maxMC()->particleID() );
    std::string                   p    = ( prop ? prop->name() : "??" );
    ss << "      --> Maximum weight MCParticle : [" << p << "] == (Quality/Weight : " << quality( maxMC() ) << " / "
       << weight( maxMC() ) << ")\n";
  }

  ss << "      -------------------------------- ";
  return ss.str();
}

const LHCb::MCParticle* CaloFuture2MCTool::findMCOrBest( LHCb::ParticleID id, double threshold ) const {
  const LHCb::MCParticle* found = findMC( id, threshold );
  return found ? found : bestMC();
}
const LHCb::MCParticle* CaloFuture2MCTool::findMCOrBest( std::string name, double threshold ) const {
  const LHCb::MCParticle* found = findMC( name, threshold );
  return found ? found : bestMC();
}
const LHCb::MCParticle* CaloFuture2MCTool::findMC( std::string name, double threshold ) const {
  const LHCb::ParticleProperty* prop = m_ppsvc->find( name );
  return prop ? findMC( prop->particleID(), threshold ) : nullptr;
}
const LHCb::MCParticle* CaloFuture2MCTool::findMC( LHCb::ParticleID id, double threshold ) const {
  double                  t    = threshold;
  const LHCb::MCParticle* best = nullptr;
  for ( const auto& imap : m_mcMap ) {
    const LHCb::MCParticle* mcPart = imap.first;
    if ( mcPart->particleID().abspid() != id.abspid() ) continue;
    double q = quality( mcPart );
    if ( q < t ) continue;
    t    = q;
    best = mcPart;
  }
  return best;
}
const LHCb::MCParticle* CaloFuture2MCTool::bestMC() const { return m_bestMC; }
const LHCb::MCParticle* CaloFuture2MCTool::maxMC() const { return m_maxMC; }

ICaloFuture2MCTool* CaloFuture2MCTool::fragment( unsigned int i ) {
  m_nFrag = 0;
  /* CaloFutureHypo level */
  if ( m_depth == 2 ) {
    m_nFrag = m_hypos.size();
    if ( i >= m_nFrag ) return nullptr;
    if ( m_nFrag == 1 ) return this;
    return m_tool->from( m_hypos[i] );
  }
  /* CaloFutureCluster level */
  if ( m_depth == 1 ) {
    m_nFrag = m_clusters.size();
    if ( i >= m_nFrag ) return nullptr;
    if ( m_nFrag == 1 ) return this;
    return m_tool->from( m_clusters[i] );
  }
  /* CaloDigit level */
  if ( m_depth == 0 ) {
    m_nFrag = m_digits.size();
    if ( i >= m_nFrag ) return nullptr;
    if ( m_nFrag == 1 ) return this;
    return m_tool->from( m_digits[i] );
  }
  return nullptr;
}
