/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureInterfaces/ICaloFutureElectron.h"
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h" // Interface
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "Event/CaloDataFunctor.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "IGammaPi0SeparationTool.h"
#include "INeutralIDTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureHypoEstimator
//
// 2010-08-18 : Olivier Deschamps
//-----------------------------------------------------------------------------

/** @class CaloFutureHypoEstimator CaloFutureHypoEstimator.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2010-08-18
 */

namespace LHCb::Calo {

  class HypoEstimator : public extends<GaudiTool, Interfaces::IHypoEstimator> {
  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;

    std::optional<double> data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo, const LHCb::CaloHypo& hypo,
                                Enum::DataType type ) const override;
    std::map<Enum::DataType, double> get_data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo,
                                               const LHCb::CaloHypo& hypo ) const override;

    Interfaces::IHypo2Calo* hypo2Calo() override { return m_toCaloFuture.get(); }

    StatusCode _setProperty( const std::string& p, const std::string& v ) override { return setProperty( p, v ); };

  private:
    Gaudi::Property<bool> m_extrapol{ this, "Extrapolation", true };
    Gaudi::Property<bool> m_seed{ this, "AddSeed", false };
    Gaudi::Property<bool> m_neig{ this, "AddNeighbors", false };

    DataObjectReadHandle<LHCb::Event::Calo::Digits> m_digits{ this, "DigitLocation", CaloDigitLocation::Ecal };

    mutable Gaudi::Accumulators::Counter<> m_nEmptyCluster{ this, "Empty cluster" };
    mutable Gaudi::Accumulators::Counter<> m_nSeedPointsToNull{ this, "Seed points to NULL" };

    ToolHandle<Interfaces::IHypo2Calo>          m_toCaloFuture{ this, "Hypo2Calo", "CaloFutureHypo2CaloFuture" };
    ToolHandle<Interfaces::IElectron>           m_electron{ this, "Electron", "CaloFutureElectron" };
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0{ this, "Pi0Separation", "FutureGammaPi0SeparationTool" };
    ToolHandle<Interfaces::IGammaPi0Separation> m_GammaPi0XGB{ this, "Pi0SeparationXGB", "FutureGammaPi0XGBoostTool" };
    ToolHandle<Interfaces::INeutralID>          m_neutralID{ this, "NeutralID", "FutureNeutralIDTool" };
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( HypoEstimator, "CaloFutureHypoEstimator" )

  StatusCode HypoEstimator::initialize() {
    StatusCode sc = GaudiTool::initialize(); // must be executed first

    m_toCaloFuture.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    auto& h2c = dynamic_cast<IProperty&>( *m_toCaloFuture );
    h2c.setProperty( "Seed", m_seed ? "true" : "false" ).ignore();
    h2c.setProperty( "PhotonLine", m_extrapol ? "true" : "false" ).ignore();
    h2c.setProperty( "AddNeighbors", m_neig ? "true" : "false" ).ignore();

    return sc;
  }

  //=============================================================================

  // ------------
  std::optional<double> HypoEstimator::data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo,
                                             const LHCb::CaloHypo& hypo, LHCb::Calo::Enum::DataType type ) const {
    auto data = get_data( fromCalo, toCalo, hypo );
    if ( auto it = data.find( type ); it != data.end() ) return it->second;
    return {};
  }

  // ------------ FROM HYPO
  std::map<Enum::DataType, double> HypoEstimator::get_data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo,
                                                            const LHCb::CaloHypo& hypo ) const {
    std::map<Enum::DataType, double> data{};
    using namespace LHCb::Calo::Enum;

    LHCb::Calo::Momentum mom( &hypo );
    data[DataType::HypoE]  = mom.e();
    data[DataType::HypoEt] = mom.pt();
    data[DataType::HypoM] =
        mom.mass(); // for mergedPi0 hypothesis only (->for all clusters - toDo in CaloFutureMergedPi0Alg)

    auto insert_if = [&]( DataType key, std::optional<double> val ) {
      if ( val ) data[key] = *val;
    };

    // get digits to build neutral ID input variables
    const LHCb::Event::Calo::Digits& digits = *m_digits.get();

    if ( auto obs = m_GammaPi0->observables( hypo, digits ); obs ) {
      // gamma/pi0 separation
      insert_if( DataType::isPhoton, m_GammaPi0->isPhoton( obs.value() ) );
      // 1- Ecal variables :
      data[DataType::isPhotonEcl]       = obs->Ecl;
      data[DataType::isPhotonfracE1]    = obs->fracE1;
      data[DataType::isPhotonfracE2]    = obs->fracE2;
      data[DataType::isPhotonfracE3]    = obs->fracE3;
      data[DataType::isPhotonfracE4]    = obs->fracE4;
      data[DataType::isPhotonfracEseed] = obs->fracEseed;
      data[DataType::isPhotonfracE6]    = obs->fracE6;
      data[DataType::isPhotonfracE7]    = obs->fracE7;
      data[DataType::isPhotonfracE8]    = obs->fracE8;
      data[DataType::isPhotonfracE9]    = obs->fracE9;
    }

    insert_if( DataType::isPhotonXGB, m_GammaPi0XGB->isPhoton( hypo, digits ) );
    // 1- Ecal variables :
    int CaloRegion = -1;

    // link 2 cluster :
    if ( const LHCb::CaloCluster* cluster = LHCb::CaloFutureAlgUtils::ClusterFromHypo( hypo ); cluster ) {
      using namespace LHCb::Calo::Enum;

      if ( cluster->entries().empty() ) {
        ++m_nEmptyCluster;
        return {};
      }
      if ( auto iseed = LHCb::ClusterFunctors::locateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                            LHCb::CaloDigitStatus::Mask::SeedCell );
           iseed != cluster->entries().end() ) {

        const LHCb::CaloDigit* seed = iseed->digit();
        if ( !seed ) {
          ++m_nSeedPointsToNull;
          return {};
        }

        double eEcal = cluster->e();

        data[DataType::ClusterE] = eEcal;
        //
        double          cSize      = fromCalo.cellSize( cluster->seed() );
        Gaudi::XYZPoint cCenter    = fromCalo.cellCenter( cluster->seed() );
        double          asX        = ( cluster->position().x() - cCenter.x() ) / cSize;
        double          asY        = ( cluster->position().y() - cCenter.y() ) / cSize;
        data[DataType::ClusterAsX] = asX;
        data[DataType::ClusterAsY] = asY;

        data[DataType::E1]               = seed->e();
        auto   it                        = data.find( DataType::HypoE );
        double eHypo                     = ( it != data.end() ) ? it->second : 0;
        data[DataType::E1Hypo]           = eHypo > 0. ? ( seed->e() ) / eHypo : -1.;
        LHCb::Detector::Calo::CellID sid = seed->cellID();
        CaloRegion                       = sid.area();
        data[DataType::CellID]           = sid.all();
        data[DataType::Spread]           = cluster->position().spread()( 1, 1 ) + cluster->position().spread()( 0, 0 );
        // E4
        std::array<double, 4> e4s       = { 0, 0, 0, 0 };
        double                e9        = 0.;
        double                ee9       = 0.; // full cluster energy without fraction applied
        double                e2        = 0.;
        bool                  hasShared = false;
        int                   code      = 0.;
        int                   mult      = 0.;
        int                   nsat      = 0; // number of saturated cells
        for ( const auto& ie : cluster->entries() ) {
          const LHCb::CaloDigit* dig = ie.digit();
          if ( !dig ) continue;
          double ecel = dig->e() * ie.fraction();
          if ( ie.status().anyOf(
                   { LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForCovariance } ) &&
               fromCalo.isSaturated( dig->e(), dig->cellID() ) )
            nsat++;
          LHCb::Detector::Calo::CellID id = dig->cellID();
          if ( id.area() != sid.area() || abs( (int)id.col() - (int)sid.col() ) > 1 ||
               abs( (int)id.row() - (int)sid.row() ) > 1 )
            continue;
          if ( id.col() <= sid.col() && id.row() >= sid.row() ) e4s[0] += ecel;
          if ( id.col() >= sid.col() && id.row() >= sid.row() ) e4s[1] += ecel;
          if ( id.col() >= sid.col() && id.row() <= sid.row() ) e4s[2] += ecel;
          if ( id.col() <= sid.col() && id.row() <= sid.row() ) e4s[3] += ecel;
          e9 += ecel;
          // new info
          ee9 += dig->e();
          mult++;
          if ( ie.status().test( LHCb::CaloDigitStatus::Mask::SharedCell ) ) hasShared = true;
          if ( !( id == sid ) && ecel > e2 ) {
            e2     = ecel;
            int dc = (int)id.col() - (int)sid.col() + 1;
            int dr = (int)id.row() - (int)sid.row() + 1;
            code   = 3 * dr + dc;
          }
        }
        data[DataType::Saturation] = nsat;
        double e4max =
            std::accumulate( e4s.begin(), e4s.end(), 0., []( double mx, const auto& i ) { return std::max( mx, i ); } );

        code = mult * 10 + code;
        if ( hasShared ) code *= -1;
        data[DataType::ClusterCode] = code;
        data[DataType::ClusterFrac] = ( e9 > 0. ? e9 / ee9 : -1 );
        data[DataType::E4]          = e4max;
        data[DataType::E9]          = e9;
        data[DataType::E49]         = ( e9 > 0. ? e4max / e9 : 0. );
        data[DataType::E19]         = ( e9 > 0. ? seed->e() / e9 : -1. );

        double eHcal              = m_toCaloFuture->energy( fromCalo, *cluster, toCalo );
        data[DataType::ToHcalE]   = eHcal;
        data[DataType::Hcal2Ecal] = ( eEcal > 0 ) ? eHcal / eEcal : 0.;
      }

    } else {
      counter( "no cluster" ) += 1;
    }

    // Estimator MUST be at the end (after all inputs are loaded)

    auto get_ = [&]( LHCb::Calo::Enum::DataType type ) {
      auto d = data.find( type );
      return d != data.end() ? d->second : 0.;
    };

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif

    auto obs = LHCb::Calo::Interfaces::INeutralID::Observables{ .e1frac    = get_( DataType::isPhotonfracE1 ),
                                                                .e2frac    = get_( DataType::isPhotonfracE2 ),
                                                                .e3frac    = get_( DataType::isPhotonfracE3 ),
                                                                .e4frac    = get_( DataType::isPhotonfracE4 ),
                                                                .eseedfrac = get_( DataType::isPhotonfracEseed ),
                                                                .e6frac    = get_( DataType::isPhotonfracE6 ),
                                                                .e7frac    = get_( DataType::isPhotonfracE7 ),
                                                                .e8frac    = get_( DataType::isPhotonfracE8 ),
                                                                .e9frac    = get_( DataType::isPhotonfracE9 ),
                                                                .area      = CaloRegion };
#pragma GCC diagnostic pop

    insert_if( DataType::isNotH, m_neutralID->isNotH( hypo, obs ) );

    return data;
  }

} // namespace LHCb::Calo
