/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "FastVertex.h"
#include "TrackForPV.h"

#include "DetDesc/Condition.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbDet/InteractionRegion.h"

namespace LHCb {

  /**
   *  @author Olivier Callot
   *  @date   2011-11-15
   */
  class FastPVFinder
      : public Algorithm::MultiTransformerFilter<std::tuple<RecVertices>( Tracks const&,
                                                                          Conditions::InteractionRegion const& ),
                                                 Algorithm::Traits::usesConditions<Conditions::InteractionRegion>> {
  public:
    FastPVFinder( const std::string& name, ISvcLocator* pSvcLocator );
    /// Initialization
    StatusCode                    initialize() override;
    std::tuple<bool, RecVertices> operator()( Tracks const&, Conditions::InteractionRegion const& ) const override;

  private:
    Gaudi::Property<double>       m_maxIPToBeam{ this, "MaxIPToBeam", 0.200 * Gaudi::Units::mm };
    Gaudi::Property<double>       m_maxDeltaZ{ this, "MaxDeltaZ", 3.000 * Gaudi::Units::mm };
    Gaudi::Property<unsigned int> m_minTracksInPV{ this, "MinTracksInPV", 5 };
    Gaudi::Property<double>       m_maxChi2ToAdd{ this, "MaxChi2ToAdd", 50 };
    Gaudi::Property<double>       m_maxChi2Fit{ this, "MaxChi2Fit", 12 };
    Gaudi::Property<double>       m_maxChi2PerDoF{ this, "MaxChi2PerDoF", 3 };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( FastPVFinder, "FastPVFinder" )

} // namespace LHCb

LHCb::FastPVFinder::FastPVFinder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformerFilter(
          name, pSvcLocator,
          { KeyValue{ "InputLocation", TrackLocation::Velo },
            KeyValue{ "InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion" } },
          { KeyValue{ "OutputLocation", RecVertexLocation::Primary } } ) {}

StatusCode LHCb::FastPVFinder::FastPVFinder::initialize() {
  return MultiTransformerFilter::initialize().andThen( [&]() {
    // This is only needed to have a fallback in case the IR condition does not exist. In that case, the information
    // is taken from DeVP and thus the Velo motion system which is not exactly the same.
    LHCb::Conditions::InteractionRegion::addConditionDerivation( this,
                                                                 inputLocation<LHCb::Conditions::InteractionRegion>() );
  } );
}

std::tuple<bool, LHCb::RecVertices>
LHCb::FastPVFinder::operator()( LHCb::Tracks const& tracks, LHCb::Conditions::InteractionRegion const& region ) const {
  std::tuple<bool, LHCb::RecVertices> fullOutput;
  auto& [filter, out] = fullOutput;
  filter              = false;

  //== Select tracks with a cut on their R at beam
  std::vector<TrackForPV> pvTracks;
  pvTracks.reserve( tracks.size() );
  for ( auto& track : tracks ) {
    Gaudi::XYZPoint beamSpot = region.avgPosition;
    TrackForPV      temp( track, beamSpot.x(), beamSpot.y() );
    if ( temp.rAtBeam() < m_maxIPToBeam ) { pvTracks.push_back( temp ); }
  }

  //== At least enough tracks for a vertex
  if ( pvTracks.size() < m_minTracksInPV ) return fullOutput;

  //== Create a vector of pointer to these tracks, and sort them by zBeam
  std::vector<TrackForPV*> myTracks;
  for ( auto& pvTrack : pvTracks ) { myTracks.push_back( &pvTrack ); }
  std::sort( myTracks.begin(), myTracks.end(), TrackForPV::LowerByZ() );

  unsigned int minTracks = 0.3 * myTracks.size(); // Search first for large accumulation
  int          nLoop     = 2;
  if ( minTracks < m_minTracksInPV ) {
    minTracks = m_minTracksInPV;
    nLoop     = 1;
  }

  //== Collect all tracks with compatible zAtBeam, and then by chi2.
  std::vector<FastVertex>            myVertices;
  std::vector<TrackForPV*>::iterator itT1;
  while ( 0 < nLoop ) {
    std::vector<TrackForPV*> unusedTracks;
    for ( auto* myTrack : myTracks ) {
      if ( !myTrack->used() ) unusedTracks.push_back( myTrack );
    }
    --nLoop;
    if ( unusedTracks.size() < minTracks ) continue;
    for ( itT1 = unusedTracks.begin(); unusedTracks.end() - minTracks > itT1; ++itT1 ) {
      std::vector<TrackForPV*>::iterator itT2 = itT1 + minTracks - 1;
      if ( ( *itT2 )->zAtBeam() > ( *itT1 )->zAtBeam() + m_maxDeltaZ ) continue; // consecutive zBeam close enough
      //== extend the range until maxDeltaZ
      while ( itT2 + 1 < unusedTracks.end() ) {
        if ( ( *itT2 + 1 )->zAtBeam() > ( *itT1 )->zAtBeam() + m_maxDeltaZ ) break;
        ++itT2;
      }
      FastVertex temp( itT1, itT2 );
      temp.removeWorsts( m_maxChi2Fit );

      //== add tracks before if they have a good chi2.
      while ( itT1 > unusedTracks.begin() ) {
        --itT1;
        double chi2 = ( *itT1 )->chi2( temp.vertex() );
        if ( chi2 < m_maxChi2ToAdd ) temp.addTrack( *itT1 );
      }
      itT1 = itT2;

      //== Add tracks after, and define the next starting point...
      bool isLast = true;
      while ( ++itT2 < unusedTracks.end() ) {
        double chi2 = ( *itT2 )->chi2( temp.vertex() );
        if ( chi2 < m_maxChi2ToAdd ) {
          temp.addTrack( *itT2 );
          if ( ( *itT2 )->zAtBeam() - temp.vertex().z() < m_maxDeltaZ ) isLast = true;
        } else {
          if ( isLast ) itT1 = itT2 - 1;
          isLast = false;
        }
      }

      //== Final fit, and checks
      temp.removeWorsts( m_maxChi2Fit );

      if ( temp.nTracks() < minTracks ) continue;
      if ( temp.nbUsed() > 0.9 * temp.tracks().size() ) continue; // avoid duplicates
      if ( temp.chi2PerDoF() > m_maxChi2PerDoF ) continue;
      myVertices.push_back( temp );
      temp.setTracksUsed( true );
    }
    minTracks = m_minTracksInPV;
  }

  //== Try with all (unused) tracks, without zBeam grouping, iteratively.
  bool found = true;
  while ( found ) {
    std::vector<TrackForPV*> unusedTracks;
    for ( itT1 = myTracks.begin(); myTracks.end() != itT1; ++itT1 ) {
      if ( !( *itT1 )->used() ) unusedTracks.push_back( *itT1 );
    }
    found = false;
    if ( m_minTracksInPV > unusedTracks.size() ) continue;
    FastVertex temp( unusedTracks.begin(), unusedTracks.end() - 1 );
    temp.removeWorsts( m_maxChi2Fit );
    if ( temp.nTracks() >= m_minTracksInPV && temp.chi2PerDoF() < m_maxChi2PerDoF ) {
      myVertices.push_back( temp );
      temp.setTracksUsed( true );
      found = true;
    }
  }

  //== Store the vertices in the appropriate container
  for ( auto& myVertex : myVertices ) {
    RecVertex* tmp = new RecVertex;
    tmp->setTechnique( LHCb::RecVertex::RecVertexType::Primary );
    tmp->setPosition( myVertex.vertex() );
    for ( std::vector<TrackForPV*>::iterator itT = myVertex.tracks().begin(); myVertex.tracks().end() != itT; ++itT ) {
      tmp->addToTracks( ( *itT )->track() );
    }
    tmp->setChi2AndDoF( myVertex.chi2(), 2 * myVertex.nTracks() - 3 );
    tmp->setCovMatrix( myVertex.cov() );
    out.insert( tmp );
  }
  filter = !out.empty();
  return fullOutput;
}
