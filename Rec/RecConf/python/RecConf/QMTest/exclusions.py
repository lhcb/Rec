###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiConf.QMTest.LHCbExclusions import LineSkipper
from GaudiConf.QMTest.LHCbExclusions import preprocessor as LHCbPreprocessor

skip_lines = LineSkipper(
    regexps=[
        r"FunctorFactory\s*INFO New functor library will be created.",
        r"FunctorFactory\s*INFO Compilation of functor library took .*",
        r"FunctorFactory\s*INFO Reusing functor library.*",
    ]
)

preprocessor = skip_lines + LHCbPreprocessor
