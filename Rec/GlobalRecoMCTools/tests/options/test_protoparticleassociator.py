###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the charged and neutral variants of the ProtoParticleAssociator algorithm.

For charged protos, this looks like:

1. Process input data that contains Track -> MCParticle LinksByKey objects.
2. Run the ChargedProtoParticleAssociator algorithm which takes as input:
  - The tracks used to used the LinksByKey object.
  - The MC particles used to used the LinksByKey object.
  - The LinksByKey objects.
3. Assert that the LinksByKey and Relation1DWeighted objects produced by
ChargedProtoParticleAssociator contain the correct ProtoParticle ->
MCParticle links.

There's similar procedure for neutral protos, using CaloHypo -> MCParticle
link objects and the NeutralProtoParticleAssociator algorithm instead.
"""

import GaudiPython as GP
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.Algorithms import (
    ChargedProtoParticleAssociator,
    MergeLinksByKeysToVector,
    NeutralProtoParticleAssociator,
    UnpackCaloHypo,
    UnpackMCParticle,
    UnpackProtoParticle,
    UnpackTrack,
)
from PyConf.Algorithms import (
    LHCb__Tests__FakeRunNumberProducer as FET,
)
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
    force_location,
    input_from_root_file,
)
from PyConf.control_flow import CompositeNode


def compare(protos, pp2mcp, link_paths, charged):
    nmatches = 0
    ref = GP.gbl.LHCb.LinkReference()
    for proto in protos.containedObjects():
        rels = pp2mcp.relations(proto)
        # It would be nicer to use sets here, but the objects we retrieve from
        # the tables are pointers, so their hashes may be different and set
        # membership checks would fail. Instead, we use a list and fall back to
        # O(n) lookup.
        pairs = []
        for idx in range(len(rels)):
            rel = rels.at(idx)
            pairs.append((getattr(rel, "from")(), rel.to()))

        basics = proto.calo() if not charged else [proto.track()]
        for jdx in range(len(basics)):
            basic = basics[jdx]
            for link_path in link_paths:
                links = TES[link_path]
                ok = links.firstReference(basic.index(), basic.parent(), ref)
                while ok:
                    to_path = links.linkMgr().link(ref.linkID()).path()
                    to_index = ref.objectKey()
                    mcp = TES[to_path].object(to_index)
                    assert (proto, mcp) in pairs, (
                        "Basic -> MCP link not found in ProtoParticle -> MCP table"
                    )
                    nmatches += 1
                    ok = links.nextReference(ref)

    return nmatches


options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("upgrade_minbias_hlt1_filtered")
options.evt_max = 100
options.conditions_version = "sim-20171127-vc-md100"
options.root_ioalg_name = "RootIOAlgExt"
options.gaudipython_mode = True
config = configure_input(options)

fet = FET(name="FakeRunNumber", Start=0, Step=0)
if UseDD4Hep:
    dd4hepSvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
    dd4hepSvc.DetectorList = []
    from PyConf.Algorithms import (
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
    )
    from PyConf.Algorithms import (
        LHCb__Tests__FakeRunNumberProducer as FET,
    )

    iov = IOVProducer(
        name="IOVProducer",
        ODIN=fet.ODIN,
        outputs={"SliceLocation": force_location("/Event/IOVLock")},
    )
else:
    from PyConf.Algorithms import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV

    iov = reserveIOV(
        name="IOVProducer",
        ODIN=fet.ODIN,
        outputs={"IOVLock": force_location("/Event/IOVLock")},
    )
cond_algs = [fet, iov]

# note the usage of force_location for all outputs of packers
# this is needed as we use association tables which have the path of their
# ends hardadoded in the input file. So we need to stick to these paths
# This is obviously very fragile
unpack_mcp = UnpackMCParticle(
    name="UnpackMCParticle",
    InputName=input_from_root_file(
        "pSim/MCParticles", forced_type="LHCb::PackedMCParticles"
    ),
    outputs={"OutputName": force_location("/Event/MC/Particles")},
)
unpack_track = UnpackTrack(
    name="UnpackTrack",
    InputName=input_from_root_file("pRec/Track/Best", forced_type="LHCb::PackedTracks"),
    outputs={"OutputName": force_location("/Event/Rec/Track/Best")},
)
unpack_charged_pp = UnpackProtoParticle(
    name="UnpackProtoParticle",
    InputName=input_from_root_file(
        "pRec/ProtoP/Charged", forced_type="LHCb::PackedProtoParticles"
    ),
    outputs={"OutputName": force_location("/Event/Rec/ProtoP/Charged")},
)
unpack_calohypos = [
    UnpackCaloHypo(
        name="Unpack{}".format(loc.replace("/", "")),
        InputName=input_from_root_file(
            "pRec/Calo/" + loc, forced_type="LHCb::PackedCaloHypos"
        ),
        outputs={"OutputName": force_location("/Event/Rec/Calo/" + loc)},
    )
    for loc in ["Photons", "SplitPhotons", "MergedPi0s"]
]
unpack_neutral_pp = UnpackProtoParticle(
    name="UnpackProtoParticleNeutrals",
    InputName=input_from_root_file(
        "pRec/ProtoP/Neutrals", forced_type="LHCb::PackedProtoParticles"
    ),
    outputs={"OutputName": force_location("/Event/Rec/ProtoP/Neutrals")},
)
charged_merger = MergeLinksByKeysToVector(
    name="ChargedMergeLinksByKeysToVector",
    InputLinksByKeys=[
        input_from_root_file(
            "/Event/Link/Rec/Track/Best", forced_type="LHCb::LinksByKey"
        )
    ],
)
charged_assoc = ChargedProtoParticleAssociator(
    name="ChargedProtoParticleAssociator",
    InputProtoParticles=unpack_charged_pp.OutputName,
    InputMCParticles=unpack_mcp.OutputName,
    InputAssociations=charged_merger.Output,
)

neutral_merger = MergeLinksByKeysToVector(
    name="NeutralMergeLinksByKeysToVector",
    InputLinksByKeys=[
        input_from_root_file("Link/Rec/Calo/Photons", forced_type="LHCb::LinksByKey"),
        input_from_root_file(
            "Link/Rec/Calo/SplitPhotons", forced_type="LHCb::LinksByKey"
        ),
        input_from_root_file(
            "Link/Rec/Calo/MergedPi0s", forced_type="LHCb::LinksByKey"
        ),
    ],
)
neutral_assoc = NeutralProtoParticleAssociator(
    name="NeutralProtoParticleAssociator",
    InputProtoParticles=unpack_neutral_pp.OutputName,
    InputMCParticles=unpack_mcp.OutputName,
    InputAssociations=neutral_merger.Output,
)

algs = (
    cond_algs
    + [create_or_reuse_rootIOAlg(options)]
    + unpack_calohypos
    + [
        unpack_neutral_pp,
        unpack_track,
        unpack_charged_pp,
        unpack_mcp,
        charged_merger,
        charged_assoc,
        neutral_merger,
        neutral_assoc,
    ]
)

config.update(configure(options, CompositeNode("TopSeq", algs)))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

i = 0
nmatches_charged = 0
nmatches_neutral = 0
while TES["/Event/Rec"] and i < options.evt_max:
    charged_protos = TES[unpack_charged_pp.OutputName.location]
    charged_pp2mcp = TES[charged_assoc.OutputTable.location]
    charged_link_paths = [h.location for h in charged_merger.inputs["InputLinksByKeys"]]
    nmatches_charged += compare(
        charged_protos, charged_pp2mcp, charged_link_paths, charged=True
    )

    neutral_protos = TES[unpack_neutral_pp.OutputName.location]
    neutral_pp2mcp = TES[neutral_assoc.OutputTable.location]
    neutral_link_paths = [h.location for h in neutral_merger.inputs["InputLinksByKeys"]]
    nmatches_neutral += compare(
        neutral_protos, neutral_pp2mcp, neutral_link_paths, charged=False
    )

    appMgr.run(1)
    i += 1

assert nmatches_charged > 0, "ERROR: Found zero charged matches"
assert nmatches_neutral > 0, "ERROR: Found zero neutral matches"
print(
    "SUCCESS: Found {}/{} charged/neutral matches".format(
        nmatches_charged, nmatches_neutral
    )
)
