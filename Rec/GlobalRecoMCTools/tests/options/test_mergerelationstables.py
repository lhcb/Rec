###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the MergeRelationsTablesPP2MCP algorithm.

Asserts that the list of input tables is merged into a single table.

Runs over Brunel output and creates a number of dummy
ProtoParticle-to-MCParticle relations, merges these using the algorithm, and
then checks that the merged table contains all the information present in the
individual tables.
"""

import random

import GaudiPython as GP
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.Algorithms import (
    LHCb__Tests__FakeRunNumberProducer as FET,
)
from PyConf.Algorithms import (
    MakeRelationsTables,
    MergeRelationsTablesPP2MCP,
    UnpackMCParticle,
    UnpackProtoParticle,
)
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
    force_location,
    input_from_root_file,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("upgrade_minbias_hlt1_filtered")
options.evt_max = 50
options.root_ioalg_name = "RootIOAlgExt"
options.gaudipython_mode = True
config = configure_input(options)

fet = FET(name="FakeRunNumber", Start=0, Step=0)
if UseDD4Hep:
    dd4hepSvc = config["LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"]
    dd4hepSvc.DetectorList = []
    from PyConf.Algorithms import (
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
    )

    iov = IOVProducer(
        name="IOVProducer",
        ODIN=fet.ODIN,
        outputs={"SliceLocation": force_location("/Event/IOVLock")},
    )
else:
    from PyConf.Algorithms import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV

    iov = reserveIOV(
        name="IOVProducer",
        ODIN=fet.ODIN,
        outputs={"IOVLock": force_location("/Event/IOVLock")},
    )
cond_algs = [fet, iov]

# note the usage of force_location for all outputs of packers
# this is needed as we use association tables which have the path of their
# ends hardadoded in the input file. So we need to stick to these paths
# This is obviously very fragile
unpack_mcp = UnpackMCParticle(
    name="UnpackMCParticle",
    InputName=input_from_root_file(
        "pSim/MCParticles", forced_type="LHCb::PackedMCParticles"
    ),
    outputs={"OutputName": force_location("/Event/MC/Particles")},
)
unpack_charged_pp = UnpackProtoParticle(
    name="UnpackProtoParticle",
    InputName=input_from_root_file(
        "pRec/ProtoP/Charged", forced_type="LHCb::PackedProtoParticles"
    ),
    outputs={"OutputName": force_location("/Event/Rec/ProtoP/Charged")},
)

relations_locations = ["TableA", "TableB", "TableC"]
maker = MakeRelationsTables(
    name="MakeRelationsTables",
    ProtoParticles=unpack_charged_pp.OutputName,
    MCParticles=unpack_mcp.OutputName,
    OutputTablesLocations=relations_locations,
)
merger = MergeRelationsTablesPP2MCP(
    name="MergeRelationsTablesPP2MCP", InputRelationsTables=relations_locations
)

algs = cond_algs + [
    create_or_reuse_rootIOAlg(options),
    unpack_mcp,
    unpack_charged_pp,
    maker,
    merger,
]
config.update(configure(options, CompositeNode("TopSeq", algs)))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

# Check that the merged table contains all relations from the input tables
i = 0
while TES["/Event/Rec"] and i < options.evt_max:
    merged = TES[merger.Output.location]
    merged_rels = set()
    for rel in merged.relations():
        merged_rels.add(
            (
                getattr(rel, "from")().index(),
                rel.to().index(),
                rel.weight(),
            )
        )

    table_rels = set()
    for loc in relations_locations:
        table = TES[loc]
        for rel in table.relations():
            table_rels.add(
                (
                    getattr(rel, "from")().index(),
                    rel.to().index(),
                    rel.weight(),
                )
            )

    assert merged_rels == table_rels
    i += 1
    appMgr.run(1)

print("SUCCESS")
