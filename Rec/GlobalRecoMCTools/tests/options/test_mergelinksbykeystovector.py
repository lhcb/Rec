###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the MergeLinksByKeysToVector algorithm.

Asserts that the list of input objects is converted to an output vector of
pointers to those objects.
"""

import GaudiPython as GP
from PyConf.Algorithms import MergeLinksByKeysToVector
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    create_or_reuse_rootIOAlg,
)
from PyConf.control_flow import CompositeNode


def compare(links_vec, original_link_paths):
    assert len(links_vec) == len(original_link_paths), "Size mismatch"
    for idx, link_path in enumerate(original_link_paths):
        element = links_vec.at(idx)
        expected = TES[link_path]
        assert element == expected, (
            "Index {} in output vector is not equal to object at {}".format(
                idx, link_path
            )
        )
        # Couple more checks out of paranoia (in case for example the equality method does very little)
        assert element.linkMgr().size() == expected.linkMgr().size(), (
            "Index {} in output vector has different LinkMgr".format(idx)
        )
        assert element.linkReference().size() == expected.linkReference().size(), (
            "Index {} in output vector has different linkReference".format(idx)
        )


options = ApplicationOptions(_enabled=False)
options.set_input_and_conds_from_testfiledb("upgrade_minbias_hlt1_filtered")
options.evt_max = 100
options.gaudipython_mode = True
options.root_ioalg_name = "RootIOAlgExt"
options.print_freq = 10
config = configure_input(options)

charged_link_paths = ["Link/Rec/Track/Best"]
charged_merger = MergeLinksByKeysToVector(
    name="ChargedMergeLinksByKeysToVector", InputLinksByKeys=charged_link_paths
)

hypo_locs = ["Rec/Calo/Photons", "Rec/Calo/SplitPhotons", "Rec/Calo/MergedPi0s"]
neutral_link_paths = [str("Link/" + loc) for loc in hypo_locs]
neutral_merger = MergeLinksByKeysToVector(
    name="NeutralMergeLinksByKeysToVector", InputLinksByKeys=neutral_link_paths
)

algs = [
    create_or_reuse_rootIOAlg(options),
    charged_merger,
    neutral_merger,
]

config.update(configure(options, CompositeNode("test", algs)))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

i = 0
while TES["/Event/Rec"] and i < options.evt_max:
    charged_links_vec = TES[charged_merger.Output.location].getData()
    compare(charged_links_vec, charged_link_paths)

    neutral_links_vec = TES[neutral_merger.Output.location].getData()
    compare(neutral_links_vec, neutral_link_paths)

    appMgr.run(1)
    i += 1

print("SUCCESS")
