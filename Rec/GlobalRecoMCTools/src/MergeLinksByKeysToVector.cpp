/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "GaudiAlg/FunctionalDetails.h"
#include "LHCbAlgs/MergingTransformer.h"

#include "Event/LinksByKey.h"

namespace {
  using LinksByKeyVOC = Gaudi::Functional::vector_of_const_<LHCb::LinksByKey>;
}

/** @class MergeLinksByKeysToVector MergeLinksByKeysToVector.cpp
 *
 * @brief Merge a list of separate LinksByKey input tables to a single vector of tables.
 *
 * This returns a vector of <b>pointers</b> to the original tables. Creating a
 * vector of copied tables effectively breaks the tables, as the embedded
 * linkMgr is cleared during the copy, and this is needed to evaluate relations
 * downstream.
 *
 * @see ProtoParticleAssociator: Consumes the output of this algorithm.
 *
 */
struct MergeLinksByKeysToVector
    : public LHCb::Algorithm::MergingTransformer<std::vector<const LHCb::LinksByKey*>( const LinksByKeyVOC& )> {
  MergeLinksByKeysToVector( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingTransformer<std::vector<const LHCb::LinksByKey*>( const LinksByKeyVOC& )>(
            name, pSvcLocator, { "InputLinksByKeys", {} }, { "Output", "" } ){};

  std::vector<const LHCb::LinksByKey*> operator()( const LinksByKeyVOC& linkers ) const override {
    std::vector<const LHCb::LinksByKey*> result;
    for ( const auto& linker : linkers ) { result.push_back( &linker ); }
    return result;
  }
};

DECLARE_COMPONENT( MergeLinksByKeysToVector )
