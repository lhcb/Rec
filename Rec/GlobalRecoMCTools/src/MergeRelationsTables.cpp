/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <vector>

#include "GaudiAlg/FunctionalDetails.h"
#include "LHCbAlgs/MergingTransformer.h"

#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "Relations/RelationWeighted1D.h"

/** @class MergeRelationsTables MergeRelationsTables.cpp
 *
 * @brief Merge a list of separate RelationsTable1DWeighted input tables to a single table.
 *
 * The merging logic assumes that there are no duplicate relations across all
 * of the input tables.
 *
 * @tparam From First template parameter of RelationWeighted1D.
 * @tparam To Second template parameter of RelationWeighted1D.
 * @tparam W Third template parameter of RelationWeighted1D.
 *
 */

template <typename From, typename To, typename W>
struct MergeRelationsTables : public LHCb::Algorithm::MergingTransformer<LHCb::RelationWeighted1D<From, To, W>(
                                  const Gaudi::Functional::vector_of_const_<LHCb::RelationWeighted1D<From, To, W>>& )> {

  using table_t    = LHCb::RelationWeighted1D<From, To, W>;
  using input_t    = const Gaudi::Functional::vector_of_const_<table_t>&;
  using output_t   = table_t;
  using base_class = LHCb::Algorithm::MergingTransformer<output_t( input_t )>;

  MergeRelationsTables( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class( name, pSvcLocator, { "InputRelationsTables", {} }, { "Output", "" } ){};

  output_t operator()( input_t tables ) const override {
    output_t merged;
    // TODO: we should be able to take advantage of the fact that all tables are ordered...
    for ( const auto& table : tables ) {
      for ( const auto& relation : table.relations() ) {
        merged.relate( relation.from(), relation.to(), relation.weight() )
            .orElse( [&] { ++m_duplicate_relation; } )
            .ignore();
      }
    }
    // TODO: by taking advantage of the ordering of the inputtables during the
    //       merging, we should be able to avoid the need to sort the final table..
    merged.i_sort();
    return merged;
  }

private:
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_duplicate_relation{ this,
                                                                            "Unexpected duplicate relation found" };
};

namespace {
  using MergeRelationsTablesPP2MCP = MergeRelationsTables<LHCb::ProtoParticle, LHCb::MCParticle, double>;
}

DECLARE_COMPONENT_WITH_ID( MergeRelationsTablesPP2MCP, "MergeRelationsTablesPP2MCP" )
