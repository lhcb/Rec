/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "Relations/RelationWeighted1D.h"

#include "LHCbAlgs/Consumer.h"

#include <random>
#include <string>
#include <vector>

namespace LHCb::Test {

  using RelationTable = RelationWeighted1D<ProtoParticle, MCParticle, double>;

  /**
   * @brief Dummy relations table maker algorithm.
   *
   * Accepts ProtoParticle and MCParticle containers, and then creates a
   * number of RelationsWeighted1D output tables which associate the
   * former to the latter.
   *
   * Note : this is actually not a consumer, but the outputs are depending on
   * some Property, so we use DataHandles directly
   */
  struct MakeRelationsTables : public Algorithm::Consumer<void( ProtoParticles const&, MCParticles const& )> {

    Gaudi::Property<unsigned int>             m_nEntriesPerTable{ this, "NEntries", 5,
                                                      "Number of relations to create per output table" };
    Gaudi::Property<std::vector<std::string>> m_outputTables{
        this, "OutputTablesLocations", {}, "Locations of output relations tables" };
    // the data handles to write the different tables, created dynamically during initialize
    std::vector<DataObjectHandle<DataObject>> m_dataHandles;

    MakeRelationsTables( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, { KeyValue{ "ProtoParticles", {} }, KeyValue{ "MCParticles", "" } } ){};

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        for ( auto& output : m_outputTables ) { m_dataHandles.emplace_back( output, Gaudi::DataHandle::Writer, this ); }
      } );
    }

    void operator()( ProtoParticles const& protos, MCParticles const& mcparticles ) const override {
      // Check we have enough objects to create all relations
      assert( protos.size() >= m_nEntriesPerTable * m_outputTables.size() &&
              mcparticles.size() >= m_nEntriesPerTable * m_outputTables.size() );

      // random uniform distribution and random engine
      std::uniform_real_distribution<double> dist{ 0.0, 1.0 };
      std::default_random_engine             engine{};

      // Create one table per nentries_per_table-length chunk of ProtoParticles and MCParticles
      unsigned int                  idx = 0;
      std::vector<ContainedObject*> cprotos;
      protos.containedObjects( cprotos );
      std::vector<ContainedObject*> cmcpart;
      mcparticles.containedObjects( cmcpart );
      for ( auto& handle : m_dataHandles ) {
        auto table = std::make_unique<RelationTable>();
        for ( unsigned int n = 0; n < m_nEntriesPerTable; n++ ) {
          auto const& pp  = static_cast<ProtoParticle const*>( cprotos[idx] );
          auto const& mcp = static_cast<MCParticle const*>( cmcpart[idx] );
          if ( table->relate( pp, mcp, dist( engine ) ).isFailure() ) {
            throw GaudiException( "Failed making a relation", "MakeRelationsTables", StatusCode::FAILURE );
          }
          idx++;
        }
        handle.put( std::unique_ptr<DataObject>( table.release() ) );
      }
    }
  };
  DECLARE_COMPONENT_WITH_ID( MakeRelationsTables, "MakeRelationsTables" )

} // namespace LHCb::Test
