/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <string>

#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiKernel/GaudiException.h"
#include "LHCbAlgs/Transformer.h"
#include "Linker/LinkedTo.h"
#include "Relations/RelationWeighted1D.h"

namespace {
  using LinksByKeyVOC = std::vector<const LHCb::LinksByKey*>;
  using PP2MCPLinks   = LHCb::LinksByKey;
  // Float would no doubt be sufficient, but LinksByKey uses double for the
  // weight, and the Relations1D packers assume double, so be consistent.
  using PP2MCPTable = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;

  // Flag defining the matching strategy.
  // If Charged, we match a ProtoParticle based on the Track association,
  // otherwise we use CaloHypo association.
  enum MatchStrategy { Charged, Neutral };

  // Strategy-based dispatch helper.
  // Defines BasicType, as the object within the ProtoParticle that the original
  // LinksByKey associations are for, and allBasics, a method to return an
  // iterable of all basics held by a single ProtoParticle. The latter is used
  // because a ProtoParticle holds only a single Track but a vector of CaloHypo,
  // so we unify the interface by making everything a vector.
  template <MatchStrategy S>
  struct TypeHelper {};

  template <>
  struct TypeHelper<Charged> {
    using BasicType = LHCb::Track;

    static const std::vector<SmartRef<BasicType>> allBasics( const LHCb::ProtoParticle* proto ) {
      return { proto->track() };
    }
  };

  template <>
  struct TypeHelper<Neutral> {
    using BasicType = LHCb::CaloHypo;

    static const std::vector<SmartRef<BasicType>> allBasics( const LHCb::ProtoParticle* proto ) {
      return proto->calo();
    }
  };
} // namespace

/**
 * @class ProtoParticleAssociator ProtoParticleAssociator.cpp
 *
 * @brief Associate a container of ProtoParticle objects to MCParticle objects
 * based on the associations of tracks referenced by the protos to MCParticles.
 *
 * Collapses the indirect ProtoParticle -> Basic -> MCParticle relation to a
 * direct ProtoParticle -> MCParticle relation, via a vector of existing direct
 * Basic -> MCParticle association tables.
 *
 * This algorithm is templated on 'charged' or 'neutral' matching. In 'charged'
 * matching the ProtoParticle must hold a Track pointer, and the input
 * associations must be Track -> MCParticle. In 'neutral' matching the
 * ProtoParticle must hold a vector of CaloHypo pointers, and the input
 * associations must be CaloHypo -> MCParticle. An exception is thrown if the
 * appropriate conditions are not meet during execution.
 *
 * Outputs both a LinksByKey object and a Relation1DWeighted object. The latter
 * is most likely what you want to use downstream, and there is broader support
 * for it in analysis tools.
 *
 * @tparam S: A MatchStrategy value that determines the 'basic' object we match
 * the ProtoParticle via.
 *
 * @see MergeLinksByKeysToVector: Produces a suitable input type for this algorithm.
 *
 */
template <MatchStrategy S>
struct ProtoParticleAssociator
    : public LHCb::Algorithm::MultiTransformer<std::tuple<PP2MCPLinks, PP2MCPTable>(
          const LHCb::ProtoParticle::Range&, const LHCb::MCParticles&, const LinksByKeyVOC& )> {

  using BasicType = typename TypeHelper<S>::BasicType;

  ProtoParticleAssociator( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MultiTransformer<std::tuple<PP2MCPLinks, PP2MCPTable>(
            const LHCb::ProtoParticle::Range&, const LHCb::MCParticles&, const LinksByKeyVOC& )>(
            name, pSvcLocator,
            { KeyValue{ "InputProtoParticles", "" }, KeyValue{ "InputMCParticles", "" },
              KeyValue{ "InputAssociations", "" } },
            { KeyValue{ "OutputLinks", "" }, KeyValue{ "OutputTable", "" } } ){};

  std::tuple<PP2MCPLinks, PP2MCPTable> operator()( const LHCb::ProtoParticle::Range& protos, const LHCb::MCParticles&,
                                                   const LinksByKeyVOC&              basic_links ) const override {
    // Validate input tables
    auto valid_links = std::all_of( basic_links.begin(), basic_links.end(), []( const auto* l ) {
      return ( l->sourceClassID() == BasicType::classID() ) && ( l->targetClassID() == LHCb::MCParticle::classID() );
    } );
    if ( !valid_links ) {
      throw GaudiException{ "An input LinksByKey has unexpected source -> target class IDs", "ProtoParticleAssociator",
                            StatusCode::FAILURE };
    }

    // Initialise output link and relations tables
    PP2MCPLinks pp_links{ std::in_place_type<LHCb::ProtoParticle>, std::in_place_type<LHCb::MCParticle>,
                          LHCb::LinksByKey::Order::decreasingWeight };
    PP2MCPTable pp_table( 1000 );

    for ( auto* const pp : protos ) {
      // Check all ProtoParticle objects have the right basic type
      if constexpr ( S == Charged ) {
        if ( pp->track() == nullptr ) {
          throw GaudiException{ "Unexpected neutral ProtoParticle object", "ChargedProtoParticleAssociator",
                                StatusCode::FAILURE };
        }
      } else {
        if ( pp->track() != nullptr ) {
          throw GaudiException{ "Unexpected charged ProtoParticle object", "NeutralProtoParticleAssociator",
                                StatusCode::FAILURE };
        }
      }

      auto&        basics        = TypeHelper<S>::allBasics( pp );
      bool         is_associated = false;
      unsigned int n_mcparticles = 0;
      // For each basic held by this ProtoParticle, find all relations known to
      // the input tables. For each Basic -> MCP relation, create an output
      // ProtoParticle -> MCParticle in the output tables.
      for ( auto basic : basics ) {
        for ( const auto* l : basic_links ) {
          for ( const auto& [to, weight] : LinkedTo<LHCb::MCParticle>{ l }.weightedRange( basic.target() ) ) {
            is_associated = true;
            ++n_mcparticles;
            pp_links.link( pp, &to, weight );
            pp_table.relate( pp, &to, weight )
                .orThrow( "Unexpected duplicate relation found", "ProtoParticleAssociator::operator()" );
          }
        }
      }
      m_efficiency += is_associated;
      if ( is_associated ) { m_mcparticles_per_proto += n_mcparticles; }
    }
    pp_table.i_sort();
    return { pp_links, pp_table };
  };

private:
  mutable Gaudi::Accumulators::BinomialCounter<>  m_efficiency{ this, "Efficiency" };
  mutable Gaudi::Accumulators::AveragingCounter<> m_mcparticles_per_proto{ this, "MC particles per ProtoParticle" };
};

DECLARE_COMPONENT_WITH_ID( ProtoParticleAssociator<Charged>, "ChargedProtoParticleAssociator" )
DECLARE_COMPONENT_WITH_ID( ProtoParticleAssociator<Neutral>, "NeutralProtoParticleAssociator" )
