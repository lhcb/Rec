/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/HltDecReports.h"
#include "Event/HltLumiSummary.h"
#include "Event/ODIN.h"

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include <LHCbAlgs/Consumer.h>

using InputLumi = LHCb::HltLumiSummary;

using InputHlt = LHCb::HltDecReports;

class LumiSummaryTuple
    : public virtual LHCb::Algorithm::Consumer<void( const InputLumi&, const LHCb::ODIN& ),
                                               Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {
private:
  Gaudi::Property<std::string> m_tupleName29kHz{ this, "LumiTuple29kHz", "LumiTuple29kHz" };
  Gaudi::Property<std::string> m_tupleName1kHz{ this, "LumiTuple1kHz", "LumiTuple1kHz" };

public:
  virtual void operator()( const InputLumi&, const LHCb::ODIN& ) const override;
  LumiSummaryTuple( const std::string& name, ISvcLocator* pSvcLocator );
};

DECLARE_COMPONENT_WITH_ID( LumiSummaryTuple, "LumiSummaryTuple" );

LumiSummaryTuple::LumiSummaryTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {
                    KeyValue{ "InputLumi", "" },
                    KeyValue{ "ODIN", "" },
                } ) {}

void LumiSummaryTuple::operator()( const InputLumi& summary, const LHCb::ODIN& odin ) const {

  // select the 1 KHz or 30 KHz tuple
  auto tuple = nTuple( fmod( odin.orbitNumber(), 30 ) == 1 ? m_tupleName1kHz : m_tupleName29kHz );
  auto sc    = tuple->column( "runNumber", odin.runNumber() );
  sc &= tuple->column( "gpsTime", static_cast<unsigned long long>( odin.gpsTime() ) );
  sc &= tuple->column( "eventNumber", static_cast<unsigned long long>( odin.eventNumber() ) );
  sc &= tuple->column( "eventType", odin.eventType() );
  sc &= tuple->column( "triggerType", odin.triggerType() );
  sc &= tuple->column( "orbitNumber", odin.orbitNumber() );
  sc &= tuple->column( "bxType", static_cast<std::uint16_t>( odin.bunchCrossingType() ) );
  sc &= tuple->column( "bxId", odin.bunchId() );
  sc &= tuple->column( "step", odin.calibrationStep() );
  for ( const auto& kvp : summary.extraInfo() ) {
    sc &= tuple->column( kvp.first, kvp.second );
    // info() << "event "<< odin.eventNumber() << " " << kvp.first << " " << kvp.second << endmsg;
  }

  sc.andThen( [&] { return tuple->write(); } ).orThrow( "Failed to fill ntuple", "LumiSummaryTuple Alg" );
}
