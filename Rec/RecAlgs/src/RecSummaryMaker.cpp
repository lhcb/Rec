/***************************************************************************** \
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RecSummary.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/EmptyProducer.h"
#include "LHCbAlgs/Transformer.h"

// All neccessary objects
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/FTLiteCluster.h"
#include "Event/PrHits.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/VPFullCluster.h"
#include "Kernel/RichSmartID.h"
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichSIMDTypes.h"

namespace {
  using Tracks        = LHCb::Track::Range;
  using PVs           = LHCb::RecVertices;
  using SciFiClusters = LHCb::FTLiteCluster::FTLiteClusters;
  using VeloHits      = LHCb::Pr::Hits<LHCb::Pr::HitType::VP>;
  using UTClusters    = LHCb::Pr::Hits<LHCb::Pr::HitType::UT>;
  using Digits        = LHCb::Event::Calo::Digits;
  using ecalClusters  = LHCb::Event::Calo::Clusters;
  using RichPixels    = Rich::Future::DAQ::DecodedData;
  using MuonHits      = LHCb::Pr::Hits<LHCb::Pr::HitType::Muon>;
} // namespace

class RecSummaryMaker : public LHCb::Algorithm::Transformer<LHCb::RecSummary(
                            Tracks const&, Tracks const&, Tracks const&, Tracks const&, Tracks const&, PVs const&,
                            SciFiClusters const&, VeloHits const&, UTClusters const&, ecalClusters const&,
                            Digits const&, Digits const&, RichPixels const&, MuonHits const& )> {
public:
  RecSummaryMaker( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "LongTracks", "" }, KeyValue{ "VeloTracks", "" }, KeyValue{ "UpstreamTracks", "" },
                       KeyValue{ "DownstreamTracks", "" }, KeyValue{ "Ttracks", "" }, KeyValue{ "PVs", "" },
                       KeyValue{ "SciFiClusters", "" }, KeyValue{ "VeloClusters", "" }, KeyValue{ "UTClusters", "" },
                       KeyValue{ "eCalClusters", "" }, KeyValue{ "EDigits", "" }, KeyValue{ "HDigits", "" },
                       KeyValue{ "RichPixels", "" }, KeyValue{ "MuonHits", "" } },
                     KeyValue{ "Output", "" } ) {}

  LHCb::RecSummary operator()( Tracks const& longtracks, Tracks const& velotracks, Tracks const& uptracks,
                               Tracks const& downtracks, Tracks const& ttracks, PVs const& pvs,
                               SciFiClusters const& scifi_clusters, VeloHits const& velohits, UTClusters const& uthits,
                               ecalClusters const& ecal_clusters, Digits const& edigits, Digits const& hdigits,
                               RichPixels const& richhits, MuonHits const& muonhits ) const override {
    LHCb::RecSummary summary{};

    auto energy = []( auto init, const auto& digit ) { return init + digit.energy(); };

    const auto ecaltot = std::accumulate( edigits.begin(), edigits.end(), 0., energy );
    const auto hcaltot = std::accumulate( hdigits.begin(), hdigits.end(), 0., energy );

    auto       isVelo = []( const auto& track ) { return track->checkType( LHCb::Track::Types::Velo ); };
    const auto nVelo  = std::count_if( velotracks.begin(), velotracks.end(), isVelo );

    auto       isVeloBack = []( const auto& track ) { return track->checkType( LHCb::Track::Types::VeloBackward ); };
    const auto nBack      = std::count_if( velotracks.begin(), velotracks.end(), isVeloBack );

    using DT = LHCb::RecSummary::DataTypes;
    summary.addInfo( DT::nPVs, pvs.size() );
    summary.addInfo( DT::nFTClusters, scifi_clusters.size() );
    summary.addInfo( DT::nVPClusters, velohits.size() );
    summary.addInfo( DT::nUTClusters, uthits.size() );
    summary.addInfo( DT::nEcalClusters, ecal_clusters.size() );
    summary.addInfo( DT::eCalTot, ecaltot );
    summary.addInfo( DT::hCalTot, hcaltot );

    summary.addInfo( DT::nLongTracks, longtracks.size() );
    summary.addInfo( DT::nDownstreamTracks, downtracks.size() );
    summary.addInfo( DT::nUpstreamTracks, uptracks.size() );
    summary.addInfo( DT::nTTracks, ttracks.size() );
    summary.addInfo( DT::nVeloTracks, nVelo );
    summary.addInfo( DT::nBackTracks, nBack );

    summary.addInfo( DT::nRich1Hits, richhits.nTotalHits( Rich::Rich1 ) );
    summary.addInfo( DT::nRich2Hits, richhits.nTotalHits( Rich::Rich2 ) );

    summary.addInfo( DT::nMuonCoordsS1, muonhits.hits( 0 ).size() );
    summary.addInfo( DT::nMuonCoordsS2, muonhits.hits( 1 ).size() );
    summary.addInfo( DT::nMuonCoordsS3, muonhits.hits( 2 ).size() );
    summary.addInfo( DT::nMuonCoordsS4, muonhits.hits( 3 ).size() );

    return summary;
  }
};

DECLARE_COMPONENT( RecSummaryMaker )

class RecSummaryPrinter : public LHCb::Algorithm::Consumer<void( LHCb::RecSummary const& )> {
public:
  RecSummaryPrinter( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, { KeyValue{ "Input", "" } } ){};

  void operator()( LHCb::RecSummary const& summary ) const override { always() << summary << endmsg; };
};

DECLARE_COMPONENT( RecSummaryPrinter )

// Algorithm to create an empty RecSummary
DECLARE_COMPONENT_WITH_ID( EmptyProducer<LHCb::RecSummary>, "FakeRecSummaryMaker" )
