/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/HltDecReports.h"
#include "Event/HltLumiSummary.h"
#include "Event/ODIN.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"
#include <LHCbAlgs/Consumer.h>

#include "DetDesc/DetectorElement.h"
#include "TrackInterfaces/IPVOfflineTool.h"

using InputVeloTracks = LHCb::Event::v1::Tracks;
using InputHlt        = LHCb::HltDecReports;
using RecPV           = LHCb::RecVertex;
class LumiPVs_nobeamline
    : public virtual LHCb::Algorithm::Consumer<
          void( const LHCb::ODIN&, const InputVeloTracks&, const InputHlt&, const DetectorElement& ),
          LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DetectorElement>> {
private:
  Gaudi::Property<std::string> m_tupleName{ this, "BeamGasTuple", "BeamGasTuple" };
  ToolHandle<IPVOfflineTool>   m_toolpv{ this, "PVOfflineTool", "PVOfflineTool" };
  LHCb::RecVertices            reconstructMultiPVFromTracks( std::vector<const LHCb::Event::v1::Track*>& tracks2use,
                                                             Gaudi::XYZPoint const&                      beamSpot ) const;

public:
  virtual void operator()( const LHCb::ODIN&, const InputVeloTracks&, const InputHlt&,
                           const DetectorElement& ) const override;
  LumiPVs_nobeamline( const std::string& name, ISvcLocator* pSvcLocator );
};

DECLARE_COMPONENT_WITH_ID( LumiPVs_nobeamline, "LumiPVs_nobeamline" );
LumiPVs_nobeamline::LumiPVs_nobeamline( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "ODIN", "" }, KeyValue{ "InputVeloTracks", "" }, KeyValue{ "InputHlt", "" },
                  KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } } ) {}

void LumiPVs_nobeamline::operator()( const LHCb::ODIN& odin, const InputVeloTracks& tracks, const InputHlt& decreport,
                                     const DetectorElement& lhcb ) const {

  auto tuple = nTuple( m_tupleName );

  std::vector<const LHCb::Track*> selectedtracks;
  std::vector<RecPV>              vtxlist;
  std::copy_if( tracks.begin(), tracks.end(), std::back_inserter( selectedtracks ),
                []( const LHCb::Track* tr ) { return tr->hasVelo(); } );

  m_toolpv->reconstructMultiPVFromTracks( selectedtracks, vtxlist, *lhcb.geometry() ).ignore();

  auto sc = tuple->column( "runNumber", odin.runNumber() );
  sc      = tuple->column( "gpsTime", static_cast<unsigned long long>( odin.gpsTime() ) );
  sc      = tuple->column( "eventNumber", static_cast<unsigned long long>( odin.eventNumber() ) );
  sc      = tuple->column( "eventType", odin.eventType() );
  sc      = tuple->column( "triggerType", odin.triggerType() );
  sc      = tuple->column( "orbitNumber", odin.orbitNumber() );
  sc      = tuple->column( "bxType", static_cast<std::uint16_t>( odin.bunchCrossingType() ) );
  sc      = tuple->column( "bxId", odin.bunchId() );
  sc      = tuple->column( "step", odin.calibrationStep() );
  sc      = tuple->column( "nVeloTracks", static_cast<unsigned long long>( tracks.size() ) );
  for ( const auto& kvp : decreport ) { sc = tuple->column( kvp.first, kvp.second.decision() ); }

  sc = tuple->farray(
      { { "PV_nTracks", +[]( const RecPV& pv ) -> double { return pv.tracks().size(); } },
        { "PV_chi2ndof", +[]( const RecPV& pv ) { return pv.chi2() / pv.nDoF(); } },
        { "PVX", +[]( const RecPV& pv ) { return pv.position().x(); } },
        { "PVY", +[]( const RecPV& pv ) { return pv.position().y(); } },
        { "PVZ", +[]( const RecPV& pv ) { return pv.position().z(); } },
        { "PVR",
          +[]( const RecPV& pv ) { return sqrt( pow( pv.position().x(), 2 ) + pow( pv.position().y(), 2 ) ); } } },
      vtxlist.begin(), vtxlist.end(), "nPVs", 1000 );

  sc.andThen( [&] { return tuple->write(); } ).orThrow( "Failed to fill ntuple", "LumiPV_nobeamline Alg" );
}
