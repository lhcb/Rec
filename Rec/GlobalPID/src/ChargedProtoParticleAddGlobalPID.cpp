/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GlobalPID/ProbNNs.h"
#include "Interfaces/IProtoParticleTool.h"

namespace LHCb::Rec::GlobalPID {

  template <typename Model, LHCb::GlobalChargedPID::ProbNN pid_type>
  class AddGlobalPID final : public extends<GaudiTool, Interfaces::IProtoParticles> {
  public:
    using extends::extends;

    StatusCode initialize() override {
      StatusCode scc = extends::initialize();
      if ( scc.isFailure() ) return scc;
      m_model = std::make_unique<Model>();
      if ( auto fname = m_weightsfilename.value(); fname != "" ) {
        auto buffer = m_filesvc->read( fname );
        scc &= m_model->load( buffer );
      } else {
        debug() << "No weights file set, using default (i.e. 0) values for weights for " << name() << endmsg;
      }
      return scc;
    };

    StatusCode operator()( ProtoParticles& protos, IGeometryInfo const& ) const override {
      // select only charged tracks of a certain track type
      auto select = [&]( LHCb::ProtoParticle const* proto ) -> bool {
        return proto->track() && proto->track()->type() == m_tracktype;
      };

      // get the one dimensional output and add to protoparticle
      auto save_output = [&]( LHCb::ProtoParticle* proto, LHCb::span<float> output ) -> void {
        LHCb::GlobalChargedPID* pid = proto->globalChargedPID();
        if ( pid ) {
          pid->setProbNN<pid_type>( output[0] );
        } else {
          auto new_pid = std::make_unique<LHCb::GlobalChargedPID>();
          new_pid->setProbNN<pid_type>( output[0] );
          proto->setGlobalChargedPID( new_pid.release() );
        }
      };

      // evaluate MLP with SIMD
      m_model->evaluate( protos, select, save_output );

      return StatusCode::SUCCESS;
    };

  private:
    // properties
    Gaudi::Property<std::string> m_weightsfilename{ this, "WeightsFileName", "",
                                                    "location of weights file, to be read with ParamFileSvc" };
    LHCb::Track::Types           m_tracktype = LHCb::Track::Types::Unknown;
    Gaudi::Property<std::string> m_trktypeprop{ this, "TrackType", "UNDEFINED", [&]( auto& ) {
                                                 if ( parse( m_tracktype, m_trktypeprop ).isFailure() )
                                                   m_tracktype = LHCb::Track::Types::Unknown;
                                               } };

  private:
    // data members
    std::unique_ptr<Model> m_model;

    // services
    ServiceHandle<IFileAccess> m_filesvc{ this, "FileAccessor", "ParamFileSvc",
                                          "Service used to retrieve file contents" };
  };

  using AddGlobalPID_Electron = AddGlobalPID<ProbNN::Long::Electron::Model, LHCb::GlobalChargedPID::ProbNN::Electron>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Electron, "ChargedProtoParticleAddGlobalPID_Electron" )

  using AddGlobalPID_Muon = AddGlobalPID<ProbNN::Long::Muon::Model, LHCb::GlobalChargedPID::ProbNN::Muon>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Muon, "ChargedProtoParticleAddGlobalPID_Muon" )

  using AddGlobalPID_Pion = AddGlobalPID<ProbNN::Long::Pion::Model, LHCb::GlobalChargedPID::ProbNN::Pion>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Pion, "ChargedProtoParticleAddGlobalPID_Pion" )

  using AddGlobalPID_Kaon = AddGlobalPID<ProbNN::Long::Kaon::Model, LHCb::GlobalChargedPID::ProbNN::Kaon>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Kaon, "ChargedProtoParticleAddGlobalPID_Kaon" )

  using AddGlobalPID_Proton = AddGlobalPID<ProbNN::Long::Proton::Model, LHCb::GlobalChargedPID::ProbNN::Proton>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Proton, "ChargedProtoParticleAddGlobalPID_Proton" )

  using AddGlobalPID_Ghost = AddGlobalPID<ProbNN::Long::Ghost::Model, LHCb::GlobalChargedPID::ProbNN::Ghost>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Ghost, "ChargedProtoParticleAddGlobalPID_Ghost" )

  // for reduced upstream features (reduced w.r.t long/downstream)
  using AddGlobalPID_Electron_Upstream =
      AddGlobalPID<ProbNN::Upstream::Electron::Model, LHCb::GlobalChargedPID::ProbNN::Electron>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Electron_Upstream, "ChargedProtoParticleAddGlobalPID_Electron_Upstream" )

  using AddGlobalPID_Muon_Upstream = AddGlobalPID<ProbNN::Upstream::Muon::Model, LHCb::GlobalChargedPID::ProbNN::Muon>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Muon_Upstream, "ChargedProtoParticleAddGlobalPID_Muon_Upstream" )

  using AddGlobalPID_Pion_Upstream = AddGlobalPID<ProbNN::Upstream::Pion::Model, LHCb::GlobalChargedPID::ProbNN::Pion>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Pion_Upstream, "ChargedProtoParticleAddGlobalPID_Pion_Upstream" )

  using AddGlobalPID_Kaon_Upstream = AddGlobalPID<ProbNN::Upstream::Kaon::Model, LHCb::GlobalChargedPID::ProbNN::Kaon>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Kaon_Upstream, "ChargedProtoParticleAddGlobalPID_Kaon_Upstream" )

  using AddGlobalPID_Proton_Upstream =
      AddGlobalPID<ProbNN::Upstream::Proton::Model, LHCb::GlobalChargedPID::ProbNN::Proton>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Proton_Upstream, "ChargedProtoParticleAddGlobalPID_Proton_Upstream" )

  using AddGlobalPID_Ghost_Upstream =
      AddGlobalPID<ProbNN::Upstream::Ghost::Model, LHCb::GlobalChargedPID::ProbNN::Ghost>;
  DECLARE_COMPONENT_WITH_ID( AddGlobalPID_Ghost_Upstream, "ChargedProtoParticleAddGlobalPID_Ghost_Upstream" )

} // namespace LHCb::Rec::GlobalPID
