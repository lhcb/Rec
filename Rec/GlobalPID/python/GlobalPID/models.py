###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from cppyy import gbl
from LHCbMath.VectorizedML import Sequence

# Long and Downstream track models
ProbNN__Electron__Model = Sequence(gbl.ProbNN.Long.Electron.Model())
ProbNN__Muon__Model = Sequence(gbl.ProbNN.Long.Muon.Model())
ProbNN__Pion__Model = Sequence(gbl.ProbNN.Long.Pion.Model())
ProbNN__Kaon__Model = Sequence(gbl.ProbNN.Long.Kaon.Model())
ProbNN__Proton__Model = Sequence(gbl.ProbNN.Long.Proton.Model())
ProbNN__Ghost__Model = Sequence(gbl.ProbNN.Long.Ghost.Model())

# Upstream track models
ProbNN__Upstream__Electron__Model = Sequence(gbl.ProbNN.Upstream.Electron.Model())
ProbNN__Upstream__Muon__Model = Sequence(gbl.ProbNN.Upstream.Muon.Model())
ProbNN__Upstream__Pion__Model = Sequence(gbl.ProbNN.Upstream.Pion.Model())
ProbNN__Upstream__Kaon__Model = Sequence(gbl.ProbNN.Upstream.Kaon.Model())
ProbNN__Upstream__Proton__Model = Sequence(gbl.ProbNN.Upstream.Proton.Model())
ProbNN__Upstream__Ghost__Model = Sequence(gbl.ProbNN.Upstream.Ghost.Model())
