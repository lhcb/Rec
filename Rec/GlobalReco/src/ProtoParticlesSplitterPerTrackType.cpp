/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ProtoParticle.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  template <typename ProtosOut, typename OutData = std::tuple<ProtosOut, ProtosOut, ProtosOut, ProtosOut>>
  struct ProtoParticlesSplitterPerTrackType
      : Algorithm::MultiTransformer<OutData( ProtoParticle::Range const& protos )> {
    using KeyValue = typename ProtoParticlesSplitterPerTrackType<ProtosOut>::KeyValue;

    ProtoParticlesSplitterPerTrackType( const std::string& name, ISvcLocator* pSvc )
        : ProtoParticlesSplitterPerTrackType<ProtosOut>::MultiTransformer(
              name, pSvc, { KeyValue( "InputProtos", "" ) },
              { KeyValue( "LongProtos", "" ), KeyValue( "DownstreamProtos", "" ), KeyValue( "UpstreamProtos", "" ),
                KeyValue( "TtrackProtos", "" ) } ) {}

    OutData operator()( ProtoParticle::Range const& protos ) const override {
      OutData out_protos;
      auto& [long_protos, down_protos, up_protos, t_protos] = out_protos;

      for ( auto proto : protos ) {
        switch ( proto->track()->type() ) {
        case LHCb::Track::Types::Long:
          long_protos.insert( proto );
          break;
        case LHCb::Track::Types::Downstream:
          down_protos.insert( proto );
          break;
        case LHCb::Track::Types::Upstream:
          up_protos.insert( proto );
          break;
        case LHCb::Track::Types::Ttrack:
          t_protos.insert( proto );
          break;
        default:
          break;
        }
      }

      return out_protos;
    }
  };

  using ProtoParticlesSharedSplitterPerTrackType = ProtoParticlesSplitterPerTrackType<LHCb::ProtoParticle::Selection>;

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::ProtoParticlesSharedSplitterPerTrackType, "ProtosSharedSplitterPerTrackType" )
