/*****************************************************************************\
 * (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ProtoParticle.h"
#include "LHCbAlgs/EmptyProducer.h"

/** @class ProtoParticlesEmptyProducer
 * @brief dummy producer of an empty container of particles
 */

DECLARE_COMPONENT_WITH_ID( EmptyProducer<LHCb::ProtoParticles>, "ProtoParticlesEmptyProducer" )
