###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep


# Returns a configured monitoring (MC free) sequence.
def RichRecMonitors(
    # Optional name given to this group.
    GroupName="",
    # ===========================================================
    # General Data processing options
    # ===========================================================
    # The data type
    dataType="",
    # Flag to indicate we are running in 'Online Brunel' mode
    onlineBrunelMode=False,
    # radiators to process
    radiators=["Rich1Gas", "Rich2Gas"],
    # enable 4D reco in each radiator
    enable4DReco=(False, False),
    # ===========================================================
    # Pixel treatment options
    # ===========================================================
    # Should pixel clustering be run, for either ( RICH1, RICH2 )
    applyPixelClustering=(False, False),
    # ===========================================================
    # Input / Output options
    # ===========================================================
    # Dict of input track locations with name key
    inputTrackLocations={"All": "Rec/Track/Best"},
    # Dict of output PID locations. Name keys must match above.
    outputPIDLocations={"All": "Rec/Rich/PIDs"},
    # ===========================================================
    # Monitoring options
    # ===========================================================
    # The histogram set to use
    histograms="OfflineFull",
    # Dictionary setting the histograms for each set
    monitors={
        "Expert": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "RichMaterial",
            "PixelBackgrounds",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "PhotonYields",
            "GeomEffs",
            "MassRings",
            "SIMDPerf",
            "TrackGeometry",
        ],
        "OfflineFull": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "RichMaterial",
            "PixelBackgrounds",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "PhotonYields",
            "GeomEffs",
            "MassRings",
            "TrackGeometry",
        ],
        "OfflineExpress": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "TrackGeometry",
        ],
        "Online": [
            "RecoStats",
            "RichHits",
            "PixelClusters",
            "TrackSelEff",
            "PhotonCherenkovAngles",
            "DLLs",
            "TrackGeometry",
        ],
        "None": [],
    },
    # ===========================================================
    # Technical options
    # ===========================================================
    # Turn on CPU time measurements
    MeasureTime=True,
):
    from Configurables import GaudiSequencer
    from RichFutureRecSys.ConfiguredRichReco import (
        CheckConfig,
        RichTESPixelMap,
        RichTESTrackMap,
        makeRichAlg,
    )

    # Check the configuration
    CheckConfig(inputTrackLocations, outputPIDLocations)

    # Check histogram set is known
    if histograms not in list(monitors.keys()):
        raise ValueError("Unknown histogram set " + histograms)

    # Dictionary of general algorithm properties
    algprops = {}

    # RICH Particle properties
    ppTool = "Rich::Future::ParticleProperties/RichPartProp" + GroupName + ":PUBLIC"
    algprops["ParticlePropertiesTool"] = ppTool

    # Detectors
    radsF = ("Rich1Gas" in radiators, "Rich2Gas" in radiators)
    algprops["Detectors"] = radsF

    # change TransportSvc in case of DD4hep
    if UseDD4Hep:
        algprops["TransportSvc"] = "TGeoTransportSvc"

    # Units
    GeV = 1000

    # Name for pixel locations
    pixname = ""
    if algprops["Detectors"][0]:
        pixname += "RICH1"
    if algprops["Detectors"][1]:
        pixname += "RICH2"

    # Clustering name extension
    clusName = ""
    if applyPixelClustering[0]:
        clusName += "R1ClusON"
    if applyPixelClustering[1]:
        clusName += "R2ClusON"

    # Pixel TES locations
    cLocs = RichTESPixelMap(pixname + clusName)

    # The complete sequence
    all = GaudiSequencer("RichMoni" + GroupName, MeasureTime=MeasureTime)

    # First track independent monitors

    # Rich Hits
    if "RichHits" in monitors[histograms]:
        from Configurables import Rich__Future__Rec__Moni__DetectorHits as DetectorHits

        richHits = makeRichAlg(DetectorHits, "RichRecPixelQC" + GroupName, algprops)
        richHits.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"
        ]
        all.Members += [richHits]

    # clusters
    if "PixelClusters" in monitors[histograms]:
        from Configurables import Rich__Future__Rec__Moni__PixelClusters as ClusterMoni
        from Configurables import Rich__Future__SmartIDClustering as RichClustering

        # Custom cluster location for monitor. Need to run clustering here as it
        # might not be enabled in the reco itself
        clusLoc = "Rec/Rich/PixelClusters/Monitoring"
        # The clustering
        clustering = makeRichAlg(
            RichClustering, "RichFutureMoniClustering" + GroupName, algprops
        )
        clustering.ApplyPixelClustering = (True, True)
        clustering.RichPixelClustersLocation = clusLoc
        # The monitoring
        clusMoni = makeRichAlg(
            ClusterMoni, "RichRecPixelClusters" + GroupName, algprops
        )
        clusMoni.RichPixelClustersLocation = clusLoc
        # Add to the sequence
        all.Members += [clustering, clusMoni]

    # Loop over tracks
    for tktype, trackLocation in sorted(inputTrackLocations.items()):
        # name for this sequence
        name = tktype + GroupName

        # Sequence for all moniotors for this track type
        tkSeq = GaudiSequencer("RichMoni" + name, MeasureTime=MeasureTime)
        all.Members += [tkSeq]

        # ==================================================================
        # Intermediary data locations.
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        # Basic reco stats.
        if "RecoStats" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__SIMDRecoStats as RecoStats,
            )

            recoStats = makeRichAlg(RecoStats, "RichRecoStats" + name, algprops, tktype)
            # Inputs
            recoStats.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            recoStats.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            recoStats.TrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
            recoStats.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            # Properties
            recoStats.Enable4D = enable4DReco
            # Add to sequence
            tkSeq.Members += [recoStats]

        if "TrackSelEff" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__TrackSelEff as TrackSelEff,
            )

            tkSelEff = makeRichAlg(
                TrackSelEff, "Ri" + tktype + "TrkEff" + GroupName, algprops, tktype
            )
            # Inputs
            tkSelEff.TracksLocation = trackLocation
            tkSelEff.RichPIDsLocation = outputPIDLocations[tktype]
            # Add to sequence
            tkSeq.Members += [tkSelEff]

        if "TrackGeometry" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__TrackGeometry as TrackGeometry,
            )

            tkGeom = makeRichAlg(
                TrackGeometry, "Ri" + tktype + "TrkGeom" + GroupName, algprops, tktype
            )
            # Inputs
            tkGeom.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            # Add to sequence
            tkSeq.Members += [tkGeom]

        if "PhotonCherenkovAngles" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PhotAngles,
            )
            from Configurables import TrackSelector

            # Standard monitor
            ckAngles = makeRichAlg(PhotAngles, "RiCKRes" + name, algprops, tktype)
            # Inputs
            ckAngles.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            ckAngles.TracksLocation = trackLocation
            ckAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            ckAngles.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            ckAngles.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            ckAngles.SummaryTracksLocation = locs["SummaryTracksLocation"]
            ckAngles.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            ckAngles.PhotonMirrorDataLocation = locs["PhotonMirrorDataLocation"]
            # Options
            if onlineBrunelMode:
                # Open up the CK res plot range, for the Wide photon selection
                ckAngles.CKResHistoRange = (0.008, 0.004)

            # Monitor with tight tracking cuts
            ckAnglesT = makeRichAlg(
                PhotAngles, "RiCKRes" + name + "Tight", algprops, tktype
            )
            # Inputs
            ckAnglesT.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            ckAnglesT.TracksLocation = trackLocation
            ckAnglesT.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            ckAnglesT.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            ckAnglesT.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            ckAnglesT.SummaryTracksLocation = locs["SummaryTracksLocation"]
            ckAnglesT.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            ckAnglesT.PhotonMirrorDataLocation = locs["PhotonMirrorDataLocation"]
            # Options
            if onlineBrunelMode:
                # Clone settings from the nominal monitor
                ckAnglesT.CKResHistoRange = ckAngles.CKResHistoRange
            # Tracks selection
            ckAnglesT.addTool(TrackSelector)
            ckAnglesT.TrackSelector.MinPCut = 10 * GeV
            ckAnglesT.TrackSelector.MinPtCut = 0.5 * GeV
            ckAnglesT.TrackSelector.MaxChi2Cut = 2
            ckAnglesT.TrackSelector.MaxGhostProbCut = 0.2

            # Add to sequence
            tkSeq.Members += [ckAngles, ckAnglesT]

        if "PhotonYields" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__PhotonYield as PhotonYield,
            )

            photYE = makeRichAlg(
                PhotonYield, "RiTkEmittedYields" + name, algprops, tktype
            )
            photYE.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            photYE.PhotonYieldLocation = locs["EmittedPhotonYieldLocation"]
            photYE.MaximumYields = (800, 800)
            photYD = makeRichAlg(
                PhotonYield, "RiTkDetectableYields" + name, algprops, tktype
            )
            photYD.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            photYD.PhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
            photYS = makeRichAlg(
                PhotonYield, "RiTkSignalYields" + name, algprops, tktype
            )
            photYS.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            photYS.PhotonYieldLocation = locs["SignalPhotonYieldLocation"]
            # Add to the sequence
            tkSeq.Members += [photYE, photYD, photYS]

        if "GeomEffs" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__GeometricalEfficiencies as GeomEffs,
            )

            geomEffs = makeRichAlg(GeomEffs, "RiTkGeomEffs" + name, algprops, tktype)
            geomEffs.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            geomEffs.GeomEffsLocation = locs["GeomEffsLocation"]
            # Add to the sequence
            tkSeq.Members += [geomEffs]

        if "MassRings" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__MassHypoRings as MassRings,
            )

            mringMoni = makeRichAlg(MassRings, "RichMassRings" + name, algprops, tktype)
            mringMoni.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mringMoni.TrackLocalPointsLocation = locs["TrackLocalPointsLocation"]
            mringMoni.MassHypothesisRingsLocation = locs[
                "EmittedMassHypothesisRingsLocation"
            ]
            mringMoni.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            # Add to the sequence
            tkSeq.Members += [mringMoni]

        if "RichMaterial" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__TrackRadiatorMaterial as TkMaterial,
            )

            tkMat = makeRichAlg(TkMaterial, "RiTkMaterial" + name, algprops, tktype)
            # Inputs
            tkMat.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            # Add to sequence
            tkSeq.Members += [tkMat]

        if "PixelBackgrounds" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__PixelBackgrounds as PixBackgrds,
            )

            # The monitor
            bkgMoni = makeRichAlg(
                PixBackgrds, "RichRecPixBkgs" + name, algprops, tktype
            )
            bkgMoni.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            bkgMoni.PixelBackgroundsLocation = "Rec/Rich/PixelBackgrounds/It1/" + name
            # Add to the sequence
            tkSeq.Members += [bkgMoni]

        if "DLLs" in monitors[histograms]:
            from Configurables import Rich__Future__Rec__Moni__DLLs as DLLs

            dllMoni = makeRichAlg(DLLs, "RichDLLs" + name, algprops, tktype)
            dllMoni.RichPIDsLocation = outputPIDLocations[tktype]
            # Add to the sequence
            tkSeq.Members += [dllMoni]

        if "SIMDPerf" in monitors[histograms]:
            from Configurables import (
                Rich__Future__Rec__Moni__PhotonSIMDEfficiency as PhotSIMDEff,
            )

            phSIMDEff = makeRichAlg(
                PhotSIMDEff, "RichPhotonSIMDEff" + name, algprops, tktype
            )
            phSIMDEff.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            phSIMDEff.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            phSIMDEff.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            # Add to the sequence
            tkSeq.Members += [phSIMDEff]

    # return the full monitoring sequence
    return all


# Returns a configured MC checking sequence.
def RichRecCheckers(
    # Optional name given to this group.
    GroupName="",
    # ===========================================================
    # General Data processing options
    # ===========================================================
    # radiators to process
    radiators=["Rich1Gas", "Rich2Gas"],
    # Should pixel clustering be run, for either ( RICH1, RICH2 )
    applyPixelClustering=(False, False),
    # Flag to indicate we are running in 'Online Brunel' mode
    onlineBrunelMode=False,
    # enable 4D reco in each radiator
    enable4DReco=(False, False),
    # ===========================================================
    # Input / Output options
    # ===========================================================
    # Dict of input track locations with name key
    inputTrackLocations={"All": "Rec/Track/Best"},
    # Dict of output PID locations. Name keys must match above.
    outputPIDLocations={"All": "Rec/Rich/PIDs"},
    # ===========================================================
    # Monitoring options
    # ===========================================================
    # The histogram set to use
    histograms="OfflineFull",
    # List of monitors to run
    checkers={
        "ResolutionParameterisation": ["CKResParameterisation"],
        "Expert": [
            "RichHits",
            "PIDPerformance",
            "PhotonCherenkovAngles",
            "CherenkovResolution",
            "TrackResolution",
            "MCOpticalPhotons",
            "Time",
        ],
        "OfflineFull": [
            "RichHits",
            "PIDPerformance",
            "PhotonCherenkovAngles",
            "TrackResolution",
            "Time",
        ],
        "OfflineExpress": [
            "RichHits",
            "PIDPerformance",
            "PhotonCherenkovAngles",
            "TrackResolution",
            "Time",
        ],
        "Online": [
            "RichHits",
            "PIDPerformance",
            "PhotonCherenkovAngles",
            "TrackResolution",
            "Time",
        ],
        "None": [],
    },
    # extra checkers
    extra_checkers=[],
    # Momentum selections for performance plots
    momentumCuts={
        "2to100": [2, 100],
        "2to10": [2, 10],
        "10to70": [10, 70],
        "70to100": [70, 100],
    },
    # ===========================================================
    # Techical options
    # ===========================================================
    # Turn on CPU time measurements
    MeasureTime=True,
):
    from Configurables import GaudiSequencer
    from RichFutureRecSys.ConfiguredRichReco import (
        CheckConfig,
        RichTESPixelMap,
        RichTESTrackMap,
        makeRichAlg,
    )

    # Check the configuration
    CheckConfig(inputTrackLocations, outputPIDLocations)

    # Check histogram set is known
    if histograms not in list(checkers.keys()):
        raise ValueError("Unknown histogram set " + histograms)

    # Units
    GeV = 1000

    # Momentum cuts for plotting etc (by RICH)
    pCuts = {"MinP": (0.5 * GeV, 0.5 * GeV), "MaxP": (120.0 * GeV, 120.0 * GeV)}
    # Bins for resolution parameterisation plots
    resBins = (200, 200)

    # Dictionary of general algorithm properties
    algprops = {}

    # RICH Particle properties
    ppTool = "Rich::Future::ParticleProperties/RichPartProp" + GroupName + ":PUBLIC"
    algprops["ParticlePropertiesTool"] = ppTool

    # Detectors
    radsF = ("Rich1Gas" in radiators, "Rich2Gas" in radiators)
    algprops["Detectors"] = radsF

    # Name for pixel locations
    pixname = ""
    if algprops["Detectors"][0]:
        pixname += "RICH1"
    if algprops["Detectors"][1]:
        pixname += "RICH2"

    # Clustering name extension
    clusName = ""
    if applyPixelClustering[0]:
        clusName += "R1ClusON"
    if applyPixelClustering[1]:
        clusName += "R2ClusON"

    # Pixel TES locations
    cLocs = RichTESPixelMap(pixname + clusName)

    # The complete sequence
    all = GaudiSequencer("RichCheck" + GroupName, MeasureTime=MeasureTime)

    # Are we cheating the tracking from RICH extended MC info ?
    isMCTracks = "MC" in inputTrackLocations.keys()
    if isMCTracks:
        # Demand MC tracking is done on its own
        if len(inputTrackLocations.items()) > 1:
            raise RuntimeError("MC tracking option can only be used on its own")

    # Make the Track->MCparticle relations table
    from Configurables import (
        Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels,
    )

    tkMCPRels = makeRichAlg(TkToMCPRels, "RichFutureTKToMCPsRels" + GroupName, algprops)
    if isMCTracks:
        # Create relations for a different track location
        tkMCPRels.TracksLocation = inputTrackLocations["MC"]
        tkMCPRels.MCParticlesLinkLocation = "Link/" + inputTrackLocations["MC"]
    all.Members += [tkMCPRels]

    checks = checkers[histograms] + extra_checkers

    # Rich Hits
    if "RichHits" in checks:
        from Configurables import (
            Rich__Future__Rec__MC__Moni__DetectorHits as DetectorHits,
        )

        richHits = makeRichAlg(DetectorHits, "RichMCHits" + GroupName, algprops)
        richHits.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"
        ]
        all.Members += [richHits]

    # Loop over tracks
    for tktype, trackLocation in sorted(inputTrackLocations.items()):
        # name for this sequence
        name = tktype + GroupName

        # Sequence for all moniotors for this track type
        tkSeq = GaudiSequencer("RichCheck" + name, MeasureTime=MeasureTime)
        all.Members += [tkSeq]

        # ==================================================================
        # Intermediary data locations.
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        if "PIDPerformance" in checks:
            pidSeq = GaudiSequencer("RichPIDCheck" + name, MeasureTime=MeasureTime)
            tkSeq.Members += [pidSeq]

            from Configurables import Rich__Future__Rec__MC__Moni__PIDQC as PIDQC
            from Configurables import TrackSelector

            for cutname, cuts in sorted(momentumCuts.items()):
                pidQC = makeRichAlg(
                    PIDQC, "RichPIDMon" + name + cutname, algprops, tktype
                )
                # Inputs
                pidQC.TracksLocation = trackLocation
                pidQC.RichPIDsLocation = outputPIDLocations[tktype]
                pidQC.TrackToMCParticlesRelations = (
                    tkMCPRels.TrackToMCParticlesRelations
                )
                # Cuts
                pidQC.addTool(TrackSelector)
                pidQC.TrackSelector.MinPCut = cuts[0] * GeV
                pidQC.TrackSelector.MaxPCut = cuts[1] * GeV
                # Add to sequence
                pidSeq.Members += [pidQC]

        if "PhotonCherenkovAngles" in checks:
            from Configurables import (
                Rich__Future__Rec__MC__Moni__SIMDPhotonCherenkovAngles as MCCKAngles,
            )

            # Standard monitor
            mcAngs = makeRichAlg(MCCKAngles, "RiCKMCRes" + name, algprops, tktype)
            # Add to sequence
            tkSeq.Members += [mcAngs]
            # Inputs
            mcAngs.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mcAngs.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            mcAngs.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            mcAngs.SummaryTracksLocation = locs["SummaryTracksLocation"]
            mcAngs.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            mcAngs.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
            mcAngs.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            mcAngs.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            mcAngs.TracksLocation = trackLocation
            # Options
            mcAngs.MinP = pCuts["MinP"]
            mcAngs.MaxP = pCuts["MaxP"]
            if onlineBrunelMode:
                # Open up the CK res plot range, for the Wide photon selection
                mcAngs.CKResHistoRange = (0.05, 0.008, 0.004)

            # Tight monitor
            mcAngsT = makeRichAlg(
                MCCKAngles, "RiCKMCRes" + name + "Tight", algprops, tktype
            )
            # Add to sequence
            tkSeq.Members += [mcAngsT]
            # Inputs
            mcAngsT.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mcAngsT.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            mcAngsT.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            mcAngsT.SummaryTracksLocation = locs["SummaryTracksLocation"]
            mcAngsT.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            mcAngsT.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
            mcAngsT.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            mcAngsT.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            mcAngsT.TracksLocation = trackLocation
            # Options
            mcAngsT.MinP = pCuts["MinP"]
            mcAngsT.MaxP = pCuts["MaxP"]
            if onlineBrunelMode:
                # Open up the CK res plot range, for the Wide photon selection
                mcAngsT.CKResHistoRange = mcAngs.CKResHistoRange
            # Tracks selection
            mcAngsT.addTool(TrackSelector)
            mcAngsT.TrackSelector.MinPCut = 10 * GeV
            mcAngsT.TrackSelector.MinPtCut = 0.5 * GeV
            mcAngsT.TrackSelector.MaxChi2Cut = 2
            mcAngsT.TrackSelector.MaxGhostProbCut = 0.2

        if "CherenkovResolution" in checks:
            from Configurables import (
                Rich__Future__Rec__MC__Moni__CherenkovResolution as MCCKReso,
            )

            # Make instance
            mcRes = makeRichAlg(MCCKReso, "RiCKTkMCRes" + name, algprops, tktype)
            # Add to sequence
            tkSeq.Members += [mcRes]
            # Inputs
            mcRes.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mcRes.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            mcRes.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            mcRes.SummaryTracksLocation = locs["SummaryTracksLocation"]
            mcRes.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            mcRes.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
            mcRes.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            mcRes.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            mcRes.TracksLocation = trackLocation
            mcRes.CherenkovResolutionsLocation = locs["CherenkovResolutionsLocation"]
            # Options
            mcRes.MinP = pCuts["MinP"]
            mcRes.MaxP = pCuts["MaxP"]
            mcRes.NPullBins = resBins

        if "CKResParameterisation" in checks:
            from Configurables import (
                Rich__Future__Rec__MC__Moni__CKResParameterisation as MCCKResParam,
            )

            # Make instance
            resParam = makeRichAlg(
                MCCKResParam, "RiMCCKResParam" + name, algprops, tktype
            )
            # Add to sequence
            tkSeq.Members += [resParam]
            # Inputs
            resParam.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            resParam.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            resParam.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
            resParam.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
            resParam.SummaryTracksLocation = locs["SummaryTracksLocation"]
            resParam.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            resParam.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
            resParam.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            resParam.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            resParam.TracksLocation = trackLocation
            # Options
            resParam.MinP = pCuts["MinP"]
            resParam.MaxP = pCuts["MaxP"]
            # resParam.NCKThetaBins = resBins
            # Enlarge CK theta resolution ranges for some track types
            if tktype == "Up" or tktype == "Seed":
                resParam.CKResHistoRange = (0.03, 0.015)

        if "TrackResolution" in checks:
            from Configurables import (
                Rich__Future__Rec__MC__Moni__TrackResolution as MCTkRes,
            )

            # Make instance
            tkRes = makeRichAlg(MCTkRes, "RiMCTkRes" + name, algprops, tktype)
            # Add to sequence
            tkSeq.Members += [tkRes]
            # Inputs
            tkRes.TracksLocation = trackLocation
            tkRes.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            tkRes.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            tkRes.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations

        if "MCOpticalPhotons" in checks:
            from Configurables import (
                Rich__Future__Rec__MC__Moni__OpticalPhotons as OptPhots,
            )

            # Make Instance
            mcOptPhots = makeRichAlg(
                OptPhots, "RiMCOpticalPhotons" + name, algprops, tktype
            )
            # Add to sequence
            tkSeq.Members += [mcOptPhots]
            # Inputs
            mcOptPhots.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mcOptPhots.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            mcOptPhots.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            mcOptPhots.SummaryTracksLocation = locs["SummaryTracksLocation"]
            mcOptPhots.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            mcOptPhots.TrackToMCParticlesRelations = (
                tkMCPRels.TrackToMCParticlesRelations
            )
            mcOptPhots.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            mcOptPhots.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            mcOptPhots.TracksLocation = trackLocation
            mcOptPhots.PhotonYieldLocation = locs["SignalPhotonYieldLocation"]
            # Options
            # mcOptPhots.MinP = pCuts["MinP"]
            # mcOptPhots.MaxP = pCuts["MaxP"]

        if "Time" in checks and (enable4DReco[0] or enable4DReco[1]):
            from Configurables import (
                Rich__Future__Rec__MC__Moni__SIMDPhotonTime as MCTime,
            )

            # Standard monitor
            mcTime = makeRichAlg(MCTime, "RiCKMCTime" + name, algprops, tktype)
            # Add to sequence
            tkSeq.Members += [mcTime]
            # Inputs
            mcTime.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            mcTime.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
            mcTime.SummaryTracksLocation = locs["SummaryTracksLocation"]
            mcTime.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            mcTime.TrackToMCParticlesRelations = tkMCPRels.TrackToMCParticlesRelations
            mcTime.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
            mcTime.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            mcTime.TracksLocation = trackLocation

    # return the full monitoring sequence
    return all
