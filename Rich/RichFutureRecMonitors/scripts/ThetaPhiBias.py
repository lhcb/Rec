#!/usr/bin/env python

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from array import array

import numpy as np
from GaudiPython import gbl
from ROOT import *
from ROOT import gROOT

gROOT.SetBatch(True)

hfile = TFile.Open("./RichRefIndexCalib.root")

fitter = gbl.Rich.Rec.CKResolutionFitter()

lhcbFont = 62
lhcbStatFontSize = 0.03
lhcbStatBoxWidth = 0.15
lhcbMarkerType = 20
lhcbMarkerSize = 0.8
gStyle.SetPadTickX(1)
gStyle.SetPadTickY(1)
gStyle.SetOptFit(101)
gStyle.SetOptStat(1110)
gStyle.SetStatBorderSize(1)
gStyle.SetStatFont(lhcbFont)
gStyle.SetStatFontSize(lhcbStatFontSize)
gStyle.SetStatW(lhcbStatBoxWidth)
gStyle.SetMarkerStyle(lhcbMarkerType)
gStyle.SetMarkerSize(lhcbMarkerSize)
nPhiBins = 16
nQuadrants = 4
phiInc = 2.0 * 3.1415 / nPhiBins


def quadString(q):
    if q == 0:
        return "Quadrant x>0 y>0"
    if q == 1:
        return "Quadrant x<0 y>0"
    if q == 2:
        return "Quadrant x>0 y<0"
    if q == 3:
        return "Quadrant x<0 y<0"
    return "UNDEFINED"


def padToQuad(p):
    if p == 0:
        return 1
    if p == 1:
        return 0
    if p == 2:
        return 3
    if p == 3:
        return 2


for rich in ["Rich1", "Rich2"]:
    canvas = TCanvas("CKFit" + rich, "CKFit" + rich, 1200, 900)

    # Open PDF
    pdfName = rich + "-CKThetaPhiPlots.pdf"
    canvas.Print(pdfName + "[")

    corrs = []

    phi_bias = {}
    phi_bias_err = {}
    phi_reso = {}
    phi_reso_err = {}
    phi_bins = {}
    phi_bins_err = {}

    for q in range(0, nQuadrants):
        phi_bias[q] = array("d")
        phi_bias_err[q] = array("d")
        phi_reso[q] = array("d")
        phi_reso_err[q] = array("d")
        phi_bins[q] = array("d")
        phi_bins_err[q] = array("d")
        for phi in range(0, nPhiBins):
            hpath = (
                "RICH/RiCKResLong/"
                + rich
                + "Gas/Quadrants/Q"
                + str(q)
                + "/PhiBins/Bin"
                + str(phi)
                + "/ckResAll"
            )
            print(hpath)

            h = hfile.Get(hpath)

            res = fitter.fit(
                h, gbl.Rich.Rich1Gas if rich == "Rich1" else gbl.Rich.Rich2Gas
            )

            h.Draw("e")
            res.overallFitFunc.SetLineColor(kBlue + 2)
            res.bkgFitFunc.SetLineColor(kRed + 3)
            res.overallFitFunc.Draw("SAME")
            res.bkgFitFunc.Draw("SAME")

            canvas.Print(pdfName)

            bias = 1000 * res.ckShift if res.fitOK else 0.0
            bias = float("{:.8f}".format(bias))
            phi_bias[q].append(bias)
            phi_bias_err[q].append(1000 * res.ckShiftErr if res.fitOK else 0.0)

            reso = 1000 * res.ckResolution if res.fitOK else 0.0
            reso = float("{:.8f}".format(reso))
            phi_reso[q].append(reso)
            phi_reso_err[q].append(1000 * res.ckResolutionErr if res.fitOK else 0.0)

            phi_bins[q].append((0.5 + phi) * phiInc)
            phi_bins_err[q].append(0)

        # corrs.append(phi_bias)

    # print(corrs)

    canvas.Clear()
    canvas.cd()
    canvas.Divide(2, 2)
    graph1 = []
    for p in range(0, nQuadrants):
        canvas.cd(p + 1)
        q = padToQuad(p)
        graph1.append(
            TGraphErrors(
                nPhiBins, phi_bins[q], phi_bias[q], phi_bins_err[q], phi_bias_err[q]
            )
        )
        graph1[p].SetTitle(rich + " CK Theta Bias V CK Phi | " + quadString(q))
        graph1[p].GetXaxis().SetTitle("CK Phi / rad")
        graph1[p].GetYaxis().SetTitle("CK Theta bias / mrad")
        graph1[p].Draw("ALP")
        gPad.Update()
    canvas.Print(pdfName)

    canvas.Clear()
    canvas.cd()
    canvas.Divide(2, 2)
    graph2 = []
    for p in range(0, nQuadrants):
        canvas.cd(p + 1)
        q = padToQuad(p)
        graph2.append(
            TGraphErrors(
                nPhiBins, phi_bins[q], phi_reso[q], phi_bins_err[q], phi_reso_err[q]
            )
        )
        graph2[p].SetTitle(rich + " CK Theta Resolution V CK Phi | " + quadString(q))
        graph2[p].GetXaxis().SetTitle("CK Phi / rad")
        graph2[p].GetYaxis().SetTitle("CK Theta Resolution / mrad")
        graph2[p].Draw("ALP")
    canvas.Print(pdfName)

    # Close PDF
    canvas.Print(pdfName + "]")
