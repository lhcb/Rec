/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPixelBackgrounds.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

namespace Rich::Future::Rec::Moni {

  /** @class PixelBackgrounds RichPixelBackgrounds.h
   *
   *  Monitors the RICH pixel backgrounds.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class PixelBackgrounds final
      : public LHCb::Algorithm::Consumer<void( const SIMDPixelSummaries&, //
                                               const SIMDPixelBackgrounds& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PixelBackgrounds( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "PixelBackgroundsLocation", SIMDPixelBackgroundsLocation::Default } } ) {
      // Default number of bins
      setProperty( "NBins1DHistos", 25 ).ignore();
      setProperty( "NBins2DHistos", 20 ).ignore();
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries&   pixels, //
                     const SIMDPixelBackgrounds& backgrounds ) const override {

      auto hb_pixBkg = h_pixBkg.buffer();

      for ( const auto&& [pix, bkg] : Ranges::ConstZip( pixels, backgrounds ) ) {
        const auto rich = pix.rich();
        if ( richIsActive( rich ) ) {
          // Loop over the scalar entries for this SIMD pixel
          for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
            // pixel background
            if ( pix.validMask()[i] ) { ++hb_pixBkg[rich][bkg[i]]; }
          }
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      // The max background value for each RICH for plots
      const DetectorArray<float> maxBkg = { 0.6, 0.3 };

      for ( const auto rich : activeDetectors() ) {
        ok &= initHist( h_pixBkg[rich], HID( "pixBkg", rich ), "Pixel Background Likelihood", //
                        -0.001, maxBkg[rich], nBins1D() );
      }

      return StatusCode{ ok };
    }

  private:
    // types

    /// Scalar type for SIMD data
    using FP = SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = SIMD::FP<Rich::SIMD::DefaultScalarFP>;

  private:
    // data

    /// Background histogram
    mutable Hist::DetArray<Hist::H1D<>> h_pixBkg = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PixelBackgrounds )

} // namespace Rich::Future::Rec::Moni
