/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Kernel
#include "LHCbMath/FastMaths.h"

// STD
#include <numeric>
#include <string>

namespace Rich::Future::Rec::Moni {

  /** @class MassHypoRings
   *
   *  Monitors the RICH mass hypothesis rings
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class MassHypoRings final : public LHCb::Algorithm::Consumer<void( const LHCb::RichTrackSegment::Vector&,  //
                                                                     const CherenkovAngles::Vector&,         //
                                                                     const SegmentPanelSpacePoints::Vector&, //
                                                                     const MassHypoRingsVector& ),           //
                                                               Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    MassHypoRings( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal },
                      KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector&  segments,     ///< Track Segments
                     const CherenkovAngles::Vector&         ckAngles,     ///< CK theta angles
                     const SegmentPanelSpacePoints::Vector& trHitPntsLoc, ///< segment hit points
                     const MassHypoRingsVector&             massRings ) const override {

      // local buffers
      auto hb_ringReuse           = h_ringReuse.buffer();
      auto hb_ringPointSegSep     = h_ringPointSegSep.buffer();
      auto hb_ringPointSegSepVPhi = h_ringPointSegSepVPhi.buffer();
      auto hb_sepVckt             = h_sepVckt.buffer();

      // loop over rings
      for ( auto&& [segment, ckT, tkLocPtn, ring] : Ranges::ConstZip( segments, ckAngles, trHitPntsLoc, massRings ) ) {

        // Which rad
        const auto rich = segment.rich();
        if ( richIsActive( rich ) ) {

          // ring reuse flagds
          ParticleArray<bool> reused{ { false } };

          // loop over IDs and deduce the reuse
          for ( const auto id : activeParticlesNoBT() ) {

            // Is the ring reused ?
            if ( ring.massAlias( id ) != id ) {
              // label this hypo and the one it is aliased to as being reused
              reused[id]                   = true;
              reused[ring.massAlias( id )] = true;
            }

            // saved vector of seperations
            std::vector<std::pair<double, double>> seps;
            seps.reserve( ring[id].size() );
            // accumlate sum of seperations for mean determination
            double sumSep = 0.0;

            // loop over points in the ring
            for ( const auto& p : ring[id] ) {
              // ring point position in local (corrected) coords
              const auto& lPosR = p.localPosition();
              // detector side
              const auto side = p.smartID().panel();
              // segment impact point on this side
              const auto& lPosT = tkLocPtn.point( side );
              // track point seperation
              const auto dx  = lPosR.x() - lPosT.x();
              const auto dy  = lPosR.y() - lPosT.y();
              const auto sep = std::sqrt( ( dx * dx ) + ( dy * dy ) );
              seps.emplace_back( LHCb::Math::fast_atan2( dy, dx ), sep );
              sumSep += sep;
            }

            if ( !seps.empty() ) {
              // compute average seperation for this ring
              const auto avSep = sumSep / seps.size();
              hb_sepVckt[rich][ckT[id]] += avSep;
              // fill deviations from average into histo
              for ( const auto& [phi, sep] : seps ) {
                const auto sepDiff = sep - avSep;
                ++hb_ringPointSegSep[rich][id][sepDiff];
                hb_ringPointSegSepVPhi[rich][id][phi] += sepDiff;
              }
            }
          }

          // fill reuse histos
          for ( const auto id : activeParticlesNoBT() ) {
            // only fill if ring not empty (i.e. above threshold)
            if ( !ring[id].empty() ) { hb_ringReuse[rich][id] += ( reused[id] ? 100.0 : 0.0 ); }
          }
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // loop over active radiators
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );
        // book ring reuse histo
        ok &= initHist( h_ringReuse[rich], HID( "ringReuse", rad ),                     //
                        "Ring Reuse (%) V Mass Hypothesis",                             //
                        -0.5, Rich::NRealParticleTypes - 0.5, Rich::NRealParticleTypes, //
                        "Ring Reuse (%)", "Mass Hypothesis",                            //
                        BinLabels{ "El", "Mu", "Pi", "Ka", "Pr", "De" } );
        // Sep versus CKT
        ok &= initHist( h_sepVckt[rich], HID( "sepVckt", rad ), //
                        "Local Seperation V CK Theta",          //
                        m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D() );
        // loop over active particle types
        for ( const auto id : activeParticlesNoBT() ) {
          // point/segment seperation derivation from average
          const Rich::RadiatorArray<double> sepHistR{ 100, 20, 50 };
          ok &= initHist( h_ringPointSegSep[rich][id], HID( "pntSegSepDev", rad, id ), //
                          "Ring - Segment local point seperation deviation",           //
                          -sepHistR[rad], sepHistR[rad], nBins1D(),                    //
                          "Rich-Segment local point seperation / mm" );
          // point/segment seperation derivation from average
          ok &= initHist( h_ringPointSegSepVPhi[rich][id], HID( "pntSegSepDevVPhi", rad, id ), //
                          "Ring - Segment local point seperation deviation V Ring Azimuth",    //
                          -M_PI, M_PI, nBins1D(),                                              //
                          "Ring azimuthal angle / rad",                                        //
                          "Rich-Segment local point seperation / mm" );
        }
      }
      return StatusCode{ ok };
    }

  private:
    // properties

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.010f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.056f, 0.033f } };

  private:
    // histos

    /// Ring Reuse histograms
    mutable Hist::DetArray<Hist::P1D<>> h_ringReuse = { {} };
    /// Deviations in point - segment ring seperation
    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_ringPointSegSep     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_ringPointSegSepVPhi = { {} };
    /// Correlation between CHerenkov theta and local segment-point seperation
    mutable Hist::DetArray<Hist::P1D<>> h_sepVckt = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( MassHypoRings )

} // namespace Rich::Future::Rec::Moni
