/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GAUDI_VERSION.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichDAQDefinitions.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// AIDA
#include "AIDA/IAxis.h"

// boost
#include "boost/container/small_vector.hpp"

// STD
#include <algorithm>
#include <cstdint>
#include <vector>

namespace Rich::Future::Rec::Moni {

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors.
   *
   *  @author Chris Jones
   *  @date   2016-12-06
   */

  class DetectorHits final : public LHCb::Algorithm::Consumer<void( const SIMDPixelSummaries& ), //
                                                              Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins2DHistos", 50 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries& pixels ) const override {

      // local buffers
      auto hb_nTotalPixsPerPD      = h_nTotalPixsPerPD.buffer();
      auto hb_nTotalPixsPerPDInner = h_nTotalPixsPerPDInner.buffer();
      auto hb_nTotalPixsPerPDOuter = h_nTotalPixsPerPDOuter.buffer();
      auto hb_nTotalPixs           = h_nTotalPixs.buffer();
      auto hb_pixXYGlo             = h_pixXYGlo.buffer();
      auto hb_pixXGlo              = h_pixXGlo.buffer();
      auto hb_pixYGlo              = h_pixYGlo.buffer();
      auto hb_pixXYLoc             = h_pixXYLoc.buffer();
      auto hb_pixXLoc              = h_pixXLoc.buffer();
      auto hb_pixYLoc              = h_pixYLoc.buffer();
      auto hb_pdXYOcc              = h_pdXYOcc.buffer();
      auto hb_pdXOcc               = h_pdXOcc.buffer();
      auto hb_pdYOcc               = h_pdYOcc.buffer();
      auto hb_hitDensityXY         = h_hitDensityXY.buffer();
      auto hb_hitDensityX          = h_hitDensityX.buffer();
      auto hb_hitDensityY          = h_hitDensityY.buffer();
      auto hb_nActivePDs           = h_nActivePDs.buffer();

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {

        // ID for last seen PD
        LHCb::RichSmartID lastPD;
        // Inner/outer region for last PD
        bool lastPDIsInner = false;
        // effective area for pixels in last PD
        double effArea = 1.0;

        // count hits in current PD
        std::size_t curPDhits{ 0 };

        // cache iX and iY indices for hits in current PD
        boost::container::small_vector<std::pair<int, int>, LHCb::RichSmartID::MaPMT::TotalPixels> hitiXiY;

        // count for occupancy in each PD
        using VD = std::vector<double>;
        std::vector<VD> pdOccXY{ nBins2D(), VD( nBins2D(), 0.0 ) };
        std::vector<VD> hitsPerMM2{ nBins2D(), VD( nBins2D(), 0.0 ) };

        // Count active PDs in each RICH
        std::uint32_t activePDs = 0;

        // functor to fill data for a given PD
        auto newPD = [&]() {
          // If last PD ID is valid fill plots etc. for the last PD
          if ( lastPD.isValid() ) {
            // Fill average HPD occ plot
            ++hb_nTotalPixsPerPD[rich][curPDhits];
            ++( lastPDIsInner ? hb_nTotalPixsPerPDInner[rich] : hb_nTotalPixsPerPDOuter[rich] )[curPDhits];
            // PD occ
            for ( const auto& [iX, iY] : hitiXiY ) {
              pdOccXY[iX][iY]    = ( 100.0 * curPDhits ) / (double)LHCb::RichSmartID::MaPMT::TotalPixels;
              hitsPerMM2[iX][iY] = curPDhits / ( (double)LHCb::RichSmartID::MaPMT::TotalPixels * effArea );
            }
          }
          // reset caches
          hitiXiY.clear();
          curPDhits = 0;
        };

        // Fill RICH hits plots
        const auto nHits = pixels.nHitsScalar( rich );
        if ( nHits > 0 ) { ++hb_nTotalPixs[rich][nHits]; }

        // Loop over pixels for this RICH
        for ( const auto& pix : pixels.range( rich ) ) {
          // scalar loop
          for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
            if ( !pix.validMask()[i] ) { continue; }

            // Hit data
            const auto id   = pix.smartID()[i];
            const auto pdID = id.pdID();
            const auto side = pdID.panel();

            // If new PD fill plots and caches
            if ( lastPD != pdID ) {
              newPD();
              lastPD        = pdID;
              lastPDIsInner = pix.isInnerRegion()[i];
              effArea       = pix.effArea()[i];
              // count active PDs
              ++activePDs;
            }

            // Count hits in current PD
            ++curPDhits;

            // get global/local coords
            const auto gPos = pix.gloPos( i );
            const auto lPos = pix.locPos( i );

            // fill plots
            ++hb_pixXYGlo[rich][side][{ gPos.X(), gPos.Y() }];
            ++hb_pixXGlo[rich][side][gPos.X()];
            ++hb_pixYGlo[rich][side][gPos.Y()];
            ++hb_pixXYLoc[rich][{ lPos.X(), lPos.Y() }];
            ++hb_pixXLoc[rich][lPos.X()];
            ++hb_pixYLoc[rich][lPos.Y()];

            // cache occupancy data
            const auto& xaxis = h_pdXYOcc[rich].axis<0>();
            const auto& yaxis = h_pdXYOcc[rich].axis<1>();
            const auto  iX    = std::min( xaxis.index( lPos.X() ), xaxis.nBins - 1u );
            const auto  iY    = std::min( yaxis.index( lPos.Y() ), yaxis.nBins - 1u );
            hitiXiY.emplace_back( iX, iY );

          } // scalar loop
        }   // SIMD pixel loop

        // call once for last hit
        newPD();

        // Fill occupancies
        const auto& xaxis = h_pdXYOcc[rich].axis<0>();
        const auto& yaxis = h_pdXYOcc[rich].axis<1>();
        for ( auto iX = 0u; iX < xaxis.nBins; ++iX ) {
          for ( auto iY = 0u; iY < yaxis.nBins; ++iY ) {
            const auto X = xaxis.minValue() + ( ( 0.5 + iX ) * ( xaxis.maxValue() - xaxis.minValue() ) / xaxis.nBins );
            const auto Y = yaxis.minValue() + ( ( 0.5 + iY ) * ( yaxis.maxValue() - yaxis.minValue() ) / yaxis.nBins );
            const auto occ = pdOccXY[iX][iY];
            hb_pdXYOcc[rich][{ X, Y }] += occ;
            hb_pdXOcc[rich][X] += occ;
            hb_pdYOcc[rich][Y] += occ;
            const auto hitPerMM2 = hitsPerMM2[iX][iY];
            hb_hitDensityXY[rich][{ X, Y }] += hitPerMM2;
            hb_hitDensityX[rich][X] += hitPerMM2;
            hb_hitDensityY[rich][Y] += hitPerMM2;
          }
        }

        // Fill active PD plots
        ++hb_nActivePDs[rich][activePDs];

      } // RICHes
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      const DetectorArray<double> panelXsizes{ { 650, 800 } };
      const DetectorArray<double> panelYsizes{ { 680, 750 } };
      const DetectorArray<double> gloXsizes{ { 750, 4100 } };
      const DetectorArray<double> gloYsizes{ { 1600, 800 } };
      const DetectorArray<double> pSF{ { 0.6, 0.5 } }; // reduced (x,y) size in global projection

      for ( const auto rich : activeDetectors() ) {
        ok &= initHist( h_nTotalPixsPerPD[rich],                      //
                        Rich::HistogramID( "nTotalPixsPerPD", rich ), //
                        "Average overall PD occupancy (nHits>0)",     //
                        0.5, 64.5, 64 );
        ok &= initHist( h_nTotalPixsPerPDInner[rich],                      //
                        Rich::HistogramID( "nTotalPixsPerInnerPD", rich ), //
                        "Average overall Inner PD occupancy (nHits>0)",    //
                        0.5, 64.5, 64 );
        ok &= initHist( h_nTotalPixsPerPDOuter[rich],                      //
                        Rich::HistogramID( "nTotalPixsPerOuterPD", rich ), //
                        "Average overall Outer PD occupancy (nHits>0)",    //
                        0.5, 64.5, 64 );
        ok &= initHist( h_nTotalPixs[rich],                      //
                        Rich::HistogramID( "nTotalPixs", rich ), //
                        "Overall occupancy (nHits>0)",           //
                        0.5, m_maxPixels[rich] + 0.5, m_maxPixels[rich] / 100 );
        ok &= initHist( h_nActivePDs[rich],                      //
                        Rich::HistogramID( "nActivePDs", rich ), //
                        "# Active PDs (nHits>0)", -0.5, 2000.5, 2001 );
        // hit points (global)
        for ( const auto side : Rich::sides() ) {
          const auto minX = ( rich == Rich::Rich1   ? -gloXsizes[rich]
                              : side == Rich::right ? -gloXsizes[rich] //
                                                    : gloXsizes[rich] - ( pSF[rich] * panelXsizes[rich] ) );
          const auto maxX = ( rich == Rich::Rich1   ? gloXsizes[rich]
                              : side == Rich::right ? ( pSF[rich] * panelXsizes[rich] ) - gloXsizes[rich] //
                                                    : gloXsizes[rich] );
          const auto minY = ( rich == Rich::Rich2 ? -gloYsizes[rich]
                              : side == Rich::top ? gloYsizes[rich] - ( pSF[rich] * panelYsizes[rich] ) //
                                                  : -gloYsizes[rich] );
          const auto maxY = ( rich == Rich::Rich2 ? gloYsizes[rich]
                              : side == Rich::top ? gloYsizes[rich] //
                                                  : -gloYsizes[rich] + ( pSF[rich] * panelYsizes[rich] ) );
          ok &= initHist( h_pixXYGlo[rich][side],        //
                          HID( "pixXYGlo", rich, side ), //
                          "Global (X,Y) hits",           //
                          minX, maxX, nBins2D(),         //
                          minY, maxY, nBins2D(),         //
                          "Global X / mm", "Global Y / mm" );
          ok &= initHist( h_pixXGlo[rich][side],        //
                          HID( "pixXGlo", rich, side ), //
                          "Global X hits",              //
                          minX, maxX, nBins2D(),        //
                          "Global X / mm" );
          ok &= initHist( h_pixYGlo[rich][side],        //
                          HID( "pixYGlo", rich, side ), //
                          "Global Y hits",              //
                          minY, maxY, nBins2D(),        //
                          "Global Y / mm" );
        }
        // hit points (local)
        ok &= initHist( h_pixXYLoc[rich],                                 //
                        HID( "pixXYLoc", rich ),                          //
                        "Local (X,Y) hits",                               //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                        "Local X / mm", "Local Y / mm" );
        ok &= initHist( h_pixXLoc[rich],                                  //
                        HID( "pixXLoc", rich ),                           //
                        "Local X hits",                                   //
                        -panelXsizes[rich], panelXsizes[rich], nBins1D(), //
                        "Local X / mm" );
        ok &= initHist( h_pixYLoc[rich],                                  //
                        HID( "pixYLoc", rich ),                           //
                        "Local Y hits",                                   //
                        -panelYsizes[rich], panelYsizes[rich], nBins1D(), //
                        "Local Y / mm" );
        ok &= initHist( h_pdXYOcc[rich],                                  //
                        HID( "pdOccXYLoc", rich ),                        //
                        "PD Average Occupancy (X,Y)",                     //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                        "Local X / mm", "Local Y / mm", "Occupancy / %" );
        ok &= initHist( h_pdXOcc[rich],                                   //
                        HID( "pdOccXLoc", rich ),                         //
                        "PD Average Occupancy (X)",                       //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        "Local X / mm", "Occupancy / %" );
        ok &= initHist( h_pdYOcc[rich],                                   //
                        HID( "pdOccYLoc", rich ),                         //
                        "PD Average Occupancy (Y)",                       //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                        "Local Y / mm", "Occupancy / %" );

        ok &= initHist( h_hitDensityXY[rich],                             //
                        HID( "hitDensityXY", rich ),                      //
                        "<#hits/mm^2> (X,Y)",                             //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(), //
                        "Local X / mm", "Local Y / mm", "hits per mm^2" );
        ok &= initHist( h_hitDensityX[rich],                              //
                        HID( "hitDensityX", rich ),                       //
                        "<#hits/mm^2> (X)",                               //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        "Local X / mm", "hits per mm^2" );
        ok &= initHist( h_hitDensityY[rich],                              //
                        HID( "hitDensityY", rich ),                       //
                        "<#hits/mm^2> (Y)",                               //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(), //
                        "Local Y / mm", "hits per mm^2" );
      }

      return StatusCode{ ok };
    }

  private:
    /// Maximum pixels
    Gaudi::Property<DetectorArray<unsigned int>> m_maxPixels{ this, "MaxPixels", { 30000u, 20000u } };

  private:
    // cached histograms

    /// Pixels per PD histograms
    mutable Hist::DetArray<Hist::H1D<>> h_nTotalPixsPerPD      = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_nTotalPixsPerPDInner = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_nTotalPixsPerPDOuter = { {} };
    /// Pixels per detector histograms
    mutable Hist::DetArray<Hist::H1D<>> h_nTotalPixs = { {} };
    /// Number active PDs histograms
    mutable Hist::DetArray<Hist::H1D<>> h_nActivePDs = { {} };

    mutable Hist::DetArray<Hist::PanelArray<Hist::H2D<>>> h_pixXYGlo = { {} };
    mutable Hist::DetArray<Hist::PanelArray<Hist::H1D<>>> h_pixXGlo  = { {} };
    mutable Hist::DetArray<Hist::PanelArray<Hist::H1D<>>> h_pixYGlo  = { {} };

    mutable Hist::DetArray<Hist::H2D<>> h_pixXYLoc = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_pixXLoc  = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_pixYLoc  = { {} };

    mutable Hist::DetArray<Hist::P2D<>> h_pdXYOcc = { {} };
    mutable Hist::DetArray<Hist::P1D<>> h_pdXOcc  = { {} };
    mutable Hist::DetArray<Hist::P1D<>> h_pdYOcc  = { {} };

    mutable Hist::DetArray<Hist::P2D<>> h_hitDensityXY = { {} };
    mutable Hist::DetArray<Hist::P1D<>> h_hitDensityX  = { {} };
    mutable Hist::DetArray<Hist::P1D<>> h_hitDensityY  = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::Moni
