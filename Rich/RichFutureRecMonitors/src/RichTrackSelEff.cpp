/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event model
#include "Event/RichPID.h"
#include "Event/Track.h"

namespace Rich::Future::Rec::Moni {

  /** @class TrackSelEff RichTrackSelEff.h
   *
   *  Monitors the efficiency of the RICH track selection and processing.
   *  I.e. the efficiency for which a RichPID object is produced.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class TrackSelEff final : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, //
                                                                   const LHCb::RichPIDs& ),
                                                             Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackSelEff( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichPIDsLocation", LHCb::RichPIDLocation::Default } } ) {
      // reset defaults in base class
      setProperty( "NBins1DHistos", 50 ).ignore();
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range& tracks, //
                     const LHCb::RichPIDs&     pids ) const override {

      // Count selected tracks
      unsigned int nSelTracks( 0 );

      // local buffers
      auto hb_effVP         = h_effVP.buffer();
      auto hb_effVPt        = h_effVPt.buffer();
      auto hb_effVChi2PDOF  = h_effVChi2PDOF.buffer();
      auto hb_effVGhostProb = h_effVGhostProb.buffer();
      auto hb_effVCloneDist = h_effVCloneDist.buffer();

      // loop over input tracks
      for ( const auto* track : tracks ) {
        // Does this track have a PID result associated to it ?
        const bool sel = std::any_of( pids.begin(), pids.end(), //
                                      [&track]( const auto pid ) { return pid->track() == track; } );

        // count selected tracks
        if ( sel ) { ++nSelTracks; }

        // clone dist
        const double cloneDist = track->info( LHCb::Track::AdditionalInfo::CloneDist, 5.5e3 );

        // Efficiencies plots
        const double richEff = ( sel ? 100.0 : 0.0 );
        hb_effVP[track->p()] += richEff;
        hb_effVPt[track->pt()] += richEff;
        hb_effVChi2PDOF[track->chi2PerDoF()] += richEff;
        hb_effVGhostProb[track->ghostProbability()] += richEff;
        hb_effVCloneDist[cloneDist] += richEff;
      }

      // fill event plots
      ++h_nTracks[tracks.size()];
      ++h_nRichTracks[nSelTracks];
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {

      bool ok = true;

      using namespace Gaudi::Units;

      // global quantities
      ok &= initHist( h_nTracks, HID( "nTracks" ), "# Tracks / Event", -0.5, 200.5, 201 );
      ok &= initHist( h_nRichTracks, HID( "nRichTracks" ), "# Rich Tracks / Event", -0.5, 200.5, 201 );

      // track variable plots
      const std::string tkClass = "All/";
      ok &= initHist( h_effVP, HID( tkClass + "effVP" ), "RICH Track Sel. Eff. V P", //
                      1.00 * GeV, 100.0 * GeV, nBins1D() );
      ok &= initHist( h_effVPt, HID( tkClass + "effVPt" ), "RICH Track Sel. Eff. V Pt", //
                      0.10 * GeV, 8.0 * GeV, nBins1D() );
      ok &= initHist( h_effVChi2PDOF, HID( tkClass + "effVChi2PDOF" ), //
                      "RICH Track Sel. Eff. V Chi^2 / D.O.F.", 0, 3, nBins1D() );
      ok &= initHist( h_effVGhostProb, HID( tkClass + "effVGhostProb" ), //
                      "RICH Track Sel. Eff. V Ghost Probability", 0.0, 0.4, nBins1D() );
      ok &= initHist( h_effVCloneDist, HID( tkClass + "effVCloneDist" ), //
                      "RICH Track Sel. Eff. V Clone Distance", 0.0, 6e3, nBins1D() );

      // return
      return StatusCode{ ok };
    }

  private:
    // Histos
    mutable Hist::H1D<> h_nTracks{};
    mutable Hist::H1D<> h_nRichTracks{};
    mutable Hist::P1D<> h_effVP{};
    mutable Hist::P1D<> h_effVPt{};
    mutable Hist::P1D<> h_effVChi2PDOF{};
    mutable Hist::P1D<> h_effVGhostProb{};
    mutable Hist::P1D<> h_effVCloneDist{};
  };

  DECLARE_COMPONENT( TrackSelEff )

} // namespace Rich::Future::Rec::Moni
