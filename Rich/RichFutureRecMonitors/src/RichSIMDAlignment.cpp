/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "yaml-cpp/yaml.h"

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi functional
#include "LHCbAlgs/Consumer.h"

// event model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// rich utils
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// track selector
#include "TrackInterfaces/ITrackSelector.h"

// STD
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <map>
#include <mutex>
#include <random>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

namespace Rich::Future::Rec::Moni {

  using namespace Gaudi::Units;

  /** @class SIMDAlignment
   *
   *  For monitoring the RICH1, RICH2 mirror alignment, fills histograms with
   *  delta-theta vs. phi of the reconstructed CHerenkov photons for a
   *  special set of the primary / secondary mirror combinations.
   *
   *  Follows RichSIMDPhotonCherenkovAngles.cpp as a prototype.
   *
   *  @author Anatoly Solomin anatoly.solomin@cern.ch
   *  @date   2020-03-17
   */

  class SIMDAlignment final : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&,                 //
                                                                     const Summary::Track::Vector&,             //
                                                                     const Relations::PhotonToParents::Vector&, //
                                                                     const LHCb::RichTrackSegment::Vector&,     //
                                                                     const CherenkovAngles::Vector&,            //
                                                                     const SIMDCherenkovPhoton::Vector&,        //
                                                                     const SIMDMirrorData::Vector& ),
                                                               Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDAlignment( const std::string& name, ISvcLocator* pSvcLocator )                                    //
        : Consumer( name, pSvcLocator,                                                                    //
                    { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },                         //
                      KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },                 //
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default }, //
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },       //
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },             //
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },        //
                      KeyValue{ "PhotonMirrorDataLocation", SIMDMirrorDataLocation::Default } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

    /// Finalize
    StatusCode finalize() override {

      if ( task_MapConstruct ) {
        YAML::Node node;
        for ( const auto& kv1 : bin_trackCount_comb_fullnessPhotCount ) {
          node[kv1.first]["tracks"] = kv1.second.first;
          for ( const auto& kv2 : kv1.second.second ) {
            node[kv1.first]["combs"][kv2.first]["full"]    = kv2.second[0];
            node[kv1.first]["combs"][kv2.first]["photons"] = kv2.second[1];
          }
        }
        YAML::Emitter emitter;
        emitter << node;
        std::ofstream ofst( m_variant + "_MapConstruct" + "_bin_trackCount_comb_fullnessPhotCount.yml" );
        ofst << emitter.c_str() << std::endl;
        ofst.close();
      }

      if ( task_Map ) {
        // it is more visually apparent
        // when the map is ordered according to the eventNumber
        // and stored in this manner
        std::map<int, std::string> eventNumber_bin;
        for ( const auto& kv : bin_eventNumber ) { eventNumber_bin[kv.second] = kv.first; }

        YAML::Node node;
        for ( const auto& kv : eventNumber_bin ) {
          // note: we store eventNumber as key,
          // while bin-name string as value
          node[kv.first] = kv.second;
        }
        YAML::Emitter emitter;
        emitter << node;
        // info() << emitter.c_str() << endmsg;
        std::ofstream ofst( m_variant + "_Map" + "_map_events_bin.yml" );
        ofst << emitter.c_str() << std::endl;
        ofst.close();

        FILE* events_all_and_included_file;
        events_all_and_included_file =
            std::fopen( ( m_variant + "_Map" + "_events_all_and_included.py" ).c_str(), "w" );
        std::fprintf( events_all_and_included_file, "allEvents      = %10s\n",
                      ( std::to_string( currentEventNumber ) ).c_str() );
        std::fprintf( events_all_and_included_file, "includedEvents = %10s\n",
                      ( std::to_string( includedEventsAmount ) ).c_str() );
        std::fclose( events_all_and_included_file );
      }

      if ( task_Map ) {
        YAML::Node node;
        for ( const auto& kv : comb_status ) {
          node[kv.first]["full"]    = kv.second[0];
          node[kv.first]["photons"] = kv.second[1];
        }
        YAML::Emitter emitter;
        emitter << node;
        // info() << emitter.c_str() << endmsg;
        std::ofstream ofst( m_variant + "_Map" + "_comb_status_full_photons.yml" );
        ofst << emitter.c_str() << std::endl;
        ofst.close();
      }

      if ( task_MapPhi ) {
        // it is more visually apparent
        // when the map is ordered according to the eventNumber
        // and stored in this manner
        std::map<int, std::string> eventNumberPhi_bin;
        for ( const auto& kv : bin_eventNumberPhi ) { eventNumberPhi_bin[kv.second] = kv.first; }

        YAML::Node node1;
        for ( const auto& kv : eventNumberPhi_bin ) {
          // note: we store eventNumber as key,
          // while bin-name string as value
          node1[kv.first] = kv.second;
        }
        YAML::Emitter emitter1;
        emitter1 << node1;
        // info() << emitter1.c_str() << endmsg;
        std::ofstream ofst_map( m_variant + "_MapPhi" + "_map_events_bin.yml" );
        ofst_map << emitter1.c_str() << std::endl;
        ofst_map.close();

        YAML::Node node2;
        for ( auto& v : binEventNumberPhi ) {
          // ordinal numbers of elements will be used as the keys,
          // while the values are the so-called terminating event numbers
          node2.push_back( v );
        }
        YAML::Emitter emitter2;
        emitter2 << node2;
        // info() << emitter2.c_str() << endmsg;
        std::ofstream ofst_array( m_variant + "_MapPhi" + "_array_bin_events.yml" );
        ofst_array << emitter2.c_str() << std::endl;
        ofst_array.close();

        FILE* events_all_and_included_file;
        events_all_and_included_file =
            std::fopen( ( m_variant + "_MapPhi" + "_events_all_and_included.py" ).c_str(), "w" );
        std::fprintf( events_all_and_included_file, "allEvents phi      = %10s\n",
                      ( std::to_string( currentEventNumber ) ).c_str() );
        std::fprintf( events_all_and_included_file, "includedEvents phi = %10s\n",
                      ( std::to_string( includedEventsAmount ) ).c_str() );
        std::fclose( events_all_and_included_file );

        YAML::Node node3;
        for ( const auto& kv : comb_status_phi ) {
          node3[kv.first]["full"]               = kv.second[0];
          node3[kv.first]["minQuantileContent"] = kv.second[1];
        }
        YAML::Emitter emitter3;
        emitter3 << node3;
        // info() << emitter3.c_str() << endmsg;
        std::ofstream ofst_quantilles( m_variant + "_MapPhi" + "_comb_status_full_photons.yml" );
        ofst_quantilles << emitter3.c_str() << std::endl;
        ofst_quantilles.close();
      }

      if ( task_MapUse ) {
        FILE* events_all_and_selected_file;
        events_all_and_selected_file =
            std::fopen( ( m_variant + "_MapUse" + "_events_all_and_selected.py" ).c_str(), "w" );
        std::fprintf( events_all_and_selected_file, "allEvents      = %10s\n",
                      ( std::to_string( currentEventNumber ) ).c_str() );
        std::fprintf( events_all_and_selected_file, "selectedEvents = %10s\n",
                      ( std::to_string( selectedEventsAmount ) ).c_str() );
        std::fclose( events_all_and_selected_file );
      }

      if ( task_MapPhiUse ) {
        FILE* events_all_and_selected_file;
        events_all_and_selected_file =
            std::fopen( ( m_variant + "_MapPhiUse" + "_events_all_and_selected.py" ).c_str(), "w" );
        std::fprintf( events_all_and_selected_file, "allEvents phi      = %10s\n",
                      ( std::to_string( currentEventNumber ) ).c_str() );
        std::fprintf( events_all_and_selected_file, "selectedEvents phi = %10s\n",
                      ( std::to_string( selectedEventsAmount ) ).c_str() );
        std::fclose( events_all_and_selected_file );
      }

      if ( task_AllFillCount ) {
        std::array<std::vector<std::pair<std::array<std::string, 2>, double>>, 2>
            sides_prisec_allCombsPopulationsSorted;

        for ( int side : { 0, 1 } ) {
          for ( auto& kv : sides_prisec_allCombsPopulations[side] ) {
            std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
            sides_prisec_allCombsPopulationsSorted[side].push_back( std::make_pair( prisec, kv.second ) );
          }
          std::sort( sides_prisec_allCombsPopulationsSorted[side].begin(),
                     sides_prisec_allCombsPopulationsSorted[side].end(),
                     []( const auto& x, const auto& y ) { return x.second > y.second; } );

          FILE* fout;
          fout = std::fopen(
              ( m_variant + "_AllFillCount" + "_all_comb_photons_side" + std::to_string( side ) + ".py" ).c_str(),
              "w" );
          std::fprintf( fout, "[\n" );
          for ( auto& kv : sides_prisec_allCombsPopulationsSorted[side] ) {
            std::fprintf( fout, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(), kv.first[1].c_str(), kv.second );
          }
          std::fprintf( fout, "]\n" );
          std::fclose( fout );
        }
      }

      if ( task_CheckRestFill ) {
        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_restCombsPopulationsSorted;

        for ( auto& kv : prisec_restCombsPopulations ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_restCombsPopulationsSorted.push_back( std::make_pair( prisec, kv.second ) );
        }

        std::sort( prisec_restCombsPopulationsSorted.begin(), prisec_restCombsPopulationsSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );

        FILE* fout;
        fout = std::fopen( ( m_variant + "_CheckRestFill" + "_rest_comb_photons.py" ).c_str(), "w" );
        std::fprintf( fout, "[\n" );
        for ( auto& kv : prisec_restCombsPopulationsSorted ) {
          std::fprintf( fout, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(), kv.first[1].c_str(), kv.second );
        }
        std::fprintf( fout, "]\n" );
        std::fclose( fout );

        info() << "Total number of events    = " << totalEventsCount << endmsg;
        info() << "Number of selected events = " << selectedEventsCount << endmsg;
      }

      if ( task_SkipFilled ) {
        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_TotalPopulationsSorted;
        for ( auto& kv : prisec_totalPopulation ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_TotalPopulationsSorted.push_back( std::make_pair( prisec, kv.second ) );
        }
        std::sort( prisec_TotalPopulationsSorted.begin(), prisec_TotalPopulationsSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );
        FILE* prisec_TotalPopulationsSortedFile;
        prisec_TotalPopulationsSortedFile =
            std::fopen( ( m_variant + "_SkipFilled " + "_comb_photons.py" ).c_str(), "w" );
        // std::fprintf(prisec_TotalPopulationsSortedFile, "prisec_TotalPopulationsSorted\n");
        std::fprintf( prisec_TotalPopulationsSortedFile, "[\n" );
        for ( auto& kv : prisec_TotalPopulationsSorted ) {
          std::fprintf( prisec_TotalPopulationsSortedFile, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(),
                        kv.first[1].c_str(), kv.second );
        }
        std::fprintf( prisec_TotalPopulationsSortedFile, "]\n" );
        std::fclose( prisec_TotalPopulationsSortedFile );

        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_PopulationsWhenFullSorted;
        for ( auto& kv : prisec_populationWhenFull ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_PopulationsWhenFullSorted.push_back( std::make_pair( prisec, kv.second ) );
        }
        std::sort( prisec_PopulationsWhenFullSorted.begin(), prisec_PopulationsWhenFullSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );
        FILE* prisec_PopulationsWhenFullSortedFile;
        prisec_PopulationsWhenFullSortedFile =
            std::fopen( ( m_variant + "_SkipFilled" + "_comb_photons_when_full.py" ).c_str(), "w" );
        // std::fprintf(prisec_PopulationsWhenFullSortedFile, "prisec_PopulationsWhenFullSorted\n");
        std::fprintf( prisec_PopulationsWhenFullSortedFile, "[\n" );
        for ( auto& kv : prisec_PopulationsWhenFullSorted ) {
          std::fprintf( prisec_PopulationsWhenFullSortedFile, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(),
                        kv.first[1].c_str(), kv.second );
        }
        std::fprintf( prisec_PopulationsWhenFullSortedFile, "]\n" );
        std::fclose( prisec_PopulationsWhenFullSortedFile );

        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_amountOfContributingEventsSorted;
        for ( auto& kv : prisec_amountOfContributingEvents ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_amountOfContributingEventsSorted.push_back( std::make_pair( prisec, kv.second ) );
        }
        std::sort( prisec_amountOfContributingEventsSorted.begin(), prisec_amountOfContributingEventsSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );
        FILE* prisec_amountOfContributingEventsSortedFile;
        prisec_amountOfContributingEventsSortedFile =
            std::fopen( ( m_variant + "_SkipFilled" + "comb_contributing_events.py" ).c_str(), "w" );
        // std::fprintf(prisec_amountOfContributingEventsSortedFile, "prisec_amountOfContributingEventsSorted\n");
        std::fprintf( prisec_amountOfContributingEventsSortedFile, "[\n" );
        for ( auto& kv : prisec_amountOfContributingEventsSorted ) {
          std::fprintf( prisec_amountOfContributingEventsSortedFile, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(),
                        kv.first[1].c_str(), kv.second );
        }
        std::fprintf( prisec_amountOfContributingEventsSortedFile, "]\n" );
        std::fclose( prisec_amountOfContributingEventsSortedFile );

        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_amountOfContributingEventsUntilFullSorted;
        for ( auto& kv : prisec_amountOfContributingEventsUntilFull ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_amountOfContributingEventsUntilFullSorted.push_back( std::make_pair( prisec, kv.second ) );
        }
        std::sort( prisec_amountOfContributingEventsUntilFullSorted.begin(),
                   prisec_amountOfContributingEventsUntilFullSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );
        FILE* prisec_amountOfContributingEventsUntilFullSortedFile;
        prisec_amountOfContributingEventsUntilFullSortedFile =
            std::fopen( ( m_variant + "_SkipFilled" + "comb_contributing_events_when_full.py" ).c_str(), "w" );
        // std::fprintf(prisec_amountOfContributingEventsUntilFullSortedFile,
        // "prisec_amountOfContributingEventsUntilFullSorted\n");
        std::fprintf( prisec_amountOfContributingEventsUntilFullSortedFile, "[\n" );
        for ( auto& kv : prisec_amountOfContributingEventsUntilFullSorted ) {
          std::fprintf( prisec_amountOfContributingEventsUntilFullSortedFile, "('%3s', '%3s', %12.1f),\n",
                        kv.first[0].c_str(), kv.first[1].c_str(), kv.second );
        }
        std::fprintf( prisec_amountOfContributingEventsUntilFullSortedFile, "]\n" );
        std::fclose( prisec_amountOfContributingEventsUntilFullSortedFile );

        std::vector<std::pair<std::array<std::string, 2>, double>> prisec_numberOfEventsUntilFullSorted;
        for ( auto& kv : prisec_numberOfEventsUntilFull ) {
          std::array<std::string, 2> prisec{ kv.first[0].c_str(), kv.first[1].c_str() };
          prisec_numberOfEventsUntilFullSorted.push_back( std::make_pair( prisec, kv.second ) );
        }
        std::sort( prisec_numberOfEventsUntilFullSorted.begin(), prisec_numberOfEventsUntilFullSorted.end(),
                   []( const auto& x, const auto& y ) { return x.second > y.second; } );
        FILE* prisec_numberOfEventsUntilFullSortedFile;
        prisec_numberOfEventsUntilFullSortedFile =
            std::fopen( ( m_variant + "_SkipFilled" + "comb_events_when_full.py" ).c_str(), "w" );
        // std::fprintf(prisec_numberOfEventsUntilFullSortedFile, "prisec_numberOfEventsUntilFullSorted\n");
        std::fprintf( prisec_numberOfEventsUntilFullSortedFile, "[\n" );
        for ( auto& kv : prisec_numberOfEventsUntilFullSorted ) {
          std::fprintf( prisec_numberOfEventsUntilFullSortedFile, "('%3s', '%3s', %12.1f),\n", kv.first[0].c_str(),
                        kv.first[1].c_str(), kv.second );
        }
        std::fprintf( prisec_numberOfEventsUntilFullSortedFile, "]\n" );
        std::fclose( prisec_numberOfEventsUntilFullSortedFile );

        FILE* events_overall_and_selected_file;
        events_overall_and_selected_file =
            std::fopen( ( m_variant + "_SkipFilled" + "_events_overall_and_selected.py" ).c_str(), "w" );
        std::fprintf( events_overall_and_selected_file, "overallNumberOfEvents  = %7s\n",
                      ( std::to_string( overallNumberOfEvents ) ).c_str() );
        std::fprintf( events_overall_and_selected_file, "amountOfSelectedEvents = %7s\n",
                      ( std::to_string( amountOfSelectedEvents ) ).c_str() );
        std::fclose( events_overall_and_selected_file );
      }

      return Consumer::finalize();
    }

    /// Functional operator
    void operator()( const LHCb::Track::Range&                 tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons,       //
                     const SIMDMirrorData::Vector&             mirrorData ) const override;

  protected:
    /// Pre-book all histograms
    StatusCode prebookHistograms() override;

  private:
    // dictionary of delta-theta vs. phi histograms
    mutable std::map<std::string, Hist::H2D<>> h_alignHistoRec2D;
    // some more 1D hitograms
    mutable std::map<std::string, Hist::H1D<>> h_alignHistoRec1D;

    // properties and tools

    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.9999f, 0.9999f } }; ///
    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } }; ///

    /// minimum momentum value for tracks
    // Gaudi::Property<float>                     m_minP4Align        {this, "MinP4Align",        50.*GeV        };
    // ///<saturation momenum for Rich2

    Gaudi::Property<int> m_nPhiBins{ this, "NPhiBins", 60 }; ///< for Rich2
                                                             // previously used m_nPhiBins values: 25, 20

    Gaudi::Property<int> m_nThetaBins{ this, "NThetaBins", 50 }; ///< for Rich2

    // ///<for Rich2: Phi bins in quantile
    /// mirrors of which radiator to align?
    Gaudi::Property<std::vector<std::string>> m_radName{ this, "RichGases", { "Rich2Gas" } }; ///< default

    Gaudi::Property<bool> m_useMCTruth{ this, "UseMCTruth", false }; ///

    Gaudi::Property<int> m_histoOutputLevel{ this, "HistoOutputLevel", 3 }; ///

    Gaudi::Property<std::vector<std::pair<std::string, std::string>>> m_prebookHistos{ this, "PrebookHistos" }; ///

    Gaudi::Property<std::vector<std::string>> m_tasks{ this, "MirrorAlignTasks", { "Produce" } }; ///

    Gaudi::Property<float> m_poorestPopulation{ this, "PoorestPopulation", 279. }; ///< poorest population for Rich2

    Gaudi::Property<std::string> m_variant{ this, "Variant", "" }; ///< e.g. 2022-07-11T17:31_rich2

    Gaudi::Property<int> m_minUsefulTracks{ this, "MinUsefulTracks",
                                            1 }; ///< min number of useful tracks to select event

    ToolHandle<const ITrackSelector> m_tkSel{ this, "TrackSelector", "TrackSelector" };

    // the above is hardly needed, instead -- a simple initialization:
    Gaudi::Property<float> m_deltaThetaRange{ this, "DeltaThetaRange", 0.004 }; ///< for Rich2

    // dictionary of tasks
    std::map<std::string, bool> alignTasks = {
        { "Produce", false },       //
        { "Monitor", false },       //
        { "MapExlore", false },     //
        { "MapConstruct", false },  //
        { "Map", false },           //
        { "MapUse", false },        //
        { "MapPhi", false },        //
        { "MapPhiUse", false },     //
        { "AllFillCount", false },  //
        { "AllFillPhi", false },    //
        { "CheckRestFill", false }, //
        { "SkipFilled", false },    //
        { "CoordSysTrans", false }, //
    };

    bool task_Produce;       // fill the production set of histograms
    bool task_Monitor;       // add various checking histograms
    bool task_MapExlore;     // explore tracks and photons for the HLT1 pre-selection line "map"
    bool task_MapConstruct;  // construct exploratory data structure for the HLT1 pre-selection line "map"
    bool task_Map;           // create data structure to be used in the HLT1 selection line with entire content
    bool task_MapUse;        // use the "map" data structure a la in the HLT1 pre-selection line "map"
    bool task_MapPhi;        // create data structure to be used in the HLT1 selection line with content in quantiles
    bool task_MapPhiUse;     // use the "mapPhi" data structure in the HLT1 pre-selection line "mapPhi"
    bool task_AllFillCount;  // add counters for optimization of the RICH2 mirror combinations subset
    bool task_AllFillPhi;    // fill 1D phi histos for optimization of the RICH2 mirror combinations subset
    bool task_CheckRestFill; // check filling the rest of RICH2 mirror combinations along with 8 poorest
    bool task_SkipFilled;    // check filling all RICH2 mirror combinations with skipping when filled
    bool task_CoordSysTrans; // explore influence of RICH1 coordinate systems on distribution shapes

    // this is only for the "AllFillCount" task
    // accumulative dictionary-counter of photons per any mirror combination,
    // that have at least one [unambiguous] photon emitted by a saturated
    // track;
    mutable std::array<std::map<std::array<std::string, 2>, double>, 2> sides_prisec_allCombsPopulations;

    mutable std::map<std::array<std::string, 2>, double> prisec_restCombsPopulations;

    mutable double totalEventsCount    = 0.;
    mutable double selectedEventsCount = 0.;

    // for the Map and MapPhi tasks
    mutable int currentEventNumber   = 0; // Map MapUse MapPhi MapPhiUse
    mutable int includedEventsAmount = 0; // Map MapPhi
    mutable int selectedEventsAmount = 0; // MapUse MapPhiUse

    mutable std::map<std::array<std::string, 2>, bool> prisec_full;

    mutable std::map<std::array<std::string, 2>, double> prisec_totalPopulation;
    mutable std::map<std::array<std::string, 2>, double> prisec_populationWhenFull;
    mutable std::map<std::array<std::string, 2>, double> prisec_amountOfContributingEvents;
    mutable std::map<std::array<std::string, 2>, double> prisec_amountOfContributingEventsUntilFull;
    mutable std::map<std::array<std::string, 2>, double> prisec_numberOfEventsUntilFull;
    mutable std::vector<std::array<std::string, 2>>      prebookPriSecCombs;
    mutable std::map<std::string, std::pair<int, std::map<std::string, std::array<int, 2>>>>
        bin_trackCount_comb_fullnessPhotCount;

    // per-combination fullness flag and accumulating population,
    // which does not increase after reachin certain fullness
    mutable std::map<std::string, std::array<int, 2>> comb_status;
    mutable std::map<std::string, std::array<int, 2>> comb_status_phi;

    // per-bin number of events after which
    // this bin is phazed ot from contributing to any of the not-full combs
    mutable std::map<std::string, int> bin_eventNumber;

    mutable std::map<std::string, int> bin_eventNumberPhi;

    // total amount of phi theta bins
    mutable int nPhiThetaBins{ m_nPhiBins * m_nThetaBins };

    // just vector, not map, which will be serialized as a trivial sequence
    // for straigtforward usage in HLT1
    mutable std::vector<int> binEventNumberPhi;

    // list of bins comprising the least-populated quantile of each comb
    mutable std::map<std::string, std::vector<int>> comb_minPhiQuantileBins;

    mutable int overallNumberOfEvents  = 0;
    mutable int amountOfSelectedEvents = 0;

    mutable std::default_random_engine             gen;
    mutable std::uniform_real_distribution<double> uniProbDistr{ 0., 1. };

    // the following inputs are generated separately, by make_array_bin_weigts.py
    const uint16_t binThetaRich1Lo{ 6 };
    const uint16_t binThetaRich1Hi{ 25 };
    double         binWeightRich1[1140]{
        0.0000000, 0.0000395, 0.0000000, 0.0000460, 0.0000000, 0.0000375, 0.0000000, 0.0000000, 0.0000238, 0.0000197,
        0.0000381, 0.0000000, 0.0000000, 0.0000108, 0.0000332, 0.0000000, 0.0000000, 0.0000000, 0.0000112, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000177, 0.0000000, 0.0000000, 0.0000000, 0.0000496,
        0.0000574, 0.0000126, 0.0000000, 0.0000684, 0.0000000, 0.0000000, 0.0000000, 0.0000350, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000554, 0.0000000, 0.0000000, 0.0000081,
        0.0000000, 0.0000334, 0.0000144, 0.0000000, 0.0000323, 0.0000121, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000343, 0.0000000, 0.0000417, 0.0000583, 0.0000314, 0.0000000, 0.0000000, 0.0000397, 0.0000000, 0.0000000,
        0.0000399, 0.0000175, 0.0000000, 0.0000247, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000141, 0.0000327, 0.0000000, 0.0000202, 0.0000000, 0.0000038, 0.0000101,
        0.0000000, 0.0000484, 0.0000653, 0.0000435, 0.0000000, 0.0000000, 0.0000635, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000552, 0.0000621, 0.0000509, 0.0000000, 0.0000000, 0.0000480, 0.0000310, 0.0000000,
        0.0000000, 0.0000000, 0.0000099, 0.0000547, 0.0000114, 0.0000087, 0.0000137, 0.0000260, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000222, 0.0000590, 0.0000000, 0.0000000, 0.0000615, 0.0000157, 0.0000000,
        0.0000000, 0.0000471, 0.0000085, 0.0000211, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000105,
        0.0000000, 0.0000179, 0.0000000, 0.0000191, 0.0000110, 0.0000330, 0.0000305, 0.0000213, 0.0000000, 0.0000000,
        0.0000410, 0.0000628, 0.0000451, 0.0000000, 0.0000170, 0.0000000, 0.0000000, 0.0000139, 0.0000372, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000581, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000406,
        0.0000000, 0.0000000, 0.0000063, 0.0000000, 0.0000000, 0.0000000, 0.0000384, 0.0000000, 0.0000532, 0.0000269,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000072, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000204, 0.0000146,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000366,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000570,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000200, 0.0000000, 0.0000000, 0.0000449, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000280, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000543, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000155, 0.0000000,
        0.0000000, 0.0000220, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000227, 0.0000000, 0.0000000, 0.0000294, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000377,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000153,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0368365, 0.0193423, 0.0261998, 0.0394142, 0.0245817, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000166, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0181847, 0.0371158, 0.0292543, 0.0104037, 0.0366833, 0.0119442, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0228653, 0.0332524, 0.0197182, 0.0340462, 0.0342102, 0.0447294, 0.0451437, 0.0448442, 0.0448059, 0.0447180,
        0.0007364, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0219253, 0.0396768, 0.0225486, 0.0134481, 0.0363018, 0.0396836, 0.0309785, 0.0000000, 0.0000000, 0.0233521,
        0.0020391, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.1063134, 0.0025166, 0.1117997, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0297486, 0.0220278, 0.0120799, 0.0310106, 0.0336378, 0.0122800, 0.0243639, 0.0065224, 0.0400635, 0.0000000,
        0.0098871, 0.0078586, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0863191, 0.0003950, 0.0357148, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0269694, 0.0245353,
        0.0197729, 0.0245606, 0.0180833, 0.0336959, 0.0360355, 0.0290367, 0.0197853, 0.0199095, 0.0190433, 0.0191166,
        0.0202256, 0.0197929, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0135197, 0.0000000,
        0.1312927, 0.0943001, 0.1122517, 0.0000000, 0.0249350, 0.0000000, 0.0000000, 0.0247378, 0.0000000, 0.0013113,
        0.0336111, 0.0000000, 0.0000000, 0.0000000, 0.0169326, 0.0409186, 0.0339868, 0.0052343, 0.0000000, 0.0347223,
        0.0437981, 0.0082628, 0.0000000, 0.0000000, 0.0000000, 0.0017881, 0.1045600, 0.0888590, 0.1056367, 0.0756500,
        0.1140486, 0.0932367, 0.0000000, 0.0781633, 0.0000000, 0.0000000, 0.0000000, 0.0158802, 0.0317108, 0.0388790,
        0.0000000, 0.0317824, 0.0267300, 0.0189123, 0.0312847, 0.0365618, 0.0184774, 0.0362392, 0.0059341, 0.0000000,
        0.0228521, 0.0330757, 0.0000000, 0.0000000, 0.0000000, 0.0741972, 0.1237749, 0.0957403, 0.0883407, 0.1425088,
        0.1305765, 0.0000000, 0.1139858, 0.0576437, 0.1044223, 0.0518931, 0.0000000, 0.0000000, 0.0176580, 0.0000000,
        0.0115369, 0.0000000, 0.0287819, 0.0317494, 0.0000000, 0.0000000, 0.0199847, 0.0000000, 0.0433248, 0.0296798,
        0.0228772, 0.0000000, 0.0529850, 0.1096644, 0.0000000, 0.0109961, 0.0000000, 0.0357231, 0.1071056, 0.0632308,
        0.1238330, 0.1207836, 0.1185110, 0.1156137, 0.1234900, 0.0809193, 0.8765723, 0.0171029, 0.0000000, 0.0000000,
        0.0282131, 0.0000000, 0.0311581, 0.0373289, 0.0404809, 0.0227783, 0.0000000, 0.0000000, 0.0010132, 0.0000000,
        0.0000000, 0.0000000, 0.0089312, 0.0000000, 0.0565482, 0.1352743, 0.1165849, 0.0000000, 0.0529341, 0.0434715,
        0.1013776, 0.1049606, 0.0493684, 0.0587013, 0.0000000, 0.8293895, 1.0000000, 0.9778928, 0.9537474, 0.0000000,
        0.0369693, 0.0273080, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.6949331, 0.7715142, 0.8439094, 0.4794864, 0.0000000, 0.1104447, 0.1160094, 0.1094186, 0.0657950,
        0.0462614, 0.1276250, 0.0182414, 0.0000000, 0.2867206, 0.8698272, 0.8985322, 0.6909681, 0.0000000, 0.0000000,
        0.0000000, 0.0325890, 0.0125527, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0384172, 0.0083309,
        0.0000000, 0.0000000, 0.5300607, 0.6534078, 0.5342385, 0.0000000, 0.0619830, 0.1173823, 0.1087834, 0.0749184,
        0.0965689, 0.0855533, 0.0000000, 0.0439105, 0.9450874, 0.5071155, 0.9930622, 0.9011446, 0.8348405, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.2085568, 0.8489904, 0.8552204, 0.8259513, 0.6332040, 0.0000000, 0.0000000, 0.1238444, 0.0928697,
        0.0000000, 0.0000000, 0.0000000, 0.0585297, 0.7456008, 0.6832885, 0.9369704, 0.6349782, 0.7228407, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.5730656, 0.2592362, 0.6456161, 0.5674175, 0.0000000, 0.1074387, 0.0185007, 0.0489054, 0.1026781,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.7378945, 0.6129670, 0.9441124, 0.9887976, 0.5433762, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.5720013, 0.4942063, 0.6635706, 0.8194979, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.7588000, 0.6971409, 0.3670800, 0.7760098, 0.7892262, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0458242, 0.5171029, 0.5199198, 0.4102743, 0.6557457, 0.1326029, 0.0000000, 0.0000000, 0.0158003, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.5978799, 0.8236111, 0.9616658, 0.4195432, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.5468843, 0.6167577, 0.3159109, 0.4494101, 0.0600383, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.7164963, 0.0000000, 0.3018103, 0.3655505, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.6323985, 0.4133283, 0.5771683, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.5564531, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.8288057, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
    };
    const uint16_t binThetaRich2Lo{ 3 };
    const uint16_t binThetaRich2Hi{ 18 };
    double         binWeightRich2[900]{
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000053, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000211, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000415, 0.0001379, 0.0001579, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000603, 0.0000637, 0.0000000,
        0.0000283, 0.0000000, 0.0000130, 0.0001050, 0.0001120, 0.0000000, 0.0000788, 0.0000000, 0.0000874, 0.0000325,
        0.0000000, 0.0000668, 0.0001356, 0.0000352, 0.0000000, 0.0000111, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0003051, 0.0002573, 0.0003009,
        0.0002269, 0.0002492, 0.0001273, 0.0002436, 0.0000647, 0.0001764, 0.0000000, 0.0000589, 0.0002244, 0.0000969,
        0.0001092, 0.0000000, 0.0000109, 0.0000000, 0.0000000, 0.0000269, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000355, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000318, 0.0000459, 0.0001495, 0.0000860,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000241, 0.0000000, 0.0000000, 0.0000000,
        0.0000364, 0.0000000, 0.0001087, 0.0001073, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000422, 0.0000000, 0.0000000, 0.0000000, 0.0002441, 0.0002670, 0.0003025, 0.0001887, 0.0002645,
        0.0000362, 0.0000079, 0.0000000, 0.0000862, 0.0000000, 0.0000000, 0.0000000, 0.0000063, 0.0000000, 0.0000855,
        0.0001201, 0.0001226, 0.0001057, 0.0001159, 0.0000000, 0.0000192, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000280, 0.0000000, 0.0000148, 0.0001500, 0.0003760, 0.0003201, 0.0003737, 0.0003619, 0.0000260,
        0.0001416, 0.0000000, 0.0000424, 0.0000000, 0.0001586, 0.0000000, 0.0000181, 0.0000000, 0.0000519, 0.0000570,
        0.0000000, 0.0000000, 0.0000714, 0.0000134, 0.0004240, 0.0005007, 0.0002378, 0.0000000, 0.0000137, 0.0000468,
        0.0000475, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0004254, 0.0003039, 0.0001041, 0.0000000, 0.0000000,
        0.0000526, 0.0000000, 0.0000042, 0.0001059, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000892, 0.0001189, 0.0000000, 0.0002381, 0.0000000, 0.0001977, 0.0000185, 0.0000213, 0.0000297, 0.0000234,
        0.0000000, 0.0000000, 0.0002170, 0.0001426, 0.0003593, 0.0000123, 0.0003776, 0.0002912, 0.0000000, 0.0000000,
        0.0001810, 0.0000000, 0.0014263, 0.0020077, 0.0000510, 0.0000000, 0.0000000, 0.0000292, 0.0000232, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0003829, 0.0004680, 0.0003533, 0.0003403, 0.0001268, 0.0001055,
        0.0001342, 0.0004307, 0.0004474, 0.0002840, 0.0003375, 0.0001326, 0.0000953, 0.0001671, 0.0002487, 0.0000000,
        0.0000239, 0.0000000, 0.0000000, 0.0000000, 0.0004448, 0.0000000, 0.0016125, 0.0013250, 0.0001790, 0.0011706,
        0.0000000, 0.0000867, 0.0002988, 0.0001759, 0.0001991, 0.0002937, 0.0002740, 0.0001305, 0.0000663, 0.0000904,
        0.0000000, 0.0000000, 0.0000000, 0.0048346, 0.0000000, 0.0000000, 0.0003943, 0.0000000, 0.0000000, 0.0026324,
        0.0038214, 0.0000000, 0.0020631, 0.0039076, 0.0025137, 0.0025205, 0.0017056, 0.0016818, 0.0009231, 0.0014604,
        0.0000000, 0.0000000, 0.0000967, 0.0000199, 0.0000000, 0.0004263, 0.0005830, 0.0021030, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0100278, 0.0003215, 0.0000116, 0.0000000, 0.0000000, 0.0000000, 0.0039618, 0.0100190,
        0.0024669, 0.0019518, 0.0029558, 0.0024210, 0.0005647, 0.0006889, 0.0010969, 0.0015499, 0.0012548, 0.0001222,
        0.0000000, 0.0000000, 0.0043003, 0.0000000, 0.0000000, 0.0000000, 0.0001699, 0.0030912, 0.0000000, 0.0000000,
        0.0006706, 0.0000000, 0.0052099, 0.0000000, 0.0000000, 0.0004536, 0.0066705, 0.0060961, 0.0061698, 0.0072051,
        0.0118554, 0.0021442, 0.0030719, 0.0032057, 0.0185672, 0.0181478, 0.0149006, 0.0022383, 0.0000000, 0.0064967,
        0.0000000, 0.0099849, 0.0063750, 0.0087675, 0.0000545, 0.0000000, 0.0006910, 0.0097276, 0.0070259, 0.0010448,
        0.0012951, 0.0060001, 0.0087883, 0.0013116, 0.0001630, 0.0087726, 0.0105014, 0.0081673, 0.0093769, 0.0075688,
        0.0098192, 0.0000000, 0.0000000, 0.0099986, 0.0191198, 0.0144023, 0.0182746, 0.0168719, 0.0348260, 0.0369526,
        0.0132263, 0.0067899, 0.0050448, 0.0080632, 0.0000000, 0.0000693, 0.0000000, 0.0058189, 0.0033364, 0.0004439,
        0.0005714, 0.0027180, 0.0737443, 0.0002455, 0.0007599, 0.0565114, 0.0558663, 0.0188896, 0.0116702, 0.0138033,
        0.0389035, 0.0203530, 0.0153140, 0.0469834, 0.0432228, 0.0454358, 0.0173859, 0.0215058, 0.0405155, 0.0402141,
        0.0309188, 0.0045975, 0.0081699, 0.0413454, 0.0082100, 0.0018174, 0.0033557, 0.0091339, 0.0079202, 0.0030172,
        0.0029029, 0.0102406, 0.1087423, 0.0012263, 0.0011646, 0.0016922, 0.0404529, 0.0272148, 0.0058056, 0.0443609,
        0.0282359, 0.0293395, 0.0000000, 0.0254308, 0.0242734, 0.0399761, 0.0260386, 0.0458923, 0.0327040, 0.0382428,
        0.0390069, 0.0126199, 0.0523199, 0.0495975, 0.0261278, 0.0025872, 0.0020798, 0.0000000, 0.0032824, 0.0009812,
        0.0201286, 0.0480849, 0.0093936, 0.0000000, 0.0498951, 0.0569131, 0.0119666, 0.0907815, 0.1738825, 0.0416013,
        0.0488527, 0.0223241, 0.0330339, 0.0243995, 0.0315220, 0.0122448, 0.0221681, 0.0272530, 0.0431595, 0.0379257,
        0.0648887, 0.1219176, 0.1235641, 0.0594758, 0.0705827, 0.0420197, 0.0311836, 0.0217425, 0.0842468, 0.0039426,
        0.0214766, 0.0809491, 0.0139094, 0.0293588, 0.0653046, 0.0722820, 0.0668461, 0.0946937, 0.0936237, 0.0938004,
        0.0355365, 0.0226048, 0.0204923, 0.0177032, 0.0314935, 0.0455708, 0.0501151, 0.0473823, 0.0361846, 0.0302814,
        0.1170262, 0.1515591, 0.1599861, 0.1377978, 0.0531122, 0.0079065, 0.0014124, 0.0285287, 0.0197869, 0.0179364,
        0.0754655, 0.0671361, 0.0236813, 0.0000000, 0.0527436, 0.1186903, 0.1947216, 0.1754076, 0.1852050, 0.0580277,
        0.0166480, 0.0031350, 0.0000000, 0.0289244, 0.0405746, 0.0153408, 0.0144866, 0.0122566, 0.0261911, 0.0000000,
        0.0917407, 0.1149044, 0.1317275, 0.0554523, 0.0732547, 0.1167063, 0.0176411, 0.0250682, 0.1046708, 0.0260029,
        0.1089701, 0.0968169, 0.0226210, 0.0088567, 0.0653744, 0.0774182, 0.0920518, 0.1032048, 0.0959279, 0.0383448,
        0.0195338, 0.0148661, 0.0092091, 0.0131579, 0.0217267, 0.0526945, 0.0338858, 0.0010603, 0.0291379, 0.0110271,
        0.0636425, 0.1555312, 0.1500885, 0.5564383, 0.0356915, 0.0878197, 0.0297162, 0.0208952, 0.0388750, 0.0406650,
        0.0000000, 0.0253119, 0.1767159, 0.1959813, 0.2114366, 0.8816745, 0.9254205, 0.1279724, 0.0478511, 0.0152196,
        0.0544557, 0.0099566, 0.0000000, 0.0116994, 0.0030926, 0.0000000, 0.0000000, 0.0134470, 0.0175062, 0.0487162,
        0.0265548, 0.0658352, 0.2339007, 1.0000000, 0.9343820, 0.0762726, 0.1170250, 0.0476021, 0.0443709, 0.0000000,
        0.0967063, 0.1079622, 0.1397962, 0.1412119, 0.1668838, 0.4175482, 0.4711346, 0.1409748, 0.0202139, 0.0339940,
        0.0000000, 0.0000000, 0.0000000, 0.0249498, 0.0060843, 0.0468130, 0.0395653, 0.0000000, 0.0076386, 0.0153116,
        0.0625268, 0.0299594, 0.4378625, 0.6453041, 0.6116147, 0.1938037, 0.1770882, 0.0901943, 0.0466264, 0.0000000,
        0.0000000, 0.0000000, 0.1781603, 0.1902417, 0.1732606, 0.7802713, 0.2429797, 0.1441308, 0.0395243, 0.0000000,
        0.0000000, 0.0254173, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0259401, 0.0000000,
        0.0756351, 0.0743579, 0.1810016, 0.2735156, 0.8355417, 0.0947816, 0.0587062, 0.0853797, 0.0161464, 0.0000000,
        0.0000000, 0.0150388, 0.1350467, 0.1599035, 0.3409654, 0.4320225, 0.1690646, 0.1616136, 0.0511251, 0.0398439,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0512264, 0.0000000, 0.0000000, 0.0161373, 0.0000000,
        0.0000000, 0.0185039, 0.1214164, 0.0728783, 0.2908287, 0.1891603, 0.1904575, 0.1347278, 0.0403822, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.1320383, 0.7775854, 0.7200883, 0.1479633, 0.1446373, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0751280, 0.2453178, 0.9380993, 0.9971845, 0.0970835, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0301680, 0.3963379, 0.4235207, 0.2608802, 0.0000000, 0.0000000, 0.0368580,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0523224, 0.0000000, 0.0806589, 0.1150729, 0.1121482, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.5802374, 0.0000000, 0.1456802, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.9442781, 0.6748683, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0938279, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
        0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000, 0.0000000,
    };

    /// mutex lock
    mutable std::mutex m_fill_histogram_lock;
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//=============================================================================
// Initialization
//=============================================================================
//=============================================================================
// Book histograms
//=============================================================================
StatusCode SIMDAlignment::prebookHistograms() {
  using namespace Gaudi::Units;

  const auto radName = m_radName[0];
  // which radiator ?
  const auto  rad = ( radName == "Rich2Gas" ? Rich::Rich2Gas : Rich::Rich1Gas );
  std::string RN  = ( radName == "Rich2Gas" ? "R2" : "R1" );

  // "un-false" tasks, those in m_tasks
  for ( auto& task : m_tasks ) alignTasks.at( task ) = true;

  task_Produce       = alignTasks.at( "Produce" );
  task_Monitor       = alignTasks.at( "Monitor" );
  task_MapExlore     = alignTasks.at( "MapExlore" );
  task_MapConstruct  = alignTasks.at( "MapConstruct" );
  task_Map           = alignTasks.at( "Map" );
  task_MapUse        = alignTasks.at( "MapUse" );
  task_MapPhi        = alignTasks.at( "MapPhi" );
  task_MapPhiUse     = alignTasks.at( "MapPhiUse" );
  task_AllFillCount  = alignTasks.at( "AllFillCount" );
  task_AllFillPhi    = alignTasks.at( "AllFillPhi" );
  task_CheckRestFill = alignTasks.at( "CheckRestFill" );
  task_SkipFilled    = alignTasks.at( "SkipFilled" );
  task_CoordSysTrans = alignTasks.at( "CoordSysTrans" );

  bool        ok    = true;
  std::string h_id  = "";
  std::string title = "";

  // Book histograms for an optimal subset of mirror combinations determined
  // in advance. Their list is stored in a vector of strings,
  // e.g. 'p00s00', 'p01s03',..., where the first three letters are
  // a primary mirror segment name (ie 'p'+number), while the second three letters
  // are a secondary mirror segment name ('s'+number).

  if ( task_AllFillPhi ) {

    if ( RN == "R1" ) {
      prebookPriSecCombs = {
          { "p00", "s00" }, { "p00", "s01" }, { "p00", "s02" }, { "p00", "s03" }, { "p01", "s04" }, { "p01", "s05" },
          { "p01", "s06" }, { "p01", "s07" }, { "p02", "s12" }, { "p02", "s13" }, { "p02", "s14" }, { "p02", "s15" },
          { "p03", "s08" }, { "p03", "s09" }, { "p03", "s10" }, { "p03", "s11" },
      };
    } else if ( RN == "R2" ) {
      prebookPriSecCombs = {
          { "p00", "s00" }, { "p00", "s01" }, { "p01", "s00" }, { "p01", "s01" }, { "p01", "s02" }, { "p02", "s01" },
          { "p02", "s02" }, { "p02", "s03" }, { "p02", "s06" }, { "p03", "s02" }, { "p03", "s03" }, { "p03", "s07" },
          { "p04", "s00" }, { "p04", "s01" }, { "p04", "s04" }, { "p04", "s05" }, { "p05", "s01" }, { "p05", "s02" },
          { "p05", "s05" }, { "p05", "s06" }, { "p06", "s02" }, { "p06", "s03" }, { "p06", "s06" }, { "p06", "s07" },
          { "p07", "s03" }, { "p07", "s07" }, { "p07", "s11" }, { "p08", "s04" }, { "p08", "s05" }, { "p08", "s08" },
          { "p08", "s09" }, { "p09", "s04" }, { "p09", "s05" }, { "p09", "s06" }, { "p09", "s08" }, { "p09", "s09" },
          { "p09", "s10" }, { "p10", "s05" }, { "p10", "s06" }, { "p10", "s07" }, { "p10", "s09" }, { "p10", "s10" },
          { "p10", "s11" }, { "p11", "s06" }, { "p11", "s07" }, { "p11", "s10" }, { "p11", "s11" }, { "p12", "s04" },
          { "p12", "s05" }, { "p12", "s08" }, { "p12", "s09" }, { "p12", "s12" }, { "p12", "s13" }, { "p13", "s05" },
          { "p13", "s06" }, { "p13", "s09" }, { "p13", "s10" }, { "p13", "s13" }, { "p13", "s14" }, { "p14", "s06" },
          { "p14", "s07" }, { "p14", "s10" }, { "p14", "s11" }, { "p14", "s14" }, { "p14", "s15" }, { "p15", "s07" },
          { "p15", "s11" }, { "p15", "s15" }, { "p16", "s08" }, { "p16", "s09" }, { "p16", "s12" }, { "p16", "s13" },
          { "p17", "s08" }, { "p17", "s09" }, { "p17", "s10" }, { "p17", "s12" }, { "p17", "s13" }, { "p17", "s14" },
          { "p18", "s09" }, { "p18", "s10" }, { "p18", "s11" }, { "p18", "s13" }, { "p18", "s14" }, { "p18", "s15" },
          { "p19", "s10" }, { "p19", "s11" }, { "p19", "s14" }, { "p19", "s15" }, { "p20", "s12" }, { "p20", "s13" },
          { "p20", "s16" }, { "p20", "s17" }, { "p21", "s13" }, { "p21", "s14" }, { "p21", "s17" }, { "p21", "s18" },
          { "p22", "s14" }, { "p22", "s15" }, { "p22", "s18" }, { "p22", "s19" }, { "p23", "s11" }, { "p23", "s15" },
          { "p23", "s19" }, { "p24", "s16" }, { "p24", "s17" }, { "p25", "s16" }, { "p25", "s17" }, { "p25", "s18" },
          { "p26", "s17" }, { "p26", "s18" }, { "p26", "s19" }, { "p27", "s15" }, { "p27", "s18" }, { "p27", "s19" },
          { "p28", "s20" }, { "p28", "s21" }, { "p28", "s24" }, { "p29", "s20" }, { "p29", "s21" }, { "p29", "s22" },
          { "p29", "s25" }, { "p30", "s21" }, { "p30", "s22" }, { "p30", "s23" }, { "p31", "s22" }, { "p31", "s23" },
          { "p32", "s20" }, { "p32", "s24" }, { "p32", "s28" }, { "p33", "s20" }, { "p33", "s21" }, { "p33", "s24" },
          { "p33", "s25" }, { "p34", "s21" }, { "p34", "s22" }, { "p34", "s25" }, { "p34", "s26" }, { "p35", "s22" },
          { "p35", "s23" }, { "p35", "s26" }, { "p35", "s27" }, { "p36", "s24" }, { "p36", "s25" }, { "p36", "s28" },
          { "p36", "s29" }, { "p37", "s24" }, { "p37", "s25" }, { "p37", "s26" }, { "p37", "s28" }, { "p37", "s29" },
          { "p37", "s30" }, { "p38", "s25" }, { "p38", "s26" }, { "p38", "s27" }, { "p38", "s29" }, { "p38", "s30" },
          { "p38", "s31" }, { "p39", "s26" }, { "p39", "s27" }, { "p39", "s30" }, { "p39", "s31" }, { "p40", "s24" },
          { "p40", "s28" }, { "p40", "s32" }, { "p41", "s24" }, { "p41", "s25" }, { "p41", "s28" }, { "p41", "s29" },
          { "p41", "s32" }, { "p41", "s33" }, { "p42", "s25" }, { "p42", "s26" }, { "p42", "s29" }, { "p42", "s30" },
          { "p42", "s33" }, { "p42", "s34" }, { "p43", "s26" }, { "p43", "s27" }, { "p43", "s30" }, { "p43", "s31" },
          { "p43", "s34" }, { "p43", "s35" }, { "p44", "s28" }, { "p44", "s29" }, { "p44", "s32" }, { "p44", "s33" },
          { "p45", "s28" }, { "p45", "s29" }, { "p45", "s30" }, { "p45", "s32" }, { "p45", "s33" }, { "p45", "s34" },
          { "p46", "s29" }, { "p46", "s30" }, { "p46", "s31" }, { "p46", "s33" }, { "p46", "s34" }, { "p46", "s35" },
          { "p47", "s30" }, { "p47", "s31" }, { "p47", "s34" }, { "p47", "s35" }, { "p48", "s28" }, { "p48", "s32" },
          { "p48", "s36" }, { "p49", "s32" }, { "p49", "s33" }, { "p49", "s36" }, { "p49", "s37" }, { "p50", "s33" },
          { "p50", "s34" }, { "p50", "s37" }, { "p50", "s38" }, { "p51", "s34" }, { "p51", "s35" }, { "p51", "s38" },
          { "p51", "s39" }, { "p52", "s32" }, { "p52", "s36" }, { "p52", "s37" }, { "p53", "s36" }, { "p53", "s37" },
          { "p53", "s38" }, { "p54", "s37" }, { "p54", "s38" }, { "p54", "s39" }, { "p55", "s38" }, { "p55", "s39" },
      };
    }

  } else {

    for ( const auto& prisec : m_prebookHistos ) {
      // store set of combinations in another handy manner
      prebookPriSecCombs.push_back( { prisec.first, prisec.second } );
      // initialize the per-comb information
      if ( task_Map ) comb_status.insert( { prisec.first + prisec.second, { 0, 0 } } );
      if ( task_MapPhi ) comb_status_phi.insert( { prisec.first + prisec.second, { 0, 0 } } );
    }
  }

  for ( const auto& prebookComb : prebookPriSecCombs ) {

    const auto& pri  = prebookComb[0].substr( 1, 2 );
    const auto& sec  = prebookComb[1].substr( 1, 2 );
    const auto& comb = prebookComb[0] + prebookComb[1];

    if ( task_Produce ) {
      h_id  = "dThetaPhiRecSaturTr" + RN + comb;
      title = "Ch. delta theta vs. phi saturated tracks: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins, -m_deltaThetaRange,
                      m_deltaThetaRange, m_nThetaBins );
    }

    if ( task_MapUse ) {
      h_id  = "dThetaPhiRecSaturTr" + RN + comb + "HLT1sel";
      title = "Ch. delta theta vs. phi saturated tracks: primary: " + pri + " secondary: " + sec +
              " in HLT1-selected events";
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins, -m_deltaThetaRange,
                      m_deltaThetaRange, m_nThetaBins );
    }

    if ( task_MapPhiUse ) {
      h_id  = "dThetaPhiRecSaturTr" + RN + comb + "HLT1selPhi";
      title = "Ch. delta theta vs. phi saturated tracks: primary: " + pri + " secondary: " + sec +
              " in HLT1-selected events phi";
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins, -m_deltaThetaRange,
                      m_deltaThetaRange, m_nThetaBins );
    }

    if ( task_MapExlore ) {
      h_id  = "thetaPhiRecSaturTr" + RN + comb;
      title = "Satur. tr. map: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, -pi, pi, m_nPhiBins,
                      // 0.,            0.5 * pi,   m_nThetaBins ) );
                      0., std::sqrt( 0.5 * pi ), m_nThetaBins ); // 1.25

      h_id  = "numPhotsRecSaturTr" + RN + comb;
      title = "Satur. tr. Ch. phots: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec1D[h_id], HID( h_id, rad ), title, 0., 100., 100 );
    }

    if ( task_CoordSysTrans ) {
      h_id  = "dThetaPhiRecSaturTr4Symm" + RN + comb;
      title = "Ch. delta theta vs. phi saturated tracks symmetrized: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins, -m_deltaThetaRange,
                      m_deltaThetaRange, m_nThetaBins );
      h_id  = "dThetaPhiRecAllTrSymm" + RN + comb;
      title = "Ch. delta theta vs. phi all tracks symmetrized: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec2D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins, -m_deltaThetaRange,
                      m_deltaThetaRange, m_nThetaBins );
    }

    if ( task_AllFillPhi ) {
      h_id  = "PhiRecSaturTr" + RN + comb;
      title = "Ch. phi saturated tracks: primary: " + pri + " secondary: " + sec;
      ok &= initHist( h_alignHistoRec1D[h_id], HID( h_id, rad ), title, 0., 2. * pi, m_nPhiBins );
    }

    if ( task_SkipFilled ) { prisec_full[{ prebookComb[0], prebookComb[1] }] = false; }
  }

  return StatusCode{ ok };
}

//=============================================================================
// Main execution
//=============================================================================
void SIMDAlignment::operator()( const LHCb::Track::Range&                 tracks,        //
                                const Summary::Track::Vector&             sumTracks,     //
                                const Relations::PhotonToParents::Vector& photToSegPix,  //
                                const LHCb::RichTrackSegment::Vector&     segments,      //
                                const CherenkovAngles::Vector&            expTkCKThetas, //
                                const SIMDCherenkovPhoton::Vector&        photons,       //
                                const SIMDMirrorData::Vector&             mirrorData ) const {

  // Sanity check that the extra mirror data is valid
  if ( photons.size() != mirrorData.size() ) { throw std::runtime_error( "SIMDMirrorData not enabled !" ); }

  // the lock
  std::scoped_lock lock{ m_fill_histogram_lock };

  using namespace Gaudi::Units;

  const auto radName = m_radName[0];
  // which radiator ?
  const auto        rad  = ( radName == "Rich1Gas" ? Rich::Rich1Gas : Rich::Rich2Gas );
  const auto        rich = richType( rad );
  const std::string RN   = ( radName == "Rich1Gas" ? "R1" : "R2" );

  currentEventNumber++;

  // info() <<"currentEventNumber = "<<currentEventNumber<< endmsg;
  if ( currentEventNumber == 1 ) {

    if ( task_MapPhi ) {
      // in our comb_minPhiQuantileBins map, for each combination,
      // store bin numbers of its least-populated quantile
      const YAML::Node& quantiles = YAML::LoadFile( m_variant + "_MapPhi" + "_comb_quantile_bins_photons.yml" );
      for ( const auto& kv : quantiles ) {
        std::vector<int> quantileBins;
        for ( const auto& v : kv.second["minQuantileBins"] ) { quantileBins.push_back( v.as<int>() ); }
        comb_minPhiQuantileBins[kv.first.as<std::string>()] = quantileBins;
      }
      // need to initialize our main map -- which is vector -- with zeros
      // and at the same time set its length according to the m_nThetaBins and m_nPhiBins
      // this seems to be the only way to define a variable length of our main map
      for ( auto i = 0; i < nPhiThetaBins; i++ ) { binEventNumberPhi.push_back( 0 ); }
    }

    if ( task_MapUse || task_MapPhiUse ) {
      // note: these tasks can only be "either or"
      // it is more pragmatic to use the map as key=bin, value=eventNumber
      // this is contrary to how it is stored, therefore we swap them here
      const std::string thisTask   = ( task_MapUse ? "MapUse" : "MapPhiUse" );
      const YAML::Node& map_loaded = YAML::LoadFile( m_variant + "_" + thisTask + "_map_events_bin.yml" );
      for ( const auto& kv : map_loaded ) {
        bin_eventNumber[kv.second.as<std::string>()] = kv.first.as<int>();
        info() << kv.second.as<std::string>() << "  " << kv.first.as<int>() << endmsg;
      }
    }
  } // currentEventNumber == 1

  // this is used only in "CheckRestFill"
  // accumulative dictionary-counter of photons per the rest of the mirror
  // combinations, apart from the most difficult to populate combinations,
  // that have at least one [unambiguous] photon emitted by a saturated track;
  std::map<std::array<std::string, 2>, double> prisec_restCombsPopulationsInEvent;

  // this is used only in "Map" and "MapPhi"
  bool includeThisEvent = false;

  // this is used only in "SkipFilled", "MapUse" and "MapPhiUse"
  // becomes false only if usefulTrackCount < m_minUsefulTracks
  bool thisEventSelected = true;

  // declare track's phi, theta etc.
  float       trPhi   = 0.;
  float       trTheta = 0.;
  float       pTot    = 0.;
  std::string h_id    = "";

  std::string bin;
  std::string pri;
  std::string sec;

  int binRaw{}; // track's angular bin number = ( binTrTheta * m_nPhiBins ) + binTrPhi
  int binSel{}; // track's angular bin number after non-empty theta raws are selected

  // ##### preparatory loop over tracks for imitation of the HLT1 selection #####

  if ( task_MapUse || task_MapPhiUse ) {

    // this is used only here, locally, in case "MapPhiUse"
    int usefulTrackCount = 0;

    // only for imitation of the HLT1 selection !!!
    // preparatory loop over the track containers of all tracks
    // in order to decide whether this event is to be selected
    // for the imitation of the HLT1 selection and histogrammiing
    for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {

      // is this track accepted ?
      if ( !m_tkSel.get()->accept( *tk ) ) continue;

      bool usefulTrack = false;

      bool thisTrackFirstPhoton = true;

      // loop over photons for this track
      for ( const auto photIn : sumTk.photonIndices() ) {

        const auto& rels = photToSegPix[photIn];
        // the segment for this photon
        const auto& seg = segments[rels.segmentIndex()];

        // track angles are the same for all photons...
        if ( thisTrackFirstPhoton ) {
          thisTrackFirstPhoton = false;

          trPhi   = seg.bestMomentum().Phi();
          trTheta = seg.bestMomentum().Theta();

          // find which bin the track belongs
          // NB! track's Phi: (-Pi, +Pi)

          int binTrPhi;
          if ( trPhi < 0. ) {
            binTrPhi = (int)( trPhi / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2 - 1;
            if ( binTrPhi < 0 ) binTrPhi = 0;
          } else {
            binTrPhi = (int)( trPhi / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2;
            if ( binTrPhi > ( m_nPhiBins - 1 ) ) binTrPhi = m_nPhiBins - 1;
          }

          int binTrTheta = (int)( std::sqrt( trTheta ) / ( std::sqrt( 0.5 * pi ) / (double)m_nThetaBins ) );

          if ( RN == "R1" ) {
            if ( binThetaRich1Lo <= binTrTheta && binTrTheta <= binThetaRich1Hi ) {
              binSel = binTrPhi + ( binTrTheta - binThetaRich1Lo ) * m_nPhiBins;
              // random choice
              if ( uniProbDistr( gen ) < binWeightRich1[binSel] ) usefulTrack = true;
            }
          } else {
            if ( binThetaRich2Lo <= binTrTheta && binTrTheta <= binThetaRich2Hi ) {
              binSel = binTrPhi + ( binTrTheta - binThetaRich2Lo ) * m_nPhiBins;
              // random choice
              if ( uniProbDistr( gen ) < binWeightRich2[binSel] ) usefulTrack = true;
            }
          }
        } // thisTrackFirstPhoton

      } // end of loop over photons of this track

      if ( usefulTrack ) usefulTrackCount++;

    } // end of preparatory loop over the track containers

    if ( usefulTrackCount < m_minUsefulTracks ) { thisEventSelected = false; }
  }

  // ################ regular loop over the track containers ####################

  if ( thisEventSelected ) { // this is default

    selectedEventsAmount++;

    // we should skip this event if it is not selected
    // normally, it is, except when usefulTrackCount < m_minUsefulTracks
    // in MapUse and  MapPhiUse

    // following counters are used only in Map & MapPhi

    int usefulTrackCount = 0; // Map, MapPhi

    std::vector<std::string> combsUpdatedByUsefulTracks; // Map, MapPhi

    std::vector<std::string> usefulTracksBins; // Map, MapPhi

    std::vector<int> usefulTracksBinsRaw; // MapPhi

    // regular loop over the track containers
    for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {

      // is this track accepted ?
      if ( !m_tkSel.get()->accept( *tk ) ) continue;

      bool usefulTrack = false; // Map and MapPhi

      // this is used only in SkipFilled
      std::map<std::array<std::string, 2>, bool> prisec_thisEventContributed;
      std::map<std::array<std::string, 2>, bool> prisec_thisEventContributedUntilFull;
      // initialize them
      for ( const auto& prebookPriSecComb : prebookPriSecCombs ) {
        prisec_thisEventContributed[prebookPriSecComb]          = false;
        prisec_thisEventContributedUntilFull[prebookPriSecComb] = false;
      }

      // this is used only in MapExlore
      // declare counter of useful photons produced by this track (if its momentum
      // is beyond saturation) in the chosen radiator, per mirror combination
      // from the production subset to which its photon groups contribute; there
      // can be more than one such combination
      std::map<std::array<std::string, 2>, double> prisec_photCountSaturTr4Map;

      // this is used only in MapExlore
      // list of mirror combinations only from the production subset, to which
      // current track contributes its photons
      std::vector<std::array<std::string, 2>> populatedPriSecCombsSaturTr4Map;

      // this is used only in MapConstruct
      // for this track: map of combs' numbers of photons
      std::map<std::string, std::array<int, 2>> comb_fullnessPhotCount;

      bool thisTrackFirstPhoton = true; // MapUse and MapPhiUse
      // loop over photons for this track
      for ( const auto photIn : sumTk.photonIndices() ) {
        // info() <<"new photon"<< endmsg;

        const auto& rels = photToSegPix[photIn];

        // the segment for this photon
        const auto& seg = segments[rels.segmentIndex()];

        /////////////// MapConstruct Map MapUse MapPhi MapPhiUse ///////////////

        if ( task_MapConstruct || task_Map || task_MapUse || task_MapPhi || task_MapPhiUse ) {

          // track angles are the same for all photons...
          if ( thisTrackFirstPhoton ) {
            thisTrackFirstPhoton = false;

            trPhi   = seg.bestMomentum().Phi();
            trTheta = seg.bestMomentum().Theta();

            // find which bin the track belongs
            // NB! track's Phi: (-Pi, +Pi)

            // 0 --> m_nPhiBins - 1
            int binTrPhi;
            if ( trPhi < 0. ) {
              binTrPhi = (int)( trPhi / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2 - 1;
              if ( binTrPhi < 0 ) binTrPhi = 0;
            } else {
              binTrPhi = (int)( trPhi / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + m_nPhiBins / 2;
              if ( binTrPhi > ( m_nPhiBins - 1 ) ) binTrPhi = m_nPhiBins - 1;
            }

            // 0 --> m_ThetaBins - 1
            int binTrTheta = (int)( std::sqrt( trTheta ) / ( std::sqrt( 0.5 * pi ) / (double)m_nThetaBins ) );

            // this is used later for better understanding of how it works
            bin = std::to_string( binTrTheta * 100 + binTrPhi );

            if ( task_MapUse || task_MapPhiUse ) {
              if ( RN == "R1" ) {
                if ( 4 <= binTrTheta && binTrTheta <= 16 ) {
                  binSel = binTrPhi + ( binTrTheta - 4 ) * m_nPhiBins;
                  // random choice
                  if ( uniProbDistr( gen ) < binWeightRich1[binSel] ) usefulTrack = true;
                }
              } else {
                if ( 3 <= binTrTheta && binTrTheta <= 18 ) {
                  binSel = binTrPhi + ( binTrTheta - 3 ) * m_nPhiBins;
                  // random choice
                  if ( uniProbDistr( gen ) < binWeightRich2[binSel] ) usefulTrack = true;
                }
              }
            } else {
              // this is used in the preparatory step: task_MapPhi
              binRaw = ( binTrTheta * m_nPhiBins ) + binTrPhi;
            }

          } // thisTrackFirstPhoton
        }

        //////////////////////////////////////////////////////////////////////////

        // radiator of this segment
        const auto segRad = seg.radiator();

        // we work with only one RICH at a time
        if ( segRad != rad ) { continue; }

        // photon data
        const auto& phot = photons[photIn];

        // segment momentum
        pTot = seg.bestMomentumMag();

        // The PID type to assume. Just use Pion here.
        const auto pid = Rich::Pion;

        // beta
        const auto beta = richPartProps()->beta( pTot, pid );

        // selection cuts
        if ( beta < m_minBeta[rich] || m_maxBeta[rich] < beta ) continue;

        // mirror data
        const auto& mirrordata = mirrorData[photIn];

        // get the expected CK theta values for this segment
        const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

        // expected CK theta
        const auto thetaExp = expCKangles[pid];

        // mirror segment numbers
        const auto& mirrorDataPri = mirrordata.primaryMirrors();
        const auto& mirrorDataSec = mirrordata.secondaryMirrors();

        // loop over scalar entries in SIMD photon
        for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; i++ ) {

          // Select valid entries
          if ( phot.validityMask()[i] ) {
            // info() <<"valid photon"<< endmsg;
            // reconstructed theta
            const auto photTheta = phot.CherenkovTheta()[i];
            // reconstructed phi
            const auto photPhi = phot.CherenkovPhi()[i];
            // reconstructed delta theta
            const auto dTheta = photTheta - thetaExp;

            const int mirrorSegmentPri = mirrorDataPri[i]->mirrorNumber();
            const int mirrorSegmentSec = mirrorDataSec[i]->mirrorNumber();

            std::ostringstream ossPri;
            ossPri << std::setw( 2 ) << std::setfill( '0' ) << mirrorSegmentPri;
            std::ostringstream ossSec;
            ossSec << std::setw( 2 ) << std::setfill( '0' ) << mirrorSegmentSec;
            std::array<std::string, 2> prisec{ "p" + ossPri.str(), "s" + ossSec.str() };

            const auto& comb = prisec[0] + prisec[1];

            // check whether this combination of the mirror segments belongs to
            // the predefined subset, i.e. is in the list of chosen combinations
            // of the  mirror segment numbers
            for ( const auto& prebookPriSecComb : prebookPriSecCombs ) {

              if ( prisec == prebookPriSecComb ) {

                // std::printf("%s   trPhi = %5.3f   trTheta = %5.3f   photPhi = %5.3f   photTheta = %5.3f\n",
                // comb.c_str(), seg.bestMomentum().Phi(), seg.bestMomentum().Theta(), photPhi, photTheta);

                // --------------------------- Produce ---------------------------

                if ( task_Produce ) {
                  // fill the production histograms from the dictionary
                  h_id = "dThetaPhiRecSaturTr" + RN + comb;
                  ++h_alignHistoRec2D.at( h_id )[{ photPhi, dTheta }];
                }

                // ------------------------------ Map ----------------------------

                if ( task_Map ) {
                  if ( !comb_status[comb][0] ) {

                    // this track made at least one contribution
                    // to a not-yet-full-quantile comb
                    if ( !includeThisEvent ) {
                      if ( !usefulTrack ) {
                        usefulTrack = true;
                        usefulTracksBins.push_back( bin );
                        usefulTrackCount++;
                        if ( usefulTrackCount >= m_minUsefulTracks ) { includeThisEvent = true; }
                      }
                    }
                    if ( usefulTrack ) {
                      combsUpdatedByUsefulTracks.push_back( comb );
                      // first or one more photon of this useful track
                      // made contribution to a not-yet-full comb
                    }
                  }
                }

                // --------------------------- MapPhi ----------------------------

                if ( task_MapPhi ) {
                  // make sure that this comb is not full yet (compared to PoorestPopulation)
                  if ( !comb_status_phi[comb][0] ) {
                    // make sure that the currnt photon hits the Phi quantile's range of this comb
                    // if not -- do nothing and go to a next photon
                    // first, translate the photPhi to bin number (1, m_nPhiBins)

                    // NB! photon's Phi: (0, 2Pi)

                    // NB! bin numbring in quantiles is as in ROOT: from 1
                    // therefore, binPhotPhi is counted from 1 too
                    int binPhotPhi = (int)( photPhi / ( ( 2.0 * pi ) / (double)m_nPhiBins ) ) + 1;
                    if ( binPhotPhi < 1 ) binPhotPhi = 1;
                    if ( binPhotPhi > m_nPhiBins ) binPhotPhi = m_nPhiBins;
                    auto& v = comb_minPhiQuantileBins[comb];
                    if ( std::find( v.begin(), v.end(), binPhotPhi ) != v.end() ) {
                      // the photon is within the Phi quantile's range

                      // this track made at least one contribution
                      // to a comb with not-yet-full quantile
                      // therefore,
                      if ( !includeThisEvent ) {
                        if ( !usefulTrack ) {
                          usefulTrack = true;
                          usefulTracksBinsRaw.push_back( binRaw );
                          usefulTracksBins.push_back( bin );
                          usefulTrackCount++;
                          if ( usefulTrackCount >= m_minUsefulTracks ) { includeThisEvent = true; }
                        }
                      }
                      if ( usefulTrack ) {
                        combsUpdatedByUsefulTracks.push_back( comb );
                        // first or one more photon of this useful track
                        // made contribution to the not-yet-full quantile of this comb
                      }
                    }
                  }
                }

                // ----------------------------- MapUse --------------------------

                if ( task_MapUse ) {
                  // fill the histograms like in Produce,
                  // but we get here only for the useful tracks
                  // and if this event is already selected, which happens
                  // only if the number of useful tracks >= m_minUsefulTracks
                  h_id = "dThetaPhiRecSaturTr" + RN + comb + "HLT1sel";
                  ++h_alignHistoRec2D.at( h_id )[{ photPhi, dTheta }];
                }

                // --------------------------- MapPhiUse -------------------------

                if ( task_MapPhiUse ) {
                  // fill the histograms like in Produce,
                  // but we get here only for the useful tracks
                  // and if this event is already selected, which happens
                  // only if the number of useful tracks >= m_minUsefulTracks
                  h_id = "dThetaPhiRecSaturTr" + RN + comb + "HLT1selPhi";
                  ++h_alignHistoRec2D.at( h_id )[{ photPhi, dTheta }];
                }

                // -------------------------- MapExlore --------------------------

                if ( task_MapExlore ) {
                  // this is a useful photon - count it for this mirror
                  // combination from the production subset for the current track
                  prisec_photCountSaturTr4Map[prisec] += 1.;
                  // memorize this mirror combination from the production subset
                  // for the current track
                  if ( std::none_of( populatedPriSecCombsSaturTr4Map.begin(), populatedPriSecCombsSaturTr4Map.end(),
                                     [&]( auto s ) { return s == prisec; } ) )
                    populatedPriSecCombsSaturTr4Map.push_back( prisec );
                }

                // ------------------------- MapConstruct ------------------------

                if ( task_MapConstruct ) {
                  if ( ( comb_fullnessPhotCount.empty() ) ||
                       ( comb_fullnessPhotCount.find( comb ) == comb_fullnessPhotCount.end() ) ) {
                    // empty or not found -- create new entry
                    comb_fullnessPhotCount.insert( { comb, { 0, 1 } } );
                  } else {
                    // found -- check if it is not full yet: 0, not 1
                    if ( !comb_fullnessPhotCount[comb][0] ) {
                      comb_fullnessPhotCount[comb][1]++;
                      // check if it becomes full with this photo (not likely)
                      if ( comb_fullnessPhotCount[comb][1] >= m_poorestPopulation ) {
                        comb_fullnessPhotCount[comb][0] = 1;
                      }
                    }
                  }
                }

                // --------------------------- CoordSysTrans ---------------------

                // some on-demand exercises, and only in case of RICH1
                if ( radName == "Rich1Gas" && task_CoordSysTrans ) {
                  // few tricks to make the histograms look intuitively symmetric

                  pri = prisec[0].substr( 1, 2 ); // in case of RICH1 it is also the quadrant number
                  sec = prisec[1].substr( 1, 2 );

                  // reconstructed phi
                  auto photPhiSymm = photPhi;
                  // reconstructed delta theta
                  auto dThetaSymm = dTheta;
                  // first, flip the deltaTheta
                  if ( pri == "03" || pri == "02" ) dThetaSymm = -dThetaSymm;
                  // secondly, change direction and shift for the phi
                  if ( pri == "00" )
                    photPhiSymm += pi;
                  else if ( pri == "02" )
                    photPhiSymm = -photPhiSymm;
                  else if ( pri == "03" )
                    photPhiSymm = -photPhiSymm + pi;
                  // finally, restrict phi to 0 - 2pi range
                  if ( photPhiSymm < 0. )
                    photPhiSymm += 2. * pi;
                  else if ( photPhiSymm > 2. * pi )
                    photPhiSymm -= 2. * pi;

                  // Chernkov photons from all tracks
                  h_id = "dThetaPhiRecAllTrSymm" + RN + comb;
                  ++h_alignHistoRec2D.at( h_id )[{ photPhiSymm, dThetaSymm }];
                }

                // ------------------------- CheckRestFill -----------------------

                if ( task_CheckRestFill ) {
                  // count how many photons get into all the rest of the mirror
                  // combinations in the events with at least one photon in at least
                  // one of the two most difficult to populatable mirror combinations
                  // of RICH2 's03', 'p07' (side 0) and 'p33', 's20' (side 1)

                  // If there is no element with the prisec key, it is added and = 1
                  // otherwise -- simply incremented.
                  // The care is taken about thread-safety.
                  prisec_restCombsPopulationsInEvent[prisec] += 1.;
                }

                // ------------------------- SkipFilled --------------------------

                if ( task_SkipFilled ) {
                  prisec_totalPopulation[prisec] += 1.;

                  if ( !prisec_thisEventContributed[prisec] ) {
                    prisec_thisEventContributed[prisec] = true;
                    prisec_amountOfContributingEvents[prisec] += 1.;
                  }

                  if ( !prisec_full[prisec] ) {
                    if ( !thisEventSelected ) {
                      thisEventSelected = true;
                      amountOfSelectedEvents++;
                    }
                    prisec_populationWhenFull[prisec] += 1.;
                    if ( !prisec_thisEventContributedUntilFull[prisec] ) {
                      prisec_thisEventContributedUntilFull[prisec] = true;
                      prisec_amountOfContributingEventsUntilFull[prisec] += 1.;
                    }
                  }
                }

                // ------------------------- AllFillPhi --------------------------

                if ( task_AllFillPhi ) {
                  // fill all 1D Phi histograms
                  h_id = "PhiRecSaturTr" + RN + comb;
                  ++h_alignHistoRec1D.at( h_id )[photPhi];
                }

                // ---------------------------------------------------------------

                break; // combination is found in the list:
                       // things done -- exit from the loop

              } // this combination of the mirror segments is in the list

            } // prebookPriSecCombs loop

            // =========================== AllFillCount ==========================

            if ( task_AllFillCount ) {
              // find all combinations of mirrors that have photons from this track

              // If there is no element with this prisec key, it is added and becomes == 1.
              // otherwise -- simply incremented.
              //
              if ( mirrorSegmentPri <= 27 && mirrorSegmentSec <= 19 )
                sides_prisec_allCombsPopulations[0][prisec] += 1.;
              else if ( 28 <= mirrorSegmentPri && mirrorSegmentPri <= 55 && 20 <= mirrorSegmentSec &&
                        mirrorSegmentSec <= 39 )
                sides_prisec_allCombsPopulations[1][prisec] += 1.;
            }

            // ===================================================================

          } // valid scalars
        }   // SIMD loop
      }     // loop over photons of this track

      // *************************** MapUse MapPhiUse ****************************

      if ( ( task_MapUse || task_MapPhiUse ) && !usefulTrack ) {
        continue; // go to next track
      }

      // ******************************* MapExlore *******************************

      if ( task_MapExlore ) {
        // loop over mirror combinations from the production subset
        // to which current track contributed its photons
        for ( const auto& populated_prisec : populatedPriSecCombsSaturTr4Map ) {
          // fill control histograms for photon-contributing tracks
          if ( prisec_photCountSaturTr4Map.find( populated_prisec ) != prisec_photCountSaturTr4Map.end() ) {
            const auto& comb = populated_prisec[0] + populated_prisec[1];
            // fill the track's angular region
            h_id = "thetaPhiRecSaturTr" + RN + comb;
            // h_alignHistoRec2D.at( h_id )->fill( trPhi,           trTheta  );
            ++h_alignHistoRec2D.at( h_id )[{ trPhi, std::sqrt( trTheta ) }];
            // fill photon conters per track
            h_id = "numPhotsRecSaturTr" + RN + comb;
            ++h_alignHistoRec1D.at( h_id )[prisec_photCountSaturTr4Map.at( populated_prisec )];
          }
        }
      }

      // ******************************* MapConstruct ****************************

      if ( task_MapConstruct ) {

        // if there is something to add at all
        if ( !comb_fullnessPhotCount.empty() ) {

          // if entry for this bin is already created earlier
          if ( bin_trackCount_comb_fullnessPhotCount.find( bin ) != bin_trackCount_comb_fullnessPhotCount.end() ) {
            // prepare permission to increment the track count for this bin,
            // but only oce
            bool increment_trackCount = true;
            // now go comb-by-comb
            for ( const auto& kv : comb_fullnessPhotCount ) {
              // we update this comb's content
              // after checking that it is not full yet
              if ( !bin_trackCount_comb_fullnessPhotCount[bin].second[kv.first][0] ) {
                // since at least one useful update is found,
                // we increment the track count for this bin by 1, but only once,
                // because usefullness of this track is in the mere fact,
                // that it provides a contributuion to at least one comb, as such,
                // no matter how many
                if ( increment_trackCount ) {
                  bin_trackCount_comb_fullnessPhotCount[bin].first++;
                  increment_trackCount = false;
                }
                // increment in this bin this comb's content
                bin_trackCount_comb_fullnessPhotCount[bin].second[kv.first][1] += kv.second[1];
                // check if it became full
                if ( bin_trackCount_comb_fullnessPhotCount[bin].second[kv.first][1] >= m_poorestPopulation ) {
                  bin_trackCount_comb_fullnessPhotCount[bin].second[kv.first][0] = 1;
                }
              }
            }
          } else {
            // create new entry with track counter = 1 and
            // the entire map of this track's contributions to combs
            bin_trackCount_comb_fullnessPhotCount[bin] = std::make_pair( 1, comb_fullnessPhotCount );
            bin_trackCount_comb_fullnessPhotCount.insert( { bin, std::make_pair( 1, comb_fullnessPhotCount ) } );
          }
        }
      }

      // *************************************************************************

    } // end loop over the track containers

    // ################################# Map ###################################

    if ( ( task_Map && includeThisEvent ) ) {

      // now it is a good time to loop through the combs
      // and mark the combs that were made full
      // by the photons of this track
      // and only if this event is included

      // first update comb_status
      for ( auto& comb : combsUpdatedByUsefulTracks ) { comb_status[comb][1]++; }

      for ( auto& kv : comb_status ) {
        if ( !kv.second[0] && ( kv.second[1] >= m_poorestPopulation ) ) { kv.second[0] = 1; }
      }

      // since at least m_minUsefulTracks tracks from this event is useful,
      // we should replace the currentEventNumber (or even create a new entry)
      for ( auto& bin : usefulTracksBins ) { bin_eventNumber[bin] = currentEventNumber; }
    }

    // ################################# MapPhi ################################

    if ( task_MapPhi && includeThisEvent ) {

      // now it is a good time to loop through the combs
      // and mark the combs that were made full
      // by the photons of this track
      // and only if this event is included

      // first update comb_status_phi
      for ( auto& comb : combsUpdatedByUsefulTracks ) { comb_status_phi[comb][1]++; }

      for ( auto& kv : comb_status_phi ) {
        if ( !kv.second[0] && ( kv.second[1] >= m_poorestPopulation ) ) { kv.second[0] = 1; }
      }
      // since at least m_minUsefulTracks tracks from this event is useful,
      // all tracks' terminating event number should be updated
      // i.e. we should replace the currentEventNumber
      for ( auto& binRaw : usefulTracksBinsRaw ) { binEventNumberPhi.at( binRaw ) = currentEventNumber; }

      // for better understanding of how it works
      // we also fill the map where the key is a string = phiBin+thetaBin

      // since at least m_minUsefulTracks tracks from this event is useful,
      // all tracks' terminating event number should be updated
      // i.e. we should replace the currentEventNumber
      // or if this bin's entry does not exist, create it
      for ( auto& bin : usefulTracksBins ) { bin_eventNumberPhi[bin] = currentEventNumber; }
    }

    // ################################# Map MapPhi ##############################

    if ( task_Map || task_MapPhi ) {
      if ( includeThisEvent ) { includedEventsAmount++; }
    }

    // ################################# CheckRestFill ###########################

    if ( task_CheckRestFill ) {
      totalEventsCount += 1.;
      if ( prisec_restCombsPopulationsInEvent.find( { "p07", "s03" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p33", "s20" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p49", "s36" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p22", "s19" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p32", "s24" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p23", "s15" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p52", "s36" } ) != prisec_restCombsPopulationsInEvent.end() or
           prisec_restCombsPopulationsInEvent.find( { "p07", "s07" } ) != prisec_restCombsPopulationsInEvent.end() ) {
        selectedEventsCount += 1.;
        for ( auto& kv : prisec_restCombsPopulationsInEvent ) {
          prisec_restCombsPopulations[{ kv.first[0].c_str(), kv.first[1].c_str() }] += kv.second;
        }
      }
    }

    // ################################# SkipFilled ##############################

    if ( task_SkipFilled ) {
      overallNumberOfEvents++;

      for ( auto& kv : prisec_populationWhenFull ) {
        if ( kv.second >= m_poorestPopulation ) { prisec_full[{ kv.first[0].c_str(), kv.first[1].c_str() }] = true; }
      }
      for ( auto& kv : prisec_full ) {
        if ( !kv.second ) { prisec_numberOfEventsUntilFull[{ kv.first[0].c_str(), kv.first[1].c_str() }] += 1.; }
      }
    }

    // ###########################################################################

  } // if ( thisEventSelected )

  // end of the main execution of this event
}
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDAlignment )
//-----------------------------------------------------------------------------
