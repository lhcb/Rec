/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// STD
#include <mutex>
#include <numeric>

// STD
#include <numeric>

namespace Rich::Future::Rec::Moni {

  /** @class TrackGeometry
   *
   *  Monitors the RICH track parameters
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class TrackGeometry final : public LHCb::Algorithm::Consumer<void( const LHCb::RichTrackSegment::Vector& ), //
                                                               Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackGeometry( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments ) const override {

      auto hb_tkPathL = h_tkPathL.buffer();
      auto hb_tkEntZ  = h_tkEntZ.buffer();
      auto hb_tkExtZ  = h_tkExtZ.buffer();
      auto hb_tkEntXY = h_tkEntXY.buffer();
      auto hb_tkExtXY = h_tkExtXY.buffer();

      // loop over the segments
      for ( const auto& segment : segments ) {
        // Which RICH
        const auto rich = segment.rich();
        // fill histos
        ++hb_tkPathL[rich][segment.pathLength()];
        ++hb_tkEntZ[rich][segment.entryPoint().Z()];
        ++hb_tkExtZ[rich][segment.exitPoint().Z()];
        ++hb_tkEntXY[rich][{ segment.entryPoint().X(), segment.entryPoint().Y() }];
        ++hb_tkExtXY[rich][{ segment.exitPoint().X(), segment.exitPoint().Y() }];
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool                        ok       = true;
      const DetectorArray<double> minZ     = { 900, 9000 };
      const DetectorArray<double> maxZ     = { 2500, 12000 };
      const DetectorArray<double> maxX     = { 1000, 5000 };
      const DetectorArray<double> maxY     = { 1000, 4000 };
      const DetectorArray<double> maxPathL = { 2000, 4000 };
      // loop over active radiators
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );
        ok &= initHist( h_tkEntZ[rich], HID( "tkEntryZ", rad ), //
                        "Track Entry point Z",                  //
                        minZ[rich], maxZ[rich], nBins1D(),      //
                        "Track Entry Point Z / mm" );
        ok &= initHist( h_tkExtZ[rich], HID( "tkExitZ", rad ), //
                        "Track Exit point Z",                  //
                        minZ[rich], maxZ[rich], nBins1D(),     //
                        "Track Exit Point Z / mm" );
        ok &= initHist( h_tkPathL[rich], HID( "tkPathLength", rad ), //
                        "Track Path Length",                         //
                        0, maxPathL[rich], nBins1D(),                //
                        "Track Path Length / mm" );
        ok &= initHist( h_tkEntXY[rich], HID( "tkEntryXY", rad ), //
                        "Track Entry point (X,Y)",                //
                        -maxX[rich], maxX[rich], nBins2D(),       //
                        -maxY[rich], maxY[rich], nBins2D(),       //
                        "Track Entry Point X / mm",               //
                        "Track Entry Point Y / mm" );
        ok &= initHist( h_tkExtXY[rich], HID( "tkExitXY", rad ), //
                        "Track Exit point (X,Y)",                //
                        -maxX[rich], maxX[rich], nBins2D(),      //
                        -maxY[rich], maxY[rich], nBins2D(),      //
                        "Track Exit Point X / mm",               //
                        "Track Exit Point Y / mm" );
      }
      return StatusCode{ ok };
    }

  private:
    // histos

    mutable Hist::DetArray<Hist::H1D<>> h_tkEntZ  = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_tkExtZ  = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_tkPathL = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_tkEntXY = { {} };
    mutable Hist::DetArray<Hist::H2D<>> h_tkExtXY = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackGeometry )

} // namespace Rich::Future::Rec::Moni
