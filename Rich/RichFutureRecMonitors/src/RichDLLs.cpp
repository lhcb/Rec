/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "Gaudi/Accumulators/CounterArray.h"
#include "LHCbAlgs/Consumer.h"

// Event model
#include "Event/RichPID.h"

// format
#include <fmt/format.h>

// STL
#include <array>

namespace Rich::Future::Rec::Moni {

  /** @class DLLs RichDLLs.h
   *
   *  Basic plotting of RICH DLL values
   *
   *  @author Chris Jones
   *  @date   2020-02-20
   */

  class DLLs final : public LHCb::Algorithm::Consumer<void( const LHCb::RichPIDs& ),
                                                      Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DLLs( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // data inputs
                    KeyValue{ "RichPIDsLocation", LHCb::RichPIDLocation::Default } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // force debug messages
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichPIDs& pids ) const override {

      // local buffers
      auto withR1     = m_withR1.buffer();
      auto withR2     = m_withR2.buffer();
      auto withR1R2   = m_withR1R2.buffer();
      auto aboveThres = m_aboveThres.buffer();
      auto hb_dlls    = h_dlls.buffer();

      // count PIDs
      m_nPIDs += pids.size();

      // Fill counts
      for ( const auto pid : pids ) {
        withR1 += ( pid->usedRich1Gas() && !pid->usedRich2Gas() );
        withR2 += ( pid->usedRich2Gas() && !pid->usedRich1Gas() );
        withR1R2 += ( pid->usedRich2Gas() && pid->usedRich1Gas() );
        for ( const auto id : activeParticlesNoBT() ) { aboveThres.at( id ) += pid->isAboveThreshold( id ); }
      }

      // fill histograms
      for ( const auto pid : pids ) {
        // Fill plots for particle types
        for ( const auto id : activeParticles() ) {
          // Only fill if either X or Y in DLL(X-Y) is above threshold.
          // note by design Y is always pion
          if ( pid->isAboveThreshold( Rich::Pion ) || pid->isAboveThreshold( id ) ) {
            ++hb_dlls.at( id )[pid->particleDeltaLL( id )];
          }
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      for ( const auto id : activeParticles() ) {
        ok &= initHist( h_dlls[id], HID( "dll", id ), "DLL", -50, 50, nBins1D() );
      }
      return StatusCode{ ok };
    }

  private:
    // data

    /// Histograms for each DLL value
    mutable Hist::PartArray<Hist::H1D<>> h_dlls = { {} };

    /// PID count
    mutable Gaudi::Accumulators::StatCounter<> m_nPIDs{ this, "# PIDs" };

    /// Only using RICH1
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1{ this, "Used RICH1 only" };

    /// Only using RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR2{ this, "Used RICH2 only" };

    /// Using both RICH1 and RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1R2{ this, "Used RICH1 and RICH2" };

    /// Mass hypothesis thresholds
    mutable Gaudi::Accumulators::CounterArray<Gaudi::Accumulators::BinomialCounter<>, Rich::NRealParticleTypes>
        m_aboveThres{ this, []( const int n ) {
                       const auto types = std::array{ "El", "Mu", "Pi", "Ka", "Pr", "De" };
                       return fmt::format( "{} Above Threshold", types.at( n ) );
                     } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DLLs )

} // namespace Rich::Future::Rec::Moni
