/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// STD
#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <cstdint>
#include <memory>
#include <mutex>
#include <sstream>
#include <vector>

namespace Rich::Future::Rec::Moni {

  namespace {
    /// Inner/Outer regions
    enum InOutRegion : std::uint8_t { Inner = 0, Outer };
    inline std::string regionString( const InOutRegion reg ) noexcept { return ( Inner == reg ? "Inner" : "Outer" ); }

    /// arrays for histograms
    template <typename TYPE>
    using ColumnHists = Hist::Array<TYPE, LHCb::RichSmartID::MaPMT::MaxModuleColumnsAnyPanel>;
    template <typename TYPE>
    using InOutArray = Hist::Array<TYPE, 2, InOutRegion>;

    /// For Phi binning
    static constexpr std::size_t NPhiBins = 16;
    static constexpr double      phiInc   = ( 2.0 * M_PI / NPhiBins );
    template <typename T>
    inline auto phiBin( const T& phi ) {
      const std::size_t bin = std::floor( phi / phiInc );
      return std::min( NPhiBins - 1, bin );
    }
    inline auto binMinPhi( const std::size_t bin ) noexcept { return bin * phiInc; }
    inline auto binMaxPhi( const std::size_t bin ) noexcept { return ( bin + 1 ) * phiInc; }
    inline auto binAvgPhi( const std::size_t bin ) noexcept { return ( bin + 0.5 ) * phiInc; }
    template <typename TYPE>
    using PhiHists = Hist::Array<TYPE, NPhiBins>;
  } // namespace

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
      : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&,                 //
                                               const Summary::Track::Vector&,             //
                                               const SIMDPixelSummaries&,                 //
                                               const Relations::PhotonToParents::Vector&, //
                                               const LHCb::RichTrackSegment::Vector&,     //
                                               const CherenkovAngles::Vector&,            //
                                               const SIMDCherenkovPhoton::Vector&,        //
                                               const SIMDMirrorData::Vector& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "PhotonMirrorDataLocation", SIMDMirrorDataLocation::Default } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range&                 tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const SIMDPixelSummaries&                 pixels,        //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons,       //
                     const SIMDMirrorData::Vector&             mirrorData ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    // cached data

    // CK theta histograms
    mutable Hist::DetArray<Hist::H1D<>>                                h_thetaRec;
    mutable Hist::DetArray<Hist::H1D<>>                                h_phiRec;
    mutable Hist::DetArray<Hist::H1D<>>                                h_ckResAll;
    mutable Hist::DetArray<Hist::QuadArray<Hist::H1D<>>>               h_ckResAllPerQuad;
    mutable Hist::DetArray<Hist::PanelArray<Hist::H1D<>>>              h_ckResAllPerPanel;
    mutable Hist::DetArray<Hist::H1D<>>                                h_ckResAllBetaCut;
    mutable Hist::DetArray<Hist::H1D<>>                                h_ckResAllDiffSide;
    mutable Hist::DetArray<Hist::PanelArray<Hist::H1D<>>>              h_ckResAllPerPanelBetaCut;
    mutable InOutArray<Hist::H1D<>>                                    h_ckResAllPerRegion;
    mutable Hist::PanelArray<InOutArray<Hist::H1D<>>>                  h_ckResAllPerPanelPerRegion;
    mutable Hist::DetArray<Hist::PanelArray<ColumnHists<Hist::H1D<>>>> h_ckResAllPerCol;
    mutable Hist::PanelArray<ColumnHists<InOutArray<Hist::H1D<>>>>     h_ckResAllPerColPerRegion;
    mutable Hist::DetArray<Hist::H2D<>>                                h_ckResAllVersusPhi;
    mutable Hist::DetArray<Hist::QuadArray<Hist::H2D<>>>               h_ckResAllVersusPhiPerQuad;

    // Phi region histograms
    struct PhiBinHists {
      Hist::DetArray<PhiHists<Hist::H1D<>>>                  ckResAllPhi;
      Hist::DetArray<Hist::QuadArray<PhiHists<Hist::H1D<>>>> ckResAllPhiPerQuad;
    };
    mutable std::unique_ptr<PhiBinHists> h_phiBinHists;

    // Types for mirror specific histograms
    // Intentionally wrap as a unique pointer and only allocated when we know they are possible
    // during the event loop, as filling these requires the additional (optional) mirror data from
    // the photon reconstruction. Use atomicity::none as we handle thread locking ourselves here.
    template <std::size_t NMIRRORS, Gaudi::Accumulators::atomicity ATOMICITY = Gaudi::Accumulators::atomicity::none>
    struct MirrorHists {
      template <typename TYPE>
      using HistArray = Hist::DetArray<Hist::PanelArray<Hist::Array<TYPE, NMIRRORS>>>;
      HistArray<Hist::H1D<Hist::DefaultArithmeticType, ATOMICITY>> thetaRec;
      HistArray<Hist::H1D<Hist::DefaultArithmeticType, ATOMICITY>> phiRec;
      HistArray<Hist::H1D<Hist::DefaultArithmeticType, ATOMICITY>> ckResAll;
      HistArray<Hist::H2D<Hist::DefaultArithmeticType, ATOMICITY>> ckResAllVersusPhi;
    };
    using PrimMirrorHists = MirrorHists<56u>;
    using SecMirrorHists  = MirrorHists<40u>;
    mutable std::unique_ptr<PrimMirrorHists> h_primMirrHists;
    mutable std::unique_ptr<SecMirrorHists>  h_secMirrHists;
    mutable std::mutex                       m_mirrorLock; ///< Manuyally handle locking for mirror hists

  private:
    // properties and tools

    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.010f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.056f, 0.033f } };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<DetectorArray<float>> m_ckResRange{ this, "CKResHistoRange", { 0.0026f, 0.002f } };

    /// Enable Detailed Histograms
    Gaudi::Property<bool> m_detailedHists{ this, "DetailedHistograms", false };

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{ this, "TrackSelector", "TrackSelector" };

  private:
    void createMirrorHistArrays() const {
      // As both primary and secondary hists are allocated at the same time only
      // need to check for one of them.
      if ( !h_primMirrHists.get() ) {
        std::scoped_lock lock( m_mirrorLock );
        if ( !h_primMirrHists.get() ) {
          h_primMirrHists = std::make_unique<PrimMirrorHists>();
          h_secMirrHists  = std::make_unique<SecMirrorHists>();
        }
      }
    }
    void initPrimMirrorHists( const Rich::DetectorType rich, const Rich::Side side, const std::size_t mirrN ) const {
      assert( h_primMirrHists.get() );
      auto& mirrHists = ( *h_primMirrHists.get() );
      if ( !mirrHists.ckResAll[rich][side].at( mirrN ).has_value() ) {
        const auto MirrS = std::to_string( mirrN );
        const auto rad   = radType( rich );
        bool       ok    = true;
        ok &= initHist( mirrHists.thetaRec[rich][side].at( mirrN ),
                        HID( "Mirrors/Primary/Mirror" + MirrS + "/thetaRec", rad ),      //
                        "Reconstructed CKTheta - All photons - Primary Mirror " + MirrS, //
                        m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),               //
                        "Cherenkov Theta / rad", "Entries" );
        ok &= initHist( mirrHists.phiRec[rich][side].at( mirrN ),                      //
                        HID( "Mirrors/Primary/Mirror" + MirrS + "/phiRec", rad ),      //
                        "Reconstructed CKPhi - All photons - Primary Mirror " + MirrS, //
                        0.0, 2.0 * Gaudi::Units::pi, nBins1D(),                        //
                        "Cherenkov Phi / rad", "Entries" );
        ok &= initHist( mirrHists.ckResAll[rich][side].at( mirrN ),                 //
                        HID( "Mirrors/Primary/Mirror" + MirrS + "/ckResAll", rad ), //
                        "Rec-Exp CKTheta - All photons - Primary Mirror " + MirrS,  //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                        "delta(Cherenkov theta) / rad", "Entries" );
        ok &= initHist( mirrHists.ckResAllVersusPhi[rich][side].at( mirrN ),                    //
                        HID( "Mirrors/Primary/Mirror" + MirrS + "/ckResAllVersusPhi", rad ),    //
                        "Rec-Exp CKTheta Versus CKPhi - All photons - Primary Mirror " + MirrS, //
                        0, 2.0 * M_PI, nBins2D(),                                               //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins2D(),                     //
                        "Cherenkov phi / rad", "delta(Cherenkov theta) / rad", "Entries" );
        if ( !ok ) { throw std::runtime_error( "Failed to initialise primary mirror histograms" ); }
      }
    }

    void initSecMirrorHists( const Rich::DetectorType rich, const Rich::Side side, const std::size_t mirrN ) const {
      assert( h_secMirrHists.get() );
      auto& mirrHists = ( *h_secMirrHists.get() );
      if ( !mirrHists.ckResAll[rich][side].at( mirrN ).has_value() ) {
        const auto MirrS = std::to_string( mirrN );
        const auto rad   = radType( rich );
        bool       ok    = true;
        ok &= initHist( mirrHists.thetaRec[rich][side].at( mirrN ),
                        HID( "Mirrors/Secondary/Mirror" + MirrS + "/thetaRec", rad ),    //
                        "Reconstructed CKTheta - All photons - Primary Mirror " + MirrS, //
                        m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),               //
                        "Cherenkov Theta / rad", "Entries" );
        ok &= initHist( mirrHists.phiRec[rich][side].at( mirrN ),                      //
                        HID( "Mirrors/Secondary/Mirror" + MirrS + "/phiRec", rad ),    //
                        "Reconstructed CKPhi - All photons - Primary Mirror " + MirrS, //
                        0.0, 2.0 * Gaudi::Units::pi, nBins1D(),                        //
                        "Cherenkov Phi / rad", "Entries" );
        ok &= initHist( mirrHists.ckResAll[rich][side].at( mirrN ),                   //
                        HID( "Mirrors/Secondary/Mirror" + MirrS + "/ckResAll", rad ), //
                        "Rec-Exp CKTheta - All photons - Secondary Mirror " + MirrS,  //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),           //
                        "delta(Cherenkov theta) / rad", "Entries" );
        ok &= initHist( mirrHists.ckResAllVersusPhi[rich][side].at( mirrN ),                      //
                        HID( "Mirrors/Secondary/Mirror" + MirrS + "/ckResAllVersusPhi", rad ),    //
                        "Rec-Exp CKTheta Versus CKPhi - All photons - Secondary Mirror " + MirrS, //
                        0, 2.0 * M_PI, nBins2D(),                                                 //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins2D(),                       //
                        "Cherenkov phi / rad", "delta(Cherenkov theta) / rad", "Entries" );
        if ( !ok ) { throw std::runtime_error( "Failed to initialise secondary mirror histograms" ); }
      }
    }
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  if ( m_detailedHists ) { h_phiBinHists = std::make_unique<PhiBinHists>(); }

  // Loop over RICHes
  for ( const auto rich : activeDetectors() ) {
    const auto rad = radType( rich );

    // beta cut string
    std::string betaS = "beta";
    if ( m_minBeta[rich] > 0.0 ) { betaS = std::to_string( m_minBeta[rich] ) + "<" + betaS; }
    if ( m_maxBeta[rich] < 1.0 ) { betaS = betaS + "<" + std::to_string( m_maxBeta[rich] ); }

    // inclusive plots
    ok &= initHist( h_thetaRec[rich], HID( "thetaRec", rad ),
                    "Reconstructed CKTheta - All photons",             //
                    m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), //
                    "Cherenkov Theta / rad", "Entries" );
    ok &= initHist( h_phiRec[rich], HID( "phiRec", rad ),
                    "Reconstructed CKPhi - All photons",    //
                    0.0, 2.0 * Gaudi::Units::pi, nBins1D(), //
                    "Cherenkov Phi / rad", "Entries" );
    ok &= initHist( h_ckResAll[rich], HID( "ckResAll", rad ),
                    "Rec-Exp CKTheta - All photons",                    //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov theta) / rad", "Entries" );
    ok &= initHist( h_ckResAllDiffSide[rich], HID( "ckResAllDiffSide", rad ),
                    "Rec-Exp CKTheta - All photons - Cross Sides",      //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov theta) / rad", "Entries" );
    // with beta cut
    ok &= initHist( h_ckResAllBetaCut[rich], HID( "ckResAllBetaCut", rad ), //
                    "Rec-Exp CKTheta - All photons - " + betaS,             //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),     //
                    "delta(Cherenkov theta) / rad", "Entries" );

    // 2D theta versus phi
    ok &= initHist( h_ckResAllVersusPhi[rich], HID( "ckResAllVersusPhi", rad ), //
                    "Rec-Exp CKTheta Versus CKPhi - All photons",               //
                    0, 2.0 * M_PI, nBins2D(),                                   //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins2D(),         //
                    "Cherenkov phi / rad", "delta(Cherenkov theta) / rad", "Entries" );

    // Quadrants
    for ( const auto q : Rich::Utils::quadrants() ) {
      ok &= initHist( h_ckResAllPerQuad[rich][q],                                     //
                      HID( "Quadrants/Q" + std::to_string( q ) + "/ckResAll", rad ),  //
                      "Rec-Exp CKTheta - All photons" + Rich::Utils::quadString( q ), //
                      -m_ckResRange[rich], m_ckResRange[rich], nBins1D() );
      ok &= initHist( h_ckResAllVersusPhiPerQuad[rich][q],
                      HID( "Quadrants/Q" + std::to_string( q ) + "/ckResAllVersusPhi", rad ),      //
                      "Rec-Exp CKTheta Versus CKPhi - All photons" + Rich::Utils::quadString( q ), //
                      0, 2.0 * M_PI, nBins2D(),                                                    //
                      -m_ckResRange[rich], m_ckResRange[rich], nBins2D(),                          //
                      "Cherenkov phi / rad", "delta(Cherenkov theta) / rad", "Entries" );
    }

    if ( m_detailedHists ) {
      // Phi Bins
      for ( std::size_t bin = 0; bin < h_phiBinHists->ckResAllPhi[rich].size(); ++bin ) {
        const auto binS   = std::to_string( bin );
        const auto binMin = binMinPhi( bin );
        const auto binMax = binMaxPhi( bin );
        ok &= ( phiBin( binAvgPhi( bin ) ) == bin );
        ok &= ( binMin <= binAvgPhi( bin ) && binAvgPhi( bin ) <= binMax );
        ok &= initHist( h_phiBinHists->ckResAllPhi[rich][bin], HID( "PhiBins/Bin" + binS + "/ckResAll", rad ),
                        "Rec-Exp CKTheta - PhiBin " + binS + " " + std::to_string( binMin ) + "->" +
                            std::to_string( binMax ) + " - All photons",
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), "delta(Cherenkov theta) / rad", "Entries" );
        for ( const auto q : Rich::Utils::quadrants() ) {
          ok &=
              initHist( h_phiBinHists->ckResAllPhiPerQuad[rich][q][bin],                                       //
                        HID( "Quadrants/Q" + std::to_string( q ) + "/PhiBins/Bin" + binS + "/ckResAll", rad ), //
                        "Rec-Exp CKTheta - PhiBin " + binS + " " + std::to_string( binMin ) + "->" +
                            std::to_string( binMax ) + " - All photons" + Rich::Utils::quadString( q ),
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), "delta(Cherenkov theta) / rad", "Entries" );
        }
      }
    }

    // loop over detector sides
    for ( const auto side : Rich::sides() ) {
      ok &= initHist( h_ckResAllPerPanel[rich][side], HID( "ckResAllPerPanel", side, rad ), //
                      "Rec-Exp CKTheta - All photons",                                      //
                      -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),                   //
                      "delta(Cherenkov theta) / rad", "Entries" );
      // beta cut
      ok &= initHist( h_ckResAllPerPanelBetaCut[rich][side],              //
                      HID( "ckResAllPerPanelBetaCut", side, rad ),        //
                      "Rec-Exp CKTheta - All photons - " + betaS,         //
                      -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                      "delta(Cherenkov theta) / rad", "Entries" );
      // Per Column resolutions
      for ( std::size_t iCol = 0; iCol < LHCb::RichSmartID::MaPMT::ModuleColumnsPerPanel[rich]; ++iCol ) {
        const auto sCol = std::to_string( iCol );
        ok &= initHist( h_ckResAllPerCol[rich][side].at( iCol ),              //
                        HID( "Columns/Col" + sCol + "/ckResAll", side, rad ), //
                        "Rec-Exp CKTheta - All photons - Column " + sCol,     //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),   //
                        "delta(Cherenkov theta) / rad", "Entries" );
      }
    }

    // Make plots for inner and outer regions for RICH2 only (for now, might change in U2)
    if ( rich == Rich::Rich2 ) {
      for ( const auto reg : { Inner, Outer } ) {
        const auto regS = "Region/" + regionString( reg );
        ok &= initHist( h_ckResAllPerRegion[reg], HID( regS + "/ckResAll", rad ), //
                        "Rec-Exp CKTheta - All photons - " + regS + " Region",    //
                        -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),       //
                        "delta(Cherenkov theta) / rad", "Entries" );
        for ( const auto side : Rich::sides() ) {
          ok &= initHist( h_ckResAllPerPanelPerRegion[side][reg], HID( regS + "/ckResAllPerPanel", side, rad ), //
                          "Rec-Exp CKTheta - All photons - " + regS + " Region",                                //
                          -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),                                   //
                          "delta(Cherenkov theta) / rad", "Entries" );
          for ( std::size_t iCol = 0; iCol < LHCb::RichSmartID::MaPMT::ModuleColumnsPerPanel[rich]; ++iCol ) {
            const auto sCol = std::to_string( iCol );
            ok &= initHist( h_ckResAllPerColPerRegion[side].at( iCol )[reg],                             //
                            HID( regS + "/Columns/Col" + sCol + "/ckResAll", side, rad ),                //
                            "Rec-Exp CKTheta - All photons - Column " + sCol + " - " + regS + " Region", //
                            -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),                          //
                            "delta(Cherenkov theta) / rad", "Entries" );
          }
        }
      }
    }

  } // active detector loop

  return StatusCode{ ok };
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const LHCb::Track::Range&                 tracks,        //
                                            const Summary::Track::Vector&             sumTracks,     //
                                            const SIMDPixelSummaries&                 pixels,        //
                                            const Relations::PhotonToParents::Vector& photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&     segments,      //
                                            const CherenkovAngles::Vector&            expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&        photons,       //
                                            const SIMDMirrorData::Vector&             mirrorData ) const {

  const bool hasMirrorData = ( !mirrorData.empty() && mirrorData.size() == photons.size() );
  if ( m_detailedHists && hasMirrorData ) { createMirrorHistArrays(); }

  // local histo buffers
  auto hb_thetaRec                  = h_thetaRec.buffer();
  auto hb_phiRec                    = h_phiRec.buffer();
  auto hb_ckResAll                  = h_ckResAll.buffer();
  auto hb_ckResAllPerPanel          = h_ckResAllPerPanel.buffer();
  auto hb_ckResAllPerRegion         = h_ckResAllPerRegion.buffer();
  auto hb_ckResAllPerPanelPerRegion = h_ckResAllPerPanelPerRegion.buffer();
  auto hb_ckResAllBetaCut           = h_ckResAllBetaCut.buffer();
  auto hb_ckResAllPerPanelBetaCut   = h_ckResAllPerPanelBetaCut.buffer();
  auto hb_ckResAllPerCol            = h_ckResAllPerCol.buffer();
  auto hb_ckResAllPerColPerRegion   = h_ckResAllPerColPerRegion.buffer();
  auto hb_ckResAllVersusPhi         = h_ckResAllVersusPhi.buffer();
  auto hb_ckResAllPerQuad           = h_ckResAllPerQuad.buffer();
  auto hb_ckResAllVersusPhiPerQuad  = h_ckResAllVersusPhiPerQuad.buffer();
  auto hb_ckResAllDiffSide          = h_ckResAllDiffSide.buffer();

  // loop over the track containers
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // Is this track selected ?
    if ( !m_tkSel.get()->accept( *tk ) ) { continue; }

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];
      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // RICH info
      const auto rich = seg.rich();
      if ( !richIsActive( rich ) ) { continue; }

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto pid = Rich::Pion;

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      const auto betaC = ( beta >= m_minBeta[rich] && beta <= m_maxBeta[rich] );

      // expected CK theta
      const auto thetaExp = expCKangles[pid];

      // Quadrant
      const auto q = Rich::Utils::quadrant( seg.bestPoint().x(), seg.bestPoint().y() );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( phot.validityMask()[i] ) {

          // reconstructed theta
          const auto thetaRec = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec = phot.CherenkovPhi()[i];
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // SmartID
          const auto id = phot.smartID()[i];

          // Detctor side
          const auto side = id.panel();

          // Inner or outer region ?
          const auto region = ( simdPix.isInnerRegion()[i] ? Inner : Outer );

          // Segment and photon in same RICH side ?
          const bool sameSide = ( Rich::Rich1 == rich ? seg.bestPoint().y() * simdPix.gloPos().y()[i] > 0.0
                                                      : seg.bestPoint().x() * simdPix.gloPos().x()[i] > 0.0 );

          // fill some general plots
          ++hb_thetaRec[rich][thetaRec];
          ++hb_phiRec[rich][phiRec];
          ++hb_ckResAll[rich][deltaTheta];
          if ( !sameSide ) { ++hb_ckResAllDiffSide[rich][deltaTheta]; }
          ++hb_ckResAllPerQuad[rich][q][deltaTheta];
          ++hb_ckResAllVersusPhi[rich][{ phiRec, deltaTheta }];
          ++hb_ckResAllVersusPhiPerQuad[rich][q][{ phiRec, deltaTheta }];
          if ( m_detailedHists ) {
            const auto phiB = phiBin( phiRec );
            ++( h_phiBinHists->ckResAllPhi[rich].at( phiB )[deltaTheta] );
            ++( h_phiBinHists->ckResAllPhiPerQuad[rich][q].at( phiB )[deltaTheta] );
          }
          ++hb_ckResAllPerPanel[rich][side][deltaTheta];
          if ( rich == Rich::Rich2 ) {
            ++hb_ckResAllPerRegion[region][deltaTheta];
            ++hb_ckResAllPerPanelPerRegion[side][region][deltaTheta];
          }
          if ( betaC ) {
            ++hb_ckResAllBetaCut[rich][deltaTheta];
            ++hb_ckResAllPerPanelBetaCut[rich][side][deltaTheta];
          }

          // Mirror plots
          if ( m_detailedHists && hasMirrorData ) {
            // Manually thread locking for these hists...
            std::scoped_lock  lock( m_mirrorLock );
            const auto&       mirrordata = mirrorData[photIn];
            const std::size_t primMirrN  = mirrordata.primaryMirrors()[i]->mirrorNumber();
            const std::size_t secMirrN   = mirrordata.secondaryMirrors()[i]->mirrorNumber();
            initPrimMirrorHists( rich, side, primMirrN );
            initSecMirrorHists( rich, side, secMirrN );
            ++h_primMirrHists->thetaRec[rich][side][primMirrN][thetaRec];
            ++h_secMirrHists->thetaRec[rich][side][secMirrN][thetaRec];
            ++h_primMirrHists->phiRec[rich][side][primMirrN][phiRec];
            ++h_secMirrHists->phiRec[rich][side][secMirrN][phiRec];
            ++h_primMirrHists->ckResAll[rich][side][primMirrN][deltaTheta];
            ++h_secMirrHists->ckResAll[rich][side][secMirrN][deltaTheta];
            ++h_primMirrHists->ckResAllVersusPhi[rich][side][primMirrN][{ phiRec, deltaTheta }];
            ++h_secMirrHists->ckResAllVersusPhi[rich][side][secMirrN][{ phiRec, deltaTheta }];
          }

          // FIXME
          // Note very old MC samples have a different number of columns.
          // Handle here by just silently rejecting those out-of-range.
          // Once support for these old samples is dropped this can be
          // changed to a full assert check.
          const std::size_t iCol = id.panelLocalModuleColumn();
          if ( iCol < hb_ckResAllPerCol[rich][side].size() ) {
            ++hb_ckResAllPerCol[rich][side][iCol][deltaTheta];
            if ( rich == Rich::Rich2 ) { ++hb_ckResAllPerColPerRegion[side][iCol][region][deltaTheta]; }
          }

        } // valid scalars
      }   // SIMD loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
