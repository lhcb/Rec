/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichFutureRecBase/RichRecHistoAlgBase.h"
#include "RichRecUtils/RichDetParams.h"
#include "RichUtils/RichTrackSegment.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "LHCbAlgs/Consumer.h"

#include <algorithm>
#include <cmath>
#include <mutex>

namespace Rich::Future::Rec::Moni {

  /** @class TrackRadiatorMaterial RichTrackRadiatorMaterial.h
   *
   *  Monitors the material traversed by RICH radiator segments
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class TrackRadiatorMaterial final
      : public LHCb::Algorithm::Consumer<
            void( LHCb::RichTrackSegment::Vector const&, DetectorElement const& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<HistoAlgBase, DetectorElement>> {

  public:
    /// Standard constructor
    TrackRadiatorMaterial( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, //
                    { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } } ) {
      // change some defaults
      setProperty( "NBins1DHistos", 200 ).ignore();
      setProperty( "NBins2DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( LHCb::RichTrackSegment::Vector const& segments, //
                     DetectorElement const&                geometry ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
#ifndef USE_DD4HEP
    /// Transport Service
    ServiceHandle<ITransportSvc> m_transSvc{ this, "TransportSvc", "TransportSvc" };
#endif

    /// radiation length computation error
    mutable WarningCounter m_radLwarn{ this, "Problem computing radiation length" };

  private:
    // cached data

    mutable Hist::DetArray<Hist::H1D<>> h_EffL        = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_EffLOvPathL = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_PathL       = { {} };
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode TrackRadiatorMaterial::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rich : activeDetectors() ) {
    const auto rad = radType( rich );

    // min/max path length
    const auto minL = ( Rich::Rich2Gas != rad ? 1000.0 : 1400.0 );
    const auto maxL = ( Rich::Rich2Gas != rad ? 1260.0 : 3100.0 );
    // min/max effective length
    const auto minEfL = ( Rich::Rich2Gas != rad ? 0.02 : 0.01 );
    const auto maxEfL = ( Rich::Rich2Gas != rad ? 0.1 : 0.2 );
    // min/max effective length / unit pathlength
    const auto minEfLOvPL = ( Rich::Rich2Gas != rad ? 2.0e-5 : 1.0e-5 );
    const auto maxEfLOvPL = ( Rich::Rich2Gas != rad ? 9.0e-5 : 1.0e-4 );

    ok &= initHist( h_EffL[rich], HID( "EffL", rad ), "log10( Effective length )", //
                    std::log10( minEfL ), std::log10( maxEfL ), nBins1D() );

    ok &= initHist( h_EffLOvPathL[rich], HID( "EffLOvPathL", rad ), "log10( Effective length / unit pathlength )", //
                    std::log10( minEfLOvPL ), std::log10( maxEfLOvPL ), nBins1D() );

    ok &= initHist( h_PathL[rich], HID( "PathL", rad ), "Track pathlength", minL, maxL, nBins1D() );
  }

  return StatusCode{ ok };
}

//-----------------------------------------------------------------------------

void TrackRadiatorMaterial::operator()( LHCb::RichTrackSegment::Vector const& segments, //
                                        DetectorElement const&
#ifndef USE_DD4HEP
                                            geometry
#endif
) const {

#ifndef USE_DD4HEP
  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();
#endif

  // local buffers
  auto hb_EffL        = h_EffL.buffer();
  auto hb_EffLOvPathL = h_EffLOvPathL.buffer();
  auto hb_PathL       = h_PathL.buffer();

  // loop over segments
  for ( const auto& seg : segments ) {

    // which rich
    const auto rich = seg.rich();

    // radiator points
    // move the entry/exit points by +-10mm to move away from mirror structures etc.
    const auto entP = seg.entryPoint() + Gaudi::XYZVector( 0, 0, 10 );
    const auto extP = seg.exitPoint() + Gaudi::XYZVector( 0, 0, -10 );

    // path length (using the exact points used below)
    const auto length = std::sqrt( ( extP - entP ).mag2() );

    if ( // select segments away from edges etc.
        ( Rich::Rich1 == rich && ( extP.rho() < 70 || entP.rho() < 70 ) ) ||
        ( Rich::Rich2 == rich && ( extP.rho() < 300 || entP.rho() < 300 || //
                                   fabs( entP.x() ) > 2200 || fabs( extP.x() ) > 3200 ) )
        // Minimum path length (10mm)
        || length < 10 ) {
      continue;
    }

    // get the radiation length
    try {
      // Which RICH
      const auto rich = seg.rich();
      // get material length from TS
      const auto effL = std::max( 1e-4,
      // Transport service currently crashes in DD4HEP builds so disable call for now
      // https://gitlab.cern.ch/lhcb/Rec/-/issues/326
#ifdef USE_DD4HEP
                                  0.0
#else
                                  m_transSvc->distanceInRadUnits( entP, extP, tsCache, geometry, 0, nullptr )
#endif
      );
      const auto effLOvL = ( length > 0 ? effL / length : 1e-7 );
      // fill plots
      ++hb_EffL[rich][std::log10( effL )];
      ++hb_EffLOvPathL[rich][std::log10( effLOvL )];
      ++hb_PathL[rich][length];
    } catch ( const TransportSvcException& excpt ) {
      ++m_radLwarn;
      _ri_debug << excpt.message() << endmsg;
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackRadiatorMaterial )

//-----------------------------------------------------------------------------
