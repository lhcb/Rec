/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <algorithm>

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rich Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichPixelCluster.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

namespace Rich::Future::Rec {

  /** @class PixelClusterGlobalPositions
   *
   *  Computes the global space points for the given pixel clusters.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class PixelClusterGlobalPositions final
      : public LHCb::Algorithm::Transformer<
            SpacePointVector( const Rich::PDPixelCluster::Vector&, //
                              const Rich::Utils::RichSmartIDs& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    PixelClusterGlobalPositions( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       { KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                         // input conditions data
                         KeyValue{ "RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey } },
                       // output data
                       { KeyValue{ "RichPixelGlobalPositionsLocation", SpacePointLocation::PixelsGlobal } } ) {
      // setProperty( "OutputLevel", MSG::DEBUG );
    }

    /// Initialize
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
      } );
    }

    /// Algorithm execution via transform
    SpacePointVector operator()( const Rich::PDPixelCluster::Vector& clusters,
                                 const Rich::Utils::RichSmartIDs&    smartIDsHelper ) const override {
      return smartIDsHelper.globalPositions( clusters, m_noClustering );
    }

  private:
    /// Shortcut in case clustering is disabled
    Gaudi::Property<bool> m_noClustering{ this, "NoClustering", false };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PixelClusterGlobalPositions )

} // namespace Rich::Future::Rec
