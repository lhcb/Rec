/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/Track.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// MC Relations
#include "RichFutureMCUtils/RichMCHitUtils.h"
#include "RichFutureMCUtils/RichMCOpticalPhotonUtils.h"
#include "RichFutureMCUtils/RichRecMCHelper.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// ROOT
#include <TF1.h>

// STD
#include <cmath>
#include <optional>
#include <string>

namespace Rich::Future::Rec::MC {

  namespace {
    using Data = SIMDCherenkovPhoton::Vector;
  }

  /** @class RichPhotonUseMCInfo RichPhotonUseMCInfo.h
   *
   *  Use MC truth to cheat RICH Cherenkov photons
   *
   *  @author Chris Jones
   *  @date   2023-09-20
   */

  class RichPhotonUseMCInfo final
      : public LHCb::Algorithm::Transformer<Data( const Data&,                                     //
                                                  const SIMDPixelSummaries&,                       //
                                                  const LHCb::Track::Range&,                       //
                                                  const CherenkovAngles::Vector&,                  //
                                                  const Relations::PhotonToParents::Vector&,       //
                                                  const Relations::SegmentToTrackVector&,          //
                                                  const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                  const LHCb::MCRichDigitSummarys&,                //
                                                  const LHCb::MCRichHits&,                         //
                                                  const LHCb::MCRichOpticalPhotons& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    RichPhotonUseMCInfo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       { KeyValue{ "InCherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                         KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                         KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                         KeyValue{ "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                         KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                         KeyValue{ "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default },
                         KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                         KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default },
                         KeyValue{ "MCRichHitsLocation", LHCb::MCRichHitLocation::Default },
                         KeyValue{ "MCRichOpticalPhotonsLocation", LHCb::MCRichOpticalPhotonLocation::Default } },
                       // output data
                       { KeyValue{ "OutCherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default + "Out" } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        if ( !m_useMcPhotCKT ) {
          for ( const auto rich : activeDetectors() ) {
            info() << rich << " Inner region CK theta resolution func: " << m_ckThetaSmearVPInner[rich] << endmsg;
            info() << rich << " Outer region CK theta resolution func: " << m_ckThetaSmearVPOuter[rich] << endmsg;
            const auto rS = Rich::text( rich );
            m_ckVPInner[rich].emplace( ( "Inner" + rS ).c_str(), m_ckThetaSmearVPInner[rich].c_str(), 0.0, 120000.0 );
            m_ckVPOuter[rich].emplace( ( "Outer" + rS ).c_str(), m_ckThetaSmearVPOuter[rich].c_str(), 0.0, 120000.0 );
          }
          m_unitGauss = Rndm::Numbers( randSvc(), Rndm::Gauss( 0.0, 1.0 ) );
        }
      } );
    }

    /// Functional operator
    Data operator()( const Data&                                     photons,    //
                     const SIMDPixelSummaries&                       pixels,     //
                     const LHCb::Track::Range&                       tracks,     //
                     const CherenkovAngles::Vector&                  ckAngles,   //
                     const Relations::PhotonToParents::Vector&       photRels,   //
                     const Relations::SegmentToTrackVector&          segToTkRel, //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkmcrels,   //
                     const LHCb::MCRichDigitSummarys&                digitSums,  //
                     const LHCb::MCRichHits&                         mchits,     //
                     const LHCb::MCRichOpticalPhotons&               mcphotons ) const override {

      // Make local MC helper objects
      const Helper               mcHelper( tkmcrels, digitSums );
      const MCHitUtils           hitHelper( mchits );
      const MCOpticalPhotonUtils photHelper( mcphotons );

      // Clone input photons before modifying them
      auto new_photons = photons;

      // Loop over new photons to update them using MC
      for ( auto&& [phot, rels] : Ranges::Zip( new_photons, photRels ) ) {

        // Get the track for this photon
        const auto  tkIn  = segToTkRel[rels.segmentIndex()];
        const auto* track = tracks[tkIn];
        // Get pixel for this photon
        const auto& pixel = pixels[rels.pixelIndex()];

        // Get the MCParticle(s) for this track
        const auto mcPs = mcHelper.mcParticles( *track, false, 0.75 );
        if ( mcPs.empty() ) { continue; }
        const auto mcP = mcPs.front();

        // Get true PID type from first MCParticle
        const auto pid = mcHelper.mcParticleType( mcP );
        if ( Rich::Unknown == pid ) { continue; }

        // Loop over scalar entries in SIMD photon
        for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
          // Select valid entries
          if ( !phot.validityMask()[i] ) { continue; }

          // ID for this hit
          const auto id = pixel.smartID()[i];

          // get the MCHits for this ID
          const auto mcHits = hitHelper.mcRichHits( id );
          // ... and then the MC Optical Photons also linked to this track's MCParticle(s)
          const auto mcPhots = photHelper.mcOpticalPhotons( mcHits, mcPs );
          _ri_verbo << "Found " << mcPhots.size() << " MC photons" << endmsg;
          if ( !mcPhots.empty() ) {
            // This is a true CK photon so update the photon angles
            if ( m_useMcPhotCKT ) {
              // Use Cherenkov angles from first MCPhoton. Should really only be 1
              const auto mcPhot = mcPhots.front();
              if ( mcPhot ) { // should never fail but just in case ...
                phot.CherenkovTheta()[i] = mcPhot->cherenkovTheta();
                phot.CherenkovPhi()[i]   = mcPhot->cherenkovPhi();
                _ri_verbo << " -> Updated from MCPhoton " << phot.CherenkovTheta()[i] << " " << phot.CherenkovPhi()[i]
                          << endmsg;
              }
            } else {
              // Use expected values for true PID type with smearing
              const auto ckTheta = ckAngles[rels.segmentIndex()][pid];
              // resolution for this momentum
              const auto ptot = mcP->p();
              // get the emulated CK theta res for this pixel, based on its detector region.
              const auto ckRes =
                  ( pixel.isInnerRegion()[i] ? m_ckVPInner[phot.rich()] : m_ckVPOuter[phot.rich()] )->Eval( ptot );
              // New CK theta value with smearing
              const auto smeared_ckT = ckTheta + ( m_unitGauss.shoot() * ckRes );
              // update the value in the photon
              _ri_verbo << " -> Updated from TK expected: " << phot.rich() << " theta=" << ckTheta << " Ptot=" << ptot
                        << " CKRes=" << ckRes << " | New CKtheta=" << smeared_ckT << endmsg;
              phot.CherenkovTheta()[i] = smeared_ckT;
            }
          }
        }
      }

      return new_photons;
    }

  private:
    // data

    /// Unit Gaussian for CK theta smearing
    Rndm::Numbers m_unitGauss{};

    // TF1 functions for CK theta smear value as a function of P (Inner)
    DetectorArray<std::optional<TF1>> m_ckVPInner{ {} };

    // TF1 functions for CK theta smear value as a function of P (Outer)
    DetectorArray<std::optional<TF1>> m_ckVPOuter{ {} };

  private:
    // properties

    /// Use true MC optical photon values
    Gaudi::Property<bool> m_useMcPhotCKT{ this, "UseMCPhotonCKThetaValues", false };

    /// CK theta smearing function versus P for each RICH for the inner regions
    Gaudi::Property<DetectorArray<std::string>> m_ckThetaSmearVPInner{
        this,
        "CKThetaSmearFuncInner",
        { "0.00078+0.0012*(std::tanh(-x/5000.0)+1.0)", "0.00065+0.0011*(std::tanh(-x/5000.0)+1.0)" } };

    /// CK theta smearing function versus P for each RICH for the outer regions
    Gaudi::Property<DetectorArray<std::string>> m_ckThetaSmearVPOuter{
        this,
        "CKThetaSmearFuncOuter",
        { "0.00078+0.0012*(std::tanh(-x/5000.0)+1.0)", "0.00065+0.0011*(std::tanh(-x/5000.0)+1.0)" } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RichPhotonUseMCInfo )

} // namespace Rich::Future::Rec::MC
