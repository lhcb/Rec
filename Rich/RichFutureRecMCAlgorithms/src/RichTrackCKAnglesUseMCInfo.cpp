/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/MCParticle.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/Track.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// MC Relations
#include "RichFutureMCUtils/TrackToMCParticle.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// STD
#include <cmath>
#include <unordered_map>

namespace Rich::Future::Rec::MC {

  namespace {
    using Data = CherenkovAngles::Vector;
  }

  /** @class RichTrackCKAnglesUseMCInfo RichTrackCKAnglesUseMCInfo.h
   *
   *  Use MC truth to cheat RICH track Cherenkov Angles
   *
   *  @author Chris Jones
   *  @date   2023-09-20
   */

  class RichTrackCKAnglesUseMCInfo final
      : public LHCb::Algorithm::Transformer<Data( const Data&,                                     //
                                                  const LHCb::Track::Range&,                       //
                                                  const LHCb::RichTrackSegment::Vector&,           //
                                                  const Relations::SegmentToTrackVector&,          //
                                                  const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                  const LHCb::MCRichTracks& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    RichTrackCKAnglesUseMCInfo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       { KeyValue{ "InCherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                         KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                         KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default },
                         KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                         KeyValue{ "MCRichTracksLocation", LHCb::MCRichTrackLocation::Default } },
                       // output data
                       { KeyValue{ "OutCherenkovAnglesLocation", CherenkovAnglesLocation::Signal + "Out" } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    Data operator()( const Data&                                     in_angles,  //
                     const LHCb::Track::Range&                       tracks,     //
                     const LHCb::RichTrackSegment::Vector&           segments,   //
                     const Relations::SegmentToTrackVector&          segToTkRel, //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkmcrels,   //
                     const LHCb::MCRichTracks&                       mcRichTks ) const override {

      // MC utils
      const Rich::Future::MC::Relations::TrackToMCParticle mcHelper( tkmcrels );

      // create map from MCP -> MCRichTrack
      std::unordered_map<const LHCb::MCParticle*, const LHCb::MCRichTrack*> mcpToMCRT;
      for ( const auto mcRichTk : mcRichTks ) {
        const auto mcp = ( mcRichTk ? mcRichTk->mcParticle() : nullptr );
        if ( mcp ) { mcpToMCRT[mcp] = mcRichTk; }
      }

      // clone input before modifying
      auto new_angles = in_angles;

      // loop over angles to modify
      for ( auto&& [segment, angles, tkIn] : Ranges::Zip( segments, new_angles, segToTkRel ) ) {

        // associated track object
        const auto* track = tracks[tkIn];

        // Get the MCParticle for this track
        const auto mcPs = mcHelper.mcParticles( *track, false, 0.75 );
        if ( mcPs.empty() ) { continue; }
        // just use the first
        const auto mcP = mcPs.front();

        // The True MCParticle type
        const auto pid = mcHelper.mcParticleType( mcP );
        if ( Rich::Unknown == pid ) { continue; }

        // Get the MC Rich Track if available
        const auto iMCRT = mcpToMCRT.find( mcP );
        if ( iMCRT == mcpToMCRT.end() ) { continue; }
        const auto mcRT = iMCRT->second;
        // then the MC segment
        const auto mcSeg = ( mcRT ? mcRT->segmentInRad( segment.radiator() ) : nullptr );
        if ( !mcSeg ) { continue; }
        // .. finally the MC photons
        const auto& mcPhots = mcSeg->mcRichOpticalPhotons();
        if ( mcPhots.empty() ) { continue; }

        // Loop over photons and form average CK theta for signal
        float theta{ 0 };
        for ( const auto& mcPhot : mcPhots ) { theta += mcPhot->cherenkovTheta(); }
        theta /= (float)mcPhots.size();

        // Update the expected value for true type
        _ri_verbo << "Updating " << angles[pid] << " PID=" << pid << " to " << theta << endmsg;
        angles[pid] = theta;
      }

      return new_angles;
    }

  private:
    // properties
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RichTrackCKAnglesUseMCInfo )

} // namespace Rich::Future::Rec::MC
