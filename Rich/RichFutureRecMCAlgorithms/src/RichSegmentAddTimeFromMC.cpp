/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/LinksByKey.h"
#include "Event/Track.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Relations
#include "RichFutureMCUtils/TrackToMCParticle.h"

namespace Rich::Future::Rec::MC {

  namespace {
    using OutData = LHCb::RichTrackSegment::Vector;
  }

  /** @class RichSegmentAddTimeFromMC RichSegmentAddTimeFromMC.h
   *
   *  Adds track origin 4D vertex to rICH track segments using MC truth
   *
   *  @author Chris Jones
   *  @date   2023-09-06
   */

  class RichSegmentAddTimeFromMC final
      : public LHCb::Algorithm::Transformer<OutData( const LHCb::Track::Range&,              //
                                                     const LHCb::RichTrackSegment::Vector&,  //
                                                     const Relations::SegmentToTrackVector&, //
                                                     const LHCb::MCParticles&,               //
                                                     const LHCb::LinksByKey& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    RichSegmentAddTimeFromMC( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                         KeyValue{ "InTrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default },
                         KeyValue{ "MCParticlesLocation", LHCb::MCParticleLocation::Default },
                         KeyValue{ "MCParticlesLinkLocation", "Link/" + LHCb::TrackLocation::Default } },
                       // output data
                       { KeyValue{ "OutTrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default + "Out" } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    OutData operator()( const LHCb::Track::Range&              tracks,     //
                        const LHCb::RichTrackSegment::Vector&  segments,   //
                        const Relations::SegmentToTrackVector& segToTkRel, //
                        const LHCb::MCParticles&               mcParts,    //
                        const LHCb::LinksByKey&                links ) const override {

      // make new segments container
      OutData segsWithTime;
      segsWithTime.reserve( segments.size() );

      // loop over segments, clone, and add time from MC
      for ( const auto&& [seg, tkIndex] : Ranges::ConstZip( segments, segToTkRel ) ) {

        // clone segment to new container
        auto& new_seg = segsWithTime.emplace_back( seg );

        // Get associated Track object
        const auto tk = tracks.at( tkIndex );

        // Find best MCP based on weight
        double                  bestWeight = -1;
        const LHCb::MCParticle* bestMCP    = nullptr;
        links.applyToLinks( tk->key(), [&bestMCP, &bestWeight, &mcParts]( auto, auto tgtKey, auto weight ) {
          if ( weight > bestWeight ) {
            const auto mcp = dynamic_cast<const LHCb::MCParticle*>( mcParts.containedObject( tgtKey ) );
            if ( mcp ) {
              bestWeight = weight;
              bestMCP    = mcp;
            }
          }
        } );

        // Set origin (primary) 4D vertex
        const auto mc_orig_v = ( bestMCP ? bestMCP->primaryVertex() : nullptr );
        new_seg.setOriginVertex( mc_orig_v ? mc_orig_v->position4vector() : Gaudi::XYZTPoint{ 0, 0, 0, 0 } );
        _ri_verbo << new_seg << endmsg;
      }

      // return new segments with time
      return segsWithTime;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RichSegmentAddTimeFromMC )

} // namespace Rich::Future::Rec::MC
