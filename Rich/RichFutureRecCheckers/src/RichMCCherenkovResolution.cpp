/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// STD
#include <algorithm>
#include <array>
#include <cmath>
#include <deque>

namespace Rich::Future::Rec::MC::Moni {

  /** @class CherenkovResolution RichCherenkovResolution.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class CherenkovResolution final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const CherenkovResolutions::Vector&,             //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CherenkovResolution( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                      KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } ) {
      setProperty( "NBins1DHistos", 100 ).ignore(); // to match number of bins in CKResParameterisation
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const CherenkovResolutions::Vector&             ckResolutions, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// momentum bin
    inline auto pullBin( const double ptot, const Rich::DetectorType rich ) const {
      return ( ptot < m_minP[rich] ? 0 : //
                   ptot > m_maxP[rich] ? m_nPullBins[rich] - 1
                                       : //
                   ( std::size_t )( ( ptot - m_minP[rich] ) * m_pullPtotInc[rich] ) );
    }

    /// min max for given momentum bin ( in GeV )
    auto binMinMaxPtot( const std::size_t bin, const Rich::DetectorType rich ) const {
      return std::make_pair( ( m_minP[rich] + ( bin / m_pullPtotInc[rich] ) ) / Gaudi::Units::GeV,
                             ( m_minP[rich] + ( ( bin + 1 ) / m_pullPtotInc[rich] ) ) / Gaudi::Units::GeV );
    }

    /// Get the histo ID string for a given bin
    inline std::string binToID( const std::size_t i ) const noexcept {
      return "Pulls/PtotBins/ckPull-Bin" + std::to_string( i );
    }

  private:
    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.0f, 0.0f } };

    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.010f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.056f, 0.033f } };

    /// Min for expected CK resolution
    Gaudi::Property<DetectorArray<float>> m_ckResMin{ this, "CKResMin", { 0.00075f, 0.00040f } };

    /// Max for expected CK resolution
    Gaudi::Property<DetectorArray<float>> m_ckResMax{ this, "CKResMax", { 0.0026f, 0.00102f } };

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_minP{ this, "MinP", { 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV } };

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV } };

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{ this, "SkipElectrons", false };

    /// Number of bins for binned pull plots
    Gaudi::Property<DetectorArray<std::size_t>> m_nPullBins{ this, "NPullBins", { 200, 200 } };

  private:
    // cached data

    /// 1/increment for pull ptot binning
    DetectorArray<double> m_pullPtotInc = { 0, 0 };

    // cached histogram pointers

    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_expCKang      = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_ckres         = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_ckresVcktheta = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_ckresVptot    = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVckang     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVPtot      = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVckang = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVPtot  = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVckangInner     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVPtotInner      = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVckangInner = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVPtotInner  = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVckangOuter     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_diffckVPtotOuter      = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVckangOuter = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsdiffckVPtotOuter  = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_ckPullVckang     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_ckPullVPtot      = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsCkPullVckang = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_fabsCkPullVPtot  = { {} };

    mutable Hist::DetArray<Hist::PartArray<std::deque<Hist::WH1D<>>>> h_pullBins = { { {} } };
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode CherenkovResolution::prebookHistograms() {
  using namespace Gaudi::Units;

  bool ok = true;

  // Loop over radiators
  for ( const auto rich : activeDetectors() ) {
    const auto rad = radType( rich );

    // cache some numbers for later use
    assert( m_maxP[rich] > m_minP[rich] );
    assert( 0 < m_minP[rich] );
    m_pullPtotInc[rich] = m_nPullBins[rich] / ( m_maxP[rich] - m_minP[rich] );

    // Loop over all particle codes
    for ( const auto hypo : activeParticlesNoBT() ) {
      // track plots
      ok &= initHist( h_expCKang[rich][hypo],                            //
                      HID( "expCKang", rad, hypo ),                      //
                      "Expected CK angle",                               //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), //
                      "Cherenkov Theta / rad" );
      ok &= initHist( h_ckres[rich][hypo],                           //
                      HID( "ckres", rad, hypo ),                     //
                      "Calculated CKres",                            //
                      m_ckResMin[rich], m_ckResMax[rich], nBins1D(), //
                      "Delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_ckresVcktheta[rich][hypo],       //
                      HID( "ckresVcktheta", rad, hypo ), //
                      "Calculated CKres V CKangle",      //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], m_nPullBins[rich], "Cherenkov Theta / rad",
                      "Cherenkov Theta Resolution / rad" );
      ok &= initHist( h_ckresVptot[rich][hypo],                      //
                      HID( "ckresVptot", rad, hypo ),                //
                      "Calculated CKres V ptot",                     //
                      m_minP[rich], m_maxP[rich], m_nPullBins[rich], //
                      "Momentum / MeV/c", "Cherenkov Theta Resolution / rad" );

      // photon plots
      // Combined
      ok &= initHist( h_diffckVckang[rich][hypo],                        //
                      HID( "diffckVckang", rad, hypo ),                  //
                      "Rec-Exp CKtheta V CKtheta - MC true photons",     //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), //
                      "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_diffckVPtot[rich][hypo],                  //
                      HID( "diffckVPtot", rad, hypo ),            //
                      "Rec-Exp CKtheta V ptot - MC true photons", //
                      m_minP[rich], m_maxP[rich], nBins1D(),      //
                      "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_fabsdiffckVckang[rich][hypo], HID( "fabsdiffckVckang", rad, hypo ), //
                      "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons",                  //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),                    //
                      "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" );
      ok &= initHist( h_fabsdiffckVPtot[rich][hypo],                    //
                      HID( "fabsdiffckVPtot", rad, hypo ),              //
                      "fabs(Rec-Exp) CKtheta V ptot - MC true photons", //
                      m_minP[rich], m_maxP[rich], nBins1D(),            //
                      "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" );
      // Inner Regions
      ok &= initHist( h_diffckVckangInner[rich][hypo],                              //
                      HID( "diffckVckangInner", rad, hypo ),                        //
                      "Rec-Exp CKtheta V CKtheta - MC true photons - Inner Region", //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),            //
                      "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_diffckVPtotInner[rich][hypo],                            //
                      HID( "diffckVPtotInner", rad, hypo ),                      //
                      "Rec-Exp CKtheta V ptot - MC true photons - Inner Region", //
                      m_minP[rich], m_maxP[rich], nBins1D(),                     //
                      "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_fabsdiffckVckangInner[rich][hypo], HID( "fabsdiffckVckangInner", rad, hypo ), //
                      "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons - Inner Region",             //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),                              //
                      "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" );
      ok &= initHist( h_fabsdiffckVPtotInner[rich][hypo],                              //
                      HID( "fabsdiffckVPtotInner", rad, hypo ),                        //
                      "fabs(Rec-Exp) CKtheta V ptot - MC true photons - Inner Region", //
                      m_minP[rich], m_maxP[rich], nBins1D(),                           //
                      "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" );
      // Outer Regions
      ok &= initHist( h_diffckVckangOuter[rich][hypo],                              //
                      HID( "diffckVckangOuter", rad, hypo ),                        //
                      "Rec-Exp CKtheta V CKtheta - MC true photons - Outer Region", //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),            //
                      "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_diffckVPtotOuter[rich][hypo],                            //
                      HID( "diffckVPtotOuter", rad, hypo ),                      //
                      "Rec-Exp CKtheta V ptot - MC true photons - Outer Region", //
                      m_minP[rich], m_maxP[rich], nBins1D(),                     //
                      "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" );
      ok &= initHist( h_fabsdiffckVckangOuter[rich][hypo], HID( "fabsdiffckVckangOuter", rad, hypo ), //
                      "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons - Outer Region",             //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),                              //
                      "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" );
      ok &= initHist( h_fabsdiffckVPtotOuter[rich][hypo],                              //
                      HID( "fabsdiffckVPtotOuter", rad, hypo ),                        //
                      "fabs(Rec-Exp) CKtheta V ptot - MC true photons - Outer Region", //
                      m_minP[rich], m_maxP[rich], nBins1D(),                           //
                      "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" );

      // pull plots
      ok &= initHist( h_ckPullVckang[rich][hypo],                          //
                      HID( "ckPullVckang", rad, hypo ),                    //
                      "(Rec-Exp)/Res CKtheta V CKtheta - MC true photons", //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),   //
                      "Cherenkov Theta / rad", "Cherenkov Resolution Pull" );
      ok &= initHist( h_ckPullVPtot[rich][hypo],                        //
                      HID( "ckPullVPtot", rad, hypo ),                  //
                      "(Rec-Exp)/Res CKtheta V ptot - MC true photons", //
                      m_minP[rich], m_maxP[rich], nBins1D(),            //
                      "Momentum / MeV/c", "Cherenkov Resolution Pull" );
      ok &= initHist( h_fabsCkPullVckang[rich][hypo], HID( "fabsCkPullVckang", rad, hypo ), //
                      "fabs((Rec-Exp)/Res) CKtheta V CKtheta - MC true photons",            //
                      m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),                    //
                      "Cherenkov Theta / rad", "abs(Cherenkov Resolution Pull)" );
      ok &= initHist( h_fabsCkPullVPtot[rich][hypo],                          //
                      HID( "fabsCkPullVPtot", rad, hypo ),                    //
                      "fabs((Rec-Exp)/Res) CKtheta V ptot - MC true photons", //
                      m_minP[rich], m_maxP[rich], nBins1D(),                  //
                      "Momentum / MeV/c", "abs(Cherenkov Resolution Pull)" );
      // 1D plots binned in ptot
      for ( std::size_t i = 0; i < m_nPullBins[rich]; ++i ) {
        // min/max Ptot for this bin
        const auto binEdges = binMinMaxPtot( i, rich );
        // hist title string
        std::ostringstream title;
        title << "(Rec-Exp)/Res CKtheta - MC true photons - Bin " << i << " Ptot " << binEdges.first << " to "
              << binEdges.second << " GeV";
        // book the histo
        ok &= initHist( h_pullBins[rich][hypo].emplace_back(), HID( binToID( i ), rad, hypo ), //
                        title.str(), -5, 5, nBins1D() );
      }

    } // active particles
  }

  return StatusCode{ ok };
}

//-----------------------------------------------------------------------------

void CherenkovResolution::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                      const LHCb::Track::Range&                       tracks,        //
                                      const SIMDPixelSummaries&                       pixels,        //
                                      const Rich::PDPixelCluster::Vector&             clusters,      //
                                      const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                      const LHCb::RichTrackSegment::Vector&           segments,      //
                                      const CherenkovAngles::Vector&                  expTkCKThetas, //
                                      const CherenkovResolutions::Vector&             ckResolutions, //
                                      const SIMDCherenkovPhoton::Vector&              photons,       //
                                      const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                      const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // local buffers
  auto hb_expCKang              = h_expCKang.buffer();
  auto hb_ckres                 = h_ckres.buffer();
  auto hb_ckresVcktheta         = h_ckresVcktheta.buffer();
  auto hb_ckresVptot            = h_ckresVptot.buffer();
  auto hb_diffckVckang          = h_diffckVckang.buffer();
  auto hb_diffckVPtot           = h_diffckVPtot.buffer();
  auto hb_fabsdiffckVckang      = h_fabsdiffckVckang.buffer();
  auto hb_fabsdiffckVPtot       = h_fabsdiffckVPtot.buffer();
  auto hb_diffckVckangInner     = h_diffckVckangInner.buffer();
  auto hb_diffckVPtotInner      = h_diffckVPtotInner.buffer();
  auto hb_fabsdiffckVckangInner = h_fabsdiffckVckangInner.buffer();
  auto hb_fabsdiffckVPtotInner  = h_fabsdiffckVPtotInner.buffer();
  auto hb_diffckVckangOuter     = h_diffckVckangOuter.buffer();
  auto hb_diffckVPtotOuter      = h_diffckVPtotOuter.buffer();
  auto hb_fabsdiffckVckangOuter = h_fabsdiffckVckangOuter.buffer();
  auto hb_fabsdiffckVPtotOuter  = h_fabsdiffckVPtotOuter.buffer();
  auto hb_ckPullVckang          = h_ckPullVckang.buffer();
  auto hb_ckPullVPtot           = h_ckPullVPtot.buffer();
  auto hb_fabsCkPullVckang      = h_fabsCkPullVckang.buffer();
  auto hb_fabsCkPullVPtot       = h_fabsCkPullVPtot.buffer();
  // auto hb_pullBins              = h_pullBins.buffer(); // FixMe

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // RICH Radiator info
      const auto rich = seg.rich();
      const auto rad  = seg.radiator();
      if ( !richIsActive( rich ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // get the resolutions for this segment
      const auto& ckRes = ckResolutions[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // loop over mass hypos
      for ( const auto hypo : activeParticlesNoBT() ) {
        if ( expCKangles[hypo] > 0 ) {
          ++hb_expCKang[rich][hypo][expCKangles[hypo]];
          ++hb_ckres[rich][hypo][ckRes[hypo]];
          hb_ckresVcktheta[rich][hypo][expCKangles[hypo]] += ckRes[hypo];
          hb_ckresVptot[rich][hypo][pTot] += ckRes[hypo];
        }
      }

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) continue;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, skip
          if ( Rich::Unknown == pid ) continue;
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) continue;
          // finally check if PID is in the active list
          if ( !particleIsActive( pid ) ) { continue; }

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          if ( mcbeta >= m_minBeta[rich] && mcbeta <= m_maxBeta[rich] ) {

            // true Cherenkov signal ?
            const bool trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // expected CK theta ( for true type )
            const auto thetaExp = expCKangles[pid];

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // fill some plots for true photons only
            if ( trueCKSig ) {
              // Res versus Ptot and CK theta
              hb_diffckVckang[rich][pid][thetaExp] += { deltaTheta, mcPW };
              hb_diffckVPtot[rich][pid][pTot] += { deltaTheta, mcPW };
              hb_fabsdiffckVckang[rich][pid][thetaExp] += { fabs( deltaTheta ), mcPW };
              hb_fabsdiffckVPtot[rich][pid][pTot] += { fabs( deltaTheta ), mcPW };
              if ( simdPix.isInnerRegion()[i] ) {
                hb_diffckVckangInner[rich][pid][thetaExp] += { deltaTheta, mcPW };
                hb_diffckVPtotInner[rich][pid][pTot] += { deltaTheta, mcPW };
                hb_fabsdiffckVckangInner[rich][pid][thetaExp] += { fabs( deltaTheta ), mcPW };
                hb_fabsdiffckVPtotInner[rich][pid][pTot] += { fabs( deltaTheta ), mcPW };
              } else {
                hb_diffckVckangOuter[rich][pid][thetaExp] += { deltaTheta, mcPW };
                hb_diffckVPtotOuter[rich][pid][pTot] += { deltaTheta, mcPW };
                hb_fabsdiffckVckangOuter[rich][pid][thetaExp] += { fabs( deltaTheta ), mcPW };
                hb_fabsdiffckVPtotOuter[rich][pid][pTot] += { fabs( deltaTheta ), mcPW };
              }
              // pulls
              if ( ckRes[pid] > 0 ) {
                const auto pull = deltaTheta / ckRes[pid];
                h_pullBins[rich][pid].at( pullBin( pTot, rich ) )[pull] += mcPW;
                hb_ckPullVckang[rich][pid][thetaExp] += { pull, mcPW };
                hb_ckPullVPtot[rich][pid][pTot] += { pull, mcPW };
                hb_fabsCkPullVckang[rich][pid][thetaExp] += { fabs( pull ), mcPW };
                hb_fabsCkPullVPtot[rich][pid][pTot] += { fabs( pull ), mcPW };
              }
            }

          } // beta selection

        } // loop over associated MCPs

      } // scalar loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CherenkovResolution )

//-----------------------------------------------------------------------------
