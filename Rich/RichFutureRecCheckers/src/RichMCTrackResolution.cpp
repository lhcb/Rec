/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "Event/Track.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Relations
#include "RichFutureMCUtils/TrackToMCParticle.h"

// STD
#include <algorithm>
#include <cassert>
#include <utility>

namespace Rich::Future::Rec::MC::Moni {

  /** @class TrackResolution RichMCTrackResolution.h
   *
   *  MC checking of the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2020-10-29
   */

  class TrackResolution final
      : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&,              //
                                               const LHCb::RichTrackSegment::Vector&,  //
                                               const Relations::SegmentToTrackVector&, //
                                               const Rich::Future::MC::Relations::TkToMCPRels& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackResolution( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range&                       tracks,     //
                     const LHCb::RichTrackSegment::Vector&           segments,   //
                     const Relations::SegmentToTrackVector&          segToTkRel, //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels ) const override {

      // Make a local MC helper object
      Rich::Future::MC::Relations::TrackToMCParticle mcHelper( tkrels );

      // local buffers
      auto hb_mcpVrecoP   = h_mcpVrecoP.buffer();
      auto hb_pdiffVrecoP = h_pdiffVrecoP.buffer();

      // loop over segments
      for ( const auto&& [seg, tkIndex] : Ranges::ConstZip( segments, segToTkRel ) ) {

        // RICH
        const auto rich = seg.rich();
        if ( !richIsActive( rich ) ) { continue; }

        // momentum
        const auto pTot = seg.bestMomentumMag();
        if ( pTot < m_minP[rich] || pTot > m_maxP[rich] ) { continue; }

        // Track pointer
        const auto tk = tracks.at( tkIndex );

        // MCP Pointers
        const auto mcPs = mcHelper.mcParticles( *tk );

        // Weight per MCP
        const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {
          if ( !mcP ) { continue; }

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, skip
          if ( Rich::Unknown == pid ) { continue; }

          // MC Particle momentum
          const auto pTotMC = mcP->p();

          // fill histograms
          hb_mcpVrecoP[rich][pid][pTot] += { pTotMC, mcPW };
          hb_pdiffVrecoP[rich][pid][pTot] += { pTot - pTotMC, mcPW };
        }
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;
      // Loop over radiators
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );
        // loop over particles
        for ( const auto pid : Rich::particles() ) {
          if ( pid == Rich::BelowThreshold ) { continue; }
          ok &= initHist( h_mcpVrecoP[rich][pid], HID( "mcpVrecop", rad, pid ), //
                          "MC V Reco Track Momentum",                           //
                          m_minP[rich], m_maxP[rich], nBins1D(),                //
                          "Reco Track Momentum (MeV/c)",                        //
                          "MC Track Momentum (MeV/c)" );
          ok &= initHist( h_pdiffVrecoP[rich][pid], HID( "pdiffVrecop", rad, pid ), //
                          "(Rec-MC) V Reco Track Momentum",                         //
                          m_minP[rich], m_maxP[rich], nBins1D(),                    //
                          "Reco Track Momentum (MeV/c)",                            //
                          "(Reco-MC) Track Momentum (MeV/c)" );
        }
      }
      return StatusCode{ ok };
    }

  private:
    // properties

    /// Min momentum by RICH (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_minP{ this, "MinP", { 2.0 * Gaudi::Units::GeV, 2.0 * Gaudi::Units::GeV } };

    /// Max momentum by RICH (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV } };

  private:
    // cached data

    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_mcpVrecoP   = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_pdiffVrecoP = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackResolution )

} // namespace Rich::Future::Rec::MC::Moni
