/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichDAQDefinitions.h"

// Relations
#include "RichFutureMCUtils/RichSmartIDMCUtils.h"

// event data
#include "Event/MCRichDigitSummary.h"

// Rec Event
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// STD
#include <algorithm>
#include <cstdint>
#include <unordered_map>

namespace Rich::Future::Rec::MC::Moni {

  /** @class DetectorHits RichDetectorHits.h
   *
   *  Monitors the data sizes in the RICH detectors using MC
   *
   *  @author Chris Jones
   *  @date   2023-11-15
   */

  class DetectorHits final : public LHCb::Algorithm::Consumer<void( const SIMDPixelSummaries&,          //
                                                                    const LHCb::MCRichDigitSummarys& ), //
                                                              Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    DetectorHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } ) {}

  public:
    /// Functional operator
    void operator()( const SIMDPixelSummaries&        pixels, //
                     const LHCb::MCRichDigitSummarys& digitSums ) const override {

      using namespace Rich::Future::MC::Relations;

      // Count hits per channel ID
      std::unordered_map<LHCb::RichSmartID, std::size_t> hitsPerID;

      // MC helper
      SmartIDUtils smartIDMCHelper( digitSums );

      // local buffers
      auto hb_hitsPerID   = h_hitsPerID.buffer();
      auto hb_digitsPerID = h_digitsPerID.buffer();
      auto hb_signalPerID = h_signalPerID.buffer();
      auto hb_sigFracVOcc = h_sigFracVOcc.buffer();

      // Loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        // Loop over pixels for this RICH
        for ( const auto& pix : pixels.range( rich ) ) {
          // scalar loop
          for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
            if ( !pix.validMask()[i] ) { continue; }
            // Hit ID
            const auto id = pix.smartID()[i];
            // count hits per channel (strip time info)
            ++hitsPerID[id.channelDataOnly()];
          } // scalar loop
        }   // SIMD pixels
      }     // RICHes

      // loop again over collected hits
      for ( const auto& [id, count] : hitsPerID ) {

        // Get MC truth for this ID
        const auto histCodes = smartIDMCHelper.mcDigitHistoryCodes( id );

        // which RICH
        const auto rich = id.rich();

        // Count signal hits in this channel
        const auto nSignal =
            std::count_if( histCodes.begin(), histCodes.end(), //
                           [&rich]( const auto& code ) {
                             return ( code.signalEvent() && ( ( Rich::Rich1 == rich && code.c4f10Hit() ) ||
                                                              ( Rich::Rich2 == rich && code.cf4Hit() ) ) );
                           } );
        const auto sigFrac = ( !histCodes.empty() ? (double)nSignal / (double)histCodes.size() : 0.0 );

        // fill histos
        ++hb_hitsPerID[rich][histCodes.size()];
        ++hb_digitsPerID[rich][count];
        ++hb_signalPerID[rich][nSignal];
        hb_sigFracVOcc[rich][histCodes.size()] += sigFrac;
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool ok = true;

      const std::size_t nHitMax = 30;

      for ( const auto rich : activeDetectors() ) {
        ok &= initHist( h_hitsPerID[rich], Rich::HistogramID( "nHitsPerChanID", rich ), //
                        "# hits per channel ID (total nHits>0)",                        //
                        -0.5, nHitMax + 0.5, nHitMax + 1 );
        ok &= initHist( h_signalPerID[rich], Rich::HistogramID( "nSignalHitsPerChanID", rich ), //
                        "# signal hits per channel ID (total nHits>0)",                         //
                        -0.5, nHitMax + 0.5, nHitMax + 1 );
        ok &= initHist( h_digitsPerID[rich], Rich::HistogramID( "nDigitsPerChanID", rich ), //
                        "# digitised hits per channel ID (total nHits>0)",                  //
                        -0.5, nHitMax + 0.5, nHitMax + 1 );
        ok &= initHist( h_sigFracVOcc[rich], Rich::HistogramID( "sigFracVOcc", rich ), //
                        "Signal fraction versus occupancy (total nHits>0)",            //
                        -0.5, nHitMax + 0.5, nHitMax + 1 );
      }

      return StatusCode{ ok };
    }

  private:
    // cached histograms

    mutable Hist::DetArray<Hist::H1D<>> h_hitsPerID   = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_signalPerID = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_digitsPerID = { {} };
    mutable Hist::DetArray<Hist::P1D<>> h_sigFracVOcc = { {} };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectorHits )

} // namespace Rich::Future::Rec::MC::Moni
