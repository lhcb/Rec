/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// boost
#include "boost/format.hpp"

// STD
#include <algorithm>
#include <array>
#include <cassert>
#include <sstream>
#include <utility>
#include <vector>

namespace Rich::Future::Rec::MC::Moni {

  /** @class CKResParameterisation RichMCCKResParameterisation.h
   *
   *  Computes parameterisation of CK resolution as function of P and expected CK Theta.
   *
   *  @author Chris Jones
   *  @date   2020-10-25
   */

  class CKResParameterisation final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const SIMDPhotonSignals::Vector&,                //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CKResParameterisation( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                      KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } ) {
      setProperty( "NBins1DHistos", 150 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const SIMDPhotonSignals::Vector&                photSignals,   ///< photon signals
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Get CK theta bin
    template <typename TYPE>
    inline std::size_t ckTBin( const TYPE               ckTheta, //
                               const Rich::DetectorType rich ) const {
      const auto ckRange = m_ckThetaMax[rich] - m_ckThetaMin[rich];
      const auto inc     = ckRange / NCKThetaBins;
      const auto bin     = ( ckTheta <= m_ckThetaMin[rich] ? 0 : //
                             ckTheta >= m_ckThetaMax[rich] ? NCKThetaBins - 1
                                                               : //
                             std::size_t( ( ckTheta - m_ckThetaMin[rich] ) / inc ) );
      assert( bin < NCKThetaBins );
      return std::min( bin, NCKThetaBins - 1 );
    }

    /// Get CK theta bin limits
    inline std::pair<float, float> ckBinLimits( const std::size_t        bin, //
                                                const Rich::DetectorType rich ) const {
      assert( bin < NCKThetaBins );
      const auto ckRange = m_ckThetaMax[rich] - m_ckThetaMin[rich];
      const auto inc     = ckRange / NCKThetaBins;
      return { m_ckThetaMin[rich] + ( inc * bin ), //
               m_ckThetaMin[rich] + ( inc * ( 1 + bin ) ) };
    }

    /// Is CK theta value inside the bin range
    template <typename TYPE>
    inline bool inCKTRange( const TYPE               ckTheta, //
                            const Rich::DetectorType rich ) const {
      return ( ckTheta >= m_ckThetaMin[rich] && ckTheta <= m_ckThetaMax[rich] );
    }

  private:
    /// Get momentum bin
    template <typename TYPE>
    inline std::size_t pBin( const TYPE               p, //
                             const Rich::DetectorType rich ) const {
      const auto pRange = m_maxP[rich] - m_minP[rich];
      const auto inc    = pRange / NCKThetaBins;
      const auto bin    = ( p <= m_minP[rich] ? 0 : //
                             p >= m_maxP[rich] ? NCKThetaBins - 1
                                                  : //
                             std::size_t( ( p - m_minP[rich] ) / inc ) );
      assert( bin < NCKThetaBins );
      return std::min( bin, NCKThetaBins - 1 );
    }

    /// Get momentum bin limits
    inline std::pair<float, float> pBinLimits( const std::size_t        bin, //
                                               const Rich::DetectorType rich ) const {
      assert( bin < NCKThetaBins );
      const auto pRange = m_maxP[rich] - m_minP[rich];
      const auto inc    = pRange / NCKThetaBins;
      return { m_minP[rich] + ( inc * bin ), //
               m_minP[rich] + ( inc * ( 1 + bin ) ) };
    }

    /// Is momentum value inside the bin range
    template <typename TYPE>
    inline bool inPRange( const TYPE               p, //
                          const Rich::DetectorType rich ) const {
      return ( p >= m_minP[rich] && p <= m_maxP[rich] );
    }

  private:
    // properties

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.010f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.056f, 0.033f } };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<DetectorArray<float>> m_ckResRange{ this, "CKResHistoRange", { 0.008f, 0.0050f } };

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_minP{
        this, "MinP", { 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV }, "Minimum momentum (MeV/c)" };

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV }, "Maximum momentum (MeV/c)" };

    /// Maximum number of MCParticles associated to a given track
    Gaudi::Property<DetectorArray<std::size_t>> m_nMaxMCPsAssoc{ this, "MaxMCPsAssoc", { 1, 1 } };

    /// The minimum cut value for photon probability
    Gaudi::Property<DetectorArray<float>> m_minPhotonProb{
        this, "MinPhotonProbability", { 1e-9f, 1e-8f }, "The minimum allowed photon probability values" };

  private:
    // cached data

    /// Number of bins for CK theta resolution parameterisation
    static constexpr std::size_t NCKThetaBins = 200;

    // plots for parameterised resolutions in each RICH
    using ParamCKRes = Hist::DetArray<Hist::PartArray<Hist::Array<Hist::WH1D<>, NCKThetaBins>>>;

    // true
    mutable ParamCKRes h_ckResTrueCKBins   = { {} };
    mutable ParamCKRes h_ckThetaTrueCKBins = { {} };
    mutable ParamCKRes h_ckResTruePBins    = { {} };
    mutable ParamCKRes h_ckThetaTruePBins  = { {} };
    // fake
    mutable ParamCKRes h_ckResFakeCKBins   = { {} };
    mutable ParamCKRes h_ckThetaFakeCKBins = { {} };
    mutable ParamCKRes h_ckResFakePBins    = { {} };
    mutable ParamCKRes h_ckThetaFakePBins  = { {} };
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode CKResParameterisation::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rich : activeDetectors() ) {
    const auto rad = radType( rich );

    assert( 0 < m_minP[rich] );
    assert( 0 < m_maxP[rich] );
    assert( m_minP[rich] < m_maxP[rich] );

    // loop over particles
    for ( const auto pid : Rich::particles() ) {
      if ( pid == Rich::BelowThreshold ) { continue; }

      // loop over CK bins
      for ( std::size_t bin = 0; bin < NCKThetaBins; ++bin ) {
        const auto binS = "bin" + std::to_string( bin + 1 ); // number from 1 for human readable bin labels
        // CK bins
        {
          const auto [ckmin, ckmax] = ckBinLimits( bin, rich );
          std::ostringstream ckS;
          boost::format      fS( "%10.6f" );
          ckS << " - " << fS % ckmin << "<CKThetaExp(rad)<" << fS % ckmax;
          ok &= initHist( h_ckResTrueCKBins[rich][pid].at( bin ),                      //
                          HID( "CKBins/ckResTrue/" + binS, rad, pid ),                 //
                          "Rec-Exp CKTheta - MC true photons - True Type" + ckS.str(), //
                          -m_ckResRange[rich], m_ckResRange[rich], 2 * nBins1D(),      //
                          "delta(Cherenkov Theta) / rad" );
          ok &= initHist( h_ckResFakeCKBins[rich][pid].at( bin ),                      //
                          HID( "CKBins/ckResFake/" + binS, rad, pid ),                 //
                          "Rec-Exp CKTheta - MC fake photons - True Type" + ckS.str(), //
                          -m_ckResRange[rich], m_ckResRange[rich], 2 * nBins1D(),      //
                          "delta(Cherenkov Theta) / rad" );
          ok &= initHist( h_ckThetaTrueCKBins[rich][pid].at( bin ),              //
                          HID( "CKBins/ckThetaTrue/" + binS, rad, pid ),         //
                          "Reconstructed CKTheta - MC true photons" + ckS.str(), //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),     //
                          "Cherenkov Theta / rad" );
          ok &= initHist( h_ckThetaFakeCKBins[rich][pid].at( bin ),              //
                          HID( "CKBins/ckThetaFake/" + binS, rad, pid ),         //
                          "Reconstructed CKTheta - MC fake photons" + ckS.str(), //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),     //
                          "Cherenkov Theta / rad" );
        }
        // momentum bins
        {
          const auto [pmin, pmax] = pBinLimits( bin, rich );
          std::ostringstream pS;
          boost::format      fS( "%8.4f" );
          pS << " - " << fS % ( pmin / Gaudi::Units::GeV ) << "<P(GeV)<" << fS % ( pmax / Gaudi::Units::GeV );
          ok &= initHist( h_ckResTruePBins[rich][pid].at( bin ),                      //
                          HID( "PBins/ckResTrue/" + binS, rad, pid ),                 //
                          "Rec-Exp CKTheta - MC true photons - True Type" + pS.str(), //
                          -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                          "delta(Cherenkov Theta) / rad" );
          ok &= initHist( h_ckResFakePBins[rich][pid].at( bin ),                      //
                          HID( "PBins/ckResFake/" + binS, rad, pid ),                 //
                          "Rec-Exp CKTheta - MC fake photons - True Type" + pS.str(), //
                          -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                          "delta(Cherenkov Theta) / rad" );
          ok &= initHist( h_ckThetaTruePBins[rich][pid].at( bin ),              //
                          HID( "PBins/ckThetaTrue/" + binS, rad, pid ),         //
                          "Reconstructed CKTheta - MC true photons" + pS.str(), //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),    //
                          "Cherenkov Theta / rad" );
          ok &= initHist( h_ckThetaFakePBins[rich][pid].at( bin ),              //
                          HID( "PBins/ckThetaFake/" + binS, rad, pid ),         //
                          "Reconstructed CKTheta - MC fake photons" + pS.str(), //
                          m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(),    //
                          "Cherenkov Theta / rad" );
        }
      }
    }
  }

  return StatusCode{ ok };
}

//-----------------------------------------------------------------------------

void CKResParameterisation::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                        const LHCb::Track::Range&                       tracks,        //
                                        const SIMDPixelSummaries&                       pixels,        //
                                        const Rich::PDPixelCluster::Vector&             clusters,      //
                                        const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                        const LHCb::RichTrackSegment::Vector&           segments,      //
                                        const CherenkovAngles::Vector&                  expTkCKThetas, //
                                        const SIMDCherenkovPhoton::Vector&              photons,       //
                                        const SIMDPhotonSignals::Vector&                photSignals,   //
                                        const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                        const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // local buffers
  auto hb_ckResFakeCKBins   = h_ckResFakeCKBins.buffer();
  auto hb_ckThetaFakeCKBins = h_ckThetaFakeCKBins.buffer();
  auto hb_ckResFakePBins    = h_ckResFakePBins.buffer();
  auto hb_ckThetaFakePBins  = h_ckThetaFakePBins.buffer();
  auto hb_ckResTrueCKBins   = h_ckResTrueCKBins.buffer();
  auto hb_ckThetaTrueCKBins = h_ckThetaTrueCKBins.buffer();
  auto hb_ckResTruePBins    = h_ckResTruePBins.buffer();
  auto hb_ckThetaTruePBins  = h_ckThetaTruePBins.buffer();

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      const auto& sigs = photSignals[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // RICH Radiator info
      const auto rich = seg.rich();
      const auto rad  = seg.radiator();
      if ( !richIsActive( rich ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();
      const auto inPR = inPRange( pTot, rich );

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, false, 0.7 );
      // require at least one MCP
      if ( mcPs.empty() ) { continue; }
      // max number of associations
      if ( mcPs.size() > m_nMaxMCPsAssoc[rich] ) { continue; }

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) { continue; }

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];
        const auto inCKR    = inCKTRange( thetaRec, rich );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );

          // If MC type not known, skip
          if ( Rich::Unknown == pid ) { continue; }

          // expected CK theta ( for true type )
          const auto thetaExp = expCKangles[pid];
          const auto inCKExpR = inCKTRange( thetaExp, rich );

          // in range ?
          if ( inCKExpR && inCKR && inPR ) {

            // Check photon signal prob is above minimum value for this radiator
            if ( sigs[pid][i] < m_minPhotonProb[rich] ) { continue; }

            // do we have an true MC Cherenkov photon
            const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

            // true Cherenkov signal ?
            const auto trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // bins
            const auto ckbin = ckTBin( thetaExp, rich );
            const auto pbin  = pBin( pTot, rich );

            // fill some plots
            if ( !trueCKSig ) {
              hb_ckResFakeCKBins[rich][pid][ckbin][deltaTheta] += mcPW;
              hb_ckThetaFakeCKBins[rich][pid][ckbin][thetaRec] += mcPW;
              hb_ckResFakePBins[rich][pid][pbin][deltaTheta] += mcPW;
              hb_ckThetaFakePBins[rich][pid][pbin][thetaRec] += mcPW;
            } else {
              hb_ckResTrueCKBins[rich][pid][ckbin][deltaTheta] += mcPW;
              hb_ckThetaTrueCKBins[rich][pid][ckbin][thetaRec] += mcPW;
              hb_ckResTruePBins[rich][pid][pbin][deltaTheta] += mcPW;
              hb_ckThetaTruePBins[rich][pid][pbin][thetaRec] += mcPW;
            }

          } // in range

        } // loop over associated MCPs

      } // SIMD scalar loop
    }   // photon loop
  }     // track loop
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CKResParameterisation )
