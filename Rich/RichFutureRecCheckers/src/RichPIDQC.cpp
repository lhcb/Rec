/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/RichPID.h"
#include "Event/Track.h"

// Relations
#include "RichFutureMCUtils/RichMCRelations.h"
#include "RichFutureMCUtils/TrackToMCParticle.h"

// tools
#include "TrackInterfaces/ITrackSelector.h"

// Boost
#include "boost/lexical_cast.hpp"

// STL
#include <algorithm>
#include <array>
#include <numeric>

namespace Rich::Future::Rec::MC::Moni {

  /** @class PIDQC RichPIDQC.h
   *
   *  MC checking of the RICH PID performance
   *
   *  @author Chris Jones
   *  @date   2016-12-08
   */

  class PIDQC final : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, //
                                                             const LHCb::RichPIDs&,     //
                                                             const Rich::Future::MC::Relations::TkToMCPRels& ),
                                                       Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PIDQC( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichPIDsLocation", LHCb::RichPIDLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles } } ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  protected:
    /// Book histograms
    StatusCode prebookHistograms() override {

      using namespace Gaudi::Units;

      bool ok = true;

      const BinLabels pidTypes = { "unknown", "electron", "muon", "pion", "kaon", "proton", "deuteron", "below_thres" };
      ok &= initHist( h_pidTable,
                      HID( "pidTable" ),                                          //
                      "PID Performance Table",                                    //
                      -1.5, Rich::NParticleTypes - 0.5, Rich::NParticleTypes + 1, //
                      -1.5, Rich::NParticleTypes - 0.5, Rich::NParticleTypes + 1, //
                      "Reco. PID Type", "MC PID Type", "Entries",                 //
                      pidTypes, pidTypes );

      ok &= initHist( h_npids, HID( "nPIDs" ), "# PIDs per event", //
                      -0.5, 200.5, 201,                            //
                      "# PIDs per event", "Entries" );

      ok &= initHist( h_evOK, HID( "evOK" ), "Event Success V Failures", //
                      -0.5, 1.5, 2,                                      //
                      "Event has PIDs", "Entries", BinLabels{ "false", "true" } );

      ok &= initHist( h_tkFrac, HID( "pidFrac" ), "Fraction of Tracks with PIDs", //
                      0, 1, nBins1D(),                                            //
                      "Track Fraction with PID information", "Entries" );

      // Min/Max P
      auto getProp = [&]( const std::string& pS, const double defV ) {
        auto tkSelProps = dynamic_cast<const IProperty*>( m_tkSel.get() );
        if ( tkSelProps && tkSelProps->hasProperty( pS ) ) {
          const auto val = boost::lexical_cast<double>( tkSelProps->getProperty( pS ).toString() );
          _ri_debug << "Using Property '" << pS << "' = " << val << endmsg;
          return val;
        } else {
          _ri_debug << "Using Default  '" << pS << "' = " << defV << endmsg;
          return defV;
        }
      };
      const auto minP = getProp( "MinPCut", 2.0 * GeV );
      const auto maxP = getProp( "MaxPCut", 100 * GeV );

      // loop over active mass hypos
      for ( const auto pid : activeParticlesNoBT() ) {
        ok &= initHist( h_nRFrac[Rich::Rich1][pid], HID( "usedRich1", pid ),
                        "Fraction of Tracks with RICH1 Information", //
                        minP, maxP, nBins1D(),                       //
                        "Track Momentum / MeV/c", "% PIDs with RICH1 Info" );
        ok &= initHist( h_nRFrac[Rich::Rich2][pid], HID( "usedRich2", pid ),
                        "Fraction of Tracks with RICH2 Information", //
                        minP, maxP, nBins1D(),                       //
                        "Track Momentum / MeV/c", "% PIDs with RICH2 Info" );
      }

      return StatusCode{ ok };
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range&                       tracks, //
                     const LHCb::RichPIDs&                           pids,   //
                     const Rich::Future::MC::Relations::TkToMCPRels& rels ) const override;

  private:
    // tools

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{ this, "TrackSelector", "TrackSelector" };

  private:
    // properties

    /// Allow reassign PID to below threshold
    Gaudi::Property<bool> m_allowBTreassign{ this, "AllowBTReassign", true,
                                             "Allow PIDs to be reassigned as Below Threshold" };

  private:
    // Gaudi counters and histos

    /// PID table
    mutable Hist::WH2D<> h_pidTable{};

    /// PIDs per event
    mutable Hist::H1D<> h_npids{};

    /// Event success / fail
    mutable Hist::H1D<> h_evOK{};

    /// Track fraction with PID
    mutable Hist::H1D<> h_tkFrac{};

    /// Fraction of tracks with RICH information versus P
    mutable Hist::DetArray<Hist::PartArray<Hist::WP1D<>>> h_nRFrac{ { {} } };

    /// Proton heavy ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_protonEff{ this, "Pr DLL(Pr)>0 ID Efficiency" };

    /// Kaon heavy ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_kaonEff{ this, "K->K,Pr,D ID Efficiency" };

    /// Pion light ID efficiency
    mutable Gaudi::Accumulators::BinomialCounter<> m_pionEff{ this, "Pi->El,Mu,Pi ID Efficiency" };

    /// RICH Event ID rate
    mutable Gaudi::Accumulators::BinomialCounter<> m_eventsWithID{ this, "Event ID rate" };

    /// RICH Track ID rate
    mutable Gaudi::Accumulators::BinomialCounter<> m_tracksWithID{ this, "Track ID rate" };

    /// Only using RICH1
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1{ this, "Used RICH1 only" };

    /// Only using RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR2{ this, "Used RICH2 only" };

    /// Using both RICH1 and RICH2
    mutable Gaudi::Accumulators::BinomialCounter<> m_withR1R2{ this, "Used RICH1 and RICH2" };

    /// Reco PID reassigned as BT
    mutable Gaudi::Accumulators::BinomialCounter<> m_recoPIDBT{ this, "Reassigned Reco PID BT" };

    /// MC PID reassigned as BT
    mutable Gaudi::Accumulators::BinomialCounter<> m_mcPIDBT{ this, "Reassigned MC PID BT" };
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

void PIDQC::operator()( const LHCb::Track::Range&                       tracks, //
                        const LHCb::RichPIDs&                           pids,   //
                        const Rich::Future::MC::Relations::TkToMCPRels& rels ) const {

  // buffers
  auto protonEff   = m_protonEff.buffer();
  auto kaonEff     = m_kaonEff.buffer();
  auto pionEff     = m_pionEff.buffer();
  auto tksWithID   = m_tracksWithID.buffer();
  auto withR1      = m_withR1.buffer();
  auto withR2      = m_withR2.buffer();
  auto withR1R2    = m_withR1R2.buffer();
  auto hb_nRFrac   = h_nRFrac.buffer();
  auto hb_pidTable = h_pidTable.buffer();

  unsigned int pidCount = 0;

  // count events with at least 1 PID object
  m_eventsWithID += !pids.empty();

  // track selector shortcut
  const auto tkSel = m_tkSel.get();

  // count total selected tracks, PIDs etc.
  const auto nTks = std::count_if( tracks.begin(), tracks.end(), [&tkSel, &pids, &tksWithID]( const auto* tk ) {
    const bool sel = tk && tkSel->accept( *tk );
    if ( sel ) {
      tksWithID += std::any_of( pids.begin(), pids.end(), [&tk]( const auto id ) { return id && tk == id->track(); } );
    }
    return sel;
  } );
  _ri_verbo << "Counted " << nTks << " tracks" << endmsg;

  // helper for the track -> MCP relations
  const Rich::Future::MC::Relations::TrackToMCParticle tkToMPCs( rels );

  // Loop over all PID results
  for ( const auto pid : pids ) {

    // Track for this PID
    const auto tk = pid->track();

    _ri_debug << *pid << endmsg;

    // is track selected
    if ( !tkSel->accept( *tk ) ) { continue; }
    // Is the track in the input list
    // (if not skip, means we are running on a reduced track list with selection cuts)
    if ( std::none_of( tracks.begin(), tracks.end(), [&tk]( const auto* t ) { return t == tk; } ) ) { continue; }

    // Count PIDs and tracks
    ++pidCount;
    withR1 += ( pid->usedRich1Gas() && !pid->usedRich2Gas() );
    withR2 += ( pid->usedRich2Gas() && !pid->usedRich1Gas() );
    withR1R2 += ( pid->usedRich2Gas() && pid->usedRich1Gas() );

    // Get best reco PID
    auto bpid = pid->bestParticleID();
    // if below threshold, set as such
    const bool reassignRecoBT = ( m_allowBTreassign && !pid->isAboveThreshold( bpid ) );
    if ( reassignRecoBT ) {
      _ri_verbo << " -> Reassigned RecoPID to BT" << endmsg;
      bpid = Rich::BelowThreshold;
    }
    m_recoPIDBT += reassignRecoBT;
    _ri_verbo << " -> Best Reco PID = " << bpid << endmsg;

    // Get the MCParticle range for this track
    const auto mcPs = tkToMPCs.mcParticleRange( *tk );
    _ri_verbo << " -> Matched to " << mcPs.size() << " MCParticles" << endmsg;

    // Did we get any MCPs ?
    if ( mcPs.empty() ) {
      // Fill once with MC type unknown (Below threshold)
      hb_pidTable[{ (int)bpid, (int)Rich::BelowThreshold }] += 1.0;
    } else {
      // Get the sum off all MCP weights
      const auto wSum = std::accumulate( mcPs.begin(), mcPs.end(), 0.0,
                                         []( const auto sum, const auto& w ) { return sum + w.weight(); } );
      // fill once per associated MCP
      for ( const auto MC : mcPs ) {
        // get the MCP
        const auto mcP = MC.to();
        // normalised weight for this MCP
        const auto mcPW = ( wSum > 0 ? MC.weight() / wSum : 1.0 );
        // MC Truth
        auto mcpid = tkToMPCs.mcParticleType( mcP );
        // If unknown, set to below threshold (i.e. ghost).
        if ( Rich::Unknown == mcpid ) { mcpid = Rich::BelowThreshold; }
        // acceptance plots (fill before we possibily reassign MCPID BT)
        if ( Rich::BelowThreshold != mcpid ) {
          hb_nRFrac[Rich::Rich1][mcpid][tk->p()] += { ( pid->usedRich1Gas() ? 100.0 : 0.0 ), mcPW };
          hb_nRFrac[Rich::Rich2][mcpid][tk->p()] += { ( pid->usedRich2Gas() ? 100.0 : 0.0 ), mcPW };
        }
        // If a real type, but below threshold, set below threshold
        const bool reassignMCBT = ( m_allowBTreassign && !pid->isAboveThreshold( mcpid ) );
        if ( reassignMCBT ) {
          _ri_verbo << " -> Reassigned MCPID to BT" << endmsg;
          mcpid = Rich::BelowThreshold;
        }
        m_mcPIDBT += reassignMCBT;
        _ri_verbo << " -> MC PID = " << mcpid << endmsg;
        // fill table
        hb_pidTable[{ (int)bpid, (int)mcpid }] += mcPW;
        // fill ID counters
        if ( Rich::BelowThreshold != bpid ) {
          // For protons use DLL cut
          if ( Rich::Proton == mcpid ) { protonEff += ( pid->particleDeltaLL( Rich::Proton ) > 0 ); }
          // For kaons/pions use heavy/light seperation
          const bool heavyID = ( (int)Rich::Kaon <= (int)bpid );
          const bool lightID = ( (int)Rich::Pion >= (int)bpid );
          if ( Rich::Kaon == mcpid ) { kaonEff += heavyID; }
          if ( Rich::Pion == mcpid ) { pionEff += lightID; }
        }
      }
    }

  } // pid loop

  ++h_npids[pidCount];
  ++h_evOK[( pids.empty() ? 0 : 1 )];
  if ( nTks > 0 ) { ++h_tkFrac[static_cast<double>( pidCount ) / static_cast<double>( nTks )]; }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PIDQC )

//-----------------------------------------------------------------------------
