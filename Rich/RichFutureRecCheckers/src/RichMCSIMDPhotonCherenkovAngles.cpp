/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "Event/MCRichDigitSummary.h"
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// STD
#include <algorithm>
#include <cassert>
#include <utility>

namespace Rich::Future::Rec::MC::Moni {

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  MC checking of the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                      KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    // properties

    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } };

    /// Min theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMin{ this, "ChThetaRecHistoLimitMin", { 0.010f, 0.010f } };

    /// Max theta limit for histos for each rad
    Gaudi::Property<DetectorArray<float>> m_ckThetaMax{ this, "ChThetaRecHistoLimitMax", { 0.056f, 0.033f } };

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<DetectorArray<float>> m_ckResRange{ this, "CKResHistoRange", { 0.0026f, 0.002f } };

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_minP{ this, "MinP", { 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV } };

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV } };

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{ this, "SkipElectrons", false, "Skip electrons from monitoring" };

  private:
    // cached data

    // histograms
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResAll         = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResTrue        = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResFake        = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResAllBetaCut  = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResTrueBetaCut = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResFakeBetaCut = { {} };

    mutable Hist::DetArray<Hist::WH1D<>> h_thetaRecTrue = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_phiRecTrue   = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_thetaRecFake = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_phiRecFake   = { {} };

    mutable Hist::DetArray<Hist::WP1D<>> h_ckResTrueVP = { {} };

    mutable Hist::DetArray<Hist::H1D<>> h_ckResAllPion         = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_ckResTruePion        = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_ckResFakePion        = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_ckResAllPionBetaCut  = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_ckResTruePionBetaCut = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_ckResFakePionBetaCut = { {} };

    mutable Hist::DetArray<Hist::WH1D<>> h_ckResTrueInner = { {} };
    mutable Hist::DetArray<Hist::WH1D<>> h_ckResTrueOuter = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_ckexpVptot = { {} };
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rich : activeDetectors() ) {
    const auto rad = radType( rich );

    // beta cut string
    std::string betaS = "beta";
    if ( m_minBeta[rich] > 0.0 ) { betaS = std::to_string( m_minBeta[rich] ) + "<" + betaS; }
    if ( m_maxBeta[rich] < 1.0 ) { betaS = betaS + "<" + std::to_string( m_maxBeta[rich] ); }

    // Plots for MC true type
    ok &= initHist( h_ckResAll[rich],                                   //
                    HID( "ckResAll", rad ),                             //
                    "Rec-Exp CKTheta - True Type",                      //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResTrue[rich],                                  //
                    HID( "ckResTrue", rad ),                            //
                    "Rec-Exp CKTheta - MC true photons - True Type",    //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResFake[rich],                                  //
                    HID( "ckResFake", rad ),                            //
                    "Rec-Exp CKTheta - MC fake photons - True Type",    //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );

    ok &= initHist( h_ckResAllBetaCut[rich],                            //
                    HID( "ckResAllBetaCut", rad ),                      //
                    "Rec-Exp CKTheta - True Type - " + betaS,           //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResTrueBetaCut[rich], HID( "ckResTrueBetaCut", rad ),   //
                    "Rec-Exp CKTheta - MC true photons - True Type - " + betaS, //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResFakeBetaCut[rich],                                   //
                    HID( "ckResFakeBetaCut", rad ),                             //
                    "Rec-Exp CKTheta - MC fake photons - True Type - " + betaS, //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                    "delta(Cherenkov Theta) / rad" );

    ok &= initHist( h_thetaRecTrue[rich],                              //
                    HID( "thetaRecTrue", rad ),                        //
                    "Reconstructed CKTheta - MC true photons",         //
                    m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), //
                    "Cherenkov Theta / rad" );
    ok &= initHist( h_phiRecTrue[rich],                      //
                    HID( "phiRecTrue", rad ),                //
                    "Reconstructed CKPhi - MC true photons", //
                    0.0, 2.0 * Gaudi::Units::pi, nBins1D(),  //
                    "Cherenkov Phi / rad" );
    ok &= initHist( h_thetaRecFake[rich],                              //
                    HID( "thetaRecFake", rad ),                        //
                    "Reconstructed CKTheta - MC fake photons",         //
                    m_ckThetaMin[rich], m_ckThetaMax[rich], nBins1D(), //
                    "Cherenkov Theta / rad" );
    ok &= initHist( h_phiRecFake[rich],                      //
                    HID( "phiRecFake", rad ),                //
                    "Reconstructed CKPhi - MC fake photons", //
                    0.0, 2.0 * Gaudi::Units::pi, nBins1D(),  //
                    "Cherenkov Phi / rad" );
    ok &= initHist( h_ckResTrueVP[rich],                                     //
                    HID( "ckResTrueVP", rad ),                               //
                    "<|Rec-Exp CKTheta|> V P - MC true photons - True Type", //
                    m_minP[rich], m_maxP[rich], nBins1D(),                   //
                    "Track Momentum (MeV/c)", "<|Rec-Exp CKTheta|> / rad" );

    // Plots assuming pion hypo

    ok &= initHist( h_ckResAllPion[rich],                               //
                    HID( "ckResAllPion", rad ),                         //
                    "Rec-Exp CKTheta - Pion Type",                      //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResTruePion[rich],                              //
                    HID( "ckResTruePion", rad ),                        //
                    "Rec-Exp CKTheta - MC true photons - Pion Type",    //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResFakePion[rich],                              //
                    HID( "ckResFakePion", rad ),                        //
                    "Rec-Exp CKTheta - MC fake photons - Pion Type",    //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );

    ok &= initHist( h_ckResAllPionBetaCut[rich],                        //
                    HID( "ckResAllPionBetaCut", rad ),                  //
                    "Rec-Exp CKTheta - Pion Type - " + betaS,           //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResTruePionBetaCut[rich],                               //
                    HID( "ckResTruePionBetaCut", rad ),                         //
                    "Rec-Exp CKTheta - MC true photons - Pion Type - " + betaS, //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                    "delta(Cherenkov Theta) / rad" );
    ok &= initHist( h_ckResFakePionBetaCut[rich],                               //
                    HID( "ckResFakePionBetaCut", rad ),                         //
                    "Rec-Exp CKTheta - MC fake photons - Pion Type - " + betaS, //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(),         //
                    "delta(Cherenkov Theta) / rad" );
    // loop over particle types
    for ( const auto hypo : activeParticlesNoBT() ) {
      ok &= initHist( h_ckexpVptot[rich][hypo],              //
                      HID( "ckexpVptot", rad, hypo ),        //
                      "Expected CK theta V track momentum",  //
                      m_minP[rich], m_maxP[rich], nBins1D(), //
                      "Track Momentum (GeV/c)" );
    }

    // resolution plots for inner and outer regions seperately
    ok &= initHist( h_ckResTrueInner[rich],                             //
                    HID( "ckResTrueInner", rad ),                       //
                    "Rec-Exp CKTheta - MC true photons - Inner Region", //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov theta) / rad" );
    ok &= initHist( h_ckResTrueOuter[rich],                             //
                    HID( "ckResTrueOuter", rad ),                       //
                    "Rec-Exp CKTheta - MC true photons - Outer Region", //
                    -m_ckResRange[rich], m_ckResRange[rich], nBins1D(), //
                    "delta(Cherenkov theta) / rad" );

  } // radiators

  return StatusCode{ ok };
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                            const LHCb::Track::Range&                       tracks,        //
                                            const SIMDPixelSummaries&                       pixels,        //
                                            const Rich::PDPixelCluster::Vector&             clusters,      //
                                            const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&           segments,      //
                                            const CherenkovAngles::Vector&                  expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&              photons,       //
                                            const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                            const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  const Helper mcHelper( tkrels, digitSums );

  // local buffers
  auto hb_ckexpVptot           = h_ckexpVptot.buffer();
  auto hb_ckResAll             = h_ckResAll.buffer();
  auto hb_ckResAllBetaCut      = h_ckResAllBetaCut.buffer();
  auto hb_ckResFake            = h_ckResFake.buffer();
  auto hb_ckResFakeBetaCut     = h_ckResFakeBetaCut.buffer();
  auto hb_thetaRecFake         = h_thetaRecFake.buffer();
  auto hb_phiRecFake           = h_phiRecFake.buffer();
  auto hb_ckResTrue            = h_ckResTrue.buffer();
  auto hb_ckResTrueBetaCut     = h_ckResTrueBetaCut.buffer();
  auto hb_thetaRecTrue         = h_thetaRecTrue.buffer();
  auto hb_phiRecTrue           = h_phiRecTrue.buffer();
  auto hb_ckResTrueVP          = h_ckResTrueVP.buffer();
  auto hb_ckResTrueInner       = h_ckResTrueInner.buffer();
  auto hb_ckResTrueOuter       = h_ckResTrueOuter.buffer();
  auto hb_ckResAllPion         = h_ckResAllPion.buffer();
  auto hb_ckResFakePion        = h_ckResFakePion.buffer();
  auto hb_ckResTruePion        = h_ckResTruePion.buffer();
  auto hb_ckResAllPionBetaCut  = h_ckResAllPionBetaCut.buffer();
  auto hb_ckResFakePionBetaCut = h_ckResFakePionBetaCut.buffer();
  auto hb_ckResTruePionBetaCut = h_ckResTruePionBetaCut.buffer();

  // loop over segments
  for ( const auto&& [seg, ckexp] : Ranges::ConstZip( segments, expTkCKThetas ) ) {
    // radiator
    const auto rich = seg.rich();
    if ( !richIsActive( rich ) ) continue;
    // momentum
    const auto pTot = seg.bestMomentumMag();
    // loop over PID types
    for ( const auto hypo : activeParticlesNoBT() ) { hb_ckexpVptot[rich][hypo][pTot] += ckexp[hypo]; }
  }

  // loop over the photon info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

    // Get the MCParticles for this track
    const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );
    _ri_verbo << "Found " << mcPs.size() << " MCPs for Track " << tk->key() << endmsg;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      _ri_verbo << " -> Segment " << rels.segmentIndex() << endmsg;
      const auto& seg = segments[rels.segmentIndex()];

      // RICH Radiator info
      const auto rich = seg.rich();
      const auto rad  = seg.radiator();
      if ( !richIsActive( rich ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) { continue; }

        // SmartID
        const auto id = phot.smartID()[i];
        _ri_verbo << " -> " << id << endmsg;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // reconstructed phi
        const auto phiRec = phot.CherenkovPhi()[i];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );
        _ri_verbo << " -> Found " << trueCKMCPs.size() << " trueCK MCPs" << endmsg;

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, assume Pion (as in real data)
          if ( Rich::Unknown == pid ) { pid = Rich::Pion; }
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) { continue; }

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          const auto betaC  = ( mcbeta >= m_minBeta[rich] && mcbeta <= m_maxBeta[rich] );

          // true Cherenkov signal ?
          const auto trueCKSig =
              ( mcP ? std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end() : false );
          _ri_verbo << "  -> Matched MCP " << ( mcP ? mcP->key() : -1 ) << " = " << trueCKSig << endmsg;

          // expected CK theta ( for true type )
          const auto thetaExp = expCKangles[pid];

          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // fill some plots
          hb_ckResAll[rich][deltaTheta] += mcPW;
          if ( betaC ) { hb_ckResAllBetaCut[rich][deltaTheta] += mcPW; }
          if ( !trueCKSig ) {
            hb_ckResFake[rich][deltaTheta] += mcPW;
            if ( betaC ) { hb_ckResFakeBetaCut[rich][deltaTheta] += mcPW; }
            hb_thetaRecFake[rich][thetaRec] += mcPW;
            hb_phiRecFake[rich][phiRec] += mcPW;
          } else {
            hb_ckResTrue[rich][deltaTheta] += mcPW;
            if ( betaC ) { hb_ckResTrueBetaCut[rich][deltaTheta] += mcPW; }
            hb_thetaRecTrue[rich][thetaRec] += mcPW;
            hb_phiRecTrue[rich][phiRec] += mcPW;
            hb_ckResTrueVP[rich][pTot] += { fabs( deltaTheta ), mcPW };
            const auto isInner = simdPix.isInnerRegion()[i];
            ( isInner ? hb_ckResTrueInner[rich] : hb_ckResTrueOuter[rich] )[deltaTheta] += mcPW;
          }

        } // loop over associated MCPs

        // Now plots when assuming all tracks are pions ( as in real data )
        {
          // beta for pion hypo
          const auto pionbeta  = richPartProps()->beta( pTot, Rich::Pion );
          const auto pionBetaC = ( pionbeta >= m_minBeta[rich] && pionbeta <= m_maxBeta[rich] );

          // delta theta (w.r.t. pion hypothesis)
          const auto deltaTheta = thetaRec - expCKangles[Rich::Pion];

          // fill some plots
          ++hb_ckResAllPion[rich][deltaTheta];
          if ( trueCKMCPs.empty() ) {
            ++hb_ckResFakePion[rich][deltaTheta];
          } else {
            ++hb_ckResTruePion[rich][deltaTheta];
          }
          if ( pionBetaC ) {
            ++hb_ckResAllPionBetaCut[rich][deltaTheta];
            if ( trueCKMCPs.empty() ) {
              ++hb_ckResFakePionBetaCut[rich][deltaTheta];
            } else {
              ++hb_ckResTruePionBetaCut[rich][deltaTheta];
            }
          }
        } // pion plots

      } // SIMD scalar loop
    }   // photon loop
  }     // track loop
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
