/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Relations
#include "RichFutureMCUtils/RichMCHitUtils.h"
#include "RichFutureMCUtils/RichMCOpticalPhotonUtils.h"
#include "RichFutureMCUtils/RichRecMCHelper.h"

// Kernel
#include "LHCbMath/FastMaths.h"

// STD
#include <algorithm>
#include <array>
#include <cmath>
#include <cstdint>

namespace Rich::Future::Rec::MC::Moni {

  // MC helpers
  using namespace Rich::Future::MC::Relations;

  /** @class OpticalPhotons RichMCOpticalPhotons.h
   *
   *  Resolutions using extended MC Optical Photon objects
   *
   *  @author Chris Jones
   *  @date   2021-08-02
   */

  class OpticalPhotons final
      : public LHCb::Algorithm::Consumer<
            void( const Summary::Track::Vector&,                   //
                  const LHCb::Track::Range&,                       //
                  const SIMDPixelSummaries&,                       //
                  const Rich::PDPixelCluster::Vector&,             //
                  const Relations::PhotonToParents::Vector&,       //
                  const LHCb::RichTrackSegment::Vector&,           //
                  const Relations::SegmentToTrackVector&,          //
                  const SIMDCherenkovPhoton::Vector&,              //
                  const Rich::Future::MC::Relations::TkToMCPRels&, //
                  const PhotonYields::Vector&,                     //
                  const LHCb::MCRichDigitSummarys&,                //
                  const LHCb::MCRichHits&,                         //
                  const LHCb::MCRichSegments&,                     //
                  const LHCb::MCRichTracks&,                       //
                  const LHCb::MCRichOpticalPhotons&,               //
                  const Rich::Utils::RichSmartIDs& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<HistoAlgBase, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    OpticalPhotons( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    { KeyValue{ "SummaryTracksLocation", Summary::TESLocations::Tracks },
                      KeyValue{ "TracksLocation", LHCb::TrackLocation::Default },
                      KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                      KeyValue{ "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default },
                      KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                      KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                      KeyValue{ "SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default },
                      KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                      KeyValue{ "TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles },
                      KeyValue{ "PhotonYieldLocation", PhotonYieldsLocation::Detectable },
                      KeyValue{ "RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default },
                      KeyValue{ "MCRichHitsLocation", LHCb::MCRichHitLocation::Default },
                      KeyValue{ "MCRichSegmentsLocation", LHCb::MCRichSegmentLocation::Default },
                      KeyValue{ "MCRichTracksLocation", LHCb::MCRichTrackLocation::Default },
                      KeyValue{ "MCRichOpticalPhotonsLocation", LHCb::MCRichOpticalPhotonLocation::Default },
                      // input conditions data
                      KeyValue{ "RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey } } ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      // base class initialise
      return Consumer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
      } );
    }

  protected:
    StatusCode prebookHistograms() override {

      bool ok = true;

      using namespace Gaudi::Units;

      const DetectorArray<double> panelXsizes{ { 650, 800 } };
      const DetectorArray<double> panelYsizes{ { 680, 750 } };
      const DetectorArray<double> richXsizes{ { 350, 1300 } };
      const DetectorArray<double> richYsizes{ { 350, 1300 } };
      const DetectorArray<double> shiftXrange{ { 10, 100 } };
      const DetectorArray<double> shiftYrange{ { 10, 25 } };
      const DetectorArray<double> richZMin{ { 800, 9000 } };
      const DetectorArray<double> richZMax{ { 2200, 11800 } };

      // loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        const auto rad = radType( rich );

        // Global X,Y Shifts
        ok &= initHist( h_pixXGloShift[rich],                             //
                        HID( "pixXGloShift", rich ),                      //
                        "Reco-MC Global X hit position",                  //
                        -shiftXrange[rich], shiftXrange[rich], nBins1D(), //
                        "Reco-MC Global X hit position / mm" );
        ok &= initHist( h_pixYGloShift[rich],                             //
                        HID( "pixYGloShift", rich ),                      //
                        "Reco-MC Global Y hit position",                  //
                        -shiftYrange[rich], shiftYrange[rich], nBins1D(), //
                        "Reco-MC Global Y hit position / mm" );
        ok &= initHist( h_pixXGloShift2D[rich],                                //
                        HID( "pixXGloShift2D", rich ),                         //
                        "<Rec-MC Global X> hit position on (x,y) local plane", //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(),      //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(),      //
                        "Panel Local X / mm", "Panel Local Y / mm" );
        ok &= initHist( h_pixYGloShift2D[rich],                                //
                        HID( "pixYGloShift2D", rich ),                         //
                        "<Rec-MC Global Y> hit position on (x,y) local plane", //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(),      //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(),      //
                        "Panel Local X / mm", "Panel Local Y / mm" );
        // Local X,Y Shifts
        ok &= initHist( h_pixXLocShift[rich],                             //
                        HID( "pixXLocShift", rich ),                      //
                        "Reco-MC Local X hit position",                   //
                        -shiftXrange[rich], shiftXrange[rich], nBins1D(), //
                        "Reco-MC Local X hit position / mm" );
        ok &= initHist( h_pixYLocShift[rich],                             //
                        HID( "pixYLocShift", rich ),                      //
                        "Reco-MC Local Y hit position",                   //
                        -shiftYrange[rich], shiftYrange[rich], nBins1D(), //
                        "Reco-MC Local Y hit position / mm" );
        ok &= initHist( h_pixXLocShift2D[rich],                               //
                        HID( "pixXLocShift2D", rich ),                        //
                        "<Rec-MC Local X> hit position on (x,y) local plane", //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(),     //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(),     //
                        "Panel Local X / mm", "Panel Local Y / mm" );
        ok &= initHist( h_pixYLocShift2D[rich],                               //
                        HID( "pixYLocShift2D", rich ),                        //
                        "<Rec-MC Local Y> hit position on (x,y) local plane", //
                        -panelXsizes[rich], panelXsizes[rich], nBins2D(),     //
                        -panelYsizes[rich], panelYsizes[rich], nBins2D(),     //
                        "Panel Local X / mm", "Panel Local Y / mm" );

        // track charges
        for ( const auto charge : { TkCharge::Pos, TkCharge::Neg } ) {
          const std::string cS = ( charge == TkCharge::Pos ? "TkPos" : "TkNeg" );

          ok &= initHist( h_tkMCEmissX[charge][rich],                     //
                          HID( cS + "/tkMCEmissX", rad ),                 //
                          cS + " MC Track Photon Emission X",             //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X Emission point / mm" );
          ok &= initHist( h_tkMCEmissY[charge][rich],                     //
                          HID( cS + "/tkMCEmissY", rad ),                 //
                          cS + " MC Track Photon Emission Y",             //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "MC Y Emission point / mm" );
          ok &= initHist( h_tkMCEmissZ[charge][rich],                //
                          HID( cS + "/tkMCEmissZ", rad ),            //
                          cS + " MC Track Photon Emission Z",        //
                          richZMin[rich], richZMax[rich], nBins1D(), //
                          "MC Z Emission point / mm" );

          ok &= initHist( h_tkRecEmissX[charge][rich],                    //
                          HID( cS + "/tkRecEmissX", rad ),                //
                          cS + " Reco Track Photon Emission X",           //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "Reco X Emission point / mm" );
          ok &= initHist( h_tkRecEmissY[charge][rich],                    //
                          HID( cS + "/tkRecEmissY", rad ),                //
                          cS + " Reco Track Photon Emission Y",           //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "Reco Y Emission point / mm" );
          ok &= initHist( h_tkRecEmissZ[charge][rich],               //
                          HID( cS + "/tkRecEmissZ", rad ),           //
                          cS + " Reco Track Photon Emission Z",      //
                          richZMin[rich], richZMax[rich], nBins1D(), //
                          "Reco Z Emission point / mm" );

          ok &= initHist( h_tkEmissXDiff[charge][rich],                 //
                          HID( cS + "/tkEmissXDiff", rad ),             //
                          "Reco-MC " + cS + " Track Photon Emission X", //
                          -100, 100, nBins1D(),                         //
                          "Reco-MC X Emission point / mm" );
          ok &= initHist( h_tkEmissXDiffvX[charge][rich],                          //
                          HID( cS + "/tkEmissXDiffvX", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission X> Versus X", //
                          -richXsizes[rich], richXsizes[rich], nBins1D(),          //
                          "MC X / mm", "<Reco-MC X Emission point> / mm" );
          ok &= initHist( h_tkEmissXDiffvY[charge][rich],                          //
                          HID( cS + "/tkEmissXDiffvY", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission X> Versus Y", //
                          -richYsizes[rich], richYsizes[rich], nBins1D(),          //
                          "MC Y / mm", "<Reco-MC X Emission point> / mm" );
          ok &= initHist( h_tkEmissXDiffvZ[charge][rich],                          //
                          HID( cS + "/tkEmissXDiffvZ", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission X> Versus Z", //
                          richZMin[rich], richZMax[rich], nBins1D(),               //
                          "MC Z / mm", "<Reco-MC X Emission point> / mm" );
          ok &= initHist( h_tkEmissXDiffvXY[charge][rich],                             //
                          HID( cS + "/tkEmissXDiffvXY", rad ),                         //
                          "<Reco-MC " + cS + " Track Photon Emission X> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),              //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),              //
                          "MC X / mm", "MC Y / mm", "<Reco-MC X Emission point> / mm" );

          ok &= initHist( h_tkEmissYDiff[charge][rich],                 //
                          HID( cS + "/tkEmissYDiff", rad ),             //
                          "Reco-MC " + cS + " Track Photon Emission Y", //
                          -100, 100, nBins1D(),                         //
                          "Reco-MC Y Emission point / mm" );
          ok &= initHist( h_tkEmissYDiffvX[charge][rich],                          //
                          HID( cS + "/tkEmissYDiffvX", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Y> Versus X", //
                          -richXsizes[rich], richXsizes[rich], nBins1D(),          //
                          "MC X / mm", "<Reco-MC Y Emission point> / mm" );
          ok &= initHist( h_tkEmissYDiffvY[charge][rich],                          //
                          HID( cS + "/tkEmissYDiffvY", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Y> Versus Y", //
                          -richYsizes[rich], richYsizes[rich], nBins1D(),          //
                          "MC Y / mm", "<Reco-MC Y Emission point> / mm" );
          ok &= initHist( h_tkEmissYDiffvZ[charge][rich],                          //
                          HID( cS + "/tkEmissYDiffvZ", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Y> Versus Z", //
                          richZMin[rich], richZMax[rich], nBins1D(),               //
                          "MC Z / mm", "<Reco-MC Y Emission point> / mm" );
          ok &= initHist( h_tkEmissYDiffvXY[charge][rich],                             //
                          HID( cS + "/tkEmissYDiffvXY", rad ),                         //
                          "<Reco-MC " + cS + " Track Photon Emission Y> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),              //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),              //
                          "MC X / mm", "MC Y / mm", "<Reco-MC Y Emission point> / mm" );

          ok &= initHist( h_tkEmissZDiff[charge][rich],                 //
                          HID( cS + "/tkEmissZDiff", rad ),             //
                          "Reco-MC " + cS + " Track Photon Emission Z", //
                          -1100, 1100, nBins1D(),                       //
                          "Reco-MC Z Emission point / mm" );
          ok &= initHist( h_tkEmissZDiffvX[charge][rich],                          //
                          HID( cS + "/tkEmissZDiffvX", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Z> Versus X", //
                          -richXsizes[rich], richXsizes[rich], nBins1D(),          //
                          "MC X / mm", "<Reco-MC Z Emission point> / mm" );
          ok &= initHist( h_tkEmissZDiffvY[charge][rich],                          //
                          HID( cS + "/tkEmissZDiffvY", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Z> Versus Y", //
                          -richYsizes[rich], richYsizes[rich], nBins1D(),          //
                          "MC Y / mm", "<Reco-MC Z Emission point> / mm" );
          ok &= initHist( h_tkEmissZDiffvZ[charge][rich],                          //
                          HID( cS + "/tkEmissZDiffvZ", rad ),                      //
                          "<Reco-MC " + cS + " Track Photon Emission Z> Versus Z", //
                          richZMin[rich], richZMax[rich], nBins1D(),               //
                          "MC Z / mm", "<Reco-MC Z Emission point> / mm" );
          ok &= initHist( h_tkEmissZDiffvXY[charge][rich],                             //
                          HID( cS + "/tkEmissZDiffvXY", rad ),                         //
                          "<Reco-MC " + cS + " Track Photon Emission Z> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),              //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),              //
                          "MC X / mm", "MC Y / mm", "<Reco-MC Z Emission point> / mm" );

          ok &= initHist( h_tkMCSlopeX[charge][rich],     //
                          HID( cS + "/tkMCSlopeX", rad ), //
                          cS + " MC Tracks X Slope",      //
                          -0.15, 0.15, nBins1D(), "MC X Slope / mrad" );
          ok &= initHist( h_tkMCSlopeY[charge][rich],     //
                          HID( cS + "/tkMCSlopeY", rad ), //
                          cS + " MC Tracks Y Slope",      //
                          -0.15, 0.15, nBins1D(), "MC Y Slope / mrad" );
          ok &= initHist( h_tkRecSlopeX[charge][rich],     //
                          HID( cS + "/tkRecSlopeX", rad ), //
                          cS + " Reco Tracks X Slope",     //
                          -0.15, 0.15, nBins1D(), "Reco X Slope / mrad" );
          ok &= initHist( h_tkRecSlopeY[charge][rich],     //
                          HID( cS + "/tkRecSlopeY", rad ), //
                          cS + " Reco Tracks Y Slope",     //
                          -0.15, 0.15, nBins1D(), "Reco Y Slope / mrad" );

          ok &= initHist( h_tkMCSlopeXvX[charge][rich],                   //
                          HID( cS + "/tkMCSlopeXvX", rad ),               //
                          cS + " <MC Tracks X Slope> Versus X",           //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X / mm", "<MC X Slope> / mrad" );
          ok &= initHist( h_tkMCSlopeYvY[charge][rich],                   //
                          HID( cS + "/tkMCSlopeYvY", rad ),               //
                          cS + " <MC Tracks Y Slope> Versus Y",           //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC Y / mm", "MC Y Slope / mrad" );
          ok &= initHist( h_tkRecSlopeXvX[charge][rich],                  //
                          HID( cS + "/tkRecSlopeXvX", rad ),              //
                          cS + " <Reco Tracks X Slope> Versus X",         //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X / mm", "Reco X Slope / mrad" );
          ok &= initHist( h_tkRecSlopeYvY[charge][rich],                  //
                          HID( cS + "/tkRecSlopeYvY", rad ),              //
                          cS + " <Reco Tracks Y Slope> Versus Y",         //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC Y / mm", "Reco Y Slope / mrad" );

          ok &= initHist( h_tkAngle[charge][rich],                                //
                          HID( cS + "/tkAngle", rad ),                            //
                          "Angle between " + cS + " Reco and MC Track Direction", //
                          0, m_maxTkAng[rich], nBins1D(),                         //
                          "Rec-MC Track Angle / mrad" );
          ok &= initHist( h_tkAngleVP[charge][rich],                           //
                          HID( cS + "/tkAngleVP", rad ),                       //
                          "<Angle between " + cS +                             //
                              " Reco and MC Track Direction> Versus Momentum", //
                          m_minP[rich], m_maxP[rich], nBins1D(),               //
                          "Track Momentum (MeV/c)", "<Rec-MC Track Angle> / mrad" );
          ok &= initHist( h_tkAngleVX[charge][rich],                      //
                          HID( cS + "/tkAngleVX", rad ),                  //
                          "<Angle between " + cS +                        //
                              " Reco and MC Track Direction> Versus X",   //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X / mm", "<Rec-MC Track Angle> / mrad" );
          ok &= initHist( h_tkAngleVY[charge][rich],                      //
                          HID( cS + "/tkAngleVY", rad ),                  //
                          "<Angle between " + cS +                        //
                              " Reco and MC Track Direction> Versus Y",   //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "MC Y / mm", "<Rec-MC Track Angle> / mrad" );
          ok &= initHist( h_tkAngleVXY[charge][rich],                       //
                          HID( cS + "/tkAngleVXY", rad ),                   //
                          "<Angle between " + cS +                          //
                              " Reco and MC Track Direction> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),   //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),   //
                          "MC X / mm", "MC Y / mm", "<Rec-MC Track Angle> / mrad" );
          ok &= initHist( h_tkAngleX[charge][rich],                                               //
                          HID( cS + "/tkAngleX", rad ),                                           //
                          "X Projection of angle between " + cS + " Reco and MC Track Direction", //
                          -m_maxTkAng[rich], m_maxTkAng[rich], nBins1D(),                         //
                          "Rec-MC Track Angle-X / mrad" );
          ok &= initHist( h_tkAngleXvP[charge][rich],                          //
                          HID( cS + "/tkAngleXvP", rad ),                      //
                          "<X Projection of angle between " + cS +             //
                              " Reco and MC Track Direction> Versus Momentum", //
                          m_minP[rich], m_maxP[rich], nBins1D(),               //
                          "Track Momentum (MeV/c)", "<Rec-MC Track Angle-X> / mrad" );
          ok &= initHist( h_tkAngleXvX[charge][rich],                     //
                          HID( cS + "/tkAngleXvX", rad ),                 //
                          "<X Projection of angle between " + cS +        //
                              " Reco and MC Track Direction> Versus X",   //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X / mm", "<Rec-MC Track Angle-X> / mrad" );
          ok &= initHist( h_tkAngleXvY[charge][rich],                     //
                          HID( cS + "/tkAngleXvY", rad ),                 //
                          "<X Projection of angle between " + cS +        //
                              " Reco and MC Track Direction> Versus Y",   //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "MC Y / mm", "<Rec-MC Track Angle-X> / mrad" );
          ok &= initHist( h_tkAngleXvXY[charge][rich],                      //
                          HID( cS + "/tkAngleXvXY", rad ),                  //
                          "<X Projection of Angle between " + cS +          //
                              " Reco and MC Track Direction> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),   //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),   //
                          "MC X / mm", "MC Y / mm", "<Rec-MC Track Angle-X> / mrad" );
          ok &= initHist( h_tkAngleY[charge][rich],                                               //
                          HID( cS + "/tkAngleY", rad ),                                           //
                          "Y Projection of angle between " + cS + " Reco and MC Track Direction", //
                          -m_maxTkAng[rich], m_maxTkAng[rich], nBins1D(),                         //
                          "Rec-MC Track Angle-Y / mrad" );
          ok &= initHist( h_tkAngleYvP[charge][rich],                          //
                          HID( cS + "/tkAngleYvP", rad ),                      //
                          "<Y Projection of angle between " + cS +             //
                              " Reco and MC Track Direction> Versus Momentum", //
                          m_minP[rich], m_maxP[rich], nBins1D(),               //
                          "Track Momentum (MeV/c)", "<Rec-MC Track Angle-Y> / mrad" );
          ok &= initHist( h_tkAngleYvX[charge][rich],                     //
                          HID( cS + "/tkAngleYvX", rad ),                 //
                          "<Y Projection of angle between " + cS +        //
                              " Reco and MC Track Direction> Versus X",   //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "MC X / mm", "<Rec-MC Track Angle-Y> / mrad" );
          ok &= initHist( h_tkAngleYvY[charge][rich],                     //
                          HID( cS + "/tkAngleYvY", rad ),                 //
                          "<Y Projection of angle between " + cS +        //
                              " Reco and MC Track Direction> Versus Y",   //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "MC Y / mm", "<Rec-MC Track Angle-Y> / mrad" );
          ok &= initHist( h_tkAngleYvXY[charge][rich],                      //
                          HID( cS + "/tkAngleYvXY", rad ),                  //
                          "<Y Projection of Angle between " + cS +          //
                              " Reco and MC Track Direction> Versus (X,Y)", //
                          -richXsizes[rich], richXsizes[rich], nBins2D(),   //
                          -richYsizes[rich], richYsizes[rich], nBins2D(),   //
                          "MC X / mm", "MC Y / mm", "<Rec-MC Track Angle-Y> / mrad" );
        }

        // loop over active mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {

          // Book MC yield histos
          ok &= initHist( h_mcYield[rich][pid],                          //
                          HID( "mcYield", rad, pid ),                    //
                          "MC Photon Yield (>0)",                        //
                          0.5, m_maxYield[rich] + 0.5, m_maxYield[rich], //
                          "Photon Yield (>0)" );
          ok &= initHist( h_mcYieldVp[rich][pid],                //
                          HID( "mcYieldVp", rad, pid ),          //
                          "MC Photon Yield (>0) V P (MeV/c)",    //
                          m_minP[rich], m_maxP[rich], nBins1D(), //
                          "Track Momentum (MeV/c)", "Photon Yield (>0)" );
          ok &= initHist( h_mcYieldVx[rich][pid],                         //
                          HID( "mcYieldVx", rad, pid ),                   //
                          "MC Photon Yield (>0) V Panel Local X",         //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "Panel Local X / mm", "Photon Yield (>0)" );
          ok &= initHist( h_mcYieldVy[rich][pid],                         //
                          HID( "mcYieldVy", rad, pid ),                   //
                          "MC Photon Yield (>0) V Panel Local Y",         //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "Panel Local Y / mm", "Photon Yield (>0)" );
          ok &= initHist( h_mcYieldVxy[rich][pid],                        //
                          HID( "mcYieldVxy", rad, pid ),                  //
                          "MC Photon Yield (>0) V Panel Local (X,Y)",     //
                          -richXsizes[rich], richXsizes[rich], nBins2D(), //
                          -richYsizes[rich], richYsizes[rich], nBins2D(), //
                          "Panel Local X / mm", "Panel Local Y / mm", "Photon Yield (>0)" );
          ok &= initHist( h_mcYieldDiff[rich][pid],           //
                          HID( "mcYieldDiff", rad, pid ),     //
                          "MC Expected-MC Photon Yield (>0)", //
                          -50.0, 50.0, nBins1D(),             //
                          "Exp-MC Photon Yield" );
          ok &= initHist( h_mcYieldDiffVp[rich][pid],                     //
                          HID( "mcYieldDiffVp", rad, pid ),               //
                          "MC Expected-MC Photon Yield (>0) V P (MeV/c)", //
                          1.0 * GeV, 100.0 * GeV, nBins1D(),              //
                          "Track Momentum (MeV/c)", "Exp-MC Photon Yield" );
          // book reco yield histos
          ok &= initHist( h_recoYield[rich][pid],                        //
                          HID( "recoYield", rad, pid ),                  //
                          "Reco Photon Yield (>0)",                      //
                          0.5, m_maxYield[rich] + 0.5, m_maxYield[rich], //
                          "Photon Yield (>0)" );
          ok &= initHist( h_recoYieldVp[rich][pid],              //
                          HID( "recoYieldVp", rad, pid ),        //
                          "Reco Photon Yield (>0) V P (MeV/c)",  //
                          m_minP[rich], m_maxP[rich], nBins1D(), //
                          "Track Momentum (MeV/c)", "Photon Yield (>0)" );
          ok &= initHist( h_recoYieldVx[rich][pid],                       //
                          HID( "recoYieldVx", rad, pid ),                 //
                          "Reco Photon Yield (>0) V Panel Local X",       //
                          -richXsizes[rich], richXsizes[rich], nBins1D(), //
                          "Panel Local X / mm", "Photon Yield (>0)" );
          ok &= initHist( h_recoYieldVy[rich][pid],                       //
                          HID( "recoYieldVy", rad, pid ),                 //
                          "Reco Photon Yield (>0) V Panel Local Y",       //
                          -richYsizes[rich], richYsizes[rich], nBins1D(), //
                          "Panel Local Y / mm", "Photon Yield (>0)" );
          ok &= initHist( h_recoYieldVxy[rich][pid],                      //
                          HID( "recoYieldVxy", rad, pid ),                //
                          "Reco Photon Yield (>0) V Panel Local (X,Y)",   //
                          -richXsizes[rich], richXsizes[rich], nBins2D(), //
                          -richYsizes[rich], richYsizes[rich], nBins2D(), //
                          "Panel Local X / mm", "Panel Local Y / mm", "Photon Yield (>0)" );
          ok &= initHist( h_recoYieldDiff[rich][pid],           //
                          HID( "recoYieldDiff", rad, pid ),     //
                          "Reco Expected-MC Photon Yield (>0)", //
                          -50.0, 50.0, nBins1D(),               //
                          "Exp-MC Photon Yield" );
          ok &= initHist( h_recoYieldDiffVp[rich][pid],                     //
                          HID( "recoYieldDiffVp", rad, pid ),               //
                          "Reco Expected-MC Photon Yield (>0) V P (MeV/c)", //
                          1.0 * GeV, 100.0 * GeV, nBins1D(),                //
                          "Track Momentum (MeV/c)", "Exp-MC Photon Yield" );
        }
      }

      return StatusCode{ ok };
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,    //
                     const LHCb::Track::Range&                       tracks,       //
                     const SIMDPixelSummaries&                       pixels,       //
                     const Rich::PDPixelCluster::Vector&             clusters,     //
                     const Relations::PhotonToParents::Vector&       photToSegPix, //
                     const LHCb::RichTrackSegment::Vector&           segments,     //
                     const Relations::SegmentToTrackVector&          segToTkRel,   //
                     const SIMDCherenkovPhoton::Vector&              photons,      //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,       //
                     const PhotonYields::Vector&                     yields,       //
                     const LHCb::MCRichDigitSummarys&                digitSums,    //
                     const LHCb::MCRichHits&                         mchits,       //
                     const LHCb::MCRichSegments& /* mcsegs */,                     //
                     const LHCb::MCRichTracks& /* mctks */,                        //
                     const LHCb::MCRichOpticalPhotons& mcphotons,                  //
                     const Rich::Utils::RichSmartIDs&  smartIDsHelper ) const override {

      // Make a local MC helper object
      const Helper mcHelper( tkrels, digitSums );
      // MC hit helper
      const MCHitUtils hitHelper( mchits );
      // MC OpticalPhoton helper
      const MCOpticalPhotonUtils photHelper( mcphotons );

      // local buffers
      auto hb_mcYield         = h_mcYield.buffer();
      auto hb_mcYieldVp       = h_mcYieldVp.buffer();
      auto hb_mcYieldVx       = h_mcYieldVx.buffer();
      auto hb_mcYieldVy       = h_mcYieldVy.buffer();
      auto hb_mcYieldVxy      = h_mcYieldVxy.buffer();
      auto hb_mcYieldDiff     = h_mcYieldDiff.buffer();
      auto hb_mcYieldDiffVp   = h_mcYieldDiffVp.buffer();
      auto hb_tkMCEmissX      = h_tkMCEmissX.buffer();
      auto hb_tkMCEmissY      = h_tkMCEmissY.buffer();
      auto hb_tkMCEmissZ      = h_tkMCEmissZ.buffer();
      auto hb_tkRecEmissX     = h_tkRecEmissX.buffer();
      auto hb_tkRecEmissY     = h_tkRecEmissY.buffer();
      auto hb_tkRecEmissZ     = h_tkRecEmissZ.buffer();
      auto hb_tkEmissXDiff    = h_tkEmissXDiff.buffer();
      auto hb_tkEmissXDiffvX  = h_tkEmissXDiffvX.buffer();
      auto hb_tkEmissXDiffvY  = h_tkEmissXDiffvY.buffer();
      auto hb_tkEmissXDiffvZ  = h_tkEmissXDiffvZ.buffer();
      auto hb_tkEmissXDiffvXY = h_tkEmissXDiffvXY.buffer();
      auto hb_tkEmissYDiff    = h_tkEmissYDiff.buffer();
      auto hb_tkEmissYDiffvX  = h_tkEmissYDiffvX.buffer();
      auto hb_tkEmissYDiffvY  = h_tkEmissYDiffvY.buffer();
      auto hb_tkEmissYDiffvZ  = h_tkEmissYDiffvZ.buffer();
      auto hb_tkEmissYDiffvXY = h_tkEmissYDiffvXY.buffer();
      auto hb_tkEmissZDiff    = h_tkEmissZDiff.buffer();
      auto hb_tkEmissZDiffvX  = h_tkEmissZDiffvX.buffer();
      auto hb_tkEmissZDiffvY  = h_tkEmissZDiffvY.buffer();
      auto hb_tkEmissZDiffvZ  = h_tkEmissZDiffvZ.buffer();
      auto hb_tkEmissZDiffvXY = h_tkEmissZDiffvXY.buffer();
      auto hb_tkAngle         = h_tkAngle.buffer();
      auto hb_tkAngleVX       = h_tkAngleVX.buffer();
      auto hb_tkAngleVY       = h_tkAngleVY.buffer();
      auto hb_tkAngleVXY      = h_tkAngleVXY.buffer();
      auto hb_tkAngleVP       = h_tkAngleVP.buffer();
      auto hb_tkMCSlopeX      = h_tkMCSlopeX.buffer();
      auto hb_tkMCSlopeY      = h_tkMCSlopeY.buffer();
      auto hb_tkRecSlopeX     = h_tkRecSlopeX.buffer();
      auto hb_tkRecSlopeY     = h_tkRecSlopeY.buffer();
      auto hb_tkMCSlopeXvX    = h_tkMCSlopeXvX.buffer();
      auto hb_tkMCSlopeYvY    = h_tkMCSlopeYvY.buffer();
      auto hb_tkRecSlopeXvX   = h_tkRecSlopeXvX.buffer();
      auto hb_tkRecSlopeYvY   = h_tkRecSlopeYvY.buffer();
      auto hb_tkAngleX        = h_tkAngleX.buffer();
      auto hb_tkAngleXvP      = h_tkAngleXvP.buffer();
      auto hb_tkAngleXvX      = h_tkAngleXvX.buffer();
      auto hb_tkAngleXvY      = h_tkAngleXvY.buffer();
      auto hb_tkAngleXvXY     = h_tkAngleXvXY.buffer();
      auto hb_tkAngleY        = h_tkAngleY.buffer();
      auto hb_tkAngleYvP      = h_tkAngleYvP.buffer();
      auto hb_tkAngleYvX      = h_tkAngleYvX.buffer();
      auto hb_tkAngleYvY      = h_tkAngleYvY.buffer();
      auto hb_tkAngleYvXY     = h_tkAngleYvXY.buffer();
      auto hb_pixXGloShift    = h_pixXGloShift.buffer();
      auto hb_pixYGloShift    = h_pixYGloShift.buffer();
      auto hb_pixXGloShift2D  = h_pixXGloShift2D.buffer();
      auto hb_pixYGloShift2D  = h_pixYGloShift2D.buffer();
      auto hb_pixXLocShift    = h_pixXLocShift.buffer();
      auto hb_pixYLocShift    = h_pixYLocShift.buffer();
      auto hb_pixXLocShift2D  = h_pixXLocShift2D.buffer();
      auto hb_pixYLocShift2D  = h_pixYLocShift2D.buffer();
      auto hb_recoYield       = h_recoYield.buffer();
      auto hb_recoYieldVp     = h_recoYieldVp.buffer();
      auto hb_recoYieldVx     = h_recoYieldVx.buffer();
      auto hb_recoYieldVy     = h_recoYieldVy.buffer();
      auto hb_recoYieldVxy    = h_recoYieldVxy.buffer();
      auto hb_recoYieldDiff   = h_recoYieldDiff.buffer();
      auto hb_recoYieldDiffVp = h_recoYieldDiffVp.buffer();

      // loop over segments
      for ( const auto&& [seg, tkIndex, yield] : Ranges::ConstZip( segments, segToTkRel, yields ) ) {

        // radiator
        const auto rad  = seg.radiator();
        const auto rich = seg.rich();
        if ( !richIsActive( rich ) ) { continue; }

        // Segment momentum
        const auto pTot = seg.bestMomentumMag();
        if ( pTot < m_minP[rich] || pTot > m_maxP[rich] ) { continue; }

        // Track pointer
        const auto tk = tracks.at( tkIndex );

        // MCP Pointers
        const auto mcPs = mcHelper.mcParticles( *tk, false, 0.75 );

        // Loop over all possibly associated MCPs
        for ( const auto mcP : mcPs ) {
          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          if ( mcP && pid != Rich::Unknown ) {
            // beta cut for true MC type
            const auto mcbeta = richPartProps()->beta( pTot, pid );
            const auto betaC  = ( mcbeta >= m_minBeta[rich] && mcbeta <= m_maxBeta[rich] );
            if ( !betaC ) { continue; }
            if ( yield[pid] > 0 ) {
              // MC Optical Photons
              const auto mcPhots = photHelper.mcOpticalPhotons( mcP );
              // Count MC signal photons in this radiator
              const auto nPhots =                                //
                  std::count_if( mcPhots.begin(), mcPhots.end(), //
                                 [&rad]( const auto mcPhot ) {
                                   const auto mcH = ( mcPhot ? mcPhot->mcRichHit() : nullptr );
                                   return ( mcH && mcH->radiator() == rad && mcH->isSignal() );
                                 } );
              if ( nPhots > 0 ) {
                ++hb_mcYield[rich][pid][nPhots];
                hb_mcYieldVp[rich][pid][pTot] += nPhots;
                hb_mcYieldVx[rich][pid][seg.entryPoint().X()] += nPhots;
                hb_mcYieldVy[rich][pid][seg.entryPoint().Y()] += nPhots;
                hb_mcYieldVxy[rich][pid][{ seg.entryPoint().X(), seg.entryPoint().Y() }] += nPhots;
                const auto yield_diff = yield[pid] - (double)nPhots;
                ++hb_mcYieldDiff[rich][pid][yield_diff];
                hb_mcYieldDiffVp[rich][pid][pTot] += yield_diff;
              }
            }
          }
        } // MCPs

      } // segment data

      // compute tx and ty
      auto get_tx = []( const auto& v ) {
        const auto mag = std::sqrt( v.Mag2() );
        return ( mag > 0 ? v.X() / mag : 0.0 );
      };
      auto get_ty = []( const auto& v ) {
        const auto mag = std::sqrt( v.Mag2() );
        return ( mag > 0 ? v.X() / mag : 0.0 );
      };

      // loop over the tracks
      for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

        // Get the MCParticles for this track
        const auto mcPs = mcHelper.mcParticles( *tk, false, 0.75 );

        // Need MC info so skip tracks without any
        if ( mcPs.empty() ) { continue; }

        // True MC Signal counts for this track
        RadiatorArray<std::size_t>  nSigPhots{ {} };
        RadiatorArray<std::int32_t> segIndex{ -1, -1, -1 };

        // track charge
        const auto charge = ( tk->charge() > 0 ? TkCharge::Pos : TkCharge::Neg );

        // loop over photons for this track
        for ( const auto photIn : sumTk.photonIndices() ) {

          // photon data
          const auto& phot = photons[photIn];
          const auto& rels = photToSegPix[photIn];

          // Get the SIMD summary pixel
          const auto& simdPix = pixels[rels.pixelIndex()];

          // the segment for this photon
          const auto& seg = segments[rels.segmentIndex()];

          // Rich / Radiator info
          const auto rich = seg.rich();
          const auto rad  = seg.radiator();
          segIndex[rad]   = rels.segmentIndex();
          if ( !richIsActive( rich ) ) { continue; }

          // Segment momentum
          const auto pTot = seg.bestMomentumMag();
          if ( pTot < m_minP[rich] || pTot > m_maxP[rich] ) { continue; }

          // Pixel position
          const auto& pixPtnGlo = simdPix.gloPos();
          const auto& pixPtnLoc = simdPix.locPos();

          // Loop over scalar entries in SIMD photon
          for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
            // Select valid entries
            if ( !phot.validityMask()[i] ) { continue; }

            // scalar cluster
            const auto& clus = clusters[simdPix.scClusIndex()[i]];

            // get the MCHits for this cluster
            const auto mcHits = hitHelper.mcRichHits( clus );
            // ... and then the MC Optical Photons also linked to this track's MCParticle(s)
            const auto mcPhots = photHelper.mcOpticalPhotons( mcHits, mcPs );

            // Weight per MCPhoton
            const auto mcPW = ( !mcPhots.empty() ? 1.0 / (double)mcPhots.size() : 1.0 );

            // Count true signal reco photons for each segment
            if ( !mcPhots.empty() ) { ++nSigPhots[rad]; }

            // Loop over MC photons
            for ( const auto mcPhot : mcPhots ) {

              // MCP for this MC photon
              const auto mchit = mcPhot->mcRichHit();
              const auto mcp   = ( mchit ? mchit->mcParticle() : nullptr );
              // MC charge
              const auto mc_charge = ( mcp ? mcp->particleID().threeCharge() / 3 : 0 );
              m_chargeMisMatch += ( ( mc_charge * tk->charge() ) <= 0 );

              // get angle between reco track segment and MC Photon parent direction at emission
              double theta{ 0 }, phi{ 0 };
              seg.angleToDirection<LHCb::RichTrackSegment::FULL_PRECISION, LHCb::RichTrackSegment::FULL_PRECISION>(
                  mcPhot->parentMomentum(), theta, phi );
              theta = 1000 * std::abs( theta ); // convert to use mrad for plots for CK theta
              if ( theta <= m_maxTkAng[rich] ) {
                // get track parameters
                const auto mc_x   = mcPhot->emissionPoint().X();
                const auto mc_y   = mcPhot->emissionPoint().Y();
                const auto mc_z   = mcPhot->emissionPoint().Z();
                const auto rec_x  = seg.bestPoint().X();
                const auto rec_y  = seg.bestPoint().Y();
                const auto rec_z  = seg.bestPoint().Z();
                const auto diff_x = rec_x - mc_x;
                const auto diff_y = rec_y - mc_y;
                const auto diff_z = rec_z - mc_z;
                const auto mc_tx  = get_tx( mcPhot->parentMomentum() );
                const auto mc_ty  = get_ty( mcPhot->parentMomentum() );
                const auto rec_tx = get_tx( seg.bestMomentum() );
                const auto rec_ty = get_ty( seg.bestMomentum() );
                double     sinPhi{ 0 }, cosPhi{ 0 };
                LHCb::Math::fast_sincos( phi, sinPhi, cosPhi );
                const auto xSlopeDiff = theta * cosPhi;
                const auto ySlopeDiff = theta * sinPhi;
                // fill histos
                hb_tkMCEmissX[charge][rich][mc_x] += mcPW;
                hb_tkMCEmissY[charge][rich][mc_y] += mcPW;
                hb_tkMCEmissZ[charge][rich][mc_z] += mcPW;
                hb_tkRecEmissX[charge][rich][rec_x] += mcPW;
                hb_tkRecEmissY[charge][rich][rec_y] += mcPW;
                hb_tkRecEmissZ[charge][rich][rec_z] += mcPW;
                hb_tkEmissXDiff[charge][rich][diff_x] += mcPW;
                hb_tkEmissXDiffvX[charge][rich][mc_x] += { diff_x, mcPW };
                hb_tkEmissXDiffvY[charge][rich][mc_y] += { diff_x, mcPW };
                hb_tkEmissXDiffvZ[charge][rich][mc_z] += { diff_x, mcPW };
                hb_tkEmissXDiffvXY[charge][rich][{ mc_x, mc_y }] += { diff_x, mcPW };
                hb_tkEmissYDiff[charge][rich][diff_y] += mcPW;
                hb_tkEmissYDiffvX[charge][rich][mc_x] += { diff_y, mcPW };
                hb_tkEmissYDiffvY[charge][rich][mc_y] += { diff_y, mcPW };
                hb_tkEmissYDiffvZ[charge][rich][mc_z] += { diff_y, mcPW };
                hb_tkEmissYDiffvXY[charge][rich][{ mc_x, mc_y }] += { diff_y, mcPW };
                hb_tkEmissZDiff[charge][rich][diff_z] += mcPW;
                hb_tkEmissZDiffvX[charge][rich][mc_x] += { diff_z, mcPW };
                hb_tkEmissZDiffvY[charge][rich][mc_y] += { diff_z, mcPW };
                hb_tkEmissZDiffvZ[charge][rich][mc_z] += { diff_z, mcPW };
                hb_tkEmissZDiffvXY[charge][rich][{ mc_x, mc_y }] += { diff_z, mcPW };
                hb_tkAngle[charge][rich][theta] += mcPW;
                hb_tkAngleVX[charge][rich][mc_x] += { theta, mcPW };
                hb_tkAngleVY[charge][rich][mc_y] += { theta, mcPW };
                hb_tkAngleVXY[charge][rich][{ mc_x, mc_y }] += { theta, mcPW };
                hb_tkAngleVP[charge][rich][pTot] += { theta, mcPW };
                hb_tkMCSlopeX[charge][rich][mc_tx] += mcPW;
                hb_tkMCSlopeY[charge][rich][mc_ty] += mcPW;
                hb_tkRecSlopeX[charge][rich][rec_tx] += mcPW;
                hb_tkRecSlopeY[charge][rich][rec_ty] += mcPW;
                hb_tkMCSlopeXvX[charge][rich][mc_x] += { mc_tx, mcPW };
                hb_tkMCSlopeYvY[charge][rich][mc_y] += { mc_ty, mcPW };
                hb_tkRecSlopeXvX[charge][rich][mc_x] += { rec_tx, mcPW };
                hb_tkRecSlopeYvY[charge][rich][mc_y] += { rec_ty, mcPW };
                hb_tkAngleX[charge][rich][xSlopeDiff] += mcPW;
                hb_tkAngleXvP[charge][rich][pTot] += { xSlopeDiff, mcPW };
                hb_tkAngleXvX[charge][rich][mc_x] += { ySlopeDiff, mcPW };
                hb_tkAngleXvY[charge][rich][mc_y] += { ySlopeDiff, mcPW };
                hb_tkAngleXvXY[charge][rich][{ mc_x, mc_y }] += { xSlopeDiff, mcPW };
                hb_tkAngleY[charge][rich][ySlopeDiff] += mcPW;
                hb_tkAngleYvP[charge][rich][pTot] += { ySlopeDiff, mcPW };
                hb_tkAngleYvX[charge][rich][mc_x] += { ySlopeDiff, mcPW };
                hb_tkAngleYvY[charge][rich][mc_y] += { ySlopeDiff, mcPW };
                hb_tkAngleYvXY[charge][rich][{ mc_x, mc_y }] += { ySlopeDiff, mcPW };
              }

              // Hit position resolution
              const auto& mcHitPtnGlo = mcPhot->pdIncidencePoint();
              const auto  mcHitPtnLoc = smartIDsHelper.globalToPDPanel( mcHitPtnGlo );
              const auto  xLoc        = pixPtnLoc.X()[i];
              const auto  yLoc        = pixPtnLoc.Y()[i];
              const auto  xDiffGlo    = pixPtnGlo.X()[i] - mcHitPtnGlo.X();
              const auto  yDiffGlo    = pixPtnGlo.Y()[i] - mcHitPtnGlo.Y();
              const auto  xDiffLoc    = xLoc - mcHitPtnLoc.X();
              const auto  yDiffLoc    = yLoc - mcHitPtnLoc.Y();
              ++hb_pixXGloShift[rich][xDiffGlo];
              ++hb_pixYGloShift[rich][yDiffGlo];
              hb_pixXGloShift2D[rich][{ xLoc, yLoc }] += xDiffGlo;
              hb_pixYGloShift2D[rich][{ xLoc, yLoc }] += yDiffGlo;
              ++hb_pixXLocShift[rich][xDiffLoc];
              ++hb_pixYLocShift[rich][yDiffLoc];
              hb_pixXLocShift2D[rich][{ xLoc, yLoc }] += xDiffLoc;
              hb_pixYLocShift2D[rich][{ xLoc, yLoc }] += yDiffLoc;

            } // MC photons

          } // SIMD scalar loop

        } // photons

        // Reco yield plots. Loop over all possibly associated MCPs
        for ( const auto mcP : mcPs ) {
          const auto pid = mcHelper.mcParticleType( mcP );
          if ( mcP && pid != Rich::Unknown ) {
            for ( const auto rad : activeRadiators() ) {
              if ( segIndex[rad] >= 0 ) {
                const auto& yield = yields[segIndex[rad]];
                if ( yield[pid] > 0 ) {
                  const auto& seg  = segments[segIndex[rad]];
                  const auto  pTot = seg.bestMomentumMag();
                  const auto  rich = seg.rich();
                  // beta cut for true MC type
                  const auto mcbeta = richPartProps()->beta( pTot, pid );
                  const auto betaC  = ( mcbeta >= m_minBeta[rich] && mcbeta <= m_maxBeta[rich] );
                  if ( !betaC ) { continue; }
                  const auto nPhots = nSigPhots[rad];
                  if ( nPhots > 0 ) {
                    ++hb_recoYield[rich][pid][nPhots];
                    hb_recoYieldVp[rich][pid][pTot] += nPhots;
                    hb_recoYieldVx[rich][pid][seg.entryPoint().X()] += nPhots;
                    hb_recoYieldVy[rich][pid][seg.entryPoint().Y()] += nPhots;
                    hb_recoYieldVxy[rich][pid][{ seg.entryPoint().X(), seg.entryPoint().Y() }] += nPhots;
                    const auto yield_diff = yield[pid] - (double)nPhots;
                    ++hb_recoYieldDiff[rich][pid][yield_diff];
                    hb_recoYieldDiffVp[rich][pid][pTot] += yield_diff;
                  }
                }
              }
            }
          }
        }

      } // tracks
    }

  private:
    // histos

    enum TkCharge : std::uint16_t { Pos = 0, Neg = 1 };
    template <typename T>
    using ChargeArray = Hist::Array<T, 2, TkCharge>;

    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkMCSlopeX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkMCSlopeY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkRecSlopeX = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkRecSlopeY = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkMCSlopeXvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkMCSlopeYvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkRecSlopeXvX = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkRecSlopeYvY = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkAngle     = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleVP   = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleVX   = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleVY   = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkAngleVXY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkAngleX    = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleXvP  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleXvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleXvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkAngleXvXY = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkAngleY    = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleYvP  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleYvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkAngleYvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkAngleYvXY = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkMCEmissX = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkMCEmissY = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkMCEmissZ = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkRecEmissX = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkRecEmissY = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkRecEmissZ = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkEmissXDiff = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkEmissYDiff = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WH1D<>>> h_tkEmissZDiff = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissXDiffvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissXDiffvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissXDiffvZ  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkEmissXDiffvXY = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissYDiffvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissYDiffvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissYDiffvZ  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkEmissYDiffvXY = { { {} } };

    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissZDiffvX  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissZDiffvY  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP1D<>>> h_tkEmissZDiffvZ  = { { {} } };
    mutable ChargeArray<Hist::DetArray<Hist::WP2D<>>> h_tkEmissZDiffvXY = { { {} } };

    mutable Hist::DetArray<Hist::H1D<>> h_pixXGloShift   = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_pixYGloShift   = { {} };
    mutable Hist::DetArray<Hist::P2D<>> h_pixXGloShift2D = { {} };
    mutable Hist::DetArray<Hist::P2D<>> h_pixYGloShift2D = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_pixXLocShift   = { {} };
    mutable Hist::DetArray<Hist::H1D<>> h_pixYLocShift   = { {} };
    mutable Hist::DetArray<Hist::P2D<>> h_pixXLocShift2D = { {} };
    mutable Hist::DetArray<Hist::P2D<>> h_pixYLocShift2D = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_mcYield       = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_mcYieldVp     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_mcYieldVx     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_mcYieldVy     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P2D<>>> h_mcYieldVxy    = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_mcYieldDiff   = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_mcYieldDiffVp = { {} };

    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_recoYield       = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_recoYieldVp     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_recoYieldVx     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_recoYieldVy     = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P2D<>>> h_recoYieldVxy    = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::H1D<>>> h_recoYieldDiff   = { {} };
    mutable Hist::DetArray<Hist::PartArray<Hist::P1D<>>> h_recoYieldDiffVp = { {} };

  private:
    // counters

    // warning for when the charge of the reco and its associated MCParticle do not match
    mutable Gaudi::Accumulators::BinomialCounter<> m_chargeMisMatch{
        this, "Charge mis-match between reco track and matched MCParticle" };

  private:
    // JOs

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_minP{
        this, "MinP", { 3.0 * Gaudi::Units::GeV, 16.0 * Gaudi::Units::GeV } };

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<DetectorArray<double>> m_maxP{
        this, "MaxP", { 80.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV } };

    /// minimum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_minBeta{ this, "MinBeta", { 0.9999f, 0.9999f } };

    /// maximum beta value for tracks
    Gaudi::Property<DetectorArray<float>> m_maxBeta{ this, "MaxBeta", { 999.99f, 999.99f } };

    /// Maximum photon yield
    Gaudi::Property<DetectorArray<float>> m_maxYield{ this, "MaximumYields", { 80, 80 } };

    /// Max track angle
    Gaudi::Property<DetectorArray<double>> m_maxTkAng{ this, "MaxTkAngle", { 5.0, 4.0 } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( OpticalPhotons )

} // namespace Rich::Future::Rec::MC::Moni
