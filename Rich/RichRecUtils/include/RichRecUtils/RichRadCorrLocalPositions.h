/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STD
#include <ostream>
#include <vector>

// Gaudi
#include "GaudiKernel/Point3DTypes.h"

// Kernel
#include "Kernel/MemPoolAlloc.h"
#include "Kernel/RichRadiatorType.h"

namespace Rich::Rec {

  /** @class RadPositionCorrector RichRecBase/RichRadCorrLocalPositions.h
   *
   *  Simple class to compute the local coordinate correction.
   *
   *  Need to migrate away from this for the upgrade, but keep for now.
   *
   *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   *  @date   2007-03-28
   */
  template <typename TYPE>
  class RadPositionCorrector final {
  public:
    /// Correct x coordinate
    inline TYPE correctX( const TYPE x, const Rich::RadiatorType rad ) const noexcept {
      return ( TYPE( 1.0 ) - m_radScale[rad] ) * x;
    }
    /// Correct y coordinate
    inline TYPE correctY( const TYPE y, const Rich::RadiatorType rad ) const noexcept {
      return ( TYPE( 1.0 ) + m_radScale[rad] ) * y;
    }
    /// Returns the corrected local position, for the given radiator
    template <typename POINT>
    inline POINT correct( const POINT& point, const Rich::RadiatorType rad ) const {
      return POINT( correctX( point.x(), rad ), correctY( point.y(), rad ), point.z() );
    }
    /// Access the correction parameters
    inline const RadiatorArray<TYPE>& params() const noexcept { return m_radScale; }

  private:
    /// The radiator correction parameters
    const RadiatorArray<TYPE> m_radScale = { { TYPE( 0.03 ), TYPE( 0.0117 ), TYPE( -0.021 ) } };
  };

  /** @class RadCorrLocalPositions RichRecBase/RichRadCorrLocalPositions.h
   *
   *  Simple class to store three local coordinate positions, the corrected positions for each
   * radiator
   *
   *  @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   *  @date   2007-03-28
   */

  class RadCorrLocalPositions final : public LHCb::MemPoolAlloc<Rich::Rec::RadCorrLocalPositions> {

  public:
    /// Standard constructor
    RadCorrLocalPositions() = default;

    /// Constructor from a default position
    RadCorrLocalPositions( const Gaudi::XYZPoint& point ) { m_pos.fill( point ); }

    /// Get the position for the given radiator (const)
    [[nodiscard]] inline const Gaudi::XYZPoint& position( const Rich::RadiatorType rad ) const noexcept {
      return m_pos[rad];
    }

    /// Get the position for the given radiator (non const)
    inline Gaudi::XYZPoint& position( const Rich::RadiatorType rad ) noexcept { return m_pos[rad]; }

    /// Set the position for the given radiator
    inline void setPosition( const Rich::RadiatorType rad, const Gaudi::XYZPoint& point ) noexcept {
      m_pos[rad] = point;
    }

    /// overload printout to ostream operator <<
    friend inline std::ostream& operator<<( std::ostream& s, const Rich::Rec::RadCorrLocalPositions& pos ) {
      return s << "[ "                                   //
               << pos.position( Rich::Aerogel ) << ", "  //
               << pos.position( Rich::Rich1Gas ) << ", " //
               << pos.position( Rich::Rich2Gas ) << " ]";
    }

  private:
    /// The corrected local positions
    Rich::RadiatorArray<Gaudi::XYZPoint> m_pos;
  };

} // namespace Rich::Rec
