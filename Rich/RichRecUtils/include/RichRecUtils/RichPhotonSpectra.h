/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichPhotonSpectra.h
 *
 *  Header file for utility class : Rich::PhotonSpectra
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-07-12
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <algorithm>
#include <array>
#include <functional>
#include <numeric>
#include <ostream>
#include <sstream>
#include <type_traits>

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/MemPoolAlloc.h"
#include "Kernel/RichParticleIDType.h"

// Rich Utils
#include "RichUtils/StlArray.h"

// Boost
#include "boost/format.hpp"

namespace Rich {

  /** @class PhotonSpectra RichPhotonSpectra.h RichRecBase/RichPhotonSpectra.h
   *
   *  A utility class that can be used to describe a generic distribution
   *  as a function of photon energy. Uses a binned approach to describe the
   *  distribution.
   *
   *  NB : The templation will only work for numerical (float, double etc.) types.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-07-12
   */

  template <typename TYPE,                //
            unsigned int NEnergyBins = 5, //
            typename                 = typename std::enable_if_t<std::is_arithmetic_v<TYPE>>>
  class PhotonSpectra final : public LHCb::MemPoolAlloc<PhotonSpectra<TYPE>> {

  public:
    // definitions

    /// Number of energy bins
    static unsigned int const EnergyBins = NEnergyBins;

    /// Definition of the underlaying distribution container
    using PhotonData = std::array<TYPE, NEnergyBins>;

    /// Definition of the container used for the particle hypothesis distributions
    using HypoPhotonData = ParticleArray<PhotonData>;

    /// Type for vector of PhotonSpectra
    using Vector = LHCb::STL::Vector<PhotonSpectra>;

  public:
    // constructors

    /// Default Constructor
    PhotonSpectra() = default;

    /** Constructor from data
     *
     *  @param minEn  Minimum energy value
     *  @param maxEn  Maximum energy value
     */
    PhotonSpectra( const TYPE minEn, const TYPE maxEn )
        : m_minEn( minEn ), m_maxEn( maxEn ), m_binSize( ( maxEn - minEn ) / NEnergyBins ) {}

    /** Constructor from data
     *
     *  @param range (min,max) energy pair
     */
    explicit PhotonSpectra( const std::pair<TYPE, TYPE>& range ) : PhotonSpectra( range.first, range.second ) {}

  public:
    // access methods

    /** Access the number of energy bins
     *  @return The number of energy bins
     */
    [[nodiscard]] inline constexpr unsigned int energyBins() const noexcept { return NEnergyBins; }

    /** Access the minimum energy for the distribution
     *  @return The minimum energy value
     */
    inline TYPE minEnergy() const noexcept { return m_minEn; }

    /** Access the maximum photon energy
     *  @return The maximum energy value
     */
    inline TYPE maxEnergy() const noexcept { return m_maxEn; }

    /** Access the energy bin size
     *  @return The energy bins size
     */
    inline TYPE binSize() const noexcept { return m_binSize; }

    /** Access the lower edge of the given energy bin
     *  @param bin The energy bin
     *  @return The energy of the lower edge of the bin
     */
    inline TYPE binEnergyLowerEdge( const unsigned int bin ) const noexcept {
      return ( minEnergy() + ( (TYPE)( bin ) ) * binSize() );
    }

    /** Access the upper edge of the energy bin
     *  @param bin The energy bin
     *  @return The energy of the upper edge of the bin
     */
    inline TYPE binEnergyUpperEdge( const unsigned int bin ) const noexcept {
      return ( minEnergy() + ( (TYPE)( 1 + bin ) ) * binSize() );
    }

    /** Access the average bin energy
     *  @param bin The energy bin
     *  @return The average energy value for the given bin
     */
    inline TYPE binEnergy( const unsigned int bin ) const noexcept {
      return ( minEnergy() + ( (TYPE)( 0.5 + bin ) ) * binSize() );
    }

    /** Access the energy distribution for a given mass hypothesis (non-const)
     *  @param id The mass hypothesis
     *  @return The energy distribution for the given mass hypothesis
     */
    inline typename PhotonSpectra::PhotonData& energyDist( const Rich::ParticleIDType id ) noexcept {
      return m_photdata[id];
    }

    /** Access the energy distribution for a given mass hypothesis (const)
     *  @param id The mass hypothesis
     *  @return The energy distribution for the given mass hypothesis
     */
    inline const typename PhotonSpectra::PhotonData& energyDist( const Rich::ParticleIDType id ) const noexcept {
      return m_photdata[id];
    }

    /** Computes the integral of the energy distribution for a given mass hypothesis
     *  @param id The mass hypothesis
     *  @return The integral of the energy distribution, over the full energy range
     */
    inline TYPE integral( const Rich::ParticleIDType id ) const {
      const auto& dist = energyDist( id );
      return std::accumulate( dist.begin(), dist.end(), (TYPE)0 );
    }

    /** multiply by another distribution
     *
     *  @param id   The mass hypothesis distribution to act upon
     *  @param data The distribution to multiple by
     *
     *  @return Status of the operation
     *  @retval true  Multiplication was successful
     *  @retval false Multiplication failed
     */
    bool multiply( const Rich::ParticleIDType id, const typename PhotonSpectra::PhotonData& data ) {
      bool OK = false;
      if ( this->energyBins() == data.size() ) {
        auto& dist = energyDist( id );
        std::transform( dist.begin(), dist.end(), data.begin(), dist.begin(), std::multiplies<TYPE>() );
        OK = true;
      }
      return OK;
    }

    /// Reset the data
    inline void reset() { /* nothing yet */
    }

  public:
    // messaging

    /// Implement ostream << method for PhotonSpectra<TYPE>
    friend inline std::ostream& operator<<( std::ostream& os, const PhotonSpectra<TYPE>& spectra ) {
      // Formatting strings
      const std::string fF = "%6.2f"; // floats
      os << std::endl
         << "[ #bins = " << spectra.energyBins() << " Min En = " << spectra.minEnergy()
         << " Max En = " << spectra.maxEnergy() << std::endl;
      for ( const auto id : Rich::particles() ) {
        std::ostringstream sID;
        sID << id;
        auto tID = sID.str();
        tID.resize( 15, ' ' );
        os << "  " << tID << " |";
        for ( unsigned int i = 0; i < spectra.energyBins(); ++i ) {
          os << " (" << boost::format( fF ) % spectra.binEnergy( i ) << ","
             << boost::format( fF ) % spectra.energyDist( id )[i] << ")";
        }
        os << std::endl;
      }
      return os << " ]";
    }

  private:
    // data

    TYPE m_minEn{ 0 };           ///< minimum energy for which the distribution is defined
    TYPE m_maxEn{ NEnergyBins }; ///< maximum energy for which the distribution is defined
    TYPE m_binSize{ 1 };         ///< energy bin size

    /// The data container
    HypoPhotonData m_photdata = { {} };
  };

} // namespace Rich
