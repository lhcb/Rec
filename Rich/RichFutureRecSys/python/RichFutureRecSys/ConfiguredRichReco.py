###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from builtins import range, str

from GaudiKernel.SystemOfUnits import GeV


def RichTESPixelMap(name="RICH1RICH2"):
    # Common data
    locs = {
        "RichPixelClustersLocation": "Rec/Rich/PixelClusters/" + name,
        "RichPixelGlobalPositionsLocation": "Rec/Rich/PixelPositions/Global/" + name,
        "RichPixelLocalPositionsLocation": "Rec/Rich/PixelPositions/Local/" + name,
        "RichSIMDPixelSummariesLocation": "Rec/Rich/SIMDPixelSummaries/" + name,
    }
    return locs


def RichTESTrackMap(name):
    # Contruct the naming map for the given name
    locs = {
        # Track data
        "TrackSegmentsLocation": "Rec/Rich/TrackSegments/" + name,
        # Segment data
        "SegmentToTrackLocation": "Rec/Rich/Relations/SegmentToTrack/" + name,
        "TrackGlobalPointsLocation": "Rec/Rich/SegmentPositions/Global/" + name,
        "TrackLocalPointsLocation": "Rec/Rich/SegmentPositions/Local/" + name,
        "EmittedPhotonYieldLocation": "Rec/Rich/PhotonYields/Emitted/" + name,
        "EmittedPhotonSpectraLocation": "Rec/Rich/PhotonSpectra/Emitted/" + name,
        "EmittedCherenkovAnglesLocation": "Rec/Rich/CherenkovAngles/Emitted/" + name,
        "SignalCherenkovAnglesLocation": "Rec/Rich/CherenkovAngles/Signal/" + name,
        "EmittedMassHypothesisRingsLocation": "Rec/Rich/MassHypoRings/Emitted/" + name,
        "DetectablePhotonYieldLocation": "Rec/Rich/PhotonYields/Detectable/" + name,
        "DetectablePhotonSpectraLocation": "Rec/Rich/PhotonSpectra/Detectable/" + name,
        "SignalPhotonYieldLocation": "Rec/Rich/PhotonYields/Signal/" + name,
        "SignalPhotonSpectraLocation": "Rec/Rich/PhotonSpectra/Signal/" + name,
        "GeomEffsLocation": "Rec/Rich/GeomEffs/" + name,
        "GeomEffsPerPDLocation": "Rec/Rich/GeomEffsPerPD/" + name,
        "SegmentPhotonFlagsLocation": "Rec/Rich/SegmentPhotonFlags/" + name,
        "CherenkovResolutionsLocation": "Rec/Rich/CherenkovResolutions/" + name,
        # photon data
        "CherenkovPhotonLocation": "Rec/Rich/CherenkovPhotons/" + name,
        "PhotonSignalsLocation": "Rec/Rich/PhotonPixelSignals/" + name,
        # (optionally filled) photon data
        "PhotonMirrorDataLocation": "Rec/Rich/PhotonMirrorData/" + name,
        # relations
        "InitialTrackToSegmentsLocation": "Rec/Rich/Relations/TrackToSegments/Initial/"
        + name,
        "SelectedTrackToSegmentsLocation": "Rec/Rich/Relations/TrackToSegments/Selected/"
        + name,
        "PhotonToParentsLocation": "Rec/Rich/Relations/PhotonToParents/" + name,
        # Summary data
        "SummaryTracksLocation": "Rec/Rich/Summary/Tracks/" + name,
        "SummaryPixelsLocation": "Rec/Rich/Summary/Pixels/" + name,
    }
    return locs


# Returns the default Track ROI pre-sel cuts
def defaultMinMaxTrackROICuts():
    return {
        "Nominal": {"Min": (0, 0), "Max": (110, 165)},
        "Online": {"Min": (0, 0), "Max": (120, 175)},
        "None": {"Min": (0, 0), "Max": (200, 200)},
    }


# Returns the default NSigma Cuts Dictionary
def defaultNSigmaCuts():
    return {
        "Nominal": {
            "Functional": {
                "Down": [(6, 22), (3.6, 4.5)],
                "Long": [(5, 24), (3.6, 4.5)],
                "Seed": [(5, 5), (3.6, 4.5)],
                "Up": [(7, 7), (3.6, 4.5)],
                "MC": [(5, 24), (3.6, 4.5)],
            },
            "Parameterised": {
                "Down": [(6.5, 14), (3.5, 4.0)],
                "Long": [(6, 14), (4.0, 4.5)],
                "Seed": [(5, 5), (3.5, 3.5)],
                "Up": [(6, 6), (3.5, 3.5)],
                "MC": [(6, 14), (4.0, 4.5)],
            },
        },
        "Online": {
            "Functional": {
                "Down": [(45, 35), (30, 12)],
                "Long": [(45, 35), (30, 12)],
                "Seed": [(45, 35), (30, 12)],
                "Up": [(45, 35), (30, 12)],
                "MC": [(45, 35), (30, 12)],
            },
            "Parameterised": {
                "Down": [(35, 35), (25, 15)],
                "Long": [(35, 35), (25, 15)],
                "Seed": [(35, 35), (25, 15)],
                "Up": [(35, 35), (25, 15)],
                "MC": [(35, 35), (25, 15)],
            },
        },
        "None": {
            "Functional": {
                "Down": [(99999, 99999), (99999, 99999)],
                "Long": [(99999, 99999), (99999, 99999)],
                "Seed": [(99999, 99999), (99999, 99999)],
                "Up": [(99999, 99999), (99999, 99999)],
                "MC": [(99999, 99999), (99999, 99999)],
            },
            "Parameterised": {
                "Down": [(99999, 99999), (99999, 99999)],
                "Long": [(99999, 99999), (99999, 99999)],
                "Seed": [(99999, 99999), (99999, 99999)],
                "Up": [(99999, 99999), (99999, 99999)],
                "MC": [(99999, 99999), (99999, 99999)],
            },
        },
    }


# Returns min CK photon probability values
def defaultMinPhotonProbabilityCuts():
    return {"Nominal": (1e-15, 1e-15), "Online": (1e-35, 1e-35), "None": (0, 0)}


# Returns the default min/max allowed cherenov theta cuts
def defaultMinMaxCKThetaCuts():
    return {
        "Nominal": {"Min": (0.005, 0.005), "Max": (0.055, 0.032)},
        "Online": {"Min": (0.005, 0.005), "Max": (0.080, 0.035)},
        "None": {"Min": (0.005, 0.005), "Max": (0.150, 0.120)},
    }


# Returns max CK theta resolution values by track type
def defaultMaxCKThetaResolutions():
    return {
        "MC": (0.0030, 0.0015),
        "Long": (0.0030, 0.0015),
        "Down": (0.0050, 0.002),
        "Up": (0.0080, 0.002),
        "Seed": (0.0050, 0.005),
    }


# makes an instance of an algorithm with the given properties set
def makeRichAlg(type, name, props, track_type=""):
    alg = type(name)
    for prop, value in props.items():
        lv = value
        # Hack to turn off RICH2 for Upstream tracks.
        if "Up" == track_type or "Upstream" == track_type:
            if "Detectors" == prop:
                lv = (value[0], False)
        # Hack to turn off RICH1 for Seed tracks.
        if "Seed" == track_type:
            if "Detectors" == prop:
                lv = (False, value[1])
        try:
            alg.setProp(prop, lv)
        except AttributeError:
            # Does not have this property so just continue
            pass
    return alg


# Creates a default Track location map given the list of types
def trackTESLocations(types=["Long", "Down", "Up"]):
    map = {}
    for type in types:
        map[type] = "Rec/Track/BestRich" + type
    return map


# Creates a default PID location map given the list of types
def pidTESLocations(types=["Long", "Down", "Up"]):
    map = {}
    for type in types:
        map[type] = "Rec/Rich/" + type + "PIDs"
    return map


# Utility method to check the consistency of the configured TES locations
def CheckConfig(inputTrackLocations, outputPIDLocations):
    if 0 == len(list(inputTrackLocations.keys())):
        raise ValueError("No input track locations specified")
    if 0 == len(list(outputPIDLocations.keys())):
        raise ValueError("No output PID locations specified")
    if inputTrackLocations.keys() != outputPIDLocations.keys():
        raise ValueError("Track and PID keys do not match")


def RichRecoSequence(
    GroupName="",  # Optional name given to this group.
    # ===========================================================
    # General Data processing options
    # ===========================================================
    # The data type
    dataType="",
    # Flag to indicate we are running in 'Online Brunel' mode
    onlineBrunelMode=False,
    # PID types to process
    particles=[
        "electron",
        "muon",
        "pion",
        "kaon",
        "proton",
        "deuteron",
        "belowThreshold",
    ],
    # radiators to process
    radiators=["Rich1Gas", "Rich2Gas"],
    # enable 4D reco in each radiator
    enable4DReco=(False, False),
    # ===========================================================
    # Input / Output options
    # ===========================================================
    # TES location for decoded data
    decodedDataLocation="Raw/Rich/DecodedData/RICH1RICH2",
    # Dict of input track locations with name key
    inputTrackLocations={"All": "Rec/Track/Best"},
    # Dict of output PID locations. Name keys must match above.
    outputPIDLocations={"All": "Rec/Rich/PIDs"},
    # Merged PID location.
    # If set, merges the above into one final location
    mergedOutputPIDLocation="",
    # PID version
    pidVersion=2,
    # ===========================================================
    # Pixel treatment options
    # ===========================================================
    # Maximum number of clusters GEC cut
    maxClusters=200000,
    # Maximum PD occupancy
    maxPDOccupancy=(999999, 999999),
    # Should pixel clustering be run, for either ( RICH1, RICH2 )
    applyPixelClustering=(False, False),
    # Average hit time expected in each RICH (for 4D reco)
    averageHitTime=(13.03, 52.94),
    # Course time window for hit selection (in ns)
    pixelTimeWindow=(1.0, 1.0),
    # MC cheating options
    # Use MC to cheat pixel hit positions
    pixUsePosFromMC=False,
    # Use MC to cheat pixel hit times
    pixUseTimeFromMC=False,
    # Override Inner/Outer regions
    pixOverrideRegions=None,
    # ===========================================================
    # Track treatment options
    # ===========================================================
    # Minimum momentum cuts for each RICH
    MinP=(0 * GeV, 0 * GeV),
    # Maximum momentum cuts for each RICH
    MaxP=(9e90 * GeV, 9e90 * GeV),
    # Minimum transverse momentum cut
    MinPt=0 * GeV,
    # Maximum number of tracks GEC cut
    maxTracks=10000,
    # Disable non-tread-safe tracking tools
    makeTkToolsThreadSafe=False,
    # Max Number points for mass hypothesis ring ray tracing
    NRingPointsMax=(96, 96),
    # Min Number points for mass hypothesis ring ray tracing
    NRingPointsMin=(16, 16),
    # Tolerence for creating new ray traced CK rings (as fraction of sat. CK theta)
    NewCKRingTol=(0.05, 0.1),
    # Track Extrapolator type
    trackExtrapolator="TrackSTEPExtrapolator/TrackExtrapolator",
    # Allow missing track states to be created on the fly
    createMissingStates=False,
    # Detectable Yield calculation
    detYieldPrecision="Average",
    # Treatment of track CK resolutions
    tkCKResTreatment="Parameterised",
    # Track CK theta resolution scale factors
    tkCKResScaleFactors=None,
    # MC cheating options
    # Add 4D origin vertex info to track segments using MC
    tkSegAddTimeFromMC=False,
    # Use MC to update expected CK theta values for each segment
    tkSegUseMCExpCKTheta=False,
    # ===========================================================
    # Photon treatment options
    # ===========================================================
    # The photon reconstruction to use. Supports either :-
    #  "Quartic"          - Full solution using the analytic solution.
    #  "FastQuartic"      - Stripped down quartic algorithm.
    #  "CKEstiFromRadius" - Estimation using the ray tracing information
    photonReco="Quartic",
    # Photon selection cut setting
    #   Nominal - Normal tuned settings
    #   Online  - Loosen settings for Online running (wider side-bands)
    #   None    - Effectively no (pre)selection cuts.
    photonSelection="Nominal",
    # Truncate Cherenkov angles to a fixed precision
    truncateCKAngles=(True, True),
    # Minimum photon probability (None means use defaults)
    minPhotonProbability=None,
    # N Sigma cuts by selection, CK resolution model and track type
    # None means use defaults
    nSigmaCuts=None,
    # Min/Max photon CK theta angle cuts
    minMaxCKThetaCuts=None,
    # Timing window for photon reconstruction (in ns) for (inner,outer) regions
    # Only used if 4D reconstruction is active
    photonTimeWindow=((0.25, 0.25), (0.25, 0.25)),
    # MC cheating options
    # Use MC to cheat photon parameters
    photUseMC=False,
    # Function to use when emulating a given CK resolution for each RICH
    photEmulatedResMC=None,
    # ===========================================================
    # Settings for the global PID minimisation
    # ===========================================================
    # Number of iterations of the global PID background and
    # likelihood minimisation
    nIterations=2,
    # The following are technical settings per iteration.
    # Do not change unless you know what you are doing ;)
    # Array size must be at least as big as the number of iterations
    # Pixel background options
    # Ignore the expected signals based on the track information
    PDBackIgnoreExpSignals=[True, False, False, False],
    # Ignore the hit data when computing backgrounds
    PDBackIgnoreHitData=[False, False, False, False],
    # Minimum allowed pixel background value (RICH1,RICH2)
    PDBackMinPixBackground=[(0, 0), (0, 0), (0, 0), (0, 0)],
    # Maximum allowed pixel background value (RICH1,RICH2)
    PDBackMaxPixBackground=[(99, 99), (99, 99), (99, 99), (99, 99)],
    # Threshold values for background values
    PDBackThresholds=[(0, 0), (0, 0), (0, 0), (0, 0)],
    # Background weights for each RICH
    PDBckWeights=[(1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0)],
    # Likelihood minimizer options
    # Freeze out DLL values
    TrackFreezeOutDLL=[2, 4, 5, 6],
    # Run a final check on the DLL values
    FinalDLLCheck=[False, True, True, True],
    # Force change DLL values
    TrackForceChangeDLL=[-1, -2, -3, -4],
    # Likelihood threshold
    LikelihoodThreshold=[-1e-2, -1e-3, -1e-4, -1e-5],
    # Maximum tracks that can change per iteration
    MaxTrackChangesPerIt=[5, 5, 4, 3],
    # The minimum DLL signal value
    MinSignalForNoLLCalc=[1e-3, 1e-3, 1e-3, 1e-3],
    # ===========================================================
    # Technical options
    # ===========================================================
    # Turn on CPU time measurements
    MeasureTime=True,
    # output level
    OutputLevel=-1,
    # end options
):
    # from GaudiConfig.ControlFlow import seq
    from Configurables import GaudiSequencer

    # Check config is sane
    CheckConfig(inputTrackLocations, outputPIDLocations)

    # ==================================================================
    # Processing options
    # ==================================================================

    # If Online Brunel mode, turn off refractive index scale factors
    if onlineBrunelMode:
        photonSelection = "Online"
        from Configurables import UpdateManagerSvc

        UpdateManagerSvc().ConditionsOverride += [
            "Conditions/Environment/Rich1/RefractivityScaleFactor := double CurrentScaleFactor = 1.0;",
            "Conditions/Environment/Rich2/RefractivityScaleFactor := double CurrentScaleFactor = 1.0;",
        ]

    # Dictionary of general algorithm properties
    algprops = {}

    # RICH Particle properties
    ppToolName = "RichPartProp" + GroupName
    ppTool = "Rich::Future::ParticleProperties/" + ppToolName + ":PUBLIC"
    from Configurables import Rich__Future__ParticleProperties as RichPartProps

    RichPartProps("ToolSvc." + ppToolName).ParticleTypes = particles
    algprops["ParticlePropertiesTool"] = ppTool

    # Detectors
    radsF = ("Rich1Gas" in radiators, "Rich2Gas" in radiators)
    algprops["Detectors"] = radsF

    # Name for pixel locations
    pixname = ""
    if algprops["Detectors"][0]:
        pixname += "RICH1"
    if algprops["Detectors"][1]:
        pixname += "RICH2"

    # output level
    if OutputLevel > 0:
        algprops["OutputLevel"] = str(OutputLevel)

    # ==================================================================
    # RICH Pixel Treatment. Common to all track types and instanciations
    # of the RICH sequences.
    # ==================================================================

    from Configurables import (
        Rich__Future__Rec__PixelClusterGlobalPositions as RichGlobalPoints,
    )
    from Configurables import (
        Rich__Future__Rec__PixelClusterLocalPositions as RichLocalPoints,
    )
    from Configurables import Rich__Future__SmartIDClustering as RichClustering

    # Clustering name extension
    clusName = ""
    if applyPixelClustering[0]:
        clusName += "R1ClusON"
    if applyPixelClustering[1]:
        clusName += "R2ClusON"

    # Pixel TES locations
    cLocs = RichTESPixelMap(pixname + clusName)

    # The pixel sequence
    pixels = GaudiSequencer("RichPixels" + GroupName, MeasureTime=MeasureTime)

    # Forms cluster objects from the raw decoded RICH hits.
    richCluster = makeRichAlg(RichClustering, "RichPixClustering", algprops)
    pixels.Members += [richCluster]
    # Options
    richCluster.ApplyPixelClustering = applyPixelClustering
    richCluster.MaxClusters = maxClusters
    richCluster.AbsoluteMaxPDOcc = maxPDOccupancy
    # 4D reco
    richCluster.Enable4D = enable4DReco
    richCluster.AvHitTime = averageHitTime
    richCluster.TimeWindow = pixelTimeWindow
    # Input
    richCluster.DecodedDataLocation = decodedDataLocation
    # Output
    richCluster.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]

    # Make SIMD pixel summaries
    from Configurables import Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels

    simdPixels = makeRichAlg(RichSIMDPixels, "RichSIMDPixels", algprops)
    pixels.Members += [simdPixels]
    # Options
    simdPixels.NoClustering = (
        not applyPixelClustering[0] and not applyPixelClustering[1]
    )
    # 4D reco
    simdPixels.Enable4D = enable4DReco
    simdPixels.AvHitTime = averageHitTime
    simdPixels.CourseTimeWindow = pixelTimeWindow
    simdPixels.InnerFineTimeWindow = photonTimeWindow[0]
    simdPixels.OuterFineTimeWindow = photonTimeWindow[1]
    if pixOverrideRegions:
        simdPixels.OverrideRegions = pixOverrideRegions
    # inputs
    simdPixels.RichPixelClustersLocation = cLocs["RichPixelClustersLocation"]
    # Outputs
    simdPixels.RichSIMDPixelSummariesLocation = cLocs["RichSIMDPixelSummariesLocation"]

    # MC cheating of positions
    if pixUsePosFromMC or pixUseTimeFromMC:
        simdPixels.RichSIMDPixelSummariesLocation = (
            cLocs["RichPixelClustersLocation"] + "BeforeMCCheat"
        )
        from Configurables import (
            Rich__Future__Rec__MC__RichPixelUseMCInfo as PixUseMCInfo,
        )

        mcPixPos = makeRichAlg(PixUseMCInfo, "RichSIMDPixelsUseMCInfo", algprops)
        pixels.Members += [mcPixPos]
        mcPixPos.UseMCPosition = pixUsePosFromMC
        mcPixPos.UseMCTime = pixUseTimeFromMC
        mcPixPos.InRichSIMDPixelSummariesLocation = (
            simdPixels.RichSIMDPixelSummariesLocation
        )
        mcPixPos.OutRichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"
        ]

    # The complete sequence
    all = GaudiSequencer("RichRecoSeq" + GroupName, MeasureTime=MeasureTime)
    all.Members = [pixels]

    # Loop over tracks
    for tktype, trackLocation in sorted(inputTrackLocations.items()):
        # name for this sequence
        name = GroupName + tktype

        # ==================================================================
        # Intermediary data locations.
        # Eventually should be handled by the framework
        # ==================================================================
        locs = RichTESTrackMap(name)

        # Sequencer for this track type
        tkSeq = GaudiSequencer("Rich" + name + "Reco", MeasureTime=MeasureTime)
        tkSeq.Members = []  # make sure empty
        all.Members += [tkSeq]

        # ==================================================================
        # RICH Track Treatment.
        # ==================================================================

        # The track sequence
        segs = GaudiSequencer("RichTracks" + name, MeasureTime=MeasureTime)
        # Add to final sequence
        tkSeq.Members += [segs]

        from Configurables import (
            Rich__Future__Rec__EmittedPhotonYields as EmittedYields,
        )
        from Configurables import (
            Rich__Future__Rec__RayTraceTrackGlobalPoints as TrackPanelGlobalPoints,
        )
        from Configurables import (
            Rich__Future__Rec__RayTraceTrackLocalPoints as TrackPanelLocalPoints,
        )

        if detYieldPrecision == "Full":
            from Configurables import (
                Rich__Future__Rec__DetectablePhotonYields as DetectableYields,
            )
        elif detYieldPrecision == "Average":
            from Configurables import (
                Rich__Future__Rec__AverageDetectablePhotonYields as DetectableYields,
            )
        else:
            raise ValueError(
                "Unknown detectable yield mode '" + detYieldPrecision + "'"
            )
        from Configurables import Rich__Future__Rec__GeomEffCKMassRings as GeomEff
        from Configurables import (
            Rich__Future__Rec__RayTraceCherenkovCones as EmittedMassCones,
        )
        from Configurables import Rich__Future__Rec__SignalPhotonYields as SignalYields
        from Configurables import (
            Rich__Future__Rec__TrackEmittedCherenkovAngles as EmittedCherenkovAngles,
        )
        from Configurables import (
            Rich__Future__Rec__TrackSignalCherenkovAngles as SignalCherenkovAngles,
        )

        if tkCKResTreatment == "Functional":
            from Configurables import (
                Rich__Future__Rec__TrackFunctionalCherenkovResolutions as TrackCKResolutions,
            )
        elif tkCKResTreatment == "Parameterised":
            from Configurables import (
                Rich__Future__Rec__TrackParameterisedCherenkovResolutions as TrackCKResolutions,
            )
        else:
            raise ValueError(
                "Unknown track Cherenkov resolution treatment '"
                + tkCKResTreatment
                + "'"
            )
        from Configurables import (
            Rich__Future__Rec__SelectTrackSegments as SelectTrackSegments,
        )

        if tktype != "MC":
            # Segment Creator from reco tracks
            from Configurables import (
                Rich__Future__Rec__DetailedTrSegMakerFromTracks as SegmentCreator,
            )

            segCr = makeRichAlg(
                SegmentCreator, "RichTrackSegments" + name, algprops, tktype
            )
            # Properties
            segCr.TrackExtrapolator = trackExtrapolator
            segCr.MaxTracks = maxTracks
            segCr.CreateMissingStates = createMissingStates
            segCr.MinP = MinP
            segCr.MaxP = MaxP
            segCr.MinPt = MinPt
            # Some track tools are not thread safe so turn off their use and extend some z tolerences to compensate...
            # This is temporary ....
            if makeTkToolsThreadSafe:
                segCr.CreateMissingStates = False
                segCr.UseStateProvider = False
                segCr.ZTolerances = (10000, 10000, 10000)
            # Inputs
            segCr.TracksLocation = trackLocation
            # Outputs
            segCr.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            segCr.TrackToSegmentsLocation = locs["InitialTrackToSegmentsLocation"]
            segCr.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            segs.Members += [segCr]
            if tkSegAddTimeFromMC:
                segCr.TrackSegmentsLocation = locs["TrackSegmentsLocation"] + "NoTime"
                from Configurables import (
                    Rich__Future__Rec__MC__RichSegmentAddTimeFromMC as AddSegTimeMC,
                )

                addSegTime = makeRichAlg(
                    AddSegTimeMC, "RichAddSegTimeFromMC" + name, algprops, tktype
                )
                addSegTime.TracksLocation = trackLocation
                addSegTime.InTrackSegmentsLocation = segCr.TrackSegmentsLocation
                addSegTime.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
                addSegTime.OutTrackSegmentsLocation = locs["TrackSegmentsLocation"]
                segs.Members += [addSegTime]

        else:
            from Configurables import (
                Rich__Future__Rec__MC__TrSegMakerFromMCRichTracks as SegmentCreator,
            )

            segCr = makeRichAlg(
                SegmentCreator, "RichTrackSegments" + name, algprops, tktype
            )
            # properties
            segCr.MinP = MinP
            segCr.MaxP = MaxP
            segCr.MinPt = MinPt
            # Outputs
            segCr.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            segCr.TrackToSegmentsLocation = locs["InitialTrackToSegmentsLocation"]
            segCr.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            segCr.OutputTracksLocation = (
                trackLocation  # Location of fake tracks created
            )
            segCr.MCParticlesLinkLocation = "Link/" + trackLocation
            segs.Members += [segCr]

        if photUseMC or tkSegUseMCExpCKTheta:
            from Configurables import (
                Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels,
            )

            mcRels = makeRichAlg(TkToMCPRels, "RichUseMCRels" + name, algprops, tktype)
            segs.Members += [mcRels]
            mcRels.TrackToMCParticlesRelations = "Rec/Track/RichMCRels" + tktype
            mcRels.TracksLocation = trackLocation
            mcRels.MCParticlesLinkLocation = "Link/" + trackLocation

        # Track global impact points on PD plane
        tkGloPnts = makeRichAlg(
            TrackPanelGlobalPoints, "RichTrackGloPoints" + name, algprops, tktype
        )
        # Inputs
        tkGloPnts.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        # Outputs
        tkGloPnts.TrackGlobalPointsLocation = locs["TrackGlobalPointsLocation"]
        segs.Members += [tkGloPnts]

        # Track local impact points on PD plane
        tkLocPnts = makeRichAlg(
            TrackPanelLocalPoints, "RichTrackLocPoints" + name, algprops, tktype
        )
        # Inputs
        tkLocPnts.TrackGlobalPointsLocation = locs["TrackGlobalPointsLocation"]
        tkLocPnts.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        # Output
        tkLocPnts.TrackLocalPointsLocation = locs["TrackLocalPointsLocation"]
        segs.Members += [tkLocPnts]

        # Emitted photon yields
        emitY = makeRichAlg(EmittedYields, "RichEmittedYields" + name, algprops, tktype)
        # Inputs
        emitY.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        # Outputs
        emitY.EmittedPhotonYieldLocation = locs["EmittedPhotonYieldLocation"]
        emitY.EmittedPhotonSpectraLocation = locs["EmittedPhotonSpectraLocation"]
        segs.Members += [emitY]

        # Expected CK anges using emitted photon spectra
        emitChAngles = makeRichAlg(
            EmittedCherenkovAngles, "RichEmittedCKAngles" + name, algprops, tktype
        )
        # Input
        emitChAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        emitChAngles.EmittedPhotonYieldLocation = locs["EmittedPhotonYieldLocation"]
        emitChAngles.EmittedPhotonSpectraLocation = locs["EmittedPhotonSpectraLocation"]
        # Output
        emitChAngles.EmittedCherenkovAnglesLocation = locs[
            "EmittedCherenkovAnglesLocation"
        ]
        segs.Members += [emitChAngles]

        # Cherenkov mass cones using emitted spectra
        emitMassCones = makeRichAlg(
            EmittedMassCones, "RichMassCones" + name, algprops, tktype
        )
        # Input
        emitMassCones.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        emitMassCones.CherenkovAnglesLocation = locs["EmittedCherenkovAnglesLocation"]
        # Output
        emitMassCones.MassHypothesisRingsLocation = locs[
            "EmittedMassHypothesisRingsLocation"
        ]
        # Options
        emitMassCones.NRingPointsMax = NRingPointsMax
        emitMassCones.NRingPointsMin = NRingPointsMin
        emitMassCones.NewCKRingTol = NewCKRingTol
        segs.Members += [emitMassCones]

        # Detectable photon yields
        detY = makeRichAlg(
            DetectableYields, "RichDetectableYields" + name, algprops, tktype
        )
        # Inputs
        detY.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        detY.EmittedSpectraLocation = locs["EmittedPhotonSpectraLocation"]
        detY.MassHypothesisRingsLocation = locs["EmittedMassHypothesisRingsLocation"]
        # Output
        detY.DetectablePhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
        detY.DetectablePhotonSpectraLocation = locs["DetectablePhotonSpectraLocation"]
        segs.Members += [detY]

        # Geometrical Eff.
        geomEff = makeRichAlg(GeomEff, "RichGeomEff" + name, algprops, tktype)
        # Inputs
        geomEff.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        geomEff.CherenkovAnglesLocation = locs["EmittedCherenkovAnglesLocation"]
        geomEff.MassHypothesisRingsLocation = locs["EmittedMassHypothesisRingsLocation"]
        # Outputs
        geomEff.GeomEffsLocation = locs["GeomEffsLocation"]
        geomEff.GeomEffsPerPDLocation = locs["GeomEffsPerPDLocation"]
        geomEff.SegmentPhotonFlagsLocation = locs["SegmentPhotonFlagsLocation"]
        segs.Members += [geomEff]

        # Select final track segments
        tkSel = makeRichAlg(
            SelectTrackSegments, "RichTkSegmentSel" + name, algprops, tktype
        )
        # Inputs
        tkSel.InTrackToSegmentsLocation = locs["InitialTrackToSegmentsLocation"]
        tkSel.GeomEffsLocation = locs["GeomEffsLocation"]
        # Outputs
        tkSel.OutTrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
        segs.Members += [tkSel]

        # Signal Photon Yields
        sigYields = makeRichAlg(
            SignalYields, "RichSignalYields" + name, algprops, tktype
        )
        # Inputs
        sigYields.DetectablePhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
        sigYields.DetectablePhotonSpectraLocation = locs[
            "DetectablePhotonSpectraLocation"
        ]
        sigYields.GeomEffsLocation = locs["GeomEffsLocation"]
        # Outputs
        sigYields.SignalPhotonYieldLocation = locs["SignalPhotonYieldLocation"]
        sigYields.SignalPhotonSpectraLocation = locs["SignalPhotonSpectraLocation"]
        segs.Members += [sigYields]

        # Signal Cherenkov angles
        sigChAngles = makeRichAlg(
            SignalCherenkovAngles, "RichSignalCKAngles" + name, algprops, tktype
        )
        # Inputs
        sigChAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        sigChAngles.SignalPhotonSpectraLocation = locs["SignalPhotonSpectraLocation"]
        sigChAngles.SignalPhotonYieldLocation = locs["SignalPhotonYieldLocation"]
        # Outputs
        sigLoc = locs["SignalCherenkovAnglesLocation"]
        sigChAngles.SignalCherenkovAnglesLocation = sigLoc
        segs.Members += [sigChAngles]
        if tkSegUseMCExpCKTheta:
            from Configurables import (
                Rich__Future__Rec__MC__RichTrackCKAnglesUseMCInfo as UseMCTKAngles,
            )

            useMCAngles = makeRichAlg(
                UseMCTKAngles, "RichSignalCKAnglesUseMC" + name, algprops, tktype
            )
            segs.Members += [useMCAngles]
            sigChAngles.SignalCherenkovAnglesLocation = sigLoc + "BeforeMCCheat"
            useMCAngles.InCherenkovAnglesLocation = sigLoc + "BeforeMCCheat"
            useMCAngles.TracksLocation = trackLocation
            useMCAngles.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            useMCAngles.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            useMCAngles.TrackToMCParticlesRelations = "Rec/Track/RichMCRels" + tktype
            useMCAngles.OutCherenkovAnglesLocation = sigLoc

        # Track Resolutions
        tkRes = makeRichAlg(
            TrackCKResolutions, "RichCKResolutions" + name, algprops, tktype
        )
        # Inputs
        tkRes.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        tkRes.SignalCherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
        if tkCKResTreatment == "Functional":
            tkRes.MassHypothesisRingsLocation = locs[
                "EmittedMassHypothesisRingsLocation"
            ]
        # Outputs
        tkRes.CherenkovResolutionsLocation = locs["CherenkovResolutionsLocation"]
        # Processing options
        if tkCKResScaleFactors:
            tkRes.ScaleFactor = tkCKResScaleFactors
        # Max Resolution values
        tkRes.MaxCKThetaRes = defaultMaxCKThetaResolutions()[tktype]
        if tkCKResTreatment == "Parameterised":
            tkRes.TrackType = tktype
        segs.Members += [tkRes]

        # ==================================================================
        # RICH Photon Treatment.
        # ==================================================================

        photAlgs = []

        if photonReco == "Quartic" or photonReco == "FastQuartic":
            from Configurables import (
                Rich__Future__Rec__SIMDQuarticPhotonReco as PhotonReco,
            )
        elif photonReco == "CKEstiFromRadius":
            from Configurables import (
                Rich__Future__Rec__SIMDCKEstiFromRadiusPhotonReco as PhotonReco,
            )
        else:
            raise ValueError("Unknown Photon Reco mode '" + photonReco + "'")
        from Configurables import (
            Rich__Future__Rec__SIMDPhotonPredictedPixelSignal as PhotonPredSignal,
        )

        # The photon reconstruction
        photReco = makeRichAlg(PhotonReco, "RichPhotonReco" + name, algprops, tktype)
        photAlgs += [photReco]
        # Inputs
        photReco.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        photReco.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
        photReco.CherenkovResolutionsLocation = locs["CherenkovResolutionsLocation"]
        photReco.TrackLocalPointsLocation = locs["TrackLocalPointsLocation"]
        photReco.TrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
        photReco.SegmentPhotonFlagsLocation = locs["SegmentPhotonFlagsLocation"]
        photReco.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"
        ]
        # Outputs
        photReco.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
        photReco.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        photReco.PhotonMirrorDataLocation = locs["PhotonMirrorDataLocation"]
        # Processing options
        #                              Aero  R1Gas  R2gas   (Aero is ignored)
        # Truncate CK angles
        photReco.TruncateCKAngles = truncateCKAngles
        # Scale factors
        photReco.ScaleFactorCKTheta = (0.045, 0.024)
        photReco.ScaleFactorSepG = (83, 107)
        # Photon (pre)selection cuts
        if not nSigmaCuts:
            nSigmaCuts = defaultNSigmaCuts()
        if not minMaxCKThetaCuts:
            minMaxCKThetaCuts = defaultMinMaxCKThetaCuts()
        nSigmaC = nSigmaCuts[photonSelection][tkCKResTreatment][tktype]
        photReco.PreSelNSigma = nSigmaC[0]
        photReco.NSigma = nSigmaC[1]
        minMaxCKT = minMaxCKThetaCuts[photonSelection]
        photReco.MinAllowedCherenkovTheta = minMaxCKT["Min"]
        photReco.MaxAllowedCherenkovTheta = minMaxCKT["Max"]
        minMaxTrackROI = defaultMinMaxTrackROICuts()[photonSelection]
        photReco.PreSelMinTrackROI = minMaxTrackROI["Min"]
        photReco.PreSelMaxTrackROI = minMaxTrackROI["Max"]
        # 4D reconstruction ?
        if photonReco == "Quartic" or photonReco == "FastQuartic":
            # enable 4D reco
            photReco.Enable4D = enable4DReco
        # Reconstruction algorithm tunings
        if photonReco == "Quartic":
            # Number of iterations for non-flat secondary mirrors
            photReco.NQuarticIterationsForSecMirrors = (1, 3)
            photReco.FindUnambiguousPhotons = (False, False)
        elif photonReco == "FastQuartic":
            # Turn off various aspects for speed  R1Gas  R2Gas
            photReco.FindUnambiguousPhotons = (False, False)
            photReco.NQuarticIterationsForSecMirrors = (1, 1)
            # Tighten the selection cuts
            if tkCKResTreatment == "Functional":
                photReco.PreSelNSigma = (6, 10)
                photReco.NSigma = (3.0, 3.0)
            elif tkCKResTreatment == "Parameterised":
                photReco.PreSelNSigma = (6, 10)
                photReco.NSigma = (3.0, 3.0)
        elif photonReco == "CKEstiFromRadius":
            # This implementation requires the CK rings
            photReco.MassHypothesisRingsLocation = locs[
                "EmittedMassHypothesisRingsLocation"
            ]
        else:
            raise ValueError(
                "Unknown photon reconstruction algorithm '" + photonReco + "'"
            )

        if photUseMC:
            from Configurables import (
                Rich__Future__Rec__MC__RichPhotonUseMCInfo as PhotonUseMCInfo,
            )

            photMCInfo = makeRichAlg(
                PhotonUseMCInfo, "RichPhotonUseMCInfo" + name, algprops, tktype
            )
            photAlgs += [photMCInfo]
            pLoc = locs["CherenkovPhotonLocation"]
            photMCInfo.OutCherenkovPhotonLocation = pLoc
            photMCInfo.InCherenkovPhotonLocation = pLoc + "BeforeMC"
            photReco.CherenkovPhotonLocation = pLoc + "BeforeMC"
            photMCInfo.TrackToMCParticlesRelations = mcRels.TrackToMCParticlesRelations
            photMCInfo.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            photMCInfo.TracksLocation = trackLocation
            photMCInfo.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            photMCInfo.SegmentToTrackLocation = locs["SegmentToTrackLocation"]
            photMCInfo.SignalCherenkovAnglesLocation = locs[
                "SignalCherenkovAnglesLocation"
            ]
            if photEmulatedResMC:
                photMCInfo.CKThetaSmearFuncInner = photEmulatedResMC[0]
                photMCInfo.CKThetaSmearFuncOuter = photEmulatedResMC[1]
            photMCInfo.UseMCPhotonCKThetaValues = False

        # Predicted pixel signals
        photPredSig = makeRichAlg(
            PhotonPredSignal, "RichPredPixelSignal" + name, algprops, tktype
        )
        photAlgs += [photPredSig]
        # Input
        photPredSig.RichSIMDPixelSummariesLocation = cLocs[
            "RichSIMDPixelSummariesLocation"
        ]
        photPredSig.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        photPredSig.CherenkovPhotonLocation = locs["CherenkovPhotonLocation"]
        photPredSig.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        photPredSig.CherenkovAnglesLocation = locs["SignalCherenkovAnglesLocation"]
        photPredSig.CherenkovResolutionsLocation = locs["CherenkovResolutionsLocation"]
        photPredSig.PhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
        # Output
        photPredSig.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
        # Options
        if minPhotonProbability:
            photPredSig.MinPhotonProbability = minPhotonProbability
        # 4D reco
        photPredSig.Enable4D = enable4DReco

        # The photon reco sequence
        phots = GaudiSequencer(
            "RichPhotons" + name, MeasureTime=MeasureTime, Members=photAlgs
        )
        # Add to final sequence
        tkSeq.Members += [phots]

        # ==================================================================
        # Create working event summary of reco info
        # ==================================================================

        from Configurables import Rich__Future__Rec__SIMDRecoSummary as RecSummary

        recSum = makeRichAlg(RecSummary, "RichRecSummary" + name, algprops, tktype)
        # Inputs
        recSum.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
        recSum.TrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
        recSum.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
        recSum.DetectablePhotonYieldLocation = locs["DetectablePhotonYieldLocation"]
        recSum.SignalPhotonYieldLocation = locs["SignalPhotonYieldLocation"]
        recSum.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
        recSum.RichSIMDPixelSummariesLocation = cLocs["RichSIMDPixelSummariesLocation"]
        # Output
        recSum.SummaryTracksLocation = locs["SummaryTracksLocation"]
        recSum.SummaryPixelsLocation = locs["SummaryPixelsLocation"]
        # Add to sequence
        tkSeq.Members += [recSum]

        # ==================================================================
        # RICH Global PID
        # ==================================================================

        from Configurables import (
            Rich__Future__Rec__GlobalPID__InitialisePIDInfo as GPIDInit,
        )
        from Configurables import (
            Rich__Future__Rec__GlobalPID__SIMDLikelihoodMinimiser as LikelihoodMinimiser,
        )
        from Configurables import (
            Rich__Future__Rec__GlobalPID__WriteRichPIDs as WriteRichPIDs,
        )
        from Configurables import (
            Rich__Future__Rec__SIMDPixelBackgroundsEstiAvHPD as PixelBackgrounds,
        )

        # Initalise some default locations
        gpidInit = makeRichAlg(GPIDInit, "RichGPIDInit" + name, algprops, tktype)
        # Inputs
        gpidInit.SummaryTracksLocation = locs["SummaryTracksLocation"]
        # Output
        gpidInit.TrackPIDHyposLocation = "Rec/Rich/TrackHypos/Init/" + name
        gpidInit.TrackDLLsLocation = "Rec/Rich/TrackDLLs/Init/" + name

        # Save the 'last iteration' containers
        lastTrackPIDHyposLocation = gpidInit.TrackPIDHyposLocation
        lastTrackDLLsInputLocation = gpidInit.TrackDLLsLocation

        # The pid sequence
        pid = GaudiSequencer(
            "RichPID" + name, MeasureTime=MeasureTime, Members=[gpidInit]
        )
        # Add to final sequence
        tkSeq.Members += [pid]

        # PID iterations
        for it in range(0, nIterations):
            itN = "It" + repr(it)

            # Pixel backgrounds
            pixBkgs = makeRichAlg(
                PixelBackgrounds, "RichPixBackgrounds" + itN + name, algprops, tktype
            )
            # Input
            pixBkgs.TrackPIDHyposLocation = lastTrackPIDHyposLocation
            pixBkgs.TrackToSegmentsLocation = locs["SelectedTrackToSegmentsLocation"]
            pixBkgs.TrackSegmentsLocation = locs["TrackSegmentsLocation"]
            pixBkgs.GeomEffsPerPDLocation = locs["GeomEffsPerPDLocation"]
            pixBkgs.DetectablePhotonYieldLocation = locs[
                "DetectablePhotonYieldLocation"
            ]
            pixBkgs.RichSIMDPixelSummariesLocation = cLocs[
                "RichSIMDPixelSummariesLocation"
            ]
            # Output
            pixBkgs.PixelBackgroundsLocation = (
                "Rec/Rich/PixelBackgrounds/" + itN + "/" + name
            )
            # Settings
            pixBkgs.IgnoreExpectedSignals = PDBackIgnoreExpSignals[it]
            pixBkgs.IgnoreHitData = PDBackIgnoreHitData[it]
            pixBkgs.MinPixelBackground = PDBackMinPixBackground[it]
            pixBkgs.MaxPixelBackground = PDBackMaxPixBackground[it]
            pixBkgs.PDBckWeights = PDBckWeights[it]
            pixBkgs.ThresholdBackground = PDBackThresholds[it]
            pixBkgs.Enable4D = enable4DReco

            # Likelihood minimiser
            like = makeRichAlg(
                LikelihoodMinimiser, "RichGPIDLikelihood" + itN + name, algprops, tktype
            )
            # Input
            like.SummaryTracksLocation = locs["SummaryTracksLocation"]
            like.SummaryPixelsLocation = locs["SummaryPixelsLocation"]
            like.PixelBackgroundsLocation = pixBkgs.PixelBackgroundsLocation
            like.TrackPIDHyposInputLocation = lastTrackPIDHyposLocation
            like.TrackDLLsInputLocation = lastTrackDLLsInputLocation
            like.PhotonToParentsLocation = locs["PhotonToParentsLocation"]
            like.PhotonSignalsLocation = locs["PhotonSignalsLocation"]
            # Output
            like.TrackPIDHyposOutputLocation = "Rec/Rich/TrackHypos/" + itN + "/" + name
            like.TrackDLLsOutputLocation = "Rec/Rich/TrackDLLs/" + itN + "/" + name
            # Settings
            like.TrackFreezeOutDLL = TrackFreezeOutDLL[it]
            like.FinalDLLCheck = FinalDLLCheck[it]
            like.TrackForceChangeDLL = TrackForceChangeDLL[it]
            like.LikelihoodThreshold = LikelihoodThreshold[it]
            like.MaxTrackChangesPerIt = MaxTrackChangesPerIt[it]
            like.MinSignalForNoLLCalc = MinSignalForNoLLCalc[it]

            # Update the 'last' TES locations
            lastTrackPIDHyposLocation = like.TrackPIDHyposOutputLocation
            lastTrackDLLsInputLocation = like.TrackDLLsOutputLocation

            # Append to the PID sequence
            pid.Members += [pixBkgs, like]

        # Write the file RichPID objects
        writePIDs = makeRichAlg(
            WriteRichPIDs, "RichGPIDWriteRichPIDs" + name, algprops, tktype
        )
        # options
        writePIDs.PIDVersion = pidVersion
        # Inputs
        writePIDs.TracksLocation = trackLocation
        writePIDs.SummaryTracksLocation = locs["SummaryTracksLocation"]
        writePIDs.TrackPIDHyposInputLocation = lastTrackPIDHyposLocation
        writePIDs.TrackDLLsInputLocation = lastTrackDLLsInputLocation
        # Outputs
        writePIDs.RichPIDsLocation = outputPIDLocations[tktype]

        # update the pid sequence
        pid.Members += [writePIDs]

    # ==================================================================
    # Merge the final PIDs
    # ==================================================================

    if "" != mergedOutputPIDLocation:
        from Configurables import Rich__Future__Rec__MergeRichPIDs as MergePIDs

        pidMerge = makeRichAlg(MergePIDs, "MergeRichPIDs" + GroupName, algprops)
        pidMerge.InputRichPIDLocations = list(outputPIDLocations.values())
        pidMerge.OutputRichPIDLocation = mergedOutputPIDLocation
        pidMerge.PIDVersion = pidVersion

        all.Members += [pidMerge]

    # ==================================================================
    # Return the final sequence
    # ==================================================================
    return all
