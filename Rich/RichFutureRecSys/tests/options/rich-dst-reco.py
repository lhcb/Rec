###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

import os

from Configurables import (
    DDDBConf,
    FPEAuditor,
    GaudiSequencer,
    LHCbApp,
    MCFTHitUnpacker,
    MCParticle2MCHitAlg,
    MCRichDigitSummaryUnpacker,
    MCRichHitUnpacker,
    MCRichOpticalPhotonUnpacker,
    MCRichSegmentUnpacker,
    MCRichTrackUnpacker,
    MCUTHitUnpacker,
    MCVPHitUnpacker,
    UnpackMCParticle,
    UnpackMCVertex,
)
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from GaudiKernel.SystemOfUnits import GeV

# --------------------------------------------------------------------------------------

# get test name
nameEnvVar = "QMTTEST_NAME"
if nameEnvVar not in os.environ:
    raise RuntimeError(f"Could not determine test NAME via EnvVar {nameEnvVar}")
myName = os.environ[nameEnvVar]

# data options
isDetDescMC = "detdesc-mc" in myName

# reco options
useMCTracks = "mctracks" in myName
useMCHits = "mchits" in myName
is4D = "4D" in myName
enable4D = (is4D, is4D)
addTkSegTimeMC = is4D and not useMCTracks
usePixelMCPos = "pixmcpos" in myName
usePixelMCTime = "pixmctime" in myName
# Monitoring/checking options
useExpertHists = "experthists" in myName

# Just to initialise
DDDBConf()
LHCbApp()

# Event numbers
nEvents = 250 if useExpertHists else 1000
EventSelector().PrintFreq = 50

# Messaging
msgSvc = getConfigurable("MessageSvc")
msgSvc.Format = "% F%30W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool

# Disable tool timing output
SequencerTimerTool().OutputLevel = 4

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().ActivateAt = ["Execute"]

# The overall sequence. Filled below.
all = GaudiSequencer("All")

# Unpacking sequence before reco runs
preUnpackSeq = GaudiSequencer("PreUnpackSeq")
all.Members += [preUnpackSeq]

# Data fetcher
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile

fetcher = FetchDataFromFile("FetchDSTData")
fetcher.DataKeys = ["Trigger/RawEvent"]
preUnpackSeq.Members += [fetcher]

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=[
        ToolSvc(),
        AuditorSvc(),
        MessageSvcSink(HistoStringsWidth=80),
        RootHistoSink(FileName=myName + "-hists.root"),
    ],
    AuditAlgorithms=True,
)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
    # dd4hep.DumpConditions = True
    app.ExtSvc += [dd4hep]

# --------------------------------------------------------------------------------------

# First various raw event decodings
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent

unpack = UnpackRawEvent(
    "UnpackRawEvent",
    BankTypes=["ODIN"],
    RawEventLocation="Trigger/RawEvent",
    RawBankLocations=["DAQ/RawBanks/ODIN"],
)
from Configurables import createODIN

decodeODIN = createODIN("ODINFutureDecode")
preUnpackSeq.Members += [unpack, decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

if addTkSegTimeMC or useMCHits or useMCTracks:
    preUnpackSeq.Members += [UnpackMCVertex(), UnpackMCParticle()]

if useMCHits or usePixelMCPos or usePixelMCTime:
    preUnpackSeq.Members += [MCRichHitUnpacker()]
    fetcher.DataKeys += ["pSim/Rich/Hits"]

if not useMCHits:
    # Regular decoding from RawBanks
    fetcher.DataKeys += ["Rich/RawEvent"]
    from Configurables import Rich__Future__RawBankDecoder as RichDecoder

    unpack_rich = UnpackRawEvent(
        "UnpackRich",
        BankTypes=["Rich"],
        RawEventLocation="Rich/RawEvent",
        RawBankLocations=["DAQ/RawBanks/Rich"],
    )
    preUnpackSeq.Members += [unpack_rich]
    richDecode = RichDecoder("RichFutureDecode")
    all.Members += [richDecode]
    # If asked for update decoded data with time from MC
    if usePixelMCTime:
        richDecode.DecodedDataLocation = (
            str(richDecode.DecodedDataLocation) + "BeforeMCCheat"
        )
        from Configurables import (
            Rich__Future__MC__DecodedDataAddMCInfo as DecodedDataAddMCInfo,
        )

        all.Members += [
            DecodedDataAddMCInfo(
                "RichDecodedDataAddMCInfo",
                InDecodedDataLocation=richDecode.DecodedDataLocation,
            )
        ]
else:
    from Configurables import (
        Rich__Future__MC__DecodedDataFromMCRichHits as RichMCDecoder,
    )

    richMCDecode = RichMCDecoder(
        "RichDecodeFromMC", IsDetDescMC=isDetDescMC, IncludeTimeInfo=is4D
    )
    all.Members += [richMCDecode]

# DataType
dType = "Upgrade"

# Min Momentum cuts
minP = (0.5 * GeV, 0.5 * GeV)
minPt = 0.1 * GeV

if not useMCTracks:
    # Explicitly unpack the Tracks
    fetcher.DataKeys += ["pRec/Track/Best"]
    from Configurables import UnpackTrack

    preUnpackSeq.Members += [UnpackTrack("UnpackTracks")]

    # Filter the tracks by type
    from Configurables import TracksSharedSplitterPerType as TrackFilter

    tkFilt = TrackFilter("TrackTypeFilter")
    tkFilt.InputTracks = "Rec/Track/Best"
    tkFilt.LongTracks = "Rec/Track/BestLong"
    tkFilt.DownstreamTracks = "Rec/Track/BestDownstream"
    tkFilt.UpstreamTracks = "Rec/Track/BestUpstream"
    tkFilt.Ttracks = "Rec/Track/BestTtrack"
    tkFilt.VeloTracks = "Rec/Track/BestVelo"
    all.Members += [tkFilt]

    # Input tracks
    tkLocs = {
        "Long": tkFilt.LongTracks,
        "Down": tkFilt.DownstreamTracks,
        "Up": tkFilt.UpstreamTracks,
    }

    # Output PID
    pidLocs = {
        "Long": "Rec/Rich/LongPIDs",
        "Down": "Rec/Rich/DownPIDs",
        "Up": "Rec/Rich/UpPIDs",
    }

else:
    # Build track info from RICH extended MC info
    fetcher.DataKeys += [
        "pSim/Rich/Segments",
        "pSim/Rich/Tracks",
        "pSim/MCVertices",
        "pSim/MCParticles",
        "pSim/Rich/Photons",
    ]
    preUnpackSeq.Members += [
        MCRichHitUnpacker(),
        MCRichOpticalPhotonUnpacker(),
        MCRichSegmentUnpacker(),
        MCRichTrackUnpacker(),
    ]
    # Needed for ideal state creator
    unpVPHits = MCVPHitUnpacker()
    unpFTHits = MCFTHitUnpacker()
    unpUTHits = MCUTHitUnpacker()
    linkVP = MCParticle2MCHitAlg(
        "LinkVPMCHits", MCHitPath=unpVPHits.OutputName, OutputData="Link/MC/VP/Hits"
    )
    linkFF = MCParticle2MCHitAlg(
        "LinkFTMCHits", MCHitPath=unpFTHits.OutputName, OutputData="Link/MC/FT/Hits"
    )
    linkUT = MCParticle2MCHitAlg(
        "LinkUTMCHits", MCHitPath=unpUTHits.OutputName, OutputData="Link/MC/UT/Hits"
    )
    preUnpackSeq.Members += [unpVPHits, unpFTHits, unpUTHits, linkVP, linkFF, linkUT]
    # Input tracks
    tkLocs = {"MC": "Rec/Track/FromRichMCTracks"}
    # Output PIDs
    pidLocs = {"MC": "Rec/Rich/MCPIDs"}

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# histograms
histos = "Expert" if useExpertHists else "OfflineFull"

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence

RichRec = RichRecoSequence(
    MinP=minP,
    MinPt=minPt,
    dataType=dType,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    enable4DReco=enable4D,
    tkSegAddTimeFromMC=addTkSegTimeMC,
    pixUsePosFromMC=usePixelMCPos,
    mergedOutputPIDLocation=finalPIDLoc,
)
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Monitoring
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors

RichMoni = RichRecMonitors(
    enable4DReco=enable4D,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    histograms=histos,
)
all.Members += [RichMoni]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Post reco Unpack sequence
postUnpackSeq = GaudiSequencer("PostUnpackSeq", MeasureTime=True)
all.Members += [postUnpackSeq]
fetcher.DataKeys += ["pMC/Vertices", "pMC/Particles"]
postUnpackSeq.Members += [UnpackMCVertex(), UnpackMCParticle()]
if not useMCHits:
    postUnpackSeq.Members += [MCRichDigitSummaryUnpacker("RichSumUnPack")]
# Extras needed for Expert histograms (e.g. RICH extended MC data)
if useExpertHists:
    if not useMCHits:
        fetcher.DataKeys += ["pSim/Rich/Hits"]
        postUnpackSeq.Members += [MCRichHitUnpacker()]
    if not useMCTracks:
        fetcher.DataKeys += [
            "pSim/Rich/Segments",
            "pSim/Rich/Tracks",
            "pSim/Rich/OpticalPhotons",
        ]
        postUnpackSeq.Members += [
            MCRichOpticalPhotonUnpacker(),
            MCRichSegmentUnpacker(),
            MCRichTrackUnpacker(),
        ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking
from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers

RichCheck = RichRecCheckers(
    enable4DReco=enable4D,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    histograms=histos,
)
all.Members += [RichCheck]
# --------------------------------------------------------------------------------------
