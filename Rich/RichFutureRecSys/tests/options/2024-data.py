from __future__ import print_function

from Configurables import DDDBConf, LHCbApp
from DDDB.CheckDD4Hep import UseDD4Hep

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper
from PRConfig.TestFileDB import test_file_db

data = test_file_db["rich-decode-2024"].filenames
IOHelper("MDF").inputFiles(data, clear=True)

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
if not UseDD4Hep:
    from Configurables import CondDB

    CondDB().setProp("Upgrade", True)
    LHCbApp().DDDBtag = "upgrade/master"
    LHCbApp().CondDBtag = "upgrade/master"
