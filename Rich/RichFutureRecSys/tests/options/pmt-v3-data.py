###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep
from Gaudi.Configuration import *
from PRConfig.TestFileDB import test_file_db

if not UseDD4Hep:
    from Configurables import CondDB

    CondDB().setProp("Upgrade", True)

# Timestamps in messages
from Configurables import LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

LHCbApp().DDDBtag = "upgrade/dddb-20220111"
if UseDD4Hep:
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    DDDBConf().GeometryVersion = "run3/before-rich1-geom-update-26052022"
else:
    LHCbApp().CondDBtag = "upgrade/sim-20210630-vc-md100"

from GaudiConf import IOHelper

data = test_file_db["rich-decode-detdesc-compat-old-rich1"].filenames
IOHelper("ROOT").inputFiles(data, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]
