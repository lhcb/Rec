###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

import os

from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import GaudiSequencer, createODIN
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent
from Configurables import Rich__Future__RawBankDecoder as RichDecoder
from Configurables import Rich__Future__Rec__Moni__DetectorHits as DetectorHits
from Configurables import Rich__Future__Rec__SIMDSummaryPixels as RichSIMDPixels
from Configurables import Rich__Future__SmartIDClustering as RichClustering
from Gaudi.Configuration import *

# get test name
nameEnvVar = "QMTTEST_NAME"
if nameEnvVar not in os.environ:
    raise RuntimeError(f"Could not determine test NAME via EnvVar {nameEnvVar}")
myName = os.environ[nameEnvVar]

# Event numbers
nEvents = 100
EventSelector().PrintFreq = 10

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
from Configurables import FPEAuditor

FPEAuditor().ActivateAt = ["Execute"]

# The overall sequence. Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=[
        ToolSvc(),
        AuditorSvc(),
        MessageSvcSink(),
        RootHistoSink(FileName=myName + "-hists.root"),
    ],
    AuditAlgorithms=True,
)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
    app.ExtSvc += [dd4hep]

# --------------------------------------------------------------------------------------

# Fetch required data from file
fetcher = FetchDataFromFile("FetchDSTData")
fetcher.DataKeys = ["Trigger/RawEvent", "Rich/RawEvent"]
all.Members += [fetcher]

# First various raw event decodings

# Unpack the ODIN raw event
all.Members += [
    UnpackRawEvent(
        "UnpackODIN",
        BankTypes=["ODIN"],
        RawEventLocation="DAQ/RawEvent",
        RawBankLocations=["DAQ/RawBanks/ODIN"],
    )
]

odinDecode = createODIN("ODINDecode")
all.Members += [odinDecode]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=odinDecode.ODIN)]

# Unpack the ODIN raw event
all.Members += [
    UnpackRawEvent(
        "UnpackRich",
        BankTypes=["Rich"],
        RawEventLocation="DAQ/RawEvent",
        RawBankLocations=["DAQ/RawBanks/Rich"],
    )
]
richDecode = RichDecoder("RichDecode")
all.Members += [richDecode]

clusters = RichClustering("RichClustering")
all.Members += [clusters]

simdPixs = RichSIMDPixels("SIMDPixels")
all.Members += [simdPixs]

pixMon = DetectorHits("RichRecPixelQC")
all.Members += [pixMon]
