from __future__ import print_function

import glob

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/MC/Run3/Sim09c-Up02/30000000/XDIGI/",
    # CRJ's CernVM
    "/home/chris/LHCb/Data/MC/Run3/Sim09c-Up02/30000000/XDIGI/",
]

print("Data Files :-")
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.xdigi"))
    data += ["'PFN:" + file for file in files]
    for f in files:
        print(f)

IOHelper("ROOT").inputFiles(data, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]

from Configurables import Brunel, LHCbApp

Brunel().InputType = "DIGI"
Brunel().DataType = "Upgrade"
Brunel().Simulation = True
Brunel().WithMC = True
LHCbApp().DDDBtag = "dddb-20171010"
LHCbApp().CondDBtag = "sim-20170301-vc-md100"
