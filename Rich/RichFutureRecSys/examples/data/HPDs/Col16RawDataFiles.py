###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob

from Gaudi.Configuration import *
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision16/LHCb/Raw/",  # Cambridge
    "/home/chris/LHCb/Data/Collision16/LHCb/Raw/",  # CRJ's CernVM
]

data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*/*.raw"))
    data += ["DATAFILE='" + file + "'" for file in files]

IOHelper("MDF").inputFiles(data, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]

from Configurables import Brunel, LHCbApp

Brunel().DataType = "2016"
Brunel().Simulation = False
Brunel().WithMC = False

Brunel().DDDBtag = "dddb-20150724"
Brunel().CondDBtag = "cond-20161011"
