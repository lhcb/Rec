from __future__ import print_function

import glob

###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/Collision16/LHCb/Raw/",  # Cambridge
    "/usera/jonesc/NFS/data/Collision15/LHCb/Raw/",  # Cambridge
    "/usera/jonesc/NFS/data/Collision12/LHCb/Raw/",  # Cambridge
    "/usera/jonesc/NFS/data/Collision11_25/LHCb/Raw/",  # Cambridge
    "/home/chris/LHCb/Data/Collision16/LHCb/Raw/",  # CRJ's CernVM
]

print("Data Files :-")
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*/*.raw"))
    print("\n".join(files))
    data += ["DATAFILE='" + file + "'" for file in files]

IOHelper("MDF").inputFiles(data, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]
