###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import glob

from Gaudi.Configuration import *
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    "/usera/jonesc/NFS/data/RunIII/LHCbSim/"  # Cambridge
]

data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "fixedNZS.mdf"))
    data += ["DATAFILE='" + file + "'" for file in files]

IOHelper("MDF").inputFiles(data, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]

# Currently no PMTs in LHCbCond ...
from Configurables import LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

# LHCbApp().DDDBtag = "upgrade/dddb-20200529"
LHCbApp().DDDBtag = "upgrade/jonrob/add-realistic-pmt-hardware-maps"

# LHCbApp().CondDBtag = "upgrade/sim-20200515-vc-md100"
LHCbApp().CondDBtag = "upgrade/jonrob/add-realistic-pmt-hardware-maps"
