###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function

import glob
import os
import shutil
import uuid
from pathlib import Path

from Gaudi.Configuration import *
from GaudiConf import IOHelper

# name tag
tag = "U2-WithSpill-NoSIN-NoRichRandHits"

# Choose sample luminosity
lumi = os.getenv("LUMI", "2.0e33")

# DetDesc DB tags
dbTag = "dddb-20221004"
cdTag = "sim-20221220-vc-md100"

# Event type
evType = os.getenv("LHCB_EVENT_TYPE", "30000000")

relPath = (
    "data/MC/UpgradeII/"
    + evType
    + "/"
    + dbTag
    + "/"
    + cdTag
    + "/"
    + tag
    + "-lumi_"
    + lumi
    + "/DIGI/"
)

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/" + relPath,
    # CERN EOS
    "/eos/lhcb/user/j/jonrob/" + relPath,
]

data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.digi"))
    if len(files) > 0:
        data += files
    else:
        print("No data at", path)

# Batch options ?
if "CONDOR_FILE_BATCH" in os.environ:
    # Use env vars to define reduced file range for this job
    batch = int(os.environ["CONDOR_FILE_BATCH"])
    nfiles = int(os.environ["CONDOR_FILES_PER_BATCH"])
    firstFile = batch * nfiles
    lastFile = (batch + 1) * nfiles
    if firstFile <= len(data):
        if lastFile > len(data):
            lastFile = len(data)
        print("INFO: Using restricted file range", firstFile, lastFile)
        data = data[firstFile:lastFile]
    else:
        print("WARNING: File range outside input data list")
        data = []

    # cache_dir = "/var/work/" + os.environ["USER"] + "/DataCache"
    # if "CACHE_DATA_LOCALLY" in os.environ:
    #    # Attempt to cache locally to work area
    #    new_data = []
    #    Path(cache_dir).mkdir(parents=True, exist_ok=True)
    #    for f in data:
    #        fsize = os.path.getsize(f)
    #        total, used, free = shutil.disk_usage(cache_dir)
    #        if free > fsize:
    #            cache_f = cache_dir + f
    #            if not os.path.isfile(cache_f):
    #                tmp_f =  cache_f + ".tmp." + str(uuid.uuid4())
    #                print("Copying", f, "to", cache_dir)
    #                Path(os.path.dirname(cache_f)).mkdir(parents=True, exist_ok=True)
    #                shutil.copy(f, tmp_f)
    #                if not os.path.isfile(cache_f):
    #                    shutil.move(tmp_f,cache_f)
    #                else:
    #                    os.remove(tmp_f)
    #                    new_data += [cache_f]
    #    data = new_data
    # else:
    #    if os.path.exists(cache_dir): shutil.rmtree(cache_dir)

print("Data Files :-")
for f in data:
    print(f)

# Append "PFN:" to each file
pfns = ["'PFN:" + f for f in data]

IOHelper("ROOT").inputFiles(pfns, clear=True)
FileCatalog().Catalogs = ["xmlcatalog_file:out.xml"]

from Configurables import DDDBConf, LHCbApp

LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = dbTag
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    # To Be Seen what is correct here...
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
    # Use a geometry from before changes to RICH1
    # https://gitlab.cern.ch/lhcb/Detector/-/merge_requests/205
    DDDBConf().GeometryVersion = "run3/before-rich1-geom-update-26052022"
else:
    LHCbApp().CondDBtag = cdTag

from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent

UnpackRawEvent("UnpackODIN").RawEventLocation = "DAQ/RawEvent"
UnpackRawEvent("UnpackRich").RawEventLocation = "DAQ/RawEvent"
