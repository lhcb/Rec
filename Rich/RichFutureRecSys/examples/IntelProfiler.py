###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Intel VTune auditor
from Configurables import IntelProfilerAuditor
from Gaudi.Configuration import *

profiler = IntelProfilerAuditor()
# profiler.OutputLevel = DEBUG
# We can skip some events
profiler.StartFromEventN = 50
# profiler.StopAtEventN = 500
AuditorSvc().Auditors += [profiler]
ApplicationMgr().AuditAlgorithms = True
# Sequence which we need to profile. If empty, we profile all algorithms
profiler.IncludeAlgorithms = []
# Pixels and decoding
profiler.IncludeAlgorithms += ["RichFutureDecode", "RichPixels"]
# Photon reco
profiler.IncludeAlgorithms += ["RichPhotonsDown", "RichPhotonsUp", "RichPhotonsLong"]
# ray tracing
profiler.IncludeAlgorithms += [
    "RichMassConesDown",
    "RichMassConesUp",
    "RichMassConesLong",
]
# PID
profiler.IncludeAlgorithms += ["RichPIDDown", "RichPIDUp", "RichPIDLong"]
# Yields
profiler.IncludeAlgorithms += [
    "RichDetectableYieldsDown",
    "RichDetectableYieldsUp",
    "RichDetectableYieldsLong",
]
# Geom effs
profiler.IncludeAlgorithms += ["RichGeomEffDown", "RichGeomEffUp", "RichGeomEffLong"]

# Mini Brunel Algorithms
profiler.IncludeAlgorithms += [
    "PrStoreFTHit",
    "PrStoreUTHit",
    "VPClustering",
    "PrPixelTrackingFast",
    "PatPV3D",
    "PrVeloUTFast",
    "PrForwardTrackingFast",
    "ForwardFitterAlgParamFast",
]

# -------------------------------- Runtime options -----------------------------------------

# Setup
# source /cvmfs/projects.cern.ch/intelsw/psxe/linux/all-setup.sh

# To avoid filling up /tmp
# export TMPDIR=/group/rich/jonesc/IntelProfile/tmp
# on lab35
# export TMPDIR=/localdisk/jonesc/tmp

# sudo sh -c 'echo 0 > /proc/sys/kernel/perf_event_paranoid'

# Example command line for the Intel profiler

# amplxe-cl --collect advanced-hotspots -start-paused -follow-child -target-duration-type=short -no-allow-multiple-runs -no-analyze-system -data-limit=800 -q -- gaudirun.py -T  ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,IntelProfiler.py,data/MCXDstUpgradeFiles.py}

# Example for the vectorisation advisor

# advixe-cl --collect survey --project-dir ./advi --search-dir src:r=/group/rich/jonesc/LHCbCMake/Feature -- gaudirun.py -T  ~/LHCbCMake/Feature/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/MCXDstUpgradeFiles.py}

# -------------------------------- Build / Install options -----------------------------------------

# Kernel drivers

# cp -r /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2018/vtune_amplifier_2018/sepdk .

# https://software.intel.com/en-us/vtune-amplifier-help-building-and-installing-the-sampling-drivers-for-linux-targets
