###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Brunel.Configuration import *
from Gaudi.Configuration import *

importOptions("$BRUNELROOT/tests/options/testBrunel-defaults.py")

importOptions("$PRCONFIGOPTS/Brunel/PR-COLLISION11-Beam3500GeV-VeloClosed-MagDown.py")

importOptions("$APPCONFIGOPTS/Brunel/MonitorExpress.py")

Brunel().EvtMax = 200
Brunel().PackType = "MDF"
MessageSvc().countInactive = True
Brunel().OnlineMode = True

# Brunel().SkipEvents = 164
