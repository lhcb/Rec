#!/usr/bin/env python3

###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import print_function

import glob
import multiprocessing
import os
import subprocess
from contextlib import closing
from shutil import which


def merge(dir):
    # Switch to directory to merge in
    if not os.path.isdir(dir):
        print(dir, "does not exist")
    else:
        os.chdir(dir)

        # Find files in sub-jobs
        files = sorted(glob.glob("jobs/job*/*.root"))
        if len(files) > 0:
            # group files by base name
            merge_list = {}
            for f in files:
                basef = os.path.basename(f)
                if basef not in merge_list.keys():
                    merge_list[basef] = []
                merge_list[basef] += [f]

            # loop over different files
            for merged in merge_list.keys():
                print(
                    "Merging",
                    merged,
                    "in",
                    dir,
                    "with",
                    len(merge_list[merged]),
                    "files",
                )
                # if merged file present first remove
                subprocess.call(["rm", "-f", merged])
                # run merge
                subprocess.call(
                    ["nice", "-n10", "hadd", "-v", "0", "-ff", merged]
                    + merge_list[merged]
                )
                # delete input files
                subprocess.call(["rm"] + merge_list[merged])

            # tar up jobs directory and remove
            subprocess.call(["nice", "-n10", "tar", "-jcf", "jobs.tar.bz2", "jobs"])
            subprocess.call(["rm", "-r", "jobs"])

        else:
            print("Found no files to merge in", dir)


def directory_find(atom, root="."):
    # Find all directories to merge below root
    paths = []
    for path, dirs, files in os.walk(root):
        if atom in dirs:
            paths += [path]
    return paths


if __name__ == "__main__":
    # Check hadd exists before proceeding
    merger = "hadd"
    if which(merger) is None:
        print(merger, "not found")
    else:
        # Find jobs to merge
        dirs = directory_find("jobs", os.getcwd())
        print("Found", len(dirs), "directories to merge")

        # Get the number of processors available
        num_cpu = min(16, multiprocessing.cpu_count())
        print("Using", num_cpu, "processing pools")
        # threads
        threads = []

        # Run jobs in parrallel using process pool
        with closing(multiprocessing.Pool(num_cpu)) as pool:
            pool.map(merge, dirs)
            pool.terminate()
