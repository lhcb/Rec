#!/bin/bash
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Parse script arguments
if [ -z "$1" ] ; then
    echo "First argument is job name"
    exit 1
fi
JobName=$1
if [ -z "$2" ] ; then
    # echo "Second argument is number of files"
    export NumFiles=-1
else
    export NumFiles=$2
fi
if [ -z "$3" ] ; then
    # echo "Third argument is files per job"
    export FilesPerJob=-1
else
    export FilesPerJob=$3
fi

if [[ ${NumFiles} -gt 0 ]] ; then
    # Deduce number of jobs
    if [ $((NumFiles%FilesPerJob)) != 0 ] ; then
        echo "Number files ("${NumFiles}") is not an exact multiple of batch size ("${FilesPerJob}")"
        exit 1
    fi
    export NumJobs=$((NumFiles/FilesPerJob))
else
    export NumJobs=1
fi

# Job directory
#JobDir=`pwd`"/CondorJobs/"${JobName}
JobDir=${HOME}"/LHCb/output/CondorJobs/"${JobName}
# Check if dir already exists
if [ -d "$JobDir" ]; then
    echo "ERROR: Job Directory '$JobDir' already exists. Aborting."
    exit 1
fi
echo "Job Directory "${JobDir}
#rm -rf ${JobDir}
mkdir -p ${JobDir}

# 30000000 MinBias
# 10000000 InclB

# Jobs options
mkdir -p ${JobDir}"/jobs"
OptsRoot="${HOME}/LHCb/stack/Feature/Rec/Rich/RichFutureRecSys/examples/"
cp ${OptsRoot}"RichFuture.py" ${JobDir}/"jobs/opts.py"
cp ${OptsRoot}"data/PMTs/UpgradeII/WithSpill/DIGI/10000000.py" ${JobDir}"/jobs/EvtType.py"
cp ${OptsRoot}"data/PMTs/UpgradeII/WithSpill/DIGI/files.py" ${JobDir}"/jobs/files.py"
export RunOptions="${JobDir}/jobs/opts.py ${JobDir}/jobs/EvtType.py ${JobDir}/jobs/files.py"

# Create run script
JobScript=${JobDir}"/jobs/run.sh"
rm -f ${JobScript}
cat > ${JobScript} << EOFSCRIPT
#!/bin/bash

JobRootDir=\$1
JobBatch=\$2
FilesPerJob=\$3

JobDir=\${JobRootDir}"/jobs/job"\${JobBatch}
echo "Job Dir "\${JobDir}
mkdir -p \${JobDir}
cd \${JobDir}

LogFile=\${JobDir}"/job.log"

export CONDOR_FILE_BATCH=\${JobBatch}
export CONDOR_FILES_PER_BATCH=\${FilesPerJob}

# Count root files already existing
#rootFileC=\`find . -name "*.root" -printf '.' | wc -m\`
#if [[ ${rootFileC} -gt 0 ]] ; then
#   echo "ROOT files already exist. Aborting.
#   exit 0
#fi

gaudirun.py \${RunOptions} 2>&1 | cat > \${LogFile}

exit 0
EOFSCRIPT
chmod +x ${JobScript}

# Determine batch OS to target
if [ $(cat /etc/redhat-release | grep "CentOS Stream release 9" | wc -l) -eq "1" ] ||
   [ $(cat /etc/redhat-release | grep "AlmaLinux release 9"     | wc -l) -eq "1" ]; then
    export CONDOROPSYS="AlmaLinux9"
else
    export CONDOROPSYS="CentOS7"
fi

# Condor log directory
export CondorLogDir=${HOME}/CondorLogs/${JobName}
#rm -rf ${CondorLogDir}
mkdir -p ${CondorLogDir}

# Create condor file
CondorScript=${JobDir}"/jobs/condor.job"
rm -f ${CondorScript}
cat > ${CondorScript} << EOFCONDOR

# Condor environment
Universe                = vanilla
getenv                  = true
copy_to_spool           = true
should_transfer_files   = YES
when_to_transfer_output = ON_EXIT_OR_EVICT
environment = CONDOR_ID=\$(Cluster).\$(Process)
JobBatchName            = ${JobName//}

# Requirements
#Requirements = (HAS_r01 && HAS_r02 && OpSysAndVer == "CentOS7" && TARGET.has_avx2)
Requirements = (HAS_r01 && HAS_r02 && OpSysAndVer == "${CONDOROPSYS}")

# Rank hosts according to floating point speed
Rank = kflops

# Memory requirement (in MB)
request_memory = 1500

# Use suspendable slots when available
# +IsSuspendableJob = True

# Condor Output
output = ${CondorLogDir}/out.\$(Process)
error  = ${CondorLogDir}/err.\$(Process)
Log    = ${CondorLogDir}/log.\$(Process)

# =============================================================================
# Submit the job script
# =============================================================================

# Arguments are <name> <job-number> <number files per job>
Executable       = ${JobScript}
Arguments        = ${JobDir} \$(Process) ${FilesPerJob}

# Submit the number of jobs required
Queue ${NumJobs}

EOFCONDOR

# Finally, submit the job
condor_submit ${CondorScript}

# cache the env to file for reference
env | cat > ${JobDir}"/jobs/env.txt"

exit 0
