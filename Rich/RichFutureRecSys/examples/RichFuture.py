###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# --------------------------------------------------------------------------------------

import os
import socket

from Configurables import (
    DDDBConf,
    FPEAuditor,
    GaudiSequencer,
    LHCbApp,
    MCFTHitUnpacker,
    MCParticle2MCHitAlg,
    MCRichDigitSummaryUnpacker,
    MCRichHitUnpacker,
    MCRichOpticalPhotonUnpacker,
    MCRichSegmentUnpacker,
    MCRichTrackUnpacker,
    MCUTHitUnpacker,
    MCVPHitUnpacker,
    UnpackMCParticle,
    UnpackMCVertex,
)
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Gaudi.Configuration import *
from GaudiConfig.ControlFlow import seq
from GaudiKernel.SystemOfUnits import GeV

# --------------------------------------------------------------------------------------


# Add PID information
def addInfo(alg, typ, name):
    t = alg.addTool(typ, name)
    alg.AddInfo += [t]
    return t


def boolOpt(var, defVal):
    if var in os.environ:
        val = os.environ[var]
        print("INFO: Using ENV VAR value for", var, "=", val)
        if val.upper() == "TRUE" or val == "1":
            return True
        if val.upper() == "FALSE" or val == "0":
            return False
        raise ValueError("could not decode boolean option", var, val)
    print("INFO: Using default value for", var, "=", defVal)
    return defVal


def floatOpt(var, defVal):
    if var in os.environ:
        val = os.environ[var]
        print("INFO: Using ENV VAR value for", var, "=", val)
        return float(val)
    print("INFO: Using default value for", var, "=", defVal)
    return float(defVal)


# --------------------------------------------------------------------------------------

# Print hostname
print("Host", socket.gethostname())

# Use own DB clones
# os.environ['GITCONDDBPATH'] = '/usera/jonesc/NFS/GitDB'

# Are we running in a batch job ?
batchMode = "CONDOR_FILE_BATCH" in os.environ

# histograms
# histos = "OfflineFull"
histos = "Expert"

rootFileBaseName = "RichFuture"
if not batchMode:
    myBuild = os.getenv("User_release_area", "None").split("/")[-1]
    myConf = os.getenv("BINARY_TAG", "None")
    rootFileBaseName += "-" + myBuild + "-" + myConf + "-" + histos
RootHistoSink().FileName = rootFileBaseName + "-Histos.root"
ApplicationMgr().HistogramPersistency = "ROOT"

# Event numbers
nEvents = 1000 if not batchMode else 99999999
EventSelector().PrintFreq = 100 if not batchMode else 250
# LHCbApp().SkipEvents     = 2

# Just to initialise
DDDBConf()
LHCbApp()

# Timestamps in messages
# LHCbApp().TimeStamp = True

msgSvc = getConfigurable("MessageSvc")
# msgSvc.OutputLevel = 1
msgSvc.Format = "% F%40W%S%7W%R%T %0W%M"
from Configurables import SequencerTimerTool

SequencerTimerTool("ToolSvc.SequencerTimerTool").NameSize = 40

# Auditors
AuditorSvc().Auditors += ["FPEAuditor"]
FPEAuditor().ActivateAt = ["Execute"]
# AuditorSvc().Auditors += ["NameAuditor"]

# JIT cache
# if batchMode:
from Configurables import FunctorFactory

# FunctorFactory().DisableJIT = True
FunctorFactory().DisableCache = True

# The overall sequence.Filled below.
all = GaudiSequencer("All", MeasureTime=True)

# --------------------------------------------------------------------------------------

# Finally set up the application
app = ApplicationMgr(
    TopAlg=[all],
    EvtMax=nEvents,  # events to be processed
    ExtSvc=[ToolSvc(), AuditorSvc(), MessageSvcSink(), RootHistoSink()],
    AuditAlgorithms=True,
)

# Are we using DD4Hep ?
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hep = DD4hepSvc(DetectorList=["/world", "Magnet", "Rich1", "Rich2"])
    # dd4hep.DumpConditions = True
    app.ExtSvc += [dd4hep]

# --------------------------------------------------------------------------------------

# Reco options
useMCTracks = boolOpt("USEMCTRACKS", False)
useMCHits = boolOpt("USEMCHITS", False)

# Get time windows from environment vars if set
# otherwise use default values as passed.
PixWin = floatOpt("PIXWIN", "1.000")
PhWinInnR1 = floatOpt("R1INNERPHOTWIN", "0.1")
PhWinOutR1 = floatOpt("R1OUTERPHOTWIN", "0.2")
PhWinInnR2 = floatOpt("R2INNERPHOTWIN", "0.1")
PhWinOutR2 = floatOpt("R2OUTERPHOTWIN", "0.2")

# Enable 4D reco in each RICH
is4D = boolOpt("ENABLE4D", False)
enable4D = (is4D, is4D)
# Average expected time for signal
avSignalTime = (13.03, 52.94)
# Pixel time window size in each RICH (ns)
pixTimeWindow = (PixWin, PixWin)
# Photon reco time window size in each RICH (ns) (inner, outer) regions
photTimeWindow = ((PhWinInnR1, PhWinInnR2), (PhWinOutR1, PhWinOutR2))

# Min Momentum cuts
minP = (1.0 * GeV, 1.0 * GeV)
minPt = 0.2 * GeV

# Add time to reco tracks using MC
# Only used if useMCTracks = False and 4D enabled
tkSegAddTimeFromMC = (enable4D[0] or enable4D[1]) and not useMCTracks

# Add time info to Rich hits
# Only if useMCHits = False and 4D enabled
usePixelMCTime = (enable4D[0] or enable4D[1]) and not useMCHits

# Cheat pixel positions using MC info
usePixelMCPos = boolOpt("USEPIXELMCINFO", False)

# Cheat photon parameters from MC
photUseMC = boolOpt("USEPHOTONMCINFO", False)

# Cheat expected track CK theta valus from MC
tkSegUseMCExpCKTheta = False

# Filter 'real' tracks based on MC info.
# Designed to help match those made from MCRichTracks, where certain
# track types cannot be made( e.g. ghosts, or BT tracks ).
filterTksUsingMC = False

# Override Inner/Outer PD regions from options
# pixOverrideRegions = (False,False)
pixOverDR = boolOpt("OVERRIDEDETREGIONS", False)
pixOverrideRegions = (pixOverDR, pixOverDR)

# Unpacking sequence before reco is run
preUnpackSeq = GaudiSequencer("PreUnpackSeq", MeasureTime=True)
all.Members += [preUnpackSeq]

# Fetch required data from file
from Configurables import Gaudi__Hive__FetchDataFromFile as FetchDataFromFile

fetcher = FetchDataFromFile("FetchDSTData")
fetcher.DataKeys = ["Trigger/RawEvent"]
preUnpackSeq.Members += [fetcher]

# Unpack the ODIN raw event
odinSeq = GaudiSequencer("ODINSeq", MeasureTime=True)
all.Members += [odinSeq]
from Configurables import LHCb__UnpackRawEvent as UnpackRawEvent

odinSeq.Members += [
    UnpackRawEvent(
        "UnpackODIN",
        BankTypes=["ODIN"],
        RawEventLocation="Trigger/RawEvent",
        RawBankLocations=["DAQ/RawBanks/ODIN"],
    )
]
# Create the ODIN object from its raw bank
from Configurables import createODIN

decodeODIN = createODIN("ODINDecode")
odinSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

    all.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

if (
    tkSegAddTimeFromMC
    or useMCHits
    or useMCTracks
    or filterTksUsingMC
    or tkSegUseMCExpCKTheta
):
    preUnpackSeq.Members += [UnpackMCVertex(), UnpackMCParticle()]

if usePixelMCPos:
    from Configurables import Rich__Future__Rec__MC__RichPixelUseMCInfo as PixUseMCInfo

    pixInfoAlg = PixUseMCInfo("RichSIMDPixelsUseMCInfo")
    innerPixQR1 = floatOpt("R1INNERPIXQUANT", "2.8")
    outerPixQR1 = floatOpt("R1OUTERPIXQUANT", "5.6")
    innerPixQR2 = floatOpt("R2INNERPIXQUANT", "2.8")
    outerPixQR2 = floatOpt("R2OUTERPIXQUANT", "5.6")
    pixInfoAlg.InnerPixelQuantization = (innerPixQR1, innerPixQR2)
    pixInfoAlg.OuterPixelQuantization = (outerPixQR1, outerPixQR2)
    innerPixEffR1 = floatOpt("R1INNERPIXEFF", "1.0")
    outerPixEffR1 = floatOpt("R1OUTERPIXEFF", "1.0")
    innerPixEffR2 = floatOpt("R2INNERPIXEFF", "1.0")
    outerPixEffR2 = floatOpt("R2OUTERPIXEFF", "1.0")
    pixInfoAlg.InnerPixelEff = (innerPixEffR1, innerPixEffR2)
    pixInfoAlg.OuterPixelEff = (outerPixEffR1, outerPixEffR2)

if usePixelMCPos or usePixelMCTime or useMCHits:
    fetcher.DataKeys += ["pSim/Rich/Hits"]
    preUnpackSeq.Members += [MCRichHitUnpacker()]
    for spill in ["PrevPrev", "Prev", "Next", "NextNext", "LHCBackground"]:
        preUnpackSeq.Members += [
            MCRichHitUnpacker(
                spill + "MCRichHitUnpack",
                InputName="/Event/" + spill + "/pSim/Rich/Hits",
                OutputName="/Event/" + spill + "/MC/Rich/Hits",
            )
        ]

if not useMCHits:
    # Run regular rawEvent decoding
    fetcher.DataKeys += ["Rich/RawEvent"]
    from Configurables import Rich__Future__RawBankDecoder as RichDecoder

    unpack_rich = UnpackRawEvent(
        "UnpackRich",
        BankTypes=["Rich"],
        RawEventLocation="Rich/RawEvent",
        RawBankLocations=["DAQ/RawBanks/Rich"],
    )
    richDecode = RichDecoder("RichDecodeRawBanks")
    preUnpackSeq.Members += [unpack_rich]
    all.Members += [richDecode]
    # If asked for update decoded data with time from MC
    if usePixelMCTime:
        richDecode.DecodedDataLocation = (
            str(richDecode.DecodedDataLocation) + "BeforeMCCheat"
        )
        from Configurables import (
            Rich__Future__MC__DecodedDataAddMCInfo as DecodedDataAddMCInfo,
        )

        all.Members += [
            DecodedDataAddMCInfo(
                "RichDecodedDataAddMCInfo",
                InDecodedDataLocation=richDecode.DecodedDataLocation,
            )
        ]
else:
    # Emulate RICH decoded data from extended MC info from Gauss
    from Configurables import (
        Rich__Future__MC__DecodedDataFromMCRichHits as RichMCDecoder,
    )

    richMCDecode = RichMCDecoder("RichDecodeFromMC")
    richMCDecode.IsDetDescMC = True
    richMCDecode.RejectAllBackgrounds = False
    richMCDecode.RejectScintillation = False
    richMCDecode.IncludeTimeInfo = True
    richMCDecode.AllowMultipleHits = True
    richMCDecode.MaxHitsPerChannel = (99999, 99999)
    # Nominal (0.95, 0.95)
    # 50% resolution (0.75, 0.85)
    # 25% resolution (0.55, 0.75)
    richMCDecode.ReadoutEfficiency = (
        floatOpt("R1DETEFF", "0.95"),
        floatOpt("R2DETEFF", "0.95"),
    )
    all.Members += [richMCDecode]

#  Now get the RICH sequence

if tkSegUseMCExpCKTheta or photUseMC or filterTksUsingMC or useMCTracks:
    # Build track info from RICH extended MC info
    fetcher.DataKeys += [
        "pSim/Rich/Segments",
        "pSim/Rich/Tracks",
        "pSim/MCVertices",
        "pSim/MCParticles",
    ]
    preUnpackSeq.Members += [
        MCRichHitUnpacker(),
        MCRichOpticalPhotonUnpacker(),
        MCRichSegmentUnpacker(),
        MCRichTrackUnpacker(),
    ]

if not useMCTracks:
    # Explicitly unpack the Tracks
    fetcher.DataKeys += ["pRec/Track/Best"]
    from Configurables import UnpackTrack

    tkUnpack = UnpackTrack("UnpackTracks")
    preUnpackSeq.Members += [tkUnpack]

    # Input tracks
    inTracks = "Rec/Track/Best"

    if filterTksUsingMC:
        from Configurables import (
            Rich__Future__MC__TrackToMCParticleRelations as TkToMCPRels,
        )

        tkRels = TkToMCPRels("TkMCLongFilterRels")
        tkRels.TrackToMCParticlesRelations = "Rec/Track/BestLongRichMCRels"
        from Configurables import Rich__Future__Rec__MC__TrackFilter as TkMCFilter

        tkMCFilter = TkMCFilter("TkMCLongFilter")
        tkMCFilter.InTracksLocation = inTracks
        inTracks = "Rec/Track/BestRichMCFiltered"
        tkMCFilter.OutTracksLocation = inTracks
        tkMCFilter.TrackToMCParticlesRelations = tkRels.TrackToMCParticlesRelations
        all.Members += [tkRels, tkMCFilter]

    # Filter the tracks by type
    from Configurables import TracksSharedSplitterPerType as TrackFilter

    tkFilt = TrackFilter("TrackTypeFilter")
    tkFilt.InputTracks = inTracks
    tkFilt.LongTracks = "Rec/Track/BestLong"
    tkFilt.DownstreamTracks = "Rec/Track/BestDownstream"
    tkFilt.UpstreamTracks = "Rec/Track/BestUpstream"
    tkFilt.Ttracks = "Rec/Track/BestTtrack"
    tkFilt.VeloTracks = "Rec/Track/BestVelo"

    all.Members += [tkFilt]

    # Input tracks
    # tkLocs = {
    # "Long" : tkFilt.LongTracks,
    # "Down" : tkFilt.DownstreamTracks,
    # "Up" : tkFilt.UpstreamTracks,
    # "Seed" : tkFilt.Ttracks
    # }
    # tkLocs = {"All" : tkFilt.InputTracks }
    tkLocs = {"Long": tkFilt.LongTracks}
    # tkLocs = {"Up" : tkFilt.UpstreamTracks }
    # tkLocs = {"Down" : tkFilt.DownstreamTracks }

    # Output PID
    # pidLocs = {
    # "Long" : "Rec/Rich/LongPIDs",
    # "Down" : "Rec/Rich/DownPIDs",
    # "Up" : "Rec/Rich/UpPIDs",
    # "Seed" : "Rec/Rich/SeedPIDs"
    # }
    # pidLocs = {"All" : "Rec/Rich/PIDs" }
    pidLocs = {"Long": "Rec/Rich/LongPIDs"}
    # pidLocs = {"Up" : "Rec/Rich/UpPIDs" }
    # pidLocs = {"Down" : "Rec/Rich/DownPIDs" }

else:
    # Needed for ideal state creator
    unpVPHits = MCVPHitUnpacker()
    unpFTHits = MCFTHitUnpacker()
    unpUTHits = MCUTHitUnpacker()
    linkVP = MCParticle2MCHitAlg(
        "LinkVPMCHits", MCHitPath=unpVPHits.OutputName, OutputData="Link/MC/VP/Hits"
    )
    linkFF = MCParticle2MCHitAlg(
        "LinkFTMCHits", MCHitPath=unpFTHits.OutputName, OutputData="Link/MC/FT/Hits"
    )
    linkUT = MCParticle2MCHitAlg(
        "LinkUTMCHits", MCHitPath=unpUTHits.OutputName, OutputData="Link/MC/UT/Hits"
    )
    preUnpackSeq.Members += [unpVPHits, unpFTHits, unpUTHits, linkVP, linkFF, linkUT]

    # Fake Input tracks
    inTracks = "Rec/Track/FromRichMCTracks"
    # Use RichMCTracks as input
    tkLocs = {"MC": inTracks}
    pidLocs = {"MC": "Rec/Rich/MCPIDs"}

# track extrapolator type
# tkExtrap = "TrackRungeKuttaExtrapolator/TrackExtrapolator"
tkExtrap = "TrackSTEPExtrapolator/TrackExtrapolator"

# RICH radiators to use
rads = ["Rich1Gas", "Rich2Gas"]
# rads = ["Rich1Gas"]
# rads = ["Rich2Gas"]

# PID types to process
parts = ["electron", "muon", "pion", "kaon", "proton", "deuteron", "belowThreshold"]
# parts = ["pion", "kaon", "proton", "deuteron", "belowThreshold"]

photonRecoType = "Quartic"
# photonRecoType = "FastQuartic"
# photonRecoType = "CKEstiFromRadius"

photonSel = "Nominal"
# photonSel = "Online"
# photonSel = "None"

# Number ring points for ray tracing( scaled by CK theta )
ringPointsMin = (16, 16)
ringPointsMax = (96, 96)

# Compute the ring share CK theta values
# rSTol        = 0.075 #as fraction of sat.CK theta
# newCKRingTol = ( rSTol, rSTol )
newCKRingTol = (0.05, 0.1)

# Detectable Yield calculation
detYieldPrecision = "Average"
# detYieldPrecision = "Full"

# Track CK resolution treatment
# tkCKResTreatment = "Functional"
tkCKResTreatment = "Parameterised"

# Pixel background weights
WR1 = floatOpt("PIXBCKWEIGHTRICH1", ("1.0" if enable4D[0] else "1.0"))
WR2 = floatOpt("PIXBCKWEIGHTRICH2", ("1.0" if enable4D[0] else "1.0"))
pdBckWeights = [(WR1, WR2), (WR1, WR2), (WR1, WR2), (WR1, WR2)]

# Hit treatment in background alg
# Need to try and understand the difference here between best 3D and 4D tunings
PDBackIgnoreHitData = [is4D, False, False, False]

# background thresholds
bT = 0.0
bkgThres = [(bT, bT), (bT, bT), (bT, bT), (bT, bT)]

# CK resolutions
from RichFutureRecSys.ConfiguredRichReco import defaultNSigmaCuts

ckResSigma = defaultNSigmaCuts()
# S = 4.5
# for tk in tkLocs.keys():
# #Override presel cuts
# ckResSigma[photonSel][tkCKResTreatment][tk][0] = ( S, S, S )
# #Override sel cuts
# ckResSigma[photonSel][tkCKResTreatment][tk][1] = ( 99, 99, 99 )

# Track CK res scale factors (RICH1, RICH2) (Inner, Outer)
scF = (
    # Inner (R1,R2)
    (floatOpt("INNERR1RESSCALEF", "1.0"), floatOpt("INNERR2RESSCALEF", "1.0")),
    # Outer (R1,R2)
    (floatOpt("OUTERR1RESSCALEF", "1.0"), floatOpt("OUTERR2RESSCALEF", "1.0")),
)

# CK theta resolution funcs to use for the emulation when enabled
defResFs = (
    # Inner (R1,R2)
    (
        "(0.00078+0.0012*(std::tanh(-x/5000.0)+1.0))",
        "(0.00065+0.0011*(std::tanh(-x/5000.0)+1.0))",
    ),
    # Outer (R1,R2)
    (
        "(0.00078+0.0012*(std::tanh(-x/5000.0)+1.0))",
        "(0.00065+0.0011*(std::tanh(-x/5000.0)+1.0))",
    ),
)
photEmulatedResMC = (
    # Inner (R1,R2)
    (
        str(scF[0][0]) + "*" + os.getenv("INNERR1CKRESFUNC", defResFs[0][0]),
        str(scF[0][1]) + "*" + os.getenv("INNERR2CKRESFUNC", defResFs[0][1]),
    ),
    # Outer (R1,R2)
    (
        str(scF[1][0]) + "*" + os.getenv("OUTERR1CKRESFUNC", defResFs[1][0]),
        str(scF[1][1]) + "*" + os.getenv("OUTERR2CKRESFUNC", defResFs[1][1]),
    ),
)

# Track CK res scale factors
# tkCKResScaleF = None  # Use nominal values (1.0)
# To Do. Improve this....
scFRes = max(max(scF[0][0], scF[1][0]), max(scF[0][1], scF[1][1]))
if scFRes < 0.5:
    scFRes = 0.5
tkCKResScaleF = (scFRes, scFRes)

# Merged output
finalPIDLoc = "Rec/Rich/PIDs"

# DataType
dType = "Upgrade"

# Online Brunel mode.
# online = True
online = False

# Create missing track states
createStates = False

# Perform clustering ?
pixelClustering = (False, False)
# pixelClustering = ( True, True )

# Truncate CK angles ?
# truncateAngles = ( False, False )
truncateAngles = (True, True)

# Max PD occupancy
maxPDOcc = (
    int(os.getenv("MAXPDOCCR1", "99999")),
    int(os.getenv("MAXPDOCCR2", "99999")),
)

# Minimum photon probability
minPhotonProb = None  # Nominal

# Likelihood Settings
# Number Iterations
nIts = 2
# likelihood thresholds
likeThres = [-1e-2, -1e-3, -1e-4, -1e-5]  # nominal
# likeThres = [-1e-3, -1e-3, -1e-4, -1e-5]

# OutputLevel( -1 means no setting )
outLevel = -1

# --------------------------------------------------------------------------------------
# The reconstruction
from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence

RichRec = RichRecoSequence(
    OutputLevel=outLevel,
    dataType=dType,
    onlineBrunelMode=online,
    enable4DReco=enable4D,
    tkSegAddTimeFromMC=tkSegAddTimeFromMC,
    pixUsePosFromMC=usePixelMCPos,
    pixOverrideRegions=pixOverrideRegions,
    photUseMC=photUseMC,
    photEmulatedResMC=photEmulatedResMC,
    tkSegUseMCExpCKTheta=tkSegUseMCExpCKTheta,
    averageHitTime=avSignalTime,
    pixelTimeWindow=pixTimeWindow,
    photonTimeWindow=photTimeWindow,
    photonReco=photonRecoType,
    photonSelection=photonSel,
    radiators=rads,
    particles=parts,
    createMissingStates=createStates,
    MinP=minP,
    MinPt=minPt,
    NRingPointsMax=ringPointsMax,
    NRingPointsMin=ringPointsMin,
    NewCKRingTol=newCKRingTol,
    minPhotonProbability=minPhotonProb,
    detYieldPrecision=detYieldPrecision,
    tkCKResTreatment=tkCKResTreatment,
    tkCKResScaleFactors=tkCKResScaleF,
    nSigmaCuts=ckResSigma,
    applyPixelClustering=pixelClustering,
    maxPDOccupancy=maxPDOcc,
    truncateCKAngles=truncateAngles,
    trackExtrapolator=tkExtrap,
    PDBckWeights=pdBckWeights,
    PDBackIgnoreHitData=PDBackIgnoreHitData,
    PDBackThresholds=bkgThres,
    nIterations=nIts,
    LikelihoodThreshold=likeThres,
    inputTrackLocations=tkLocs,
    outputPIDLocations=pidLocs,
    mergedOutputPIDLocation=finalPIDLoc,
)

# The final sequence to run
all.Members += [RichRec]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Simple debugging / testing
enableDebug = False
if enableDebug:
    from Configurables import Rich__Future__TestDecodeAndIDs as TestDecodeAndIDs
    from Configurables import (
        Rich__Future__TestDerivedDetObjects as TestDerivedDetObjects,
    )

    all.Members += [
        TestDerivedDetObjects("TestDerivedDets", OutputLevel=1),
        TestDecodeAndIDs("TestDecodeAndIDs"),
    ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Data Monitoring
enableDataMoni = True
if enableDataMoni:
    from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors

    all.Members += [
        RichRecMonitors(
            enable4DReco=enable4D,
            inputTrackLocations=tkLocs,
            radiators=rads,
            applyPixelClustering=pixelClustering,
            onlineBrunelMode=online,
            outputPIDLocations=pidLocs,
            histograms=histos,
        )
    ]
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# MC Checking and PID tupling

enableMCChecks = True
enablePIDTuples = True

if enableMCChecks or enablePIDTuples:
    fetcher.DataKeys += [
        "pSim/MCVertices",
        "pSim/MCParticles",
        "pSim/Rich/Hits",
        "pSim/Rich/Segments",
        "pSim/Rich/Tracks",
        "pSim/Rich/OpticalPhotons",
    ]
    # Unpacking sequence after reco is run
    postUnpackSeq = GaudiSequencer("PostUnpackSeq", MeasureTime=True)
    all.Members += [postUnpackSeq]
    postUnpackSeq.Members += [
        UnpackMCVertex(),
        UnpackMCParticle(),
        MCRichHitUnpacker(),
        MCRichOpticalPhotonUnpacker(),
        MCRichSegmentUnpacker(),
        MCRichTrackUnpacker(),
    ]
    if not useMCHits:
        postUnpackSeq.Members += [MCRichDigitSummaryUnpacker()]

if enableMCChecks:
    from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecCheckers

    all.Members += [
        RichRecCheckers(
            enable4DReco=enable4D,
            inputTrackLocations=tkLocs,
            radiators=rads,
            applyPixelClustering=pixelClustering,
            onlineBrunelMode=online,
            outputPIDLocations=pidLocs,
            histograms=histos,
        )
    ]

if enablePIDTuples:
    from Configurables import NTupleSvc

    app.ExtSvc.append(NTupleSvc())
    # ProtoParticle monitoring.For detailed PID monitoring.
    protoMoniSeq = GaudiSequencer("ProtoMonitoring", MeasureTime=True)
    all.Members += [protoMoniSeq]
    # Remake the protos with the new Rich PID objects
    from Configurables import (
        ChargedProtoParticleAddCombineDLLs,
        ChargedProtoParticleAddRichInfo,
        DelegatingTrackSelector,
        FunctionalChargedProtoParticleMaker,
        TrackSelector,
    )

    makecproto = FunctionalChargedProtoParticleMaker("ProtoPMaker", AddInfo=[])
    makecproto.Inputs = [inTracks]
    makecproto.OutputPIDs = "Rec/ProtoPMakerPIDs"
    makecproto.addTool(DelegatingTrackSelector, name="TrackSelector")
    tracktypes = ["Long", "Upstream", "Downstream", "Ttrack"]
    makecproto.TrackSelector.TrackTypes = tracktypes
    selector = makecproto.TrackSelector
    for tsname in tracktypes:
        selector.addTool(TrackSelector, name=tsname)
        ts = getattr(selector, tsname)
        ts.TrackTypes = [tsname]
    rich = addInfo(makecproto, ChargedProtoParticleAddRichInfo, "AddRich")
    comb = addInfo(makecproto, ChargedProtoParticleAddCombineDLLs, "CombDLLs")
    protoMoniSeq.Members += [makecproto]
    # monitoring config
    from Configurables import GlobalRecoChecks

    GlobalRecoChecks().Sequencer = protoMoniSeq
    GlobalRecoChecks().ProtoTupleName = rootFileBaseName + "-ProtoTuple.root"
# --------------------------------------------------------------------------------------

# --------------------------------------------------------------------------------------
# Example command lines
# --------------------------------------------------------------------------------------

# Normal running( old data format )
# gaudirun.py ~/LHCb/stack/Master/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCFormat/MCXDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${BINARY_TAG}.log

#  Normal running (realistic data format)
# gaudirun.py ~/LHCb/stack/Master/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/RealTel40Format/MCLDstFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${BINARY_TAG}.log

# Run on XSIM files
# gaudirun.py ~/LHCb/stack/Master/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/MCFormat/MCXSimFiles.py} 2>&1 | tee RichFuture-${User_release_area##/*/}-${BINARY_TAG}.log

# DD4HEP tests
# GITCONDDBPATH='/usera/jonesc/NFS/GitDB' gaudirun.py ....

# Upgrade II Studies
# gaudirun.py ~/LHCb/stack/Master/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/UpgradeII/NoSpill/DIGI/{30000000.py,files.py}} 2>&1 | tee RichFuture-${User_release_area##/*/}-${BINARY_TAG}.log
# gaudirun.py ~/LHCb/stack/Master/Rec/Rich/RichFutureRecSys/examples/{RichFuture.py,data/PMTs/UpgradeII/WithSpill/DIGI/{30000000.py,files.py}} 2>&1 | tee RichFuture-${User_release_area##/*/}-${BINARY_TAG}.log

# --------------------------------------------------------------------------------------
