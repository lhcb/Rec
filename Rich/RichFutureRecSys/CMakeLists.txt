###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichFutureRecSys
---------------------
#]=======================================================================]

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)

if(NOT USE_DD4HEP)
    set_property(
        TEST
            RichFutureRecSys.decode.2022-data
            RichFutureRecSys.decode.2023-data
            RichFutureRecSys.decode.2024-data
        PROPERTY
            DISABLED TRUE
    )
endif()
