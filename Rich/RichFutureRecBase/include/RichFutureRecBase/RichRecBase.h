/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichRecBase.h
 *
 *  Header file for RICH reconstruction base class : Rich::Rec::CommonBase
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2005-08-26
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <algorithm>

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/ToolHandle.h"

// Interfaces
#include "RichInterfaces/IRichParticleProperties.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichRadiatorType.h"

namespace Rich::Future::Rec {

  //-----------------------------------------------------------------------------
  /** @class CommonBase RichRecBase.h RichRecBase/RichRecBase.h
   *
   *  Base class containing common RICH reconstruction functionality
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2005-08-26
   */
  //-----------------------------------------------------------------------------

  template <class PBASE>
  class CommonBase {

  public:
    /// Standard constructor
    CommonBase( PBASE* base = nullptr ) : m_base( base ) {}

  protected:
    /// Intialise common RICH Reco
    StatusCode initialiseRichReco();

    /// Finalise common RICH Reco
    StatusCode finaliseRichReco();

  private:
    /// Const access to derived class
    inline const PBASE* base() const noexcept { return m_base; }

    /// Non-const access to derived class
    inline PBASE* base() noexcept { return m_base; }

  public:
    /// Access the particle properties tool
    inline decltype( auto ) richPartProps() const noexcept { return m_richPartProp.get(); }

    /// Access the list of all active Particle ID types to consider (including below threshold)
    inline const Rich::Particles& activeParticles() const noexcept { return m_pidTypes; }

    /// Access the list of active Particle ID types to consider (excluding below threshold)
    inline const Rich::Particles& activeParticlesNoBT() const noexcept { return m_pidTypesNoBT; }

    /// Access the list of active RICH detectors
    inline const Rich::Detectors& activeDetectors() const noexcept { return m_activeRiches; }

    /// Access the list of active RICH radiators
    inline const Rich::Radiators& activeRadiators() const noexcept { return m_activeRadiators; }

    /// The lightest active mass hypothesis
    inline Rich::ParticleIDType lightestActiveHypo() const noexcept { return m_pidTypes.front(); }

    /// The heaviest active mass hypothesis
    inline Rich::ParticleIDType heaviestActiveHypo() const noexcept { return m_pidTypes.back(); }

    /// Is given RICH active ?
    inline bool richIsActive( const Rich::DetectorType rich ) const noexcept { return m_richIsActive[rich]; }

    /// Is given radiator active ?
    inline bool radiatorIsActive( const Rich::RadiatorType rad ) const noexcept {
      // FixMe: Eventually remove explicit handling of Aerogel here
      return ( Rich::Aerogel == rad ? false : richIsActive( richType( rad ) ) );
    }

    /// Is given Particle ID in the list being considered
    inline bool particleIsActive( const Rich::ParticleIDType pid ) const noexcept {
      return std::find( m_pidTypes.begin(), m_pidTypes.end(), pid ) != m_pidTypes.end();
    }

    /// Access active RICH detector flags
    inline const Rich::DetectorArray<bool>& richIsActive() const noexcept { return m_richIsActive; }

    /// Access active RICH radiator flags
    inline auto radiatorIsActive() const noexcept {
      return Rich::RadiatorArray<bool>{ false, richIsActive( Rich::Rich1 ), richIsActive( Rich::Rich2 ) };
    }

  private:
    // cached data

    /// Real particle ID types to consider (excluding below threshold)
    Rich::Particles m_pidTypesNoBT;

    /// All particle ID types to consider (including below threshold)
    Rich::Particles m_pidTypes;

    /// All Detector types to consider
    Rich::Detectors m_activeRiches;

    /// All Radiator types to consider
    Rich::Radiators m_activeRadiators;

    /// Pointer to derived class
    PBASE* m_base = nullptr;

  private:
    // properties

    /// Active RICH detectors (RICH1,RICH2)
    Gaudi::Property<DetectorArray<bool>> m_richIsActive{ m_base, "Detectors", { true, true } };

    /// Pointer to RichParticleProperties interface
    PublicToolHandle<const IParticleProperties> m_richPartProp{
        m_base, "ParticlePropertiesTool", "Rich::Future::ParticleProperties/RichPartProp:PUBLIC" };
  };

} // namespace Rich::Future::Rec
