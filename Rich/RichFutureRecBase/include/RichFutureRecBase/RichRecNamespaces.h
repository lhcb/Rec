/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec
 *
 *  General namespace for all RICH reconstruction software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::MC
 *
 *  General namespace for RICH reconstruction MC related software
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/** @namespace Rich::Future::Rec::Moni
 *
 *  General namespace for RICH reconstruction monitoring utilities
 *
 *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
 *  @date   01/02/2017
 */
//-----------------------------------------------------------------------------
