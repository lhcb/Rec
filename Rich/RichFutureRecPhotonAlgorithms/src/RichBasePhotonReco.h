/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Range.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Kernel
#include "Kernel/STLExtensions.h"

// STL
#include <algorithm>
#include <array>
#include <iterator>
#include <sstream>
#include <tuple>
#include <vector>

namespace Rich::Future::Rec {

  /** @class BasePhotonReco RichBasePhotonReco.h
   *
   *  Base class for photon reconstruction algorithms
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class BasePhotonReco : public AlgBase<> {

  public:
    /// Standard constructor
    BasePhotonReco( const std::string& name, ISvcLocator* pSvcLocator ) : AlgBase( name, pSvcLocator ) {}

    /// Initialization after creation
    virtual StatusCode initialize() override;

  protected:
    /// Basic precision (float)
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD version of FP
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;
    /// Type for container of mirror pointers. Same size as SIMDFP.
    using Mirrors = SIMD::STDArray<const Detector::Mirror*, SIMDFP>;

  protected: // helpers
    /// Data Guard for containers. Will remove the last element added
    /// unless explicitly told data is OK
    template <typename TYPE>
    class DataGuard final {
    public:
      /// Set OK for this guard.
      void setOK( const bool ok = true ) noexcept { m_ok = ok; }

    public:
      /// No default constructor.
      DataGuard() = delete;
      /// Constructor with the container to guard
      explicit DataGuard( TYPE& data ) : m_data( data ) {}
      /// Destructor. Removes the last added object unless explicitly told to keep it
      ~DataGuard() noexcept {
        if ( !m_ok ) { m_data.pop_back(); }
      }

    private:
      /// The data container
      TYPE& m_data;
      /// Status flag. Default to false so last entry removed unless explicitly saved.
      bool m_ok = false;
    };

  protected:
    /// Check if the points a and b are on the same detector side
    template <typename POINT>
    inline decltype( auto ) sameSide( const Rich::RadiatorType rad, //
                                      const POINT&             a,   //
                                      const POINT&             b ) const {
      return ( Rich::Rich2Gas == rad ? a.x() * b.x() > 0 : a.y() * b.y() > 0 );
    }

    /// Correction for CK theta
    inline double ckThetaCorrection( const Rich::DetectorType rich ) const {
      return m_ckJOCorrs[rich] + m_ckBiasCorrs[rich];
    }

    /** Absolute maximum Cherenkov theta value to reconstuct for given track segment
     *
     *  @param segment The track segment
     *
     *  @return The maximum Cherenkov angle to reconstruct
     */
    inline double absMaxCKTheta( const Rich::DetectorType rich ) const noexcept { return m_maxCKtheta[rich]; }

    /** Absolute minimum Cherenkov theta value to reconstuct for given track segment
     *
     *  @param segment The track segment
     *
     *  @return The minimum Cherenkov angle to reconstruct
     */
    inline double absMinCKTheta( const Rich::DetectorType rich ) const noexcept { return m_minCKtheta[rich]; }

    /// Check the final Cherenkov theta angle (Scalar)
    template <typename HYPODATA, typename FTYPE,
              typename std::enable_if<std::is_arithmetic<FTYPE>::value>::type* = nullptr>
    inline decltype( auto ) checkAngle( const Rich::DetectorType rich,       //
                                        const HYPODATA&          tkCkAngles, //
                                        const HYPODATA&          tkCkRes,    //
                                        const FTYPE              ckTheta ) const noexcept {
      return (
          // First the basic checks
          ckTheta < absMaxCKTheta( rich ) && ckTheta > absMinCKTheta( rich ) &&
          // Now check each real hypo
          std::any_of( activeParticlesNoBT().begin(), activeParticlesNoBT().end(),
                       [sigma = m_nSigma[rich], &ckTheta, &tkCkAngles, &tkCkRes]( const auto id ) {
                         return fabs( ckTheta - tkCkAngles[id] ) < ( sigma * tkCkRes[id] );
                       } ) );
    }

    /// Check the final Cherenkov theta angle (SIMD)
    template <typename HYPODATA, typename FTYPE, typename MASK = typename FTYPE::mask_type,
              typename std::enable_if<!std::is_arithmetic<FTYPE>::value>::type* = nullptr>
    inline void checkAngle( const Rich::DetectorType rich,       //
                            const HYPODATA&          tkCkAngles, //
                            const HYPODATA&          tkCkRes,    //
                            const FTYPE&             ckTheta,    //
                            MASK&                    OK ) const noexcept {
      using namespace LHCb::SIMD;
      // First the basic checks
      OK &= ( ckTheta < m_maxCKthetaSIMD[rich] && ckTheta > m_minCKthetaSIMD[rich] );
      // Now check each hypo
      if ( any_of( OK ) ) {
        auto hypoMask = !OK;
        for ( const auto hypo : activeParticlesNoBT() ) {
          hypoMask |= ( abs( ckTheta - tkCkAngles[hypo] ) < ( m_nSigmaSIMD[rich] * tkCkRes[hypo] ) );
          if ( all_of( hypoMask ) ) { break; }
        }
        // Update the mask
        OK &= hypoMask;
      }
    }

  protected:
    /// SIMD Square of m_maxROI
    DetectorArray<SIMDFP> m_maxROI2PreSelSIMD = { {} };

    /// SIMD Square of m_minROI
    DetectorArray<SIMDFP> m_minROI2PreSelSIMD = { {} };

    /// SIMD Internal cached parameter for speed
    DetectorArray<SIMDFP> m_scalePreSelSIMD = { {} };

    /// SIMD N sigma for acceptance bands for preselection
    DetectorArray<SIMDFP> m_nSigmaPreSelSIMD = { {} };

    /// SIMD Absolute minimum allowed Cherenkov Angle
    DetectorArray<SIMDFP> m_minCKthetaSIMD = { {} };

    /// SIMD Absolute maximum allowed Cherenkov Angle
    DetectorArray<SIMDFP> m_maxCKthetaSIMD = { {} };

    /// SIMD N sigma for acceptance bands
    DetectorArray<SIMDFP> m_nSigmaSIMD = { {} };

    /// SIMD CK theta correction factors
    DetectorArray<SIMDFP> m_ckThetaCorrSIMD = { {} };

  protected:
    // Pre-sel options

    /// Min hit radius of interest around track centres for preselection
    Gaudi::Property<DetectorArray<float>> m_minROIPreSel{ this, "PreSelMinTrackROI", { 0, 0 } };

    /// Max hit radius of interest around track centres for preselection
    Gaudi::Property<DetectorArray<float>> m_maxROIPreSel{ this, "PreSelMaxTrackROI", { 110, 165 } };

    /// N sigma for acceptance bands for preselection
    Gaudi::Property<DetectorArray<float>> m_nSigmaPreSel{ this, "PreSelNSigma", { 5, 24 } };

    // cache values for speed

    /// Square of m_maxROI
    DetectorArray<float> m_maxROI2PreSel{ 0.0, 0.0 };

    /// Square of m_minROI
    DetectorArray<float> m_minROI2PreSel{ 0.0, 0.0 };

    /// Internal cached parameter for speed
    DetectorArray<float> m_scalePreSel{ 0.0, 0.0 };

  protected:
    // reco options

    /// Check for photons that cross between the different RICH 'sides'
    // Gaudi::Property<DetectorArray<bool>> m_checkPhotCrossSides{this, "CheckSideCrossing", {false, false}};

    /// Absolute minimum allowed Cherenkov Angle
    Gaudi::Property<DetectorArray<float>> m_minCKtheta{
        this,
        "MinAllowedCherenkovTheta",
        { 0.005, 0.005 },
        "The minimum allowed CK theta values for each RICH (R1Gas/R2Gas)" };

    /// Absolute maximum allowed Cherenkov Angle
    Gaudi::Property<DetectorArray<float>> m_maxCKtheta{
        this,
        "MaxAllowedCherenkovTheta",
        { 0.055, 0.032 },
        "The maximum allowed CK theta values for each RICH (R1Gas/R2Gas)" };

    /// N sigma for acceptance bands
    Gaudi::Property<DetectorArray<float>> m_nSigma{
        this, "NSigma", { 3.6, 4.5 }, "The CK theta # sigma selection range for each RICH (R1Gas/R2Gas)" };

    /** Cherenkov theta bias corrections, specific for each photon
     *  reconstruction method. */
    DetectorArray<float> m_ckBiasCorrs{ 0.0, 0.0 };

  private:
    /** Job-Option Corrections applied to the reconstructed theta vales.
     *  By default 0. */
    Gaudi::Property<DetectorArray<float>> m_ckJOCorrs{ this, "CKThetaQuartzRefractCorrections", { 0.0, 0.0 } };

    // Parameters for the scale factor calculations

    // The CK theta value
    Gaudi::Property<DetectorArray<float>> m_ckThetaScale{ this, "ScaleFactorCKTheta", { 0.045, 0.024 } };

    // The seperation the scale factors apply to
    Gaudi::Property<DetectorArray<float>> m_sepGScale{ this, "ScaleFactorSepG", { 83, 107 } };
  };

} // namespace Rich::Future::Rec
