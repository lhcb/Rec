/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichCommonQuarticPhotonReco.h"

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode CommonQuarticPhotonReco::initialize() {

  // Sets up various tools and services
  auto sc = BasePhotonReco::initialize();
  if ( !sc ) { return sc; }

  // loop over RICHes
  for ( const auto rich : activeDetectors() ) {

    // If rejection of ambiguous photons is turned on make sure test is turned on
    if ( m_rejectAmbigPhots[rich] && !m_testForUnambigPhots[rich] ) {
      info() << "Unambigous photon check will be enabled in order to reject ambiguous photons" << endmsg;
      m_testForUnambigPhots[rich] = true;
    }

    // information printout about configuration
    if ( m_testForUnambigPhots[rich] ) {
      _ri_debug << "Will test for unambiguous     " << rich << " photons" << endmsg;
    } else {
      _ri_debug << "Will not test for unambiguous " << rich << " photons" << endmsg;
    }

    if ( m_rejectAmbigPhots[rich] ) {
      _ri_debug << "Will reject ambiguous " << rich << " photons" << endmsg;
    } else {
      _ri_debug << "Will accept ambiguous " << rich << " photons" << endmsg;
    }

    if ( m_useAlignedMirrSegs[rich] ) {
      _ri_debug << "Will use fully alligned mirror segments for " << rich << " reconstruction" << endmsg;
    } else {
      _ri_debug << "Will use nominal mirrors for " << rich << " reconstruction" << endmsg;
    }

    _ri_debug << "Minimum active " << rich << " segment fraction = " << m_minActiveFrac[rich] << endmsg;
  }

  _ri_debug << "Max # iterations (R1Gas/R2Gas) = " << m_nMaxQits << endmsg;

  // return
  return sc;
}

//=============================================================================
