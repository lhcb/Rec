/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Array properties
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichBasePhotonReco.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Rec Utils
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"

namespace Rich::Future::Rec {

  namespace {
    /// Shortcut to the output data type
    using OutData = std::tuple<SIMDCherenkovPhoton::Vector, Relations::PhotonToParents::Vector>;
  } // namespace

  /** @class CKEstiFromRadiusPhotonReco RichCKEstiFromRadiusPhotonReco.h
   *
   *  Reconstructs photon candidates using the Quartic solution.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class SIMDCKEstiFromRadiusPhotonReco final
      : public LHCb::Algorithm::MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&,  //
                                                          const CherenkovAngles::Vector&,         //
                                                          const CherenkovResolutions::Vector&,    //
                                                          const SegmentPanelSpacePoints::Vector&, //
                                                          const MassHypoRingsVector&,             //
                                                          const SegmentPhotonFlags::Vector&,      //
                                                          const SIMDPixelSummaries&,              //
                                                          const Relations::TrackToSegments::Vector& ),
                                                 Gaudi::Functional::Traits::BaseClass_t<BasePhotonReco>> {

  private:
    /// Basic precision (float)
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD version of FP
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

  public:
    /// Standard constructor
    SIMDCKEstiFromRadiusPhotonReco( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // inputs
                            { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                              KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Emitted },
                              KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                              KeyValue{ "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal },
                              KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted },
                              KeyValue{ "SegmentPhotonFlagsLocation", SegmentPhotonFlagsLocation::Default },
                              KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                              KeyValue{ "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected } },
                            // outputs
                            { KeyValue{ "CherenkovPhotonLocation", CherenkovPhotonLocation::Default },
                              KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default } } ) {
      // Corrections for the intrinsic biases
      //                Rich1Gas  Rich2Gas
      m_ckBiasCorrs = { -6.687e-5, 1.787e-6 };
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    OutData operator()( const LHCb::RichTrackSegment::Vector&     segments,      ///< segments
                        const CherenkovAngles::Vector&            ckAngles,      ///< CK theta angles
                        const CherenkovResolutions::Vector&       ckResolutions, ///< track CK resolutions
                        const SegmentPanelSpacePoints::Vector&    trHitPntsLoc,  ///< segment panel hit points
                        const MassHypoRingsVector&                massRings,     ///< CK mass rings
                        const SegmentPhotonFlags::Vector&         segPhotFlags,  ///< segment flags
                        const SIMDPixelSummaries&                 pixels,        ///< hit pisxels
                        const Relations::TrackToSegments::Vector& tkToSegRels    ///< track -> segment relations
    ) const override;

  private:
    /// Flag to turn on interpolation between two nearest rings, by RICH
    Gaudi::Property<DetectorArray<bool>> m_useRingInterp{ this, "UseRingInterpolation", { true, true } };

    /// Flag to turn on the rejection of 'ambiguous' photons
    Gaudi::Property<bool> m_rejAmbigPhots{ this, "RejectAmbiguousPhotons", false };
  };

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

//=============================================================================

OutData SIMDCKEstiFromRadiusPhotonReco::operator()( const LHCb::RichTrackSegment::Vector&     segments,      //
                                                    const CherenkovAngles::Vector&            ckAngles,      //
                                                    const CherenkovResolutions::Vector&       ckResolutions, //
                                                    const SegmentPanelSpacePoints::Vector&    trHitPntsLoc,  //
                                                    const MassHypoRingsVector&                massRings,     //
                                                    const SegmentPhotonFlags::Vector&         segPhotFlags,  //
                                                    const SIMDPixelSummaries&                 pixels,        //
                                                    const Relations::TrackToSegments::Vector& tkToSegRels ) const {

  using namespace LHCb::SIMD;

  // make the output data
  OutData outData;

  // Shortcut to the photons and relations
  auto& [photons, relations] = outData;

  // guess at reserve size
  const auto resSize = segments.size() * pixels.size() / 8;
  photons.reserve( resSize );
  relations.reserve( resSize );

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector<SIMDFP> corrector;

  // global photon index
  int photonIndex( -1 );

  // SIMD PI
  const SIMDFP simdpi( Gaudi::Units::pi );

  // Loop over the track->segment relations
  for ( const auto& inTkRel : tkToSegRels ) {
    // loop over segments for this track
    for ( const auto& segIndex : inTkRel.segmentIndices ) {
      // Get the data from the zipped container
      const auto& segment    = segments[segIndex];
      const auto& tkCkAngles = ckAngles[segIndex];
      const auto& tkCkRes    = ckResolutions[segIndex];
      const auto& tkLocPtn   = trHitPntsLoc[segIndex];
      const auto& tkRings    = massRings[segIndex];
      const auto& segFlags   = segPhotFlags[segIndex];

      // Is this segment above threshold
      // if ( ! ( tkCkAngles[lightestActiveHypo()] > 0 ) ) continue;

      // which RICH and radiator
      const auto rich = segment.rich();
      const auto rad  = segment.radiator();

      // This implementiation does not support Aerogel
      // if ( Rich::Aerogel == rad ) { Exception("Aerogel not supported"); }

      // cache SIMD version of segment point for each side
      const PanelArray<SIMDPixel::Point> segSidePtns{ { SIMDPixel::Point( tkLocPtn.point( Rich::top ) ), // also R2 left
                                                        SIMDPixel::Point( tkLocPtn.point( Rich::bottom ) ) } }; // also
                                                                                                                // R2
                                                                                                                // right

      //_ri_verbo << endmsg;
      //_ri_verbo << "Segment P=" << segment.bestMomentum() << " " << rich << " " << rad << endmsg;

      // get the best pixel range for this segment, based on where hits are expected
      const auto pixR =
          ( segFlags.inBothPanels() ? pixels.range( rich )
            : Rich::Rich1 == rich   ? ( segFlags.inPanel( Rich::top ) ? pixels.range( Rich::Rich1, Rich::top )
                                                                      : pixels.range( Rich::Rich1, Rich::bottom ) )
                                    : ( segFlags.inPanel( Rich::left ) ? pixels.range( Rich::Rich2, Rich::left )
                                                                       : pixels.range( Rich::Rich2, Rich::right ) ) );

      // SIMD Pixel index in container (start index - 1) for this range
      int pixIndex = pixR.begin() - pixels.begin() - 1;

      // loop over selected pixels
      for ( const auto& pix : pixR ) {

        // increment the pixel index
        ++pixIndex;

        //_ri_verbo << endmsg;
        //_ri_verbo << " -> Pixel IDs    " << pix.smartID() << endmsg;
        //_ri_verbo << " ->       GloPos " << pix.gloPos() << endmsg;
        //_ri_verbo << "          LocPos " << pix.locPos() << endmsg;
        //_ri_verbo << "          Mask   " << pix.validMask() << endmsg;

        // Pixel position, in local HPD coords corrected for average radiator distortion
        // Might need to pre-compute and cache this as we repeat it here each segment...
        const auto pixPRad = corrector.correct( pix.locPos(), rad );

        // side for this pack of pixels
        const auto side = pix.side();

        // SIMD Track local hit point on the same panel as the hit
        const auto& segPSide = segSidePtns[side];
        // Scalar Track local hit point on the same panel as the hit (for loop later on...)
        const auto& segPSide_sc = tkLocPtn.point( side );

        // x,y differences
        const auto diff_x = ( segPSide.x() - pixPRad.x() );
        const auto diff_y = ( segPSide.y() - pixPRad.y() );

        // compute the seperation squared
        const auto sep2 = ( ( diff_x * diff_x ) + ( diff_y * diff_y ) );

        // Start mask off as the pixel mask
        auto pixmask = pix.validMask();

        // Check overall boundaries
        pixmask &= ( sep2 < m_maxROI2PreSelSIMD[rich] && sep2 > m_minROI2PreSelSIMD[rich] );
        //_ri_verbo << "  -> sep2 = " << sep2 << " OK=" << pixmask << endmsg;
        if ( any_of( pixmask ) ) {
          // estimated CK theta
          const auto ckThetaEsti = std::sqrt( sep2 ) * m_scalePreSelSIMD[rich];
          // Is any hit close to any mass hypo in local coordinate space ?
          // only need confirmation for those already selected so start with !pixmask
          auto hypoMask = !pixmask;
          for ( const auto hypo : activeParticlesNoBT() ) {
            const SIMDPixel::SIMDFP angs( tkCkAngles[hypo] ); // maybe should cache this
            const SIMDPixel::SIMDFP reso( tkCkRes[hypo] );    // maybe should cache this
            hypoMask |= ( abs( angs - ckThetaEsti ) < ( m_nSigmaPreSelSIMD[rich] * reso ) );
            if ( all_of( hypoMask ) ) break;
          }
          pixmask &= hypoMask;
        }

        // Did any hit pass the pre-sel
        //_ri_verbo << "  -> PreSel=" << pixmask << endmsg;
        if ( none_of( pixmask ) ) { continue; }

        // make a photon object to work on
        photons.emplace_back( rich, pix.smartID() );
        auto& gPhoton = photons.back();
        // Use a guard to remove the last added photon unless explicitly saved
        DataGuard<decltype( photons )> photGuard( photons );

        // fraction of segment path length accessible to the photon
        // cannot determine this here so set to 1
        const SIMDFP fraction{ SIMDFP::One() };

        // The track - pixel seperation
        const auto track_pix_sep = std::sqrt( sep2 );

        // estimate phi from these hits
        const auto phiCerenkov = ( simdpi + LHCb::Math::fast_atan2( diff_y, diff_x ) );

        // cherenkov theta to fill
        SIMDFP thetaCerenkov( -SIMDFP::One() );

        // Ambiguous photons
        SIMDFP::mask_type unambigPhoton( false ), last_unambigPhoton( false );

        // resort to scalar loop... to do to minimise this..
        for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
          // skip failed combinations
          if ( !pixmask[i] ) continue;

          // make a scalar pixel point...
          const Gaudi::XYZPoint pixPRad_sc{ pixPRad.x()[i], pixPRad.y()[i], pixPRad.z()[i] };

          // Find the best ring and points to use as the calibration data
          const RayTracedCKRingPoint::Vector* ring{ nullptr };
          // const RayTracedCKRingPoint *best_point{nullptr};
          Rich::ParticleIDType ring_pid{ Rich::Unknown }, last_ring_pid{ Rich::Unknown };
          float                sep_diff2 = std::numeric_limits<float>::max();
          float                sep_calib{ 0 }, last_sep_calib{ 0 };
          bool                 pointOutsideLargestRing{ false };
          for ( const auto pid : activeParticlesNoBT() ) {

            // Load the ring and select the point for this PID type
            const auto& ring_tmp = tkRings[pid];

            // Did we find a ring (i.e. above threshold)
            if ( !ring_tmp.empty() ) {
              // check how close to threshold the ring is. Do not use if too close.
              // if ( tkCkAngles[pid] > minCalibRingRadius(radiator) ) // todo
              if ( tkCkAngles[pid] > 0 ) {

                // select the calibration point to use from this ring
                const auto points = getPointsClosestInAzimuth( ring_tmp, phiCerenkov[i] );

                // if found see if this point is better than the last one tried
                if ( points.first && points.second && sameSide( rad, pixPRad_sc, points.first->localPosition() ) &&
                     sameSide( rad, pixPRad_sc, points.second->localPosition() ) ) {
                  // get distance to each calibration point in phi
                  const auto lD   = fabs( points.first->azimuth() - phiCerenkov[i] );
                  const auto hD   = fabs( points.second->azimuth() - phiCerenkov[i] );
                  const auto Dinv = 1.0 / ( lD + hD );

                  // corrected interpolated local calibration point position (x,y)
                  const auto& lP     = points.first->localPosition();
                  const auto& hP     = points.second->localPosition();
                  const auto  calp_x = ( ( lP.x() * hD ) + ( hP.x() * lD ) ) * Dinv;
                  const auto  calp_y = ( ( lP.y() * hD ) + ( hP.y() * lD ) ) * Dinv;

                  // pixel - calibration point seperation ^ 2
                  const auto pix_calib_sep2 =
                      ( std::pow( pixPRad_sc.x() - calp_x, 2 ) + std::pow( pixPRad_sc.y() - calp_y, 2 ) );
                  const bool calibIsCloser = ( pix_calib_sep2 < sep_diff2 );

                  // Is this point a better calibration point to use ?
                  if ( m_useRingInterp[rich] || calibIsCloser ) {
                    // update decision variable
                    sep_diff2 = pix_calib_sep2;

                    // First ring found ?
                    const bool firstFoundRing = ( ring == nullptr );

                    // Update best point pointer
                    // best_point = ( lD < hD ? points.first : points.second );

                    // Update calibration distance
                    last_sep_calib = sep_calib;
                    sep_calib =
                        std::sqrt( std::pow( segPSide_sc.x() - calp_x, 2 ) + std::pow( segPSide_sc.y() - calp_y, 2 ) );

                    // Update best ring and pixel pointers
                    last_ring_pid = ring_pid;
                    ring          = &ring_tmp;
                    ring_pid      = pid;

                    // Ambiguous photon test, based on the mirrors used by both calibration points.
                    // Not quite the same as what is done in the Quartic tool (which tests
                    // reconstruction from the start and end of the track segment) but its better
                    // than doing nothing.
                    last_unambigPhoton[i] =
                        (unsigned int)unambigPhoton[i]; // cast is hack for ve. To be removed once fixed in ve
                    unambigPhoton[i] = ( ( points.first->primaryMirror() == points.second->primaryMirror() ) &&
                                         ( points.first->secondaryMirror() == points.second->secondaryMirror() ) );

                    // Is the data point outside the first ring ?
                    if ( firstFoundRing ) { pointOutsideLargestRing = track_pix_sep[i] > sep_calib; }

                    // If interpolation is enabled, and we are between rings, use them
                    if ( m_useRingInterp[rich] && !firstFoundRing && track_pix_sep[i] >= sep_calib &&
                         track_pix_sep[i] < last_sep_calib ) {
                      // Interpolate between the two rings, then break out as this is the best we
                      // can do
                      thetaCerenkov[i] = ( ( ( tkCkAngles[ring_pid] * ( track_pix_sep[i] - last_sep_calib ) ) +
                                             ( tkCkAngles[last_ring_pid] * ( sep_calib - track_pix_sep[i] ) ) ) /
                                           ( sep_calib - last_sep_calib ) );
                      break;
                    } else {
                      // Update CK theta value just using this ring
                      thetaCerenkov[i] = tkCkAngles[ring_pid] * ( track_pix_sep[i] / sep_calib );
                      // If this is the first found ring, and the point is further away than
                      // the calibration point, do not bother searching other (smaller) rings
                      // as they are only going to get further away.
                      if ( firstFoundRing && pointOutsideLargestRing ) { break; }
                    }
                  } else if ( ring ) {
                    // a ring is already found, and we are getting further away, so break out ...
                    break;
                  }

                } // point found

              } // ring saturation check
            } else {
              // no ring was found. This means we are below threshold so there is no
              // point in searching other heavier rings, so break out.
              break;
            }

          } // loop over PID types

          // set mask for photon if ring was found
          pixmask[i] &= decltype( pixmask )::value_type( ring != nullptr );

        } // scalar loop

        // if any are good, make photon
        if ( any_of( pixmask ) ) {

          // check for ambiguous photons ?
          const auto unambig = unambigPhoton && last_unambigPhoton;
          if ( m_rejAmbigPhots ) { pixmask &= unambig; }
          if ( !m_rejAmbigPhots || any_of( pixmask ) ) {

            // Add bias correction to CK theta value
            thetaCerenkov += m_ckThetaCorrSIMD[rich];

            // --------------------------------------------------------------------------------------
            // Final checks on the Cherenkov theta angles
            // --------------------------------------------------------------------------------------
            checkAngle( rich, tkCkAngles, tkCkRes, thetaCerenkov, pixmask );
            if ( any_of( pixmask ) ) {

              // ------------------------------------------------------------------------
              // Set (remaining) photon parameters
              // ------------------------------------------------------------------------
              gPhoton.setCherenkovTheta( thetaCerenkov );
              gPhoton.setCherenkovPhi( phiCerenkov );
              gPhoton.setActiveSegmentFraction( fraction );
              // gPhoton.setUnambiguousPhoton( unambig );
              gPhoton.setValidityMask( pixmask );
              //_ri_verbo << "Created photon " << gPhoton << endmsg;
              // ------------------------------------------------------------------------

              // if get here keep the photon
              photGuard.setOK();
              // Save relations
              relations.emplace_back( ++photonIndex, pixIndex, segIndex, inTkRel.tkIndex );
              //_ri_verbo << relations.back() << endmsg;
            }
          }
        }

      } // pixel loop

    } // segment loop

  } // track loop

  // return
  return outData;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDCKEstiFromRadiusPhotonReco )

//=============================================================================
