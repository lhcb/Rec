/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Base class
#include "RichCommonQuarticPhotonReco.h"

// Gaudi
#include "LHCbAlgs/Transformer.h"

// Utils
#include "RichFutureUtils/RichMirrorFinder.h"
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichUtils/RichRayTracingUtils.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"

// STL
#include <cassert>
#include <cmath>
#include <iterator>
#include <string>
#include <tuple>

namespace Rich::Future::Rec {

  namespace {
    /// Shortcut to the output data type
    using OutData = std::tuple<SIMDCherenkovPhoton::Vector,        ///< Photon objects
                               Relations::PhotonToParents::Vector, ///< Relations data to tracks, segments
                               SIMDMirrorData::Vector              ///< (Optionally filled) mirror data
                               >;
  } // namespace

  /** @class SIMDQuarticPhotonReco
   *
   *  Reconstructs SIMD photon candidates using the Quartic solution.
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDQuarticPhotonReco final : public LHCb::Algorithm::MultiTransformer<
                                          OutData( const LHCb::RichTrackSegment::Vector&,     //
                                                   const CherenkovAngles::Vector&,            //
                                                   const CherenkovResolutions::Vector&,       //
                                                   const SegmentPanelSpacePoints::Vector&,    //
                                                   const SegmentPhotonFlags::Vector&,         //
                                                   const SIMDPixelSummaries&,                 //
                                                   const Relations::TrackToSegments::Vector&, //
                                                   const Rich::Detector::Rich1&,              //
                                                   const Rich::Detector::Rich2&,              //
                                                   const Rich::Utils::MirrorFinder&,          //
                                                   const Utils::TabulatedRefIndex& ),
                                          LHCb::Algorithm::Traits::usesBaseAndConditions<CommonQuarticPhotonReco,   //
                                                                                         Detector::Rich1,           //
                                                                                         Detector::Rich2,           //
                                                                                         Rich::Utils::MirrorFinder, //
                                                                                         Utils::TabulatedRefIndex>> {

  public:
    // definitions

    using FP      = BasePhotonReco::FP;      ///< Basic precision (float)
    using SIMDFP  = BasePhotonReco::SIMDFP;  ///< SIMD version of FP
    using Mirrors = BasePhotonReco::Mirrors; ///< Type for container of mirror pointers. Same size as SIMDFP.

  public:
    // framework

    /// Standard constructor
    SIMDQuarticPhotonReco( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // Input data
                            { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                              KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                              KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default },
                              KeyValue{ "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal },
                              KeyValue{ "SegmentPhotonFlagsLocation", SegmentPhotonFlagsLocation::Default },
                              KeyValue{ "RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default },
                              KeyValue{ "TrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected },
                              // conditions input
                              KeyValue{ "Rich1", Detector::Rich1::DefaultConditionKey },
                              KeyValue{ "Rich2", Detector::Rich2::DefaultConditionKey },
                              KeyValue{ "RichMirrorFinder", Rich::Utils::MirrorFinder::DefaultConditionKey },
                              KeyValue{ "TabulatedRefIndex", Utils::TabulatedRefIndex::DefaultConditionKey } },
                            // Output data
                            { KeyValue{ "CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default },
                              KeyValue{ "PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default },
                              KeyValue{ "PhotonMirrorDataLocation", SIMDMirrorDataLocation::Default } } ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialization after creation
    StatusCode initialize() override {

      // Sets up various tools and services
      auto sc = MultiTransformer::initialize();
      if ( !sc ) return sc;

      // loop over RICHes
      for ( const auto rich : activeDetectors() ) {
        if ( m_enable4D[rich] ) { info() << "4D photons enabled for " << rich << endmsg; }
        // Local SIMD copies of various base class properties
        m_minActiveFracSIMD[rich]   = SIMDFP( m_minActiveFrac[rich] );
        m_minSphMirrTolItSIMD[rich] = SIMDFP( m_minSphMirrTolIt[rich] );
        // Do not support 0 iterations here...
        if ( m_nMaxQits[rich] <= 0 ) {
          error() << "Iterations must be >0" << endmsg;
          return StatusCode::FAILURE;
        }
      }

      // derived condition objects
      Detector::Rich1::addConditionDerivation( this );
      Detector::Rich2::addConditionDerivation( this );
      Utils::MirrorFinder::addConditionDerivation( this );
      Utils::TabulatedRefIndex::addConditionDerivation( this );

      // return
      return sc;
    }

  public:
    // methods

    /// Functional operator
    OutData operator()( const LHCb::RichTrackSegment::Vector&     segments,      ///< segments
                        const CherenkovAngles::Vector&            ckAngles,      ///< CK theta angles
                        const CherenkovResolutions::Vector&       ckResolutions, ///< CK theta resolutions
                        const SegmentPanelSpacePoints::Vector&    trHitPntsLoc,  ///< segment hit points
                        const SegmentPhotonFlags::Vector&         segPhotFlags,  ///< segment flags
                        const SIMDPixelSummaries&                 pixels,        ///< RICH SIMD hits
                        const Relations::TrackToSegments::Vector& tkToSegRels,   ///< track relations
                        const Rich::Detector::Rich1&              deRich1,       ///< RICH1 detector element
                        const Rich::Detector::Rich2&              deRich2,       ///< RICH2 detector element
                        const Rich::Utils::MirrorFinder&          mirrorFinder,  ///< Mirror finder
                        const Utils::TabulatedRefIndex&           tabRefIndx     ///< Refractive index
    ) const override;

  private:
    // methods

    /// Compute the best emission point for the gas radiators using
    /// the given spherical mirror reflection points
    SIMDFP::mask_type getBestGasEmissionPoint( const Rich::RadiatorType radiator,       ///< RICH radiator
                                               const SIMD::Point<FP>&   sphReflPoint1,  ///< first spherical refl point
                                               const SIMD::Point<FP>&   sphReflPoint2,  ///< second spherical refl point
                                               const SIMD::Point<FP>&   detectionPoint, ///< detection point
                                               const LHCb::RichTrackSegment& segment,   ///< track segment
                                               SIMD::Point<FP>&              emissionPoint, ///< assumed emission point
                                               SIMDFP&                       fraction       ///< active track fraction
    ) const;

    /// Find mirror segments and reflection points for given data
    void findMirrorData( const Rich::Detector::RichBase& deRich, ///< The RICH detector element
                         const Rich::Side                side,   ///< The RICH panel
                         const SIMD::Point<FP>& virtDetPoint,    ///< virtual detection point (flat mirror removed)
                         const SIMD::Point<FP>& emissionPoint,   ///< assumed emission point
                         Mirrors&               primMirr,        ///< primary mirrors
                         Mirrors&               secMirr,         ///< secondary mirrors
                         SIMD::Point<FP>&       sphReflPoint,    ///< primary mirror reflection point
                         SIMD::Point<FP>&       secReflPoint,    ///< secondary mirror reflection point
                         const Rich::Utils::MirrorFinder& mirrorFinder ///< Mirror finder
    ) const;

  private:
    // SIMD cache of various quantities

    /// SIMD Minimum active segment fraction in each radiator
    alignas( LHCb::SIMD::VectorAlignment ) DetectorArray<SIMDFP> m_minActiveFracSIMD = { {} };

    /// SIMD Minimum tolerance for mirror reflection point during iterations
    alignas( LHCb::SIMD::VectorAlignment ) DetectorArray<SIMDFP> m_minSphMirrTolItSIMD = { {} };

  private:
    // properties

    /// Save optional mirror data
    Gaudi::Property<bool> m_saveMirrorData{ this, "SaveMirrorData", false };

    Gaudi::Property<DetectorArray<float>> m_pdTransitTime{
        this, "PDTransitTime", { 0.04605 * Gaudi::Units::ns, 0.04605 * Gaudi::Units::ns } };
  };

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future;
using namespace Rich::Future::Rec;

//=============================================================================

namespace {
  // constants
  alignas( LHCb::SIMD::VectorAlignment ) const SIMDQuarticPhotonReco::SIMDFP HALF( 0.5f ), TWO( 2.0f );
  alignas( LHCb::SIMD::VectorAlignment ) const SIMDQuarticPhotonReco::SIMDFP::mask_type FALSE( false );
} // namespace

//=============================================================================

OutData                                                                                     //
SIMDQuarticPhotonReco::operator()( const LHCb::RichTrackSegment::Vector&     segments,      // segments
                                   const CherenkovAngles::Vector&            ckAngles,      // CK theta angles
                                   const CherenkovResolutions::Vector&       ckResolutions, // CK theta resolutions
                                   const SegmentPanelSpacePoints::Vector&    trHitPntsLoc,  // segment hit points
                                   const SegmentPhotonFlags::Vector&         segPhotFlags,  // segment flags
                                   const SIMDPixelSummaries&                 pixels,        // RICH SIMD hits
                                   const Relations::TrackToSegments::Vector& tkToSegRels,   // track relations
                                   const Rich::Detector::Rich1&              deRich1,       // RICH1 detector element
                                   const Rich::Detector::Rich2&              deRich2,       // RICH2 detector element
                                   const Rich::Utils::MirrorFinder&          mirrorFinder,  // Mirror finder
                                   const Utils::TabulatedRefIndex&           tabRefIndx     // ref index
) const {

  using namespace LHCb::SIMD;

  // make the output data
  OutData outData;

  // Shortcut to the photons, relations and mirror data
  auto& [photons, relations, mirrorData] = outData;

  // guess at reserve size. Better to be a bit to big than too small...
  const auto resSize = ( segments.size() * pixels.size() ) / 8;
  photons.reserve( resSize );
  relations.reserve( resSize );
  // only need to reserve mirror data vector if it is to be filled.
  if ( m_saveMirrorData ) { mirrorData.reserve( resSize ); }

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector<SIMDFP> corrector;

  // best secondary and spherical mirror data caches
  Rich::SIMD::MirrorData sphMirrData, secMirrData;
  Mirrors                sphMirrors{ {} }, secMirrors{ {} };

  // array for RICH det elems
  const DeRiches deRiches = { &deRich1, &deRich2 };

  // global photon index
  Relations::PhotonToParents::PhotonIndex photonIndex( 0 );

  // Loop over the track->segment relations
  for ( const auto& inTkRel : tkToSegRels ) {

    // loop over segments for this track
    for ( const auto& segIndex : inTkRel.segmentIndices ) {

      // Get the data from the segment containers
      const auto& segment    = segments[segIndex];
      const auto& tkCkAngles = ckAngles[segIndex];
      const auto& tkCkRes    = ckResolutions[segIndex];
      const auto& tkLocPtn   = trHitPntsLoc[segIndex];
      const auto& segFlags   = segPhotFlags[segIndex];

      // which RICH and radiator
      const auto rich = segment.rich();
      const auto rad  = segment.radiator();

      // cache segment CK angles in SIMD form
      const Rich::Future::HypoData<SIMDPixel::SIMDFP> simd_tkCkAngles( tkCkAngles );

      // cache SIMD version of expected track resolutions
      const Rich::Future::HypoData<SIMDPixel::SIMDFP> simd_tkCkRes( tkCkRes );
      // ... scaled by pre-sel #sigma parameter
      const auto simd_presel_scaled_tkCkRes = simd_tkCkRes * m_nSigmaPreSelSIMD[rich];

      // Cache the ambiguous photon check start points outside pixel loop
      // First emission point, at start of track segment
      const SIMDPixel::Point emissionPoint1( m_testForUnambigPhots[rich] ? segment.bestPoint( 0.01 )
                                                                         : segment.bestPoint() );
      // now do it again for emission point #2, at end of segment
      const SIMDPixel::Point emissionPoint2( m_testForUnambigPhots[rich] ? segment.bestPoint( 0.99 )
                                                                         : segment.bestPoint() );

      // cache SIMD version of segment point for each side
      const PanelArray<SIMDPixel::Point>                                         //
          segSidePtns{ { SIMDPixel::Point( tkLocPtn.point( Rich::top ) ),        // also R2 left
                         SIMDPixel::Point( tkLocPtn.point( Rich::bottom ) ) } }; // also R2 right

      // Obtain the refractive index to compute the photon speed
      const SIMDFP refIndex( m_enable4D[rich]
                                 ? tabRefIndx.refractiveIndex( segment.radIntersections(), segment.avPhotonEnergy() )
                                 : SIMDFP::One() );
      // ... cache the photon velocity in the radiator medium based on ref index
      const SIMDFP photonVelocity( m_enable4D[rich] ? Gaudi::Units::c_light / refIndex[0] : 0.0 );

      //_ri_verbo << endmsg;
      //_ri_verbo << "Segment P=" << segment.bestMomentum() << " " << rich << " " << rad << endmsg;

      // get the best pixel range for this segment, based on where hits are expected
      const auto pixR =
          ( segFlags.inBothPanels() ? pixels.range( rich )
            : Rich::Rich1 == rich   ? ( segFlags.inPanel( Rich::top ) ? pixels.range( Rich::Rich1, Rich::top )
                                                                      : pixels.range( Rich::Rich1, Rich::bottom ) )
                                    : ( segFlags.inPanel( Rich::left ) ? pixels.range( Rich::Rich2, Rich::left )
                                                                       : pixels.range( Rich::Rich2, Rich::right ) ) );

      // SIMD Pixel index in container (start index) for this range
      Relations::PhotonToParents::PixelIndex pixIndex = std::distance( pixels.begin(), pixR.begin() );

      // loop over selected pixels
      for ( auto ipix = pixR.begin(); ipix != pixR.end(); ++ipix, ++pixIndex ) {

        // reference to pixel
        const auto& pix = *ipix;

        //_ri_verbo << endmsg;
        //_ri_verbo << " -> Pixel IDs    " << pix.smartID() << endmsg;
        //_ri_verbo << " ->       GloPos " << pix.gloPos() << endmsg;
        //_ri_verbo << "          LocPos " << pix.locPos() << endmsg;
        //_ri_verbo << "          Mask   " << pix.validMask() << endmsg;

        // side for this pack of pixels
        const auto side = pix.side();

        // Pixel position, in local PD panel coords corrected for average radiator distortion
        // Might need to pre-compute and cache this as we repeat it here each segment...
        const auto pixP = corrector.correct( pix.locPos(), rad );

        // Track local hit point on the same panel as the hit
        const auto& segPanelPnt = segSidePtns[side];

        // compute the seperation squared
        const auto dx   = pixP.x() - segPanelPnt.x();
        const auto dy   = pixP.y() - segPanelPnt.y();
        const auto sep2 = ( ( dx * dx ) + ( dy * dy ) );

        // Start mask off as the pixel mask
        auto pixmask = pix.validMask();

        // Check overall boundaries
        pixmask &= ( sep2 < m_maxROI2PreSelSIMD[rich] && sep2 > m_minROI2PreSelSIMD[rich] );
        //_ri_verbo << "  -> sep2 = " << sep2 << " OK=" << pixmask << endmsg;
        if ( any_of( pixmask ) ) {

          // estimated CK theta
          const auto ckThetaEsti = std::sqrt( sep2 ) * m_scalePreSelSIMD[rich];
          //_ri_verbo << "   -> CK theta Esti " << ckThetaEsti << endmsg;

          // Is any hit close to any mass hypo in local coordinate space ?
          // only need confirmation for those already selected so start with !pixmask
          auto hypoMask = !pixmask;
          for ( const auto hypo : activeParticlesNoBT() ) {
            hypoMask |= ( abs( simd_tkCkAngles[hypo] - ckThetaEsti ) < simd_presel_scaled_tkCkRes[hypo] );
            if ( all_of( hypoMask ) ) { break; }
          }
          pixmask &= hypoMask;

          // Did any hit pass the pre-sel
          //_ri_verbo << "  -> PreSel=" << pixmask << endmsg;
          if ( none_of( pixmask ) ) { continue; }

        } else {
          // All scalars failed, so skip to next
          continue;
        }

        // If get here, we have passed the pre-sel so proceed with full reconstruction

        // Emission point to use for photon reconstruction
        SIMD::Point<FP> emissionPoint{ segment.bestPoint() };

        // Final reflection points on sec and spherical mirrors
        SIMD::Point<FP> sphReflPoint, secReflPoint;

        // fraction of segment path length accessible to the photon
        auto fraction = SIMDFP::One();

        // flag to say if these photon candidates are un-ambiguous - default to false
        auto unambigPhoton = FALSE;

        // find the reflection of the detection point in the sec mirror
        // (virtual detection point) starting with nominal values
        // At this point we are assuming a flat nominal mirror common to all segments
        // Note rich and side are the same for all SIMD pixels by design
        const auto nom_dist     = deRiches[rich]->nominalPlaneSIMD( side ).Distance( pix.gloPos() );
        auto       virtDetPoint = pix.gloPos();
        virtDetPoint -= ( TWO * nom_dist * deRiches[rich]->nominalNormalSIMD( side ) );
        //_ri_verbo << "  -> virtDetPoint = " << virtDetPoint << endmsg;

        // --------------------------------------------------------------------------------------
        // For gas radiators, try start and end points to see if photon is unambiguous
        // NOTE : For this we are using the virtual detection point determined using
        // the noimnal flat secondary mirror plane. Now the secondary mirrors are actually
        // spherical this may be introducing some additional uncertainties.
        // --------------------------------------------------------------------------------------
        if ( m_testForUnambigPhots[rich] ) {

          // -------------------------------------------------------------------------------
          // Test reconstruction from segment start point
          // -------------------------------------------------------------------------------
          // Find mirror segments for this emission point
          SIMD::Point<FP> sphReflPoint1, secReflPoint1;
          findMirrorData( *deRiches[rich], side, virtDetPoint, emissionPoint1, //
                          sphMirrors, secMirrors, sphReflPoint1, secReflPoint1, mirrorFinder );
          //_ri_verbo << "  -> unambig1 " << pixmask << endmsg;
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Test reconstruction from segment end point
          // -------------------------------------------------------------------------------
          // Temporary mirror data objects
          Mirrors         sphMirrors2, secMirrors2;
          SIMD::Point<FP> sphReflPoint2, secReflPoint2;
          findMirrorData( *deRiches[rich], side, virtDetPoint, emissionPoint2, //
                          sphMirrors2, secMirrors2, sphReflPoint2, secReflPoint2, mirrorFinder );
          //_ri_verbo << "  -> unambig2 " << pixmask << endmsg;
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Get the best gas emission point
          // -------------------------------------------------------------------------------
          pixmask &= getBestGasEmissionPoint( rad,                          //
                                              sphReflPoint1, sphReflPoint2, //
                                              pix.gloPos(), segment,        //
                                              emissionPoint, fraction );
          //_ri_verbo << "  -> getBestGasEmissionPoint " << pixmask << endmsg;
          if ( none_of( pixmask ) ) {
            //_ri_debug << rad << " : Failed to compute best gas emission point" << endmsg;
            continue;
          }
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // Is this an unambiguous photon - I.e. only one possible mirror combination
          // -------------------------------------------------------------------------------
          GAUDI_LOOP_UNROLL( SIMDFP::Size )
          for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
            unambigPhoton[i] = ( ( sphMirrors[i] == sphMirrors2[i] ) && //
                                 ( secMirrors[i] == secMirrors2[i] ) );
          }
          if ( any_of( unambigPhoton ) ) {
            // rough guesses at reflection points (improved later on)
            sphReflPoint = sphReflPoint1 + HALF * ( sphReflPoint2 - sphReflPoint1 );
            secReflPoint = secReflPoint1 + HALF * ( secReflPoint2 - secReflPoint1 );
          }
          // -------------------------------------------------------------------------------

          // -------------------------------------------------------------------------------
          // if configured to do so reject ambiguous photons
          // -------------------------------------------------------------------------------
          if ( m_rejectAmbigPhots[rich] ) {
            pixmask &= unambigPhoton;
            //_ri_debug << rad << " : Failed ambiguous photon test" << endmsg;
            if ( none_of( pixmask ) ) { continue; }
          }
          // -------------------------------------------------------------------------------

          //_ri_verbo << "  -> end unambiguous photon check " << pixmask << endmsg;

        } // end unambiguous photon check
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Active segment fraction cut
        // --------------------------------------------------------------------------------------
        pixmask &= ( fraction >= m_minActiveFracSIMD[rich] );
        //_ri_verbo << "  -> active fraction " << pixmask << endmsg;
        if ( none_of( pixmask ) ) {
          //_ri_debug << rad << " : Failed active segment fraction cut" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // if ambiguous gas photon or if ambiguous photon check above has been skipped, try again
        // using best emission point and nominal mirror geometries to get the spherical and sec
        // mirrors.
        // --------------------------------------------------------------------------------------
        if ( !m_testForUnambigPhots[rich] || !all_of( unambigPhoton ) ) {
          findMirrorData( *deRiches[rich], side,       //
                          virtDetPoint, emissionPoint, //
                          sphMirrors, secMirrors,      //
                          sphReflPoint, secReflPoint, mirrorFinder );
          //_ri_verbo << "  -> retest ambig photon " << pixmask << endmsg;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // make sure the mirror caches are up to date at this point
        // --------------------------------------------------------------------------------------
        secMirrData.update( secMirrors );
        sphMirrData.update( sphMirrors );
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Finally reconstruct the photon using best emission point and the best mirror segments
        // --------------------------------------------------------------------------------------
        if ( m_useAlignedMirrSegs[rich] ) {

          // Iterate to final solution, improving the secondary mirror info
          int             iIt( 0 );
          SIMD::Point<FP> last_mirror_point( 0, 0, 0 );
          while ( iIt < m_nMaxQits[rich] ) {

            // Get secondary mirror reflection point,
            // using the best actual secondary mirror segment at this point
            const auto dir = virtDetPoint - sphReflPoint;
            if ( m_treatSecMirrsFlat[rich] ) {
              intersectPlane( sphReflPoint, dir, secMirrData.getNormalPlane(), secReflPoint );
            } else {
              pixmask &= intersectSpherical( sphReflPoint, dir,                            //
                                             secMirrData.getCoCs(), secMirrData.getRoCs(), //
                                             secReflPoint );
              //_ri_verbo << "  -> It" << iIt << " secMirrIntersect " << pixmask << endmsg;
              if ( none_of( pixmask ) ) { break; }
            }

            // (re)find the secondary mirror
            secMirrors = mirrorFinder.findSecMirror( rich, side, secReflPoint );
            // update the cache
            secMirrData.update( secMirrors );

            // Compute the virtual reflection point

            // Construct plane tangential to secondary mirror passing through reflection point
            // const Gaudi::Plane3D plane( secSegment->centreOfCurvature()-secReflPoint,
            // secReflPoint );
            // re-find the reflection of the detection point in the sec mirror
            // (virtual detection point) with this mirror plane
            // const auto distance     = plane.Distance(gloPos);
            // virtDetPoint = gloPos - 2.0 * distance * plane.Normal();

            // Same as above, just written out by hand and simplified a bit
            const auto normV    = secMirrData.getCoCs() - secReflPoint;
            const auto distance = ( ( normV.X() * ( pix.gloPos().X() - secReflPoint.X() ) ) +
                                    ( normV.Y() * ( pix.gloPos().Y() - secReflPoint.Y() ) ) +
                                    ( normV.Z() * ( pix.gloPos().Z() - secReflPoint.Z() ) ) );
            virtDetPoint        = pix.gloPos() - ( normV * ( TWO * distance / normV.Mag2() ) );

            // solve the quartic using the new data
            m_quarticSolver.solve<SIMDFP, 2, 3>( emissionPoint, sphMirrData.getCoCs(), virtDetPoint,
                                                 sphMirrData.getRoCs(), sphReflPoint );

            // Iteration flags
            // First iteration ?
            const bool firstIt = ( 0 == iIt );
            // increment iteration counter
            ++iIt;
            // Last it ?
            const bool lastIt = ( m_nMaxQits[rich] == iIt );

            // for iterations after the first, and not the last, see if we are still moving.
            // If not, abort iterations early.
            if ( !firstIt && !lastIt ) {
              const auto diffSq = ( last_mirror_point - secReflPoint ).Mag2();
              if ( all_of( diffSq < m_minSphMirrTolItSIMD[rich] ) ) {
                //_ri_verbo << "   -> Not Moving -> Abort iterations."<< endmsg;
                break;
              }
            }

            // Are we going to iterate again ?
            if ( !lastIt ) {
              //  (re)find the spherical mirror segments
              //_ri_verbo << "   -> Update for sphMirrors using " << sphReflPoint << endmsg;
              sphMirrors = mirrorFinder.findSphMirror( rich, side, sphReflPoint );
              // update the caches
              sphMirrData.update( sphMirrors );
              // store last sec mirror point
              last_mirror_point = secReflPoint;
            }
          }

          // if above while loop failed, abort this photon
          //_ri_verbo << "  -> End It " << pixmask << endmsg;
          if ( none_of( pixmask ) ) { continue; }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // check that spherical mirror reflection point is on the same side as detection point
        // and (if configured to do so) photon does not cross between detector sides
        // --------------------------------------------------------------------------------------
        pixmask &= sameSide( rad, sphReflPoint, virtDetPoint );
        //_ri_verbo << "  -> sameSide " << pixmask << endmsg;
        if ( none_of( pixmask ) ) {
          //_ri_debug << rad << " : Reflection point on wrong side" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // If using aligned mirror segments, get the final sec mirror reflection
        // point using the best mirror segments available at this point
        // For RICH2, use the spherical nature of the scondary mirrors
        // For RICH1, where they are much flatter, assume complete flatness
        // --------------------------------------------------------------------------------------
        if ( m_useAlignedMirrSegs[rich] ) {
          const auto dir = virtDetPoint - sphReflPoint;
          if ( m_treatSecMirrsFlat[rich] ) {
            intersectPlane( sphReflPoint, dir, secMirrData.getNormalPlane(), secReflPoint );
          } else {
            pixmask &= intersectSpherical( sphReflPoint, dir,                            //
                                           secMirrData.getCoCs(), secMirrData.getRoCs(), //
                                           secReflPoint );
            //_ri_verbo << "  -> alignedMirrors " << pixmask << endmsg;
            if ( none_of( pixmask ) ) {
              //_ri_debug << rad << " : Failed final secondary mirror plane intersection" << endmsg;
              continue;
            }
          }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Calculate the cherenkov angles using the photon and track vectors
        // --------------------------------------------------------------------------------------
        // direction at emission. No need to be normalised.
        const auto photonDirection = ( sphReflPoint - emissionPoint );
        SIMDFP     thetaCerenkov( SIMDFP::Zero() ), phiCerenkov( SIMDFP::Zero() );
        segment.angleToDirection( photonDirection, thetaCerenkov, phiCerenkov );
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Truncate the angles to a fixed precision ?
        // --------------------------------------------------------------------------------------
        if ( m_ckValueTruncate[rich] ) {
          using namespace LHCb::Math;
          thetaCerenkov = truncate<PrecisionMode::Relative, 18>( thetaCerenkov );
          phiCerenkov   = truncate<PrecisionMode::Relative, 18>( phiCerenkov );
        }
        // --------------------------------------------------------------------------------------

        //---------------------------------------------------------------------------------------
        // Apply fudge factor correction for small biases in CK theta
        //---------------------------------------------------------------------------------------
        thetaCerenkov += m_ckThetaCorrSIMD[rich];
        //---------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Final checks on the Cherenkov angle
        // --------------------------------------------------------------------------------------
        checkAngle( rich, simd_tkCkAngles, simd_tkCkRes, thetaCerenkov, pixmask );
        //_ri_verbo << "  -> checkAngles " << pixmask << endmsg;
        if ( none_of( pixmask ) ) {
          //_ri_verbo << "    -> photon FAILED checkAngleInRange test" << endmsg;
          continue;
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // 4D reconstruction (i.e. with time)
        // --------------------------------------------------------------------------------------
        SIMDFP photDetTime = SIMDFP::Zero(); // time for pion mass hypo
        if ( m_enable4D[rich] ) {

          // @todo In future this might benefit from some optimisation, to try and avoid math
          // functions or use fast (or approx) versions of them where appropriate

          // Track segment velocity in radiator medium, based on cherenkov angle for this photon
          const auto cosThetaC   = LHCb::Math::fast_cos( thetaCerenkov );
          const auto segVelocity = SIMDFP( Gaudi::Units::c_light ) / ( refIndex * cosThetaC );

          // path length of segment in radiator to emission point
          const auto segPath = std::sqrt( ( emissionPoint - segment.entryPoint() ).Mag2() );

          // photon path length
          const auto photPath = std::sqrt( ( sphReflPoint - emissionPoint ).Mag2() ) +
                                std::sqrt( ( secReflPoint - sphReflPoint ).Mag2() ) +
                                std::sqrt( ( pix.gloPos() - secReflPoint ).Mag2() );

          // transit time inside the PD
          const SIMDFP pdTransitTime( m_pdTransitTime[rich] );

          // Compute reconstructed estimate of the photon detection time assuming baseline (pion)
          // mass hypothesis
          //  = origin time + transit time to radiator entry + track transit time in radiator to
          //    emission point + photon propagation time to photon detector + photoelectron
          //    transit time
          // Save the RICH-only time to the photon object
          photDetTime = ( segPath / segVelocity ) + ( photPath / photonVelocity ) + pdTransitTime;

          //_ri_verbo << "  -> reco.phot time " << photDetTime << endmsg;
          //_ri_verbo << "  ->  pix time " << pix.hitTime() << endmsg;

          // Apply time window to select photons.
          // Loop over mass hypos and apply OR of time window around each.
          // Photon is kept if it is in any one of the windows.
          // Only need confirmation for those not yet rejected already so start with !pixmask
          auto hypoMask = !pixmask;
          for ( const auto hypo : activeParticlesNoBT() ) {
            const auto predT = photDetTime + SIMDFP( segment.timeToRadEntry( hypo ) );
            hypoMask |= pix.isInTime( predT );
            if ( all_of( hypoMask ) ) { break; }
          }
          pixmask &= hypoMask;

          // Finally check if any photons survive.
          if ( none_of( pixmask ) ) {
            //_ri_verbo << "    -> photon FAILED time window test" << endmsg;
            continue;
          }
        }
        // --------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------
        // Create the final photon candidate
        // --------------------------------------------------------------------------------------
        // Note, directly calling constructors here is intentional.
        // Oddily enough, helps with inlining. Without them, relying on argument perfect
        // forwarding results in the emplace_back calls not being inlined and thus a
        // small but noticable runtime penalty.
        assert( photonIndex == photons.size() );
        assert( pixIndex < pixels.size() );
        photons.emplace_back( SIMDCherenkovPhoton{ rich,                                 //
                                                   thetaCerenkov, phiCerenkov,           //
                                                   pix.smartID(), fraction, photDetTime, //
                                                   pixmask } );
        // Save relations for this photon
        relations.emplace_back( Relations::PhotonToParents{ photonIndex, pixIndex, //
                                                            segIndex, inTkRel.tkIndex } );
        // info() << relations.back() << endmsg;
        // info() << gPhoton << endmsg;
        // for ( const auto & sphot : gPhoton.scalarPhotons() ) { info() << sphot << endmsg; }
        // increment photon index for next candidate
        ++photonIndex;

        // If get here, and mirror data saving is enabled so add to output vector
        if ( m_saveMirrorData ) {
          // create a new entry for this photon with the mirror pointers
          mirrorData.emplace_back( sphMirrors, secMirrors, unambigPhoton );
        }
        // --------------------------------------------------------------------------------------

      } // pixel loop

    } // segment loop
  }   // track loop

  // if ( photons.size() > resSize )
  // {
  //   warning() << "Photons reserve size too small. Scale = "
  //             << (float)( segments.size() * pixels.size() ) / photons.size()
  //             << endmsg;
  // }

  // return the final data
  return outData;
}

//=============================================================================
// Find mirror segments and reflection points for given data
//=============================================================================
void SIMDQuarticPhotonReco::findMirrorData( const Rich::Detector::RichBase& deRich,        // The RICH detector
                                            const Rich::Side                side,          // The RICH panel
                                            const SIMD::Point<FP>&          virtDetPoint,  // virtual detection point
                                            const SIMD::Point<FP>&          emissionPoint, // assumed emission point
                                            Mirrors&                        primMirr,      // primary mirrors
                                            Mirrors&                        secMirr,       // secondary mirrors
                                            SIMD::Point<FP>& sphReflPoint, // primary mirror reflection point
                                            SIMD::Point<FP>& secReflPoint, // secondary mirror reflection point
                                            const Rich::Utils::MirrorFinder& mirrorFinder // Mirror finder
) const {
  using namespace LHCb::SIMD;

  // RICH enum
  const auto rich = deRich.rich();

  // solve quartic equation with nominal values and find spherical mirror reflection point
  m_quarticSolver.solve<SIMDFP, 2, 2>( emissionPoint,                               //
                                       deRich.nominalCentreOfCurvatureSIMD( side ), //
                                       virtDetPoint,                                //
                                       deRich.sphMirrorRadiusSIMD(),                //
                                       sphReflPoint );

  // find the spherical mirror segments
  //_ri_verbo << "   -> sphReflPoint " << sphReflPoint << endmsg;
  primMirr = mirrorFinder.findSphMirror( rich, side, sphReflPoint );

  // Search for the secondary segment

  // Direction vector from primary mirror point to virtual detection point
  const auto dir( virtDetPoint - sphReflPoint );

  // find the sec mirror intersction point and secondary mirror segment
  intersectPlane( sphReflPoint, dir, deRich.nominalPlaneSIMD( side ), secReflPoint );

  // find the secondary mirrors
  secMirr = mirrorFinder.findSecMirror( rich, side, secReflPoint );
}

//=============================================================================
// Compute the best emission point for the gas radiators using
// the given spherical mirror reflection points
//=============================================================================
SIMDQuarticPhotonReco::SIMDFP::mask_type
SIMDQuarticPhotonReco::getBestGasEmissionPoint( const Rich::RadiatorType radiator,       // RICH radiator
                                                const SIMD::Point<FP>&   sphReflPoint1,  // first spherical refl point
                                                const SIMD::Point<FP>&   sphReflPoint2,  // second spherical refl point
                                                const SIMD::Point<FP>&   detectionPoint, // detection point
                                                const LHCb::RichTrackSegment& segment,   // track segment
                                                SIMD::Point<FP>&              emissionPoint, // Assumed emission point
                                                SIMDFP&                       fraction       // active track fraction
) const {
  using namespace LHCb::SIMD;

  // Default to all OK
  auto ok = SIMDFP::mask_type( true );

  // Default emission point is in the middle
  emissionPoint = segment.bestPoint();

  // Default point for emission point
  const SIMDFP defaultFrac( m_midPointZFrac[radiator] );
  SIMDFP       alongTkFrac = defaultFrac;

  // which radiator ?
  if ( radiator == Rich::Rich1Gas ) {
    // First reflection and hit point on same y side ?
    const auto sameSide1 = ( ( sphReflPoint1.Y() * detectionPoint.Y() ) > SIMDFP::Zero() );
    const auto sameSide2 = ( ( sphReflPoint2.Y() * detectionPoint.Y() ) > SIMDFP::Zero() );
    // set return flag
    ok = sameSide1 || sameSide2;
    // Update those that pass just one test
    if ( any_of( sameSide1 ^ sameSide2 ) ) {
      const auto m1         = sameSide1 && !sameSide2;
      const auto m2         = sameSide2 && !sameSide1;
      auto       diff       = sphReflPoint1.Y() - sphReflPoint2.Y();
      diff( !( m1 || m2 ) ) = SIMDFP::One(); // prevent div by zero
      const auto f          = SIMDFP::One() / diff;
      fraction( m1 )        = abs( sphReflPoint1.Y() * f );
      fraction( m2 )        = abs( sphReflPoint2.Y() * f );
      alongTkFrac( m1 )     = defaultFrac * fraction;
      alongTkFrac( m2 )     = SIMDFP::One() - ( defaultFrac * fraction );
      emissionPoint         = segment.bestPoint( alongTkFrac );
    }
  } else {
    // RICH2 gas
    // First reflection and hit point on same y side ?
    const auto sameSide1 = ( ( sphReflPoint1.X() * detectionPoint.X() ) > SIMDFP::Zero() );
    const auto sameSide2 = ( ( sphReflPoint2.X() * detectionPoint.X() ) > SIMDFP::Zero() );
    // set return flag
    ok = sameSide1 || sameSide2;
    // Update those that pass just one test
    if ( any_of( sameSide1 ^ sameSide2 ) ) {
      const auto m1         = sameSide1 && !sameSide2;
      const auto m2         = sameSide2 && !sameSide1;
      auto       diff       = sphReflPoint1.X() - sphReflPoint2.X();
      diff( !( m1 || m2 ) ) = SIMDFP::One(); // prevent div by zero
      const auto f          = SIMDFP::One() / diff;
      fraction( m1 )        = abs( sphReflPoint1.X() * f );
      fraction( m2 )        = abs( sphReflPoint2.X() * f );
      alongTkFrac( m1 )     = defaultFrac * fraction;
      alongTkFrac( m2 )     = SIMDFP::One() - ( defaultFrac * fraction );
      emissionPoint         = segment.bestPoint( alongTkFrac );
    }
  }

  // return the mask
  return ok;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDQuarticPhotonReco )

//=============================================================================
