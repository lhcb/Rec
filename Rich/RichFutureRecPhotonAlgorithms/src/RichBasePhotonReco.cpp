/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichBasePhotonReco.h"

// boost
#include "boost/format.hpp"

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode BasePhotonReco::initialize() {

  // Sets up various tools and services
  auto sc = AlgBase::initialize();
  if ( !sc ) return sc;

  // loop over RICHes
  for ( const auto rich : activeDetectors() ) {

    // cache some numbers
    m_minROI2PreSel[rich] = std::pow( m_minROIPreSel[rich], 2 );
    m_maxROI2PreSel[rich] = std::pow( m_maxROIPreSel[rich], 2 );
    m_scalePreSel[rich]   = ( m_ckThetaScale[rich] / m_sepGScale[rich] );
    if ( msgLevel( MSG::DEBUG ) ) {
      // printout for this rich
      auto trich = Rich::text( rich );
      trich.resize( 8, ' ' );
      debug() << trich << " : Pre-Sel. Sep. range       "                          //
              << boost::format( "%9.5f" ) % m_minROIPreSel[rich] << " -> "         //
              << boost::format( "%9.5f" ) % m_maxROIPreSel[rich] << " mm  : Tol. " //
              << boost::format( "%5.1f" ) % m_nSigmaPreSel[rich] << " # sigma"     //
              << endmsg;
      debug() << "         :     Sel. Min/Max CK-theta "                         //
              << boost::format( "%9.5f" ) % m_minCKtheta[rich] << " -> "         //
              << boost::format( "%9.5f" ) % m_maxCKtheta[rich] << " rad : Tol. " //
              << boost::format( "%5.1f" ) % m_nSigma[rich] << " # sigma"         //
              << endmsg;
    }

    // Local SIMD copies of various properties
    m_minROI2PreSelSIMD[rich] = SIMDFP( m_minROI2PreSel[rich] );
    m_maxROI2PreSelSIMD[rich] = SIMDFP( m_maxROI2PreSel[rich] );
    m_scalePreSelSIMD[rich]   = SIMDFP( m_scalePreSel[rich] );
    m_nSigmaPreSelSIMD[rich]  = SIMDFP( m_nSigmaPreSel[rich] );
    m_minCKthetaSIMD[rich]    = SIMDFP( absMinCKTheta( rich ) );
    m_maxCKthetaSIMD[rich]    = SIMDFP( absMaxCKTheta( rich ) );
    m_nSigmaSIMD[rich]        = SIMDFP( m_nSigma[rich] );
    m_ckThetaCorrSIMD[rich]   = SIMDFP( ckThetaCorrection( rich ) );
  }

  _ri_debug << "Bias Corrs : " << m_ckBiasCorrs << endmsg;

  _ri_debug << "JO   Corrs : " << m_ckJOCorrs.value() << endmsg;

  // return
  return sc;
}

//=============================================================================
