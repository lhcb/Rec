/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"

namespace Rich::Future::Rec {

  /// Shortcut to the output data type
  namespace {
    using OutData = CherenkovPhoton::Vector;
  }

  /** @class CreateScalarPhotons RichCreateScalarPhotons.h
   *
   *  Creates Scalar representations of SIMD photons.
   *
   *  @author Chris Jones
   *  @date   2017-11-09
   */
  class CreateScalarPhotons final
      : public LHCb::Algorithm::Transformer<OutData( const SIMDCherenkovPhoton::Vector& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    CreateScalarPhotons( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input
                       KeyValue{ "InputPhotonsLocation", SIMDCherenkovPhotonLocation::Default },
                       // output
                       KeyValue{ "OutputPhotonsLocation", CherenkovPhotonLocation::Default } ) {}

  public:
    /// Functional operator
    OutData operator()( const SIMDCherenkovPhoton::Vector& simdPhotons ) const override {

      /// SIMD floating point type
      using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

      // Create output contain and reserve size based on # and SIMD vector size
      OutData scalarPhotons;
      scalarPhotons.reserve( simdPhotons.size() * SIMDFP::Size );

      // loop over SIMD photons and for scalars for valid entries
      for ( const auto& phot : simdPhotons ) {
        // loop of scalar entries
        for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
          if ( phot.validityMask()[i] ) { scalarPhotons.emplace_back( phot.scalarPhoton( i ) ); }
        }
      }

      // return photons
      return scalarPhotons;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( CreateScalarPhotons )

} // namespace Rich::Future::Rec

//=============================================================================
