###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import ROOT


# Fits a Gaussian distribution to given histogram
def fitHist(h):
    # make pre-fitter
    range = h.GetRMS()
    center = h.GetBinCenter(h.GetMaximumBin())
    height = h.GetBinContent(h.GetMaximumBin())
    prefit = ROOT.TF1(
        h.GetTitle() + " PreFitter", "gaus", center - range, center + range
    )
    prefit.SetParameter(0, height)
    prefit.SetParameter(1, center)
    prefit.SetParameter(2, range)

    # Run prefit
    h.Fit(prefit, "MQRSE0")

    # make final fitter
    range = prefit.GetParameter(2)
    center = prefit.GetParameter(1)
    height = prefit.GetParameter(0)
    scale = 1.7
    fitter = ROOT.TF1(
        h.GetTitle() + " Fitter",
        "gaus",
        center - (scale * range),
        center + (scale * range),
    )
    fitter.SetParameter(0, height)
    fitter.SetParameter(1, center)
    fitter.SetParameter(2, range)

    # Fit
    h.Fit(fitter, "MQRSE")

    # Return Fitted info
    return fitter
