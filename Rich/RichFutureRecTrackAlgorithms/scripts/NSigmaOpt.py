#!/usr/bin/env python3 -B

###############################################################################
# (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import glob
import math
import os

import GaussFit
import matplotlib.pyplot as plt
import ROOT
from matplotlib.backends.backend_pdf import PdfPages
from parse import parse

ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetPadTickX(1)
ROOT.gStyle.SetPadTickY(1)
ROOT.gStyle.SetOptFit(101)
ROOT.gStyle.SetOptStat(1110)
ROOT.gErrorIgnoreLevel = ROOT.kWarning

plt.style.use("seaborn-whitegrid")

dataSet = "Funct"
# dataSet = "Param"

# optName = "NS"
optName = "NSPre"

dataVer = "V3"

# NSigma cuts for each radiator
cutValuesAll = {
    "Funct": {
        "NS": {
            "Rich1Gas": ["5.0", "4.5", "4.0", "3.5", "3.0"],
            "Rich2Gas": ["5.0", "4.5", "4.0", "3.5", "3.0"],
        },
        "NSPre": {
            "Rich1Gas": ["7", "6", "5", "4.5", "4.0", "3.5", "3.0"],
            "Rich2Gas": [
                "24",
                "22",
                "20",
                "18",
                "16",
                "12",
                "10",
                "9",
                "8",
                "7",
                "6",
                "5",
            ],
        },
    },
    "Param": {
        "NS": {
            "Rich1Gas": ["5.0", "4.5", "4.0", "3.5", "3.0"],
            "Rich2Gas": ["5.0", "4.5", "4.0", "3.5", "3.0"],
        },
        "NSPre": {
            "Rich1Gas": ["7", "6", "5", "4.5", "4.0", "3.5", "3.0"],
            "Rich2Gas": ["20", "18", "16", "14", "12", "10", "9", "8", "7", "6", "5"],
        },
    },
}
cutValues = cutValuesAll[dataSet][optName]

minPlotEff = 95

# Track types by RICH radiator
tkTypes = {
    "Rich1Gas": ["Long", "Down", "Up"],
    "Rich2Gas": ["Long", "Down", "Seed"],
    # "Rich1Gas": ["Long"],
    # "Rich2Gas": ["Long"]
}

rads = ["Rich1Gas", "Rich2Gas"]
pids = ["electron", "muon", "pion", "kaon", "proton"]
# rads = ["Rich2Gas"]
# pids = ["kaon"]

minPtot = {"Long": 3.0, "Down": 3.0, "Up": 2.0, "Seed": 3.0}

radPtotThreshold = {
    "Rich1Gas": {
        "electron": 0.1,
        "muon": 2.1,
        "pion": 2.7,
        "kaon": 9.5,
        "proton": 18.5,
    },
    "Rich2Gas": {
        "electron": 0.1,
        "muon": 3.5,
        "pion": 4.7,
        "kaon": 16.5,
        "proton": 31.7,
    },
}

tag = "PBins"


def loadROOTFile(tag):
    fname = (
        "data/NSigmaOpt/"
        + dataVer
        + "/"
        + dataSet
        + "-"
        + optName
        + "-"
        + tag
        + "/RichFuture-Feature-x86_64-centos7-gcc9-opt-Expert-Histos.root"
    )
    rfile = ROOT.TFile.Open(fname)
    if not rfile or rfile.IsZombie():
        print("Error opening", fname)
        return None
    print("  -> Reading", fname)
    return rfile


def histoPath(tk, rad, pid, tag):
    return (
        "RICH/RiMCCKResParam" + tk + "/" + rad + "/" + pid + "/" + tag + "/ckResTrue/"
    )


def getHistEntries(h):
    return h.GetEntries() if h else 0


def getHistIntegral(h):
    if h.GetEntries() > 0:
        res = GaussFit.fitHist(h)
        xrange = 5.0 * res.GetParameter(2)
        integral = res.Integral(-xrange, xrange) / h.GetBinWidth(1)
        return integral
    else:
        return 0


def getHistoMean(h):
    # Parse the title to get bin min and max boundaries
    r = parse("{}-{}-{}-{}-{}<{}<{}", h.GetTitle())
    vmin = float(r[4])
    vmax = float(r[6])
    return round(0.5 * (vmin + vmax), 3)


def getHists(tk, rad, pid, rfile, tag):
    hists = []
    for iBin in range(0, 200):
        # CK res V ck histo
        h = rfile.Get(histoPath(tk, rad, pid, tag) + "bin" + str(iBin + 1))
        # Hist mean
        hMean = getHistoMean(h)
        if h and hMean > minPtot[tk] and hMean > radPtotThreshold[rad][pid]:
            hists += [h.Clone()]
    return hists


def getPhotonYields(hists):
    yields = []
    for h in hists:
        yields += [getHistEntries(h)]
        # yields += [ getHistIntegral(h) ]
        # print( getHistEntries(h),  getHistIntegral(h) )
    return yields


def binMeans(hists):
    binMeans = []
    for h in hists:
        binMeans += [getHistoMean(h)]
    return binMeans


def histToArrays(h):
    x = []
    y = []
    e = []
    for i in range(1, h.GetNbinsX() + 1):
        x += [h.GetBinCenter(i)]
        y += [h.GetBinContent(i)]
        e += [h.GetBinError(i)]
    return x, y, e


def makeEffs(yields, means):
    plplots = []
    min_yield = 50000

    for cut in yields.keys():
        curr_mean = 0
        curr_yield = 0
        curr_base = 0
        effs = []
        selmeans = []
        # loop over bins
        for i in range(len(yields[cut])):
            ents = yields[cut][i]
            mean = means[i]
            base = yields["99"][i]
            if ents > 0:
                curr_mean = ((curr_yield * curr_mean) + (ents * mean)) / (
                    ents + curr_yield
                )
                curr_yield = ents + curr_yield
                curr_base = base + curr_base
                if curr_yield >= min_yield:
                    effs += [max(minPlotEff, 100 * curr_yield / curr_base)]
                    selmeans += [curr_mean]
                    curr_yield = 0
                    curr_base = 0
                    curr_mean = 0
        (effp,) = plt.plot(
            selmeans, effs, markersize=2, linewidth=1, label="cut=" + cut
        )
        plplots += [effp]
    return plplots


# loop over radiators
for rad in rads:
    # Loop over track types
    for tk in tkTypes[rad]:
        # Plot dir
        dir = (
            "Performance/Selection Optimisation/Efficiencies/"
            + dataSet
            + "/"
            + tag
            + "/"
            + rad
            + "/"
            + tk
            + "/"
            + optName
            + "/"
            + dataVer
        )
        if not os.path.exists(dir):
            os.makedirs(dir)
        for pdf in glob.glob(dir + "/*.pdf"):
            os.remove(pdf)
        print("Output Dir", dir)

        # Loop over particles
        for pid in pids:
            print(" -> Processing", dataSet, rad, tk, pid)

            name = rad + "-" + tk + "-" + pid

            yields = {}
            hists = {}
            files = {}

            # Baseline yields in Ptot bins
            baseROOTF = loadROOTFile("99")
            if not baseROOTF:
                continue
            hists["99"] = getHists(tk, rad, pid, baseROOTF, tag)
            yields["99"] = getPhotonYields(hists["99"])

            # mean bin values
            means = binMeans(hists["99"])

            # Loop over cuts and get yields for each
            for cut in cutValues[rad]:
                rfile = loadROOTFile(cut)
                if rfile:
                    files[cut] = rfile
                    hists[cut] = getHists(tk, rad, pid, files[cut], tag)
                    yields[cut] = getPhotonYields(hists[cut])

            detailedPlots = True
            if detailedPlots:
                # ROOT canvas
                c = ROOT.TCanvas(name + tag, name + tag, 1200, 1200)
                ncuts = len(cutValues[rad])
                dimA = math.ceil(math.sqrt(ncuts))
                dimB = math.ceil(ncuts / dimA)
                c.Divide(dimB, dimA)
                # Open PDF for bin plots
                pdfName = dir + "/CKTResPlots-" + name + ".pdf"
                print(" -> Creating", pdfName)
                c.SaveAs(pdfName + "[")
                for iH in range(len(hists["99"])):
                    # base histogram
                    baseH = hists["99"][iH]
                    if baseH and baseH.GetEntries() > 0:
                        c.cd(1)
                        titleS = (
                            dataSet
                            + " "
                            + dataVer
                            + " | "
                            + rad
                            + " "
                            + pid
                            + " "
                            + tk
                            + " | Mean "
                            + str(means[iH])
                            + " | Cut "
                        )
                        baseH.SetTitle(titleS + "99")
                        baseH.Draw()
                        c.Update()
                        # cut histos
                        for i, cut in enumerate(cutValues[rad], 2):
                            if cut in hists.keys():
                                H = hists[cut][iH]
                                if H:
                                    c.cd(i)
                                    # H.SetLineColor(ROOT.kRed)
                                    H.SetTitle(titleS + cut)
                                    H.Draw()
                                    c.Update()
                        c.SaveAs(pdfName)
                # Finalise PDF
                c.SaveAs(pdfName + "]")

            # Make a plot for efficiency of each cut
            plplots = makeEffs(yields, means)
            plt.title(
                dataSet
                + " "
                + dataVer
                + " | "
                + rad
                + " "
                + tk
                + " "
                + pid
                + " | "
                + optName
                + " Cut Efficiencies"
            )
            plt.xlabel("Momentum / GeV/c")
            plt.ylabel("Efficiency / %")
            plt.legend(handles=plplots, prop={"size": 7})
            # Save to PDF
            pdfName = dir + "/Effs-" + name + ".pdf"
            print(" -> Creating", pdfName)
            plt.savefig(pdfName)
            plt.close()
