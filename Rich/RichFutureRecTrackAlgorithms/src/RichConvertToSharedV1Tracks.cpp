/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureKernel/RichAlgBase.h"

// Event Model
#include "Event/Track.h"

namespace Rich::Future::Rec {

  namespace {
    using InData  = LHCb::Tracks;
    using OutData = LHCb::Track::Selection;
  } // namespace

  /** @class TrackFilter RichTrackFilter.h
   *
   *  Converts input tracks to a shared selection
   *
   *  @author Chris Jones
   *  @date   2019-11-15
   */
  class TrackV1ToSelection final
      : public LHCb::Algorithm::Transformer<OutData( const InData& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Constructor
    TrackV1ToSelection( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input
                       KeyValue{ "InTracksLocation", LHCb::TrackLocation::Default },
                       // output
                       KeyValue{ "OutTracksLocation", LHCb::TrackLocation::Default + "RichShared" } ) {
      // Debug messages
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// data converter
    OutData operator()( const InData& tracks ) const override {
      OutData outdata;
      // outdata.reserve( tracks.size() );
      for ( const auto tk : tracks ) { outdata.insert( tk ); }
      return outdata;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackV1ToSelection )

} // namespace Rich::Future::Rec
