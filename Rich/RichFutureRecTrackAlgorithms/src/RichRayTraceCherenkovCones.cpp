/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "Core/FloatComparison.h"
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichRayTracing.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Utils
#include "RichRecUtils/RichMassAliasArray.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"

// boost
#include <boost/container/static_vector.hpp>

// STL
#include <algorithm>
#include <array>
#include <cassert>
#include <iterator>
#include <sstream>
#include <type_traits>
#include <utility>
#include <vector>

namespace Rich::Future::Rec {

  namespace {

    // SIMD types
    using FP         = Rich::SIMD::DefaultScalarFP;
    using SIMDFP     = SIMD::FP<FP>;
    using SIMDVector = SIMD::Vector<FP>;

    /// Abs. max number of 'scalar' ring points
    constexpr std::size_t AbsMaxRingPoints = 96u;
    /// Abs. max number of SIMD ring points
    constexpr std::size_t AbsMaxRingPointsSIMD = AbsMaxRingPoints / SIMDFP::Size;
    /// Demand total number of scalar points is a fixed multiple of SIMD size
    static_assert( 0 == AbsMaxRingPoints % SIMDFP::Size );

    /** @class CosSinPhi RichRayTraceCherenkovCone.h
     *
     *  Utility class to cache cos and sin values
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date   17/02/2008
     */
    template <typename TYPE>
    class CosSinPhi final : public Vc::AlignedBase<Vc::VectorAlignment> {
    public:
      /// Container type
      // Appears to be some issue between ranges and boost static vectors in LCG 101
      // https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/2542
      // For now just use std::vector. Allocated once during initialise so likely not a performance issue.
      // using Vector = boost::container::static_vector<CosSinPhi<TYPE>, AbsMaxRingPointsSIMD>;
      using Vector = std::vector<CosSinPhi<TYPE>>;

    public:
      /// Contructor from a phi value
      explicit CosSinPhi( const TYPE _phi ) : phi( _phi ) { Rich::Maths::fast_sincos( phi, sinPhi, cosPhi ); }

    public:
      alignas( LHCb::SIMD::VectorAlignment ) TYPE phi    = 0; ///< CK phi
      alignas( LHCb::SIMD::VectorAlignment ) TYPE cosPhi = 0; ///< Cos(CK phi)
      alignas( LHCb::SIMD::VectorAlignment ) TYPE sinPhi = 0; ///< Sin(CK phi)
    };

  } // namespace

  /** @class RayTraceCherenkovCones
   *
   *  Creates the Cherenkov cones for each segment and mass hypothesis,
   *  using photon raytracing.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceCherenkovCones final                                                                            //
      : public LHCb::Algorithm::Transformer<MassHypoRingsVector( const LHCb::RichTrackSegment::Vector&,         //
                                                                 const CherenkovAngles::Vector&,                //
                                                                 const Utils::RichSmartIDs&,                    //
                                                                 const Utils::RayTracing&,                      //
                                                                 const Utils::TabulatedRefIndex& ),             //
                                            LHCb::Algorithm::Traits::usesBaseAndConditions<AlgBase<>,           //
                                                                                           Utils::RichSmartIDs, //
                                                                                           Utils::RayTracing,   //
                                                                                           Utils::TabulatedRefIndex>> {

  public:
    /// Standard constructor
    RayTraceCherenkovCones( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "CherenkovAnglesLocation", CherenkovAnglesLocation::Emitted },
                         // input conditions data
                         KeyValue{ "RichSmartIDs", Utils::RichSmartIDs::DefaultConditionKey },
                         KeyValue{ "RichRayTracing", Utils::RayTracing::DefaultConditionKey },
                         KeyValue{ "TabulatedRefIndex", Utils::TabulatedRefIndex::DefaultConditionKey } },
                       // data output
                       { KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted } } ) {
      // debugging
      //   setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Functional operator
    MassHypoRingsVector operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                                    const CherenkovAngles::Vector&        ckAngles,       //
                                    const Utils::RichSmartIDs&            smartIDsHelper, //
                                    const Utils::RayTracing&              rayTrace,       //
                                    const Utils::TabulatedRefIndex&       refIndex ) const override;

  private:
    // data caches

    /// Cached SIMD cos and sin values around the ring, for each SIMD points number
    alignas( LHCb::SIMD::VectorAlignment ) std::array<CosSinPhi<SIMDFP>::Vector, AbsMaxRingPointsSIMD + 1> m_cosSinPhiV;

    /// Cached trace modes for each RICH
    DetectorArray<LHCb::RichTraceMode> m_traceModeRICH = { {} };

    /// Maximum number of SIMD points to ray trace on each ring, for each RICH
    DetectorArray<unsigned int> m_nPointsMaxSIMD = { {} };

    /// Minimum number of points to ray trace on each ring, for each RICH
    DetectorArray<unsigned int> m_nPointsMinSIMD = { {} };

  private:
    // properties

    /// Minimum number of points to ray trace on each ring, for each RICH
    Gaudi::Property<DetectorArray<unsigned int>> m_nPointsMin{ this, "NRingPointsMin", { 16u, 16u } };

    /// Maximum number of points to ray trace on each ring, for each RICH
    Gaudi::Property<DetectorArray<unsigned int>> m_nPointsMax{ this, "NRingPointsMax", { 96u, 96u } };

    /// Tolerence (in %) for creating new CK rings.
    Gaudi::Property<DetectorArray<float>> m_newCKRingTol{ this, "NewCKRingTol", { 0.05f, 0.1f } };

    /// Flag to turn on or off checking of intersections with beampipe
    Gaudi::Property<bool> m_checkBeamPipe{ this, "CheckBeamPipe", false };

    /// Flag to switch between simple or detail HPD description in ray tracing
    Gaudi::Property<bool> m_useDetailedHPDsForRayT{ this, "UseDetailedHPDs", false };

    /** Bailout fraction. If no ray tracings have worked after this
     *  fraction have been perfromed, then give up */
    Gaudi::Property<DetectorArray<float>> m_bailoutFrac{ this, "BailoutFraction", { 0.75f, 0.75f } };
  };

} // namespace Rich::Future::Rec

//=============================================================================

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode RayTraceCherenkovCones::initialize() {

  // Sets up various tools and services
  auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // loop over RICHes
  for ( const auto rich : Rich::detectors() ) {
    // To simplify things demand an exact fit to SIMD vector size
    if ( m_nPointsMax[rich] % SIMDFP::Size != 0 || //
         m_nPointsMin[rich] % SIMDFP::Size != 0 ) {
      error() << rich << " nPoints (" << m_nPointsMin[rich] << "," << m_nPointsMax[rich]
              << ") not a multiple of SIMD vector size (" << SIMDFP::Size << ")." << endmsg;
      return StatusCode::FAILURE;
    }
    // comute min,max SIMD points for current SIMD size (4,8 etc.)
    m_nPointsMaxSIMD[rich] = m_nPointsMax[rich] / SIMDFP::Size;
    m_nPointsMinSIMD[rich] = m_nPointsMin[rich] / SIMDFP::Size;
    // Is Max >= Min ?
    if ( m_nPointsMax[rich] < m_nPointsMin[rich] ) {
      error() << rich << " Max rings (" << m_nPointsMax[rich] //
              << ") < Min rings (" << m_nPointsMin[rich] << ") points !!" << endmsg;
      return StatusCode::FAILURE;
    }
    // Is max number of points <= absolute maximum
    if ( m_nPointsMaxSIMD[rich] > AbsMaxRingPointsSIMD ) {
      error() << rich << " Too many ring points " << m_nPointsMax[rich] << endmsg;
      return StatusCode::FAILURE;
    }
  }

  // Fill the cached cos/sin values
  for ( std::size_t nSIMDPtns = 1; nSIMDPtns <= AbsMaxRingPointsSIMD; ++nSIMDPtns ) {

    // total scalar points
    const auto nPtns = nSIMDPtns * SIMDFP::Size;

    // phi increment
    const auto incPhi = Gaudi::Units::twopi / static_cast<double>( nPtns );

    // vector to fill
    auto& vect = m_cosSinPhiV[nSIMDPtns];
    vect.clear();
    vect.reserve( nSIMDPtns );

    // Fill cos and sin values
    SIMDFP phiSIMD( SIMDFP::Zero() );
    double ckPhi = 0.0;
    for ( unsigned int iPhot = 0; iPhot < nPtns; ++iPhot, ckPhi += incPhi ) {
      // Vc value
      const auto ivc = iPhot % SIMDFP::Size;
      phiSIMD[ivc]   = ckPhi;
      // If SIMD value is full, push to vector
      if ( SIMDFP::Size - 1 == ivc ) {
        vect.emplace_back( phiSIMD );
        phiSIMD = SIMDFP::Zero();
      }
    }

    assert( nSIMDPtns == vect.size() );
  }

  // the ray-tracing mode
  LHCb::RichTraceMode tmpMode( LHCb::RichTraceMode::DetectorPlaneBoundary::RespectPDTubes,
                               ( m_useDetailedHPDsForRayT ? LHCb::RichTraceMode::DetectorPrecision::SphericalPDs
                                                          : LHCb::RichTraceMode::DetectorPrecision::FlatPDs ) );
  if ( m_checkBeamPipe ) { tmpMode.setBeamPipeIntersects( true ); }
  m_traceModeRICH.fill( tmpMode );
  _ri_debug << "Rich1Gas Track " << m_traceModeRICH[Rich::Rich1] << endmsg;
  _ri_debug << "Rich2Gas Track " << m_traceModeRICH[Rich::Rich2] << endmsg;

  // Derived condition objects.
  Utils::RichSmartIDs::addConditionDerivation( this );
  Utils::RayTracing::addConditionDerivation( this );
  Utils::TabulatedRefIndex::addConditionDerivation( this );

  // return
  return sc;
}

//=============================================================================

MassHypoRingsVector                                                                       //
RayTraceCherenkovCones::operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                                    const CherenkovAngles::Vector&        ckAngles,       //
                                    const Utils::RichSmartIDs&            smartIDsHelper, //
                                    const Utils::RayTracing&              rayTrace,       //
                                    const Utils::TabulatedRefIndex&       refIndex ) const {

  // The data to return
  MassHypoRingsVector ringsV;
  ringsV.reserve( segments.size() );

  // local position corrector
  // longer term need to remove this
  const Rich::Rec::RadPositionCorrector<SIMDFP> corrector;

  // Comparison result for good ray tracings
  const RayTracingUtils::SIMDResult::Results goodRes( (int)LHCb::RichTraceMode::RayTraceResult::InPDPanel );

  // local cache values for start vectors, sin(theta) and cos(theta)
  SIMDFP                      sinTheta( SIMDFP::Zero() ), cosTheta( SIMDFP::Zero() );
  SIMD::STDVector<SIMDVector> simdVects;
  simdVects.reserve( std::max( m_nPointsMaxSIMD[Rich::Rich1], m_nPointsMaxSIMD[Rich::Rich2] ) );

  // cache the CK ring tolerance values, using tol. fraction and nom. saturated CK theta values
  const DetectorArray<float> ckTol{
      m_newCKRingTol[Rich::Rich1] * refIndex.nominalSaturatedCherenkovTheta( Rich::Rich1Gas ), //
      m_newCKRingTol[Rich::Rich2] * refIndex.nominalSaturatedCherenkovTheta( Rich::Rich2Gas ) };

  // loop over the input data
  for ( const auto&& [segment, ckTheta] : Ranges::ConstZip( segments, ckAngles ) ) {

    // Add a set of mass hypo rings for this segment
    auto& rings = ringsV.emplace_back();

    // which radiator
    const auto rad = segment.radiator();

    // which rich
    const auto rich = segment.rich();

    // best emission point
    const auto& emissionPoint = segment.bestPoint();

    // CK angle for lightest hypo for this track segment
    const auto lightestCKtheta = ckTheta[lightestActiveHypo()];

    // local mass alias array for CK theta values to compute average values to use for each PID
    MassAliasArray<CherenkovAngles::Type> avgCkTheta;
    {
      // cache hypo for last ring created.
      auto lastRingHypo = lightestActiveHypo();
      // number of values in current average
      unsigned int nAverage{ 0 };
      // set the grouped average CK theta values to use
      for ( const auto id : activeParticlesNoBT() ) {
        if ( id != lightestActiveHypo() &&                           // always make new ring for lightest (first) hypo
             ckTheta[id] > 0 &&                                      // only use alias when above threshold
             ( ckTheta[lastRingHypo] - ckTheta[id] ) < ckTol[rich] ) // Is change 'small' ? No abs
                                                                     // as we know the order is
                                                                     // decreasing in ring size
        {
          // group this ID with previous ones(s)
          const auto alias_id = avgCkTheta.massAlias( lastRingHypo );
          avgCkTheta.setMassAlias( id, alias_id );
          // make an average of this CK theta value with those in the current group.
          ++nAverage;
          avgCkTheta[alias_id] = ( ( nAverage * avgCkTheta[alias_id] ) + ckTheta[id] ) / ( nAverage + 1 );
        } else {
          // just save the ck theta value
          avgCkTheta[id] = ckTheta[id];
          // reset the number of values in the CK theta average
          nAverage = 0;
          // update last ring hypo
          lastRingHypo = id;
        }
      }
    }

    // Loop over PID types
    for ( const auto id : activeParticlesNoBT() ) {

      // CK theta to use for this ring, based on the averages above
      const auto ckT = avgCkTheta[id];

      // above threshold ?
      if ( !LHCb::essentiallyZero( ckT ) ) {

        // Is this an alias ring or are we making a new one ?
        const auto aliasID = avgCkTheta.massAlias( id );
        if ( aliasID < id ) {

          // CK ring radius change small, so reuse last
          rings.setMassAlias( id, aliasID );

        } else {

          // compute the number of SIMD ring points to use for this segment/hypo
          const auto nSIMDPtns =
              m_nPointsMinSIMD[rich] + static_cast<std::size_t>( ( m_nPointsMaxSIMD[rich] - m_nPointsMinSIMD[rich] ) *
                                                                 ( ckT / lightestCKtheta ) );
          // number scalar points
          const auto nPtns = nSIMDPtns * SIMDFP::Size;

          // cached cos/sin vector to use for this # points
          assert( nSIMDPtns < m_cosSinPhiV.size() );
          const auto& cosSinV = m_cosSinPhiV[nSIMDPtns];
          assert( nSIMDPtns == cosSinV.size() );

          // compute sin and cos theta
          Maths::fast_sincos( SIMDFP( ckT ), sinTheta, cosTheta );

          // shortcut to hypo ring
          auto& ring = rings[id];
          assert( ring.empty() );

          // reserve size in the points container
          ring.reserve( nPtns );

          // loop around the ring to create the directions
          simdVects.clear(); // clear any previous values
          for ( const auto& P : cosSinV ) {
            // Photon direction around loop
            simdVects.emplace_back( segment.vectorAtCosSinThetaPhi( cosTheta, sinTheta, P.cosPhi, P.sinPhi ) );
          }

          // The vectorised ray tracing.
          // note the input directions are modified by this call.
          const auto results = rayTrace.traceToDetector( emissionPoint, simdVects, segment, m_traceModeRICH[rich] );

          // bailout number
          const auto nBailOut = m_bailoutFrac[rich] * nPtns;

          // loop over the results and fill
          unsigned int nPhot( 0 ), nOK( 0 );
          for ( const auto&& [res, cosphi] : Ranges::ConstZip( results, cosSinV ) ) {

            // Count total photons
            nPhot += SIMDFP::Size;

            // Count good photons
            nOK += ( res.result >= goodRes ).count();

            // bailout check
            if ( 0 == nOK && nPhot >= nBailOut ) { break; }

            // detection point (SIMD)
            const auto& gP = res.detectionPoint;

            // get corrected SIMD local point
            const auto lP = corrector.correct( smartIDsHelper.globalToPDPanel( rich, gP ), rad );

            // Scalar loop to fill the output container
            // should eventually update output data model to keep in SIMD form
            GAUDI_LOOP_UNROLL( SIMDFP::Size )
            for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {

              // Add a new point
              ring.emplace_back( Gaudi::XYZPoint{ gP.X()[i], gP.Y()[i], gP.Z()[i] },         //
                                 Gaudi::XYZPoint{ lP.X()[i], lP.Y()[i], lP.Z()[i] },         //
                                 res.smartID[i],                                             //
                                 ( RayTracedCKRingPoint::Acceptance )( (int)res.result[i] ), //
                                 res.primaryMirror[i],                                       //
                                 res.secondaryMirror[i],                                     //
                                 res.photonDetector[i],                                      //
                                 cosphi.phi[i] );

            } // scalar loop

          } // results loop
        }
      }
    }
  }

  return ringsV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RayTraceCherenkovCones )

//=============================================================================
