/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <array>
#include <iomanip>
#include <tuple>

// Event Model
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Utils
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec {

  namespace {
    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;
  } // namespace

  /** @class SignalPhotonYields RichSignalPhotonYields.h
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class SignalPhotonYields final
      : public LHCb::Algorithm::MultiTransformer<OutData( const PhotonYields::Vector&,  //
                                                          const PhotonSpectra::Vector&, //
                                                          const GeomEffs::Vector& ),
                                                 Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    SignalPhotonYields( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // input data
                            { KeyValue{ "DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable },
                              KeyValue{ "DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable },
                              KeyValue{ "GeomEffsLocation", GeomEffsLocation::Default } },
                            // output data
                            { KeyValue{ "SignalPhotonYieldLocation", PhotonYieldsLocation::Signal },
                              KeyValue{ "SignalPhotonSpectraLocation", PhotonSpectraLocation::Signal } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const PhotonYields::Vector&  detYields,  //
                        const PhotonSpectra::Vector& detSpectra, //
                        const GeomEffs::Vector&      geomEffs ) const override {

      // make the data to return
      OutData data;
      auto&   yieldV   = std::get<PhotonYields::Vector>( data );
      auto&   spectraV = std::get<PhotonSpectra::Vector>( data );

      // reserve sizes
      yieldV.reserve( detYields.size() );
      spectraV.reserve( detYields.size() );

      // Loop over input data
      for ( auto&& [detYield, detSpec, geomEff] : Ranges::ConstZip( detYields, detSpectra, geomEffs ) ) {

        // Create the signal photon spectra, starting from the detectable spectra
        auto& sigSpectra = spectraV.emplace_back( detSpec );

        // create the yield data
        auto& yields = yieldV.emplace_back();

        // Loop over PID types
        for ( const auto id : activeParticlesNoBT() ) {
          //_ri_verbo << std::setprecision(9)
          //          << id << " detYield " << detYield[id] << " geomEff " << geomEff[id] << endmsg;
          // Scale the detectable yields by the geom eff
          yields.setData( id, detYield[id] * geomEff[id] );
          for ( auto& i : sigSpectra.energyDist( id ) ) { i *= geomEff[id]; }
        }
      }

      // return the new data
      return data;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( SignalPhotonYields )

} // namespace Rich::Future::Rec

//=============================================================================
