/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi Kernel
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "DetDesc/DetectorElement.h"
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichGeomFunctions.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Detector Description
#include "RichDetectors/Rich1.h"

// DetDesc
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

// STL
#include <algorithm>
#include <cmath>
#include <string>

namespace Rich::Future::Rec {

  namespace {

    /// Data cache
    class TkResDataCache final {
    public:
      /// Cache ref. index standard deviations for each radiator
      RadiatorArray<double> refIndexSD = { { 0.488e-3, 0.2916e-4, 0.6777e-5 } };

    public:
      /// Constructor from RICH1
      TkResDataCache( const Detector::Rich1& /* rich1 */ ) {
        // standard deviations
        // If the values exist in the DB, use these
        // comment out for now as values will not be correct for Run3
        // and this algorithm is anyway no longer the default
        // if ( rich1.exists( "RichBrunelRefIndexSDParameters" ) ) {
        //  const auto aRefSD = rich1.paramVect<double>( "RichBrunelRefIndexSDParameters" );
        //  refIndexSD        = {aRefSD[0], aRefSD[1], aRefSD[2]};
        //}
      }
    };

  } // namespace

  /** @class TrackFunctionalCherenkovResolutions
   *
   *  Computes the expected Cherenkov resolutions for the given track segments
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackFunctionalCherenkovResolutions final
      : public LHCb::Algorithm::Transformer<
            CherenkovResolutions::Vector( LHCb::RichTrackSegment::Vector const&, //
                                          CherenkovAngles::Vector const&,        //
                                          MassHypoRingsVector const&,            //
                                          TkResDataCache const&,                 //
                                          Utils::TabulatedRefIndex const&,       //
                                          DetectorElement const&,                //
                                          RadiatorArray<double> const& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<AlgBase<>, TkResDataCache, Utils::TabulatedRefIndex,
                                                           DetectorElement, RadiatorArray<double>>> {

  public:
    /// Standard constructor
    TrackFunctionalCherenkovResolutions( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data input
                       { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal },
                         KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted },
                         // conditions input
                         KeyValue{ "DataCache", DeRichLocations::derivedCondition( name + "-DataCache" ) },
                         KeyValue{ "TabulatedRefIndex", Utils::TabulatedRefIndex::DefaultConditionKey },
                         KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top },
                         KeyValue{ "radLenPerUnitL", DeRichLocations::derivedCondition( name + "-radLenPerUnitL" ) } },
                       // data outputs
                       { KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default } } ) {}

    /// Initialization after creation
    StatusCode initialize() override;

  public:
    /// Algorithm execution via transform
    CherenkovResolutions::Vector operator()( LHCb::RichTrackSegment::Vector const& segments,   //
                                             CherenkovAngles::Vector const&        ckAngles,   //
                                             MassHypoRingsVector const&            massRings,  //
                                             TkResDataCache const&                 dataCache,  //
                                             Utils::TabulatedRefIndex const&       tabRefIndx, //
                                             DetectorElement const&                lhcb,       //
                                             RadiatorArray<double> const&          radLenPerUnitL ) const override;

  private:
    /// Scattering coefficent. should be used with p in GeV
    const double m_scatt = 13.6e-03;

  private:
    // services

    /// Transport Service
    ServiceHandle<ITransportSvc> m_transSvc{ this, "TransportSvc", "TransportSvc" };

  private:
    // properties

    /// Overall scale factors for each radiator
    Gaudi::Property<DetectorArray<float>> m_scale{ this, "ScaleFactor", { 1.0f, 1.0f } };

    /// Flag to turn on the full treatment of MS using the TS
    Gaudi::Property<DetectorArray<bool>> m_useTSForMS{ this, "UseTSForMultScat", { false, false } };

    /// Flag to turn on the caching of the radiation length parameter
    Gaudi::Property<DetectorArray<bool>> m_cacheRadLenP{ this, "CacheRadLenParam", { true, true } };

    /// RICH PD contributions to CK theta resolution
    Gaudi::Property<DetectorArray<double>> m_pdErr{ this, "PDErrors", { 0.0002, 0.0002 } };

    /// RICH PD reference areas (mm^2).
    Gaudi::Property<DetectorArray<double>> m_pdRefArea{ this, "PDRefAreas", { 7.77016, 37.4688 } };

    /// Flag to turn on the full PD area treatment per RICH
    Gaudi::Property<DetectorArray<bool>> m_fullPDAreaTreatment{ this, "FullPDAreaTreatment", { false, true } };

    /// Absolute max CK theta resolution per RICH
    Gaudi::Property<DetectorArray<float>> m_maxRes{ this, "MaxCKThetaRes", { 0.0025f, 0.001f } };

  private:
    // messaging

    /// Problems during determination of path length
    mutable WarningCounter m_pathLenWarn{ this, "Problem computing radiation length" };

    /// Null PD pointer
    mutable WarningCounter m_nullPD{ this, "NULL RICH PD pointer !!" };
  };

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

StatusCode TrackFunctionalCherenkovResolutions::initialize() {
  return Transformer::initialize().andThen( [&]() {
    // derived conditions
    Detector::Rich1::addConditionDerivation( this );
    addConditionDerivation( { Detector::Rich1::DefaultConditionKey }, //
                            inputLocation<TkResDataCache>(),          //
                            []( const Detector::Rich1& r1 ) { return TkResDataCache{ r1 }; } );
    Utils::TabulatedRefIndex::addConditionDerivation( this );

    // Compute for each radiator the radiator radiation length / unit path length
    addConditionDerivation( { inputLocation<DetectorElement>() }, inputLocation<RadiatorArray<double>>(),
                            [&]( DetectorElement const& lhcb ) {
                              RadiatorArray<double> radLenPerUnitL = { { 0, 3.46137e-05, 1.14196e-05 } };
                              auto                  tsCache        = m_transSvc->createCache();
                              // Fixed vector start/end points for each radiator
                              const RadiatorArray<std::pair<Gaudi::XYZPoint, Gaudi::XYZPoint>> radVs{ {
                                  { { 150, 150, 1110 }, { 150, 150, 1170 } },  // Aero (not used in practise)
                                  { { 150, 150, 1500 }, { 150, 150, 1700 } },  // RICH1 gas
                                  { { 500, 500, 10000 }, { 500, 500, 11000 } } // RICH2 gas
                              } };
                              // Use the fixed vectors to compute from the TS the radiation length / mm
                              // in each RICH radiator medium
                              for ( const auto rad : Rich::radiators() ) {
                                const auto rich = richType( rad );
                                if ( m_cacheRadLenP[rich] && !m_useTSForMS[rich] ) {
                                  const auto effL     = m_transSvc->distanceInRadUnits( radVs[rad].first,  //
                                                                                        radVs[rad].second, //
                                                                                        tsCache, *lhcb.geometry(), 0 );
                                  const auto length   = std::sqrt( ( radVs[rad].second - radVs[rad].first ).mag2() );
                                  radLenPerUnitL[rad] = ( length > 0 ? effL / length : 0.0 );
                                  _ri_debug << std::setprecision( 9 ) << rad //
                                            << " radiation length / mm = " << radLenPerUnitL[rad] << endmsg;
                                }
                              }
                              return radLenPerUnitL;
                            } );
  } );
}

CherenkovResolutions::Vector
TrackFunctionalCherenkovResolutions::operator()( LHCb::RichTrackSegment::Vector const& segments,   //
                                                 CherenkovAngles::Vector const&        ckAngles,   //
                                                 MassHypoRingsVector const&            massRings,  //
                                                 TkResDataCache const&                 dataCache,  //
                                                 Utils::TabulatedRefIndex const&       tabRefIndx, //
                                                 DetectorElement const&                lhcb,       //
                                                 RadiatorArray<double> const&          radLenPerUnitL ) const {

  using namespace Gaudi::Units;

  // data to return
  CherenkovResolutions::Vector resV;
  resV.reserve( segments.size() );

  // Create accelerator cache for the transport service
  auto tsCache = m_transSvc->createCache();

  // Loop over input segment data
  for ( auto&& [segment, angles, rings] : Ranges::ConstZip( segments, ckAngles, massRings ) ) {

    // Add a resolution entry for this segment
    auto& res = resV.emplace_back();

    // Is the lowest mass hypo for this track above threshold
    const bool aboveThreshold = ( angles[lightestActiveHypo()] > 0 );

    // momentum for this segment, in GeV units
    const auto ptot = segment.bestMomentumMag() / GeV;

    // check if segment is valid
    if ( aboveThreshold && ptot > 0 ) {

      // Which RICH, radiator
      const auto rad  = segment.radiator();
      const auto rich = segment.rich();

      // cache 1 / momentum
      const auto ptotInv = 1.0 / ptot;

      // the res^2 to fill
      double res2 = 0;

      // Start with the mass hypo independent bits

      //-------------------------------------------------------------------------------
      // multiple scattering
      //-------------------------------------------------------------------------------
      double effL = 0;
      if ( !m_useTSForMS[rich] ) {
        // compute from the cached parameter
        effL = radLenPerUnitL[rad] * segment.pathLength();
      } else {
        // Full TS treatment ...
        try {
          effL = m_transSvc->distanceInRadUnits( segment.entryPoint(), segment.exitPoint(), //
                                                 tsCache, *lhcb.geometry(), 0 );
        } catch ( const TransportSvcException& excpt ) {
          effL = 0;
          ++m_pathLenWarn;
          debug() << "Exception during path length determination " << excpt.message() << endmsg;
        }
      }
      // compute the scattering coefficient from the radiation length
      if ( effL > 0 ) {
        const auto multScattCoeff = ( m_scatt * std::sqrt( effL ) * ( 1.0 + 0.038 * Rich::Maths::fast_log( effL ) ) );
        const auto scatCOvP       = multScattCoeff * ptotInv;
        const auto scattErr       = 2.0 * std::pow( scatCOvP, 2 );
        //_ri_debug << std::setprecision(9) << " Scattering error = " << scattErr << endmsg;
        res2 += scattErr;
      }

      //-------------------------------------------------------------------------------
      // track curvature in the radiator volume
      //-------------------------------------------------------------------------------
      const auto curvErr = ( Rich::Aerogel == rad //
                                 ? 0
                                 : std::pow( Rich::Geom::AngleBetween( segment.entryMomentum(), //
                                                                       segment.exitMomentum() ) *
                                                 0.25,
                                             2 ) );
      //_ri_debug << std::setprecision(9) << " Curvature error = " << curvErr << endmsg;
      res2 += curvErr;

      //-------------------------------------------------------------------------------

      //-------------------------------------------------------------------------------
      // tracking direction errors
      //-------------------------------------------------------------------------------
      const auto dirErr = segment.entryErrors().errTX2() + segment.entryErrors().errTY2();
      //_ri_debug << std::setprecision(9) << " Track direction error = " << dirErr << endmsg;
      res2 += dirErr;
      //-------------------------------------------------------------------------------

      // Loop over active hypos ( note including BT here )
      for ( const auto hypo : activeParticles() ) {

        // start with the hypo independent part
        double hypo_res2 = res2;

        //-------------------------------------------------------------------------------
        // RICH contributions (pixel, PSF errors etc...)
        // Uses the supplied reference contribution for the given pixel area, and scales
        // according to the area for the associated PD
        //-------------------------------------------------------------------------------
        auto pdErr = std::pow( m_pdErr[rich], 2 );
        // Are we using the full treatment to deal with PD size difference ?
        if ( m_fullPDAreaTreatment[rich] && Rich::BelowThreshold != hypo ) {
          // loop over the ring points for this hypo
          unsigned int totalInPD{ 0 };
          double       totalSize{ 0.0 };
          if ( !rings[hypo].empty() ) {
            for ( const auto& P : rings[hypo] ) {
              if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() ) {
                // Check PD pointer
                if ( P.photonDetector() ) {
                  // Count accepted points
                  ++totalInPD;
                  // sum up pixel size
                  totalSize += P.photonDetector()->effectivePixelArea();
                } else {
                  ++m_nullPD;
                }
              }
            }
            // Scale PD errors by average pixel size using reference size
            if ( totalInPD > 0 ) {
              totalSize /= (double)totalInPD;
              pdErr *= totalSize / m_pdRefArea[rich];
            }
          }
        }
        //_ri_debug << std::setprecision(9) << " HPD error = " << pdErr << endmsg;
        hypo_res2 += pdErr;

        // Expected CK theta
        if ( Rich::BelowThreshold != hypo && angles[hypo] > 1e-6 ) {

          // cache tan(cktheta)
          const auto tanCkExp = Rich::Maths::fast_tan( angles[hypo] );
          // 1 /  tan(cktheta)
          const auto tanCkExpInv = 1.0 / tanCkExp;
          //_ri_debug << std::setprecision(9) << "  " << hypo
          //          << " ckExp = " << angles[hypo]
          //          << " tanCkExp = " << tanCkExp << endmsg;

          //-------------------------------------------------------------------------------
          // chromatic error
          //-------------------------------------------------------------------------------
          const auto index        = tabRefIndx.refractiveIndex( segment.radIntersections(), segment.avPhotonEnergy() );
          const auto chromFact    = ( index > 0 ? dataCache.refIndexSD[rad] / index : 0.0 );
          const auto chromatErr   = chromFact * tanCkExpInv;
          const auto chromatErrSq = std::pow( chromatErr, 2 );
          //_ri_debug << std::setprecision(9) << "         Chromatic err = " << chromatErr <<
          // endmsg;
          hypo_res2 += chromatErrSq;
          //-------------------------------------------------------------------------------

          //-------------------------------------------------------------------------------
          // momentum error
          //-------------------------------------------------------------------------------
          constexpr auto GeVSqInv   = 1.0 / (double)( GeV * GeV );
          const auto     mass2      = richPartProps()->massSq( hypo ) * GeVSqInv;
          const auto     massFactor = mass2 / ( mass2 + std::pow( ptot, 2 ) );
          const auto     A          = massFactor * ptotInv * tanCkExpInv;
          const auto     momErrSq   = ( ( segment.entryErrors().errP2() * GeVSqInv ) * std::pow( A, 2 ) );
          //_ri_debug << std::setprecision(9) << "         momentum err = " << momErrSq << endmsg;
          hypo_res2 += momErrSq;
          //-------------------------------------------------------------------------------

        } // theta > 0

        // Compute the final resolution, including global scale factor
        const auto ckRes = std::min( m_scale[rich] * (float)( std::sqrt( hypo_res2 ) ), m_maxRes[rich] );
        //_ri_debug << std::setprecision(9) << "           -> Final error^2 " << ckRes << endmsg;
        res.setData( hypo, ckRes );

      } // hypo loop

    } // above threshold

  } // track loop

  return resV;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TrackFunctionalCherenkovResolutions )

//=============================================================================
