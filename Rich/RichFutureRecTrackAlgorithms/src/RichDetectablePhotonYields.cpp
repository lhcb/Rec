/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/StlArray.h"
#include "RichUtils/ZipRange.h"

// Rich Detector
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichMirror.h"
#include "RichDetectors/RichPD.h"

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <limits>
#include <tuple>
#include <vector>

namespace Rich::Future::Rec {

  namespace {

    // Scalar type to work with
    using ScType = PhotonYields::Type;

    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;

    // init value to make sure we never use uninitalised data
    inline constexpr auto NaN = std::numeric_limits<float>::signaling_NaN();

    /// data cache
    class DetYieldsDataCache final {
    public:
      /// Rich pointers
      DetectorArray<const Rich::Detector::RichBase*> riches = { {} };
      /// cached energy bin efficiences
      RadiatorArray<std::array<ScType, NPhotonSpectraBins>> spectraEffs = { { { NaN } } };

    public:
      /// Constructor from Riches
      DetYieldsDataCache( const Rich::Detector::Rich1&     rich1,     //
                          const Rich::Detector::Rich2&     rich2,     //
                          const Rich::DetectorArray<float> minPhotEn, //
                          const Rich::DetectorArray<float> maxPhotEn ) {

        // cache riches
        riches = { &rich1, &rich2 };

#ifdef USE_DD4HEP
        // PMT Eff.
        const auto pdEff = rich1.param<double>( "PMTSiHitDetectionEff" );
#else
        const auto pdEff =
            ( rich1.param<double>( "HPDQuartzWindowEff" ) * rich1.param<double>( "PMTPedestalDigiEff" ) );
#endif

        // Quartz window params
        const RadiatorArray<double> qWinZSize{ rich1.param<double>( "Rich1GasQuartzWindowThickness" ),
                                               rich1.param<double>( "Rich1GasQuartzWindowThickness" ),
                                               rich2.param<double>( "Rich2GasQuartzWindowThickness" ) };

        // Loop over radiators
        for ( const auto rad : Rich::radiators() ) {

          // Rich for this radiator
          const auto deR  = ( Rich::Rich2Gas == rad ? riches[Rich::Rich2] : riches[Rich::Rich1] );
          const auto rich = richType( rad );

          // temporary photon spectra object
          PhotonSpectra spectra( minPhotEn[rich], maxPhotEn[rich] );

          // loop over the energy bins
          for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {
            // bin efficiency
            double eff = 0.0;
            // bin energy ( in eV )
            const auto energy = spectra.binEnergy( iEnBin ) * Gaudi::Units::eV;
            if ( energy > 0 ) {
              // scale by pedestal loss and Quartz window eff.
              eff = pdEff;
              // The Quartz window efficiency
              eff *= Rich::Maths::fast_exp( -qWinZSize[rad] / ( *( deR->gasWinAbsLength() ) )[energy] );
            }
            // save the final efficiency for this energy bin
            ( spectraEffs[rad] )[iEnBin] = eff;
          } // energy bin loop
        }   // radiator loop
      }
    };

    template <typename TYPE>
    using CountEntry = std::pair<TYPE*, std::uint32_t>;

    /// Utility class to count entry for a given entity.
    template <typename TYPE, std::size_t N>
    class Count final : public LHCb::Boost::Small::Vector<CountEntry<TYPE>, N> {

    public:
      // definitions

      /// The Data type
      using Data = CountEntry<TYPE>;

    public:
      /// Count the given object
      inline void count( TYPE* p ) noexcept {
        // Is this the same object as last time ?
        if ( p == m_last ) {
          ++( m_data->second );
        } else {
          // need to do a search to try and find the entry for this object
          auto it = std::find_if( this->begin(), this->end(), [&p]( const auto& i ) { return i.first == p; } );
          // if not preset add a new count of one
          if ( it == this->end() ) {
            m_data = &( this->emplace_back( p, 1 ) );
          } else {
            // increment existing count
            ++( it->second );
            m_data = &*it;
          }
          // update 'last' pointer cache
          m_last = p;
        }
      }

      /// Reset
      inline void reset() noexcept {
        // clear the vector
        this->clear();
        // reset the cache pointers
        m_last = nullptr;
        m_data = nullptr;
      }

    private:
      // data

      /// The last object added to the count
      TYPE* m_last = nullptr;

      /// Pointer to the data count for the last object added
      Data* m_data = nullptr;
    };

  } // namespace

  /** @class DetectablePhotonYields
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class DetectablePhotonYields final
      : public LHCb::Algorithm::MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&,           //
                                                          const PhotonSpectra::Vector&,                    //
                                                          const MassHypoRingsVector&,                      //
                                                          const DetYieldsDataCache& ),                     //
                                                 LHCb::Algorithm::Traits::usesBaseAndConditions<AlgBase<>, //
                                                                                                DetYieldsDataCache>> {

  public:
    /// Standard constructor
    DetectablePhotonYields( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // data inputs
                            { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                              KeyValue{ "EmittedSpectraLocation", PhotonSpectraLocation::Emitted },
                              KeyValue{ "MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted },
                              // conditions input
                              KeyValue{ "DataCache", DeRichLocations::derivedCondition( name + "-DataCache" ) } },
                            // outputs
                            { KeyValue{ "DetectablePhotonYieldLocation", PhotonYieldsLocation::Detectable },
                              KeyValue{ "DetectablePhotonSpectraLocation", PhotonSpectraLocation::Detectable } } ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialization after creation
    StatusCode initialize() override {
      return MultiTransformer::initialize().andThen( [&] {
        // The detector objects
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        // derived data cache
        _ri_debug << "Declaring DetYieldsDataCache derived condition at '" << inputLocation<DetYieldsDataCache>() << "'"
                  << endmsg;
        addConditionDerivation( { Detector::Rich1::DefaultConditionKey,          // input conditions locations
                                  Detector::Rich2::DefaultConditionKey },        //
                                inputLocation<DetYieldsDataCache>(),             // output location
                                [minPhotEn = richPartProps()->minPhotonEnergy(), //
                                 maxPhotEn = richPartProps()->maxPhotonEnergy()] //
                                ( const Detector::Rich1& r1, const Detector::Rich2& r2 ) {
                                  return DetYieldsDataCache{ r1, r2, minPhotEn, maxPhotEn };
                                } );
      } );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments,       //
                        const PhotonSpectra::Vector&          emittedSpectra, //
                        const MassHypoRingsVector&            massRings,      //
                        const DetYieldsDataCache&             dataCache       //
    ) const override {

      // make the data to return
      OutData data;
      auto&   yieldV   = std::get<PhotonYields::Vector>( data );
      auto&   spectraV = std::get<PhotonSpectra::Vector>( data );

      // reserve sizes
      yieldV.reserve( segments.size() );
      spectraV.reserve( segments.size() );

      // local counts
      Count<const Detector::PD, 40>    pdCount;
      Count<const Detector::Mirror, 4> primMirrCount;
      Count<const Detector::Mirror, 6> secMirrCount;

      // Loop over input data
      for ( auto&& [segment, emitSpectra, rings] : Ranges::ConstZip( segments, emittedSpectra, massRings ) ) {

        // Create the detectable photon spectra, using the same energy range
        // as the emitted spectra
        auto& detSpectra = spectraV.emplace_back( emitSpectra.minEnergy(), emitSpectra.maxEnergy() );

        // sanity check on cached data
        assert( emitSpectra.energyBins() == detSpectra.energyBins() );

        // create the yield data
        auto& yields = yieldV.emplace_back();

        // Which radiator
        const auto rad = segment.radiator();

        // Loop over real PID types
        for ( const auto id : activeParticlesNoBT() ) {

          // the signal
          ScType signal = 0;

          // Skip rings with no hits
          //_ri_debug << "  -> " << id << " #Rings=" << rings[id].size() << endmsg;
          if ( !rings[id].empty() ) {

            // Collect the signal info for this ring
            std::uint32_t totalInPD{ 0 };
            pdCount.reset();
            primMirrCount.reset();
            secMirrCount.reset();
            for ( const auto& P : rings[id] ) {
              if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() ) {
                // Mirrors
                const auto pM = P.primaryMirror();
                const auto sM = P.secondaryMirror();
                // PD
                const auto pd = P.photonDetector();
                // check
                assert( pM && sM && pd );
                // Count accepted points
                ++totalInPD;
                // Count PDs hit by this ring
                pdCount.count( pd );
                // Count primary mirrors
                primMirrCount.count( pM );
                // Count secondary mirrors
                secMirrCount.count( sM );
              }
            }

            // Any hits in acceptance ?
            if ( totalInPD > 0 ) {

              // loop over the energy bins
              for ( std::size_t iEnBin = 0; iEnBin < emitSpectra.energyBins(); ++iEnBin ) {

                // bin energy ( in eV )
                const auto energy = emitSpectra.binEnergy( iEnBin ) * Gaudi::Units::eV;
                if ( energy > 0 ) {

                  // start with emitted signal
                  auto sig = ( emitSpectra.energyDist( id ) )[iEnBin];
                  if ( sig > 0 ) {

                    // Get weighted average PD Q.E.
                    ScType pdQEEff( 0 );
                    if ( !pdCount.empty() ) {
                      for ( const auto& PD : pdCount ) {
                        // add up Q.E. eff
                        pdQEEff += (ScType)( PD.second ) * (ScType)( *( PD.first->pdQuantumEff() ) )[energy];
                      }
                      // normalise the result (and scale from % to fraction)
                      pdQEEff /= (ScType)( 100 * totalInPD );
                    } else {
                      pdQEEff = 1.0;
                      ++m_warnNoPDs;
                    }

                    // Weighted primary mirror reflectivity
                    ScType primMirrRefl( 0 );
                    if ( !primMirrCount.empty() ) {
                      for ( const auto& PM : primMirrCount ) {
                        // add up mirror refl.
                        primMirrRefl += (ScType)( PM.second ) * (ScType)( *( PM.first->reflectivity() ) )[energy];
                      }
                      // normalise the result
                      primMirrRefl /= (ScType)( totalInPD );
                    } else {
                      primMirrRefl = 1.0;
                      ++m_warnNoPrimMirrs;
                    }

                    // Weighted secondary mirror reflectivity
                    ScType secMirrRefl( 0 );
                    if ( !secMirrCount.empty() ) {
                      for ( const auto& SM : secMirrCount ) {
                        // add up mirror refl.
                        secMirrRefl += (ScType)( SM.second ) * (ScType)( *( SM.first->reflectivity() ) )[energy];
                      }
                      // normalise the result
                      secMirrRefl /= (ScType)( totalInPD );
                    } else {
                      secMirrRefl = 1.0;
                      ++m_warnNoSecMirrs;
                    }

                    // Scale the distribution in this bin by the above efficiencies
                    sig *= pdQEEff * primMirrRefl * secMirrRefl;

                    // scale by the cached eff. for this bin
                    sig *= ( dataCache.spectraEffs[rad] )[iEnBin];

                    // if we still have some signal, save the values
                    if ( sig > 0 ) {
                      // Save to the output spectra for this bin
                      ( detSpectra.energyDist( id ) )[iEnBin] = sig;
                      // update the overall detectable signal
                      signal += sig;
                    }

                  } // emitted signal in energy bin > 0

                } // bin energy > 0

              } // loop over energy bins

            } // >0 PDs

          } // Not below threshold

          // save the yield for this hypo
          yields.setData( id, signal );

        } // loop over PID types
      }

      // return the new data
      return data;
    }

  private:
    // messaging

    /// No PDs found
    mutable WarningCounter m_warnNoPDs{ this, "No PDs found -> Assuming Av. PD Q.E. of 1" };

    /// No primary mirrors found
    mutable WarningCounter m_warnNoPrimMirrs{ this, "No primary mirrors found -> Assuming Av. reflectivity of 1" };

    /// No secondary mirrors found
    mutable WarningCounter m_warnNoSecMirrs{ this, "No secondary mirrors found -> Assuming Av. reflectivity of 1" };
  };

  //=============================================================================

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( DetectablePhotonYields )

  //=============================================================================

} // namespace Rich::Future::Rec
