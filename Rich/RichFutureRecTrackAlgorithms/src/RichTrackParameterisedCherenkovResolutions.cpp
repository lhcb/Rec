/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi Kernel
#include "Gaudi/Parsers/Factory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"

// Utils
#include "RichFutureUtils/RichTabulatedRefIndex.h"
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// RichDet
#include "RichDet/Rich1DTabFunc.h"

// Resolution parameters
#include "ResolutionParameters.h"

// STL
#include <algorithm>
#include <cmath>
#include <string>

namespace Rich::Future::Rec {

  namespace {
    using OutD = CherenkovResolutions::Vector;
  }

  /** @class TrackParameterisedCherenkovResolutions
   *
   *  Computes the expected Cherenkov resolutions for the given track segments
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackParameterisedCherenkovResolutions final
      : public LHCb::Algorithm::Transformer<OutD( const LHCb::RichTrackSegment::Vector&, //
                                                  const CherenkovAngles::Vector& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    TrackParameterisedCherenkovResolutions( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data input
                       { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "SignalCherenkovAnglesLocation", CherenkovAnglesLocation::Signal } },
                       // data outputs
                       { KeyValue{ "CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default } } ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

    /// Initialization after creation
    StatusCode initialize() override {

      // Sets up various tools and services
      auto sc = Transformer::initialize();
      if ( !sc ) return sc;

      // Track type
      auto tkType = m_tkType.value();
      // handle minor name variations...
      if ( "Upstream" == tkType ) { tkType = "Up"; }
      if ( "Downstream" == tkType ) { tkType = "Down"; }
      // If 'MC', i.e. we are faking tracks using the RICH extended MC info,
      // then just use parameters for Long tracks
      if ( "MC" == tkType ) { tkType = "Long"; }

      // PID type loop
      for ( const auto pid : activeParticles() ) {

        // Load the resolution parameters for the configured track type
        const std::string mode = ( m_modes[pid] == Momentum ? "Momentum" : "CKTheta" );
        if ( tkType.empty() ) {
          error() << "Track type must be configured" << endmsg;
          return StatusCode::FAILURE;
        }
        const auto params = resolutionParameters( tkType, mode );

        // Initialise the interpolators for each RICH
        for ( const auto rich : activeDetectors() ) {
          const auto rad = radType( rich );
          if ( params[rad][pid].empty() || !m_resInterp[rich][pid].initInterpolator( params[rad][pid] ) ) {
            error() << "Failed to initialise interpolator for " << rad << " " << pid << " " << m_tkType << endmsg;
            return StatusCode::FAILURE;
          }
        }

      } // pids

      // return
      return sc;
    }

  public:
    /// Algorithm execution via transform
    OutD operator()( const LHCb::RichTrackSegment::Vector& segments, //
                     const CherenkovAngles::Vector&        ckAngles ) const override {

      using namespace Gaudi::Units;

      // data to return
      OutD resV;
      resV.reserve( segments.size() );

      // Loop over input segment data
      for ( auto&& [segment, angles] : Ranges::ConstZip( segments, ckAngles ) ) {

        // Add a resolution entry for this segment
        auto& res = resV.emplace_back();

        // Is the lowest mass hypo for this track above threshold
        const bool aboveThreshold = ( angles[lightestActiveHypo()] > 0 );

        // check if segment is valid
        if ( aboveThreshold ) {

          // Which RICH
          const auto rich = segment.rich();

          // Loop over active hypos ( note including BT here )
          for ( const auto pid : activeParticles() ) {

            // get the variable for the interpolation mode, clamped to valid range
            const double var = std::clamp( ( m_modes[pid] == Momentum ? segment.bestMomentumMag() : angles[pid] ),
                                           m_resInterp[rich][pid].minX(), m_resInterp[rich][pid].maxX() );

            // Compute the final resolution, including global scale factor
            const auto ckRes =
                std::clamp( m_scale[rich] * m_resInterp[rich][pid].value( var ), m_minRes[rich], m_maxRes[rich] );
            //_ri_verbo << rich << " " << pid << " " << segment.bestMomentumMag() << " " << ckRes << endmsg;

            // save value
            res.setData( pid, ckRes );

          } // hypo loop

        } // above threshold

      } // track loop

      return resV;
    }

  private:
    // types

    /// Enum for resolution parameterisation mode
    enum ParamMode { Momentum, CKTheta };

  private:
    // data

    /// Interpolated resolutions by radiator and mass hypothesis
    DetectorArray<ParticleArray<Rich::TabulatedFunction1D>> m_resInterp;

    /// Parameterisation modes by mass hypo.
    /// Hardcoded for now. Could potentially become a property if the need arises.
    /// For electron use momentum (as CK theta always saturated), for everything else CK theta.
    // const ParticleArray<ParamMode> m_modes = {Momentum, CKTheta, CKTheta, CKTheta, CKTheta, CKTheta, CKTheta};
    /// Use momentum for all types
    const ParticleArray<ParamMode> m_modes = { Momentum, Momentum, Momentum, Momentum, Momentum, Momentum, Momentum };

  private:
    // properties

    /// Track Type
    Gaudi::Property<std::string> m_tkType{ this, "TrackType", "" };

    /// Overall scale factors for each RICH
    Gaudi::Property<DetectorArray<double>> m_scale{ this, "ScaleFactor", { 1.0f, 1.0f } };

    /// Absolute max CK theta resolution per RICH
    Gaudi::Property<DetectorArray<double>> m_maxRes{ this, "MaxCKThetaRes", { 0.0025f, 0.001f } };

    /// Absolute min CK theta resolution per RICH
    Gaudi::Property<DetectorArray<double>> m_minRes{ this, "MinCKThetaRes", { 0.0f, 0.0f } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackParameterisedCherenkovResolutions )

} // namespace Rich::Future::Rec
