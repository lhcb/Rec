/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

// Rec event model
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecRelations.h"

namespace Rich::Future::Rec {

  namespace {
    /// The output data
    using OutData = Relations::TrackToSegments::Vector;
  } // namespace

  /** @class SelectTrackSegments RichSelectTrackSegments.h
   *
   *  Filters the initial track to segment relations to select only
   *  segments with RICH information.
   *
   *  @author Chris Jones
   *  @date   2016-10-31
   */

  class SelectTrackSegments final
      : public LHCb::Algorithm::Transformer<OutData( const Relations::TrackToSegments::Vector&, //
                                                     const GeomEffs::Vector& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    SelectTrackSegments( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       { KeyValue{ "InTrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Initial },
                         KeyValue{ "GeomEffsLocation", GeomEffsLocation::Default } },
                       { KeyValue{ "OutTrackToSegmentsLocation", Relations::TrackToSegmentsLocation::Selected } } ) {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const Relations::TrackToSegments::Vector& tkToSegRels, //
                        const GeomEffs::Vector&                   geomEffs ) const override {

      // return data
      OutData outRels;
      outRels.reserve( tkToSegRels.size() );

      // count the number selected
      // unsigned int nSelSegs(0);

      // loop over the input track -> segment relations
      for ( const auto& inTkRel : tkToSegRels ) {

        // Make an entry in the selected relations for this track
        auto& outRel = outRels.emplace_back( inTkRel.tkKey, inTkRel.tkIndex );

        // loop over the segments for this track
        for ( const auto& iSeg : inTkRel.segmentIndices ) {
          // perform segment selection using info now available, but was
          // not when the segment was first formed.

          // Check lightest geom eff. > 0
          if ( ( geomEffs[iSeg] )[lightestActiveHypo()] > 0 ) {
            // save in output relations
            outRel.segmentIndices.push_back( iSeg );
            //++nSelSegs;
          }
        }
      }

      //_ri_debug << "Selected " << nSelSegs << " of " << geomEffs.size()
      //          << " Track Segments" << endmsg;

      return outRels;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( SelectTrackSegments )

} // namespace Rich::Future::Rec

//=============================================================================
