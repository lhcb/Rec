/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichRecUtils/RichRadCorrLocalPositions.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec {

  /** @class RayTraceTrackLocalPoints
   *
   *  Converts the ray traced track global PD positions to local coordinates.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class RayTraceTrackLocalPoints final
      : public LHCb::Algorithm::Transformer<
            SegmentPanelSpacePoints::Vector( const LHCb::RichTrackSegment::Vector&, //
                                             const SegmentPanelSpacePoints::Vector&, const Rich::Utils::RichSmartIDs& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    RayTraceTrackLocalPoints( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       { KeyValue{ "TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default },
                         KeyValue{ "TrackGlobalPointsLocation", SpacePointLocation::SegmentsGlobal },
                         // input conditions data
                         KeyValue{ "RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey } },
                       // output data
                       { KeyValue{ "TrackLocalPointsLocation", SpacePointLocation::SegmentsLocal } } ) {
      // debugging
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialize
    StatusCode initialize() override {
      // base class initialise
      return Transformer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
      } );
    }

  public:
    /// Functional operator
    SegmentPanelSpacePoints::Vector                              //
    operator()( const LHCb::RichTrackSegment::Vector&  segments, //
                const SegmentPanelSpacePoints::Vector& gPoints,  //
                const Rich::Utils::RichSmartIDs&       smartIDsHelper ) const override {

      SegmentPanelSpacePoints::Vector lPoints;
      lPoints.reserve( gPoints.size() );

      // local position corrector
      const Rich::Rec::RadPositionCorrector<double> corr;

      // Convert the global points
      for ( const auto& [segment, gPoint] : Ranges::ConstZip( segments, gPoints ) ) {
        if ( gPoint.ok() ) {
          const auto rad  = segment.radiator();
          const auto rich = segment.rich();
          // convert the global info to local
          if ( m_applyLocCorrs[rich] ) {
            lPoints.emplace_back( corr.correct( smartIDsHelper.globalToPDPanel( gPoint.point() ), rad ),              //
                                  corr.correct( smartIDsHelper.globalToPDPanel( gPoint.point( Rich::left ) ), rad ),  //
                                  corr.correct( smartIDsHelper.globalToPDPanel( gPoint.point( Rich::right ) ), rad ), //
                                  gPoint.bestSide(), gPoint.closestPD() );
          } else {
            lPoints.emplace_back( smartIDsHelper.globalToPDPanel( gPoint.point() ),              //
                                  smartIDsHelper.globalToPDPanel( gPoint.point( Rich::left ) ),  //
                                  smartIDsHelper.globalToPDPanel( gPoint.point( Rich::right ) ), //
                                  gPoint.bestSide(), gPoint.closestPD() );
          }
        } else {
          // save a default object
          lPoints.emplace_back();
        }
        _ri_verbo << "Segment PD panel point (local) " << lPoints.back() << endmsg;
      }

      return lPoints;
    }

  private:
    // properties

    /// Flag to turn on average radiator local position corrections
    Gaudi::Property<DetectorArray<bool>> m_applyLocCorrs{ this, "ApplyLocalRadiatorCorrections", { true, true } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RayTraceTrackLocalPoints )

} // namespace Rich::Future::Rec
