/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cstdint>
#include <ostream>
#include <vector>

// Utils
#include "RichFutureUtils/RichSIMDGeomPhoton.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"

// Kernel
#include "Kernel/FastAllocVector.h"

// Event Model
#include "Event/Track.h"

namespace Rich::Future::Rec::Relations {

  //=============================================================================

  /// Basic type (size) for indices.
  /// 32bits ( 2^32 - 1 = 4,294,967,295 ) should be enough...
  using IndexType = std::uint32_t;

  /// Type for storing a list of track segment indices.
  /// Use small vector optimised for two radiators.
  using SegmentIndices = LHCb::Boost::Small::Vector<IndexType, 2>;

  /** @class TrackToSegments RichFutureRecEvent/RichRecRelations.h
   *
   *  Store the relationship between tracks and RICH segments.
   *
   *  The key of the track and the container index for the segments.
   *  This is likely to evolve...
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class TrackToSegments final {

  public:
    /// Constructor from track key
    TrackToSegments( const LHCb::Tracks::key_type key, //
                     const IndexType              index )
        : tkKey( key ) //
        , tkIndex( index ) {}

  public:
    /// The track key
    LHCb::Tracks::key_type tkKey{};
    /// Track index
    IndexType tkIndex{ 0 };
    /// The list of segment indices
    SegmentIndices segmentIndices{};

  public:
    /// Container type
    using Vector = LHCb::STL::Vector<TrackToSegments>;

  public:
    /// overload printout to ostream operator <<
    friend inline std::ostream& operator<<( std::ostream& s, const TrackToSegments& rels ) {
      return s << "Track Key = " << rels.tkKey << " Segments = " << rels.segmentIndices;
    }
  };

  /// TES locations for Track to Segment relations
  namespace TrackToSegmentsLocation {
    /// Default Location in TES for the track to segment 'relations, before selection.
    inline const std::string Initial = "Rec/RichFuture/Relations/TrackToSegments/Initial";
    /// Default Location in TES for the track to segment 'relations, after segment selection.
    inline const std::string Selected = "Rec/RichFuture/Relations/TrackToSegments/Selected";
  } // namespace TrackToSegmentsLocation

  //=============================================================================

  /// List of segment to track indices
  using SegmentToTrackVector = LHCb::STL::Vector<LHCb::Tracks::size_type>;

  /// TES locations for lists of track indices
  namespace SegmentToTrackLocation {
    /// Default Location in TES for the track to photon indices relations
    inline const std::string Default = "Rec/RichFuture/Relations/SegmentToTrack/Default";
  } // namespace SegmentToTrackLocation

  //=============================================================================

  /** @class PhotonToParents RichFutureRecEvent/RichRecRelations.h
   *
   *  Class storing the relationship between photons and the track, segment and
   *  pixel objects they are built from.
   *
   *  Container indices are used through. Again, liable to change....
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */

  class PhotonToParents final {

  public:
    /// Container type
    using Vector = LHCb::STL::Vector<PhotonToParents>;

    /// Photon index type
    using PhotonIndex = IndexType; // Rich::SIMD::Future::RecoPhoton::Vector::size_type;
    /// Pixel index type
    using PixelIndex = IndexType; // Rich::PDPixelCluster::Vector::size_type;
    /// Segment index type
    using SegmentIndex = IndexType; // LHCb::RichTrackSegment::Vector::size_type;
    /// Track index type
    using TrackIndex = IndexType; // LHCb::Tracks::size_type;

  public:
    /// Default constructor
    PhotonToParents() = default;
    /// Constructor from a pixel and segment index pair
    PhotonToParents( const PhotonIndex  photIn, //
                     const PixelIndex   pixIn,  //
                     const SegmentIndex segIn,  //
                     const TrackIndex   tkIn )
        : m_photonIndex( photIn ) //
        , m_pixelIndex( pixIn )   //
        , m_segmentIndex( segIn ) //
        , m_trackIndex( tkIn ) {}

  public:
    /// access the photon index
    [[nodiscard]] inline decltype( auto ) photonIndex() const noexcept { return m_photonIndex; }
    /// access the pixel index
    [[nodiscard]] inline decltype( auto ) pixelIndex() const noexcept { return m_pixelIndex; }
    /// access the segment index
    [[nodiscard]] inline decltype( auto ) segmentIndex() const noexcept { return m_segmentIndex; }
    /// access the track index
    [[nodiscard]] inline decltype( auto ) trackIndex() const noexcept { return m_trackIndex; }

  public:
    /// overload printout to ostream operator <<
    friend inline std::ostream& operator<<( std::ostream& s, const PhotonToParents& rels ) {
      return s << "Photon=" << rels.photonIndex() << " Pixel=" << rels.pixelIndex()
               << " Segment=" << rels.segmentIndex() << " Track=" << rels.trackIndex();
    }

  private:
    /// The photon index
    PhotonIndex m_photonIndex{ 0 };
    /// The pixel index
    PixelIndex m_pixelIndex{ 0 };
    /// The segment index
    SegmentIndex m_segmentIndex{ 0 };
    /// The track index
    TrackIndex m_trackIndex{ 0 };
  };

  /// TES locations for Photon to Pixel+Segment relations
  namespace PhotonToParentsLocation {
    /// Default Location in TES for the photon to pixel+segment relations
    inline const std::string Default = "Rec/RichFuture/Relations/PhotonToParents/Default";
  } // namespace PhotonToParentsLocation

  //=============================================================================

  /// Type for storing a list of photon indices
  using PhotonIndices = LHCb::Boost::Small::Vector<IndexType, 128 / Rich::SIMD::Future::RecoPhoton::SIMDFP::Size>;

  /// List of photon indices
  using PhotonIndicesVector = LHCb::STL::Vector<PhotonIndices>;

  /// TES locations for lists of Photon indices
  namespace PhotonIndicesLocation {
    /// Default Location in TES for the track to photon indices relations
    inline const std::string Tracks = "Rec/RichFuture/Relations/PhotonIndices/Tracks";
  } // namespace PhotonIndicesLocation

  //=============================================================================

} // namespace Rich::Future::Rec::Relations
