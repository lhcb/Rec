/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <ostream>
#include <vector>

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichParticleIDType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// Utils
#include "RichFutureUtils/RichHypoData.h"
#include "RichRecUtils/RichMassAliasArray.h"
#include "RichRecUtils/RichPhotonSpectra.h"

// Boost
#include <boost/container/small_vector.hpp>

namespace Rich::Future::Rec {

  //=============================================================================

  /// Type for geometrical efficiencies
  using GeomEffs = Rich::Future::HypoData<float>;

  /// geometrical efficiencies TES locations
  namespace GeomEffsLocation {
    /// Location in TES for the default geometrical efficiencies
    inline const std::string Default = "Rec/RichFuture/GeomEffs/Default";
  } // namespace GeomEffsLocation

  //=============================================================================

  /// Type for the geometrical efficiency contribution from a single PD
  struct PDGeom {
    /// Default Constructor
    PDGeom() = default;
    /// Constructor from ID and efficiency value
    PDGeom( const LHCb::RichSmartID id, const float e ) : pdID( id ), eff( e ) {}
    /// PD identifier
    LHCb::RichSmartID pdID;
    /// efficieny contribution for this PD
    float eff{ 0.0f };
    /// Container type
    using Vector = boost::container::small_vector<PDGeom, 30>;
  };

  /// Type for the per PD Geom Effs for each mass hypothesis
  using GeomEffsPerPD = MassAliasArray<PDGeom::Vector>;

  /// Vector of GeomEffsPerPD
  using GeomEffsPerPDVector = LHCb::STL::Vector<GeomEffsPerPD>;

  /// geometrical efficiencies per PD TES locations
  namespace GeomEffsPerPDLocation {
    /// Location in TES for the default geometrical efficiencies
    inline const std::string Default = "Rec/RichFuture/GeomEffsPerPD/Default";
  } // namespace GeomEffsPerPDLocation

  //=============================================================================

  /** @class SegmentPhotonFlags
   *  Type for storing segment 'photon regions' data
   *  Indicates if the segment in question has photon in either of the two
   *  RICH detector sides [0,1] = ( top,bottom RICH1, Left,Right in RICH2).
   *  regions. */
  class SegmentPhotonFlags final {
  public:
    /// Type for container
    using Vector = LHCb::STL::Vector<SegmentPhotonFlags>;

  public:
    /// Get the side for a given position and RICH type
    template <typename POINT>
    inline Rich::Side side( const Rich::DetectorType rich, const POINT& p ) const noexcept {
      return ( Rich::Rich1 == rich ? p.y() > 0 ? Rich::top : Rich::bottom : p.x() > 0 ? Rich::left : Rich::right );
    }

    /// Set the flag for the given side
    inline void setInAcc( const Rich::Side side, const bool ok = true ) noexcept { m_photsEachSide[side] = ok; }

    /// Set the flag for a given RICH and point
    template <typename POINT>
    inline void setInAcc( const Rich::DetectorType rich, const POINT& p, const bool ok = true ) noexcept {
      setInAcc( side( rich, p ), ok );
    }

    /// Access the flag for the given side
    [[nodiscard]] inline bool inPanel( const Rich::Side side ) const noexcept { return m_photsEachSide[side]; }

    // Is this segment active in both side ?
    [[nodiscard]] inline bool inBothPanels() const noexcept { return m_photsEachSide[0] && m_photsEachSide[1]; }

    /// Access the flags array
    [[nodiscard]] inline const Rich::PanelArray<bool>& flags() const noexcept { return m_photsEachSide; }

  public:
    /// overload printout to ostream operator <<
    friend inline std::ostream& operator<<( std::ostream& s, const SegmentPhotonFlags& flags ) {
      return s << "[" << flags.flags()[0] << "," << flags.flags()[1] << "]";
    }

  private:
    /// Stores the flag for each RICH side
    Rich::PanelArray<bool> m_photsEachSide = { { false, false } };
  };

  /// Locations in TES for segment photon side flags
  namespace SegmentPhotonFlagsLocation {
    /// Location in TES for the default segment photon flags
    inline const std::string Default = "Rec/RichFuture/SegmentPhotonFlags/Default";
  } // namespace SegmentPhotonFlagsLocation

  //=============================================================================

} // namespace Rich::Future::Rec
