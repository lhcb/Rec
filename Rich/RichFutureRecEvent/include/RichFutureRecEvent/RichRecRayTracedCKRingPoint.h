/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cstdint>
#include <ostream>
#include <utility>

// Gaudi
#include "GaudiKernel/Point3DTypes.h"

// Kernel
#include "Kernel/FastAllocVector.h"
#include "Kernel/RichSmartID.h"

// RichDet
#include "RichDetectors/RichMirror.h"
#include "RichDetectors/RichPD.h"

namespace Rich::Future {

  /** @class RayTracedCKRingPoint RayTracedCKRingPoint.h
   *
   * Ultility class to represent a point on a ray traced Cherenkov Ring
   *
   * @author Chris Jones   (Christopher.Rob.Jones@cern.ch)
   * created Thu Oct  6 16:44:59 2016
   *
   */

  class RayTracedCKRingPoint final {

  public:
    /// Container of RayTracedCKRingPoint
    using Vector = LHCb::STL::Vector<RayTracedCKRingPoint>;

    /// Describes the how this position lies in the RICH acceptance
    enum Acceptance : int8_t {
      UndefinedAcceptance = 0, // The acceptance is undefined
      OutsideHPDPanel     = 1, // Point is outside the general HPD panel acceptance
      InHPDPanel          = 2, // Point is inside the acceptance of the HPD panels
      InHPDTube           = 3  // Point is inside the acceptance of singe HPD tube
    };

  public:
    /// Constructor from global position, location position, acceptance flag and RichSmartID
    RayTracedCKRingPoint( Gaudi::XYZPoint         gPos,                           ///< Global position
                          Gaudi::XYZPoint         lPos,                           ///< Local position
                          const LHCb::RichSmartID id       = LHCb::RichSmartID(), ///< ID
                          const Acceptance        acc      = UndefinedAcceptance, ///< Acceptance flag
                          const Detector::Mirror* primMirr = nullptr,             ///< Primary mirror
                          const Detector::Mirror* secMirr  = nullptr,             ///< secondary mirror
                          const Detector::PD*     pd       = nullptr,             ///< photon detector
                          const float             azimuth  = -1 )
        : m_globalPosition( std::move( gPos ) )
        , m_localPosition( std::move( lPos ) )
        , m_azimuth( azimuth )
        , m_smartID( id )
        , m_acceptance( acc )
        , m_primaryMirror( primMirr )
        , m_secondaryMirror( secMirr )
        , m_pd( pd ) {}

    /// Default Constructor
    RayTracedCKRingPoint() = default;

  public:
    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const;

  public:
    /// Retrieve const  Position on the ring in global coordinates
    [[nodiscard]] const Gaudi::XYZPoint& globalPosition() const noexcept { return m_globalPosition; }

    /// Retrieve const  Position on the ring in local coordinates
    [[nodiscard]] const Gaudi::XYZPoint& localPosition() const noexcept { return m_localPosition; }

    /// Retrieve const  Azimuthal phi w.r.t the ring centre for this position
    [[nodiscard]] float azimuth() const noexcept { return m_azimuth; }

    /// Retrieve const  RichSmartID identifying the PD or pixel this point is in, if known
    [[nodiscard]] const LHCb::RichSmartID& smartID() const noexcept { return m_smartID; }

    /// Retrieve const  The RICH acceptance state of the point
    [[nodiscard]] const Acceptance& acceptance() const noexcept { return m_acceptance; }

    /// Retrieve const  Pointer to the associated Mirror object for the primary mirror
    [[nodiscard]] decltype( auto ) primaryMirror() const noexcept { return m_primaryMirror; }

    /// Retrieve const  Pointer to the associated Mirror object for the secondary mirror
    [[nodiscard]] decltype( auto ) secondaryMirror() const noexcept { return m_secondaryMirror; }

    /// Retrieve const  Pointer to the associated PD object
    [[nodiscard]] decltype( auto ) photonDetector() const noexcept { return m_pd; }

  public:
    /// Update Position on the ring in global coordinates
    void setGlobalPosition( const Gaudi::XYZPoint& value ) noexcept { m_globalPosition = value; }

    /// Update Position on the ring in local coordinates
    void setLocalPosition( const Gaudi::XYZPoint& value ) noexcept { m_localPosition = value; }

    /// Update Azimuthal phi w.r.t the ring centre for this position
    void setAzimuth( const float value ) noexcept { m_azimuth = value; }

    /// Update RichSmartID identifying the PD or pixel this point is in, if known
    void setSmartID( const LHCb::RichSmartID value ) noexcept { m_smartID = value; }

    /// Update The RICH acceptance state of the point
    void setAcceptance( const Acceptance value ) noexcept { m_acceptance = value; }

    /// Update Pointer to the associated Mirror object for the primary mirror
    void setPrimaryMirror( const Detector::Mirror* value ) noexcept { m_primaryMirror = value; }

    /// Update Pointer to the associated Mirror object for the secondary mirror
    void setSecondaryMirror( const Detector::Mirror* value ) noexcept { m_secondaryMirror = value; }

    /// Update Pointer to the associated PD object
    void setPhotonDetector( const Detector::PD* value ) noexcept { m_pd = value; }

  private:
    Gaudi::XYZPoint         m_globalPosition{ 0, 0, 0 }; ///< Position on the ring in global coordinates
    Gaudi::XYZPoint         m_localPosition{ 0, 0, 0 };  ///< Position on the ring in local coordinates
    float                   m_azimuth{ -1 };             ///< Azimuthal phi w.r.t the ring centre for this position
    LHCb::RichSmartID       m_smartID; ///< RichSmartID identifying the PD or pixel this point is in, if known
    Acceptance              m_acceptance{ UndefinedAcceptance }; ///< The RICH acceptance state of the point
    const Detector::Mirror* m_primaryMirror{ nullptr };          ///< Pointer to the associated Mirror object for the
                                                                 ///< primary mirror
    const Detector::Mirror* m_secondaryMirror{ nullptr };        ///< Pointer to the associated Mirror object for the
                                                                 ///< secondary mirror
    const Detector::PD* m_pd{ nullptr };                         ///< Pointer to the associated PD object
  };

  inline std::ostream& RayTracedCKRingPoint::fillStream( std::ostream& s ) const {
    s << "{ "
      << "globalPosition : " << m_globalPosition << std::endl
      << "localPosition : " << m_localPosition << std::endl
      << "azimuth : " << m_azimuth << std::endl
      << "smartID : " << m_smartID << std::endl
      << "acceptance : " << m_acceptance << std::endl
      << "primaryMirror : " << m_primaryMirror << std::endl
      << "secondaryMirror : " << m_secondaryMirror << std::endl
      << "photonDetector :    " << m_pd << std::endl
      << " }";
    return s;
  }

} // namespace Rich::Future

inline std::ostream& operator<<( std::ostream& str, const Rich::Future::RayTracedCKRingPoint& obj ) {
  return obj.fillStream( str );
}

inline std::ostream& operator<<( std::ostream& s, Rich::Future::RayTracedCKRingPoint::Acceptance e ) {
  switch ( e ) {
  case Rich::Future::RayTracedCKRingPoint::UndefinedAcceptance:
    return s << "UndefinedAcceptance";
  case Rich::Future::RayTracedCKRingPoint::OutsideHPDPanel:
    return s << "OutsideHPDPanel";
  case Rich::Future::RayTracedCKRingPoint::InHPDPanel:
    return s << "InHPDPanel";
  case Rich::Future::RayTracedCKRingPoint::InHPDTube:
    return s << "InHPDTube";
  default:
    return s << "ERROR wrong value " << int( e ) << " for enum RayTracedCKRingPoint::Acceptance";
  }
}
