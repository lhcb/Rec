/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <string>
#include <vector>

#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichSIMDGeomPhoton.h"

namespace Rich::Detector {
  class Mirror;
}

namespace Rich::Future::Rec {

  /** @class CherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A scalar reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  using CherenkovPhoton = Rich::Future::RecoPhoton;

  /// TES locations
  namespace CherenkovPhotonLocation {
    /// Default Location in TES for the scalar photons
    inline const std::string Default = "Rec/RichFuture/CherenkovPhotons/Default";
  } // namespace CherenkovPhotonLocation

  /** @class SIMDCherenkovPhoton RichFutureRecEvent/RichRecCherenkovPhotons.h
   *
   *  A SIMD vectorised reconstructed photon object.
   *
   *  @author Chris Jones
   *  @date   2017-10-13
   */
  using SIMDCherenkovPhoton = Rich::SIMD::Future::RecoPhoton;

  /// TES locations
  namespace SIMDCherenkovPhotonLocation {
    /// Default Location in TES for the SIMD photons
    inline const std::string Default = "Rec/RichFuture/SIMDCherenkovPhotons/Default";
  } // namespace SIMDCherenkovPhotonLocation

  /// Class to store mirror data for SIMD photon candidates
  class SIMDMirrorData {
  public:
    /// SIMDFP type
    using SIMDFP = Rich::SIMD::Future::RecoPhoton::SIMDFP;
    /// Mask type
    using MASK = SIMDFP::mask_type;
    /// Container type
    using Vector = std::vector<SIMDMirrorData>;
    /// Array for mirror numbers, same size as SIMD scalar size
    using Mirrors = Rich::SIMD::STDArray<const Detector::Mirror*, SIMDFP>;

  private:
    /// Primary mirror pointers
    Mirrors m_primMirrors{ { nullptr } };
    /// Secondary mirror pointers
    Mirrors m_secMirrors{ { nullptr } };
    /// Flag to indicate if unambiguous photons or not
    alignas( LHCb::SIMD::VectorAlignment ) MASK m_unambigPhot{ MASK( false ) };

  public:
    /// Constructor
    SIMDMirrorData( const Mirrors& sph,        ///< Pointers to primary mirrors
                    const Mirrors& sec,        ///< Pointers to secondary mirrors
                    const MASK&    unambigPhot ///< unambiguous photon mask
                    )
        : m_primMirrors( sph ), m_secMirrors( sec ), m_unambigPhot( unambigPhot ) {}

  public:
    /// Access the primary mirrors
    const Mirrors& primaryMirrors() const noexcept { return m_primMirrors; }
    /// Access the secondary mirrors
    const Mirrors& secondaryMirrors() const noexcept { return m_secMirrors; }
    /// Access the unambiguous photon mask
    const MASK& unambiguousPhoton() const noexcept { return m_unambigPhot; }
  };

  /// TES locations
  namespace SIMDMirrorDataLocation {
    /// Default Location in TES for the SIMD photon mirror numbers
    inline const std::string Default = "Rec/RichFuture/SIMDMirrorData/Default";
  } // namespace SIMDMirrorDataLocation

} // namespace Rich::Future::Rec
