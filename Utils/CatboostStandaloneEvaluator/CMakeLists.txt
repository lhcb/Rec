# ==============================================================================
# Code imported and adapted from
# - https://github.com/catboost/catboost/blob/v1.2.1/catboost/libs/standalone_evaluator
# - https://github.com/catboost/catboost/tree/v1.2.1/catboost/libs/model/flatbuffers
# - https://github.com/catboost/catboost/blob/v1.2.1/catboost/libs/helpers/flatbuffers
#
# See https://github.com/catboost/catboost/blob/v1.2.1/README.md for copyright and license
# ==============================================================================
#[=======================================================================[.rst:
Utils/CatboostStandaloneEvaluator
---------------------------------

This package is a port of some catboost example that is needed to use
catboost models in LHCb.

We need to import these files because of a change in the LCG deployment
of catboost going from version 0.26.1 to 1.2.
#]=======================================================================]
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}")

find_package(catboost REQUIRED)
find_package(flatbuffers REQUIRED)

if(flatc_with_no_warnings_option)
    set(silence_warnings --no-warnings)
endif()

function(compile_flatbuffers_schema_to_cpp SRC_FBS_DIR)
    get_filename_component(SRC_FBS ${SRC_FBS_DIR} NAME)
    string(REGEX REPLACE "\\.fbs$" "_generated.h" GEN_HEADER ${SRC_FBS})
    add_custom_command(
            OUTPUT ${GEN_HEADER}
            COMMAND run flatc -c --gen-mutable
            ${silence_warnings}
            -o "${CMAKE_CURRENT_BINARY_DIR}"
            --reflect-names
            "${CMAKE_CURRENT_SOURCE_DIR}/${SRC_FBS_DIR}")
endfunction()

compile_flatbuffers_schema_to_cpp(flatbuffers/features.fbs)
compile_flatbuffers_schema_to_cpp(flatbuffers/ctr_data.fbs)
compile_flatbuffers_schema_to_cpp(flatbuffers/model.fbs)
compile_flatbuffers_schema_to_cpp(flatbuffers/guid.fbs)

add_library(CatboostStandaloneEvaluator OBJECT
    evaluator.cpp
    features_generated.h
    ctr_data_generated.h
    model_generated.h
    guid_generated.h
)
set_target_properties(CatboostStandaloneEvaluator
    PROPERTIES
        POSITION_INDEPENDENT_CODE YES
)
target_link_libraries(CatboostStandaloneEvaluator
    PUBLIC
        catboost::catboost
        flatbuffers::flatbuffers
)
target_include_directories(CatboostStandaloneEvaluator
    PUBLIC
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_BINARY_DIR}
)
