###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
find_path(flatbuffers_INCLUDE_DIR NAMES flatbuffers/flatbuffers.h
          HINTS ${flatbuffers_home}/include/)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(flatbuffers DEFAULT_MSG flatbuffers_INCLUDE_DIR)

mark_as_advanced(flatbuffers_FOUND flatbuffers_INCLUDE_DIR)
set(flatbuffers_INCLUDE_DIRS ${flatbuffers_INCLUDE_DIR})

if(NOT DEFINED flatc_with_no_warnings_option)
    execute_process(
        COMMAND flatc --help
        ERROR_QUIET
        OUTPUT_VARIABLE _flatc_help
    )
    if(_flatc_help MATCHES "--no-warnings")
        set(flatc_with_no_warnings_option TRUE CACHE BOOL "true if flatc supports the --no-warnings option")
    else()
        set(flatc_with_no_warnings_option FALSE CACHE BOOL "true if flatc supports the --no-warnings option")
    endif()
    mark_as_advanced(flatc_with_no_warnings_option)
endif()

if(TARGET flatbuffers::flatbuffers)
    return()
endif()
if(flatbuffers_FOUND)
  add_library(flatbuffers::flatbuffers IMPORTED INTERFACE)
  target_include_directories(flatbuffers::flatbuffers SYSTEM INTERFACE ${flatbuffers_INCLUDE_DIR})
endif()
