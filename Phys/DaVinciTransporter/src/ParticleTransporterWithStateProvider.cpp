/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/IParticle2State.h"
#include "Kernel/IParticleTransporter.h"
#include "Kernel/TransporterFunctions.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackStateProvider.h"

#include "Gaudi/Parsers/Factory.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Chrono.h"

#include <tuple>

namespace DaVinci {

  /** @class ParticleTransporter
   *  implementation of IParticleTransporter interface that
   *  uses ITrackStateProvider for charged tracks
   *  Note that this approch is really coherent with DecayTreeFitter
   *  @see IParticleTransporter
   *  @see ITrackStateProvider
   *  @attention for single usage it could be a bit slower,
   *             but being used widely it should be rather efficient
   *             due to smart caching embedded into TrackStateProvider
   *  @todo Better treatment of electrons is needed
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @daet 2013-06-03
   */
  class ParticleTransporter : public extends<GaudiTool, IParticleTransporter> {
  public:
    /** Transport a Particle to specified z position
     *  @param particle    (INPUT) the particle to be transported
     *  @param znew        (INPUT) new z-position
     *  @param transported (UPDATE) the transported particle
     *  @see IParticleTransporter::transport
     */
    StatusCode transport( LHCb::Particle const* particle, double znew, LHCb::Particle& transported,
                          IGeometryInfo const& geometry ) const override;

    /** Transport and project a Particle to specified z position.
     *  @param particle    (INPUT) the particle to be transported
     *  @param znew        (INPUT) new z-position
     *  @param transported (UPDATE) the transported particle
     *  @see IParticleTransporter::transportAndProject
     */
    StatusCode transportAndProject( LHCb::Particle const* particle, double znew, LHCb::Particle& transported,
                                    IGeometryInfo const& geometry ) const override;

  protected:
    StatusCode initialize() override;

  public:
    /// standard constructor
    ParticleTransporter( const std::string& type, // the type ??
                         const std::string& name, const IInterface* parent );

  protected:
    StatusCode transportChargedBasic( LHCb::Particle const* particle, double znew, LHCb::Particle& transported,
                                      IGeometryInfo const& geometry ) const;
    // transport electrons (to be improved in future)
    StatusCode transportElectron( const LHCb::Particle* particle, const double znew, LHCb::Particle& transported,
                                  IGeometryInfo const& geometry ) const;

  private: // properties:
    /// the name of state provider
    std::string m_stateprovidername = "TrackStateProvider:PUBLIC"; // the name of state provider
    /// the name of particle <-->state tool
    std::string m_particle2statename = "Particle2State:PUBLIC"; // the name of particle <-->state tool
    /// the name of extrapolator name (1)
    std::string m_extrapolator1name = "TrackParabolicExtrapolator:PUBLIC"; // the name of extrapolator name (1)
    /// the name of extrapolator name (2)
    std::string m_extrapolator2name = "TrackRungeKuttaExtrapolator:PUBLIC"; // the name of extrapolator name (2)
    /// area in Z, where "Trajectory" approximation to be used for long track
    std::pair<double, double> m_region = { -1000 * Gaudi::Units::cm, 1000 * Gaudi::Units::cm };
    /// use stateFromTrajectory only for long tracks
    bool m_interpolateOnlyLongTracks = false;
    /// measure CPU performance?
    bool m_timing = false; // measure CPU performance?

    ITrackStateProvider* m_stateprovider  = nullptr;
    IParticle2State*     m_particle2state = nullptr;
    ITrackExtrapolator*  m_extrapolator1  = nullptr;
    ITrackExtrapolator*  m_extrapolator2  = nullptr;
  };

} // namespace DaVinci

StatusCode DaVinci::ParticleTransporter::initialize() {
  // initialize the base
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) { return sc; }

  m_stateprovider  = tool<ITrackStateProvider>( m_stateprovidername, this );
  m_particle2state = tool<IParticle2State>( m_particle2statename, this );
  m_extrapolator1  = tool<ITrackExtrapolator>( m_extrapolator1name, this );
  if ( !m_extrapolator2name.empty() ) { m_extrapolator2 = tool<ITrackExtrapolator>( m_extrapolator2name, this ); }

  if ( !m_interpolateOnlyLongTracks && m_region.second < 220 * Gaudi::Units::cm ) {
    warning() << "Region ( " << m_region.first << " , " << m_region.second
              << " ) is too small to to interpolate all Downstream tracks. "
              << "Consider to extend it" << endmsg;
  }

  // is message level is low enough, active timing
  if ( msgLevel( MSG::DEBUG ) ) { m_timing = true; }

  return sc;
}

// standard constructor
DaVinci::ParticleTransporter::ParticleTransporter( const std::string& type, const std::string& name,
                                                   const IInterface* parent )
    : extends( type, name, parent ) {
  if ( std::string::npos != name.find( "Master" ) ) {
    m_extrapolator1name = "TrackMasterExtrapolator:PUBLIC";
    m_extrapolator2name = "";
  } else if ( std::string::npos != name.find( "Runge" ) ) {
    m_extrapolator1name = "TrackRungeKuttaExtrapolator:PUBLIC";
    m_extrapolator2name = "";
  } else if ( std::string::npos != name.find( "Linear" ) ) {
    m_extrapolator1name = "TrackLinearExtrapolator:PUBLIC";
    m_extrapolator2name = "TrackParabolicExtrapolator:PUBLIC";
  }
  declareProperty( "TrackStateProvider", m_stateprovidername, "Tool to be used as Track State Provider" );
  declareProperty( "Particle2State", m_particle2statename, "Tool to be used as Particle <--> State converter" );
  declareProperty( "Extrapolator1", m_extrapolator1name, "Track extrapolator to be used for tracks/electrons" );
  declareProperty( "Extrapolator2", m_extrapolator2name, "Track extrapolator to be used for tracks/electrons" );
  declareProperty( "TrajectoryRegion", m_region,
                   "Region in Z where Trajectory approximation should be used for long tracks" );
  declareProperty( "InterpolateOnlyLongTracks", m_interpolateOnlyLongTracks,
                   "Use stateFromTrajectory method only for long tracks, state method for other track types" );
  declareProperty( "MeasureCPUPerformance", m_timing, "Measure CPU perormance" );
}

/*  Transport a Particle to specified z position
 *  @param particle    (INPUT) the particle to be transported
 *  @param znew        (INPUT) new z-position
 *  @param transported (UPDATE) the transported particle
 *  @see IParticleTransporter::transport
 */
StatusCode DaVinci::ParticleTransporter::transport( LHCb::Particle const* particle, const double znew,
                                                    LHCb::Particle& transported, IGeometryInfo const& geometry ) const {
  // measure CPU if required
  Chrono chrono( m_timing ? chronoSvc() : nullptr, name() );
  if ( !particle ) { return Error( "Invalid particle, impossible to transport" ); }
  if ( particle != &transported ) { transported = *particle; }
  if ( !particle->isBasicParticle() ) {
    return DaVinci::Transporter::transportComposite( particle, znew, transported );
  } // RETURN
  else if ( 0 == particle->charge() ) {
    return DaVinci::Transporter::transportNeutralBasic( particle, znew, transported );
  } // RETURN

  // special treatment of electrons (to be improved)
  // the next two lines were copied from transportAndProject()
  // see LHCBPS-1675
  if ( 11 == particle->particleID().abspid() ) { return transportElectron( particle, znew, transported, geometry ); }
  return transportChargedBasic( particle, znew, transported, geometry );
}

/*  Transport and project a Particle to specified z position.
 *  @param particle    (INPUT) the particle to be transported
 *  @param znew        (INPUT) new z-position
 *  @param transported (UPDATE) the transported particle
 *  @see IParticleTransporter::transportAndProject
 */

StatusCode DaVinci::ParticleTransporter::transportAndProject( LHCb::Particle const* particle, const double znew,
                                                              LHCb::Particle&      transported,
                                                              IGeometryInfo const& geometry ) const {
  // measure CPU if required
  Chrono chrono( m_timing ? chronoSvc() : nullptr, name() );
  if ( 0 == particle ) { return Error( "Invalid particle, impossible to transport" ); }
  if ( particle != &transported ) { transported = *particle; }
  if ( !particle->isBasicParticle() ) {
    return DaVinci::Transporter::transportAndProjectComposite( particle, znew, transported );
  } // RETURN
  else if ( 0 == particle->charge() ) {
    return DaVinci::Transporter::transportAndProjectNeutralBasic( particle, znew, transported );
  } // RETURN
  // special treatment of electrons (to be improved)
  if ( 11 == particle->particleID().abspid() ) { return transportElectron( particle, znew, transported, geometry ); }
  return transportChargedBasic( particle, znew, transported, geometry );
}

StatusCode DaVinci::ParticleTransporter::transportChargedBasic( LHCb::Particle const* particle, double znew,
                                                                LHCb::Particle&      transported,
                                                                IGeometryInfo const& geometry ) const {
  if ( !particle ) { return StatusCode::FAILURE; } // RETURN
  const LHCb::ProtoParticle* pp = particle->proto();
  if ( !pp ) { return StatusCode::FAILURE; } // RETURN
  const LHCb::Track* track = pp->track();
  if ( !track ) { return StatusCode::FAILURE; } // RETURN
  // finally use two tools to get an answer
  LHCb::State state;
  if ( ( !m_interpolateOnlyLongTracks || track->checkType( LHCb::Track::Types::Long ) ) && m_region.first <= znew &&
       znew <= m_region.second ) {
    StatusCode sc = m_stateprovider->stateFromTrajectory( state, *track, znew, geometry );
    if ( sc.isFailure() ) {
      Warning( "Error from StateProvider::stateFromTrajectory", sc, 3 ).ignore();
      // make  a try with another method:
      sc = m_stateprovider->state( state, *track, znew, geometry );
      if ( sc.isFailure() ) { return Warning( "Error from StateProvider::state", sc, 3 ); }
    }
  } else {
    StatusCode sc = m_stateprovider->state( state, *track, znew, geometry );
    if ( sc.isFailure() ) { return Warning( "Error from StateProvider::state", sc, 3 ); }
  }
  StatusCode sc = m_particle2state->state2Particle( state, transported );
  if ( sc.isFailure() ) { return Warning( "Error from Particle2State", sc, 3 ); }
  return sc;
}

StatusCode DaVinci::ParticleTransporter::transportElectron( const LHCb::Particle* particle, const double znew,
                                                            LHCb::Particle&      transported,
                                                            IGeometryInfo const& geometry ) const {
  if ( 0 == particle ) { return StatusCode::FAILURE; } // RETURN
  const LHCb::ProtoParticle* pp = particle->proto();
  if ( 0 == pp ) { return StatusCode::FAILURE; } // RETURN
  const LHCb::Track* track = pp->track();
  if ( 0 == track ) { return StatusCode::FAILURE; } // RETURN
  // finally use two tools to get an answer
  LHCb::State state;
  StatusCode  sc = m_particle2state->particle2State( *particle, state );
  if ( sc.isFailure() ) { return Warning( "Error from Particle2State", sc, 3 ); }
  if ( track->checkType( LHCb::Track::Types::Long ) || track->checkType( LHCb::Track::Types::Velo ) ||
       0 == m_extrapolator2 ) {
    // first try "simple" extrapolator
    sc = m_extrapolator1->propagate( state, znew, geometry, LHCb::Tr::PID( particle->particleID().abspid() ) );
    if ( sc.isFailure() && 0 != m_extrapolator2 ) {
      Warning( "Failure from extrapolator-1, try the extrapolator-2", sc, 0 ).ignore();
      m_particle2state->particle2State( *particle, state ).ignore();
      // the second try: use more complicated extrapolator
      sc = m_extrapolator2->propagate( state, znew, geometry, LHCb::Tr::PID( particle->particleID().abspid() ) );
    }
  } else // extrapolator2
  {
    sc = m_extrapolator2->propagate( state, znew, geometry, LHCb::Tr::PID( particle->particleID().abspid() ) );
  }
  if ( sc.isFailure() ) { return Warning( "Error TrackExtrapolator", sc, 3 ); }
  sc = m_particle2state->state2Particle( state, transported );
  if ( sc.isFailure() ) { return Warning( "Error from Particle2State", sc, 3 ); }
  return sc;
}

// finally : the factory:
DECLARE_COMPONENT( DaVinci::ParticleTransporter )
