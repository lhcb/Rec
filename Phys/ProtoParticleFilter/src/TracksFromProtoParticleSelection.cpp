/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "Functors/Function.h"
#include "Functors/with_functors.h"
#include "GaudiKernel/ISvcLocator.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Track& );
    static constexpr auto PropertyName = "TrackPredicate";
  };

  struct ProtoParticlePredicate {
    using Signature                    = bool( const LHCb::ProtoParticle& );
    static constexpr auto PropertyName = "ProtoParticlePredicate";
  };

  using Protos = LHCb::ProtoParticle::Range;
  using Output = LHCb::Track::Selection;
  using Algo =
      with_functors<LHCb::Algorithm::Transformer<Output( Protos const& )>, TrackPredicate, ProtoParticlePredicate>;
} // namespace

namespace LHCb {
  class TracksFromProtoParticleSelection : public Algo {
  public:
    using KeyValue = TracksFromProtoParticleSelection::KeyValue;

    TracksFromProtoParticleSelection( std::string const& name, ISvcLocator* pSvcLocator )
        : Algo( name, pSvcLocator, { KeyValue{ "Input", "" } }, { KeyValue{ "OutputTracks", "" } } ) {}

    Output operator()( Protos const& protos ) const override {
      Output output;

      auto const& track_pred = getFunctor<TrackPredicate>();
      auto const& proto_pred = getFunctor<ProtoParticlePredicate>();

      for ( auto const proto : protos ) {
        auto track = proto->track();
        if ( !track ) {
          ++m_proto_with_no_track;
          continue;
        }

        bool filtered_proto = track_pred( *track ) && proto_pred( *proto );
        m_eff_proto += filtered_proto;

        if ( filtered_proto ) output.insert( track );
      }
      return output;
    }

  private:
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_proto_with_no_track{
        this, "Charged ProtoParticle with no track found. Ignoring" };
    mutable Gaudi::Accumulators::BinomialCounter<> m_eff_proto{ this, "selection efficiency" };
  };

  DECLARE_COMPONENT_WITH_ID( TracksFromProtoParticleSelection, "TracksFromProtoParticleSelection" )
} // namespace LHCb
