/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ObjectContainersSharedMerger.h"
#include "Event/ProtoParticle.h"

namespace LHCb {
  using ProtoContainersSharedMerger = LHCb::ObjectContainersSharedMerger<LHCb::ProtoParticle, LHCb::ProtoParticles>;
  DECLARE_COMPONENT_WITH_ID( ProtoContainersSharedMerger, "ProtoContainersSharedMerger" )
} // namespace LHCb
