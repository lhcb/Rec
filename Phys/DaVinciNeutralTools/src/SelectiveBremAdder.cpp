/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Bremsstrahlung.h"
#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/ISelectiveBremAdder.h"
#include "SelTools/Utilities.h"

using BremWrapper = LHCb::Event::Bremsstrahlung::BremsstrahlungWrapper<LHCb::Particle>;

namespace {
  struct BremKinematics {
    std::optional<Gaudi::LorentzVector> momentum        = std::nullopt;
    std::optional<Gaudi::SymMatrix4x4>  momCovMatrix    = std::nullopt;
    std::optional<Gaudi::Matrix4x3>     posMomCovMatrix = std::nullopt;
  };
} // namespace

/**
 *  Adds brem corrections to particles using energy
 *  (should be output of SelectiveBremMatchAlg)
 *  stored in ProtoParticle
 */

class SelectiveBremAdder : public extends<GaudiTool, ISelectiveBremAdder> {
public:
  using extends::extends;

  bool addBrem( LHCb::Particle& particle, bool force = false ) const override;
  bool addBrem2Pair( LHCb::Particle& p1, LHCb::Particle& p2, bool force = false ) const override;

protected:
  bool           erasebreminfo( LHCb::Particle& particle, bool force = false ) const;
  BremKinematics bremkinematics( BremWrapper const& wrapper ) const;
  void           updatebreminfo( LHCb::Particle& particle, BremKinematics const& brem ) const;

private:
  // properties
  Gaudi::Property<int>   m_overlap_method{ this, "OverlapAssignmentMethod", 0,
                                         "For di-electron (pair) in case of overlap, determine assignment: 0 is to "
                                           "one with lowest matching chi2; 1 is with highest (pre-brem) momentum track" };
  Gaudi::Property<bool>  m_update_covs{ this, "UpdateCovariances", true };
  Gaudi::Property<float> m_relative_mom_inbrem_var{ this, "RelativeMomentumScalingVarianceInBrem",
                                                    LHCb::Event::Bremsstrahlung::details::defaults::err2_out.cast() };
  Gaudi::Property<float> m_relative_mom_outbrem_var{ this, "RelativeMomentumScalingVarianceNotInBrem",
                                                     LHCb::Event::Bremsstrahlung::details::defaults::err2_in.cast() };

  // other members
  ToolHandle<IParticle2State> m_p2s{ this, "Particle2State", "Particle2State" };

  // Counters
  mutable Gaudi::Accumulators::Counter<>     m_revBeamCorrCounter{ this, "Revert brem correction" };
  mutable Gaudi::Accumulators::StatCounter<> m_nAddPhotons{ this, "Nb photons added to single electrons" };
  mutable Gaudi::Accumulators::StatCounter<> m_nAddPairPhotons{ this, "Nb photons added to electron pair" };
  mutable Gaudi::Accumulators::StatCounter<> m_nAddPairSharedPhotons{ this, "Nb photons shared by electron pair" };
  mutable Gaudi::Accumulators::StatCounter<> m_DeltaE{ this, "Delta(E)" };
};

//============================================================================= INTERFACED METHODS

// ---- Add brem to particle
bool SelectiveBremAdder::addBrem( LHCb::Particle& particle, bool force ) const {
  if ( !erasebreminfo( particle, force ) ) return false;
  auto brem = bremkinematics(
      BremWrapper( particle, false, m_relative_mom_inbrem_var.value(), m_relative_mom_outbrem_var.value() ) );
  updatebreminfo( particle, brem );
  m_nAddPhotons += Sel::Utils::hasBrem( particle );
  return true;
}

//---- Add brem(s) to a pair of particles removing overlaps
bool SelectiveBremAdder::addBrem2Pair( LHCb::Particle& p1, LHCb::Particle& p2, bool force ) const {
  if ( !( erasebreminfo( p1, force ) && erasebreminfo( p2, force ) ) ) return false;

  BremKinematics bk1;
  BremKinematics bk2;
  bool           has_overlap = false;
  // scope of BremWrapper (needs const Particle)
  {
    auto b1 = BremWrapper( p1, false, m_relative_mom_inbrem_var.value(), m_relative_mom_outbrem_var.value() );
    auto b2 = BremWrapper( p2, false, m_relative_mom_inbrem_var.value(), m_relative_mom_outbrem_var.value() );
    // check for overlap with CellIDs
    auto b1_ids = b1.bremID();
    auto b2_ids = b2.bremID();
    if ( b1_ids == b2_ids && b1.hasBrem() && b2.hasBrem() ) {
      has_overlap = true;
      switch ( m_overlap_method ) {
      case ( 0 ):
        ( b1_ids < b2_ids ? b2 : b1 ).setOverlapVeto();
        break;
      case ( 1 ):
        ( p1.p() > p2.p() ? b2 : b1 ).setOverlapVeto();
        break;
      }
    }
    bk1 = bremkinematics( b1 );
    bk2 = bremkinematics( b2 );
  }

  updatebreminfo( p1, bk1 );
  updatebreminfo( p2, bk2 );

  if ( has_overlap ) {
    m_nAddPairPhotons += 1;
    m_nAddPairSharedPhotons += 1;
  } else {
    m_nAddPairPhotons += Sel::Utils::hasBrem( p1 );
    m_nAddPairPhotons += Sel::Utils::hasBrem( p2 );
  }

  return true;
}

//============================================================================= PRIVATE METHODS
bool SelectiveBremAdder::erasebreminfo( LHCb::Particle& particle, bool force ) const {
  // boolean 'force' : force re-adding brem

  //-- consistency checks
  const auto* proto = particle.proto();
  if ( !proto ) return false;
  if ( !proto->track() ) return false;

  //-- check if bremsstrahlung has already been added
  if ( Sel::Utils::hasBrem( particle ) && !force ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << " The bremsstrahlung has already been added - nothing to add" << endmsg;
    return false;
  }

  // revert the particle momentum  (as in Charged CombinedParticle)
  if ( force ) {
    ++m_revBeamCorrCounter;
    auto state = &proto->track()->firstState();
    m_p2s->state2Particle( *state, particle ).ignore();
  }

  particle.eraseInfo( LHCb::Particle::additionalInfo::HasBremAdded );
  return true;
}

BremKinematics SelectiveBremAdder::bremkinematics( BremWrapper const& bremwrapper ) const {
  // separate BremWrapper (which needs const Particle) from Particle updating
  BremKinematics brem;

  // update momentum if necessary
  if ( bremwrapper.hasBrem() ) brem.momentum = convert( fourMomentum( bremwrapper ) );

  // update covariance matrices
  if ( m_update_covs ) {
    brem.momCovMatrix    = LHCb::LinAlg::convert<double>( momCovMatrix( bremwrapper ) );
    brem.posMomCovMatrix = convert( momPosCovMatrix( bremwrapper ) );
  }
  return brem;
}

void SelectiveBremAdder::updatebreminfo( LHCb::Particle& particle, BremKinematics const& brem ) const {
  // update momentum if necessary
  if ( brem.momentum.has_value() ) {
    m_DeltaE += brem.momentum.value().E() - particle.momentum().E();
    particle.setMomentum( brem.momentum.value() );
  }

  // update covariance matrices
  if ( brem.momCovMatrix.has_value() ) particle.setMomCovMatrix( brem.momCovMatrix.value() );
  if ( brem.posMomCovMatrix.has_value() ) particle.setPosMomCovMatrix( brem.posMomCovMatrix.value() );

  // update flags
  particle.addInfo( LHCb::Particle::additionalInfo::HasBremAdded, brem.momentum.has_value() );
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( SelectiveBremAdder )
