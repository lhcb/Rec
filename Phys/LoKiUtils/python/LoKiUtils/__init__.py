#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# =============================================================================
## @file  LoKiUtils/__init__.py
#  Helper file to manage LoKiUtils package
#
#        This file is a part of LoKi project -
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2010-05-28
# =============================================================================
"""
Helper file to manage LoKiUtils package

      This file is a part of LoKi project -
'C++ ToolKit  for Smart and Friendly Physics Analysis'

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""

from __future__ import print_function

# =============================================================================
__author__ = "Vanya BELYAEV Ivan.BElyaev@nikhef.nl"
__date__ = "2010-05-28"

# =============================================================================
if "__main__" == __name__:
    print(80 * "*")
    print(__doc__)
    print(" Author  : ", __author__)
    print(" Date    : ", __date__)
    print(80 * "*")

# =============================================================================
# The END
# =============================================================================
