/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Kernel/IParticleDescendants.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/MatrixTransforms.h"
#include "LHCbMath/MatrixUtils.h"

#include <functional>
#include <memory>
#include <string>

#include "Kernel/IParticleCombiner.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

/**
 * This filter is entirely designed to re-order and select
 * D(s)+ -> phi pi events in data, based on a combination
 * of long tracks and a VELO track. It is intended to be used
 * for measurements of the Kaon reconstruction asymmetry.
 *
 * The method of selection is based on (1) a phi mass constraint,
 * with (2) a PV constriant.
 *
 * A variant of this implemented entirely in a 40-line premabulo
 * with LoKi functors was used in Run 2. This was no longer
 * an option for Run 3.
 *
 * This algorithm is thus not intended to be used for other
 * decays.
 *
 * It expects a structure of Ds -> K* K, in which the K*
 * is actually the combination of the two tag long tracks,
 * and the last kaon is the VELO track.
 *
 * Here are the cuts that were applied in run 2:
 * 'Phi2KPi': {
 *                            'PHITAGS_AMAXIPCHI2': 14,
 *                            'Phi_MinVDZ': 2.5,
 *                            'VCHI2': 10,
 *                            'VDCHI2': 15,
 *                            'MAXCHILDMinPT': 1800*MeV,
 *                            'MAXCORRM': 2300,
 *                            'AMMIN': 700,
 *                            'AMMAX': 1600},
 * 'Ds2PhiPi_KaonProbe': {
 *                           'Ds_AMAXIPCHI2': 15,
 *                           'Ds_MaxDOCA': 0.15*mm,
 *                           'Ds_MaxVDZ': 300.0*mm,
 *                           'Ds_MaxVCHI2NDF': 6,
 *                           'Ds_MinVDCHI2': 40,
 *                           'Ds_MinVDZ': 3.5*mm,
 *                           'DIRA': 0.99,
 *                           'Ds_MinSimpleFitM': 1270,
 *                           'Ds_MaxSimpleFitM': 2570,
 *                           'Phi_M_PV_FitMin': 980,
 *                           'Phi_M_PV_FitMax': 1100,
 *                           'DELTA_MASS_MIN': 800,
 *                           'DELTA_MASS_MAX': 1100
 *                       },
 *
 * @author Laurent Dufour <laurent.dufour@cern.ch>
 */
namespace {

  using in_pv           = LHCb::RecVertex::Range;
  using in_parts        = LHCb::Particle::Range;
  using output_t        = std::tuple<LHCb::Particle::Container, LHCb::Particle::Container, LHCb::Particle::Container,
                              LHCb::Vertices, LHCb::Vertices>;
  using filter_output_t = std::tuple<bool, LHCb::Particle::Container, LHCb::Particle::Container,
                                     LHCb::Particle::Container, LHCb::Vertices, LHCb::Vertices>;
  using base_t =
      LHCb::Algorithm::MultiTransformerFilter<output_t( in_parts const&, in_pv const&, DetectorElement const& ),
                                              LHCb::Algorithm::Traits::usesConditions<DetectorElement>>;

  float impactParameterSquared( const Gaudi::XYZPoint& vertex, const Gaudi::XYZPoint& point,
                                const Gaudi::XYZVector& dir ) {
    // Direction does not need to be normalised.
    const auto distance = point - vertex;
    return ( distance - dir * 1. / dir.Mag2() * ( distance.Dot( dir ) ) ).Mag2();
  }

  auto bpvAndMinImpactParameterSquared( const in_pv& pvs, const int nTracksMin, const Gaudi::XYZPoint& point,
                                        const Gaudi::XYZVector& dir ) {
    // Direction does not need to be normalised.
    return std::accumulate( pvs.begin(), pvs.end(), std::pair<LHCb::RecVertex const*, float>{ nullptr, -1 },
                            [&]( auto r, const auto& pv ) {
                              if ( ( pv->nDoF() + 3 ) > 2 * nTracksMin ) {
                                float ip2 = impactParameterSquared( pv->position(), point, dir );
                                if ( !r.first || ip2 < r.second ) return std::pair{ pv, ip2 };
                              }
                              return r;
                            } );
  }

  double transverseMomentumDir( const Gaudi::XYZVector& mom, const Gaudi::XYZVector& dir ) {
    const double dmag2 = dir.Mag2();
    if ( LHCb::essentiallyZero( dmag2 ) ) { return mom.R(); }
    const Gaudi::XYZVector perp = mom - dir * ( mom.Dot( dir ) / dmag2 );
    return perp.R();
  }

  Gaudi::XYZVector get_ip_vector( const Gaudi::XYZPoint& vertex_point, const Gaudi::XYZPoint& point,
                                  const Gaudi::XYZVector& direction ) {
    const Gaudi::Math::Line my_line( point, direction );
    return Gaudi::Math::closestPoint( vertex_point, my_line ) - vertex_point;
  }

  double get_ip_perp( const Gaudi::XYZPoint& vertex_point, const Gaudi::XYZPoint& point,
                      const Gaudi::XYZVector& direction, const Gaudi::XYZVector& momentum_vector_A,
                      const Gaudi::XYZVector& momentum_vector_B ) {
    const auto&      ipVector = get_ip_vector( vertex_point, point, direction );
    Gaudi::XYZVector perp     = momentum_vector_A.Unit().Cross( momentum_vector_B.Unit() );
    return perp.Dot( ipVector );
  }

} // namespace

class Velo2Long_Ds2PhiPi_TrackEffFilter final : public base_t {
  constexpr static auto kaonMass  = 493.67 * Gaudi::Units::MeV;
  constexpr static auto kaonMass2 = kaonMass * kaonMass;
  constexpr static auto pionMass  = 139.57 * Gaudi::Units::MeV;
  constexpr static auto pionMass2 = pionMass * pionMass;
  constexpr static auto PhiMass   = 1019.46 * Gaudi::Units::MeV;
  constexpr static auto PhiMass2  = PhiMass * PhiMass;

public:
  Velo2Long_Ds2PhiPi_TrackEffFilter( const std::string& name, ISvcLocator* pSvc )
      : base_t( name, pSvc,
                { KeyValue{ "InputParticle", "" }, KeyValue{ "InputPVs", "" },
                  KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                { KeyValue{ "OutputParticles", "" }, KeyValue{ "OutputPhiContainer", "" },
                  KeyValue{ "OutputBasicContainer", "" }, KeyValue{ "OutputPhiVertices", "" },
                  KeyValue{ "OutputDsVertices", "" } } ) {}

  filter_output_t operator()( const in_parts& parts, const in_pv& vertices,
                              DetectorElement const& lhcb ) const override {
    filter_output_t all_output;
    bool&           event_accepted   = std::get<0>( all_output );
    auto&           out_heads        = std::get<1>( all_output );
    auto&           out_phi          = std::get<2>( all_output );
    auto&           out_basics       = std::get<3>( all_output );
    auto&           out_phi_vertices = std::get<4>( all_output );
    auto&           out_ds_vertices  = std::get<5>( all_output );

    ++m_eventCount;
    for ( const LHCb::Particle* orig_p : parts ) {

      ++m_candidateCount;

      if ( orig_p->daughters().size() < 2 ) continue;

      const LHCb::Particle* orig_probe_particle  = ( orig_p->daughters() ).at( 1 );
      const LHCb::Particle* orig_tag_combination = ( orig_p->daughters() ).at( 0 ); // should be the "phi"

      if ( orig_probe_particle == nullptr || orig_tag_combination == nullptr ) continue;
      if ( orig_probe_particle->proto() == nullptr ) continue;

      if ( orig_tag_combination->daughters().empty() || orig_tag_combination->daughters().size() != 2 ) { continue; }

      if ( orig_p->endVertex() == nullptr ) continue;

      const auto& tag_kaon              = orig_tag_combination->daughters().at( 0 );
      const auto& tag_pion              = orig_tag_combination->daughters().at( 1 );
      const auto  my_total_tag_momentum = tag_kaon->momentum() + tag_pion->momentum();

      auto my_probe_momentum               = orig_probe_particle->momentum();
      auto mass_constrained_probe_momentum = orig_probe_particle->momentum();

      // first do the mass constraint, and make a relatively loose cut on the mass
      const float ETagKaon = tag_kaon->momentum().E();

      const float original_momentum_probe = std::sqrt( orig_probe_particle->momentum().Vect().Mag2() );
      const float ptagcostheta =
          tag_kaon->momentum().Vect().Dot( orig_probe_particle->momentum().Vect() ) / original_momentum_probe;
      const float DeltaMSquared = PhiMass2 - kaonMass2 - kaonMass2;

      float my_new_momentum = 0.5 * ( DeltaMSquared ) / ( ETagKaon - ptagcostheta );

      float Eprobe    = std::sqrt( 1. + ( kaonMass / my_new_momentum ) * ( kaonMass / my_new_momentum ) );
      my_new_momentum = DeltaMSquared / ( 2.0 * ( ETagKaon * Eprobe - ptagcostheta ) );

      for ( unsigned int it = 0; it < 5; ++it ) {
        Eprobe          = std::sqrt( 1. + ( kaonMass / my_new_momentum ) * ( kaonMass / my_new_momentum ) );
        my_new_momentum = DeltaMSquared / ( 2.0 * ( ETagKaon * Eprobe - ptagcostheta ) );
      }

      const float scale_MConstr = my_new_momentum / original_momentum_probe;

      mass_constrained_probe_momentum.SetPxPyPzE(
          scale_MConstr * mass_constrained_probe_momentum.x(), scale_MConstr * mass_constrained_probe_momentum.y(),
          scale_MConstr * mass_constrained_probe_momentum.z(),
          std::sqrt( kaonMass2 + scale_MConstr * scale_MConstr * mass_constrained_probe_momentum.P2() ) );

      Gaudi::LorentzVector mass_constrained_composite_momentum_vector =
          mass_constrained_probe_momentum + tag_kaon->momentum() + tag_pion->momentum();

      auto [bpv, ip2] = bpvAndMinImpactParameterSquared( vertices, 6, orig_p->endVertex()->position(),
                                                         ( mass_constrained_composite_momentum_vector ).Vect() );
      if ( bpv == nullptr ) {
        ++m_noBPVfound;
        continue;
      }

      // Now time for the IP perp
      const double ip_perp = get_ip_perp(
          bpv->position(), orig_p->endVertex()->position(), mass_constrained_composite_momentum_vector.Vect(),
          mass_constrained_probe_momentum.Vect(), orig_tag_combination->momentum().Vect() );

      Gaudi::LorentzVector PV_constrained_probe_momentum( orig_p->momentum() );

      const LHCb::VertexBase* vx         = orig_p->endVertex();
      const Gaudi::XYZVector  dir        = vx->position() - bpv->position();
      auto                    prb_p_PERP = transverseMomentumDir( orig_probe_particle->momentum().Vect(), dir );
      auto                    tag_p_perp = transverseMomentumDir( my_total_tag_momentum.Vect(), dir );

      if ( fabs( prb_p_PERP ) < 1 ) continue; // momentum colinear with the flight direction

      const auto scalePVConst = fabs( ( tag_p_perp ) / ( prb_p_PERP ) );
      PV_constrained_probe_momentum.SetPxPyPzE(
          scalePVConst * my_probe_momentum.x(), scalePVConst * my_probe_momentum.y(),
          scalePVConst * my_probe_momentum.z(),
          std::sqrt( kaonMass2 + scalePVConst * scalePVConst * my_probe_momentum.P2() ) );
      Gaudi::LorentzVector PV_constrained_total_momentum =
          PV_constrained_probe_momentum + orig_tag_combination->momentum();
      Gaudi::LorentzVector PV_constrained_phi_momentum = PV_constrained_probe_momentum + tag_kaon->momentum();
      float PV_constrained_delta_mass = PV_constrained_total_momentum.M() - PV_constrained_phi_momentum.M();

      if ( mass_constrained_composite_momentum_vector.M() > m_phiconstr_d_mass_min &&
           mass_constrained_composite_momentum_vector.M() < m_phiconstr_d_mass_max &&
           PV_constrained_total_momentum.M() > m_pvconstr_D_mass_min &&
           PV_constrained_total_momentum.M() < m_pvconstr_D_mass_max &&
           PV_constrained_phi_momentum.M() > m_pvconstr_phi_mass_min &&
           PV_constrained_phi_momentum.M() < m_pvconstr_phi_mass_max &&
           PV_constrained_delta_mass > m_pvconstr_delta_mass_min &&
           PV_constrained_delta_mass < m_pvconstr_delta_mass_max && PV_constrained_probe_momentum.P() > m_probe_p_min &&
           PV_constrained_probe_momentum.Pt() > m_probe_pt_min && PV_constrained_probe_momentum.Pt() < m_probe_pt_max &&
           fabs( ip_perp ) < ip_perp_max && std::sqrt( ip2 ) < ip_max ) {

        auto new_probe       = std::unique_ptr<LHCb::Particle>( orig_probe_particle->clone() );
        auto original_charge = orig_probe_particle->charge();
        auto new_charge      = -1 * tag_kaon->charge();
        new_probe->setParticleID(
            LHCb::ParticleID( new_charge / original_charge * orig_probe_particle->particleID().pid() ) );
        new_probe->setMomentum( mass_constrained_probe_momentum );
        new_probe->setPV( bpv );

        auto new_phi        = std::make_unique<LHCb::Particle>( orig_tag_combination->particleID() );
        auto new_phi_vertex = std::make_unique<LHCb::Vertex>();

        LHCb::Particle::ConstVector phiDaughters = { tag_kaon, new_probe.get() };
        auto fit_success = m_combiner->combine( phiDaughters, *new_phi, *new_phi_vertex, *lhcb.geometry() ).isSuccess();
        if ( !fit_success ) {
          ++m_msg_fit_failure;
          continue; // all new objects should be unique_ptrs and cleaned up
        }
        new_phi->setPV( bpv );

        LHCb::Particle::ConstVector DsDaughters   = { new_phi.get(), tag_pion };
        int                         pid_ds        = tag_pion->charge() * 431;
        auto                        new_Ds        = std::make_unique<LHCb::Particle>( LHCb::ParticleID( pid_ds ) );
        auto                        new_Ds_vertex = std::make_unique<LHCb::Vertex>();

        fit_success = m_combiner->combine( DsDaughters, *new_Ds, *new_Ds_vertex, *lhcb.geometry() ).isSuccess();
        if ( !fit_success ) {
          ++m_msg_fit_failure;
          continue; // all new objects should be unique_ptrs and cleaned up
        }

        new_Ds->setPV( bpv );

        out_heads.insert( new_Ds.release() );

        out_phi_vertices.insert( new_phi_vertex.release() );
        out_ds_vertices.insert( new_Ds_vertex.release() );

        out_phi.insert( new_phi.release() );
        out_basics.insert( new_probe.release() );

        ++m_newpartCounts;
      }
    }

    event_accepted = !( out_heads.empty() );
    return all_output;
  };

private:
  // cuts
  Gaudi::Property<float> m_probe_p_min{ this, "PVConstrainedProbePMin", 5000. * Gaudi::Units::MeV };

  Gaudi::Property<float> m_probe_pt_min{ this, "PVConstrainedProbePtMin", 250. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_probe_pt_max{ this, "PVConstrainedProbePtMax", 100000. * Gaudi::Units::MeV };

  Gaudi::Property<float> m_pvconstr_phi_mass_min{ this, "PVConstrainedPhiMassMin", 920 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_pvconstr_phi_mass_max{ this, "PVConstrainedPhiMassMax", 1140 * Gaudi::Units::MeV };

  Gaudi::Property<float> m_pvconstr_D_mass_min{ this, "PVConstrainedDMassMin", 1700 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_pvconstr_D_mass_max{ this, "PVConstrainedDMassMax", 2100 * Gaudi::Units::MeV };

  Gaudi::Property<float> m_phiconstr_d_mass_min{ this, "PhiConstrainedDMassMin", 1805 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_phiconstr_d_mass_max{ this, "PhiConstrainedDMassMax", 2040 * Gaudi::Units::MeV };

  Gaudi::Property<float> m_pvconstr_delta_mass_min{ this, "PVConstrainedDeltaMassMin", 800 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_pvconstr_delta_mass_max{ this, "PVConstrainedDeltaMassMax", 1100 * Gaudi::Units::MeV };

  Gaudi::Property<float> ip_perp_max{ this, "IPperpendicularMax", 0.1 };
  Gaudi::Property<float> ip_max{ this, "ConstrainedIPMax", 0.15 };

  mutable Gaudi::Accumulators::Counter<> m_eventCount{ this, "Events" };
  mutable Gaudi::Accumulators::Counter<> m_candidateCount{ this, "Input Particles" };
  mutable Gaudi::Accumulators::Counter<> m_newpartCounts{ this, "Accepted Particles" };
  mutable Gaudi::Accumulators::Counter<> m_noBPVfound{ this, "Particles without BPV" };

  mutable Gaudi::Accumulators::Counter<> m_msg_fit_failure{ this, "Failed vertex fit" };

  ToolHandle<IParticleCombiner> m_combiner{ this, "ParticleCombiner", "ParticleVertexFitter" };
};

DECLARE_COMPONENT( Velo2Long_Ds2PhiPi_TrackEffFilter )
