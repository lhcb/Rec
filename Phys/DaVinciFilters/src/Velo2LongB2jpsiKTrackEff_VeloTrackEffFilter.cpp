/*****************************************************************************\
* (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "DetDesc/IGeometryInfo.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Kernel/DecayTree.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include <Gaudi/Accumulators.h>
#include <cassert>

namespace {
  using in_pv         = LHCb::RecVertex::Range;
  using Table         = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;
  using in_parts      = LHCb::Particle::Range;
  using out_parts_sel = LHCb::Particle::Selection;
  using out_parts     = LHCb::Particles;
  using out_t         = std::tuple<LHCb::Particles, Table>;

  float impactParameterSquared( const Gaudi::XYZPoint& vertex, const Gaudi::XYZPoint& point,
                                const Gaudi::XYZVector& dir ) {
    // Direction does not need to be normalised.
    const auto distance = point - vertex;
    return ( distance - dir * 1. / dir.Mag2() * ( distance.Dot( dir ) ) ).Mag2();
  }

  std::tuple<const LHCb::RecVertex*, float> bpvAndMinImpactParameterSquared( const in_pv& my_pvs, const int nTracksMin,
                                                                             const Gaudi::XYZPoint&  DV_point,
                                                                             const Gaudi::XYZVector& B_momentum ) {
    // Direction does not need to be normalised.
    float                  minIp2 = -1;
    const LHCb::RecVertex* bestPV = nullptr;

    // The best PV is chosen as the one with respect to which the IP of the B is the smallest.
    for ( const auto& pv : my_pvs ) {
      if ( ( pv->nDoF() + 3 ) > 2 * nTracksMin ) // A minimum number of tracks can be set.
      {
        float ip2 = impactParameterSquared( pv->position(), DV_point, B_momentum );
        if ( !bestPV || ( ip2 < minIp2 ) ) {
          minIp2 = ip2;
          bestPV = pv;
        }
      }
    }
    return { bestPV, minIp2 };
  }

  double transverseMomentumDir( const Gaudi::XYZVector& mom, const Gaudi::XYZVector& dir ) {
    const double dmag2 = dir.Mag2();
    if ( LHCb::essentiallyZero( dmag2 ) ) { return mom.R(); }
    const Gaudi::XYZVector perp = mom - dir * ( mom.Dot( dir ) / dmag2 );
    return perp.R();
  }
} // namespace

class Velo2LongB2jpsiKTrackEff_VeloTrackEffFilter
    : public LHCb::Algorithm::Transformer<out_parts_sel( in_parts const&, in_pv const& )> {
  /**
   * Applies a mass constraint per CERN-THESIS-2019-281 chapter 8,
   * allowing for additional cuts on the Impact Parameter, pT and
   * mass of the combination.
   *
   * Intended use: HLT2 line for velo2long tracking efficiency,
   * including electrons, muons, and hadrons
   *
   * @author Laurent Dufour <laurent.dufour@cern.ch>
   * @author Guillaume Pietrzyk <guillaume.pietrzyk@cern.ch>
   **/
public:
  // ==========================================================================

  Velo2LongB2jpsiKTrackEff_VeloTrackEffFilter( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, { KeyValue{ "Input", "" }, KeyValue{ "InputPVs", "" } },
                     { KeyValue{ "Output", "" } } ) {}

  out_parts_sel operator()( const in_parts& parts, const in_pv& vertices ) const override {
    ++m_eventCount;

    // output
    out_parts_sel parts_out_sel;
    out_parts     parts_out;
    Table         p2p_out;

    m_candidateCount += parts.size();
    for ( const LHCb::Particle* orig_p : parts ) {
      if ( orig_p->daughters().size() < 2 ) continue;

      const LHCb::Particle* orig_jpsi_particle = ( orig_p->daughters() ).at( 0 );

      if ( orig_jpsi_particle->daughters().size() < 2 ) continue;

      const LHCb::Particle*      orig_probe_particle = ( orig_jpsi_particle->daughters() ).at( m_probe_lepton_index );
      const LHCb::Particle*      orig_tag_lepton     = ( orig_jpsi_particle->daughters() ).at( m_tag_lepton_index );
      const Gaudi::LorentzVector total_tag_momentum  = orig_p->momentum() - orig_probe_particle->momentum();

      if ( orig_probe_particle == nullptr || orig_tag_lepton == nullptr ) continue;

      // The absolute momentum of the probe lepton is not known (only its direction is known), so we need to find a
      // scale to rescale its momentum. This scale is obtained with the Jpsi constraint, since we know that the dilepton
      // pair must come from a Jpsi.
      const float Etag   = orig_tag_lepton->momentum().E();
      const float pprobe = sqrt( orig_probe_particle->momentum().Vect().Mag2() );
      const float ptagcostheta =
          orig_tag_lepton->momentum().Vect().Dot( orig_probe_particle->momentum().Vect() ) / pprobe;
      const float DeltaMSquared =
          m_dilepton_mass * m_dilepton_mass - orig_probe_particle->momentum().M2() - orig_tag_lepton->momentum().M2();
      const float my_new_momentum = 0.5 * ( DeltaMSquared ) / ( Etag - ptagcostheta );
      const float scale           = my_new_momentum / pprobe;

      Gaudi::LorentzVector my_probe_momentum = orig_probe_particle->momentum();
      my_probe_momentum.SetPxPyPzE( scale * my_probe_momentum.x(), scale * my_probe_momentum.y(),
                                    scale * my_probe_momentum.z(),
                                    sqrt( my_probe_momentum.M2() + scale * scale * my_probe_momentum.P2() ) );
      Gaudi::LorentzVector my_total_momentum = my_probe_momentum + total_tag_momentum;

      // Using the new probe momentum, we use the derived B momentum to find the Best PV (may have been biased when
      // probe momentum was wrong), and to obtain the IP with respect to this Best PV.

      Gaudi::XYZPoint  DV_point   = orig_p->endVertex()->position();
      Gaudi::XYZVector B_momentum = my_total_momentum.Vect();
      auto [bpv, ip2]             = bpvAndMinImpactParameterSquared( vertices, m_min_pv_tracks, DV_point, B_momentum );
      if ( !bpv ) continue;

      Gaudi::LorentzVector PV_constrained_probe_momentum( my_probe_momentum );

      Gaudi::LorentzVector PV_constrained_total_momentum = PV_constrained_probe_momentum + total_tag_momentum;
      Gaudi::LorentzVector PV_constrained_twobody_momentum =
          PV_constrained_probe_momentum + orig_tag_lepton->momentum();

      Gaudi::XYZPoint bpv_pos = bpv->position();

      Gaudi::XYZVector FD_vector( DV_point.x() - bpv_pos.x(), DV_point.y() - bpv_pos.y(), DV_point.z() - bpv_pos.z() );
      Gaudi::XYZVector FD_vector_jpsi = orig_jpsi_particle->endVertex()->position() - bpv_pos;

      // This is the variable 1.0 - log(1.0 - CosThetaDira), which is effective to separate signal from comb bkg (the
      // greater the more likely it is to be signal).
      float CosThetaDira = ( B_momentum.Dot( FD_vector ) ) / ( sqrt( B_momentum.Mag2() ) * sqrt( FD_vector.Mag2() ) );
      float CosThetaDiraLOG = 1.0 - log( 1.0 - CosThetaDira );

      // Another scale is computed. This time we force the momentum of the B to align with its flight distance
      // (defined as line connecting BPV and B DV). The scale of the probe is obtained satisfying this condition.
      auto       prb_p_PERP   = transverseMomentumDir( my_probe_momentum.Vect(), FD_vector );
      auto       tag_p_perp   = transverseMomentumDir( total_tag_momentum.Vect(), FD_vector );
      const auto scalePVConst = ( tag_p_perp ) / ( prb_p_PERP );
      PV_constrained_probe_momentum.SetPxPyPzE(
          scalePVConst * my_probe_momentum.x(), scalePVConst * my_probe_momentum.y(),
          scalePVConst * my_probe_momentum.z(),
          sqrt( my_probe_momentum.M2() + scalePVConst * scalePVConst * my_probe_momentum.P2() ) );
      PV_constrained_total_momentum   = PV_constrained_probe_momentum + total_tag_momentum;
      PV_constrained_twobody_momentum = PV_constrained_probe_momentum + orig_tag_lepton->momentum();

      // We apply a series of cuts.
      if ( my_total_momentum.M() > m_mass_min && my_total_momentum.M() < m_mass_max &&
           PV_constrained_total_momentum.M() > m_pvconstr_mass_min &&
           PV_constrained_total_momentum.M() < m_pvconstr_mass_max &&
           ( PV_constrained_total_momentum.M() - PV_constrained_twobody_momentum.M() ) > Deltam_pvconstr_mass_min &&
           ( PV_constrained_total_momentum.M() - PV_constrained_twobody_momentum.M() ) < Deltam_pvconstr_mass_max &&
           my_total_momentum.pt() > m_massconstrained_pt_min && ip2 < m_ip_max * m_ip_max &&
           CosThetaDiraLOG > m_costhetadiralog_min && FD_vector.Mag2() > m_fd_min * m_fd_min &&
           FD_vector_jpsi.Mag2() > m_fd_min * m_fd_min && my_probe_momentum.P() > m_probe_p_min &&
           my_probe_momentum.P() < m_probe_p_max && my_probe_momentum.Pt() > m_probe_pt_min

      ) {
        parts_out_sel.push_back( orig_p );
      }
    }
    m_okCount += parts_out_sel.size();
    return parts_out_sel;
  }

private:
  // ==========================================================================
  // cuts
  Gaudi::Property<float> m_dilepton_mass{ this, "MassToConstrainTo", 3096. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_mass_min{ this, "MassConstrainedMassMin", 4600. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_mass_max{ this, "MassConstrainedMassMax", 6000. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_massconstrained_pt_min{ this, "MassConstrainedPtMin", 2500. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_probe_p_min{ this, "ProbeMassConstrainedPMin", 2000. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_probe_p_max{ this, "ProbeMassConstrainedPMax", 150000. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_probe_pt_min{ this, "ProbeMassConstrainedPtMin", 250. * Gaudi::Units::MeV };
  Gaudi::Property<float> m_ip_max{ this, "ConstrainedIPMax", 0.2 * Gaudi::Units::mm };
  Gaudi::Property<float> m_costhetadiralog_min{ this, "ConstrainedDIRAMin",
                                                6.0 }; // Minimum value of 1.0 - log(1.0 - CosDIRA)
  Gaudi::Property<float> m_fd_min{ this, "FDMin", 3.0 * Gaudi::Units::mm };
  Gaudi::Property<float> m_pvconstr_mass_min{ this, "PVConstrainedMassMin", 0 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_pvconstr_mass_max{ this, "PVConstrainedMassMax", 15000 * Gaudi::Units::MeV };
  Gaudi::Property<float> Deltam_pvconstr_mass_min{ this, "PVConstrainedDeltaMassMin", 0 * Gaudi::Units::MeV };
  Gaudi::Property<float> Deltam_pvconstr_mass_max{ this, "PVConstrainedDeltaMassMax", 15000 * Gaudi::Units::MeV };
  Gaudi::Property<int>   m_min_pv_tracks{ this, "MinPVTracks", 0 };

  Gaudi::Property<unsigned int> m_probe_lepton_index{ this, "ProbeLeptonIndex", 1 };
  Gaudi::Property<unsigned int> m_tag_lepton_index{ this, "TagLeptonIndex", 0 };

  // counters
  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{ this, "Events" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_candidateCount{ this, "Input Particles" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_okCount{ this, "Accepted Particles" };

  ToolHandle<IParticleDescendants> m_descendants{ this, "Descendants", "ParticleDescendants" };
};

DECLARE_COMPONENT( Velo2LongB2jpsiKTrackEff_VeloTrackEffFilter )
