###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from Functors.grammar import BoundFunctor
from Gaudi.Configuration import INFO  # type: ignore[import]
from PyConf.Algorithms import WeightedRelTableAlg
from PyConf.dataflow import DataHandle  # type: ignore[import]


class VertexAndConeIsolation:
    """
    Python tool for the configuration of VertexAndConeIsolation class to get the isolation variables.
    The class is instantiated by the 'WeightedRelTableAlg' algorithm.

    Args:
    name (str): name of the isolation algorithm, will be printed in Gaudi.
        reference_particles (DataHandle): TES location of the reference particles
        related_particles (DataHandle): TES location of the related particles
        cut (DataHandle): selection to relate reference_particles with related_particles
        sumcone_invalid_value (optional): Value that allows user flexibility in defining
          the invalid values related to the WeightedRelTable being empty.
          Defaults to `F.NaN`.

    Example:
    from VertexAndConeIsolation import VertexAndConeIsolation

     # Create the tool
     cone_isolation_b = VertexAndConeIsolation(
        name = 'cone_isolation_b',
        reference_particles = b2ksttaumu_data,
        related_particles = b_cciso_data,
        cut = (F.DR<1.)
     )

     vertex_isolation_b = VertexAndConeIsolation(
        name = 'vertex_isolation_b',
        reference_particles = b2ksttaumu_data,
        related_particles = b_extra_one_track_cand,
        cut = (F.CHI2@F.F.FORWARDARG1<9.)
     )

     Note:
     __call__ method is configured by `functor` (BoundFunctor) and `map_functor` (BoundFunctor, optional) arguments
     If `map_functor` argument is specified, it determine how `functor` argument is applied to the relation table:
     `map_functor(Functor=functor, Relations=self.Output)`
     If `map_functor` argument is not specified, `functor` is directly applied to the relation table:
     `functor(Relations=self.Output)`
    """

    def __init__(
        self,
        name: str,
        reference_particles: DataHandle,
        related_particles: DataHandle,
        cut: DataHandle,
        sumcone_invalid_value=F.NaN,
        output_level: int = INFO,
    ) -> None:
        rel_table = WeightedRelTableAlg(
            ReferenceParticles=reference_particles,
            InputCandidates=related_particles,
            Cut=cut,
        )

        self.name = name
        self.sumcone_invalid_value = sumcone_invalid_value
        self.Output = rel_table.OutputRelations

        self.MULT = F.VALUE_OR(0) @ self.__call__(functor=F.MAP_INPUT_SIZE)
        self.CP = F.VALUE_OR(self.sumcone_invalid_value) @ self.__call__(
            functor=F.P, map_functor=F.SUMCONE
        )
        self.CPT = F.VALUE_OR(self.sumcone_invalid_value) @ self.__call__(
            functor=F.PT, map_functor=F.SUMCONE
        )
        self.CPX = F.VALUE_OR(self.sumcone_invalid_value) @ self.__call__(
            functor=F.PX, map_functor=F.SUMCONE
        )
        self.CPY = F.VALUE_OR(self.sumcone_invalid_value) @ self.__call__(
            functor=F.PY, map_functor=F.SUMCONE
        )
        self.CPZ = F.VALUE_OR(self.sumcone_invalid_value) @ self.__call__(
            functor=F.PZ, map_functor=F.SUMCONE
        )

        self.PASY = self.__call__(functor=F.P, map_functor=F.ASYM)
        self.PTASY = self.__call__(functor=F.PT, map_functor=F.ASYM)
        self.PXASY = self.__call__(functor=F.PX, map_functor=F.ASYM)
        self.PYASY = self.__call__(functor=F.PY, map_functor=F.ASYM)
        self.PZASY = self.__call__(functor=F.PZ, map_functor=F.ASYM)

        delta_eta = self._get_delta_functor(functor=F.ETA)
        delta_phi = F.ADJUST_ANGLE @ self._get_delta_functor(functor=F.PHI)

        self.DETA = self._apply_map_functor(delta_eta)
        self.DPHI = self._apply_map_functor(delta_phi)
        self.MAXCP = self.__call__(functor=F.P, map_functor=F.MAXCONE)
        self.MAXCPT = self.__call__(functor=F.PT, map_functor=F.MAXCONE)
        self.MINCP = self.__call__(functor=F.P, map_functor=F.MINCONE)
        self.MINCPT = self.__call__(functor=F.PT, map_functor=F.MINCONE)

        self.Smallest_CHI2 = F.MIN_ELEMENT @ self.__call__(
            functor=F.CHI2, map_functor=F.MAP_INPUT_ARRAY
        )
        self.Smallest_DELTACHI2 = self.__call__(functor=F.SMALLEST_DELTACHI2)
        self.Smallest_Mass_DELTACHI2 = self.__call__(
            functor=F.MASS, map_functor=F.PARTICLE_PROPERTY_WITH_SMALLEST_DELTACHI2
        )

    def _get_delta_functor(self, functor: BoundFunctor) -> BoundFunctor:
        """
        Internal function that applies the functor to related and
        reference particles and retrieves the delta value between them.

        Args:
           functor (BoundFunctor): the functor to be applied
        """
        ref_func = functor @ F.FORWARDARG1
        rel_func = functor @ F.TO @ F.FORWARDARG0
        return rel_func - ref_func

    def _apply_map_functor(self, functor: BoundFunctor):
        """
        Internal function that maps functor to certain relation.

        Args:
           functor (BoundFunctor): the functor to be applied
        """
        # get the set of relations from the relation table stored in the TES location
        RELS = F.RELATIONS.bind(F.TES(self.Output), F.FORWARDARGS)

        return F.MAP(functor).bind(RELS, F.FORWARDARGS)

    def __call__(self, functor: BoundFunctor, map_functor: BoundFunctor = None):
        """
        Retrieve the variable information applying the input functor in different ways
        (specified by the `map_functor` argument) to the relations table.
        If no map_functor is specified the functor itself is applied to the relation.

        Args:
           functor (BoundFunctor): the functor to be applied
           map_functor (BoundFunctor): way to apply functor to the relation table.
           If `map_functor` is not specified, `functor` is applied directly to the relation table
        """

        if map_functor:
            IsolationVariable_Functor = map_functor(
                Functor=functor, Relations=self.Output
            )
        else:
            IsolationVariable_Functor = functor(Relations=self.Output)
        return IsolationVariable_Functor
