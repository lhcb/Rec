/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Event/PrHits.h"
#include "Kernel/HashIDs.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Transformer.h"

/**** @ class MuonProbeToLongMatcher
 *
 *  Function to match the probe daughter of Jpsi composite to a long track
 *  by comparing the hits in sub-detector
 *  Needed in the study of Tracking efficiency
 *
 *
 */

class MuonProbeToLongMatcher
    : public LHCb::Algorithm::MultiTransformerFilter<std::tuple<LHCb::Particle::Selection, LHCb::Particle::Selection>(
          LHCb::Particle::Range const&, LHCb::Particle::Range const&, MuonHitContainer const& )> {

public:
  MuonProbeToLongMatcher( const std::string& name, ISvcLocator* pSvcLocator );

  std::tuple<bool, LHCb::Particle::Selection, LHCb::Particle::Selection>
  operator()( LHCb::Particle::Range const& composites, LHCb::Particle::Range const& longparts,
              MuonHitContainer const& ) const override;

private:
  void addNeighbouringMuonIDs( std::vector<LHCb::LHCbID>& muonIDs, const MuonHitContainer& muonHits ) const;

  // properties are members of the class
  // need to specify owner, name, default value, doc
  Gaudi::Property<double> m_thresMuon{ this, "MinMuonFrac", 0.4 }; // overlap threshold of Muon stations
  Gaudi::Property<double> m_thresVP{ this, "MinVPFrac", 0.4 };     // overlap threshold of VP
  Gaudi::Property<double> m_thresUT{ this, "MinUTFrac", 0.4 };     // overlap threshold of UT
  Gaudi::Property<double> m_thresFT{ this, "MinFTFrac", 0.4 };     // overlap threshold of FT

  Gaudi::Property<bool> p_checkMuon{ this, "checkMuon", false };
  Gaudi::Property<bool> p_checkVP{ this, "checkVP", false };
  Gaudi::Property<bool> p_checkUT{ this, "checkUT", false };
  Gaudi::Property<bool> p_checkFT{ this, "checkFT", false };

  Gaudi::Property<bool> p_addNeighbouringMuonHits{ this, "addNeighbouringMuonHits", false };

  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_all{ this,
                                                                "#Input composites" }; // Number of JPsi candidates
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_matched{
      this, "#Matched composites" }; // Number of JPsi candidates with a matched probe track
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_no_track{
      this, "Charged ProtoParticle with no Track found. This is likely to be a bug." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_state{
      this, "Track has empty states. This is likely to be a bug." };
};

DECLARE_COMPONENT( MuonProbeToLongMatcher )

// Implementation
MuonProbeToLongMatcher::MuonProbeToLongMatcher( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformerFilter(
          name, pSvcLocator,
          { KeyValue{ "TwoBodyComposites", "" }, KeyValue{ "LongTracks", "" }, KeyValue{ "MuonHitContainer", "" } },
          { KeyValue{ "MatchedComposites", "" }, KeyValue{ "MatchedLongTracks", "" } } ) {}

void MuonProbeToLongMatcher::addNeighbouringMuonIDs( std::vector<LHCb::LHCbID>& muonIDs,
                                                     const MuonHitContainer&    muonHits ) const {

  const auto muonIDsToLoop = muonIDs;

  for ( auto id : muonIDsToLoop ) {
    auto       tileID  = id.muonID();
    const auto station = tileID.station();
    auto       hits    = muonHits.hits( station );
    for ( auto const& hit : hits ) {
      // -- Add hits which are identical in X but neighbouring in Y, or vide versa
      if ( ( (int)hit.tile().nX() == (int)tileID.nX() && std::abs( (int)hit.tile().nY() - (int)tileID.nY() ) == 1 ) ||
           ( (int)hit.tile().nY() == (int)tileID.nY() && std::abs( (int)hit.tile().nX() - (int)tileID.nX() ) == 1 ) ) {
        muonIDs.push_back( LHCb::LHCbID( hit.tile() ) );
      }
    }
  }

  std::sort( muonIDs.begin(), muonIDs.end() );
}

std::tuple<bool, LHCb::Particle::Selection, LHCb::Particle::Selection>
MuonProbeToLongMatcher::operator() // we use a mulitransformer to define multiple outputs
    ( LHCb::Particle::Range const& composites, LHCb::Particle::Range const& longparts,
      MuonHitContainer const& muonhits ) const {

  // initialize counter
  m_all += composites.size();

  // output containers
  auto results = std::tuple<bool, LHCb::Particle::Selection, LHCb::Particle::Selection>{};
  auto& [filter_passed, selectedComp, selectedLong] = results;

  for ( const auto composite : composites ) {

    for ( const auto& daughter : composite->daughters() ) {

      const LHCb::ProtoParticle* proto      = daughter->proto();
      const LHCb::Track*         track      = proto->track();
      const auto                 lhcbids    = track->lhcbIDs();
      const auto                 tagmuonpid = proto->muonPID();
      std::vector<LHCb::LHCbID>  tagmuonids;
      tagmuonids.reserve( 10 );
      LHCb::HashIDs::lhcbIDs( tagmuonpid, tagmuonids );

      // Sanity check from Particle_to_Particle.cpp
      if ( proto == nullptr ) {
        ++m_no_track;
        continue;
      }
      if ( track->states().empty() ) {
        ++m_no_state;
        continue;
      }

      /// Remove tag tracks from the daughters
      switch ( track->type() ) {
      case LHCb::Event::Enum::Track::Type::SeedMuon:
      case LHCb::Event::Enum::Track::Type::VeloMuon:
      case LHCb::Event::Enum::Track::Type::MuonUT:
      case LHCb::Event::Enum::Track::Type::Downstream:
        break;
      default:
        continue;
      }

      const LHCb::Particle* tmp_long = nullptr;
      double                tmp_frac = 0;

      for ( const auto longpart : longparts ) {

        if ( daughter->charge() != longpart->charge() ) continue;

        // define LHCbIDs and muonPID for the long track
        const LHCb::ProtoParticle* longproto   = longpart->proto();
        const LHCb::Track*         longtrack   = longproto->track();
        const auto                 longIDs     = longtrack->lhcbIDs();
        const auto                 longmuonpid = longproto->muonPID();
        std::vector<LHCb::LHCbID>  longmuonids;
        longmuonids.reserve( 10 );
        LHCb::HashIDs::lhcbIDs( longmuonpid, longmuonids );
        if ( p_addNeighbouringMuonHits ) addNeighbouringMuonIDs( longmuonids, muonhits );

        // evaluate the overlap
        std::pair<double, double> fracVP( 0., 0. ), fracUT( 0., 0. ), fracFT( 0., 0. ), fracMuon( 0., 0. );

        if ( p_checkVP ) fracVP = LHCb::HashIDs::overlap( lhcbids, longIDs, LHCb::LHCbID::channelIDtype::VP );
        if ( p_checkUT ) fracUT = LHCb::HashIDs::overlap( lhcbids, longIDs, LHCb::LHCbID::channelIDtype::UT );
        if ( p_checkFT ) fracFT = LHCb::HashIDs::overlap( lhcbids, longIDs, LHCb::LHCbID::channelIDtype::FT );
        if ( p_checkMuon ) {
          if ( tagmuonids.size() == 0 )
            fracMuon = LHCb::HashIDs::overlap( lhcbids, longmuonids, LHCb::LHCbID::channelIDtype::Muon );
          else
            fracMuon = LHCb::HashIDs::overlap( tagmuonids, longmuonids, LHCb::LHCbID::channelIDtype::Muon );
        }
        if ( p_checkVP ) debug() << "match fraction of VP hits " << fracVP.first << " " << fracVP.second << endmsg;
        if ( p_checkUT ) debug() << "match fraction of UT hits " << fracUT.first << " " << fracUT.second << endmsg;
        if ( p_checkFT ) debug() << "match fraction of FT hits " << fracFT.first << " " << fracFT.second << endmsg;
        if ( p_checkMuon )
          debug() << "match fraction of muon hits " << fracMuon.first << " " << fracMuon.second << endmsg;

        if ( p_checkVP && fracVP.first < m_thresVP.value() ) continue;
        if ( p_checkUT && fracUT.first < m_thresUT.value() ) continue;
        if ( p_checkFT && fracFT.first < m_thresFT.value() ) continue;
        if ( p_checkMuon && fracMuon.first < m_thresMuon.value() ) continue;

        double frac = fracVP.first + fracUT.first + fracFT.first + fracMuon.first;

        if ( tmp_frac < frac ) {
          tmp_frac = frac;
          tmp_long = longpart;
        }
      } // end loop over long tracks

      if ( tmp_frac > 0 ) {
        selectedComp.insert( composite );
        selectedLong.insert( tmp_long );
      }
    } // end loop over daughters
  }   // end loop over composites

  filter_passed = selectedComp.size() > 0;
  m_matched += selectedComp.size();
  return results;
}
