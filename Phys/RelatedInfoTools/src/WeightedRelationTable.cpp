/***************************************************************************** \
 * (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files
#include "Event/Particle.h"
#include "Functors/with_functors.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

namespace {
  using WeightedRelationTable = LHCb::RelationWeighted1D<LHCb::Particle, LHCb::Particle, float>;
  using RelationTable         = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;

  using RelTableTransformer =
      LHCb::Algorithm::Transformer<RelationTable( const LHCb::Particle::Range&, const LHCb::Particle::Range& )>;
  using SelectionTransformer = LHCb::Algorithm::Transformer<LHCb::Particle::Selection( const RelationTable& )>;
  using Transformer          = LHCb::Algorithm::Transformer<LHCb::Particle::Selection( const LHCb::Particle::Range& )>;

  using WeightedSelectionTransformer =
      LHCb::Algorithm::Transformer<LHCb::Particle::Selection( const WeightedRelationTable& )>;

  using weight_t = bool;

  struct PredFunct {
    constexpr static auto PropertyName = "Cut";
    using Signature                    = weight_t( LHCb::Particle const&, LHCb::Particle const& );
  };

  struct Funct {
    constexpr static auto PropertyName = "Functor";
    using Signature                    = const LHCb::Particle::ConstVector( LHCb::Particle const& );
  };
  const SmartRefVector<LHCb::CaloHypo> s_empty_calohypos{};
} // namespace

// ============================================================================
/** @class WeightedRelTableAlg
 *  Algorithm that looks for candidates in the event and stores the output table
 *  Unlike the name, it does not store a weight!
 *
 *  @param InputCandidates Location of extra particles
 *  @param ReferenceParticles Location of reference particles
 *  @param Cut Predicate that filters the table
 *  @returns OutputRelations location of relation table between in and out particles
 *
 *  Use:
 *   @code
 *     import Functors as F
 *     from PyConf.Algorithms import WeightedRelTableAlg
 *
 *     #Definition of dielectron container
 *
 *     isoAlg = WeightedRelTableAlg(InputCandidates=make_photons(), ReferenceParticles=dielectrons,
 * Cut=F.DR2()<0.4)
 *     isoAlgRels = isoAlg.OutputRelations  # Relations functor
 *   @endcode
 *
 */

// This algorithm includes GetPhotonsForDalitzDecay

struct WeightedRelTableAlg final : with_functors<RelTableTransformer, PredFunct> {
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */
  WeightedRelTableAlg( const std::string& name, ISvcLocator* pSvc )
      : with_functors<RelTableTransformer, PredFunct>::with_functors(
            name, pSvc, { KeyValue{ "InputCandidates", "" }, KeyValue{ "ReferenceParticles", "" } },
            KeyValue{ "OutputRelations", "" } ) {}

  // ==========================================================================
  /// the standard execution of the algorithm
  // ==========================================================================

  RelationTable operator()( const LHCb::Particle::Range& cands_in_cone,
                            const LHCb::Particle::Range& ref_parts ) const override {

    RelationTable iso_table;

    auto const& fun = this->template getFunctor<PredFunct>();
    for ( const auto& cand : cands_in_cone ) {
      for ( const auto& ref_part : ref_parts ) {
        const weight_t& weight = fun( *ref_part, *cand );
        if ( weight ) {
          iso_table.relate( ref_part, cand ).ignore();
          ++m_outCount;
        }
      }
    }
    m_inCount += cands_in_cone.size();
    m_refCount += ref_parts.size();

    return iso_table;
  }

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{ this, "#InputCandidates" };
  mutable Gaudi::Accumulators::SummingCounter<> m_refCount{ this, "#InputParticles" };
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{ this, "#OutputParticles" };
};

struct Projection {
  LHCb::Particle const* p;

  // the following is not needed for C++20
  // as it supports aggregate constructors for
  // emplace_back
#if __cplusplus < 202002
  Projection( LHCb::Particle const* particle ) : p( particle ) {}
#endif

  LHCb::Track const* track() const { return ( p && p->proto() ) ? p->proto()->track() : nullptr; }

  LHCb::NeutralPID const* neutralPID() const { return ( p && p->proto() ) ? p->proto()->neutralPID() : nullptr; }

  int pid() const { return p->particleID().pid(); }

  SmartRefVector<LHCb::CaloHypo> const& calo() const {
    return ( p && p->proto() ) ? p->proto()->calo() : s_empty_calohypos;
  }

  bool operator<( Projection const& rhs ) const {
    auto const* lhs_t = track();
    auto const* rhs_t = rhs.track();

    if ( lhs_t && rhs_t ) {
      // track-track
      const auto less = Relations::ObjectTypeTraits<LHCb::Track>::Less{};
      return less( *lhs_t, *rhs_t );
    }

    if ( !lhs_t && !rhs_t ) {
      // calo-calo
      return std::lexicographical_compare( calo().begin(), calo().end(), rhs.calo().begin(), rhs.calo().end(),
                                           Relations::ObjectTypeTraits<LHCb::CaloHypo>::Less{} );
    }

    // calo-track or track-calo -- in which case we order tracks first
    return lhs_t != nullptr;
  }
};

namespace {
  LHCb::Particle::Selection selectionFromProjectionVector( std::vector<Projection>& wparts ) {
    std::sort( wparts.begin(), wparts.end() );
    auto end = std::unique( wparts.begin(), wparts.end(), []( const auto& lhs, const auto& rhs ) {
      return ( ( lhs.track() && ( lhs.track() == rhs.track() ) ) ||
               ( lhs.neutralPID() && ( lhs.neutralPID() == rhs.neutralPID() ) ) ) &&
             lhs.pid() == rhs.pid();
    } );

    LHCb::Particle::Selection output;
    std::transform( wparts.begin(), end, std::back_inserter( output ), []( const auto& p ) { return p.p; } );

    return output;
  }
} // namespace

// ============================================================================
/** @class SelectionFromRelationTable
 *  Algorithm that gives a selection of particles in the TO side of a relation table
 *
 *  @param InputRelations Location of Relation Table
 *  @returns OutputLocation Location of selection of particles in the TO side of the relation table
 *
 */
struct SelectionFromRelationTable final : SelectionTransformer {
  // ==========================================================================
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */

  SelectionFromRelationTable( const std::string& name, ISvcLocator* svcLoc )
      : Transformer( name, svcLoc, KeyValue( "InputRelations", "" ), KeyValue( "OutputLocation", "" ) ) {}

  LHCb::Particle::Selection operator()( const RelationTable& input ) const override {

    std::vector<Projection> wparts;
    const auto&             rels = input.relations();
    wparts.reserve( rels.size() );
    std::transform( rels.begin(), rels.end(), std::back_inserter( wparts ),
                    []( const auto& r ) { return Projection{ r.to() }; } );

    return selectionFromProjectionVector( wparts );
  }
};

// ============================================================================
/** @class SelectionFromWeightedRelationTable
 *  Algorithm that gives a selection of particles in the TO side of a relation table,
 *  with the option to cut on the minimum weight.
 *
 *  @param InputRelations Location of *weighted* Relation Table
 *  @returns OutputLocation Location of selection of particles in the TO side of the relation table
 *
 */
struct SelectionFromWeightedRelationTable final : WeightedSelectionTransformer {
  SelectionFromWeightedRelationTable( const std::string& name, ISvcLocator* svcLoc )
      : Transformer( name, svcLoc, KeyValue( "InputRelations", "" ), KeyValue( "OutputLocation", "" ) ) {}

  LHCb::Particle::Selection operator()( const WeightedRelationTable& input ) const override {

    std::vector<Projection> wparts;
    const auto&             rels = input.relations();
    wparts.reserve( rels.size() );

    // possibly some transform_if would be useful, but this is probably fine.
    for ( const auto& rel : rels ) {
      if ( rel.weight() > m_minWeight.value() && rel.weight() < m_maxWeight.value() ) wparts.emplace_back( rel.to() );
    }

    return selectionFromProjectionVector( wparts );
  }

  Gaudi::Property<float> m_minWeight{ this, "MinimumWeight", -1.0, "Minimum Weight" };
  Gaudi::Property<float> m_maxWeight{ this, "MaximumWeight", 1000.0, "Maximum Weight" };
};

// ============================================================================
/** @class ThOrParticleSelection
 *  Algorithm that returns a selection as output of the functor's call
 *
 *  @param InputParticles Location of particles
 *  @param Functor Functor that retrieves the selection of particles
 *  @returns OutputSelection Location of particles as output of the functor
 *
 */
struct ThOrParticleSelection : with_functors<Transformer, Funct> {
  // ==========================================================================
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */
  ThOrParticleSelection( const std::string& name, ISvcLocator* pSvc )
      : with_functors<Transformer, Funct>::with_functors( name, pSvc, KeyValue{ "InputParticles", "" },
                                                          KeyValue{ "OutputSelection", "" } ) {}
  // ==========================================================================
  /// the standard execution of the algorithm
  // ==========================================================================

  LHCb::Particle::Selection operator()( const LHCb::Particle::Range& input ) const override {

    LHCb::Particle::Selection vec;

    auto const& fun = this->template getFunctor<Funct>();
    for ( const auto& p : input ) {
      const LHCb::Particle::ConstVector parts = fun( *p );
      for ( const auto& part : parts ) { vec.push_back( part ); }
    }
    m_outCount += vec.size();

    return vec;
  }

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{ this, "#OutputParticles" };
};

DECLARE_COMPONENT( WeightedRelTableAlg )
DECLARE_COMPONENT( SelectionFromRelationTable )
DECLARE_COMPONENT( SelectionFromWeightedRelationTable )
DECLARE_COMPONENT( ThOrParticleSelection )
