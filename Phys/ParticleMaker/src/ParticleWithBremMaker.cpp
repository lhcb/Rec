/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/IDetectorElement.h"
#include "Event/Particle.h"
#include "Kernel/ISelectiveBremAdder.h"
#include "LHCbAlgs/Transformer.h"

/**
 * @brief LHCb::Particle creation implementing pre-determined brem recovery
 * The SelectiveBremAdder tool is used to use pre-determined brem recovery
 * and correct the particle momentum.
 *
 * Example usage:
 *
 * @code {.py}
 *
 * ParticleWithBremMaker(
 *     InputParticles=electrons)
 *
 * @endcode
 *
 */

// Declarations
class ParticleWithBremMaker : public LHCb::Algorithm::Transformer<LHCb::Particles( LHCb::Particle::Range const& )> {

public:
  ParticleWithBremMaker( const std::string& name, ISvcLocator* pSvcLocator );

  LHCb::Particles operator()( LHCb::Particle::Range const& particles ) const override;

private:
  /// Tool to add brem
  ToolHandle<ISelectiveBremAdder> m_bremAdder{ this, "BremAdder", "SelectiveBremAdder" };

  /// Number of created LHCb::Particle objects
  mutable Gaudi::Accumulators::Counter<> m_nbrem{
      this, "Nb of particles corrected with brem (including those without change)" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_hasBrem{ this, "Input particle has already brem added" };
};

DECLARE_COMPONENT( ParticleWithBremMaker )

// Implementation
ParticleWithBremMaker::ParticleWithBremMaker( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, { KeyValue{ "InputParticles", "" } }, { KeyValue{ "Particles", "" } } ) {}

LHCb::Particles ParticleWithBremMaker::operator()( LHCb::Particle::Range const& inparts ) const {

  // output container
  LHCb::Particles outparts;
  outparts.reserve( inparts.size() );

  // loop over input particles
  auto nbrem = m_nbrem.buffer();
  for ( const auto* inp : inparts ) {
    if ( !inp ) continue;
    // apply only to particles that have a track
    if ( !inp->proto() || !inp->proto()->track() ) continue;
    // check that brem has not been added already
    if ( inp->info( LHCb::Particle::additionalInfo::HasBremAdded, 0. ) > 0. ) {
      ++m_hasBrem;
      continue;
    }

    auto newpart = std::make_unique<LHCb::Particle>( *inp );
    auto success = m_bremAdder->addBrem( *newpart );
    outparts.add( newpart.release() ); // cp particle even if brem was not found
    if ( success ) nbrem++;
  }
  return outparts;
}
