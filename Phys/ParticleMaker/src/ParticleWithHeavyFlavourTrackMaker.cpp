/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation2D.h"

namespace {
  using Track          = LHCb::Event::v1::Track;
  using RelationTable  = LHCb::Relation2D<LHCb::Particle, Track>;
  using Output         = std::tuple<LHCb::Particles, LHCb::ProtoParticles>;
  using InvertedOutput = std::tuple<RelationTable, LHCb::Particle::Selection>;
} // namespace

// main algorithm
class ParticleWithHeavyFlavourTrackMaker
    : public LHCb::Algorithm::MultiTransformer<Output( LHCb::Particle::Range const&, RelationTable const& )> {
public:
  ParticleWithHeavyFlavourTrackMaker( const std::string&, ISvcLocator* );

  StatusCode initialize() override;

  Output operator()( LHCb::Particle::Range const&, RelationTable const& ) const override;

private:
  // properties
  Gaudi::Property<std::string> m_pid{ this, "ParticleID", "B+" };

  // particle property information
  LHCb::ParticleProperty const* m_partprop     = nullptr;
  LHCb::ParticleProperty const* m_antipartprop = nullptr;

  // services
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };
};

// include also algorithm to decode Particle into relation table again (for use in functors etc..)
class HeavyFlavourTrackRelationTableMaker
    : public LHCb::Algorithm::MultiTransformer<InvertedOutput( LHCb::Particle::Range const& )> {
public:
  HeavyFlavourTrackRelationTableMaker( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator, { KeyValue{ "ParticlesWithHeavyFlavourTracks", "" } },
                          { KeyValue{ "OutputTable", "" }, KeyValue{ "OutputComposites", "" } } ){};

  InvertedOutput operator()( LHCb::Particle::Range const& particles ) const override {
    auto result               = InvertedOutput{};
    auto& [table, composites] = result;
    for ( LHCb::Particle const* particle : particles ) {
      if ( !particle->proto() ) continue;
      auto daughters = particle->daughters();
      if ( !daughters.size() ) continue;
      LHCb::Particle const* composite = daughters.front();
      composites.insert( composite );
      LHCb::Track const* hf_track = particle->proto()->track();
      if ( !hf_track ) continue;
      table.i_push( composite, hf_track );
    }
    table.i_sort();
    return result;
  };
};

ParticleWithHeavyFlavourTrackMaker::ParticleWithHeavyFlavourTrackMaker( const std::string& name,
                                                                        ISvcLocator*       pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        { KeyValue{ "InputComposites", "" }, KeyValue{ "Composite2TrackRelations", "" } },
                        { KeyValue{ "OutputParticles", "" }, KeyValue{ "OutputProtoParticles", "" } } ) {}

StatusCode ParticleWithHeavyFlavourTrackMaker::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    // Get particle properties from ParticlePropertySvc
    m_partprop = m_particlePropertySvc->find( m_pid );
    if ( !m_partprop ) {
      throw GaudiException( "Could not find ParticleProperty for " + m_pid.value(),
                            "ParticleWithHeavyFlavourTrackMaker::initialize", StatusCode::FAILURE );
    }
    m_antipartprop = m_partprop->antiParticle();
    return StatusCode::SUCCESS;
  } );
}

Output ParticleWithHeavyFlavourTrackMaker::operator()( LHCb::Particle::Range const& in_particles,
                                                       RelationTable const&         relation_table ) const {
  auto result                       = Output{};
  auto& [out_particles, out_protos] = result;

  for ( auto const* in_particle : in_particles ) {
    // heavy flavour track
    auto         track_range = relation_table.relations( in_particle );
    Track const* track       = track_range.empty() ? NULL : track_range.front().to();

    // create proto from heavy flavour track
    auto* out_proto = new LHCb::ProtoParticle();
    if ( track ) out_proto->setTrack( track );
    out_protos.add( out_proto );

    // create new particle containing proto/track and daughters (in_particle)
    auto* out_particle = new LHCb::Particle(
        ( out_proto->charge() == m_partprop->charge() ? m_partprop : m_antipartprop )->particleID() );
    out_particle->setProto( out_proto );

    // add information from daughter composite
    out_particle->addToDaughters( in_particle );

    out_particle->setEndVertex( in_particle->endVertex() );
    out_particle->setReferencePoint( in_particle->referencePoint() );

    out_particle->setMomentum( in_particle->momentum() );
    out_particle->setMeasuredMass( in_particle->measuredMass() );

    out_particle->setPosCovMatrix( in_particle->posCovMatrix() );
    out_particle->setMomCovMatrix( in_particle->momCovMatrix() );
    out_particle->setPosMomCovMatrix( in_particle->posMomCovMatrix() );

    out_particle->setPV( in_particle->pv() );

    out_particles.add( out_particle );
  }

  return result;
}

DECLARE_COMPONENT_WITH_ID( ParticleWithHeavyFlavourTrackMaker, "ParticleWithHeavyFlavourTrackMaker" )
DECLARE_COMPONENT_WITH_ID( HeavyFlavourTrackRelationTableMaker, "HeavyFlavourTrackRelationTableMaker" )
