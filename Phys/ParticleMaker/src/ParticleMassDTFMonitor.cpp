/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DecayTreeFitter/Fitter.h"
#include "DetDesc/DetectorElement.h"
#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/StaticHistogram.h>

#include <optional>

using base_type = LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range&, DetectorElement const& ),
                                            LHCb::Algorithm::Traits::usesConditions<DetectorElement>>;

class ParticleMassDTFMonitor : public base_type {

public:
  ParticleMassDTFMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : base_type{ name,
                   pSvcLocator,
                   { KeyValue{ "Particles", "" }, KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } } } {}

  StatusCode initialize() override {
    return base_type::initialize().andThen( [&] {
      m_histo.emplace( this, m_histoName, m_histoName,
                       Gaudi::Accumulators::Axis<double>{ m_bins, m_mean - 20 * m_sigma, m_mean + 20 * m_sigma } );
    } );
  }

  void operator()( LHCb::Particle::Range const& particles, DetectorElement const& lhcb ) const override {
    // loop over particles
    auto b_mass = m_mass.buffer();
    auto b_1sig = m_n1sigma.buffer();
    auto b_ndtf = m_ndtf.buffer();
    for ( const auto& p : particles ) {
      auto m = p->momentum().M();
      if ( m_usedtf ) {
        auto fitter = DecayTreeFitter::Fitter( *p, m_stateprovider );
        for ( const auto& constr : m_mass_constraints ) {
          LHCb::ParticleProperty const* p_prop = m_particlePropertySvc->find( constr );
          fitter.setMassConstraint( p_prop->particleID() );
        }
        fitter.fit( *lhcb.geometry() );
        if ( fitter.status() == DecayTreeFitter::Fitter::FitStatus::Success ) {
          b_ndtf++;
          auto fitted_tree = fitter.getFittedTree();
          m                = fitted_tree.head()->momentum().M();
        }
      }
      b_mass += m;

      // fill histo with 5sigma range on each side of mean
      ++( *m_histo )[m];

      // count candidates in 1 sigma
      if ( std::abs( m_mean - m ) / m_sigma < 1 ) b_1sig++;
    }
  }

private:
  // properties
  Gaudi::Property<unsigned int>             m_bins{ this, "Bins", 20 };
  Gaudi::Property<float>                    m_mean{ this, "MassMean", 5279.65 * Gaudi::Units::MeV };
  Gaudi::Property<float>                    m_sigma{ this, "MassSigma", 50 * Gaudi::Units::MeV };
  Gaudi::Property<std::string>              m_histoName{ this, "histoName", "Invariant Mass" };
  Gaudi::Property<bool>                     m_usedtf{ this, "UseDTF", true, "Use DecayTreeFitter" };
  Gaudi::Property<std::vector<std::string>> m_mass_constraints{ this, "MassConstraints" };
  // counters
  mutable Gaudi::Accumulators::StatCounter<double> m_mass{ this, "Candidate mass" };
  mutable Gaudi::Accumulators::Counter<>           m_n1sigma{ this, "Candidates in 1 sigma" };
  mutable Gaudi::Accumulators::Counter<>           m_ndtf{ this, "Candidates with succesful DTF fit" };
  // histograms
  mutable std::optional<Gaudi::Accumulators::StaticHistogram<1>> m_histo{};
  // property service
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };
  // tools
  ToolHandle<ITrackStateProvider> m_stateprovider{ this, "StateProvider", "TrackStateProvider" };
};

DECLARE_COMPONENT( ParticleMassDTFMonitor )
