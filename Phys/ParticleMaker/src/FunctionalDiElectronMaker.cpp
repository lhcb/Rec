/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/IParticleCombiner.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IParticleTransporter.h"
#include "Kernel/ISelectiveBremAdder.h"
#include "Kernel/ParticleProperty.h"

#include "Core/FloatComparison.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/Particle.h"
#include "TrackKernel/PrimaryVertexUtils.h"

#include "LHCbAlgs/Transformer.h"

/**
 *  functional maker to produce dielectron candidates with bremsstrahlung
 *  correction, avoiding overlap
 *
 *  @author Carla Marin
 *  based on DiElectronMaker from Olivier Deschamps
 *  @date   2020-11-18
 */

class FunctionalDiElectronMaker
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::Particles, LHCb::Vertices, LHCb::Particles>(
                                                   LHCb::Particle::Range const&, LHCb::PrimaryVertices const&,
                                                   DetectorElement const& ),
                                               LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {

public:
  FunctionalDiElectronMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  std::tuple<LHCb::Particles, LHCb::Vertices, LHCb::Particles> operator()( LHCb::Particle::Range const& electrons,
                                                                           LHCb::PrimaryVertices const& pvs,
                                                                           DetectorElement const& lhcb ) const override;

private:
  StatusCode combinepair( LHCb::Particle& ele1, LHCb::Particle& ele2, LHCb::Particle& parent, LHCb::Vertex& vertex,
                          IGeometryInfo const& geometry ) const;

  bool hasBrem( LHCb::Particle const& el ) const;

  // properties
  Gaudi::Property<float>       m_ptmin{ this, "MinDiElecPT", 500 * Gaudi::Units::MeV };
  Gaudi::Property<bool>        m_oppSign{ this, "OppositeSign", true };
  Gaudi::Property<float>       m_minM{ this, "MinDiElecMass", 0. * Gaudi::Units::MeV };
  Gaudi::Property<float>       m_maxM{ this, "MaxDiElecMass", 200. * Gaudi::Units::MeV };
  Gaudi::Property<float>       m_aFactor{ this, "CombMassFactor", 1.5 };
  Gaudi::Property<std::string> m_pid{ this, "DiElecID", "gamma" };
  Gaudi::Property<float>       m_maxdistpair{ this, "DistMaxpair", 10 * Gaudi::Units::mm };

  // Particle property information
  const LHCb::ParticleProperty*             m_particle_prop = nullptr;
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };

  // tools
  ToolHandle<IParticleCombiner>    m_combiner{ this, "ParticleCombiner", "ParticleVertexFitter" };
  ToolHandle<ISelectiveBremAdder>  m_bremAdder{ this, "BremAdder", "SelectiveBremAdder" };
  ToolHandle<IParticleTransporter> m_transporter{ this, "ParticleTransporter", "ParticleTransporter" };

  // counters
  mutable Gaudi::Accumulators::BinomialCounter<>         m_npassed{ this, "# passed" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nBremCorr{ this, "Corrected electron pairs" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nDiElec{ this, "DiElectrons selected" };
  mutable Gaudi::Accumulators::Counter<>                 m_nCombFail{ this, "Failed combinations" };
  mutable Gaudi::Accumulators::Counter<>                 m_nComb2Fail{ this, "Failed combinations 2nd try" };
  mutable Gaudi::Accumulators::Counter<>                 m_nComb3Fail{ this, "Failed combinations 3rd try" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>  m_transFail{ this, "Transporter returned failure" };
};

DECLARE_COMPONENT( FunctionalDiElectronMaker )

// Implementation
FunctionalDiElectronMaker::FunctionalDiElectronMaker( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        { KeyValue{ "InputParticles", "" }, KeyValue{ "PrimaryVertices", "" },
                          KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                        {
                            KeyValue{ "Particles", "" },
                            KeyValue{ "OutputVertices", "" },
                            KeyValue{ "OutputChildren", "" },
                        } ) {}

StatusCode FunctionalDiElectronMaker::initialize() {
  return MultiTransformer::initialize().andThen( [&] {
    // Get particle properties from ParticlePropertySvc
    m_particle_prop = m_particlePropertySvc->find( m_pid );
    if ( !m_particle_prop ) {
      throw GaudiException( "Could not find ParticleProperty for " + m_pid.value(),
                            "FunctionalDiElectronMaker::initialize", StatusCode::FAILURE );
    }
    return StatusCode::SUCCESS;
  } );
}

std::tuple<LHCb::Particles, LHCb::Vertices, LHCb::Particles>
FunctionalDiElectronMaker::operator()( LHCb::Particle::Range const& electrons, LHCb::PrimaryVertices const& pvs,
                                       DetectorElement const& lhcb ) const {

  // output containers
  auto result                             = std::tuple<LHCb::Particles, LHCb::Vertices, LHCb::Particles>{};
  auto& [diElectrons, vertices, children] = result;
  diElectrons.reserve( electrons.size() * electrons.size() ); // upper limit
  vertices.reserve( diElectrons.size() );
  children.reserve( diElectrons.size() * 2 );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "FunctionalDiElectronMaker: start loop on input electrons" << endmsg;
    debug() << "FunctionalDiElectronMaker: " << electrons.size() << endmsg;
  }

  // loop on electrons and make pairs
  int nBremCorr = 0;
  for ( auto i1 = electrons.begin(); i1 != electrons.end(); ++i1 ) {
    // check particle is charged
    auto* proto1 = ( *i1 )->proto();
    if ( !proto1 || !proto1->track() ) continue;

    for ( auto i2 = i1 + 1; i2 != electrons.end(); ++i2 ) {
      // check particle is charged
      auto* proto2 = ( *i2 )->proto();
      if ( !proto2 || !proto2->track() ) continue;

      // check sign of particles
      if ( m_oppSign && ( *i1 )->charge() == ( *i2 )->charge() )
        continue;
      else if ( !m_oppSign && ( *i1 )->charge() != ( *i2 )->charge() )
        continue;

      // clone electrons so we only modify the clone
      std::unique_ptr<LHCb::Particle> e1( ( *i1 )->clone() );
      std::unique_ptr<LHCb::Particle> e2( ( *i2 )->clone() );

      // add brem correction to pair
      auto bremadded = m_bremAdder->addBrem2Pair( *e1, *e2 );
      if ( bremadded ) nBremCorr++;

      // apply cuts on combination
      const Gaudi::LorentzVector da = e1->momentum() + e2->momentum();
      if ( da.M() > m_maxM * m_aFactor || da.M() < m_minM / m_aFactor ) continue;
      if ( da.pt() < m_ptmin ) continue;

      if ( msgLevel( MSG::DEBUG ) )
        debug() << "DiElectron sum: " << da.px() << " , " << da.py() << " , " << da.pz() << " , " << da.E() << " , "
                << da.M() << endmsg;

      // combine electrons
      auto diElec = std::make_unique<LHCb::Particle>( m_particle_prop->particleID() );
      auto vtx    = std::make_unique<LHCb::Vertex>();
      auto combSc = m_combiner->combine( { e1.get(), e2.get() }, *diElec, *vtx, *lhcb.geometry() );
      if ( combSc.isFailure() || ( diElec->p() < 0. ) ) {
        // add brem to original pair again changing order of e1 and e2
        // run combination again if e1 p is different in "normal" and "inverted" order
        if ( msgLevel( MSG::DEBUG ) ) debug() << "Error in vertex fit, try changing order of brem correction" << endmsg;
        ++m_nCombFail;

        // nothing to do if no brem was actually added
        if ( !hasBrem( *e1 ) && !hasBrem( *e2 ) ) continue;

        // start from original electrons again
        auto mom1 = e1->p();
        e1.reset( ( *i1 )->clone() );
        e2.reset( ( *i2 )->clone() );
        const bool b2 = m_bremAdder->addBrem2Pair( *e2, *e1 );
        // check new brem correction is different from previous
        if ( !b2 || LHCb::essentiallyEqual( mom1, e1->p() ) ) continue;
        auto combSc2 = m_combiner->combine( { e1.get(), e2.get() }, *diElec, *vtx, *lhcb.geometry() );
        if ( combSc2.isFailure() || ( diElec->p() < 0. ) ) {
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Error in 2nd vertex fit try" << endmsg;
          ++m_nComb2Fail;
          if ( m_pid != "gamma" ) continue;
          // dedicated combiner for conversions if vertex fit fails
          auto combSc3 = combinepair( *e1, *e2, *diElec, *vtx, *lhcb.geometry() );
          if ( combSc3.isFailure() ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << "Error in combinepair" << endmsg;
            ++m_nComb3Fail;
            continue;
          }
        }
      }

      // apply mass cut
      const auto diElecM = diElec->momentum().M();
      if ( diElecM < m_minM || diElecM > m_maxM ) continue;

      // assign the PV
      const auto& pv = LHCb::bestPV( pvs, vtx->position(), diElec->momentum() );
      if ( pv ) diElec->setPV( pv );

      // store diElectron
      diElectrons.add( diElec.release() );
      vertices.add( vtx.release() );
      children.add( e1.release() );
      children.add( e2.release() );
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "FunctionalDiElectronMaker: end loop on input electrons" << endmsg;
    debug() << "FunctionalDiElectronMaker: " << diElectrons.size() << endmsg;
  }

  m_nBremCorr += nBremCorr;
  m_nDiElec += diElectrons.size();
  m_npassed += !diElectrons.empty();
  return result;
}

bool FunctionalDiElectronMaker::hasBrem( LHCb::Particle const& el ) const {
  return ( el.info( LHCb::Particle::additionalInfo::HasBremAdded, 0. ) > 0. );
}

StatusCode FunctionalDiElectronMaker::combinepair( LHCb::Particle& ele1, LHCb::Particle& ele2, LHCb::Particle& parent,
                                                   LHCb::Vertex& vertex, IGeometryInfo const& geometry ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "in combinepair" << endmsg;

  StatusCode      sc = StatusCode::SUCCESS;
  LHCb::Particle  transParticle1;
  LHCb::Particle  transParticle2;
  Gaudi::XYZPoint refPoint;
  double          dz      = 100;
  double          distmin = 9999, distY = 0, distX = 0, distYmin = 0, distXmin = 0;
  double          z1   = ele1.referencePoint().Z();
  double          z2   = ele2.referencePoint().Z();
  double          znew = z1;
  if ( z2 > z1 ) znew = z2;
  double dist12 = distmin;
  while ( znew > -dz && LHCb::essentiallyEqual( dist12, distmin ) ) {
    sc = m_transporter->transport( &ele1, znew, transParticle1, geometry );
    if ( sc.isFailure() ) {
      ++m_transFail;
      continue;
    }
    sc = m_transporter->transport( &ele2, znew, transParticle2, geometry );
    if ( sc.isFailure() ) {
      ++m_transFail;
      continue;
    }
    const Gaudi::XYZPoint& pos1 = transParticle1.referencePoint();
    const Gaudi::XYZPoint& pos2 = transParticle2.referencePoint();
    //    dist12 = std::sqrt((pos1.X()-pos2.X())*(pos1.X()-pos2.X())+(pos1.Y()-pos2.Y())*(pos1.Y()-pos2.Y()));
    distY  = fabs( pos1.Y() - pos2.Y() );
    distX  = fabs( pos1.X() - pos2.X() );
    dist12 = std::sqrt( distX * distX + distY * distY );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Transport to " << znew << " dist= " << dist12 << endmsg;
    if ( dist12 < distmin ) {
      distmin  = dist12;
      distYmin = distY;
      distXmin = distX;
      ele1     = transParticle1;
      ele2     = transParticle2;
      refPoint = Gaudi::XYZPoint( ( pos1.X() + pos2.X() ) / 2, ( pos1.Y() + pos2.Y() ) / 2, pos1.Z() );
    }
    znew -= dz;
  }

  if ( distmin > m_maxdistpair || distYmin > m_maxdistpair / 2 ) { return StatusCode::FAILURE; }

  Gaudi::SymMatrix3x3 posCov;
  posCov[0][0] = ( distXmin / 2 ) * ( distXmin / 2 );
  posCov[1][1] = ( distYmin / 2 ) * ( distYmin / 2 );
  posCov[2][2] = ( dz / 2 ) * ( dz / 2 );
  posCov[0][1] = 0;
  posCov[0][2] = 0;
  posCov[1][0] = 0;
  posCov[1][2] = 0;
  posCov[2][0] = 0;
  posCov[2][1] = 0;
  vertex.clearOutgoingParticles();
  vertex.setChi2( 0.0 );
  vertex.setNDoF( 0 );
  vertex.setPosition( refPoint );
  vertex.setCovMatrix( posCov );
  parent.clearDaughters();
  parent.setEndVertex( &vertex );
  vertex.addToOutgoingParticles( &ele1 );
  vertex.addToOutgoingParticles( &ele2 );
  parent.addToDaughters( &ele1 );
  parent.addToDaughters( &ele2 );
  Gaudi::LorentzVector momentum   = ele1.momentum() + ele2.momentum();
  Gaudi::SymMatrix4x4  covariance = ele1.momCovMatrix() + ele2.momCovMatrix();
  Gaudi::Matrix4x3     posMomcov  = ele1.posMomCovMatrix() + ele2.posMomCovMatrix();
  parent.setReferencePoint( refPoint );
  parent.setMomentum( momentum );
  const double mass = momentum.M();
  parent.setPosCovMatrix( posCov );
  parent.setMomCovMatrix( covariance );
  parent.setPosMomCovMatrix( posMomcov );
  parent.setMeasuredMass( mass );

  Gaudi::Vector4 vct( -momentum.Px(), -momentum.Py(), -momentum.Pz(), momentum.E() );
  vct /= mass;

  double massErr2 = ROOT::Math::Similarity( covariance, vct );
  if ( massErr2 < 0 ) massErr2 = 0.; // FPE protection  (rounding ?)
  parent.setMeasuredMassErr( std::sqrt( massErr2 ) );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Created parent: momentum=" << parent.momentum() << ", mass= " << parent.measuredMass() << endmsg;

  return StatusCode::SUCCESS;
}

//
