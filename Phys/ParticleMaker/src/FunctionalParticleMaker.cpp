/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"
#include "Functors/with_functors.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackKernel/PrimaryVertexUtils.h"

#include "SelTools/Utilities.h"

#include <memory>
#include <string>

/** @class FunctionalParticleMaker FunctionalParticleMaker.h
 *
 * @brief LHCb::Particle creation from LHCb::ProtoParticle objects.
 *
 * TODO
 *
 * Example usage:
 *
 * @code {.py}
 * # TODO
 * @endcode
 *
 */

namespace {
  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Track& );
    static constexpr auto PropertyName = "TrackPredicate";
  };
  struct ProtoParticlePredicate {
    using Signature                    = bool( const LHCb::ProtoParticle& );
    static constexpr auto PropertyName = "ProtoParticlePredicate";
  };
} // namespace

class FunctionalParticleMaker
    : public with_functors<LHCb::Algorithm::Transformer<LHCb::Particles( LHCb::ProtoParticle::Range const&,
                                                                         LHCb::PrimaryVertices const& )>,
                           TrackPredicate, ProtoParticlePredicate> {

public:
  FunctionalParticleMaker( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  LHCb::Particles operator()( LHCb::ProtoParticle::Range const& protos,
                              LHCb::PrimaryVertices const&      pvs ) const override;

private:
  /// Return the State that should be used for defining the particle's kinematics
  const LHCb::State* usedState( const LHCb::Track* track ) const;

  /// Fill the Particle's members using the protoparticle and particle properties
  std::unique_ptr<LHCb::Particle> create_particle( const LHCb::ProtoParticle*    proto,
                                                   const LHCb::ParticleProperty* property,
                                                   LHCb::PrimaryVertices const&  pvs ) const;

  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };

  /// Tool for filling the particle's kinematics from a given state
  ToolHandle<IParticle2State> m_particle_from_state_tool = { this, "Particle2StateTool", "Particle2State" };

  /// Particle property information for matter particles to be created
  const LHCb::ParticleProperty* m_particle_prop = nullptr;
  /// Particle property information for antimatter particles to be created
  const LHCb::ParticleProperty* m_antiparticle_prop = nullptr;

  /// Confidence level assigned to each Particle
  const double m_CL = 50 * Gaudi::Units::perCent;

  void                         decode_particle_id();
  Gaudi::Property<std::string> m_particleid{ this, "ParticleID", "UNDEFINED",
                                             [this]( auto& ) {
                                               if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                               this->decode_particle_id();
                                             },
                                             "Particle species hypothesis to apply to each created object." };

  /// Number of events passed
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed{ this, "# passed" };
  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{ this, "Nb created particles" };
  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{ this, "Nb created anti-particles" };
  /// Number of protoparticles passing the protoparticle filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_proto_filter{ this, "# passed ProtoParticle filter" };
  /// Number of tracks passing the track filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_track_filter{ this, "# passed Track filter" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_part_with_no_track{
      this, "Charged ProtoParticle with no Track found. Ignoring" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_part_with_empty_state{
      this, "Track has empty states. This is likely to be a bug" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_fail_to_fill_from_state{
      this, "Failed to fill Particle from State" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_fail_to_fill{ this, "Failed to fill Particle, rejecting" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_state{
      this, "Found no state at ClosestToBeam or at FirstMeasurement for track. Using first state instead" };
};

DECLARE_COMPONENT( FunctionalParticleMaker )

void FunctionalParticleMaker::decode_particle_id() {

  const std::map<std::string, std::string> id_to_descriptor = {
      { "pion", "pi+" },      { "kaon", "K+" },           { "muon", "mu+" },    { "electron", "e+" },
      { "proton", "p+" },     { "deuteron", "deuteron" }, { "helium3", "He3" }, { "helium4", "alpha" },
      { "sigmap", "Sigma+" }, { "sigmam", "Sigma-" },     { "xim", "Xi-" },     { "omegam", "Omega-" } };
  const auto result = id_to_descriptor.find( m_particleid.value() );
  if ( result == id_to_descriptor.end() ) {
    throw GaudiException( "Unknown ParticleID value: " + m_particleid.value(), "FunctionalParticleMaker::initialize",
                          StatusCode::FAILURE );
  }
  auto descriptor = ( *result ).second;

  m_particle_prop = m_particlePropertySvc->find( descriptor );
  if ( !m_particle_prop ) {
    throw GaudiException( "Could not find ParticleProperty for " + m_particleid.value(),
                          "FunctionalParticleMaker::initialize", StatusCode::FAILURE );
  }
  m_antiparticle_prop = m_particle_prop->antiParticle();
}

FunctionalParticleMaker::FunctionalParticleMaker( const std::string& name, ISvcLocator* pSvcLocator )
    : with_functors( name, pSvcLocator,
                     { KeyValue{ "InputProtoParticles", LHCb::ProtoParticleLocation::Charged },
                       KeyValue{ "PrimaryVertices", "" } },
                     { KeyValue{ "Particles", "" } } ) {}

StatusCode FunctionalParticleMaker::initialize() {
  return with_functors::initialize().andThen( [&] { decode_particle_id(); } );
}

LHCb::Particles FunctionalParticleMaker::operator()( LHCb::ProtoParticle::Range const& protos,
                                                     LHCb::PrimaryVertices const&      pvs ) const {
  LHCb::Particles particles;
  int             nparticles     = 0;
  int             nantiparticles = 0;

  auto const& proto_pred = getFunctor<ProtoParticlePredicate>();
  auto const& track_pred = getFunctor<TrackPredicate>();
  for ( const auto* proto : protos ) {
    const auto* track = proto->track();
    // Sanity checks
    if ( !track ) {
      ++m_part_with_no_track;
      continue;
    }
    if ( track->states().empty() ) {
      ++m_part_with_empty_state;
      continue;
    }

    const bool selected_track = track_pred( *track );
    m_npassed_track_filter += selected_track;
    if ( !selected_track ) { continue; }

    const auto selected_proto = proto_pred( *proto );
    m_npassed_proto_filter += selected_proto;
    if ( !selected_proto ) { continue; }

    // Get the (anti)particle property corresponding to the charge we have
    const auto* prop = [this, &proto] {
      if ( const auto q = proto->charge(); q * m_particle_prop->charge() > 0 ) {
        return m_particle_prop;
      } else if ( q * m_antiparticle_prop->charge() > 0 ) {
        return m_antiparticle_prop;
      } else {
        throw GaudiException( "Unexpected proto-particle with charge " + std::to_string( q ) +
                                  ". Check decay descriptor and avoid unphysical tracks.",
                              this->name(), StatusCode::FAILURE );
      }
    }();
    // Create the particle
    auto particle = create_particle( proto, prop, pvs );
    if ( !particle ) {
      ++m_fail_to_fill;
      continue;
    }

    // Record what we successfully fill
    if ( prop == m_particle_prop ) {
      ++nparticles;
    } else if ( prop == m_antiparticle_prop ) {
      ++nantiparticles;
    }

    particles.add( particle.release() );
  }

  m_nparticles += nparticles;
  m_nantiparticles += nantiparticles;
  m_npassed += !particles.empty();
  return particles;
}

const LHCb::State* FunctionalParticleMaker::usedState( const LHCb::Track* track ) const {
  if ( track == nullptr ) return nullptr;

  const LHCb::State* state = Sel::Utils::defaultStateForParticle( *track );

  // Backup
  if ( !state ) {
    ++m_no_state;
    state = &track->firstState();
  }

  return state;
}

std::unique_ptr<LHCb::Particle> FunctionalParticleMaker::create_particle( const LHCb::ProtoParticle*    proto,
                                                                          const LHCb::ParticleProperty* property,
                                                                          LHCb::PrimaryVertices const&  pvs ) const {
  auto particle = std::make_unique<LHCb::Particle>();
  particle->setMeasuredMass( property->mass() );
  particle->setMeasuredMassErr( 0 );

  particle->setParticleID( property->particleID() );
  particle->setProto( proto );

  // Take the default confidence level
  particle->setConfLevel( m_CL );

  // Find the appropriate state and use it to define this object's kinematics
  const LHCb::State* state = usedState( proto->track() );
  m_particle_from_state_tool->state2Particle( *state, *particle )
      .orElse( [&] {
        ++m_fail_to_fill_from_state;
        particle.reset();
      } )
      .ignore();

  // Set the primary vertex: FIXME::shouldn't we use state here?
  const auto& pv = LHCb::bestPV( pvs, particle->referencePoint(), particle->momentum() );
  if ( pv ) particle->setPV( pv );

  return particle;
}
