/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/with_functors.h"
#include "LHCbAlgs/FilterPredicate.h"

namespace {
  struct VoidCut {
    using Signature                    = bool();
    static constexpr auto PropertyName = "Cut";
  };
} // namespace

/** @class VoidFilter VoidFilter.cpp
 */
struct VoidFilter final : public with_functors<LHCb::Algorithm::FilterPredicate<bool()>, VoidCut> {
  using with_functors::with_functors;

  bool operator()() const override {
    auto const& functor = getFunctor<VoidCut>();
    auto        result  = functor();
    m_cutEff += result;
    return result;
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{ this, "Cut selection efficiency" };
};

DECLARE_COMPONENT_WITH_ID( VoidFilter, "VoidFilter" )
