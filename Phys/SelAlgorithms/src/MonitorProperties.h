/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/FunctorDesc.h"
#include "Gaudi/Parsers/Grammars.h"
#include <string>
#include <tuple>
#include <variant>

namespace SelAlgorithms::monitoring {

  // The type of the values stored in the histograms
  using histogram_value_type = float;

  // Configuration object of a histogram axis that can be build from python
  struct histogram_axis {

    // Enable the default constructor so we can use histogram classes in a std::variant
    histogram_axis() = default;

    // Constructor from the fields
    histogram_axis( ThOr::FunctorDesc _functor_desc, unsigned int _bins, histogram_value_type _min,
                    histogram_value_type _max, std::string _label )
        : functor_desc{ std::move( _functor_desc ) }
        , bins{ _bins }
        , min{ _min }
        , max{ _max }
        , label{ std::move( _label ) } {}

    // The functor that will be used to compute the values to the histogram
    ThOr::FunctorDesc functor_desc;
    // Number of bins in the histogram
    unsigned int bins = 1u;
    // Minimum value of the variable that is shown
    histogram_value_type min = 0;
    // Maximum value of the variable that is shown
    histogram_value_type max = 1;
    // Label of the axis
    std::string label = "";
  };

  // Configuration object of a 1D histogram that can be build from python
  struct histogram_1d {

    // Enable the default constructor so we can use it in a std::variant
    histogram_1d() = default;

    // Constructor from the fields
    histogram_1d( std::string _name, std::string _title, histogram_axis _axis )
        : name{ std::move( _name ) }, title{ std::move( _title ) }, axis{ std::move( _axis ) } {}

    // Name of the histogram in the ROOT file
    std::string name;
    // Title of the histogram in the ROOT file
    std::string title;
    // Configuration of the axis
    histogram_axis axis;
  };

  // Configuration of a 2D histogram that can be build from python
  struct histogram_2d {

    // Enable the default constructor so we can use it in a std::variant
    histogram_2d() = default;

    // Construct the object from the name, title and the configuration of the axes
    histogram_2d( std::string _name, std::string _title, histogram_axis _xaxis, histogram_axis _yaxis )
        : name{ std::move( _name ) }
        , title{ std::move( _title ) }
        , xaxis{ std::move( _xaxis ) }
        , yaxis{ std::move( _yaxis ) } {}

    // Name of the histogram in the ROOT file
    std::string name;
    // Title of the histogram in the ROOT file
    std::string title;
    // Definition of the Y axis
    histogram_axis xaxis;
    // Configuration of the Y axis
    histogram_axis yaxis;
  };

  // Configuration of a histogram that can be either 1D or 2D
  using histogram_variant = std::variant<histogram_1d, histogram_2d>;

} // namespace SelAlgorithms::monitoring

BOOST_FUSION_ADAPT_STRUCT( SelAlgorithms::monitoring::histogram_axis,
                           ( ThOr::FunctorDesc, functor_desc )( unsigned int, bins )(
                               SelAlgorithms::monitoring::histogram_value_type,
                               min )( SelAlgorithms::monitoring::histogram_value_type, max )( std::string, label ) )

BOOST_FUSION_ADAPT_STRUCT( SelAlgorithms::monitoring::histogram_1d,
                           ( std::string, name )( std::string, title )( SelAlgorithms::monitoring::histogram_axis,
                                                                        axis ) )

BOOST_FUSION_ADAPT_STRUCT( SelAlgorithms::monitoring::histogram_2d,
                           ( std::string, name )( std::string,
                                                  title )( SelAlgorithms::monitoring::histogram_axis,
                                                           xaxis )( SelAlgorithms::monitoring::histogram_axis, yaxis ) )

namespace Gaudi::Parsers {

  template <typename Iterator, typename Skipper>
  struct histogram_axis_grammar : qi::grammar<Iterator, SelAlgorithms::monitoring::histogram_axis(), Skipper> {
    using ResultT = SelAlgorithms::monitoring::histogram_axis;
    histogram_axis_grammar() : histogram_axis_grammar::base_type( literal ) {
      literal = '(' >> gfunctor /*functor serialization*/ >> ',' >> gbins /*number of bins*/ >> ',' >> gbound /*min*/ >>
                ',' >> gbound /*max*/ >> ',' >> gstring /*label*/ >> ')';
    }
    qi::rule<Iterator, ResultT(), Skipper>                                          literal;
    RealGrammar<Iterator, unsigned int, Skipper>                                    gbins;
    RealGrammar<Iterator, SelAlgorithms::monitoring::histogram_value_type, Skipper> gbound;
    FunctorDescGrammar<Iterator, Skipper>                                           gfunctor;
    StringGrammar<Iterator, Skipper>                                                gstring;
  };
  REGISTER_GRAMMAR( SelAlgorithms::monitoring::histogram_axis, histogram_axis_grammar );

  template <typename Iterator, typename Skipper>
  struct histogram_1d_grammar : qi::grammar<Iterator, SelAlgorithms::monitoring::histogram_1d(), Skipper> {
    using ResultT = SelAlgorithms::monitoring::histogram_1d;
    histogram_1d_grammar() : histogram_1d_grammar::base_type( literal ) {
      literal = '(' >> gstring /*name*/ >> ',' >> gstring /*title*/ >> ',' >> gaxis /*axis*/ >> ')';
    }
    qi::rule<Iterator, ResultT(), Skipper>    literal;
    StringGrammar<Iterator, Skipper>          gstring;
    histogram_axis_grammar<Iterator, Skipper> gaxis;
  };
  REGISTER_GRAMMAR( SelAlgorithms::monitoring::histogram_1d, histogram_1d_grammar );

  template <typename Iterator, typename Skipper>
  struct histogram_2d_grammar : qi::grammar<Iterator, SelAlgorithms::monitoring::histogram_2d(), Skipper> {
    using ResultT = SelAlgorithms::monitoring::histogram_2d;
    histogram_2d_grammar() : histogram_2d_grammar::base_type( literal ) {
      literal = '(' >> gstring /*name*/ >> ',' >> gstring /*title*/ >> ',' >> gaxis /*x-axis*/ >> ',' >>
                gaxis /*y-axis*/ >> ')';
    }
    qi::rule<Iterator, ResultT(), Skipper>    literal;
    StringGrammar<Iterator, Skipper>          gstring;
    histogram_axis_grammar<Iterator, Skipper> gaxis;
  };
  REGISTER_GRAMMAR( SelAlgorithms::monitoring::histogram_2d, histogram_2d_grammar );

  template <typename Iterator, typename Skipper>
  struct histogram_variant_grammar : qi::grammar<Iterator, SelAlgorithms::monitoring::histogram_variant(), Skipper> {
    using ResultT = SelAlgorithms::monitoring::histogram_variant;
    histogram_variant_grammar() : histogram_variant_grammar::base_type( literal ) { literal = ghistcfg1d | ghistcfg2d; }
    qi::rule<Iterator, ResultT(), Skipper>  literal;
    histogram_1d_grammar<Iterator, Skipper> ghistcfg1d;
    histogram_2d_grammar<Iterator, Skipper> ghistcfg2d;
  };
  REGISTER_GRAMMAR( SelAlgorithms::monitoring::histogram_variant, histogram_variant_grammar );
} // namespace Gaudi::Parsers

namespace std {
  ostream& operator<<( ostream& os, SelAlgorithms::monitoring::histogram_axis const& obj ) {
    // we must parse the functor ourselves because the stream operator would give us
    // the representation enclosed between double quotes
    return os << '(' << obj.functor_desc << ", " << obj.bins << ", " << obj.min << ", " << obj.max << ", "
              << quoted( obj.label, '\'' ) << ')';
  }

  ostream& operator<<( ostream& os, SelAlgorithms::monitoring::histogram_1d const& obj ) {
    return os << '(' << quoted( obj.name, '\'' ) << ", " << quoted( obj.title, '\'' ) << ", " << obj.axis << ')';
  }

  ostream& operator<<( ostream& os, SelAlgorithms::monitoring::histogram_2d const& obj ) {
    return os << '(' << quoted( obj.name, '\'' ) << ", " << quoted( obj.title, '\'' ) << ", " << obj.xaxis << ", "
              << obj.yaxis << ')';
  }

  ostream& operator<<( ostream& os, SelAlgorithms::monitoring::histogram_variant const& obj ) {
    return std::visit( [&]( auto const& o ) -> std::ostream& { return os << o; }, obj );
  }
} // namespace std
