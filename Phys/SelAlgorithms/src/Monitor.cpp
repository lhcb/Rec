/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Monitor.h"
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/PrVeloTracks.h"
#include "PrKernel/PrSelection.h"

namespace SelAlgorithms::monitoring {

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Pr::Velo::Tracks>, "Monitor__PrVeloTracks" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::v3::Tracks>, "Monitor__PrFittedForwardTracks" ) // TODO: deduplicate

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::v3::TracksWithPVs>, "Monitor__PrFittedForwardTracksWithPVs" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::v3::TracksWithMuonID>, "Monitor__PrFittedForwardTracksWithMuonID" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::v3::Tracks>, "Monitor__SOATracks" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::ChargedBasics>, "Monitor__ChargedBasics" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Event::Composites>, "Monitor__Composites" )

  DECLARE_COMPONENT_WITH_ID( Monitor<LHCb::Particle::Range>, "Monitor__ParticleRange" )

  DECLARE_COMPONENT_WITH_ID( Monitor<void>, "Monitor__Global" )
} // namespace SelAlgorithms::monitoring
