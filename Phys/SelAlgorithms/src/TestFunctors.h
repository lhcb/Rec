/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOAUtils.h"
#include "Functors/Filter.h"
#include "Functors/with_functor_maps.h"

#include "GaudiKernel/SerializeSTL.h"
#include "LHCbAlgs/Consumer.h"

#include <any>

namespace ThOr::Functors::Tests::detail {
  template <typename>
  struct as_sequence;

  template <typename T, T... Ints>
  struct as_sequence<boost::mp11::mp_list<std::integral_constant<T, Ints>...>> {
    using type = std::integer_sequence<T, Ints...>;
  };

  /** Helper to transform
   *    mp_list<integral_constant<T, I0>, integral_constant<T, I1>, ...>
   *  into
   *    integer_sequence<T, I0, I1, ...>
   */
  template <typename T>
  using as_sequence_t = typename as_sequence<T>::type;

  /** Figure out which backends are really distinct in this stack.
   */
  // mp_list<integral_constant<InstructionSet, AVX2>, ...>
  using all_vector_backends = boost::mp11::mp_list_c<SIMDWrapper::InstructionSet, SIMDWrapper::Scalar, SIMDWrapper::SSE,
                                                     SIMDWrapper::AVX2, SIMDWrapper::AVX256, SIMDWrapper::AVX512>;
  // e.g. transform SIMDWrapper::AVX2 into SIMDWrapper::avx2::types
  template <typename T>
  using mapped_t = SIMDWrapper::type_map_t<T::value>;
  // mp_list of SIMDWrapper::scalar::types, SIMDWrapper::sse::types, ... that
  // will have duplicate entries if not all backends are enabled in this stack
  using mapped_types = boost::mp11::mp_transform<mapped_t, all_vector_backends>;
  // get the index of the first occurrence of T in mapped_types
  template <typename T>
  using index_in_mapped_types = boost::mp11::mp_find<mapped_types, T>;
  template <typename, typename T, typename U>
  using second_and_third_are_same = std::is_same<T, U>;
  // Filter 'all_vector_backends', keeping only the first entry that
  // corresponds to each type in 'mapped_types'. This will be something like
  // mp_list<integral_constant<InstructionSet, Scalar>,
  //         integral_constant<InstructionSet, SSE>,
  //         ...>
  using distinct_vector_backends =
      boost::mp11::mp_filter<second_and_third_are_same, all_vector_backends,
                             boost::mp11::mp_transform<index_in_mapped_types, mapped_types>,
                             boost::mp11::mp_iota<boost::mp11::mp_size<all_vector_backends>>>;
  // This one is integer_sequence<InstructionSet, Scalar, SSE, ...>
  using distinct_vector_backend_sequence = as_sequence_t<distinct_vector_backends>;

  template <typename, SIMDWrapper::InstructionSet, bool>
  struct return_helper {};
  template <SIMDWrapper::InstructionSet Target, bool ShouldGivePOD>
  struct return_helper<int, Target, ShouldGivePOD> {
    using type = std::conditional_t<ShouldGivePOD, int, typename SIMDWrapper::type_map_t<Target>::int_v>;
  };
  template <SIMDWrapper::InstructionSet Target, bool ShouldGivePOD>
  struct return_helper<bool, Target, ShouldGivePOD> {
    using type = std::conditional_t<ShouldGivePOD, bool, typename SIMDWrapper::type_map_t<Target>::mask_v>;
  };
  template <SIMDWrapper::InstructionSet Target, bool ShouldGivePOD>
  struct return_helper<float, Target, ShouldGivePOD> {
    using type = std::conditional_t<ShouldGivePOD, float, typename SIMDWrapper::type_map_t<Target>::float_v>;
  };

  /** Take some functors that act on elements of the input.
   */
  template <SIMDWrapper::InstructionSet Target, bool WrappedInPOD, typename... ConsumedTypes>
  struct Dump {
    static_assert( Target == SIMDWrapper::Scalar || !WrappedInPOD );
    template <typename T>
    using item_t =
        typename LHCb::Event::simd_zip_t<Target, T>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
    using Signature                     = std::any( item_t<ConsumedTypes> const&... );
    constexpr static bool IsAFilterTag  = false;
    constexpr static auto VectorBackend = Target;
    constexpr static bool ShouldGivePOD = WrappedInPOD;
    constexpr static auto PropertyName  = WrappedInPOD ? "PODFunctors" : "Functors";
    static std::string    name() {
      return SIMDWrapper::instructionSetName( Target ) + " " + ( WrappedInPOD ? " POD" : "" );
    }
    // Possible expected return types
    template <typename T>
    using return_t = typename return_helper<T, Target, ShouldGivePOD>::type;
  };

  /** For completeness, also take a functor that filters an entire container.
   */
  template <SIMDWrapper::InstructionSet Target, typename ConsumedType>
  struct Filter {
    using iterable_t                    = typename LHCb::Event::simd_zip_t<Target, ConsumedType>;
    using Signature                     = ::Functors::filtered_t<ConsumedType>( iterable_t const& );
    constexpr static bool IsAFilterTag  = true;
    constexpr static auto VectorBackend = Target;
    constexpr static auto PropertyName  = "Cut";
    static std::string    name() { return SIMDWrapper::instructionSetName( Target ) + " " + " filter"; }
  };

  /** Construct the base type -- defining an alias here saves typing below.
   */
  template <typename InstantiatedTypeList, typename ConsumedTypeList>
  struct base_helper {
    // Put together the arguments for the with_functor_maps mixin
    template <typename... Ts>
    using base_class_helper = boost::mp11::mp_list<LHCb::Algorithm::Consumer<void( Ts const&... )>>;
    // mp_list<Consumer<void( T const& )>> if ConsumedTypeList = {T},
    // otherwise mp_list<Consumer<void()>>
    using base_class = boost::mp11::mp_apply<base_class_helper, ConsumedTypeList>;
    template <typename... Ts>
    using first_dumps_helper = boost::mp11::mp_list<Dump<SIMDWrapper::Scalar, true, Ts...>>;
    // mp_list<Dump<...>, Dump<...>, Dump<...>> where each Dump<...> received
    // the contents InstantiatedTypeList as a trailing parameter pack
    using first_dumps = boost::mp11::mp_apply<first_dumps_helper, InstantiatedTypeList>;
    template <typename... InstantiatedTypes>
    struct second_dumps_helper {
      template <typename Target>
      using fn = Dump<Target::value, false /* no POD */, InstantiatedTypes...>;
    };
    // mp_list<Dump<Target0, .., InstantiatedTypes...>,
    //         Dump<Target1, .., InstantiatedTypes...>,
    //         ...>
    // for VectorBackends = {Target0, Target1, ...}
    // and InstantiatedTypeList = {InstantiatedTypes...}
    // The list of backends is distinct_vector_backends unless the list of
    // instantiated types is empty (void functor), in which case it is just
    // scalar (the first element of distinct_vector_backends)
    using target_vector_backends =
        std::conditional_t<boost::mp11::mp_empty<InstantiatedTypeList>::value,
                           boost::mp11::mp_take_c<distinct_vector_backends, 1>, distinct_vector_backends>;
    using second_dumps = boost::mp11::mp_transform_q<boost::mp11::mp_apply<second_dumps_helper, InstantiatedTypeList>,
                                                     target_vector_backends>;
    template <typename T, typename TargetIntegralConstant>
    using filters_helper = Filter<TargetIntegralConstant::value, T>;
    // Empty list if InstantiatedTypeList is empty (void functor), otherwise
    // mp_list<Filter<Target0, ..., InstantiatedType>,
    //         Filter<Target1, ..., InstantiatedType>,
    //         ...>
    // for distinct_vector_backends = {Target0, Target1, ...}
    using filters  = boost::mp11::mp_product<filters_helper, InstantiatedTypeList, distinct_vector_backends>;
    using all_args = boost::mp11::mp_append<base_class, first_dumps, second_dumps, filters>;
    using type     = boost::mp11::mp_apply<with_functor_maps, all_args>;
  };
  template <typename InstantiatedTypeList, typename ConsumedTypeList>
  using base_t = typename base_helper<InstantiatedTypeList, ConsumedTypeList>::type;
} // namespace ThOr::Functors::Tests::detail

namespace ThOr::Functors::Tests {
  using GaudiUtils::operator<<;
  /** @class  ThOr::Functors::Tests::InitialiseAndOptionallyEvaluate TestFunctors.h
   *  @tparam SkipEvaluation       Should this algorithm just instantiate the
   *                               functors and perform initialize-time checks, or
   *                               should it also perform execute-time checks (this
   *                               requires input data!)
   *  @tparam InstantiatedTypeList boost::mp11::mp_list<...> list of argument
   *                               types to instantiate functors with. This will
   *                               typically be empty (void functors) or have one
   *                               entry.
   *  @tparam ConsumedTypes...     Type of the container that will be iterated
   *                               over to test the functors, and filtered to
   *                               test filters. Is is expected that this pack
   *                               contains zero (for void functors, or non-void
   *                               functors that are skipping the runtime checks)
   *                               or one entry.
   */
  template <bool SkipEvaluation, typename InstantiatedTypeList, typename... ConsumedTypes>
  struct InitialiseAndOptionallyEvaluate final
      : public detail::base_t<InstantiatedTypeList, boost::mp11::mp_list<ConsumedTypes...>> {
    using ConsumedTypeList                       = boost::mp11::mp_list<ConsumedTypes...>;
    using base_t                                 = detail::base_t<InstantiatedTypeList, ConsumedTypeList>;
    constexpr static auto num_consumed_types     = boost::mp11::mp_size<ConsumedTypeList>::value;
    constexpr static auto num_instantiated_types = boost::mp11::mp_size<InstantiatedTypeList>::value;
    constexpr static bool ConsumeInput           = ( num_consumed_types > 0 );
    static_assert( num_consumed_types < 2 );
    static_assert( num_instantiated_types < 2 );

    template <bool ConsumeInput_ = ConsumeInput, std::enable_if_t<ConsumeInput_, int> = 0>
    InitialiseAndOptionallyEvaluate( std::string const& name, ISvcLocator* pSvcLocator )
        : base_t( name, pSvcLocator, { "Input", "" } ) {}

    template <bool ConsumeInput_ = ConsumeInput, std::enable_if_t<!ConsumeInput_, int> = 0>
    InitialiseAndOptionallyEvaluate( std::string const& name, ISvcLocator* pSvcLocator )
        : base_t( name, pSvcLocator ) {}

    static std::string prettyName() { return System::typeinfoName( typeid( InitialiseAndOptionallyEvaluate ) ); }

    // Get the full set of filter + function tag types
    using tag_types_tuple = typename base_t::functor_map_tag_types;
    using tag_types       = boost::mp11::mp_rename<tag_types_tuple, boost::mp11::mp_list>;

    // Split these into Dump and Filter tags
    template <typename T>
    using is_filter_tag                   = std::integral_constant<bool, T::IsAFilterTag>;
    using filter_tag_types                = boost::mp11::mp_copy_if<tag_types, is_filter_tag>;
    using dump_tag_types                  = boost::mp11::mp_remove_if<tag_types, is_filter_tag>;
    constexpr static auto num_filter_tags = boost::mp11::mp_size<filter_tag_types>::value;
    constexpr static auto num_dump_tags   = boost::mp11::mp_size<dump_tag_types>::value;

    StatusCode initialize() override {
      // Let the mixin prepare the functors
      auto sc = base_t::initialize();
      // Delegate to something with parameter packs
      initialize( std::make_index_sequence<num_dump_tags>{}, dump_tag_types{},
                  std::make_index_sequence<num_filter_tags>{}, filter_tag_types{},
                  detail::distinct_vector_backend_sequence{} );
      return sc;
    }

    using pod_variant_t = std::variant<int, bool, float>;
    std::vector<pod_variant_t> m_variants{};

    /** The meat of the initialize checking.
     *  @todo actually check the filter functor(s)
     */
    template <std::size_t... Dump_Is, std::size_t... Filter_Is, typename... dump_tags, typename... filter_tags,
              SIMDWrapper::InstructionSet... Backends>
    void initialize( std::index_sequence<Dump_Is...>, boost::mp11::mp_list<dump_tags...>,
                     std::index_sequence<Filter_Is...>, boost::mp11::mp_list<filter_tags...>,
                     std::integer_sequence<SIMDWrapper::InstructionSet, Backends...> ) {
      static_assert( sizeof...( Dump_Is ) == sizeof...( dump_tags ) );
      static_assert( sizeof...( Filter_Is ) == sizeof...( filter_tags ) );
      // Get all the loaded/JITed functors
      auto const dump_functors   = std::tie( this->template getFunctorMap<dump_tags>()... );
      auto const filter_functors = std::tie( this->template getFunctorMap<filter_tags>()... );
      // Get the names of the tag types
      std::array<std::string, num_dump_tags>   dump_tag_names{ dump_tags::name()... };
      std::array<std::string, num_filter_tags> filter_tag_names{ filter_tags::name()... };
      this->debug() << "dump_tag_names: " << dump_tag_names << endmsg;
      this->debug() << "filter_tag_names: " << filter_tag_names << endmsg;
      // Shorthand for below
      auto const exception = [&]( std::string name ) {
        std::ostringstream oss;
        oss << std::move( name ) << " [dump_tag_names = " << dump_tag_names
            << ", filter_tag_names = " << filter_tag_names << ']';
        return GaudiException{ oss.str(), prettyName() + "::initialize()", StatusCode::FAILURE };
      };

      // Count how many valid functors there are in each container
      auto const check_counts = [&]( std::string_view name, auto const& functor_tuple ) {
        auto const valid_counts = std::apply(
            []( auto const&... functors ) {
              // Count non-null because during cache generation container will be empty (and all will be null)
              [[maybe_unused]] auto const num_valid = []( auto const& fmap ) {
                return std::count_if( fmap.begin(), fmap.end(), []( auto const& name_functor_pair ) -> bool {
                  return name_functor_pair.second;
                } );
              };
              return std::array<long, sizeof...( functors )>{ num_valid( functors )... };
            },
            functor_tuple );
        // Do some basic checks -- note that this should "really" be in some kind of update handler.
        if ( !all_same( valid_counts ) ) {
          std::ostringstream oss;
          oss << "Got " << name << " = " << valid_counts;
          throw exception( oss.str() );
        }
      };
      check_counts( "dump_counts", dump_functors );
      check_counts( "filter_counts", filter_functors );

      // Good sets of return values for dump functors
      std::array const good_rtypes{
          std::array{ System::typeinfoName( typeid( typename dump_tags::template return_t<int> ) )... },
          std::array{ System::typeinfoName( typeid( typename dump_tags::template return_t<bool> ) )... },
          std::array{ System::typeinfoName( typeid( typename dump_tags::template return_t<float> ) )... } };
      // Variants initialised with the corresponding plain types -- the order
      // here MUST match good_rtypes just above.
      [[maybe_unused]] std::array<pod_variant_t, 3> const good_rtypes_variants{ int{}, bool{}, float{} };

      // The sets of functors are all the same length, so we can loop through them in lockstep
      m_variants.clear();
      for ( auto iters = std::tuple{ std::get<Dump_Is>( dump_functors ).begin()... };
            iters != std::tuple{ std::get<Dump_Is>( dump_functors ).end()... };
            ( ++std::get<Dump_Is>( iters ), ... ) ) {
        // Get the keys from all the functor maps
        std::array functor_names{ std::string_view{ std::get<Dump_Is>( iters )->first }... };
        // Get a tuple of const references to the functors themselves (map values)
        auto const functors = std::tie( std::as_const( std::get<Dump_Is>( iters )->second )... );
        // Check that they all match
        if ( !all_same( functor_names ) ) {
          std::ostringstream oss;
          oss << "Got functor_names = " << functor_names;
          throw exception( oss.str() );
        }
        // See if all the functors are null
        if ( ( !std::get<Dump_Is>( functors ) && ... ) ) {
          // This happens during cache generation (both cling and the cache are
          // disabled)
          continue;
        }
        if ( ( !std::get<Dump_Is>( functors ) || ... ) ) { throw exception( "A subset of functors were null" ); }
        // Get all of the return types as strings
        std::array const functor_rtypes{ System::typeinfoName( std::get<Dump_Is>( functors ).rtype() )... };
        // We also allow all functor return types that are trivially copyable (e.g. enums)
        // std::array const trivially_copyable{ std::is_trivially_copyable_v<decltype(std::get<Dump_Is>( functors
        // ).rtype())>... };

        this->info() << functor_names.front() << " return types " << functor_rtypes << endmsg;
        // Slightly different handling for void/nonvoid functors
        if constexpr ( num_instantiated_types > 0 ) {
          // Check if the list of strings matches our expectation
          auto const good_rtypes_iter = std::find( good_rtypes.begin(), good_rtypes.end(), functor_rtypes );
          if ( good_rtypes_iter == good_rtypes.end() /*&& std::none_of(trivially_copyable.begin(), trivially_copyable.end(), [](auto a){return !a;})*/) {
            std::ostringstream oss;
            oss << "Return types " << functor_rtypes << " not found in list of known types " << good_rtypes;
            throw exception( oss.str() );
          }
          // If we got this far the types behave as expected. That means there is a
          // single int/bool/float type that is relevant for this row of functors.
          // We will need to know/use that type for the value comparison.
          std::size_t const type_offset = std::distance( good_rtypes.begin(), good_rtypes_iter );
          // Push a variant object initialised with the relevant type into a vector
          m_variants.push_back( good_rtypes_variants[type_offset] );
          assert( type_offset == m_variants.back().index() );
        } else {
          // void branch, here let's just check that all the return types were the same
          if ( !all_same( functor_rtypes ) ) {
            std::ostringstream oss;
            oss << "Return types " << functor_rtypes << " from different versions of a void functor did not match";
            throw exception( oss.str() );
          }
        }
      }

      if constexpr ( num_filter_tags > 0 ) {
        // Do some basic checking of the filter functor(s) that we received
        for ( auto iters = std::tuple{ std::get<Filter_Is>( filter_functors ).begin()... };
              iters != std::tuple{ std::get<Filter_Is>( filter_functors ).end()... };
              ( ++std::get<Filter_Is>( iters ), ... ) ) {
          // Get the keys from all the functor maps
          std::array functor_names{ std::string_view{ std::get<Filter_Is>( iters )->first }... };
          // Check that they all match
          if ( !all_same( functor_names ) ) {
            std::ostringstream oss;
            oss << "Got filter functor_names = " << functor_names;
            throw exception( oss.str() );
          }
          // Get a tuple of const references to the functors themselves (map values)
          auto const functors = std::tie( std::get<Filter_Is>( iters )->second... );
          // See if all the functors are null
          if ( ( !std::get<Filter_Is>( functors ) && ... ) ) {
            // This happens during cache generation (both cling and the cache are
            // disabled)
            continue;
          }
          // Get the return types of all the different versions as strings
          std::array const functor_rtypes{ System::typeinfoName( std::get<Filter_Is>( functors ).rtype() )... };
          // Check that they all match
          if ( !all_same( functor_rtypes ) ) {
            std::ostringstream oss;
            oss << "Got filter functor_rtypes = " << functor_rtypes;
            throw exception( oss.str() );
          }
        }
      }
    }

    void operator()( ConsumedTypes const&... input_container ) const override {
      // Delegate to something with parameter packs
      if constexpr ( !SkipEvaluation ) {
        // It's a bit less contorted to branch on void/not void here
        if constexpr ( sizeof...( ConsumedTypes ) > 0 ) {
          // non-void case
          do_the_work( input_container..., std::make_index_sequence<num_dump_tags>{}, dump_tag_types{},
                       std::make_index_sequence<num_filter_tags>{}, filter_tag_types{},
                       detail::distinct_vector_backend_sequence{} );
        } else {
          // void case
          do_the_work( std::make_index_sequence<num_dump_tags>{}, dump_tag_types{} );
        }
      }
    }

    /** Runtime checking for void functors
     */
    template <std::size_t... Dump_Is, typename... dump_tags>
    void do_the_work( std::index_sequence<Dump_Is...>, boost::mp11::mp_list<dump_tags...> ) const {
      // Get all the dumping functors/functions
      auto const dump_functors = std::tie( this->template getFunctorMap<dump_tags>()... );
      for ( auto iters = std::tuple{ std::get<Dump_Is>( dump_functors ).begin()... };
            iters != std::tuple{ std::get<Dump_Is>( dump_functors ).end()... };
            ( ++std::get<Dump_Is>( iters ), ... ) ) {
        // Evaluate all of the functors
        std::array<std::any, num_dump_tags> results{ std::get<Dump_Is>( iters )->second()... };
        // Deal with the type-erasure and check the results are all the same
        if ( !erased_values_all_match<bool, int>( results ) ) {
          // We already checked that all of the names are the same
          auto const& functor_name = std::get<0>( iters )->first;
          throw GaudiException{ "Mismatched void functor (" + functor_name + ") return values",
                                prettyName() + "::operator()(void)", StatusCode::FAILURE };
        }
      }
    }

    /** Get the size of the object returned by calling .filter() on a zip.
     */
    template <typename FilterResult>
    auto filtered_size( FilterResult const& filtered ) const -> decltype( filtered.size() ) {
      return filtered.size();
    }

    template <typename... Ts>
    auto filtered_size( std::tuple<Ts...> const& filtered_tuple ) const {
      auto const first_size = std::get<0>( filtered_tuple ).size();
      assert( ( ( std::get<Ts>( filtered_tuple ).size() == first_size ) && ... ) );
      return first_size;
    }

    /** The meat of the runtime checking.
     */
    template <typename InputType, std::size_t... Dump_Is, std::size_t... Filter_Is, typename... dump_tags,
              typename... filter_tags, SIMDWrapper::InstructionSet... Backends>
    void do_the_work( InputType const& input_container, std::index_sequence<Dump_Is...>,
                      boost::mp11::mp_list<dump_tags...>, std::index_sequence<Filter_Is...>,
                      boost::mp11::mp_list<filter_tags...>,
                      std::integer_sequence<SIMDWrapper::InstructionSet, Backends...> ) const {
      // Shorthand for below
      auto exception = [&]( std::string_view name ) {
        std::ostringstream oss;
        oss << name << " [dump_tag_names = " << std::array{ dump_tags::name()... } << ']';
        return GaudiException{ oss.str(), prettyName() + "::operator()", StatusCode::FAILURE };
      };

      // First check the filter functors
      auto const filter_functors = std::tie( this->template getFunctorMap<filter_tags>()... );

      // Prepare the input types that they expect
      std::tuple const iterables_for_filters{ LHCb::Event::make_zip<filter_tags::VectorBackend>( input_container )... };

      if constexpr ( num_filter_tags > 0 ) {
        // Execute each of the filters, looping over the map (quite possibly there is only one entry)
        for ( auto iters = std::tuple{ std::get<Filter_Is>( filter_functors ).begin()... };
              iters != std::tuple{ std::get<Filter_Is>( filter_functors ).end()... };
              ( ++std::get<Filter_Is>( iters ), ... ) ) {
          // Get all the different cache/cling scalar/vector combinations
          auto const functors = std::tie( std::get<Filter_Is>( iters )->second... );
          // execute them on their prepared input types
          std::tuple const filtered_containers{
              std::get<Filter_Is>( functors )( std::get<Filter_Is>( iterables_for_filters ) )... };
          // not much more to do...check the output sizes are all the same
          std::array const filtered_sizes{ filtered_size( std::get<Filter_Is>( filtered_containers ) )... };
          // Check that they all match
          if ( !all_same( filtered_sizes ) ) {
            std::ostringstream oss;
            oss << "Got filter filtered_sizes = " << filtered_sizes;
            throw exception( oss.str() );
          }
        }
      }

      // Get all the dumping functors/functions
      auto const all_functors = std::tie( this->template getFunctorMap<dump_tags>()... );

      // Prepare an input container that's suitable for each set of functors.
      // This is more iterable containers than strictly necessary, but it
      // shouldn't be too expensive...
      auto const       input_size = input_container.size();
      std::tuple const iterable_containers{ LHCb::Event::make_zip<dump_tags::VectorBackend>( input_container )... };

      // The sets of functors are all the same length, so we can loop through them in lockstep
      auto variant_iter = m_variants.begin();
      for ( auto iters =
                std::tuple{
                    std::get<Dump_Is>( all_functors ).begin()...,
                };
            variant_iter != m_variants.end() && iters != std::tuple{ std::get<Dump_Is>( all_functors ).end()... };
            ++variant_iter, ( ++std::get<Dump_Is>( iters ), ... ) ) {
        // We already checked that all of the names are the same
        auto const& functor_name = std::get<0>( iters )->first;
        // variant<int, bool, float> initialised with the relevant plain data
        // type for this set of functors -- this is just a convenient way of
        // despatching to something that can assume the correct type
        auto const& variant = *variant_iter;
        std::visit(
            [&]( auto plain_value ) {
              // Argument just used to deduce a type, its value isn't relevant
              using data_t = decltype( plain_value );
              // Make some storage to collect the results of evaluating all of the different functors
              std::array<std::vector<data_t>, sizeof...( Dump_Is )> functor_values;
              // Because SIMDWrapper-like .store( ptr ) always writes a vector width past ptr we need
              // either special handling, or to round up the capacity. Take the second option here.
              ( functor_values[Dump_Is].reserve( LHCb::Event::safe_simd_capacity<data_t>( input_size ) ), ... );
              // ...but make sure that .size() will return the right thing.
              ( functor_values[Dump_Is].resize( input_size ), ... );
              // Use the zips in 'iterable_containers' to evaluate this functor on all
              // rows of the input container, operating in appropriately-sized chunks
              ( fill_container<typename dump_tags::template return_t<data_t>>( std::get<Dump_Is>( iterable_containers ),
                                                                               std::get<Dump_Is>( iters )->second,
                                                                               functor_values[Dump_Is] ),
                ... );
              // Now do a scalar loop, checking that all of the values in
              // 'functor_values' are finite and consistent with one another
              for ( auto i = 0; i < int( input_size ); ++i ) {
                std::array const values{ functor_values[Dump_Is][i]... };
                if ( std::any_of( values.begin(), values.end(), [&]( data_t value ) {
                       if ( !approx_equal( value, values.front() ) ) { return true; }
                       if constexpr ( !std::is_same_v<data_t, bool> ) {
                         // Don't want to ask if a bool is finite (and it doesn't
                         // work anyway, because vector<bool> is specialised)
                         if ( !std::isfinite( value ) ) { return true; }
                       }
                       return false;
                     } ) ) {
                  std::ostringstream oss;
                  oss << "Got values = " << values << " for i = " << i << " and functor_name = " << functor_name;
                  throw exception( oss.str() );
                }
              }
            },
            variant );
      }
    }

  private:
    constexpr static auto approx_equal( int a, int b ) { return a == b; }
    constexpr static auto approx_equal( bool a, bool b ) { return a == b; }
    template <typename T>
    constexpr static auto approx_equal( T const& a, T const& b, float tolerance = 1e-6f ) {
      using std::abs;
      using std::max;
      auto const diff = abs( a - b );
      return ( diff < tolerance ) || ( diff < max( abs( a ), abs( b ) ) * tolerance );
    }

    template <typename result_t, typename iterable_container_t, typename functor_t, typename data_t>
    static void fill_container( iterable_container_t const& in, functor_t const& functor,
                                std::vector<data_t>& values ) {
      std::size_t offset{ 0 };
      for ( auto const& chunk : in ) {
        auto const mask       = chunk.loop_mask();
        auto const any_result = functor( chunk );
        auto const result     = std::any_cast<result_t>( any_result );
        if constexpr ( std::is_arithmetic_v<result_t> ) {
          // we're dealing with a "POD" functor, returning bool/int/float
          static_assert( std::is_same_v<std::decay_t<decltype( mask )>, SIMDWrapper::scalar::mask_v> );
          assert( mask.cast() );
          values[offset] = result;
        } else {
          // values is vector<int/bool/float>
          if constexpr ( std::is_same_v<data_t, bool> ) {
            // result is some mask_v type
            static_assert( std::is_same_v<std::decay_t<decltype( mask )>, result_t> );
            for ( auto i = 0ul; i < mask.size(); ++i ) { values[offset + i] = testbit( result, i ); }
          } else {
            // result is some int_v/float_v type
            result.store( &values[offset] );
          }
        }
        offset += popcount( mask );
      }
      if ( offset != in.size() ) { throw std::runtime_error( "size mismatch" ); }
    }

    template <typename container_t>
    static bool all_same( container_t const& container ) {
      return std::all_of( container.begin(), container.end(),
                          [&]( auto const& item ) { return item == container.front(); } );
    }

    template <typename T, std::size_t N>
    static bool erased_values_all_match_helper( std::array<std::any, N> const& any_values, bool& result ) {
      // If all of 'values' hold type 'T', set 'result' according to whether the
      // values all match and return true. Otherwise, return false.
      std::array<T, N> values;
      for ( auto i = 0ul; i < N; ++i ) {
        auto const* value_ptr = std::any_cast<T>( &any_values[i] );
        if ( !value_ptr ) { return false; }
        values[i] = *value_ptr;
      }
      // All types were T
      result = all_same( values );
      return true;
    }

    template <typename... Ts, std::size_t N>
    static bool erased_values_all_match( std::array<std::any, N> const& values ) {
      bool ret;
      if ( ( erased_values_all_match_helper<Ts>( values, ret ) || ... ) ) {
        // one of the types in Ts... was appropriate, and ret contains the result of the comparison
        return ret;
      }
      std::ostringstream oss;
      oss << "erased_values_all_match did not support types:";
      for ( auto const& value : values ) { oss << ' ' << System::typeinfoName( value.type() ); }
      throw std::runtime_error( oss.str() );
    }
  };

  /** It's a bit annoying to need this 'SkipEvaluation' flag, but when the pack
   *  is empty you cannot use the 3rd argument to distinguish the two cases.
   */
  template <typename... Ts>
  using Initialise = InitialiseAndOptionallyEvaluate<true, boost::mp11::mp_list<Ts...>>;
  template <typename... Ts>
  using Evaluate = InitialiseAndOptionallyEvaluate<false, boost::mp11::mp_list<Ts...>, Ts...>;
} // namespace ThOr::Functors::Tests
