/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"
#include "LoKi/PhysExtract.h"

using counter_t       = Gaudi::Accumulators::SummingCounter<unsigned int>;
using out_particles_t = LHCb::Particle::Selection;
using in_particles_t  = const LHCb::Particle::Range&;

namespace LHCb {
  /**
   * Trivial algorithm to extract all particles from a container,
   * this includes basics, but also the composites.
   *
   * When ForceUnique is set, it is checked whether there aren't any
   * duplicates in the output container, where a duplicate particle
   * is defined through (particle_1 == particle_2).
   *
   * @author Laurent Dufour
   */
  class FlattenDecayTree final : public Algorithm::Transformer<out_particles_t( in_particles_t )> {
  public:
    FlattenDecayTree( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, { "InputParticles", {} }, { KeyValue{ "OutputParticles", "" } } ) {}

    out_particles_t operator()( in_particles_t particles ) const override {
      LHCb::Particle::Selection all_particles;
      m_ninput_particles += particles.size();

      for ( const auto& rawParticle : particles ) {
        LoKi::Extract::particles( rawParticle, std::back_inserter( all_particles ) );
      }

      if ( m_only_unique.value() ) {
        sort( all_particles.begin(), all_particles.end() );
        auto end_unique = unique( all_particles.begin(), all_particles.end() );

        LHCb::Particle::Selection unqiue_particles( all_particles.begin(), end_unique );

        m_noutput_particles += unqiue_particles.size();

        return unqiue_particles;
      } else {
        m_noutput_particles += all_particles.size();

        return all_particles;
      }
    }

  private:
    Gaudi::Property<bool> m_only_unique{ this, "ForceUnique", true };
    mutable counter_t     m_ninput_particles{ this, "# input particles" };
    mutable counter_t     m_noutput_particles{ this, "# output particles" };
  };

  DECLARE_COMPONENT_WITH_ID( FlattenDecayTree, "FlattenDecayTree" )
} // namespace LHCb
