/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "Functors/Filter.h"
#include "Functors/with_functors.h"
#include "Gaudi/Accumulators/StaticHistogram.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/EventLocalUnique.h"
#include "LHCbAlgs/Consumer.h"
#include "MonitorProperties.h"
#include "SelKernel/TrackZips.h"
#include "SelKernel/Utilities.h"
#include <array>
#include <vector>

namespace SelAlgorithms::monitoring {

  namespace {

    // FIXME: this mapping is a bit silly -- at the time the histogram is defined, we _know_ what
    // it takes to fill it -- so the 'return' type of any functor used should match the definition
    // of the histogram axis...

    template <typename T, typename... U>
    auto mk_map() {
      std::unordered_map<std::type_index, T ( * )( const std::any& )> m;
      ( ( m.emplace( std::type_index( typeid( U ) ),
                     []( std::any const& a ) {
                       return static_cast<T>( LHCb::Utils::as_arithmetic( std::any_cast<U const&>( a ) ) );
                     } ) ),
        ... );
      assert( m.size() == sizeof...( U ) );
      return m;
    }

    template <typename T>
    auto const cmap = mk_map<T, bool, float, double, short, unsigned short, int, unsigned int, long, unsigned long,
                             long long, unsigned long long, SIMDWrapper::scalar::int_v, SIMDWrapper::scalar::float_v,
                             SIMDWrapper::scalar::mask_v>();

    template <typename T>
    T as_( std::any const& i ) {
      return cmap<T>.at( i.type() )( i );
    }

    // Define a helper with the input and item types
    // defining the template arguments, and the signature of the functor.

    // Implementation for a single input container (must be a zip)
    template <typename Ts>
    struct MonitorVariable {
      using ItemType =
          typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar,
                                           Ts>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
      using Signature = std::any( ItemType const& );
    };

    // Implementation for a particle range
    template <>
    struct MonitorVariable<LHCb::Particle::Range> {
      using ItemType  = typename LHCb::Particle::Range::value_type;
      using Signature = std::any( ItemType const& );
    };

    // Implementation for a LHCb variant
    template <class... Ts>
    struct MonitorVariable<LHCb::variant<Ts...>> {
      using ItemType  = std::variant<typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar, Ts>::template zip_proxy_type<
          LHCb::Pr::ProxyBehaviour::Contiguous>...>;
      using Signature = std::any( ItemType const& );
    };

    // Signature for global (e.g. event) variables
    template <>
    struct MonitorVariable<void> {
      using Signature = std::any();
    };

    // Define the actual consumer type of objects working with the given
    // input type, allowing to treat consumers with no inputs when the
    // type is "void".
    template <class InputType>
    struct consumer_type_for_input_type {
      using type = LHCb::Algorithm::Consumer<void( InputType const& )>;
    };

    template <>
    struct consumer_type_for_input_type<void> {
      using type = LHCb::Algorithm::Consumer<void()>;
    };

    template <class InputType>
    using consumer_type_for_input_type_t = typename consumer_type_for_input_type<InputType>::type;

    // Determine the underlying STL-like iterable object that will be used for a particular
    // input type, treating consumers with no input arguments correctly when the input type
    // is set to "void".
    template <class InputType>
    struct underlying_iterable_type_for_input_type {

      using type = decltype( LHCb::Event::make_zip<SIMDWrapper::Scalar>( std::declval<InputType const&>() ) );

      type operator()( InputType const& cont ) const { return LHCb::Event::make_zip<SIMDWrapper::Scalar>( cont ); }
    };

    template <>
    struct underlying_iterable_type_for_input_type<LHCb::Particle::Range> {

      using type = LHCb::Particle::Range;

      type const& operator()( LHCb::Particle::Range const& cont ) const { return cont; }
    };

    template <>
    struct underlying_iterable_type_for_input_type<void> {
      using type = void;
    };

    template <class InputType>
    using underlying_iterable_type_for_input_type_t = typename underlying_iterable_type_for_input_type<InputType>::type;

  } // namespace

  // Base class for objects that fill histograms from an input container
  template <class InputType>
  struct histogram_wrapper_base {
    virtual ~histogram_wrapper_base()     = default;
    virtual void fill( InputType const& ) = 0;
  };

  // Base class for objects that fill histograms from global (e.g. event) information
  template <>
  struct histogram_wrapper_base<void> {
    virtual ~histogram_wrapper_base() = default;
    virtual void fill()               = 0;
  };

  // Convenient class to do type erasure and allow to process histograms independently of the number of dimensions
  template <class InputType, std::size_t N>
  class general_histogram_wrapper_base : public histogram_wrapper_base<InputType> {
  public:
    // Base class
    using base_class = histogram_wrapper_base<InputType>;
    // Functor type that computes the value of a variable
    using functor_type = Functors::Functor<typename MonitorVariable<InputType>::Signature>;
    // Collection of functors (one per dimension)
    using functors_pointer_type = std::unique_ptr<std::array<functor_type, N>>;
    // Type of the Gaudi histogram
    using histogram_type =
        Gaudi::Accumulators::StaticHistogram<N, Gaudi::Accumulators::atomicity::full, histogram_value_type>;
    // The type of the axes in the histogram
    using axis_type = Gaudi::Accumulators::Axis<histogram_value_type>;

    // No default constructor
    general_histogram_wrapper_base() = delete;

    // Build the histogram wrapper froin the pointer to the owning algorithm and its attributes
    template <class Owner, class... Axis>
    general_histogram_wrapper_base( Owner* owner, std::string const& name, std::string const& title,
                                    functors_pointer_type&& functors, Axis&&... axis )
        : base_class{}
        , m_functors_ptr{ std::move( functors ) }
        , m_histogram( owner, name, title, axis_type( std::forward<Axis>( axis ) )... ) {
      static_assert( sizeof...( Axis ) == N,
                     "The number of axes provided must match the number of dimensions of the histogram" );
    }

    virtual ~general_histogram_wrapper_base() = default;

  protected:
    // Collection of functors; one per dimension
    functors_pointer_type m_functors_ptr;
    // Pointer to the underlying histogram
    histogram_type m_histogram;
  };

  /*
   * Store a histogram using an unique pointer. This way we avoid doing any
   * kind of copy/move operations when passing the histogram objects to
   * containers, which would lead to compilation errors due to the absence
   * of copy/move constructors in the histogram class.
   */
  template <class InputType, std::size_t N>
  class histogram_wrapper : public general_histogram_wrapper_base<InputType, N> {
  public:
    // Base class
    using base_class = general_histogram_wrapper_base<InputType, N>;
    // Inherit constructors
    using base_class::base_class;

    // Fill the histogram using the values extracted from calls of the functors to the elements in the container
    void fill( InputType const& container ) override { this->fill_impl( container, std::make_index_sequence<N>() ); }

  private:
    // Implementation of the action of filling the underlying histogram
    template <std::size_t... I>
    void fill_impl( InputType const& container, std::index_sequence<I...> ) {
      for ( auto const& item : container ) {
        auto values = std::array{ std::invoke( this->m_functors_ptr->at( I ), item )... };
        if constexpr ( sizeof...( I ) == 1 ) {
          // for 1D histograms we can not pass a tuple
          ++( this->m_histogram )[as_<float>( std::get<0>( values ) )]; // TODO: check whether m_histogram want floats
                                                                        // or something else...
        } else {
          ++( this->m_histogram )[{ as_<float>( std::get<I>( values ) )... }]; // TODO: check whether m_histogram want
                                                                               // floats or something else...
        }
      }
    }
  };

  // Histogram wrapper for global (e.g. event) variables
  template <std::size_t N>
  class histogram_wrapper<void, N> : public general_histogram_wrapper_base<void, N> {
  public:
    // Base class
    using base_class = general_histogram_wrapper_base<void, N>;
    // Inherit constructors
    using base_class::base_class;

    // Fill the histogram using the values extracted from calls to the functors
    void fill() override { this->fill_impl( std::make_index_sequence<N>() ); }

  private:
    // Implementation of the action of filling the underlying histogram
    template <std::size_t... I>
    void fill_impl( std::index_sequence<I...> ) {
      auto values = std::array{ std::invoke( this->m_functors_ptr->at( I ) )... };
      if constexpr ( sizeof...( I ) == 1 ) {
        // for 1D histograms we can not pass a tuple, but just the only argument
        ++( this->m_histogram )[as_<float>( std::get<0>( values ) )]; // TODO: check whether m_histogram want floats or
                                                                      // something else...
      } else {
        ++( this->m_histogram )[{ as_<float>( std::get<I>( values ) )... }]; // TODO: check whether m_histogram want
                                                                             // floats or something else...
      }
    }
  };

  namespace {
    // Make a wrapper around a histogram and return an unique pointer typed on the base class
    template <class InputType, class Owner, class... FunctorDescriptionAndAxis, std::size_t... I>
    auto make_monitor_histogram_wrapper_ptr_impl( Owner* owner, std::string const& name, std::string const& title,
                                                  std::index_sequence<I...>, FunctorDescriptionAndAxis&&... fax ) {
      // Extract the functor descriptors and register them in the factory. Note that the functors
      // must be present in a pointer because the factory needs to store the address.
      using functor_type = Functors::Functor<typename MonitorVariable<InputType>::Signature>;
      auto functors_ptr  = std::make_unique<std::array<functor_type, sizeof...( FunctorDescriptionAndAxis )>>();
      ( owner->functor_factory().register_functor( owner, std::get<I>( *functors_ptr ), fax.functor_desc ), ... );
      // build the underlying histogram wrapper, where the return type is a pointer to the base class
      using histogram_wrapper_type = histogram_wrapper<InputType, sizeof...( FunctorDescriptionAndAxis )>;
      using axis_type              = typename histogram_wrapper_type::axis_type;
      using return_type = std::unique_ptr<histogram_wrapper_base<underlying_iterable_type_for_input_type_t<InputType>>>;
      return return_type{ std::make_unique<histogram_wrapper_type>(
          owner, name, title, std::move( functors_ptr ), axis_type( fax.bins, fax.min, fax.max, fax.label )... ) };
    }

    // Make an unique pointer to an object wrapping any kind of histogram
    template <class InputType, class Owner, class... FunctorDescriptionAndAxis>
    auto make_monitor_histogram_wrapper_ptr( Owner* owner, std::string const& name, std::string const& title,
                                             FunctorDescriptionAndAxis&&... fax ) {
      return make_monitor_histogram_wrapper_ptr_impl<InputType>(
          owner, name, title, std::make_index_sequence<sizeof...( FunctorDescriptionAndAxis )>(),
          std::forward<FunctorDescriptionAndAxis>( fax )... );
    }

    // Helper class to make a histogram pointer based on the configuration
    template <class InputType, class MonitorHistogramConfig>
    struct make_histogram_ptr_t;

    template <class InputType>
    struct make_histogram_ptr_t<InputType, histogram_1d> {
      template <class Owner>
      auto operator()( Owner* owner, histogram_1d const& config ) const {
        return make_monitor_histogram_wrapper_ptr<InputType>( owner, config.name, config.title, config.axis );
      }
    };

    template <class InputType>
    struct make_histogram_ptr_t<InputType, histogram_2d> {
      template <class Owner>
      auto operator()( Owner* owner, histogram_2d const& config ) const {
        return make_monitor_histogram_wrapper_ptr<InputType>( owner, config.name, config.title, config.xaxis,
                                                              config.yaxis );
      }
    };
  } // namespace

  /** @class MonitorBase Monitor.h
   *
   * Base class for the monitoring classes. This class must exist to offer a common
   * base to monitor classes that act either on containers or on the global event
   * information (where the input type is "void").
   */
  template <class InputType>
  class MonitorBase : public consumer_type_for_input_type_t<InputType> {
  public:
    // The base class
    using base_type = consumer_type_for_input_type_t<InputType>;
    // The underlying input type when converting it to a STL-like container
    using underlying_iterable_type = underlying_iterable_type_for_input_type_t<InputType>;
    // The type of a collection of histogram configurations
    using histogram_configurations_type = std::vector<histogram_variant>;
    // The pointer type to a built histogram
    using histogram_pointer_type = std::unique_ptr<histogram_wrapper_base<underlying_iterable_type>>;
    // The type of a collection of built histogram pointers
    using histograms_type = std::vector<histogram_pointer_type>;

    // The derived class takes the responsability of passing the
    // right arguments to the algorithm constructor
    template <class... Arg>
    MonitorBase( Arg&&... arg ) : base_type( std::forward<Arg>( arg )... ) {}

    // Initialize the histograms from the configuration
    StatusCode initialize() override {

      auto sc = base_type::initialize();

      if ( sc.isFailure() ) return sc;

      m_histograms.reserve( m_configurations.value().size() );
      for ( auto const& config : m_configurations.value() )
        m_histograms.emplace_back( std::visit(
            [this]( auto const& c ) {
              return make_histogram_ptr_t<underlying_iterable_type, std::decay_t<decltype( c )>>{}( this, c );
            },
            config ) );

      return sc;
    }

    // Retrieve the functor factor
    Functors::IFactory& functor_factory() {
      m_functor_factory.retrieve().ignore();
      return *m_functor_factory;
    }

  protected:
    // properties
    Gaudi::Property<histogram_configurations_type> m_configurations{
        this, "Histograms", {}, "Histogram configurations" };
    mutable histograms_type           m_histograms;
    ServiceHandle<Functors::IFactory> m_functor_factory{ this, "FunctorFactory", "FunctorFactory" };
  };

  /** @class ContainerMonitor Monitor.h
   *
   *  ContainerMonitor<T> Base class to define the a monitoring algorithm that generates
   *  one or more histograms, with different dimensions.
   */
  template <typename InputType>
  class ContainerMonitor : public MonitorBase<InputType> {

  public:
    // Base class
    using base_class = MonitorBase<InputType>;
    // The only constructor
    ContainerMonitor( std::string const& name, ISvcLocator* pSvcLocator )
        : base_class( name, pSvcLocator, typename base_class::KeyValue{ "Input", "" } ) {}
    // Virtual method that should preprocess the container, if necessary, and call the "fill" member function
    void operator()( InputType const& in ) const override = 0;

  protected:
    // The actual function that fills the histograms from the container
    template <class Container>
    void fill( Container const& input_container ) const {
      for ( auto& hist : this->m_histograms )
        hist->fill(
            underlying_iterable_type_for_input_type<std::decay_t<decltype( input_container )>>{}( input_container ) );
    }
  };

  /** @class Monitor Monitor.h
   *
   * Monitor<T> evaluates a series of functors in the input type and generates a
   * histogram. If the type is "void" then the functors are assumed to act on the event.
   *
   *  @tparam T The selected object type (e.g. Track, Particle, ...). If multiple template
   * arguments are provided, a zip container is created.
   */
  template <typename T>
  class Monitor final : public ContainerMonitor<T> {
  public:
    // Base class
    using base_class = ContainerMonitor<T>;
    // Inherit constructors
    using base_class::base_class;
    // Fill the histograms with the given container
    void operator()( T const& in ) const override { this->fill( in ); }
  };

  // Monitor class for variants
  template <typename... T>
  class Monitor<LHCb::variant<T...>> final : public ContainerMonitor<LHCb::variant<T...>> {
  public:
    // Base class
    using base_class = ContainerMonitor<LHCb::variant<T...>>;
    // The input type
    using input_type = LHCb::variant<T...>;
    // Inherit constructors
    using base_class::base_class;
    // Visit the input variant and call the "fill" member function with the underlying container
    void operator()( input_type const& in ) const override {
      LHCb::invoke_or_visit( [&]( auto const& x ) { this->fill( x ); }, in );
    }
  };

  // Monitor class for global (e.g. event) variables
  template <>
  class Monitor<void> final : public MonitorBase<void> {
  public:
  public:
    // Base class
    using base_class = MonitorBase<void>;
    // Inherit constructors
    using base_class::base_class;
    // Fill all the histograms
    void operator()() const override {
      for ( auto& hist : this->m_histograms ) hist->fill();
    }
  };

} // namespace SelAlgorithms::monitoring
