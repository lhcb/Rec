/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrimaryVertices.h"
#include "Event/Track_v3.h"
#include "LHCbAlgs/Transformer.h"
#include "SelKernel/VertexRelation.h"

namespace {
  using Output = BestVertexRelations;
  // using Vertices = LHCb::Event::v2::RecVertices const&;
  using Vertices = LHCb::Event::PV::PrimaryVertexContainer const&;
  template <typename PrTracks>
  using Base = LHCb::Algorithm::Transformer<Output( PrTracks const&, Vertices )>;
} // namespace

/** @class MakePVRelations MakePVRelations.cpp
 *  @brief Produce a PV relations container from the given objects and vertices
 *
 *  This creates a new BestVertexRelations container that can be zipped with
 *  the given input container to provide relations to the given set of
 *  vertices.
 */
template <typename PrTracks>
struct MakePVRelations final : public Base<PrTracks> {
  using typename Base<PrTracks>::KeyValue;
  MakePVRelations( const std::string& name, ISvcLocator* pSvcLocator )
      : Base<PrTracks>( name, pSvcLocator, { KeyValue{ "Input", "" }, KeyValue{ "InputVertices", "" } },
                        { "Output", "" } ) {}
  Output operator()( PrTracks const& in, Vertices vertices ) const override {
    return Sel::calculateBestVertices( in, vertices );
  }
};

using MakePVRelations__PrFittedForwardTracks = MakePVRelations<LHCb::Event::v3::Tracks>;
DECLARE_COMPONENT_WITH_ID( MakePVRelations__PrFittedForwardTracks, "MakePVRelations__PrFittedForwardTracks" )
