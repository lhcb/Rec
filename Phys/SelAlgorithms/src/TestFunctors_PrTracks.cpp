/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TestFunctors.h"

#include "Event/Track_v3.h"
#include "SelKernel/TrackZips.h"

DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Event::v3::Tracks>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Event::v3::Tracks>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracks" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Event::v3::TracksWithPVs>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Event::v3::TracksWithPVs>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracksWithPVs" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Event::v3::TracksWithMuonID>,
                           "TestThOrFunctorEvaluation__PrFittedForwardTracksWithMuonID" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Event::v3::TracksWithMuonID>,
                           "TestThOrFunctorInitialisation__PrFittedForwardTracksWithMuonID" )
