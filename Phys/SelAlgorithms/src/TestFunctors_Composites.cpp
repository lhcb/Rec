/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TestFunctors.h"

#include "Event/Particle_v2.h"

DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Evaluate<LHCb::Event::Composites>,
                           "TestThOrFunctorEvaluation__Composites" )
DECLARE_COMPONENT_WITH_ID( ThOr::Functors::Tests::Initialise<LHCb::Event::Composites>,
                           "TestThOrFunctorInitialisation__Composites" )
