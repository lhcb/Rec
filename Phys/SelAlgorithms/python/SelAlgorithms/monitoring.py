###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Python wrappers over C++ objects passed to the monitoring algorithms
"""

import Functors
import PyConf.Algorithms as algorithms
from PyConf.dataflow import DataHandle


class histogram_axis(object):
    """
    Object wrapping a exposed C++ monitoring class that represents a
    histogram axis
    """

    __slots__ = "functor", "bins", "min", "max", "label"

    def __init__(
        self,
        functor: Functors.Functor,
        bins: int,
        range: tuple[float, float],
        label: str,
    ):
        """
        Args:
            functor (Functors.Functor): any functor object which can be
            translated into a functor descriptor by calling the
            "__str__" method.
            bins (int): number of bins
            range (tuple[float, float]): minimum and maximum bounds
            label (str): axis label
        """
        super().__init__()
        # expand the range here to get clearer errors
        min, max = range
        # the functor is converted to a string and parsed following the grammar
        self.functor = functor
        self.bins = bins
        self.min = min
        self.max = max
        self.label = label

    def __iter__(self):
        for n in self.__slots__:
            yield getattr(self, n)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(tuple(self))

    def data_dependencies(self):
        """
        Return a list of DataHandle inputs to the stored functor.

        This implements the API which allows PyConf to deduce that a functor
        property must 'inject' TES dependencies into its owning Algorithm.
        """
        return self.functor.data_dependencies()

    def to_json(self):
        """Representation of the underlying C++ class as a string"""
        return {k: getattr(self, k) for k in self.__slots__}


class histogram_1d(object):
    """
    Object wrapping a exposed C++ monitoring class that represents a
    1D histogram
    """

    __slots__ = ["name", "title", "axis"]

    def __init__(
        self,
        name: str,
        title: str,
        functor: Functors.Functor,
        bins: int,
        range: tuple[float, float],
        label: str,
    ):
        """
        Args:
            name (str): name of the histogram in the ROOT file
            title (str): title of the histogram in the ROOT file
            functor (Functors.Functor): any functor object which can be
            translated into a functor descriptor by calling the
            "__str__" method.
            bins (int): number of bins
            range (tuple[float, float]): minimum and maximum bounds
            label (str): axis label
        """
        super().__init__()
        self.name = name
        self.title = title
        self.axis = histogram_axis(functor, bins, range, label)

    def __iter__(self):
        for n in self.__slots__:
            yield getattr(self, n)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(tuple(self))

    def data_dependencies(self):
        """
        Return a list of DataHandle inputs to the stored functor.

        This implements the API which allows PyConf to deduce that a functor
        property must 'inject' TES dependencies into its owning Algorithm.
        """
        return self.axis.data_dependencies()

    def to_json(self):
        """Representation of the underlying C++ class as a string"""
        return {"name": self.name, "title": self.title, "axis": self.axis.to_json()}


class histogram_2d(object):
    """
    Object wrapping a exposed C++ monitoring class that represents a
    2D histogram
    """

    __slots__ = "name", "title", "xaxis", "yaxis"

    def __init__(
        self, name: str, title: str, xaxis: histogram_axis, yaxis: histogram_axis
    ):
        """
        Args:
            name (str): name of the histogram in the ROOT file
            title (str): title of the histogram in the ROOT file
            xaxis (histogram_axis): x-axis of the histogram
            haxis (histogram_axis): y-axis of the histogram
        """
        super().__init__()
        self.name = name
        self.title = title
        self.xaxis = xaxis
        self.yaxis = yaxis

    def __iter__(self):
        for n in self.__slots__:
            yield getattr(self, n)

    def __repr__(self):
        return str(self)

    def __str__(self):
        return str(tuple(self))

    def data_dependencies(self):
        """
        Return a list of DataHandle inputs to the stored functor.

        This implements the API which allows PyConf to deduce that a functor
        property must 'inject' TES dependencies into its owning Algorithm.
        """
        return self.xaxis.data_dependencies() + self.yaxis.data_dependencies()

    def to_json(self):
        return {
            "name": self.name,
            "title": self.title,
            "xaxis": self.xaxis.to_json(),
            "yaxis": self.yaxis.to_json(),
        }


def monitor(
    *,
    histograms: list[histogram_1d, histogram_2d],
    data: DataHandle = None,
    data_type: str = "ParticleRange",
    **kwargs,
):
    """
    Function that builds a monitoring algorithm as a function of
    the data object that is passed.

    Args:
        histograms (histogram_1d or histogram_2d): collection of
        histogram configurations
        data (DataHandle or None): objects to make the histograms
        from. If set to None, a global monitor is created.
        data_type (str): alias of the container name
        kwargs (dict): keyword arguments forwarded to the monitor
        algorithm constructor
    """
    if data is None:
        return algorithms.Monitor__Global(Histograms=histograms, **kwargs)
    else:
        # In order to be able to access objects from the name, it is necessary
        # that the monitoring algorithms are exposed with a similar name
        # based on the input container, "Monitor__<container name>", where
        # the container name is equal to the concatenation of namespaces and
        # class types replacing the colons by underscores.
        #
        # Ideally we would be able to infer the type of data from the data
        # handler itself, but this seems to be hardly possible due to the
        # presence of type aliases and the usage of templates.
        try:
            monitor_cls = getattr(algorithms, f"Monitor__{data_type}")
        except AttributeError:
            raise TypeError(
                f'Unable to make a monitor for data handler with "\
                "type named "{data_type}" (unexisting binding for C++ class "\
                "with name "Monitor__{data_type}")'
            )
        return monitor_cls(Input=data, Histograms=histograms, **kwargs)
