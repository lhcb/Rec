/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once
// ============================================================================
// Include files
// ============================================================================
#include <algorithm>
// ============================================================================
// GaudiAlg
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IMessageSvc.h"

// ============================================================================
// Event
// ============================================================================
#include "Event/Particle.h"
#include "Event/VertexBase.h"
// ============================================================================
// LHCbMath
// ============================================================================
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/MatrixTransforms.h"
#include "LHCbMath/MatrixUtils.h"
// ============================================================================
// TrackInterfaces
// ============================================================================
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackStateProvider.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IParticleTransporter.h"
// ============================================================================
// KalmanFilter
// ============================================================================
#include "KalmanFilter/VertexFit.h"
#include "KalmanFilter/VertexFitWithTracks.h"
// ============================================================================
// Local
// ============================================================================
#include "ParticleClassificator.h"
// ============================================================================
/** @file
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2008-03-05
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class DistanceCalculatorBase DistanceCalculatorBase.h
   *
   *  It is the simplest implementation of the basic math,
   *  needed for the real implementation
   *  on the abstract interface IDistanceCalculator
   *  Essentially it relies on many nice functions, coded
   *
   *
   *  @see IDistanceCalculator
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date   2008-03-05
   */
  class DistanceCalculatorBase : public LoKi::ParticleClassificator {
  public:
    // ========================================================================
    /// get the actual type of the line
    typedef Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector> Line_;
    // ========================================================================
  public:
    // ========================================================================
    /// the error code
    enum {
      /// Invalid Particle
      InvalidParticle = 901, // Invalid Particle
      /// Invalid Vertex
      InvalidVertex = 902, // Invalid Vertex
      /// Invalid Track
      InvalidTrack = 903, // Invalid Vertex
      /// Invalid Data
      InvalidData = 904, // Invalid Data
      /// Error in Matrix Inversion
      ErrorInMatrixInversion = 905, // Error in Matrix Inversion
      /// Error from transporter
      ErrorFromTransporter = 906, // Error from Particle Transporter
      /// No Convergency
      NoConvergency = 910, // No Convergency is detected
      /// Not Implemented Yet
      NotImplemented = 920 // not yet implemented
    };
    // ========================================================================
  protected:
    // ========================================================================
    /// construct the line trajectory from the particle
    Line_ line( const LHCb::Particle& particle ) const;
    /// construct the line trajectory from the track
    Line_ line( const LHCb::State& state ) const;
    // ========================================================================
    /** find the impact parameter ("distance") vector
     *  of the particle with respect to some vertex
     *  @param particle (input) the particle
     *  @param vertex   (input) the vertex
     *  @param impact   (output) the evaluated impact parameter vector
     */
    void i_distance( const LHCb::Particle& particle, const LHCb::VertexBase& vertex, Gaudi::XYZVector& impact ) const;
    // =========================================================================
    /** find the impact parameter ("distance") vector
     *  of the particle with respect to some fixed point
     *  @param particle (input) the particle
     *  @param point    (input) the fixed point
     *  @param impact   (output) the evaluated impact parameter vector
     */
    void i_distance( const LHCb::Particle& particle, const Gaudi::XYZPoint& point, Gaudi::XYZVector& impact ) const;
    // ========================================================================
    /** find the points of the closest approach for two  particles
     *  @param particle1 (input) the first particle
     *  @param particle2 (input) the second particle
     *  @param point1    (output) the point on the first trajectory
     *  @param point2    (output) the point on the second trajectory
     */
    void i_distance( const LHCb::Particle& particle1, // the first particle
                     const LHCb::Particle& particle2, // the second particle
                     Gaudi::XYZPoint&      point1,    // the point on the first trajectory
                     Gaudi::XYZPoint&      point2 ) const;
    // ========================================================================
    /**  evaluate the  distance between two vertices
     *   @param vx1  (input) the first vertex
     *   @param vx2  (input) the second vertex
     *   @param dist (output) the distance between vertices
     *   @param chi2 (output,optional) the chi2 separation significance
     */
    StatusCode i_distance( const LHCb::VertexBase& vx1, const LHCb::VertexBase& vx2, double& dist,
                           double* chi2 = 0 ) const;
    // ========================================================================
    /**  evaluate the  distance between the vertex and the fixed point point
     *   @param vertex (input) the vertex
     *   @param point  (input) the fixed point
     *   @param dist   (output) the distance between vertices
     *   @param chi2   (output,optional) the chi2 separation significance
     */
    StatusCode i_distance( const LHCb::VertexBase& vertex, const Gaudi::XYZPoint& point, double& dist,
                           double* chi2 = 0 ) const;
    // ========================================================================
    /* evalute the "projected" distance
     *
     *  \f$s=\frac{\left(\vec{\mathbf{v}}\vec{\mathbf{p}}
     *     \right)}{\left|\vec{\mathbf{p}}\right|}\f$
     *  where vector \f$\vec{\mathbf{v}}\f$ is a vector from
     *  the primary to the secondary vertex:
     *    \f$\vec{\mathbf{v}}=\vec{\mathbf{x}}_{d}-\vec{\mathbf{x}}_{pv}\f$,
     *
     * @param primary  (input)  the production vertex
     * @param particle (input)  the particle
     * @param decay    (input)  the decay particle
     * @param return the projected distance
     * @return status code
     */
    double i_distance( const LHCb::VertexBase& primary, const LHCb::Particle& particle,
                       const LHCb::VertexBase& decay ) const;
    // ========================================================================
  protected: // track-related distances
    // ========================================================================
    /** find the impact parameter ("distance") vector
     *  of the particle with respect to some vertex
     *  @param state    (input) the track state
     *  @param vertex   (input) the vertex
     *  @param impact   (output) the evaluated impact parameter vector
     */
    void i_distance( const LHCb::State& state, const LHCb::VertexBase& vertex, Gaudi::XYZVector& impact ) const;
    // =========================================================================
    /** find the impact parameter ("distance") vector
     *  of the particle with respect to some fixed point
     *  @param state    (input) the track state
     *  @param point    (input) the fixed point
     *  @param impact   (output) the evaluated impact parameter vector
     */
    void i_distance( const LHCb::State& state, const Gaudi::XYZPoint& point, Gaudi::XYZVector& impact ) const;
    // =========================================================================
    /** find the points of the closest approach for two tracks
     *  @param state1    (input) the first particle
     *  @param state2    (input) the second particle
     *  @param point1    (output) the point on the first trajectory
     *  @param point2    (output) the point on the second trajectory
     */
    void i_distance( const LHCb::State& state1, // the first track
                     const LHCb::State& state2, // the second track
                     Gaudi::XYZPoint&   point1, // the point on the first trajectory
                     Gaudi::XYZPoint&   point2 ) const;
    // ========================================================================
  protected:
    // ========================================================================
    /** check the validity of the particle
     *  @param p pointer to the particle
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Particle* v ) const;
    /** check the validity of the vertex
     *  @param v pointer to the vertex
     *  @return StatusCode
     */
    StatusCode check( const LHCb::VertexBase* v ) const;
    /** check the validity of the particles
     *  @param p1 pointer to the first particle
     *  @param p2 pointer to the second particle
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const;
    /** check the validity of the particle and the vertex
     *  @param p pointer to the particle
     *  @param v pointer to the vertex
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Particle* p, const LHCb::VertexBase* v ) const;
    /** check the validity of the vertices
     *  @param v1 pointer to the first vertex
     *  @param v2 pointer to the second vertex
     *  @return StatusCode
     */
    StatusCode check( const LHCb::VertexBase* v1, const LHCb::VertexBase* v2 ) const;
    /** check the validity of the track
     *  @param t pointer to the track
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Track* t ) const;
    /** check the validity of the tracks
     *  @param t1 pointer to the first  track
     *  @param t2 pointer to the second track
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Track* t1, const LHCb::Track* t2 ) const;
    /** check the validity of track & vertex
     *  @param t pointer to the track
     *  @param v pointer to the vertex
     *  @return StatusCode
     */
    StatusCode check( const LHCb::Track* t, const LHCb::VertexBase* v ) const;
    // ========================================================================
  protected:
    // ========================================================================
    /** Check if the new z position is inside a sensible region
     *  Set it to the maximum if too large
     *  @param newZ     (input) new position
     *  @return double
     */
    double checkNewZ( const double newZ ) const {
      return std::clamp( newZ, m_detectorRegion.value().first, m_detectorRegion.value().second );
    }

    // ========================================================================
    /** transport the particle to a certain Z position
     *  @param particle (input) the particle to be transported
     *  @param newZ     (input) new position
     *  @param transported (output) the transported particle
     *  @return status code
     */
    StatusCode transport( LHCb::Particle const* particle, double newZ, LHCb::Particle& transported,
                          IGeometryInfo const& geometry ) const {
      return m_transporter->transport( particle, checkNewZ( newZ ), transported, geometry );
    }
    // ========================================================================
    /** transport the particle to a certain Z position
     *  @param particle (input) the particle to be transported
     *  @param z        (input) new position
     *  @param transported (output) the transported particle
     *  @return status code
     */
    StatusCode transport( LHCb::Particle const* particle, Gaudi::XYZPoint const& z, LHCb::Particle& transported,
                          IGeometryInfo const& geometry ) const {
      return transport( particle, z.Z(), transported, geometry );
    }
    // ========================================================================
    /** transport the particle to a certain Z position
     *  @param particle (input) the particle to be transported
     *  @param newZ     (input) new position
     *  @param transported (output) the transported particle
     *  @return status code
     */
    StatusCode transportAndProject( LHCb::Particle const* particle, double newZ, LHCb::Particle& transported,
                                    IGeometryInfo const& geometry ) const {
      return m_transporter->transportAndProject( particle, checkNewZ( newZ ), transported, geometry );
    }
    // ========================================================================
    /** transport the particle to a certain Z position
     *  @param particle (input) the particle to be transported
     *  @param z        (input) new position
     *  @param transported (output) the transported particle
     *  @return status code
     */
    StatusCode transportAndProject( LHCb::Particle const* particle, Gaudi::XYZPoint const& z,
                                    LHCb::Particle& transported, IGeometryInfo const& geometry ) const {
      return transportAndProject( particle, z.Z(), transported, geometry );
    }
    // ========================================================================
  protected:
    // ========================================================================
    /// get particle transporter
    IParticleTransporter const* transporter() const { return m_transporter.get(); }
    // ========================================================================
    /// get state provider
    ITrackStateProvider const* stateProvider() const { return m_stateProvider.get(); }
    // ========================================================================
    /// get track extrapolator
    ITrackExtrapolator const* extrapolator() const { return m_extrapolator.get(); }
    // ========================================================================
  protected:
    // ========================================================================
    /// get the state from the track
    const LHCb::State& state( const LHCb::Track& t ) const {
      const LHCb::State* s = t.stateAt( LHCb::State::Location::ClosestToBeam );
      return s ? *s : t.firstState();
    }
    // ========================================================================
    /// get the state from the track
    const LHCb::State& state( const LHCb::Track& t, const double z ) const { return t.closestState( z ); }
    // ========================================================================
  protected:
    // ========================================================================
    /** Standard constructor
     *  @param type tool type(?)
     *  @param name tool instance name
     *  @param parent the pointer to the parent
     */
    using ParticleClassificator::ParticleClassificator;
    // ========================================================================
  private:
    // ========================================================================
    /// Particle transporter tool
    PublicToolHandle<IParticleTransporter> m_transporter{ this, "Transporter", "DaVinci::ParticleTransporter" };
    // =======================================================================
    /// State provider tool
    PublicToolHandle<ITrackStateProvider> m_stateProvider{ this, "StateProvider", "TrackStateProvider" };
    // =======================================================================
    /// Track extrapolator tool
    PublicToolHandle<ITrackExtrapolator> m_extrapolator{ this, "TrackExtrapolator", "TrackMasterExtrapolator" };
    // =======================================================================
    // =======================================================================
    /// The z region allowed for extrapolations
    Gaudi::Property<std::pair<double, double>> m_detectorRegion{
        this, "DetectorRegion", { -10000., 20000. }, "The z region allowed for extrapolations " };
    // =======================================================================
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_no_particle{ this, "LHCb::Particle* points to NULL" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_no_vertexbase{ this, "LHCb::VertexBase* points to NULL" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_no_track{ this, "LHCb::Track* points to NULL" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_matrix_inversion{ this, "Error in matrix inversion" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_same_vertex{ this, "distance(v,v): the same vertex" };
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// check the validity of the particle
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Particle* p ) const {
  if ( !p ) {
    ++m_no_particle;
    return StatusCode{ InvalidParticle };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// check the validity of the vertex
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::VertexBase* v ) const {
  if ( !v ) {
    ++m_no_vertexbase;
    return StatusCode{ InvalidVertex };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// check the validity of the particles
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const {
  if ( !p1 || !p2 ) {
    ++m_no_particle;
    return StatusCode{ InvalidParticle };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// check the validity of the particle and the vertex
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Particle* p, const LHCb::VertexBase* v ) const {
  if ( !p ) {
    ++m_no_particle;
    return StatusCode{ InvalidParticle };
  }
  if ( !v ) {
    ++m_no_vertexbase;
    return StatusCode{ InvalidVertex };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// check the validity of the vertices
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::VertexBase* v1, const LHCb::VertexBase* v2 ) const {
  if ( !v1 || !v2 ) {
    ++m_no_vertexbase;
    return StatusCode{ InvalidVertex };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
//  check the validity of the track
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Track* t ) const {
  if ( !t ) {
    ++m_no_track;
    return StatusCode{ InvalidTrack };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
//  check the validity of the tracks
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Track* t1, const LHCb::Track* t2 ) const {
  if ( !t1 || !t2 ) {
    ++m_no_track;
    return StatusCode{ InvalidTrack };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
//  check the validity of track & vertex
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::check( const LHCb::Track* t, const LHCb::VertexBase* v ) const {
  if ( !t ) {
    ++m_no_track;
    return StatusCode{ InvalidTrack };
  }
  if ( !v ) {
    ++m_no_vertexbase;
    return StatusCode{ InvalidVertex };
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// construct the line trajector from the particle
// ============================================================================
inline LoKi::DistanceCalculatorBase::Line_ LoKi::DistanceCalculatorBase::line( const LHCb::Particle& p ) const {
  return Line_( p.referencePoint(), p.momentum().Vect() );
}
// ============================================================================
// construct the line trajector from the tarck state
// ============================================================================
inline LoKi::DistanceCalculatorBase::Line_ LoKi::DistanceCalculatorBase::line( const LHCb::State& s ) const {
  return Line_( Gaudi::XYZPoint( s.x(), s.y(), s.z() ), Gaudi::XYZVector( s.tx(), s.ty(), 1.0 ) );
}
// ============================================================================
/*  find the impact parameter ("distance") vector
 *  of the particle with respect to some vertex
 */
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::Particle& particle, const LHCb::VertexBase& vertex,
                                                      Gaudi::XYZVector& impact ) const {
  i_distance( particle, vertex.position(), impact );
}
// ============================================================================
/*  find the impact parameter ("distance") vector
 *  of the particle with respect to some fixed point
 */
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::Particle& particle, const Gaudi::XYZPoint& point,
                                                      Gaudi::XYZVector& impact ) const {
  // (re)use the nice functions by Matt&Juan
  // std::cout << "point: " << point << '\n';
  // std::cout << "reference_point: " << particle.referencePoint() << '\n';
  // std::cout << "momentum: " << particle.momentum().Vect().Unit() << '\n';
  impact = Gaudi::Math::closestPoint( point, line( particle ) ) - point;
  // std::cout << "impact squared: " << impact.mag2() << '\n';
}
// ============================================================================
// find the points of the closest approach for two  particles
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::Particle& particle1, // the first particle
                                                      const LHCb::Particle& particle2, // the second particle
                                                      Gaudi::XYZPoint&      point1, // the point on the first trajectory
                                                      Gaudi::XYZPoint&      point2 ) const {
  double      mu1 = 0;
  double      mu2 = 0;
  const Line_ line1( line( particle1 ) );
  const Line_ line2( line( particle2 ) );

  // (re)use the nice functions by Matt&Juan
  Gaudi::Math::closestPointParams( line1, line2, mu1, mu2 );

  point1 = line1( mu1 ); // the point on the first tarjectory
  point2 = line2( mu2 ); // the point on the second trajectory
}
// ============================================================================
//  evaluate the  distance (and chi^2) between two vertices
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::i_distance( const LHCb::VertexBase& vx1, const LHCb::VertexBase& vx2,
                                                            double& dist, double* chi2 ) const {
  //
  if ( &vx1 == &vx2 ) {
    dist = 0;
    if ( chi2 ) { *chi2 = 0; }
    ++m_same_vertex;
    return StatusCode::SUCCESS;
  }
  // calculate the distance
  const Gaudi::XYZVector delta( vx1.position() - vx2.position() );
  dist = delta.R();
  if ( chi2 ) {
    // evaluate chi2:
    Gaudi::SymMatrix3x3 cov( vx1.covMatrix() + vx2.covMatrix() );
    if ( !cov.Invert() ) {
      ++m_matrix_inversion;
      return StatusCode{ ErrorInMatrixInversion };
    }
    // evaluate the chi2
    *chi2 = Gaudi::Math::Similarity( delta, cov );
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
//  evaluate the  distance (and chi^2) between
// ============================================================================
inline StatusCode LoKi::DistanceCalculatorBase::i_distance( const LHCb::VertexBase& v, const Gaudi::XYZPoint& p,
                                                            double& dist, double* chi2 ) const {
  // calculate the distance
  const Gaudi::XYZVector delta( v.position() - p );
  dist = delta.R();
  if ( chi2 ) {
    // evaluate chi2:
    Gaudi::SymMatrix3x3 cov( v.covMatrix() );
    if ( !cov.Invert() ) {
      ++m_matrix_inversion;
      return StatusCode{ ErrorInMatrixInversion };
    }
    // evaluate the chi2
    *chi2 = Gaudi::Math::Similarity( delta, cov );
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// evalute the "projected" distance
// ============================================================================
inline double LoKi::DistanceCalculatorBase::i_distance( const LHCb::VertexBase& primary, const LHCb::Particle& particle,
                                                        const LHCb::VertexBase& decay ) const {
  // decay position
  const Gaudi::XYZPoint& vd = decay.position();
  // origin position
  const Gaudi::XYZPoint& vp = primary.position();
  // the unit vector along the momentum
  const Gaudi::XYZVector p = particle.momentum().Vect().Unit();
  //
  return ( vd - vp ).Dot( p );
}
// ============================================================================
// track-related distances
// ============================================================================
/* find the impact parameter ("distance") vector
 *  of the particle with respect to some vertex
 *  @param state    (input) the track state
 *  @param vertex   (input) the vertex
 *  @param impact   (output) the evaluated impact parameter vector
 */
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::State& state, const LHCb::VertexBase& vertex,
                                                      Gaudi::XYZVector& impact ) const {
  return i_distance( state, vertex.position(), impact );
}
// ============================================================================
/* find the impact parameter ("distance") vector
 *  of the particle with respect to some fixed point
 *  @param state    (input) the track state
 *  @param point    (input) the fixed point
 *  @param impact   (output) the evaluated impact parameter vector
 */
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::State& state, const Gaudi::XYZPoint& point,
                                                      Gaudi::XYZVector& impact ) const {
  // (re)use the nice functions by Matt&Juan
  impact = Gaudi::Math::closestPoint( point, line( state ) ) - point;
}
// ============================================================================
/*  find the points of the closest approach for two tracks
 *  @param state1    (input) the first particle
 *  @param state2    (input) the second particle
 *  @param point1    (output) the point on the first trajectory
 *  @param point2    (output) the point on the second trajectory
 */
// ============================================================================
inline void LoKi::DistanceCalculatorBase::i_distance( const LHCb::State& state1, // the first track
                                                      const LHCb::State& state2, // the second track
                                                      Gaudi::XYZPoint&   point1, // the point on the first trajectory
                                                      Gaudi::XYZPoint&   point2 ) const {
  double mu1 = 0;
  double mu2 = 0;
  //
  const Line_ line1( line( state1 ) );
  const Line_ line2( line( state2 ) );

  // (re)use the nice functions by Matt&Juan
  Gaudi::Math::closestPointParams( line1, line2, mu1, mu2 );

  point1 = line1( mu1 ); // the point on the first tarjectory
  point2 = line2( mu2 ); // the point on the second trajectory
}
// ============================================================================
// The END
// ============================================================================
