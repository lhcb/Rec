/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IAlgorithm.h"
#include "GaudiKernel/SmartIF.h"
// ============================================================================
// PartProp
// ============================================================================
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleID.h"
#include "Kernel/ParticleProperty.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/Particle.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/IDecay.h"
#include "LoKi/Trees.h"
// ============================================================================
// Local
// ============================================================================
#include "ParticleClassificator.h"
// ============================================================================
/** @file
 *  Implementation file for class  LoKi::ParticleClassificator
 *  @see LoKi::ParticleClassificator
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   2010-11-11
 */
// ============================================================================
// standard constructor
// ============================================================================
LoKi::ParticleClassificator::ParticleClassificator( const std::string& type, const std::string& name,
                                                    const IInterface* parent )
    : base_class( type, name, parent )
    /// particle classification:
    , m_ppSvc( 0 )
    , m_longLived()
    , m_shortLived()
    , m_gammaLike( "gamma" )
    , m_gammaCLike( Decays::Trees::Invalid_<const LHCb::Particle*>() )
    , m_digammaLike( Decays::Trees::Invalid_<const LHCb::Particle*>() )
    , m_mergedPi0Like( "pi0" )
    , m_jetsLike( Decays::Nodes::Invalid() )
    //
    , m_dd_gammaC( " gamma -> e+ e-    " )
    , m_dd_digamma( " [ ( pi0 -> <gamma> <gamma> ) , ( eta -> <gamma> <gamma> ) , <pi0> ] " )
    , m_dd_jets( " CELLjet | CLUSjet " )
    //
    , m_unclassified()
    , m_gamma_like()
    , m_gammaC_like()
    , m_digamma_like()
    , m_mergedPi0_like()
    , m_jets_like()
    , m_rhoplus_like()
//
{
  // ==========================================================================
  declareProperty( "GammaCDecays", m_dd_gammaC, "The gammaC-decays" );
  declareProperty( "DiGammaDecays", m_dd_digamma, "The di-gamma-decays" );
  declareProperty( "Jets", m_dd_jets, "Particles to be considered as jets" );
  // ==========================================================================
}
// ============================================================================
// initialize the tool
// ============================================================================
StatusCode LoKi::ParticleClassificator::initialize() {
  /// initialize the base
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) { return sc; } // RETURN
  //
  svc<IService>( "LoKiSvc", true );
  //
  // get particle property service
  m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
  // validate
  sc = m_longLived.validate( m_ppSvc )
           .orElse( [&] { error() << "Unable to validate Long-Lived particles" << endmsg; } )
           .andThen( [&] { return m_shortLived.validate( m_ppSvc ); } )
           .orElse( [&] { error() << "Unable to validate Short-Lived particles" << endmsg; } )
           .andThen( [&] { return m_gammaLike.validate( m_ppSvc ); } )
           .orElse( [&] { error() << "Unable to validate Gamma-Like particles" << endmsg; } )
           .andThen( [&] { return m_mergedPi0Like.validate( m_ppSvc ); } )
           .orElse( [&] { error() << "Unable to validate Merged-pi0-Like particles" << endmsg; } );
  if ( sc.isFailure() ) return sc;
  //
  // GammaC ?
  //
  if ( !m_dd_gammaC.empty() ) {
    // construct gamma_c descriptor
    Decays::IDecay* decay = tool<Decays::IDecay>( "LoKi::Decay/Decays" );
    //
    m_gammaCLike = decay->tree( m_dd_gammaC );
    if ( !m_gammaCLike.valid() ) {
      error() << "Unable to decode Gamma_c: '" << m_dd_gammaC << "'" << endmsg;
      return StatusCode::FAILURE;
    }
    //
    debug() << " Gamma_c  descriptor : " << m_gammaCLike << endmsg;
    //
    release( decay ).ignore();
  } else {
    // disable gamma_c treatment
    m_gammaCLike = Decays::Trees::Any_<const LHCb::Particle*>();
    m_gammaCLike = Decays::Trees::Not_<const LHCb::Particle*>( m_gammaCLike );
    ++m_gammac_warning;
  }
  //
  // DiGamma  ?
  //
  if ( !m_dd_digamma.empty() ) { // construct digamma descriptors
    Decays::IDecay* decay = tool<Decays::IDecay>( "LoKi::Decay/Decays" );
    //
    m_digammaLike = decay->tree( m_dd_digamma );
    if ( !m_digammaLike.valid() ) { return Error( "Unable to decode DiGamma: '" + m_dd_digamma + "'" ); }
    //
    debug() << " Di-Gamma descriptor : " << m_digammaLike << endmsg;
    //
    release( decay ).ignore();
  } else {
    // disable di-gamma treatment
    m_digammaLike = Decays::Trees::Any_<const LHCb::Particle*>();
    m_digammaLike = Decays::Trees::Not_<const LHCb::Particle*>( m_digammaLike );
    ++m_digamma_warning;
  }
  //
  // Jets
  //
  if ( !m_dd_jets.empty() ) { // construct digamma descriptors
    Decays::IDecay* decay = tool<Decays::IDecay>( "LoKi::Decay/Decays" );
    //
    m_jetsLike = decay->node( m_dd_jets );
    if ( !m_jetsLike.valid() ) { return Error( "Unable to decode JetLike: '" + m_dd_jets + "'" ); }
    //
    debug() << " Jets-like descriptor : " << m_jetsLike << endmsg;
    //
    release( decay ).ignore();
  } else {
    // disable jets-like treatment
    m_jetsLike = Decays::Nodes::Not( Decays::Nodes::Any() );
    ++m_jets_warning;
  }
  //
  return sc;
}

// ========================================================================
// the standard finalization of the tool
// ========================================================================
StatusCode LoKi::ParticleClassificator::finalize() {
  // statistics:
  if ( msgLevel( MSG::DEBUG ) ) {
    m_shortLived.printAcceptedAsTable( debug() << "Short-Lived      particles : \n" ) << endmsg;
    m_longLived.printAcceptedAsTable( debug() << "Long-Lived       particles : \n" ) << endmsg;
    //
    // Gamma
    if ( !m_gamma_like.empty() ) {
      LHCb::ParticleProperties::printAsTable(
          m_gamma_like, debug() << "Gamma-like       particles : " << m_gammaLike << '\n', m_ppSvc )
          << endmsg;
    }
    // GammaC
    if ( !m_gammaC_like.empty() ) {
      LHCb::ParticleProperties::printAsTable(
          m_gammaC_like, debug() << "Gamma-conv-like  particles : " << m_gammaCLike << '\n', m_ppSvc )
          << endmsg;
    }
    // DiGamma
    if ( !m_digamma_like.empty() ) {
      LHCb::ParticleProperties::printAsTable(
          m_digamma_like, debug() << "DiGamma-like     particles : " << m_digammaLike << '\n', m_ppSvc )
          << endmsg;
    }
    //
    // Merged-pi0
    if ( !m_mergedPi0_like.empty() ) {
      LHCb::ParticleProperties::printAsTable(
          m_mergedPi0_like, debug() << "Merged-pi0-like  particles : " << m_mergedPi0Like << '\n', m_ppSvc )
          << endmsg;
    }
    //
    // jets-like
    if ( !m_jets_like.empty() ) {
      LHCb::ParticleProperties::printAsTable( m_jets_like, debug() << "Jets-like  particles : " << m_jetsLike << '\n',
                                              m_ppSvc )
          << endmsg;
    }
    //
    // rho+-like
    if ( !m_rhoplus_like.empty() ) {
      LHCb::ParticleProperties::printAsTable( m_rhoplus_like, debug() << "Rho+-like  particles : \n", m_ppSvc )
          << endmsg;
    }
    //
  }
  //
  if ( !m_unclassified.empty() ) {
    warning() << "Unclassified particles : \n";
    LHCb::ParticleProperties::printAsTable( m_unclassified, warning(), m_ppSvc ) << endmsg;
  }
  //
  // reset particle property service & functors
  m_ppSvc = 0;
  m_shortLived.setService( m_ppSvc ).ignore();
  m_longLived.setService( m_ppSvc ).ignore();
  m_gammaCLike  = Decays::Trees::Invalid_<const LHCb::Particle*>();
  m_digammaLike = Decays::Trees::Invalid_<const LHCb::Particle*>();
  //
  return extends::finalize();
}
// ============================================================================
// get the particle type
// ============================================================================
LoKi::KalmanFilter::ParticleType LoKi::ParticleClassificator::particleType_( const LHCb::Particle& p ) const {
  //
  const bool basic   = p.isBasicParticle();
  const bool neutral = 0 == p.charge();
  //
  if ( !basic && m_jetsLike( p.particleID() ) ) {
    m_jets_like.insert( p.particleID() );
    return LoKi::KalmanFilter::JetLikeParticle; // RETURN
  } else if ( !basic && m_gammaCLike( &p ) ) {
    m_gammaC_like.insert( p.particleID() );
    // ATTENTION! GammaC is *LONG_LIVED_PARTICLE*
    return LoKi::KalmanFilter::LongLivedParticle; // RETURN
  } else if ( neutral && m_mergedPi0Like( &p ) ) {
    m_mergedPi0_like.insert( p.particleID() );
    return LoKi::KalmanFilter::MergedPi0LikeParticle; // RETURN
  } else if ( neutral && m_digammaLike( &p ) ) {
    m_digamma_like.insert( p.particleID() );
    return LoKi::KalmanFilter::DiGammaLikeParticle; // RETURN
  } else if ( basic && neutral && m_gammaLike( &p ) ) {
    m_gamma_like.insert( p.particleID() );
    return LoKi::KalmanFilter::GammaLikeParticle; // RETURN
  } else if ( m_longLived( p.particleID() ) ) {
    return LoKi::KalmanFilter::LongLivedParticle;
  } // RETURN
  else if ( !basic && rhoPlusLike_( &p ) ) {
    m_rhoplus_like.insert( p.particleID() );
    return LoKi::KalmanFilter::RhoPlusLikeParticle; // RETURN
  } else if ( !basic && m_shortLived( p.particleID() ) ) {
    return LoKi::KalmanFilter::ShortLivedParticle;
  } // RETURN
  //
  m_unclassified.insert( p.particleID() );
  //
  return LoKi::KalmanFilter::UnspecifiedParticle;
}
// ============================================================================
/// the factory needed for instantiation
DECLARE_COMPONENT( LoKi::ParticleClassificator )
// ============================================================================
// The END
// ============================================================================
