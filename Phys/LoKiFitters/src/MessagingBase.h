/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef MESSAGINGBASE_H
#  define MESSAGINGBASE_H 1
// ============================================================================
// from Gaudi
#  include "GaudiAlg/GaudiTool.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class MessagingBase MessagingBase.h
   *
   *  Simple base class adding methods for messageing with context.
   *
   *  @author Chris Jones
   *  @date   2014-10-21
   */
  class MessagingBase : public GaudiTool {
    // ========================================================================
  public:
    // ========================================================================
    /// Standard constructor
    MessagingBase( const std::string& type, const std::string& name, const IInterface* parent );
    // ========================================================================
  public:
    // ========================================================================
    /// initialize the tool
    StatusCode initialize() override;
    // ========================================================================
  protected:
    // ========================================================================
    StatusCode _Warning( const std::string& msg, const StatusCode& code = StatusCode::FAILURE,
                         const unsigned int prints = 2 ) const;
    // ========================================================================
    StatusCode _Error( const std::string& msg, const StatusCode& code = StatusCode::FAILURE,
                       const unsigned int prints = 2 ) const;
    // ========================================================================
  public:
    // ========================================================================
    /// measure CPU performance ?
    bool timing() const { return m_timing; } // measure CPU performance ?
    /// measure CPU performance ?
    void setTiming( const bool value ) { m_timing = value; }
    // ========================================================================
  private:
    // ========================================================================
    /// Access the current algorithm context name
    const std::string& myAlg() const { return m_myAlg; }
    // ========================================================================
    /// get the correct algorithm context
    bool getMyAlg() const;
    // ========================================================================
  private:
    // ========================================================================
    /// get the actual algorithm name context
    mutable std::string m_myAlg;
    // ========================================================================
    /// Option to enable printing of the algorithm name context
    bool m_printMyAlg = false;
    // ========================================================================
    /// # of prints
    unsigned int m_prints = 2;
    // ========================================================================
    /// measure CPU performance ?
    bool m_timing = false; /// measure CPU performance ?
    // ========================================================================
  };
  // ==========================================================================
} //                                                     end of namespace LoKi
// ============================================================================
// Warning message
// ============================================================================
inline StatusCode LoKi::MessagingBase::_Warning( const std::string& msg, const StatusCode& code,
                                                 const unsigned int prints ) const {
  // ==========================================================================
  if ( m_printMyAlg ) {
    getMyAlg();
  } else {
    m_myAlg.clear();
  }
  //
  if ( m_printMyAlg ) {
    return Warning( msg + m_myAlg, code, std::min( prints, m_prints ) );
  } else {
    return Warning( msg, code, std::min( prints, m_prints ) );
  }
  //
  if ( msgLevel( MSG::DEBUG ) ) { warning() << "'" << msg << "' " << m_myAlg << " Code =" << code << endmsg; }
  //
  return code;
  // ==========================================================================
}
// ============================================================================
// Error message
// ===========================================================================
inline StatusCode LoKi::MessagingBase::_Error( const std::string& msg, const StatusCode& code,
                                               const unsigned int prints ) const {
  // ==========================================================================
  if ( m_printMyAlg ) {
    getMyAlg();
  } else {
    m_myAlg.clear();
  }
  //
  if ( m_printMyAlg ) {
    return Error( msg + m_myAlg, code, std::min( prints, m_prints ) );
  } else {
    return Error( msg, code, std::min( prints, m_prints ) );
  }
  //
  if ( msgLevel( MSG::DEBUG ) ) { error() << "'" << msg << "' " << m_myAlg << " Code =" << code << endmsg; }
  //
  return code;
  // ==========================================================================
}
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // MESSAGINGBASE_H
// ============================================================================
