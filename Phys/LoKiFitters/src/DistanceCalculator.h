/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "DistanceCalculatorBase.h"
#include "FitterUtils.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "KalmanFilter/VertexFit.h"
#include "KalmanFilter/VertexFitWithTracks.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IParticleTransporter.h"

/** @file
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2008-03-05
 */
namespace LoKi {

  /** @class DistanceCalculator DistanceCalculator.h
   *
   *  It is the simplest implementation of the basic math,
   *  needed for the real implementation
   *  on the abstract interface IDistanceCalculator
   *  Essentially it relies on many nice functions,
   *  coded by Juan & Matt
   *
   *  @see IDistanceCalculator
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date   2008-03-05
   */
  class DistanceCalculator : public extends<LoKi::DistanceCalculatorBase, IDistanceCalculator> {
  public:
    /** @defgroup ParticlePoint
     *   Evaluation of the distance between the particle and the vertex
     *  @{
     */

    /** The method for the evaluation of the impact parameter  ("distance")
     *  vector of the particle with respect to some vertex.
     *  @param particle (input) pointer to the particle
     *  @param vertex   (input) pointer to the vertex
     *  @param imppar   (output) the value of impact parameter ("distance")
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* particle, LHCb::VertexBase const* vertex, double& imppar,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle, vertex ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *particle, *vertex, impact, allow, geometry ).andThen( [&] { imppar = impact.R(); } );
      } );
    }
    // ========================================================================
    /** The method for the evaluation of the impact parameter  ("distance")
     *  vector of the particle with respect to some vertex.
     *  @param particle (input) pointer to the particle
     *  @param vertex   (input) pointer to the vertex
     *  @param imppar   (output) the value of impact parameter ("distance")
     *  @param chi2     (output) the chi2 estimate for the separation
     *  @return status code
     */
    StatusCode distance( const LHCb::Particle* particle, const LHCb::VertexBase* vertex, double& imppar, double& chi2,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle, vertex ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *particle, *vertex, impact, allow, geometry, &chi2 ).andThen( [&] { imppar = impact.R(); } );
      } );
    }
    /// @}

    /** @defgroup ParticlePoint
     *   Evalaution of the distance between the particle and the fixed point
     *  @{
     */
    /** The method for the evaluation of the impact parameter ("distance")
     *  vector of the particle with respect to the fixed point
     *  @param particle (input) pointer to the particle
     *  @param point    (input) the fixed point
     *  @param imppar   (output) the value of impact parameter ("distance")
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* particle, Gaudi::XYZPoint const& point, double& imppar,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *particle, point, impact, allow, geometry ).andThen( [&] { imppar = impact.R(); } );
      } );
    }

    /** The method for the evaluation of the impact parameter ("distance")
     *  vector of the particle with respect to the fixed point
     *  @param particle (input) pointer to the particle
     *  @param point    (input) the fixed point
     *  @param imppar   (output) the value of impact parameter ("distance")
     *  @param chi2     (output) the chi2 estimate for the separation
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* particle, Gaudi::XYZPoint const& point, double& imppar, double& chi2,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *particle, point, impact, allow, geometry, &chi2 ).andThen( [&] { imppar = impact.R(); } );
      } );
    }
    /// @}

    /** @defgroup IPVector
     *   Evalaution of the vector distance between the particle point/vertex
     *  @{
     */
    /** The method for the evaluation of the impact parameter vector
     *  vector of the particle with respect to the fixed point
     *  @param particle (input) pointer to the particle
     *  @param point    (input) the fixed point
     *  @param impact   (output) the value of impact parameter vector
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* particle, Gaudi::XYZPoint const& point, Gaudi::XYZVector& impact,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle ).andThen( [&] { return _distance( *particle, point, impact, allow, geometry ); } );
    }

    /** The method for the evaluation of the impact parameter vector
     *  vector of the particle with respect to the vertex
     *  @param particle (input) pointer to the particle
     *  @param point    (input) the fixed point
     *  @param impact   (output) the value of impact parameter vector
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* particle, LHCb::VertexBase const* vertex, Gaudi::XYZVector& impact,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( particle, vertex ).andThen( [&] {
        return _distance( *particle, *vertex, impact, allow, geometry );
      } );
    }
    /// @}

    /** @defgroup VertexVertex
     *   Evalaution of the distance between two vertices
     *  @{
     */
    /** The trivial method for evaluation of the distance between two vertices
     *  @param v1   (input) the pointr to the first vertex
     *  @param v2   (input) the pointer to the second vertex
     *  @param dist (output) the distance between two vertices
     *  @param return status code
     */
    StatusCode distance( LHCb::VertexBase const* v1, LHCb::VertexBase const* v2, double& dist ) const override {
      return check( v1, v2 ).andThen( [&] {
        if ( v1 == v2 ) {
          dist = 0;
          ++m_same_vertex;
          return StatusCode{ StatusCode::SUCCESS };
        }
        return i_distance( *v1, *v2, dist ); // RETURN
      } );
    }

    /** The method for evaluation of the distance between two vertices and the
     *  corresponding \f$\chi^2\f$ for the separation significance.
     *  @param v1   (input) the pointr to the first vertex
     *  @param v2   (input) the pointer to the second vertex
     *  @param dist (output) the distance between two vertices
     *  @param return status code
     */
    StatusCode distance( LHCb::VertexBase const* v1, LHCb::VertexBase const* v2, double& dist,
                         double& chi2 ) const override {
      return check( v1, v2 ).andThen( [&] {
        if ( v1 == v2 ) {
          dist = 0;
          chi2 = 0;
          ++m_same_vertex;
          return StatusCode{ StatusCode::SUCCESS };
        }
        // make the real calculations
        return i_distance( *v1, *v2, dist, &chi2 ); // RETURN
      } );
    }
    /// @}

    /** @defgroup VertexPoint
     *   The set of the methods for evaluation of the various distances
     *   between the vertex and the fixed point
     *  @{
     */
    /** The trivial method for evaluation of the distance between the vertex
     *  and some "fixed" point
     *  @param v   (input) the pointer to the first vertex
     *  @param p   (input) the fixed point
     *  @param dist (output) the distance between two vertices
     *  @param return status code
     */
    StatusCode distance( LHCb::VertexBase const* v, Gaudi::XYZPoint const& p, double& dist ) const override {
      return check( v ).andThen( [&] { return i_distance( *v, p, dist ); } );
    }

    /** The method for evaluation of the distance between the vertices
     *  and some fixed point and the corresponding \f$\chi^2\f$ for
     *  the separation significance.
     *  @param v   (input)   the pointer to the first vertex
     *  @param p   (input)   the fixed point
     *  @param dist (output) the distance between two vertices
     *  @param chi2 (output) the chi2 of the separation significance
     *  @param return status code
     */
    StatusCode distance( LHCb::VertexBase const* v, Gaudi::XYZPoint const& p, double& dist,
                         double& chi2 ) const override {
      return check( v ).andThen( [&] { return i_distance( *v, p, dist, &chi2 ); } );
    }
    /// @}

    /** @defgroup ParticleParticle
     *   The set of the methods for evaluation of the various distances
     *   between two particles
     *  @{
     */
    /** The method for evaluation of the scalar distance between two particles,
     *  aka "distance of the closest approach".
     *  @param p1 (input) the pointer to the first particle
     *  @param p2 (input) the pointer to the second particle
     *  @param dist (output) the shortest distance between two trajectories
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* p1, LHCb::Particle const* p2, double& dist,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( p1, p2 ).andThen( [&] {
        if ( p1 == p2 ) {
          dist = 0;
          ++m_same_particle;
          return StatusCode{ StatusCode::SUCCESS };
        }
        return _distance( *p1, *p2, dist, allow, geometry ); // RETURN
      } );
    }
    // ========================================================================
    /** The method for evaluation of the scalar distance between two particles,
     *  aka "distance of the closest approach" and also its
     *   \f$\chi^2\f$ for separation significance
     *  @param p1 (input) the pointer to the first particle
     *  @param p2 (input) the pointer to the second particle
     *  @param dist (output) the shortest diostance between trajectories
     *  @param chi2 (output) chi2-estimate for the separation significance
     *  @return status code
     */
    StatusCode distance( LHCb::Particle const* p1, LHCb::Particle const* p2, double& dist, double& chi2,
                         IGeometryInfo const& geometry, bool allow ) const override {
      return check( p1, p2 ).andThen( [&] {
        if ( p1 == p2 ) {
          dist = 0;
          chi2 = 0;
          ++m_same_particle;
          return StatusCode{ StatusCode::SUCCESS };
        }
        // make the real calculations
        return _distance( *p1, *p2, dist, allow, geometry, &chi2 ); // RETURN
      } );
    }
    /// @}

    /** @defgroup OtherDistances
     *   The set of the methods for evaluation of "other" distances
     *  @{
     */
    /** The method for evaluation of the "path"-distance
     *  between the decay vertex of the particle and the vertex.
     *
     *  The path-distance,
     *   is defined as the value of the scalar parameter \f$s\f$
     *  from the vector equation:
     *
     *   \f[  \vec{\mathbf{v}}_{decay} = \vec{\mathbf{v}}_{production} +
     *           \frac{\vec{\mathbf{p}}}{\left|\vec{\mathbf{p}}\right|}s \f]
     *
     *  @param   particle (input) the pointer to the particle
     *  @param   primary  (input) the pointer to the production vertex
     *  @param   path     (output) the "path-distance"
     *  @param   error    (output) the estimate of the uncertanti in
     *                the projected distance
     *  @param   chi2 (output) the overall chi2 the procedure,
     *                which is the measure of the consistency
     *  @return  status code
     */
    StatusCode pathDistance( const LHCb::Particle* particle, const LHCb::VertexBase* primary, double& path,
                             double& error, IGeometryInfo const& geometry, double& chi2 ) const override {
      // check the input data
      return check( particle, primary ).andThen( [&] {
        // check the end-vertex
        const LHCb::VertexBase* decay = particle->endVertex();
        return check( decay ).andThen(
            [&] { return _distance( *primary, *particle, *decay, path, error, geometry, chi2 ); } );
      } );
    }

    /** Calculate the projected distance
     *
     *  \f$s=\frac{\left(\vec{\mathbf{v}}\vec{\mathbf{p}}
     *     \right)}{\left|\vec{\mathbf{p}}\right|}\f$
     *  where vector \f$\vec{\mathbf{v}}\f$ i a vector from
     *  the primary to the secondary vertex:
     *    \f$\vec{\mathbf{v}}=\vec{\mathbf{x}}_{d}-\vec{\mathbf{x}}_{pv}\f$,
     *
     * @param particle (input)  the pointer to the particle
     * @param primary  (input)  the pointer to the production vertex
     * @param dist     (output) the projected distance
     * @return status code
     */
    StatusCode projectedDistance( const LHCb::Particle* particle, const LHCb::VertexBase* primary,
                                  IGeometryInfo const& geometry, double& dist ) const override {
      return check( particle, primary ).andThen( [&] {
        const LHCb::VertexBase* decay = particle->endVertex();
        return check( decay ).andThen(
            [&] { return _distance( *primary, *particle, *decay, dist, geometry, nullptr ); } );
      } );
    }

    /** Calculate the projected distance
     *
     *  \f$s=\frac{\left(\vec{\mathbf{v}}\vec{\mathbf{p}}
     *     \right)}{\left|\vec{\mathbf{p}}\right|}\f$
     *  where vector \f$\vec{\mathbf{v}}\f$ i a vector from
     *  the primary to the secondary vertex:
     *    \f$\vec{\mathbf{v}}=\vec{\mathbf{x}}_{d}-\vec{\mathbf{x}}_{pv}\f$,
     *  and its error
     *
     * @param particle (input)  the pointer to the particle
     * @param primary  (input)  the pointer to the production vertex
     * @param dist     (output) the projected distance
     * @param error    (output) the estimate of the error in the distance
     * @return status code
     */
    StatusCode projectedDistance( const LHCb::Particle* particle, const LHCb::VertexBase* primary, double& dist,
                                  double& error, IGeometryInfo const& geometry ) const override {
      return check( particle, primary ).andThen( [&] {
        const LHCb::VertexBase* decay = particle->endVertex();
        return check( decay ).andThen(
            [&] { return _distance( *primary, *particle, *decay, dist, geometry, &error ); } );
      } );
    }
    /// @}

    /** @defgroup  TrackVertex
     *  Evaluation of the distance between the track and vertex
     *  @{
     */

    /** evaluate the impact parameter of the track with respect to the vertex
     *  @param track (input)   the track
     *  @param vertex (input)  the vertex
     *  @param imppar (output) the value of impact parameter
     */
    StatusCode distance( const LHCb::Track* track, const LHCb::VertexBase* vertex, double& imppar,
                         IGeometryInfo const& geometry ) const override {
      return check( track, vertex ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *track, *vertex, impact, geometry ).andThen( [&] { imppar = impact.R(); } );
      } );
    }

    /** evaluate the impact parameter of the track with respect to the vertex
     *  @param track (input)   the track
     *  @param vertex (input)  the vertex
     *  @param imppar (output) the value of impact parameter
     *  @param chi2   (output) chi2 of impact parameter
     */
    StatusCode distance( const LHCb::Track* track, const LHCb::VertexBase* vertex, double& imppar, double& chi2,
                         IGeometryInfo const& geometry ) const override {
      return check( track, vertex ).andThen( [&] {
        Gaudi::XYZVector impact;
        return _distance( *track, *vertex, impact, geometry, &chi2 ).andThen( [&] { imppar = impact.R(); } );
      } );
    }
    /** evaluate the impact parameter of the track with respect to the vertex
     *  @param track (input)   the track
     *  @param vertex (input)  the vertex
     *  @param imppar (output) the value of impact parameter
     */
    StatusCode distance( const LHCb::Track* track, const LHCb::VertexBase* vertex, Gaudi::XYZVector& impact,
                         IGeometryInfo const& geometry ) const override {
      return check( track, vertex ).andThen( [&] { return _distance( *track, *vertex, impact, geometry ); } );
    }
    /// @}

    /** @defgroup  TrackTrack
     *  Evaluation of the distance between the tracks
     *  @{
     */
    /** evaluate the distance between two tracks
     *  @param track1 (input)  the first track
     *  @param track2 (input)  the second track
     *  @param doca   (output) the value of distance
     */
    StatusCode distance( const LHCb::Track* track1, const LHCb::Track* track2, double& doca,
                         IGeometryInfo const& geometry ) const override {
      return check( track1, track2 ).andThen( [&] {
        if ( track1 == track2 ) {
          doca = 0;
          ++m_same_track;
          return StatusCode{ StatusCode::SUCCESS };
        }
        return _distance( *track1, *track2, doca, geometry );
      } );
    }

    /** evaluate the distance between two tracks
     *  @param track1 (input)  the first track
     *  @param track2 (input)  the second track
     *  @param doca   (output) the value of distance
     *  @param chi2   (output) the chi2 of distance
     */
    StatusCode distance( const LHCb::Track* track1, const LHCb::Track* track2, double& doca, double& chi2,
                         IGeometryInfo const& geometry ) const override {
      return check( track1, track2 ).andThen( [&] {
        if ( track1 == track2 ) {
          doca = 0;
          chi2 = 0;
          ++m_same_track;
          return StatusCode{ StatusCode::SUCCESS };
        }
        return _distance( *track1, *track2, doca, geometry, &chi2 );
      } );
    }
    /// @}

    /** Standard constructor
     *  @param type tool type(?)
     *  @param name tool instance name
     *  @param parent the pointer to the parent
     */
    DistanceCalculator( const std::string& type,    // tool type (?)
                        const std::string& name,    // tool instance name
                        const IInterface*  parent ); // the parent

  protected:
    /// use fast (==no transportation, no iteration) algorithms?
    bool fastalgo() const { return m_fastalgo; }
    /// use fast (==no transportation, no iteration) algorithms?
    void setFastAlgo( const bool value ) { m_fastalgo = value; }

  private:
    /// get the state from the track at certain Z
    StatusCode stateAtZ( LHCb::State& s, LHCb::Track const& t, const double z, IGeometryInfo const& geometry ) const {
      //
      const double tolerance = 0.35 * m_deltaZ;
      const double newZ      = checkNewZ( z );
      //
      // For long tracks in vicinity of origin point, use trajectory approximation.
      // According to Wouter it should be much more CPU efficient.
      if ( t.checkType( LHCb::Track::Types::Long ) && m_region.first <= newZ && newZ <= m_region.second ) {
        StatusCode sc = stateProvider()->stateFromTrajectory( s, t, newZ, geometry );
        if ( sc.isSuccess() ) { return sc; }
      }
      //
      StatusCode sc = stateProvider()->state( s, t, newZ, geometry, 0.9 * tolerance );
      if ( sc.isSuccess() && std::fabs( s.z() - newZ ) < tolerance ) { return sc; }
      //
      ++m_unable_to_get_state_provider;
      ;
      //
      sc = extrapolator()->propagate( t, newZ, s, geometry );
      if ( sc.isSuccess() && std::fabs( s.z() - newZ ) < tolerance ) { return sc; }
      //
      ++m_unable_to_get_state_extrapolator;
      //
      s = t.closestState( newZ );
      return StatusCode::SUCCESS;
    }

    /* The method for the evaluation of the impact parameter ("distance")
     *  vector of the particle with respect to some vertex.
     *  @param particle (input) pointer to the particle
     *  @param vertex   (input) pointer to the vertex
     *  @param imppar   (output) the impact parameter ("distance") vector
     *  @param chi2     (output,optional) the chi2 of the inmpact parameter
     *  @return status code
     */
    StatusCode _distance( const LHCb::Particle& particle, const LHCb::VertexBase& vertex, Gaudi::XYZVector& impact,
                          const bool allow, IGeometryInfo const& geometry, double* chi2 = 0 ) const;

    /** The method for the evaluation of the impact parameter ("distance")
     *  vector of the particle with respect to the fixed vertex
     *  @param particle (input) the particle
     *  @param point    (input) the fixed point
     *  @param imppar   (output) the impact parameter ("distance") vector
     *  @param chi2     (output,optional) the chi2 of the inmpact parameter
     *  @return status code
     */
    StatusCode _distance( const LHCb::Particle& particle, const Gaudi::XYZPoint& point, Gaudi::XYZVector& imppar,
                          const bool allow, IGeometryInfo const& geometry, double* chi2 = 0 ) const;

    /** The method for evaluation of the scalar distance between two particles,
     *  aka "distance of the closest approach" and also its
     *   \f$\chi^2\f$ for separation significance
     *  @param p1 (input) the first particle
     *  @param p2 (input) the second particle
     *  @param dist (output) the shortest distance between trajectories
     *  @param chi2 (output,optional) chi2-estimate for the separation significance
     *  @return status code
     */
    StatusCode _distance( const LHCb::Particle& p1, const LHCb::Particle& p2, double& dist, const bool allow,
                          IGeometryInfo const& geometry, double* chi2 = 0 ) const;

    /** the method for the evaluation of "path"-distance
     *  @param primary  (input) the production(primary) vertex
     *  @param particle (input) the particle
     *  @param decay    (input) the decay vertex of the particle
     *  @param path     (output) the path-distance
     *  @param error    (output) the error inpath distance
     *  @param chi2     (output) the chi2 of the procedure
     *  @return status code
     */
    StatusCode _distance( const LHCb::VertexBase& primary, const LHCb::Particle& particle,
                          const LHCb::VertexBase& decay, double& path, double& error, IGeometryInfo const& geometry,
                          double& chi2 ) const;

    /** Calculate the projected distance
     *
     *  \f$s=\frac{\left(\vec{\mathbf{v}}\vec{\mathbf{p}}
     *     \right)}{\left|\vec{\mathbf{p}}\right|}\f$
     *  where vector \f$\vec{\mathbf{v}}\f$ is a vector from
     *  the primary to the secondary vertex:
     *    \f$\vec{\mathbf{v}}=\vec{\mathbf{x}}_{d}-\vec{\mathbf{x}}_{pv}\f$,
     *  and its error
     *
     * The simplest way to evaluate the error it to considner formally
     * the problem as a constrained minimization with the constraint:
     *
     * \f$ H = \left(\vec{\mathbf{v}}\vec{\mathbf{p}}\right) -
     *    \mathbf{s}\left|\vec{\mathbf{p}}\right| = 0
     * \f$
     * Of course there is no need to perform the actual minimiation
     * The solution is known in advance!), but formalizm is easy to reuse
     * for evaluation of \f$\mathbf{C_s}\f$
     *
     * @param primary  (input)  the production vertex
     * @param particle (input)  the particle
     * @param decay    (input)  the decay particle
     * @param dist     (output) the projected distance
     * @param error    (output) the estimate of the error in the distance
     * @return status code
     */
    StatusCode _distance( const LHCb::VertexBase& primary, const LHCb::Particle& particle,
                          const LHCb::VertexBase& decay, double& dist, IGeometryInfo const& geometry,
                          double* error = 0 ) const;

    /** The method for the evaluation of the impact parameter ("distance")
     *  vector of the particle with respect to some vertex.
     *  @param track    (input)  the track
     *  @param vertex   (input)  the vertex
     *  @param imppar   (output) the impact parameter ("distance") vector
     *  @param chi2     (output,optional) the chi2 of distance
     *  @return status code
     */
    StatusCode _distance( const LHCb::Track& track, const LHCb::VertexBase& vertex, Gaudi::XYZVector& imppar,
                          IGeometryInfo const& geometry, double* chi2 = 0 ) const;

    /** The method for the evaluation of the distance between two tracks
     *  @param track1  (input)  the first tarck
     *  @param track2  (input)  the first tarck
     *  @param doca    (output) the distance
     *  @param chi2    (output,optional) the chi2 of distance
     *  @return status code
     */
    StatusCode _distance( const LHCb::Track& track1, const LHCb::Track& track2, double& doca,
                          IGeometryInfo const& geometry, double* chi2 = 0 ) const;

    // properties:

    /// the maximal number of iterations
    unsigned int m_nIter_max = 10; // the maximal number of iterations
    /// the convergency criterion for impact parameter evaluations
    double m_deltaZ = 2 * Gaudi::Units::micrometer; // the convergency criterion for ip-evaluations
    /// the convergency criterion for delta(chi2)
    double m_delta_chi2 = 0.05; // the convergency criterion for delta(chi2)
    // the convergency criterion for delta(path)
    double m_delta_path = 2 * Gaudi::Units::micrometer; // the convergency criterion for delta(path)
    // the ragion where Trajectory" approximation is good for long tracks
    std::pair<double, double> m_region = { -30 * Gaudi::Units::cm, 100 * Gaudi::Units::cm };
    // use fast (no transportation, no iteration) algorithms?
    bool m_fastalgo = false;

    mutable Gaudi::Accumulators::Counter<> m_IP2DIGamma{ this, "IP->(DI)GAMMA" };
    mutable Gaudi::Accumulators::Counter<> m_IP2VD{ this, "IP->VD" };
    mutable Gaudi::Accumulators::Counter<> m_DOCA2DIGamma{ this, "DOCA->(DI)GAMMA" };
    mutable Gaudi::Accumulators::Counter<> m_DOCA2IP{ this, "DOCA->IP" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_same_vertex{ this, "distance(v,v): the same vertex" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_same_particle{ this, "distance(p,p): the same particle" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_same_track{ this, "distance(t,t): the same track" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unable_to_get_state_provider{
        this, "stateAtZ: unable to get the proper state with provider" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unable_to_get_state_extrapolator{
        this, "stateAtZ: unable to get the proper state with extrapolator" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_transport_error{
        this, "Error from ParticleTransporter(ip_1), ignore" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_transport_error2{
        this, "Error from ParticleTransporter(ip_2), ignore" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_I{ this, "There is no convergency-I", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_II{ this, "There is no convergency-II", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_III{ this, "There is no convergency-III",
                                                                                0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_IV{ this, "There is no convergency-IV", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_V{ this, "There is no convergency-V", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_convergence_VI{ this, "There is no convergency-VI", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_transport_error_chi2{
        this, "Error from ParticleTransporter(chi2_1), ignore" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_transport_error_chi2_2{
        this, "Error from ParticleTransporter(chi2_2), ignore" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_transport_ierror{
        this, "distance(IV):Error from IParticleTransporter" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_kalman_load{ this,
                                                                         "distance(I): KalmanFilter::load failed" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_kalman_load2{
        this, "distance(III): error from KalmanFilter::load(s)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_kalman_gain{
        this, "distance(I): unable to calculate the gain matrix" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_kalman_filter{
        this, "distance(I): error from Kalman Filter step" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_kalman_step{
        this, "distance(III): error from KalmanFilter::step(2)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_distance_II{ this,
                                                                       "distance(II): error from KalmanFilter::load" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_III{
        this, "distance(III):Error from ParticleTransporter, ignore" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_IV{
        this, "distance(IV):Error code from LoKi::Fitters::path0" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_IV_path_step{
        this, "distance(IV): error from path_step" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_V{
        this, "distance(V):The negative covarinace, return error=-1" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_VII_1{
        this, "distance(V): can't get the proper state(1)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_VII_2{
        this, "distance(V): can't get the proper state(2)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_distance_VI{
        this, "distance(VI): can't get the proper state(1)" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_path_step{ this, "_pathDistance(): error from path_step" };
  };

} // end of namespace LoKi
