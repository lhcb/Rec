/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DirectionFitBase.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "Kernel/ILifetimeFitter.h"

/**
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2008-02-12
 */

namespace LoKi {

  /** @class LifeTimeFitter
   *  The simple implementation of following major  abstract interfaces:
   *
   *    - ILifetimeFitter
   *
   *  The implementation follows the note by Paul AVERY
   *    "Directly Determining Lifetime Using a 3-D Fit"
   *
   *  The actual algorithm si described in detail for
   *  the base class LoKi::DirectionFitBase
   *
   *  @see LoKi::DirectionFitBase
   *  @see ILifetimeFitter
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2008-02-12
   */
  struct LifetimeFitter : extends<LoKi::DirectionFitBase, ILifetimeFitter> {

    using extends::extends;

    /** Evaluate the particle  lifetime
     *
     *  @code
     *
     *  // get the tool:
     *  ILifetimeFitter* fitter = ... ;
     *
     *  // get B-candidate:
     *  const LHCb::Particle* myB = ... ;
     *
     *  // get the corresponidng primary vertex:
     *  const LHCb::VertexBase* primary = ... ;
     *
     *  // use the tool:
     *  double lifetime = -1*e+10 * Gaudi::Units::ns ;
     *  double error    = -1 ;
     *  double chi2     = -1 ;
     *  StatusCode sc = fitter -> fit ( primary , myB , lifetime , error , chi2 ) ;
     *  if ( sc.isFailure() ) { ... error here ... }
     *
     *  @endcode
     *
     *  @see ILifetimeFitter
     *
     *  @param primary  the production vertex   (input)
     *  @param particle the particle            (input)
     *  @param lifetime the proper lifetime     (output)
     *  @param error error estgimate for the proper lifetime  (output)
     *  @param chi2 chi2 of the fit            (output)
     */
    StatusCode fit( LHCb::VertexBase const& primary, LHCb::Particle const& particle, double& lifetime, double& error,
                    double& chi2, IGeometryInfo const& geometry ) const override {
      const LHCb::VertexBase* decay = particle.endVertex();
      if ( !decay ) {
        lifetime = -1.e+10 * Gaudi::Units::nanosecond;
        error    = -1.e+10 * Gaudi::Units::nanosecond;
        chi2     = -1.e+10;
        ++m_no_end_vertex;
        return StatusCode{ NoEndVertex };
      }

      // make the actual iterations
      return fitConst_( &primary, &particle, decay, lifetime, error, chi2, geometry )
          .andThen( [&] {
            // convert c*tau into time
            lifetime /= Gaudi::Units::c_light;
            error /= Gaudi::Units::c_light;
          } )
          .orElse( [&] { ++m_fitConst_failure; } );
    }

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_fitConst_failure{ this, "Error from LoKi::DirectionFitBase",
                                                                              0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_no_end_vertex{ this, "No valid end-vertex is found" };
  };

} //                                                      end of namespace LoKi

/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::LifetimeFitter )
