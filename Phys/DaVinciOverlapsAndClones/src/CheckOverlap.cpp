/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : CheckOverlap: CheckOverlap.cpp
//
// 28/06/2002
//-----------------------------------------------------------------------------
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/ICheckOverlap.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include <string>

/** @class CheckOverlap CheckOverlap.h
 *
 *  Tool to check if more than one LHCb::Particle in the particle tree have the
 *  same underying LHCb::ProtoParticle. Also checks for the same underlying
 *  LHCb::Track for charged basic particles.
 *
 *  @author Jose' Helder Lopes
 *  @date   28/06/2002
 */
class CheckOverlap : public extends<GaudiTool, ICheckOverlap> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override {
    auto sc = extends::initialize();
    m_ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
    return sc && m_ppSvc ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }

  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Argument: parts is a vector of pointers to particles.
  ///  Create an empty vector of pointers to protoparticles.
  ///  Call the real check method.
  bool foundOverlap( const LHCb::Particle::ConstVector& parts ) const override;

  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Arguments: particle1 up to particle4 are pointers to particles.
  ///  Create a ParticleVector and fill it with the input particles.
  ///  Create an empty vector of pointers to protoparticles.
  ///  Call the real check method.

  bool foundOverlap( const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle* ) const override;
  bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle*,
                     const LHCb::Particle* ) const override;

public:
  /// Check for duplicate use of a protoparticle to produce decay tree of
  /// any particle in vector. Removes found particles from vector.
  StatusCode removeOverlap( LHCb::Particle::ConstVector& ) const override;

  /// Check for duplicate use of a protoparticle to produce decay tree of
  /// any particle in vector. Removes found particles from vector.
  StatusCode removeOverlap( LHCb::Particle::Vector& ) const;

private:
  //===========================================================================
  /// Check for duplicate use of a protoparticle to produce particles.
  /// Continue a previous check using the contents of the vector of pointers
  /// to protoparticles.(Most intended for internal use by the other methods).
  /// Arguments: parts is a vector of pointer to particles.
  ///            proto is a vector of pointers to protoparticles.
  //  Real checking method. Scans the tree headed by parts. Add each
  //  protoparticle to proto if it is a new one. Returns true otherwise.
  //  If called directly by the user, it will continue a previous check,
  //  not start a new one!
  //===========================================================================
  bool foundOverlap( const LHCb::Particle::ConstVector& parts, std::vector<const LHCb::ProtoParticle*>& proto ) const;

  bool addOrigins( const LHCb::Particle::ConstVector&, std::vector<const LHCb::ProtoParticle*>& ) const;

  /// Only look at protoparticles
  bool searchOverlap( std::vector<const LHCb::ProtoParticle*>& proto ) const;

private:
  /// Accessor for ParticlePropertySvc
  LHCb::IParticlePropertySvc* m_ppSvc = nullptr;

}; // End of class header.

// Declaration of the Tool Factory
DECLARE_COMPONENT( CheckOverlap )

namespace detail {
  template <typename... Particles>
  bool find_overlap_in_basics( Particles const*... particles ) {
    static_assert( ( std::is_same_v<Particles, LHCb::ProtoParticle> && ... ),
                   "All Particles must be LHCb::ProtoParticle" );
    std::array ps{ particles... };
    for ( auto i = 0u; i < ps.size(); ++i ) {
      for ( auto j = i + 1; j < ps.size(); ++j ) {
        if ( ps[i] == ps[j] || ( ps[i]->track() && ( ps[i]->track() == ps[j]->track() ) ) ) { return true; }
      }
    }
    return false;
  }
} // namespace detail

//===========================================================================
/// Check for duplicate use of a protoparticle to produce particles.
/// Continue a previous check using the contents of the vector of pointers
/// to protoparticles.(Most intended for internal use by the recursive calls).
/// Arguments: parts is a vector of pointer to particles.
///            proto is a vector of pointers to protoparticles.
//  Real checking method. Scans the tree headed by parts. Add each
//  protoparticle to proto if it is a new one. Return true otherwise.
//  If called directly by the user, it will continue a previous check,
//  not start a new one!
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle::ConstVector&       parts,
                                 std::vector<const LHCb::ProtoParticle*>& proto ) const {
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "foundOverlap(parts, protos) " << parts.size() << " " << proto.size() << endmsg;
  const bool sc = addOrigins( parts, proto );
  if ( !sc ) { Exception( "Unable to get origin vector of particle vector" ); }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "searching overlap" << endmsg;
  return searchOverlap( proto );
}

//===========================================================================
// Check duplicate entries
//===========================================================================
bool CheckOverlap::searchOverlap( std::vector<const LHCb::ProtoParticle*>& proto ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "searchOverlap(protos)" << endmsg;
  // It its a simple particle made from protoparticle. Check.

  // We should only perform a track comparison with non-null track
  // pointers, else we will determine all neutrals and composites to
  // overlap with each other (they have nullptr tracks)
  for ( auto i = proto.begin(); i != proto.end(); ++i ) {
    auto itrack = ( *i )->track();
    if ( !itrack ) continue;
    for ( auto j = std::next( i ); j != proto.end(); ++j ) {
      auto jtrack = ( *j )->track();
      if ( !jtrack ) continue;
      if ( itrack == jtrack || *i == *j ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Found overlap " << *i << endmsg;
        return true;
      }
    }
  }
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Found no overlap" << endmsg;
  return false;
}
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle::ConstVector& parts ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "foundOverlap(LHCb::Particle::ConstVector)" << endmsg;
  std::vector<const LHCb::ProtoParticle*> proto{};
  proto.reserve( 10 );
  return foundOverlap( parts, proto );
}
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle* particle1 ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "foundOverlap(1)" << endmsg;
  if ( !particle1 ) Exception( "Null pointer" );
  std::vector<const LHCb::ProtoParticle*> proto{};
  proto.reserve( 10 );
  return foundOverlap( { particle1 }, proto );
}
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2 ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "foundOverlap(2)" << endmsg;
  if ( !particle1 || !particle2 ) Exception( "Null pointer" );
  if ( particle1->proto() && particle2->proto() ) {
    return detail::find_overlap_in_basics( particle1->proto(), particle2->proto() );
  }
  std::vector<const LHCb::ProtoParticle*> proto{};
  proto.reserve( 10 );
  return foundOverlap( { particle1, particle2 }, proto );
}
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2,
                                 const LHCb::Particle* particle3 ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "foundOverlap(3)" << endmsg;
  if ( !particle1 || !particle2 || !particle3 ) Exception( "Null pointer" );
  if ( particle1->proto() && particle2->proto() && particle3->proto() ) {
    return detail::find_overlap_in_basics( particle1->proto(), particle2->proto(), particle3->proto() );
  }
  std::vector<const LHCb::ProtoParticle*> proto{};
  proto.reserve( 10 );
  return foundOverlap( { particle1, particle2, particle3 }, proto );
}
//===========================================================================
bool CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2,
                                 const LHCb::Particle* particle3, const LHCb::Particle* particle4 ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "foundOverlap(4)" << endmsg;
  if ( !particle1 || !particle2 || !particle3 || !particle4 ) Exception( "Null pointer" );
  if ( particle1->proto() && particle2->proto() && particle3->proto() && particle4->proto() ) {
    return detail::find_overlap_in_basics( particle1->proto(), particle2->proto(), particle3->proto(),
                                           particle4->proto() );
  }
  std::vector<const LHCb::ProtoParticle*> proto{};
  proto.reserve( 10 );
  return foundOverlap( { particle1, particle2, particle3, particle4 }, proto );
}
//===========================================================================
// Check for duplicate use of a protoparticle to produce decay tree of
// any particle in vector. Removes found particles from vector.
//===========================================================================
StatusCode CheckOverlap::removeOverlap( LHCb::Particle::Vector& PV ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "removeOverlap( LHCb::Particle::Vector)" << endmsg;
  PV.erase( std::remove_if( PV.begin(), PV.end(), [&]( const auto& i ) { return foundOverlap( i ); } ), PV.end() );
  return StatusCode::SUCCESS;
}
//===========================================================================
StatusCode CheckOverlap::removeOverlap( LHCb::Particle::ConstVector& PV ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "removeOverlap(LHCb::Particle::ConstVector)" << endmsg;
  PV.erase( std::remove_if( PV.begin(), PV.end(), [&]( const auto& i ) { return foundOverlap( i ); } ), PV.end() );
  return StatusCode::SUCCESS;
}
//=============================================================================
// Replace resonance by daughters in vector, helper to the Tree methods
//=============================================================================
bool CheckOverlap::addOrigins( const LHCb::Particle::ConstVector&       parts,
                               std::vector<const LHCb::ProtoParticle*>& protos ) const {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "addOrigins() " << parts.size() << endmsg;

  for ( const auto& c : parts ) {

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Particle " << c->particleID().pid() << " " << c->momentum() << endmsg;

    if ( c->proto() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "has an origin " << c->proto() << endmsg;
      protos.push_back( c->proto() );
    } else if ( !( c->daughters().empty() ) ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << "has a daughters " << c->daughters().size() << endmsg;
      const LHCb::Particle::ConstVector& dau = c->daughtersVector();
      const bool                         sc  = addOrigins( dau, protos );
      if ( !sc ) return sc;
    } else {
      if ( !m_ppSvc ) { throw GaudiException{ "", "", StatusCode::FAILURE }; }
      const LHCb::ParticleProperty* pp = m_ppSvc->find( c->particleID() );
      if ( pp ) {
        Warning( pp->particle() + " has no proto nor endVertex. Assuming it's from MC.", StatusCode{ 10 }, 1 ).ignore();
      } else {
        err() << "Particle with unknown PID " << c->particleID().pid() << " has no endVertex. "
              << "Assuming it's from MC" << endmsg;
      }
    }
  }

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "addOrigins() left " << protos.size() << endmsg;
  return true;
}
