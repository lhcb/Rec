/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

namespace MassConstrainer {
  /// Methods to apply a mass constraint to the output of a vertex fit. Could be moved to LHCbMath.

  /// Helper function. Do not call directly.
  template <bool WithPosition>
  double ConstrainMassImp( Gaudi::LorentzVector& p4, Gaudi::SymMatrix4x4& momcov, Gaudi::XYZPoint* pos,
                           Gaudi::SymMatrix3x3* poscov, Gaudi::Matrix4x3* momposcov, const double pdgmass,
                           const double pdgmasscov = 0 ) {
    // Compute the mass
    const auto px = p4.Px();
    const auto py = p4.Py();
    const auto pz = p4.Pz();
    const auto E  = p4.E();
    const auto m  = std::sqrt( E * E - px * px - py * py - pz * pz );
    // Compute the Jacobian: Hp = dm/dp4.
    ROOT::Math::SMatrix<double, 1, 4> Hp;
    Hp( 0, 0 ) = -px / m;
    Hp( 0, 1 ) = -py / m;
    Hp( 0, 2 ) = -pz / m;
    Hp( 0, 3 ) = E / m;
    // Compute the variance in the mass
    const double mcov = Similarity( Hp, momcov )( 0, 0 );
    double       chi2 = 0;
    if ( mcov > 1.0 ) { // Need some sort of threshold. Could add as function argument
      // Compute the inverse of the variance of the residual
      const ROOT::Math::SMatrix<double, 1, 1> Rinv{ 1. / ( mcov + pdgmasscov ) };
      // Compute the gain matrix in mass space
      const ROOT::Math::SMatrix<double, 3, 1> Kbeta_mom = momcov.Sub<Gaudi::Matrix3x4>( 0, 0 ) * Transpose( Hp ) * Rinv;
      const ROOT::Math::SMatrix<double, 1, 1> Kbeta_mass = mcov * Rinv;
      const ROOT::Math::SVector<double, 1>    residual{ pdgmass - m };
      const ROOT::Math::SVector<double, 3>    deltamom  = Kbeta_mom * residual;
      const ROOT::Math::SVector<double, 1>    deltamass = Kbeta_mass * residual;
      // Must find a less error prone method to update
      const double pxnew = px + deltamom( 0 );
      const double pynew = py + deltamom( 1 );
      const double pznew = pz + deltamom( 2 );
      const double mnew  = m + deltamass( 0 );
      const double Enew  = std::sqrt( pxnew * pxnew + pynew * pynew + pznew * pznew + mnew * mnew );
      chi2               = residual( 0 ) * residual( 0 ) / mcov;

      p4.SetPxPyPzE( pxnew, pynew, pznew, Enew );

      // std::cout << "Updating momentum in massconstrainer: "
      // << m << " " << residual << " " << chi2 << " " << p4 << " " << deltamom << " " << p4.M() << " " << pdgmass <<
      // std::endl ;

      // Update the covariance matrix.
      // If we really want to error on the mass to appear as zero, then we need to recompute using the updated jacobian.
      Hp( 0, 0 )                        = -pxnew / mnew;
      Hp( 0, 1 )                        = -pynew / mnew;
      Hp( 0, 2 )                        = -pznew / mnew;
      Hp( 0, 3 )                        = Enew / mnew;
      const double              mcovnew = Similarity( Hp, momcov )( 0, 0 );
      const Gaudi::SymMatrix1x1 Rinvmatrix{ 1 / ( mcovnew + pdgmasscov ) };
      // We rewrote the expressions to make them more efficient.
      // const Gaudi::SymMatrix4x4 matrixA = - Similarity(Transpose(Hp), Rinvmatrix) ;
      // const Gaudi::SymMatrix4x4 deltamomcov = Similarity(momcov,matrixA) ;
      const ROOT::Math::SMatrix<double, 1, 4> Hpmomcov    = Hp * momcov;
      const Gaudi::SymMatrix4x4               deltamomcov = -1 * Similarity( Transpose( Hpmomcov ), Rinvmatrix );
      momcov += deltamomcov;

      // std::cout << "Mass cov: " << mcov << " " << mcovnew << " " << Similarity(Hp,momcov)(0,0) <<
      // std::endl ;

      if constexpr ( WithPosition ) {
        const ROOT::Math::SMatrix<double, 1, 3> Hpmomposcov = Hp * ( *momposcov );
        const ROOT::Math::SMatrix<double, 3, 1> Kbeta_pos   = Transpose( Hpmomposcov ) * Rinvmatrix;
        const ROOT::Math::SVector<double, 3>    deltapos    = Kbeta_pos * residual;
        pos->SetX( pos->x() + deltapos( 0 ) );
        pos->SetY( pos->y() + deltapos( 1 ) );
        pos->SetZ( pos->z() + deltapos( 2 ) );
        // const Gaudi::SymMatrix3x3 deltaposcov = Similarity(Transpose(*momposcov),matrixA ) ;
        // const Gaudi::Matrix4x3    deltamomposcov = momcov * matrixA * (*momposcov) ;
        const Gaudi::SymMatrix3x3 deltaposcov    = -1 * Similarity( Transpose( Hpmomposcov ), Rinvmatrix );
        const Gaudi::Matrix4x3    deltamomposcov = -1 * Transpose( Hpmomcov ) * Rinvmatrix * Hpmomposcov;
        *poscov += deltaposcov;
        *momposcov += deltamomposcov;
      }
    }
    return chi2;
  }

  /// Method to constrain mass on composite daughter. Returns chi2
  auto ConstrainMass( Gaudi::XYZPoint& pos, Gaudi::LorentzVector& p4, Gaudi::SymMatrix3x3& poscov,
                      Gaudi::Matrix4x3& momposcov, Gaudi::SymMatrix4x4& momcov, const double pdgmass,
                      const double pdgmasscov = 0 ) {
    return ConstrainMassImp<true>( p4, momcov, &pos, &poscov, &momposcov, pdgmass, pdgmasscov );
  }

  /// Method to constrain mass on composite daughter.  Returns chi2
  auto ConstrainMass( Gaudi::LorentzVector& p4, Gaudi::SymMatrix4x4& momcov, const double pdgmass,
                      const double pdgmasscov = 0 ) {
    return ConstrainMassImp<false>( p4, momcov, nullptr, nullptr, nullptr, pdgmass, pdgmasscov );
  }
} // namespace MassConstrainer
