/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/FloatComparison.h"
#include "Event/TrackVertexUtils.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ITrajPoca.h"
#include "Kernel/IVertexFit.h"
#include "Kernel/LineTraj.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatrixTransforms.h"
#include "MassConstrainer.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackKernel/TrackTraj.h"
#include <boost/container/static_vector.hpp>
#include <variant>

namespace {
  class VertexDaughter;
}

class ParticleVertexFitter : public extends<GaudiTool, IVertexFit> {
public:
  /// Standard constructor
  using extends::extends;

  /// Initialize
  StatusCode initialize() override;

  /// Finalize
  StatusCode finalize() override;

  /// Method to fit a vertex
  StatusCode fit( LHCb::Vertex& vertex, const LHCb::Particle::ConstVector& daughters,
                  const IGeometryInfo& geometry ) const override final {
    LHCb::Particle* mother{ nullptr };
    return fit( daughters, vertex, mother, geometry );
  }

  /// Method to fit a vertex returning a Particle (that should already know its PID)
  StatusCode fit( const LHCb::Particle::ConstVector& daughters, LHCb::Vertex& vertex, LHCb::Particle& mother,
                  const IGeometryInfo& geometry ) const override final {
    StatusCode sc = fit( daughters, vertex, &mother, geometry );
    mother.setDaughters( vertex.outgoingParticles() );
    return sc;
  }

  /// Method to refit a particle
  StatusCode reFit( LHCb::Particle& particle, const IGeometryInfo& geometry ) const override final {
    LHCb::Vertex* vertex = particle.endVertex();
    return fit( particle.daughtersVector(), *vertex, particle, geometry );
  }

  StatusCode combine( const LHCb::Particle::ConstVector& daughters, LHCb::Particle& mother, LHCb::Vertex& vertex,
                      const IGeometryInfo& geometry ) const override final {
    return fit( daughters, vertex, mother, geometry );
  }

  StatusCode add( const LHCb::Particle*, LHCb::Vertex&, const IGeometryInfo& ) const override final {
    Error( "add is not implemented for ParticleVertexFitterter (though quite trivial)" ).ignore();
    return StatusCode::FAILURE;
  }

  StatusCode remove( const LHCb::Particle*, LHCb::Vertex&, const IGeometryInfo& ) const override final {
    Error( "remove is not implemented for ParticleVertexFitterter" ).ignore();
    return StatusCode::FAILURE;
  }

private:
  StatusCode fit( const LHCb::Particle::ConstVector&, LHCb::Vertex&, LHCb::Particle*, const IGeometryInfo& ) const;
  void       sync_pids() {
    m_massConstraintMap.clear();
    for ( const auto& name : m_massConstraints ) {
      const auto& pp = this->m_ppsvc->find( name );
      if ( !pp ) {
        this->warning() << "Cannot find particle property for name '" << name << "'" << endmsg;
      } else {
        this->debug() << "Setting mass constraint for " << name << endmsg;
        m_massConstraintMap[pp->pdgID().pid()] = pp->mass();
      }
    }
  }

  std::optional<double> massConstraint( const LHCb::Particle& p ) const {
    const auto it = m_massConstraintMap.find( p.particleID().pid() );
    return it != m_massConstraintMap.end() ? std::optional<double>{ it->second } : std::optional<double>{};
  }

private:
  Gaudi::Property<int>                      m_maxnumiter{ this, "MaxNumIter", 5 };
  Gaudi::Property<double>                   m_maxdchisq{ this, "MaxDeltaChi2", 0.01 };
  Gaudi::Property<bool>                     m_extrapolateTtracks{ this, "extrapolateTtracks", false };
  PublicToolHandle<ITrackStateProvider>     m_stateprovider{ this, "StateProvider", "TrackStateProvider" };
  ToolHandle<ITrajPoca>                     m_trajpoca{ "TrajPoca" };
  const LHCb::IParticlePropertySvc*         m_ppsvc{ nullptr };
  Gaudi::Property<std::vector<std::string>> m_massConstraints{
      this, "MassConstraints" /*, [this]( auto& ) { this->sync_pids(); }*/ };
  std::map<int, double>                                 m_massConstraintMap;
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_matrix_inversion_failed{ this, "Problem inverting matrix!",
                                                                                   0 };
};

DECLARE_COMPONENT( ParticleVertexFitter )

/// Initialize
StatusCode ParticleVertexFitter::initialize() {
  StatusCode sc = GaudiTool::initialize();
  m_ppsvc       = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc" );
  if ( !m_ppsvc ) return StatusCode::FAILURE;
  /// Would rather trigger this via the Gaudi::Property, but it cannot work if the propertysvc has not yet been
  /// initialized
  this->sync_pids();
  return sc;
}

/// Finalize
StatusCode ParticleVertexFitter::finalize() {
  m_stateprovider.release().ignore();
  m_trajpoca.release().ignore();
  return GaudiTool::finalize();
}

namespace {

  // Class that represents a state-vector (x,y,tx,ty) at a given z. In
  // constrast to 'LHCb/StateVector' it does not have q/p, becaudse we
  // do not need that for vertexing.
  class StateVector4 {
  private:
    double         m_z;
    Gaudi::Vector2 m_x;
    Gaudi::Vector2 m_tx;

  public:
    StateVector4() = default;
    template <class State>
    StateVector4( const State& s ) : m_z( s.z() ) {
      m_x( 0 )  = s.x();
      m_x( 1 )  = s.y();
      m_tx( 0 ) = s.tx();
      m_tx( 1 ) = s.ty();
    }
    template <class XYZPoint, class XYZVector>
    StateVector4( const XYZPoint& point, const XYZVector& direction ) : m_z( point.z() ) {
      m_x( 0 )  = point.x();
      m_x( 1 )  = point.y();
      m_tx( 0 ) = direction.x() / direction.z();
      m_tx( 1 ) = direction.y() / direction.z();
    }
    double      z() const { return m_z; }
    double      x() const { return m_x( 0 ); }
    double      y() const { return m_x( 1 ); }
    double      tx() const { return m_tx( 0 ); }
    double      ty() const { return m_tx( 1 ); }
    const auto& slopes() const { return m_tx; }
    auto&       z() { return m_z; }
    auto&       x() { return m_x( 0 ); }
    auto&       y() { return m_x( 1 ); }
    auto&       tx() { return m_tx( 0 ); }
    auto&       ty() { return m_tx( 1 ); }
  };

  class State4 : public StateVector4 {
  public:
    const auto& covXX() const { return m_covXX; }
    const auto& covXT() const { return m_covXT; }
    const auto& covTT() const { return m_covTT; }
    auto&       covXX() { return m_covXX; }
    auto&       covXT() { return m_covXT; }
    auto&       covTT() { return m_covTT; }
    // Copied from LHCb::State
    void linearTransportTo( double z ) {
      const double dz  = z - StateVector4::z();
      const double dz2 = dz * dz;
      x() += dz * tx();
      y() += dz * ty();
      m_covXX( 0, 0 ) += dz2 * m_covTT( 0, 0 ) + 2 * dz * m_covXT( 0, 0 );
      m_covXX( 1, 1 ) += dz2 * m_covTT( 1, 1 ) + 2 * dz * m_covXT( 1, 1 );
      m_covXX( 1, 0 ) += dz2 * m_covTT( 1, 0 ) + dz * ( m_covXT( 0, 1 ) + m_covXT( 1, 0 ) );
      // would this make covXT always symmetric as well?
      m_covXT( 0, 0 ) += dz * m_covTT( 0, 0 );
      m_covXT( 0, 1 ) += dz * m_covTT( 0, 1 );
      m_covXT( 1, 0 ) += dz * m_covTT( 1, 0 );
      m_covXT( 1, 1 ) += dz * m_covTT( 1, 1 );
      // m_covXT      += dz*m_covTT ;
      StateVector4::z() = z;
    }

  private:
    Gaudi::SymMatrix2x2 m_covXX;
    Gaudi::SymMatrix2x2 m_covTT;
    Gaudi::Matrix2x2    m_covXT;
  };

  inline Gaudi::SymMatrix2x2 covXX( const State4& s ) { return s.covXX(); }
  inline Gaudi::SymMatrix2x2 covXX( const LHCb::State& s ) { return s.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 ); }
  inline Gaudi::Matrix2x2    covXT( const State4& s ) { return s.covXT(); }
  inline Gaudi::Matrix2x2    covXT( const LHCb::State& s ) { return s.covariance().Sub<Gaudi::Matrix2x2>( 0, 2 ); }

  template <typename Composite>
  inline State4 stateVectorFromComposite( const Composite& p ) {
    const auto&  px = p.momentum().Px();
    const auto&  py = p.momentum().Py();
    const auto&  pz = p.momentum().Pz();
    const double tx = px / pz;
    const double ty = py / pz;

    State4 s; //( p.endVertex()->position(), p.momentum() ) ;
    s.x()  = p.position().x();
    s.y()  = p.position().y();
    s.z()  = p.position().z();
    s.tx() = tx;
    s.ty() = ty;

    // For the computation of the Jacobian it is important to understand the following.
    //  x' = x + (z' - z) * tx
    // --> dx/dz = - tx ;
    //
    // Notation:
    //      J_{4,6} = ( Jxpos      Jxmom )
    //                ( Jtxpos     Jtxmom )
    // Jtxpos = 0 and Jxmom=0, so we rather do not introduce them. Instead, we'll compute Jxpos and Jxtxmom
    //
    // however, to test what we are doing, we first go full monty:
    // {
    //   // only 8 out of 24 elements are non-zero, so this is far too slow. we'll fix that later
    //   ROOT::Math::SMatrix<double,4,6> J ;
    //   J(0,0) = J(1,1) = 1 ;
    //   J(0,2) = -tx ;
    //   J(1,2) = -ty ;
    //   J(2,3) = 1/pz ;
    //   J(3,4) = 1/pz ;
    //   J(2,5) = -tx/pz ;
    //   J(3,5) = -ty/pz ;
    //   auto cov44 = Similarity( J, p.covMatrix().Sub<Gaudi::SymMatrix6x6>(0,0) ) ;
    //   std::cout << "cov44 full calculation: " << cov44 << std::endl ;
    // }
    ROOT::Math::SMatrix<double, 2, 3> Jxpos;
    Jxpos( 0, 0 ) = Jxpos( 1, 1 ) = 1;
    Jxpos( 0, 2 )                 = -tx;
    Jxpos( 1, 2 )                 = -ty;
    ROOT::Math::SMatrix<double, 2, 3> Jtxmom;
    Jtxmom( 0, 0 ) = 1 / pz;
    Jtxmom( 1, 1 ) = 1 / pz;
    Jtxmom( 0, 2 ) = -tx / pz;
    Jtxmom( 1, 2 ) = -ty / pz;
    Gaudi::SymMatrix4x4 cov44alt;
    const auto&         momposcov = p.posMomCovMatrix(); // it has the wrong name: it mompos not posmom
    s.covXT() = Jxpos * Transpose( momposcov.template Sub<Gaudi::Matrix3x3>( 0, 0 ) ) * Transpose( Jtxmom );
    // Jtxmom * momposcov.Sub<Gaudi::Matrix3x3>(0,0) * Transpose(Jxpos) ;
    s.covXX() = Similarity( Jxpos, p.posCovMatrix() );
    s.covTT() = Similarity( Jtxmom, p.momCovMatrix().template Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
    return s;
  }

  // Fast utility function for inverting a 2x2 matrix. This just comes
  // from ROOT/Math/Dinv, but I took out the test on the determinant
  // since we do not need it.

  inline void Invert2x2( Gaudi::SymMatrix2x2& m ) {
    auto rhs = m.Array();
    auto det = rhs[0] * rhs[2] - rhs[1] * rhs[1];
    //    if (det == T(0.)) { return false; }
    auto s   = double( 1.0 ) / det;
    auto c11 = s * rhs[2];
    rhs[1]   = -s * rhs[1];
    rhs[2]   = s * rhs[0];
    rhs[0]   = c11;
  }

  // Different daughter types
  enum DaughterType { TrackWithVelo, TrackWithoutVelo, Composite, Resonance, Other, NumTypes };

  // Base class for daughters in the vertex
  class VertexDaughter {
  public:
    VertexDaughter( const LHCb::Particle& p ) : m_particle( &p ) {}
    const LHCb::Particle& particle() const { return *m_particle; }

  protected:
    const LHCb::Particle* m_particle;
  };

  struct CompositeDaughter : VertexDaughter {
    Gaudi::LorentzVector m_p4;
    Gaudi::XYZPoint      m_pos;
    Gaudi::SymMatrix4x4  m_momCovMatrix; ///< Covariance matrix relative to momentum (4x4)
    Gaudi::SymMatrix3x3  m_posCovMatrix; ///< Covariance matrix relative to point at which the momentum is given (3x3)
    Gaudi::Matrix4x3 m_posMomCovMatrix;  ///< Matrix with correlation errors between momemtum and pointOnTrack (momentum
    CompositeDaughter( const LHCb::Particle& p )
        : VertexDaughter{ p }
        , m_p4{ p.momentum() }
        , m_pos{ p.endVertex()->position() }
        , m_momCovMatrix{ p.momCovMatrix() }
        , m_posCovMatrix{ p.endVertex()->covMatrix() }
        , m_posMomCovMatrix{ p.posMomCovMatrix() } {}
    CompositeDaughter( const LHCb::Particle& p, const double pdgmass )
        : VertexDaughter{ p }
        , m_p4{ p.momentum() }
        , m_pos{ p.endVertex()->position() }
        , m_momCovMatrix{ p.momCovMatrix() }
        , m_posCovMatrix{ p.endVertex()->covMatrix() }
        , m_posMomCovMatrix{ p.posMomCovMatrix() } {
      MassConstrainer::ConstrainMass( m_pos, m_p4, m_posCovMatrix, m_posMomCovMatrix, m_momCovMatrix, pdgmass );
    }
    const auto& position() const { return m_pos; }
    const auto& momentum() const { return m_p4; }
    const auto& momCovMatrix() const { return m_momCovMatrix; }
    const auto& posMomCovMatrix() const { return m_posMomCovMatrix; }
    const auto& posCovMatrix() const { return m_posCovMatrix; }
  };

  // Class for daughters with a straight-line trajectory
  template <typename State>
  class VertexTraj {
  protected:
    State                             m_state;
    ROOT::Math::SVector<double, 2>    m_q; // predicted/fitted slope (tx,ty)
    Gaudi::SymMatrix2x2               m_G; // weight matrix of (x,y) of state
    ROOT::Math::SMatrix<double, 2, 3> m_A; // projection matrix for vertex position
  public:
    VertexTraj( const State& state ) : m_state{ state }, m_q{ state.tx(), state.ty() } {
      // std::cout << "old fitted_slopes: " << m_q << std::endl;
    }

    const auto& state() const { return m_state; }

    void project( const ROOT::Math::SVector<double, 3>& vertexpos, ROOT::Math::SVector<double, 3>& halfDChisqDX,
                  Gaudi::SymMatrix3x3& halfD2ChisqDX2, double& chi2, int& ndof ) /* override final */
    {
      // move the state (not sure we should do it this way: maybe better to just cache the z.)
      m_state.linearTransportTo( vertexpos( 2 ) );

      // compute the weight matrix
      m_G = covXX( m_state );
      // m_G.Invert() ;
      Invert2x2( m_G );

      // compute residual
      Gaudi::Vector2 res{ vertexpos( 0 ) - m_state.x(), vertexpos( 1 ) - m_state.y() };

      // fill the projection matrix: use the fitted momentum!
      m_A( 0, 0 ) = m_A( 1, 1 ) = 1;
      m_A( 0, 2 )               = -m_q( 0 );
      m_A( 1, 2 )               = -m_q( 1 );

      // I tried to make this faster by writing it out, but H does
      // not contain sufficiently manby zeroes. Better to
      // parallelize.

      // I am not sure that the compilor realizes that A^T*G is used
      // more than once here, so I'll substitute the code from
      // similarity. Note that similarity does not return an expression.
      // halfD2ChisqDX2 += Similarity( Transpose(m_A), m_G ) ;
      // halfDChisqDX   += ( Transpose(m_A) * m_G) * res ;
      ROOT::Math::SMatrix<double, 3, 2> ATG = Transpose( m_A ) * m_G;
      Gaudi::SymMatrix3x3               thishalfD2ChisqDX2;
      ROOT::Math::AssignSym::Evaluate( thishalfD2ChisqDX2, ATG * m_A );
      halfD2ChisqDX2 += thishalfD2ChisqDX2;
      halfDChisqDX += ATG * res;
      chi2 += Similarity( res, m_G );
      ndof += 2;
    }

    void updateSlopes( const ROOT::Math::SVector<double, 3>& vertexpos ) /* override final */
    {
      // first update the residual. (note the subtle difference with that in project!)
      const double   dz = vertexpos( 2 ) - m_state.z();
      Gaudi::Vector2 res{ vertexpos( 0 ) - ( m_state.x() + m_state.tx() * dz ),
                          vertexpos( 1 ) - ( m_state.y() + m_state.ty() * dz ) };

      // get the matrix that is the correlation of (x,y) and (tx,ty,qop)
      // ROOT::Math::SMatrix<double,2,2> Vba = m_state.covariance().template Sub<ROOT::Math::SMatrix<double,2,2>>(2,0) ;
      // compute the corresponding gain matrix (this is WBG in the BFR fit, but here it is Vba * Vaa^-1)
      // ROOT::Math::SMatrix<double,2,2> K = Vba*m_G ;
      // compute the momentum vector
      m_q( 0 ) = m_state.tx();
      m_q( 1 ) = m_state.ty();
      m_q += Transpose( covXT( m_state ) ) * m_G * res;
    }

    void addToFourVector( const ROOT::Math::SVector<double, 3>& vertexpos, Gaudi::LorentzVector& p4,
                          Gaudi::SymMatrix4x4& p4cov, ROOT::Math::SMatrix<double, 4, 3>& gainmatrix ) const
        /* override final */;
  };

  /***********
   * class to deal with composites
   ***********/
  struct VertexComposite : public CompositeDaughter, public VertexTraj<State4> {
    // Constructors without mass constraint
    VertexComposite( const LHCb::Particle& p )
        : CompositeDaughter{ p }, VertexTraj<State4>{ stateVectorFromComposite( *this ) } {}
    // Constructor with mass constraint
    VertexComposite( const LHCb::Particle& p, double constrainedmass )
        : CompositeDaughter{ p, constrainedmass }, VertexTraj<State4>{ stateVectorFromComposite( *this ) } {}

    void addToFourVector( const ROOT::Math::SVector<double, 3>& vertexpos, Gaudi::LorentzVector& p4,
                          Gaudi::SymMatrix4x4& p4cov, ROOT::Math::SMatrix<double, 4, 3>& gainmatrix ) const {
      // first need to 'update' the momentum vector. but for that we
      // first need to 'transport'.
      const auto&  ppos = CompositeDaughter::position();
      const auto&  mom  = CompositeDaughter::momentum();
      const double pz   = mom.Pz();
      // const double tx = mom.Px()/pz ;
      // const double ty = mom.Py()/pz ;
      const double tx = m_q( 0 );
      const double ty = m_q( 1 );

      // first update the residual
      const double   dz = vertexpos( 2 ) - ppos.z();
      Gaudi::Vector2 res{ vertexpos( 0 ) - ( ppos.x() + tx * dz ), vertexpos( 1 ) - ( ppos.y() + ty * dz ) };
      const auto&    R         = m_state.covXX();
      const auto&    Rinv      = m_G;
      const auto&    momCov    = CompositeDaughter::momCovMatrix();
      const auto&    momPosCov = CompositeDaughter::posMomCovMatrix();

      // To do this right we need THREE projection matrices for the residual:
      //    r = A*x_vertex + B*mom_dau + C*x_dau
      // Note that C=-A.
      // Lets call the matrix F^T = (B^T C^T). then the uncertainty on the residual is
      //    R = F V77 F^T where V77 is the 7x7 covariance matrix of the daughter
      // We will need R every iteration
      // The correlation matrix between residual and the 7 parameters of the daughter is
      //    Vr_xmom = V77 F^T
      // Consequently, the gain matrix for the momentum-decayvertex is
      //    K72 = V77 F^T R^-1
      // Now, we do not need the updated parameters for the decay
      // vertex. If we just need the momentum part, then this reduces to
      //    K42 = ( V43 * C^T + V44 * B^T ) R^-1

      // The easiest implementation is this ...
      //  Gaudi::Matrix3x2 CT{ Transpose(m_A) } ;
      //  ROOT::Math::SMatrix<double,4,2> BT ;
      //  BT(0,0) = +dz/pz;
      //  BT(2,0) = -dz/pz*tx ;
      //  BT(1,1) = +dz/pz ;
      //  BT(2,1) = -dz/pz*ty ;
      //  ROOT::Math::SMatrix<double,4,2> K42 = ( momPosCov * CT + momCov * BT ) * Rinv ;
      // but since B and C are largely empty, we better write it out:
      const double                      dzopz = dz / pz;
      ROOT::Math::SMatrix<double, 4, 2> K42part;
      for ( int i = 0; i < 4; ++i )
        K42part( i, 0 ) =
            momPosCov( i, 0 ) + momPosCov( i, 2 ) * ( -tx ) + momCov( i, 0 ) * dzopz + momCov( i, 2 ) * ( -dzopz * tx );
      for ( int i = 0; i < 4; ++i )
        K42part( i, 1 ) =
            momPosCov( i, 1 ) + momPosCov( i, 2 ) * ( -ty ) + momCov( i, 1 ) * dzopz + momCov( i, 2 ) * ( -dzopz * ty );
      ROOT::Math::SMatrix<double, 4, 2> K42 = K42part * Rinv;

      // update the fourvector
      auto deltap4 = K42 * res;
      p4 += CompositeDaughter::momentum() +
            Gaudi::LorentzVector( deltap4( 0 ), deltap4( 1 ), deltap4( 2 ), deltap4( 3 ) );
      // to understand this, compare to what is done for the track
      p4cov += CompositeDaughter::momCovMatrix();
      p4cov -= Similarity( K42, R );
      gainmatrix += K42 * m_A;
    }
  };

  /***********
   * class to deal with tracks
   ***********/
  struct VertexTrack : public VertexDaughter, public VertexTraj<LHCb::State> {
    VertexTrack( const LHCb::Particle& p, const LHCb::State& state ) : VertexDaughter{ p }, VertexTraj{ state } {}

    void addToFourVector( const ROOT::Math::SVector<double, 3>& vertexpos, Gaudi::LorentzVector& p4,
                          Gaudi::SymMatrix4x4& p4cov, ROOT::Math::SMatrix<double, 4, 3>& gainmatrix ) const {
      // we first need to update q/p. since we also need the full gain matrix, we could as well redo that part.
      const double                      dz = vertexpos( 2 ) - m_state.z();
      Gaudi::Vector2                    res{ vertexpos( 0 ) - ( m_state.x() + m_q( 0 ) * dz ),
                          vertexpos( 1 ) - ( m_state.y() + m_q( 1 ) * dz ) };
      const auto                        Vba = m_state.covariance().Sub<ROOT::Math::SMatrix<double, 3, 2>>( 2, 0 );
      ROOT::Math::SMatrix<double, 3, 2> K   = Vba * m_G;
      Gaudi::Vector3                    q   = m_state.stateVector().Sub<Gaudi::Vector3>( 2 );
      q += K * res;
      // now transform to p4
      Gaudi::LorentzVector p4tmp;
      const double         mass  = particle().measuredMass();
      const int            abs_q = std::abs( particle().charge() );
      Gaudi::Math::geo2LA( q, mass, p4tmp );
      if ( abs_q != 1 ) { // correct for non-unity charge
        const double momentum = p4tmp.R() * abs_q;
        const double energy   = std::sqrt( momentum * momentum + mass * mass );
        p4tmp.SetPxPyPzE( p4tmp.X() * abs_q, p4tmp.Y() * abs_q, p4tmp.Z() * abs_q, energy );
      }
      p4 += p4tmp;
      ROOT::Math::SMatrix<double, 4, 3> dP4dMom;
      if ( abs_q == 0 || abs_q == 1 ) {
        Gaudi::Math::JacobdP4dMom( q, mass, dP4dMom );
      } else { // correct for non-unity charge
        dP4dMom = Gaudi::Math::JacobdP4dMom( q, mass, abs_q );
      }
      ROOT::Math::SMatrix<double, 4, 2> FK = dP4dMom * K;
      p4cov += Similarity( dP4dMom, m_state.covariance().Sub<Gaudi::SymMatrix3x3>( 2, 2 ) );
      p4cov -= Similarity( FK, m_state.covariance().Sub<Gaudi::SymMatrix2x2>( 0, 0 ) );
      gainmatrix += FK * m_A;

      // For brem-recovered electrons, we need to do something
      // special. So, electrons are an ugly exception here. We could
      // also add to q/p instead, which is cheaper, but perhaps even
      // more ugly.
      if ( particle().particleID().abspid() == 11 ) {
        const double absqop               = std::abs( m_state.qOverP() );
        const double momentumFromParticle = particle().momentum().P();
        // is 1% a reasonable threshold?
        if ( momentumFromParticle * absqop > 1.01 ) {
          const double momentumFromTrack       = 1 / absqop;
          const double momentumError2FromTrack = m_state.covariance()( 4, 4 ) * std::pow( momentumFromTrack, 4 );
          const double momentumError2FromParticle =
              particle().momCovMatrix()( 3, 3 ) * std::pow( particle().momentum().E() / momentumFromParticle, 2 );
          const double bremEnergyCov = momentumError2FromParticle - momentumError2FromTrack;
          const double bremEnergy    = momentumFromParticle - momentumFromTrack;
          // if the correction is unphysical, ignore it.
          if ( bremEnergyCov > 0 ) {
            const auto tx = q( 0 );
            const auto ty = q( 1 );
            auto       t  = std::sqrt( 1 + tx * tx + ty * ty );
            // we could also 'scale' the momentum, but we anyway need the components for the jacobian
            Gaudi::LorentzVector p4brem{ bremEnergy * tx / t, bremEnergy * ty / t, bremEnergy / t, bremEnergy };
            p4 += p4brem;
            ROOT::Math::SMatrix<double, 4, 1> J;
            J( 0, 0 ) = tx / t;
            J( 1, 0 ) = ty / t;
            J( 2, 0 ) = 1 / t;
            J( 3, 0 ) = 1;
            p4cov += Similarity( J, Gaudi::SymMatrix1x1{ bremEnergyCov } );
          }
        }
      }
    }
  };

  //*************************************************
  // Resonance
  //*************************************************
  class VertexResonance : public CompositeDaughter {
  private:
    Gaudi::Vector3      m_pos;
    Gaudi::SymMatrix3x3 m_G;

    void init() {
      const auto& pos = CompositeDaughter::position();
      m_pos           = Gaudi::Vector3{ pos.x(), pos.y(), pos.z() };
      const auto& cov = CompositeDaughter::posCovMatrix();
      m_G             = cov;
      m_G.InvertChol();
    }

  public:
    VertexResonance( const LHCb::Particle& p ) : CompositeDaughter{ p } { init(); }
    VertexResonance( const LHCb::Particle& p, double constrainedmass ) : CompositeDaughter{ p, constrainedmass } {
      init();
    }

    void project( const ROOT::Math::SVector<double, 3>& vertexpos, ROOT::Math::SVector<double, 3>& halfDChisqDX,
                  Gaudi::SymMatrix3x3& halfD2ChisqDX2, double& chi2, int& ndof ) /* override final */
    {
      Gaudi::Vector3 res = vertexpos - m_pos;

      // I tried to make this faster by writing it out, but H does
      // not contain sufficiently manby zeroes. Better to
      // parallelize.
      halfD2ChisqDX2 += m_G;
      halfDChisqDX += m_G * res;
      chi2 += Similarity( res, m_G );
      ndof += 3;
    }

    void addToFourVector( const ROOT::Math::SVector<double, 3>& vertexpos, Gaudi::LorentzVector& p4,
                          Gaudi::SymMatrix4x4& p4cov, ROOT::Math::SMatrix<double, 4, 3>& gainmatrix ) const {
      // compute momentum gain matrix
      ROOT::Math::SMatrix<double, 4, 3> K43 = CompositeDaughter::posMomCovMatrix() * m_G;
      // update the fourvector
      Gaudi::Vector3 res     = vertexpos - m_pos;
      auto           deltap4 = K43 * res;
      p4 += CompositeDaughter::momentum() +
            Gaudi::LorentzVector( deltap4( 0 ), deltap4( 1 ), deltap4( 2 ), deltap4( 3 ) );
      // to understand this, compare to what is done for the track
      p4cov += CompositeDaughter::momCovMatrix();
      const auto& poscov = CompositeDaughter::posCovMatrix();
      p4cov -= Similarity( K43, poscov );
      gainmatrix += K43;
    }

    void updateSlopes( const ROOT::Math::SVector<double, 3>& /*vertexpos*/ ) {}
  };

  //*************************************************
  // Other
  //*************************************************
  class VertexOther : public VertexDaughter {
  private:
    Gaudi::LorentzVector m_p4;
    Gaudi::SymMatrix4x4  m_p4cov;

  public:
    VertexOther( const LHCb::Particle& p ) : VertexDaughter{ p }, m_p4{ p.momentum() }, m_p4cov{ p.momCovMatrix() } {}
    VertexOther( const LHCb::Particle& p, double constrainedmass )
        : VertexDaughter{ p }, m_p4{ p.momentum() }, m_p4cov{ p.momCovMatrix() } {
      MassConstrainer::ConstrainMass( m_p4, m_p4cov, constrainedmass );
    }

    void project( const ROOT::Math::SVector<double, 3>& /* vertexpos */,
                  ROOT::Math::SVector<double, 3>& /* halfDChisqDX */, Gaudi::SymMatrix3x3& /* halfD2ChisqDX2 */,
                  double& /* chi2*/, int& /*ndof*/ ) {}

    void addToFourVector( const ROOT::Math::SVector<double, 3>& /*vertexpos*/, Gaudi::LorentzVector& p4,
                          Gaudi::SymMatrix4x4& p4cov, ROOT::Math::SMatrix<double, 4, 3>& /*gainmatrix*/ ) const {
      p4 += m_p4;
      p4cov += m_p4cov;
    }

    void updateSlopes( const ROOT::Math::SVector<double, 3>& /*vertexpos*/ ) {}
  };

  // Function to derive the category that a particular daughter belongs in
  inline DaughterType derivetype( const LHCb::Particle& p, const LHCb::IParticlePropertySvc& ppsvc ) {
    if ( p.proto() && p.proto()->track() ) {
      return p.proto()->track()->hasVelo() ? TrackWithVelo : TrackWithoutVelo;
    } else if ( p.daughters().size() > 0 && p.endVertex() && p.endVertex()->nDoF() >= 0 ) {
      const LHCb::ParticleProperty* prop        = ppsvc.find( p.particleID() );
      bool                          isresonance = prop && prop->ctau() < 0.001 * Gaudi::Units::mm;
      return isresonance ? Resonance : Composite;
    }
    return Other;
  }

  // Recursive function to collect daughters that are either a recontruction object or have a valid decayvertex
  template <typename ChildContainerIn, typename ChildContainerOut>
  void collectdaughters( const ChildContainerIn& daughtersin, ChildContainerOut& daughtersout ) {
    for ( const auto& p : daughtersin )
      if ( p->daughters().size() > 0 ) {
        if ( p->endVertex() && p->endVertex()->nDoF() >= 0 )
          daughtersout.emplace_back( p );
        else
          collectdaughters( p->daughters(), daughtersout );
      } else {
        daughtersout.emplace_back( p );
      }
  }

} // namespace

StatusCode ParticleVertexFitter::fit( const LHCb::Particle::ConstVector& origdaughters, LHCb::Vertex& vertex,
                                      LHCb::Particle* mother, const IGeometryInfo& geometry ) const {
  // for the vertex fit, we distinguish:
  // - TracksWithVelo: tracks with velo hits (long, upstream, velo-only)
  // - TracksWithoutVelo: tracks without velo hits (only downstream, hopefully)
  // - Resonances:  composites with ctau<1micron
  // - Composities: other composites
  // - Others: everything else, e.g. photons, pi0, jets
  constexpr int                                                                 DefaultMaxNumDaughters{ 10 };
  boost::container::small_vector<const LHCb::Particle*, DefaultMaxNumDaughters> daughters;
  collectdaughters( origdaughters, daughters );

  const size_t                                                         N = daughters.size();
  boost::container::small_vector<DaughterType, DefaultMaxNumDaughters> types{ N };
  std::transform( daughters.begin(), daughters.end(), types.begin(),
                  [this]( const LHCb::Particle* p ) { return derivetype( *p, *m_ppsvc ); } );
  std::array<unsigned char, NumTypes> counttypes{ 0 };
  std::for_each( types.begin(), types.end(), [&]( const DaughterType& t ) { counttypes[int( t )] += 1; } );

  // Obtain an estimate of the vertex position for initialization:
  // 1. if there are 'resonance' daughters, take the vertex position of the first.
  // 2. if not, use the states of the first two tracks with velo or composites
  // 3. if not, try with downstream tracks and trajpoca
  bool posinitialized{ false };

  Gaudi::XYZPoint position;
  if ( counttypes[Resonance] > 0 ) {
    // try 1
    for ( size_t i = 0; i < N && !posinitialized; ++i )
      if ( types[i] == Resonance ) {
        position       = daughters[i]->endVertex()->position();
        posinitialized = true;
      }
  } else if ( counttypes[TrackWithVelo] + counttypes[Composite] >= 2 ) {
    // try 2
    StateVector4  velotrajs[2];
    unsigned char nvelotrajs( 0 );
    for ( size_t i = 0; i < N && nvelotrajs < 2; ++i ) {
      if ( types[i] == TrackWithVelo ) {
        velotrajs[nvelotrajs++] = StateVector4( daughters[i]->proto()->track()->firstState() );
      } else if ( types[i] == Composite ) {
        velotrajs[nvelotrajs++] = StateVector4( daughters[i]->endVertex()->position(), daughters[i]->momentum() );
      }
    }
    if ( nvelotrajs == 2 ) {
      LHCb::TrackVertexUtils::poca( velotrajs[0], velotrajs[1], position );
      posinitialized = true;
    }
  } else if ( counttypes[TrackWithVelo] + counttypes[Composite] + counttypes[TrackWithoutVelo] >= 2 ) {
    // try 3. create two trajectories, then use trajpoca
    boost::container::static_vector<LHCb::LineTraj<double>, 2> ownedtrajs;
    const LHCb::Trajectory<double>*                            trajs[2];
    unsigned char                                              ntrajs( 0 );
    for ( size_t i = 0; i < N && ntrajs < 2; ++i ) {
      if ( types[i] == TrackWithVelo || types[i] == TrackWithoutVelo ) {
        auto traj = m_stateprovider->trajectory( *( daughters[i]->proto()->track() ), geometry );
        if ( traj ) trajs[ntrajs++] = traj;
      } else if ( types[i] == Composite ) {
        ownedtrajs.emplace_back( daughters[i]->endVertex()->position(), daughters[i]->momentum().Vect(),
                                 std::make_pair( 0., 1. ) );
        trajs[ntrajs++] = &( ownedtrajs.back() );
      }
    }
    bool parallelTracks = false;
    if ( ntrajs == 2 && m_extrapolateTtracks ) {
      // this is NOT default behaviour
      // For T-tracks it does not make sense to use 3-D POCA starting position
      // Trajectory in YZ plane is approx straight line
      // calculate the yz intersection z and extrapolate there

      const float ty0 = daughters[0]->proto()->track()->firstState().ty();
      const float ty1 = daughters[1]->proto()->track()->firstState().ty();

      if ( LHCb::essentiallyEqual( ty0, ty1 ) ) {
        debug() << "tracks are parallel in yz plane. Cannot calculate yz intersection. Default to 3-D POCA starting "
                   "position. "
                << endmsg;
        parallelTracks = true;
      } else {
        const float z0 = daughters[0]->proto()->track()->firstState().z();
        const float z1 = daughters[1]->proto()->track()->firstState().z();

        const float y0 = daughters[0]->proto()->track()->firstState().y();
        const float y1 = daughters[1]->proto()->track()->firstState().y();

        // calculate the y-axis intersection of each track
        const float c0 = y0 - ty0 * z0;
        const float c1 = y1 - ty1 * z1;

        // calculate the point in the yz-plane where the tracks intersect
        const float yzIntersectionZ = ( c1 - c0 ) / ( ty0 - ty1 );
        const float yzIntersectionY = ty0 * yzIntersectionZ + c1;

        position.SetZ( yzIntersectionZ );
        position.SetY( yzIntersectionY );
        position.SetX( 0 );
        posinitialized = true;
      }
    }

    if ( ( ntrajs == 2 && !m_extrapolateTtracks ) || ( ntrajs == 2 && parallelTracks ) ) {
      // this is the default behaviour
      double           mu0( 0 ), mu1( 0 );
      Gaudi::XYZVector deltaX;
      StatusCode sc = m_trajpoca->minimize( *( trajs[0] ), mu0, *( trajs[1] ), mu1, deltaX, 0.1 * Gaudi::Units::mm );
      if ( sc.isSuccess() ) {
        auto pos0      = trajs[0]->position( mu0 );
        auto pos1      = trajs[1]->position( mu1 );
        position       = pos0 + 0.5 * ( pos1 - pos0 );
        posinitialized = true;
      }
    }
  }

  if ( !posinitialized ) {
    error() << "ParticleVertexFitter didn't properly initialize: "
            << " #tracks with velo: " << int( counttypes[TrackWithVelo] )
            << " #tracks without velo: " << int( counttypes[TrackWithoutVelo] )
            << " #composites: " << int( counttypes[Composite] ) << " #resonances: " << int( counttypes[Resonance] )
            << endmsg;
    return StatusCode::FAILURE;
  }

  // Now that we have an estimate of the vertex, we initialize everything that goes into the vertex.
  using vertex_member = std::variant<VertexOther, VertexTrack, VertexComposite, VertexResonance>;
  std::vector<vertex_member> vertexdaughters;
  vertexdaughters.reserve( N );
  // the last trick: as long as the current estimate tells us
  // that we are within the beampipe, we could as well just use
  // the first state.
  const bool vertexinsidefoil = true; // position.Perp2() < 8. * 8.; // Rxy < 8 mm //TODO
  for ( size_t i = 0; i < N; ++i ) {
    auto p = daughters[i];
    switch ( types[i] ) {
    case TrackWithVelo: {
      const LHCb::Track* track = p->proto()->track();
      if ( vertexinsidefoil )
        vertexdaughters.emplace_back( VertexTrack{ *p, track->firstState() } );
      else {
        // If we are not inside the beam-pipe, use tracktraj
        const LHCb::TrackTraj* tracktraj = m_stateprovider->trajectory( *( p->proto()->track() ), geometry );
        vertexdaughters.emplace_back(
            VertexTrack{ *p, tracktraj ? tracktraj->state( position.z() ) : track->firstState() } );
      }
    } break;
    case TrackWithoutVelo: {
      if ( !m_extrapolateTtracks ) {
        // this is the default behaviour
        const LHCb::Track*     track     = p->proto()->track();
        const LHCb::TrackTraj* tracktraj = m_stateprovider->trajectory( *( p->proto()->track() ), geometry );
        vertexdaughters.emplace_back(
            VertexTrack{ *p, tracktraj ? tracktraj->state( position.z() ) : track->firstState() } );
      } else {
        // useful for some applications -- extrapolate instead of using TrackTraj
        const LHCb::Track* track = p->proto()->track();
        LHCb::State        extrapolatedState;
        StatusCode         success_extr = m_stateprovider->state( extrapolatedState, *track, position.z(), geometry );
        vertexdaughters.emplace_back( VertexTrack{ *p, success_extr ? extrapolatedState : track->firstState() } );
      }

    } break;

    case Composite: {
      auto massconstraint = massConstraint( *p );
      vertexdaughters.emplace_back( massconstraint ? VertexComposite{ *p, *massconstraint } : VertexComposite{ *p } );
      break;
    }
    case Resonance: {
      auto massconstraint = massConstraint( *p );
      vertexdaughters.emplace_back( massconstraint ? VertexResonance{ *p, *massconstraint } : VertexResonance{ *p } );
    } break;
    case Other:
      vertexdaughters.emplace_back( VertexOther{ *p } );
      break;
    case NumTypes:
      break;
    }
  }

  // run vertex fit iterations
  Gaudi::Vector3 posvec{ position.x(), position.y(), position.z() };
  // std::cout << "old vertex_position: " << posvec << std::endl;
  Gaudi::SymMatrix3x3 poscov;
  double              chi2{ 0 };
  int                 ndof{ 0 };
  bool                converged{ false };
  for ( unsigned short iter{ 0 }; iter < m_maxnumiter && !converged; ++iter ) {
    chi2 = 0;
    ndof = -3;
    Gaudi::SymMatrix3x3            halfD2ChisqDX2;
    ROOT::Math::SVector<double, 3> halfDChisqDX;

    // add all particles that contribute to the vertex chi2
    for ( auto& dau : vertexdaughters )
      std::visit( [&]( auto& d ) { d.project( posvec, halfDChisqDX, halfD2ChisqDX2, chi2, ndof ); }, dau );

    // calculate the covariance and the change in the position
    poscov        = halfD2ChisqDX2;
    const bool ok = poscov.InvertChol();
    if ( !ok ) {
      ++m_matrix_inversion_failed;
      return StatusCode::FAILURE;
    }
    ROOT::Math::SVector<double, 3> dpos = -poscov * halfDChisqDX;
    // update the position
    posvec += dpos;
    // compute the delta-chisquare
    double dchisq = Dot( dpos, halfDChisqDX );
    chi2 += dchisq;
    converged = -dchisq < m_maxdchisq;

    // update the slopes
    if ( !converged )
      for ( auto& dau : vertexdaughters ) std::visit( [&]( auto& d ) { d.updateSlopes( posvec ); }, dau );
  }

  // Now fill the particle
  Gaudi::XYZPoint pos{ posvec( 0 ), posvec( 1 ), posvec( 2 ) };
  if ( mother ) {
    // compute the total fourvector and all other parts of the covariance matrix
    Gaudi::LorentzVector              p4;
    Gaudi::SymMatrix4x4               p4cov;
    ROOT::Math::SMatrix<double, 4, 3> gainmatrix;
    for ( const auto& dau : vertexdaughters )
      std::visit( [&]( const auto& d ) { d.addToFourVector( posvec, p4, p4cov, gainmatrix ); }, dau );
    p4cov += Similarity( gainmatrix, poscov );
    Gaudi::Matrix4x3 covmompos = gainmatrix * poscov;
    // apply the mother mass constraint, if requested
    auto massconstraint = massConstraint( *mother );
    if ( massconstraint ) {
      const double masschi2 = MassConstrainer::ConstrainMass( pos, p4, poscov, covmompos, p4cov, *massconstraint );
      if ( masschi2 > 0 ) {
        chi2 += masschi2;
        ndof += 1;
      }
    }
    // Now set the mother position and momentum
    mother->setEndVertex( &vertex );
    mother->setReferencePoint( pos );
    mother->setPosCovMatrix( poscov );
    mother->setMomentum( p4 );
    mother->setMomCovMatrix( p4cov );
    mother->setPosMomCovMatrix( covmompos );
    const auto mass = p4.M();
    mother->setMeasuredMass( mass );
    const Gaudi::Vector4 jacobian{ -p4.Px() / mass, -p4.Py() / mass, -p4.Pz() / mass, p4.E() / mass };
    const auto           massErr2 = Similarity( p4cov, jacobian );
    mother->setMeasuredMassErr( massErr2 > 0 ? std::sqrt( massErr2 ) : 0.0 );
  }
  // Fill the vertex after dealing with an eventual mass constraint
  vertex.setPosition( pos );
  vertex.setCovMatrix( poscov );
  vertex.setChi2AndDoF( chi2, ndof );
  vertex.setOutgoingParticles( SmartRefVector<LHCb::Particle>{ origdaughters.begin(), origdaughters.end() } );
  vertex.setTechnique( LHCb::Vertex::VertexFitter );

  return StatusCode::SUCCESS;
}
