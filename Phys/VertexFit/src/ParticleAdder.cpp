/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/IParticleCombiner.h" // Interface

#include "Event/Particle.h"
#include "Event/Vertex.h"

#include "GaudiAlg/GaudiTool.h"

using namespace LHCb;
using namespace Gaudi::Units;

/**
 *  Simple Particle Momentum Adder w/ IParticleCombiner interface
 *
 *  @author Yasmine Amhis & Olivier Deschamps
 *  @date   2006-11-30
 */
template <int AssignChildVertex = 0>
struct ParticleAdder : GaudiTool, virtual IParticleCombiner {
  ParticleAdder( const std::string& type, const std::string& name, const IInterface* parent );
  StatusCode combine( const LHCb::Particle::ConstVector& daughters, LHCb::Particle& mother, LHCb::Vertex& vertex,
                      const IGeometryInfo& geometry ) const override;
};

// Standard constructor, initializes variables
template <int AssignChildVertex>
ParticleAdder<AssignChildVertex>::ParticleAdder( const std::string& type, const std::string& name,
                                                 const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  declareInterface<IParticleCombiner>( this );
}

// Fit the vertex from a vector of Particles
template <int AssignChildVertex>
StatusCode ParticleAdder<AssignChildVertex>::combine( const LHCb::Particle::ConstVector& parts, LHCb::Particle& P,
                                                      LHCb::Vertex&, const IGeometryInfo& ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "start ParticleAdder fit " << endmsg;
    debug() << "using " << parts.size() << " particles" << endmsg;
  }

  P.clearDaughters();
  Gaudi::LorentzVector momentum;
  Gaudi::SymMatrix4x4  momcovmatrix;

  for ( const auto& daughter : parts ) {
    P.addToDaughters( daughter );
    momentum += daughter->momentum();
    momcovmatrix += daughter->momCovMatrix();
  }
  P.setMomentum( momentum );
  P.setMomCovMatrix( momcovmatrix );
  const auto mass = momentum.M();
  P.setMeasuredMass( mass );
  const Gaudi::Vector4 jacobian( -momentum.Px() / mass, -momentum.Py() / mass, -momentum.Pz() / mass,
                                 momentum.E() / mass );
  const auto           massErr2 = ROOT::Math::Similarity( momcovmatrix, jacobian );
  P.setMeasuredMassErr( massErr2 > 0 ? std::sqrt( massErr2 ) : 0.0 );

  if constexpr ( AssignChildVertex > 0 ) {
    const auto daughter = parts[AssignChildVertex - 1];
    P.setEndVertex( daughter->endVertex() );
    P.setPosCovMatrix( daughter->posCovMatrix() );
    P.setPosMomCovMatrix( daughter->posMomCovMatrix() );
    P.setReferencePoint( daughter->referencePoint() );
  }

  return StatusCode::SUCCESS;
}

// Declaration of the Tool Factory
DECLARE_COMPONENT_WITH_ID( ParticleAdder<0>, "ParticleAdder" )
DECLARE_COMPONENT_WITH_ID( ParticleAdder<1>, "ParticleAdderAssignVtx1" )
DECLARE_COMPONENT_WITH_ID( ParticleAdder<2>, "ParticleAdderAssignVtx2" )
