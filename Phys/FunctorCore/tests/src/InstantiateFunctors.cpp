/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSelection
#include "Functors/JIT_includes.h"

#include <boost/mp11/algorithm.hpp>
#include <boost/mp11/bind.hpp>
#include <boost/test/unit_test.hpp>

using namespace boost::mp11;
namespace FT     = Functors::Track;
namespace FPID   = Functors::PID;
namespace FC     = Functors::Common;
namespace FF     = Functors::Functional;
namespace FP     = Functors::Particle;
using mask_arg_t = Functors::mask_arg_t;

//
// The idea of this file is not to run any given functor but to check that it at least compiles.
// We do this by using `std::invoke_result_t` which is a helper function that
// returns the type of the return value of a function/functor F when invoked with
// arguments of certain types
//
//  Example: let's take the Functors FT::Slopes
//
//  using t = std::invoke_result_t<FT::Slopes, LHCb::Track>;
//
//  now t = float. We don't really care about that type though, but the fact
//  that the compiler has to invoke the templated functor FT::Slopes and check
//  what happens if we call it with LHCb::Track, to obtain that result.
//
//  That's all the below code does for various functors. And we use a bit of
//  boost::mp11 and the below ptr_and_ref helper to make this a bit easier.
//
// given a list of types, returns a mp_list of those types with lvalue ref and
// pointer e.g. using x = ptr_and_ref<int, doublt> -> mp_list<int&, int*, double&, double*>
template <typename... Ts>
using ptr_and_ref = mp_append<mp_transform<std::add_lvalue_reference_t, mp_list<Ts...>>,
                              mp_transform<std::add_pointer_t, mp_list<Ts...>>>;

//
//  Let's think about what kind of types we can encounter
//
//  Tracklike:        LHCb::Track{&,*}, LHCb::Event::v3::Tracks
//  Charged basics:   LHCb::Particle{&,*}, LHCb::Event::ChargedBasics
//
//  The {Two,Three,Four}BodyCombiner takes as input LHCb::Particles and returns
//  a composite LHCb::Particle.
//
//  The ThorCombiner takes as input various combinations of
//  Event::ChargedBasics, Event::Composites, Event::Particle (which is a
//  variant of composite and charged basic) and produces a Event::Composite as
//  output
//
//
//  Composite:        LHCb::Particle{&,*}, LHCb::Event::Composites
//  Combinations:     LHCb::ParticleCombination<T>, Sel::ParticleCombination<child1_t, child2_t,...>,
//  Sel::ParticleCombinationSpan Vertices:         LHCb::RecVertex, LHCb::Event::v2::RecVertex,
//  LHCb::PrimaryVertex VertexContainers: LHCb::RecVertices, LHCb::Event::v2::RecVertices,
//  LHCb::Event::PV::PrimaryVertexContainer
//
//  MC stuff: LHCb::MCParticle, LHCB::MCVertex
//

// get the type of a single proxy of these SOACollections
using v3_track      = decltype( *std::declval<LHCb::Event::v3::Tracks const>().scalar().begin() );
using charged_basic = decltype( *std::declval<LHCb::Event::ChargedBasics const>().scalar().begin() );
using soa_composite = decltype( *std::declval<LHCb::Event::Composites const>().scalar().begin() );

// to save ourselfes a lot of writing we use mp_product below a lot.
// mp_product calls invoke_result_t for each combination of the list of functors and provided arguments
// see mp_product doc: https://www.boost.org/doc/libs/master/libs/mp11/doc/html/mp11.html#mp_productf_l
using test1 = mp_product<std::invoke_result_t,
                         mp_list<decltype( FT::Slopes ), decltype( FT::ReferencePoint ), decltype( FT::ThreeMomentum )>,
                         mp_append<mp_list<v3_track, charged_basic>,
                                   ptr_and_ref<LHCb::Track const, LHCb::Particle const, LHCb::MCParticle const>>>;

using test2 = mp_product<std::invoke_result_t, mp_list<decltype( FT::FourMomentum )>,
                         mp_append<ptr_and_ref<LHCb::Track const, LHCb::Particle const>, mp_list<charged_basic>>>;

using test3 =
    mp_product<std::invoke_result_t, mp_list<decltype( FT::Charge )>,
               mp_append<ptr_and_ref<LHCb::Track const, LHCb::Particle const>, mp_list<charged_basic, v3_track>>>;

//
// CHECK FOR COMPOSED FUNCTORS
//
// Helper to get the type of the lambda that is returned from a prepare call of
// a composed functor of functors F1 and F2
template <typename F1, typename F2>
using composed_t =
    decltype( std::declval<decltype( Functors::detail::compose<Functors::Function, Functors::Function>(
                  Functors::detail::composition::chain{}, std::declval<F1>(), std::declval<F2>() ) ) const&>()
                  .prepare( EventContext{}, Functors::TopLevelInfo{} ) );

template <typename F>
using apply_vec_accessors_to =
    mp_list<composed_t<decltype( FC::X_Coordinate ), F>, composed_t<decltype( FC::Y_Coordinate ), F>,
            composed_t<decltype( FC::Z_Coordinate ), F>, composed_t<decltype( FC::Phi_Coordinate ), F>,
            composed_t<decltype( FC::Eta_Coordinate ), F>, composed_t<decltype( FC::Rho_Coordinate ), F>,
            composed_t<decltype( FC::Magnitude ), F>>;

using all_vec_accessors =
    mp_flatten<mp_transform<apply_vec_accessors_to, mp_list<decltype( FT::ReferencePoint ),
                                                            decltype( FT::ThreeMomentum ), decltype( FT::Slopes )>>>;

using test4 = mp_product<std::invoke_result_t, all_vec_accessors, mp_list<mask_arg_t>, mp_list<bool>,
                         mp_append<mp_list<v3_track, charged_basic>,
                                   ptr_and_ref<LHCb::Track const, LHCb::Particle const, LHCb::MCParticle const>>>;

using mom4_accessors = mp_list<composed_t<decltype( FC::X_Coordinate ), decltype( FT::FourMomentum )>,
                               composed_t<decltype( FC::Y_Coordinate ), decltype( FT::FourMomentum )>,
                               composed_t<decltype( FC::Z_Coordinate ), decltype( FT::FourMomentum )>,
                               composed_t<decltype( FC::Magnitude ), decltype( FT::FourMomentum )>,
                               composed_t<decltype( FC::E_Coordinate ), decltype( FT::FourMomentum )>>;

// FIXME fourmomentum is broken for v1 Track as it returns a 3-mom Gaudi::XYZVector
using test5 = mp_product<std::invoke_result_t, mom4_accessors, mp_list<mask_arg_t>, mp_list<bool>,
                         mp_append<mp_list<charged_basic>, ptr_and_ref<LHCb::Particle const, LHCb::MCParticle const>>>;

// check this helper works with the specified set of types:

using PRV2 = decltype( Sel::Utils::impactParameterChi2( std::declval<LHCb::Particle const&>(),
                                                        std::declval<LHCb::Event::v2::RecVertex const&>() ) );

using PRV1 = decltype( Sel::Utils::impactParameterChi2( std::declval<LHCb::Particle const&>(),
                                                        std::declval<LHCb::RecVertex const&>() ) );

using IPCHI2PV = decltype( Sel::Utils::impactParameterChi2( std::declval<LHCb::Particle const&>(),
                                                            std::declval<LHCb::PrimaryVertex const&>() ) );

using IPCHI2PV_ptr = decltype( Sel::Utils::impactParameterChi2( std::declval<LHCb::Particle const*>(),
                                                                std::declval<LHCb::PrimaryVertex const&>() ) );

using ip =
    mp_product<std::invoke_result_t, mp_list<decltype( FC::ImpactParameter )>,
               mp_list<LHCb::LinAlg::Vec<float, 3> const>,
               mp_append<mp_list<v3_track, charged_basic>, ptr_and_ref<LHCb::Particle const, LHCb::Track const>>>;

using ipchi2 =
    mp_product<std::invoke_result_t, mp_list<decltype( FC::ImpactParameterChi2 )>,
               ptr_and_ref<LHCb::Event::v2::RecVertex const, LHCb::RecVertex const, LHCb::PrimaryVertex const>,
               mp_append<mp_list<v3_track, charged_basic>, ptr_and_ref<LHCb::Particle const, LHCb::Track const>>>;

using ipchi2_to_vtx =
    mp_product<std::invoke_result_t, mp_list<decltype( FC::ImpactParameterChi2ToVertex )>,
               mp_list<LHCb::Event::PV::PrimaryVertexContainer const&, LHCb::RecVertices const&>,
               mp_append<mp_list<v3_track, charged_basic>, ptr_and_ref<LHCb::Particle const, LHCb::Track const>>>;

using pid = mp_product<std::invoke_result_t,
                       mp_list<decltype( FPID::IsMuon ), decltype( FT::PIDmu ), decltype( FT::PIDp ),
                               decltype( FT::PIDe ), decltype( FT::PIDk, FT::PIDpi )>,
                       mp_append<mp_list<charged_basic>, ptr_and_ref<LHCb::Particle const>>>;

using muonpid =
    mp_product<std::invoke_result_t,
               mp_list<decltype( FPID::IsMuon ), decltype( FPID::IsMuonTight ), decltype( FPID::MuonChi2Corr ),
                       decltype( FPID::MuonLLMu ), decltype( FPID::MuonLLBg ), decltype( FPID::MuonCatBoost )>,
               mp_append<mp_list<charged_basic>, ptr_and_ref<LHCb::Particle const>>>;

// For more complex functors it is easier to do them one by one, otherwise
// compilation errors might kill us :(

//
// FlightDistanceChi2ToVertex( vertex, composite )
//
using fdchi2_1 = std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ), LHCb::PrimaryVertex,
                                      LHCb::Particle const&>;
using fdchi2_2 = std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ), LHCb::PrimaryVertex,
                                      LHCb::Particle const*>;
using fdchi2_3 = std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ), LHCb::RecVertex,
                                      LHCb::Particle const*>;
using fdchi2_4 = std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ),
                                      LHCb::Event::v2::RecVertex, LHCb::Particle const*>;
using fdchi2_5 = std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ), LHCb::PrimaryVertex,
                                      soa_composite>;
using fdchi2_6 =
    std::invoke_result_t<decltype( Functors::Composite::FlightDistanceChi2ToVertex ), LHCb::RecVertex, soa_composite>;

//
// CorrectedMass( vertex, composite )
//
using cm1 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::PrimaryVertex, LHCb::Particle const&>;
using cm2 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::PrimaryVertex, LHCb::Particle const*>;
using cm3 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::Event::v2::RecVertex, LHCb::Particle const&>;
using cm4 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::PrimaryVertex, soa_composite>;
using cm5 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::Event::v2::RecVertex, soa_composite>;
// FIXME shouldn't these work?
// using cm6 = std::invoke_result_t<Functors::Composite::CorrectedMass, LHCb::RecVertex, LHCb::Particle const&>;

//
// CorrectedMassError( vertex, composite )
//
using cmerr1 =
    std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::PrimaryVertex, LHCb::Particle const&>;
using cmerr2 =
    std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::PrimaryVertex, LHCb::Particle const*>;
using cmerr3 =
    std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::Event::v2::RecVertex, LHCb::Particle const&>;
using cmerr4 = std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::PrimaryVertex, soa_composite>;
using cmerr5 = std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::Event::v2::RecVertex, soa_composite>;
// FIXME shouldn't these work?
// using cmerr6 = std::invoke_result_t<Functors::Composite::CorrectedMassError, LHCb::RecVertex, LHCb::Particle const&>;

//
// BTracking::CorrectedMass( heavyflavourtrackrelations, composite )
//
using cmbt1 = std::invoke_result_t<decltype( Functors::Composite::BTracking::Track ),
                                   LHCb::Relation2D<LHCb::Particle, LHCb::Event::v1::Track>, LHCb::Particle const&>;
using cmbt2 = std::invoke_result_t<decltype( Functors::Composite::BTracking::CorrectedMass ),
                                   LHCb::Relation2D<LHCb::Particle, LHCb::Event::v1::Track>, LHCb::Particle const&>;

//
// MassWithHypotheses( vertices, composite )
//

using mh_1 = std::invoke_result_t<Functors::Composite::MassWithHypotheses, LHCb::Particle const&>;
using mh_2 = std::invoke_result_t<Functors::Composite::MassWithHypotheses, LHCb::Particle const*>;
using mh_3 =
    std::invoke_result_t<Functors::Composite::MassWithHypotheses, LHCb::ParticleCombination<LHCb::Particle> const&>;
// FIXME can't iterate over Sel::ParticleCombination -> no begin & end
// using mh_4 = std::invoke_result_t<Functors::Composite::MassWithHypotheses<float,float>,
// Sel::ParticleCombinationN<charged_basic, 2> const& >;
// FIXME ParticleCombinationSpan doesn't have a size method
// using mh_5 = std::invoke_result_t<Functors::Composite::MassWithHypotheses<float,float>, pcs>;

//
// Mass
//

using m_1 = std::invoke_result_t<decltype( Functors::Composite::Mass ), LHCb::Particle const&>;
using m_2 = std::invoke_result_t<decltype( Functors::Composite::Mass ), LHCb::Particle const*>;
using m_3 = std::invoke_result_t<decltype( Functors::Composite::Mass ), soa_composite>;
// FIXME charged basic doesn't support mass2()
// using m_4 = std::invoke_result_t<Functors::Composite::Mass, charged_basic>;

//
// Liftetime & DLS
//

using l_1 =
    std::invoke_result_t<decltype( Functors::Composite::Lifetime ), LHCb::RecVertex const&, LHCb::Particle const&>;
using l_1 = std::invoke_result_t<decltype( Functors::Composite::Lifetime ), LHCb::Event::v2::RecVertex const*,
                                 LHCb::Particle const*>;
using l_1 = std::invoke_result_t<decltype( Functors::Composite::Lifetime ), LHCb::PrimaryVertex const&, soa_composite>;

using dls_1 = std::invoke_result_t<decltype( Functors::Composite::ComputeDecayLengthSignificance ),
                                   LHCb::RecVertex const&, LHCb::Particle const&>;
using dls_2 = std::invoke_result_t<decltype( Functors::Composite::ComputeDecayLengthSignificance ),
                                   LHCb::Event::v2::RecVertex const*, LHCb::Particle const*>;
using dls_3 = std::invoke_result_t<decltype( Functors::Composite::ComputeDecayLengthSignificance ),
                                   LHCb::PrimaryVertex const&, soa_composite>;

auto chi2perdof_func      = Functors::chain( FF::ValueOr{ 0. }, FT::Chi2PerDoF, FP::GetTrack );
using chi2perdof_prepared = decltype( chi2perdof_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using chi2perdof_1        = std::invoke_result_t<chi2perdof_prepared, mask_arg_t, bool, LHCb::Particle const&>;
using chi2perdof_2        = std::invoke_result_t<chi2perdof_prepared, mask_arg_t, bool, LHCb::Particle const*>;
// // needed because Chi2PerDof has overloads see Moore#452
using chi2perdof_3 = std::invoke_result_t<chi2perdof_prepared, mask_arg_t, bool, LHCb::Particle*>;
using chi2perdof_4 = std::invoke_result_t<chi2perdof_prepared, mask_arg_t, bool, charged_basic>;
using chi2perdof_5 = std::invoke_result_t<chi2perdof_prepared, mask_arg_t, bool, soa_composite>;

auto chi2_func      = Functors::chain( FF::ValueOr{ 0. }, FT::Chi2, FP::GetTrack );
using chi2_prepared = decltype( chi2_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using chi2_1        = std::invoke_result_t<chi2_prepared, mask_arg_t, bool, LHCb::Particle const&>;
using chi2_2        = std::invoke_result_t<chi2_prepared, mask_arg_t, bool, LHCb::Particle const*>;
// // needed because Chi2PerDof has overloads see Moore#452
using chi2_3 = std::invoke_result_t<chi2_prepared, mask_arg_t, bool, LHCb::Particle*>;
using chi2_4 = std::invoke_result_t<chi2_prepared, mask_arg_t, bool, charged_basic>;
using chi2_5 = std::invoke_result_t<chi2_prepared, mask_arg_t, bool, soa_composite>;

auto PT = Functors::chain( ::FC::Rho_Coordinate, ::FT::ThreeMomentum );

// Test for ALLPV_FD
//
// create an instance of ALLPV flight distance functor (obtained from functor.code() in python)
auto allpv_fd_func =
    // bind map and TES functors with forwarding the arguments
    Functors::bind( FF::Map( /* The functor to map over a range. */
                             // chain magnitude and flight distance functor
                             Functors::chain(
                                 // get magnitude of the flight distance vector
                                 FC::Magnitude,
                                 // subtract the vertex position from the particle position
                                 operator-(
                                     // vertex position: LHCb::Vertex -> Gaudi::XYZPoint -> vec3
                                     // Arg1: LHCb::Vertex
                                     Functors::chain( FC::ToLinAlg, FC::Position, FC::EndVertex, FC::ForwardArg1 ),
                                     // end vertex of particle: LHCb::Particle -> Gaudi::XYZPoint -> vec3
                                     // Arg0: LHCb::Particle
                                     Functors::chain( FC::ToLinAlg, FC::Position, FC::ForwardArg0 ) ) ) ),
                    // get TES object and forward it
                    FC::TES<LHCb::Event::PV::PrimaryVertexContainer>(
                        /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } ),
                    FC::ForwardArgs );

// get the type of the prepared functor that is returned when calling prepare() on the above functor
using allpv_fd_func_type = decltype( allpv_fd_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
// get the result_type of invoking the allpv_fd_func_type with inputs.
// The bool needs to be there since it is a mask to be passed to prepared functor
// Then the actual input, in this instance LHCb::Particle const ref.
using allpv_fd_func_result_type = std::invoke_result_t<allpv_fd_func_type, mask_arg_t, bool, LHCb::Particle const&>;

// Test for ALLPV_IP
//
// create an instance of ALLPV IP functor (obtained from functor.code() in python)
auto allpv_ip_func = Functors::bind( FF::Map( FC::ImpactParameter ),
                                     Functors::chain(
                                         // vertex position: LHCb::Vertex -> Gaudi::XYZPoint -> vec3
                                         FF::Map( Functors::chain( FC::ToLinAlg, FC::Position ) ),
                                         // get TES object and forward it
                                         FC::TES<LHCb::Event::PV::PrimaryVertexContainer>(
                                             /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } ) ),
                                     FC::ForwardArgs );
// get the type of the prepared functor that is returned when calling prepare() on the above functor
using allpv_ip_func_type = decltype( allpv_ip_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
// get the result_type of invoking the allpv_ip_func_type with inputs.
// The bool needs to be there since it is a mask to be passed to prepared functor
// Then the actual input, in this instance LHCb::Particle const ref.
using allpv_ip_func_result_type = std::invoke_result_t<allpv_ip_func_type, mask_arg_t, bool, LHCb::Particle const&>;

auto RELS = ::FC::TES<LHCb::Relation1D<LHCb::Particle, LHCb::Particle>>(
    /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } );
auto SUMCONE = Functors::chain( ::FF::ValueOr( /* The default value. */ 0.0f ), ::FF::Sum,
                                ::FF::Map( /* The functor to map over a range. */ Functors::chain( PT, ::FC::To ) ),
                                Functors::bind( ::FC::Relations, RELS, ::FC::ForwardArgs ) );
// TEST ASYM FUNCTOR
auto asym_func              = ( PT - SUMCONE ) / ( PT + SUMCONE );
using asym_func_type        = decltype( asym_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using asym_func_result_type = std::invoke_result_t<asym_func_type, mask_arg_t, bool, LHCb::Particle const&>;

// create an instances of functors reading RecSummary
auto npvs_func = Functors::chain(
    ::Functors::TES::RecSummaryInfo{ LHCb::RecSummary::nPVs },
    ::FC::TES<LHCb::RecSummary>( /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } ) );
// get the type of the prepared functors reading RecSummary
using npvs_func_type = decltype( npvs_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
// get the result_type of functors reading RecSummary
using npvs_func_res_type = std::invoke_result_t<npvs_func_type, mask_arg_t, bool, LHCb::RecSummary const&>;
using inmuon_1           = std::invoke_result_t<decltype( FPID::InAcceptance ), LHCb::Particle const&>;

// Create an instance for FIND_DECAY functor
auto find_decay_func = Functors::chain( PT, ::Functors::Decay::FindDecay( "[B0 -> K+ pi-]CC" ) );
// get the type of the prepared functor
using find_decay_func_type = decltype( find_decay_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
// get the result_type of invoking the find_decay_func_type with inputs.
// The bool needs to be there since it is a mask to be passed to prepared functor
// Then the actual input is passed forwarding the arguments (LHCb::Particle const ref).
using find_decay_func_result_type = std::invoke_result_t<find_decay_func_type, mask_arg_t, bool, LHCb::Particle const&>;
using find_decay_func_result_type_ptr =
    std::invoke_result_t<find_decay_func_type, mask_arg_t, bool, LHCb::Particle const*>;
using find_decay_func_result_type_non_const =
    std::invoke_result_t<find_decay_func_type, mask_arg_t, bool, LHCb::Particle>;

auto find_mc_decay_func =
    Functors::chain( PT, ::Functors::Decay::FindMCDecay( /* Decay descriptor */ "[B0 => K+ pi-]CC" ) );
using find_mc_decay_func_type = decltype( find_mc_decay_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using find_mc_decay_func_result_type =
    std::invoke_result_t<find_mc_decay_func_type, mask_arg_t, bool, LHCb::MCParticle const&>;
using find_mc_decay_func_result_type_ptr =
    std::invoke_result_t<find_mc_decay_func_type, mask_arg_t, bool, LHCb::MCParticle const*>;
using find_mc_decay_func_result_type_non_const =
    std::invoke_result_t<find_mc_decay_func_type, mask_arg_t, bool, LHCb::MCParticle>;

auto map_find_decay_func = Functors::chain(
    ::FF::Map( /* The functor to map over a range. */ Functors::chain(
        PT, ::Functors::Decay::FindDecay( /* Decay descriptor */ "[B0 => K+ pi-]CC" ) ) ),
    ::FC::TES<LHCb::Particle::ConstVector>( /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } ) );
using map_find_decay_func_type = decltype( map_find_decay_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using map_find_decay_func_result_type =
    std::invoke_result_t<map_find_decay_func_type, mask_arg_t, bool, LHCb::Particle const&>;

auto map_find_decay_range_func = Functors::chain( ::FF::Map( /* The functor to map over a range. */ Functors::chain(
    PT, ::Functors::Decay::FindDecay( /* Decay descriptor */ "[B0 => K+ pi-]CC" ) ) ) );
using map_find_decay_range_func_type =
    decltype( map_find_decay_range_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using map_find_decay_range_func_result_type =
    std::invoke_result_t<map_find_decay_range_func_type, mask_arg_t, bool, LHCb::Particle::Range const&>;

auto map_find_mc_decay_func =
    Functors::chain( ::FF::Map( /* The functor to map over a range. */ Functors::chain(
                         PT, ::Functors::Decay::FindMCDecay( /* Decay descriptor */ "[B0 => K+ pi-]CC" ) ) ),
                     ::FC::TES<LHCb::MCParticle::ConstVector>(
                         /* List of DataHandles */ std::vector{ std::string{ "FakeLocation" } } ) );
using map_find_mc_decay_func_type =
    decltype( map_find_mc_decay_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using map_find_mc_decay_func_result_type =
    std::invoke_result_t<map_find_mc_decay_func_type, mask_arg_t, bool, LHCb::MCParticle const&>;

auto map_find_mc_decay_range_func = Functors::chain( ::FF::Map( /* The functor to map over a range. */ Functors::chain(
    PT, ::Functors::Decay::FindMCDecay( /* Decay descriptor */ "[B0 => K+ pi-]CC" ) ) ) );
using map_find_mc_decay_range_func_type =
    decltype( map_find_mc_decay_range_func.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using map_find_mc_decay_range_func_result_type =
    std::invoke_result_t<map_find_mc_decay_range_func_type, mask_arg_t, bool, LHCb::MCParticle::Range const&>;

auto test_mintree =
    Functors::chain( ::FF::Min, ::FF::Map( /* The functor to map over a range. */ PT ),
                     ::Functors::Filter( /* Predicate to filter the container with. */ ::Functors::AcceptAll ),
                     ::Functors::Adapters::DescendantsFromComposite{} );
using test_mintree_type         = decltype( test_mintree.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using test_mintree_result_type  = std::invoke_result_t<test_mintree_type, mask_arg_t, bool, LHCb::Particle const&>;
using test_mintree_result_type2 = std::invoke_result_t<test_mintree_type, mask_arg_t, bool, LHCb::Particle const*>;

auto const ID     = ::Functors::Simulation::Particle_Id;
auto const MASS   = ::Functors::Composite::Mass;
auto const PIPLUS = ::Functors::Particle::ParticlePropertyUser( /* ID to check */ "pi+" );

auto test_is_id               = Functors::chain( ID, PIPLUS ) == ID;
using test_is_id_type         = decltype( test_is_id.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using test_is_id_result_type  = std::invoke_result_t<test_is_id_type, mask_arg_t, bool, LHCb::Particle const&>;
using test_is_id_result_type2 = std::invoke_result_t<test_is_id_type, mask_arg_t, bool, LHCb::Particle const*>;
using test_is_id_result_type3 = std::invoke_result_t<test_is_id_type, mask_arg_t, bool, LHCb::MCParticle const&>;
using test_is_id_result_type4 = std::invoke_result_t<test_is_id_type, mask_arg_t, bool, LHCb::MCParticle const*>;

auto test_delta_mass              = MASS - Functors::chain( MASS, PIPLUS );
using test_delta_mass_type        = decltype( test_delta_mass.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using test_delta_mass_result_type = std::invoke_result_t<test_delta_mass_type, mask_arg_t, bool, LHCb::Particle const&>;
using test_delta_mass_result_type2 =
    std::invoke_result_t<test_delta_mass_type, mask_arg_t, bool, LHCb::Particle const*>;

auto test_extrapolate_track = FT::Extrapolate( /* z coordinate */ 50 );
using test_extrapolate_track_type =
    decltype( test_extrapolate_track.prepare( EventContext{}, Functors::TopLevelInfo{} ) );
using test_extrapolate_track_result_type  = std::invoke_result_t<test_extrapolate_track_type, v3_track>;
using test_extrapolate_track_result_type2 = std::invoke_result_t<test_extrapolate_track_type, LHCb::Track const*>;
using test_extrapolate_track_result_type3 = std::invoke_result_t<test_extrapolate_track_type, LHCb::Track const&>;
using test_extrapolate_track_result_type4 = std::invoke_result_t<test_extrapolate_track_type, LHCb::Track>;

BOOST_AUTO_TEST_CASE( instantiate_all ) {
  // empty on purpose -- just need to compile and instantiate...
}
