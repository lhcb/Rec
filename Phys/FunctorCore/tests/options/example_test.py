###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import FunctorExampleAlg, FunctorExampleAlg_int
from Configurables import Gaudi__Examples__IntDataProducer as IDP
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Functors import Ex_GreaterThan, Ex_TBL, Ex_TimesTwo
from Gaudi.Configuration import *

# Application setup
app = ApplicationMgr()

types = []

app.TopAlg = [
    FunctorExampleAlg("name0"),  # the default cut should work
    FunctorExampleAlg(
        "name1",
        Cut=Ex_TBL,
        Cuts=[[Ex_TimesTwo, Ex_TBL], [Ex_GreaterThan(2.0) & Ex_GreaterThan(4.0)]],
    ),  # always evaluates to true
    IDP(OutputLocation="SomeInt"),  # needed as input for the next algorithm
    FunctorExampleAlg_int(
        "name2",
        Cut=Ex_GreaterThan(8.0),  # is my input greater than 8?
        Input0="SomeInt",
    ),
    FunctorExampleAlg_int(
        "name3",
        Cut=Ex_GreaterThan(8.0) & Ex_TBL,  # you can build the logical and
        Input0="SomeInt",
    ),
    FunctorExampleAlg_int(
        "name4", Cut=Ex_TimesTwo > 10, Input0="SomeInt"
    ),  # or build predicates like that
]
# - Events
app.EvtMax = 1
app.EvtSel = "NONE"
app.HistogramPersistency = "NONE"
app.ExtSvc.append(MessageSvcSink())
