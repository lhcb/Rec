###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Configurables import EvtStoreSvc
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Functors import SIZE
from Gaudi.Configuration import VERBOSE, ApplicationMgr

# algorithms are coming from PyConf because we need to use DataHandles etc.
from PyConf.Algorithms import FunctorExampleAlg as FEA
from PyConf.Algorithms import Gaudi__Examples__VectorDataProducer as VDP
from PyConf.dataflow import dataflow_config

# user verbose so we can see the DH registration in the output
app = ApplicationMgr(OutputLevel=VERBOSE)
# FEA has counters so we need a sink
app.ExtSvc.append(MessageSvcSink())
# why does the default EventDataSvc not work? good question -> Gaudi#218
whiteboard = EvtStoreSvc("EventDataSvc", EventSlots=1)
app.ExtSvc.append(whiteboard)

vdp = VDP(name="VDP")

fea = FEA(name="FEA", Cut=SIZE(vdp.OutputLocation) < 5)

c = dataflow_config()
c.update(fea.configuration())
algs, _ = c.apply()
app.TopAlg = algs

# - Event
app.EvtMax = 1
app.EvtSel = "NONE"
app.HistogramPersistency = "NONE"
