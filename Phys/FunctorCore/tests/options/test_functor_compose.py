###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import Gaudi__Examples__IntDataProducer as IDP
from Configurables import SimpleFunctorAlg
from Functors import Ex_PlusN, Ex_TimesTwo
from Gaudi.Configuration import VERBOSE, ApplicationMgr

# Application setup
app = ApplicationMgr(OutputLevel=VERBOSE)

types = []

# the functor expressions we want to evaluate
# Input will be an int with value 5

# 2 * (2 * (5+5))
f_exp1 = Ex_TimesTwo @ Ex_TimesTwo @ Ex_PlusN(5)

# 2 * ( 5 + (2*5) )
f_exp2 = Ex_TimesTwo @ Ex_PlusN(5) @ Ex_TimesTwo

app.TopAlg = [
    # needed as input for the next algorithm
    IDP(OutputLocation="SomeInt", Value=5),
    SimpleFunctorAlg(
        "SFA1",
        Functor_Property=f_exp1,
        InputLocation="SomeInt",
        OutputLocation="SFA1_out",
    ),
    SimpleFunctorAlg(
        "SFA2",
        Functor_Property=f_exp2,
        InputLocation="SomeInt",
        OutputLocation="SFA2_out",
    ),
]
# - Events
app.EvtMax = 1
app.EvtSel = "NONE"
app.HistogramPersistency = "NONE"
