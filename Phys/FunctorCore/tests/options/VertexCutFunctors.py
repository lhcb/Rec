#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Functors import *
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf.Algorithms import (
    CombineTracksSIMD__2Body__PrFittedForwardTracks,
    CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs,
)
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    force_location,
)
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.dddb_tag = "master"
options.conddb_tag = "master"
options.geometry_version = "run3/trunk"
options.conditions_version = "master"
options.input_type = "NONE"
options.evt_max = 0
options.simulation = True
config = configure_input(options)

# Basic declaration test
vertex_functors = {"Mass": (MASS > 200.0 * MeV), "RefPt": REFERENCEPOINT_Z > -1000 * mm}
algs = []
for key in vertex_functors:
    algs.append(
        CombineTracksSIMD__2Body__PrFittedForwardTracks(
            name="CombineTracksSIMD__2Body__PrFittedForwardTracks_" + key,
            InputUniqueIDGenerator=force_location(""),
            InputTracks=force_location(""),
            VertexCut=vertex_functors[key],
        )
    )
    algs.append(
        CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs(
            name="CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_" + key,
            InputUniqueIDGenerator=force_location(""),
            InputTracks=force_location(""),
            VertexCut=vertex_functors[key],
        )
    )

config.update(configure(options, CompositeNode("test", algs)))
