/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/FunctorDesc.h"
#include "Functors/FunctorArguments.h"
#include "GaudiKernel/SerializeSTL.h"
#include "Kernel/STLExtensions.h"
#include "fmt/format.h"

namespace {

  auto extract_( std::string_view s ) {
    struct R {
      std::string_view type{};
      std::string_view value{};
      explicit operator bool() const { return !type.empty(); }
    };
    // literals are assumed to be of the form  `some_type{ some_value }`
    // so we assume the last character is _always_ `}`, and then
    // we search backwards for the matching `{` and anything before
    // that must be the type...
    if ( s.size() > 1 && s.back() == '}' ) {
      int c = 0;
      for ( int i = s.size() - 1; i > 0; --i ) {
        if ( s[i] == '}' ) ++c;
        if ( s[i] == '{' ) --c;
        if ( c == 0 ) return R{ s.substr( 0, i ), s.substr( i + 1, s.size() - i - 2 ) };
      }
    }
    return R{};
  }
} // namespace

namespace ThOr {
  /**
   * @brief operator<< specialization for a Functor Description (FuntorDesc)
   *
   * Output should match the python repr result, e.g for the PT Functor:
   * "('::Functors::Track::TransverseMomentum{}', 'PT')"
   *
   * @param o stream to output into
   * @param f FunctorDesc to stream into o
   * @return ostream& filled with the string representation of f
   */
  std::ostream& operator<<( std::ostream& o, FunctorDesc const& f ) {
    o << "\"(" << std::quoted( f.code, '\'' ) << ", ";
    if ( !f.literals.empty() ) {
      GaudiUtils::details::ostream_joiner(
          o << '[', f.literals, ",",
          []( std::ostream& s, auto const& i ) -> decltype( auto ) { return s << std::quoted( i, '\'' ); } )
          << "], ";
    }
    o << std::quoted( f.repr, '\'' ) << ")\"";
    return o;
  }

  std::string substitute_literals( FunctorDesc const& f, std::string_view arg_name ) {
    std::string code = f.code;
    for ( const auto& [i, l] : LHCb::range::enumerate( f.literals ) ) {
      auto j = code.find( l );
      if ( j == code.npos )
        throw GaudiException( fmt::format( "could not find '{}' in '{}'", l, code ), __PRETTY_FUNCTION__,
                              StatusCode::FAILURE );
      auto t = extract_( l );
      if ( !t ) throw GaudiException( fmt::format( "bad literal: '{}'", l ), __PRETTY_FUNCTION__, StatusCode::FAILURE );
      code.replace( j, l.size(), fmt::format( "static_cast<{}>( {}[{}] )", t.type, arg_name, i ) );
    }
    return code;
  }

  FunctorDesc::operator Functors::Arguments() const {
    std::vector<std::string> args;
    args.reserve( literals.size() );
    std::transform( literals.begin(), literals.end(), std::back_inserter( args ), []( std::string const& lit ) {
      auto r = extract_( lit );
      if ( !r ) throw GaudiException( fmt::format( "bad literal: {}", lit ), __PRETTY_FUNCTION__, StatusCode::FAILURE );
      return std::string{ r.value };
    } );
    return Functors::Arguments{ std::move( args ) };
  }

} // namespace ThOr
