/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Cache.h"
#include "fmt/format.h"
#include <string>

/** @file  Cache.cpp
 *  @brief Definitions of cache-related functions.
 */

namespace {
  const std::hash<std::string_view> sv_hash{};
}

namespace Functors::Cache {
  // thin wrapper around std::hash, in case we want to change to a different algorithm...
  HashType makeHash( std::string_view data ) { return sv_hash( data ); }

  // Function used to generate the Gaudi component names from the hashes
  std::string hashToStr( HashType hash ) { return fmt::format( "0x{:08x}", hash ); }

} // namespace Functors::Cache
