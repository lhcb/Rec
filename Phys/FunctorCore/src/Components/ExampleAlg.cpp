/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Example.h"
#include "Functors/Function.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "LHCbAlgs/FilterPredicate.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  template <std::size_t... Is>
  auto create_default_helper( std::index_sequence<Is...> ) {
    return std::make_tuple( std::pair{ "Input" + std::to_string( Is ), "Val" + std::to_string( Is ) }... );
  }

  template <typename... Ins>
  using FilterPredicate = typename LHCb::Algorithm::FilterPredicate<bool( Ins const&... )>;
} // namespace

template <typename... Ins>
struct FunctorExampleAlg;

// no arguments
template <>
struct FunctorExampleAlg<> final : public FilterPredicate<> {
  // This the type-erased wrapper around the functor we use to make the decision
  using Predicate = Functors::Functor<bool()>;

  using FilterPredicate::FilterPredicate;

  bool operator()() const override {
    this->info() << m_functorproxies << endmsg; // try if one can parse vector<vector<functordesc>>
    auto p = m_pred.prepare();
    this->info() << m_functorproxy << endmsg;
    auto result = this->m_pred();
    this->info() << "Result: " << result << endmsg;
    m_cutEff += result;
    return result;
  }

  StatusCode initialize() override {
    return FilterPredicate::initialize().andThen( [&] { decode(); } );
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{ this, "Functor cut efficiency" };

  // all the information to tiate the functor via the factory m_factory
  Gaudi::Property<ThOr::FunctorDesc> m_functorproxy{ this, "Cut", ThOr::Defaults::ALL, [this]( auto& ) {
                                                      if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                                      this->decode();
                                                    } };
  Gaudi::Property<std::vector<std::vector<ThOr::FunctorDesc>>> m_functorproxies{ this, "Cuts", {} };

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  Predicate m_pred;

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{ this, "Factory", "FunctorFactory" };

  void decode() {
    m_factory.retrieve().ignore();
    m_factory->register_functor( this, m_pred, m_functorproxy );
  }
};

// with arguments
template <typename... Ins>
struct FunctorExampleAlg final : public FilterPredicate<Ins...> {
  // This the type-erased wrapper around the functor we use to make the decision
  using Predicate = Functors::Functor<bool( Ins... )>;

  using base = FilterPredicate<Ins...>;

  FunctorExampleAlg( std::string const& name, ISvcLocator* pSvcLocator )
      : base::FilterPredicate( name, pSvcLocator, create_default_helper( std::index_sequence_for<Ins...>{} ) ) {}

  bool operator()( Ins const&... ins ) const override {
    auto p = m_pred.prepare();
    this->info() << m_functorproxy << endmsg;
    auto result = p( ins... );
    this->info() << "Result: " << result << endmsg;
    m_cutEff += result;
    return result;
  }

  StatusCode initialize() override {
    return FilterPredicate<Ins...>::initialize().andThen( [&] { decode(); } );
  }

private:
  // Counter for recording cut retention statistics
  mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{ this, "Functor cut efficiency" };

  // all the information to instantiate the functor via the factory m_factory
  Gaudi::Property<ThOr::FunctorDesc> m_functorproxy{ this, "Cut", ThOr::Defaults::ALL, [this]( auto& ) {
                                                      if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) return;
                                                      this->decode();
                                                    } };

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  Predicate m_pred;

  // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{ this, "Factory", "FunctorFactory" };

  void decode() {
    m_factory.retrieve().ignore();
    m_factory->register_functor( this, m_pred, m_functorproxy );
  }
};
DECLARE_COMPONENT_WITH_ID( FunctorExampleAlg<>, "FunctorExampleAlg" )
DECLARE_COMPONENT_WITH_ID( FunctorExampleAlg<int>, "FunctorExampleAlg_int" )
// using fea_if = FunctorExampleAlg<int,float>;
// using fea_vi = FunctorExampleAlg<std::vector<int>>;
// DECLARE_COMPONENT_WITH_ID( fea_if, "FunctorExampleAlg_int_float" )
// DECLARE_COMPONENT_WITH_ID( fea_vi, "FunctorExampleAlg_vector_int" )

//
//
// stupid simple example with less unrelated template noise
struct SimpleFunctorAlg final : public LHCb::Algorithm::Transformer<int( int const& )> {

  // This the type-erased wrapper around the functor expression we will use to create our output
  using OurFunctor_t = Functors::Functor<int( int const& )>;

  SimpleFunctorAlg( std::string const& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue( "InputLocation", "" ), KeyValue( "OutputLocation", "" ) ) {}

  int operator()( int const& input ) const override {
    auto func = m_pred.prepare();
    // print our functor so we can look how pretty it is :)
    this->info() << m_functorproxy << endmsg;
    // alright here we apply the actual functor to our input int
    auto result = func( input );

    info() << "Result: " << result << endmsg;
    return result;
  }

  StatusCode initialize() override {
    return Transformer::initialize().andThen( [&] { decode(); } );
  }

private:
  // A Gaudi property holding a FunctorDesc which is simply a description the Functor
  // its a struct holding string representations of:
  // the c++ code, headers to include, nice represenation (the way we write it in python)
  Gaudi::Property<ThOr::FunctorDesc> m_functorproxy{ this, "Functor_Property", ThOr::Defaults::ALL,
                                                     [this]( auto& /*unused*/ ) {
                                                       if ( this->FSMState() < Gaudi::StateMachine::INITIALIZED ) {
                                                         return;
                                                       }
                                                       this->decode();
                                                     } };

  // This is the type-erased type (roughly std::function) that contains the just-in-time compiled functor
  // I think this has some overhead, but it may be avoidable...TBC
  OurFunctor_t m_pred;

  // // This is the factory tool that handles the functor cache and/or the just-in-time compilation
  ServiceHandle<Functors::IFactory> m_factory{ this, "Factory", "FunctorFactory" };
  //
  void decode() {
    m_factory.retrieve().ignore();
    // here we use the functor factory to turn our string representation into an actual functor object
    m_factory->register_functor( this, m_pred, m_functorproxy );
  }
};

DECLARE_COMPONENT( SimpleFunctorAlg )
