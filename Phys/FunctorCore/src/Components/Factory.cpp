/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Cache.h"
#include "Functors/FunctorArguments.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/Service.h"
#include <boost/container_hash/hash.hpp>
#include <boost/lexical_cast.hpp>
#include <chrono>
#include <cstdio>
#include <dlfcn.h>
#include <filesystem>
#include <fmt/format.h>
#include <fstream>
#include <set>
#include <vector>

/** @file  Factory.cpp
 *  @brief Implementation and documentation of the FunctorFactory
 *
 *  The FunctorFactory is the service which enables our application to change
 *  functor expressions at configuration time. The following will try to give
 *  an overview of the different pieces that come together to enable the
 *  functionality of the FunctorFactory.
 *
 *  A bit simplified, one can look at the entire functor framework as a system
 *  of 3 layers:
 *  1. Functor configuration in python.
 *  2. Translate the python objects to C++ Functor code.
 *  3. Turn the C++ string representation into compiled and executable C++
 *
 *  We won't talk much more about the general functor implementation and level
 *  1 and 2, for that please refer to the Moore documentation and Moore#284.
 *  The FunctorFactory only comes in at the 3rd level and is the service
 *  which is used by algorithms to turn a string representation into the
 *  final functor object. For that the algorithm registers a functor during
 *  the algorithm's initialize() step, like so:
 *
 *    factory->register_functor( this, m_functor, m_functorproxy );
 *
 *  where m_functor will be filled with a functor object which is created from
 *  the content of the m_functorproxy property of the algorithm.
 *
 *  An algorithms has to register all functors they plan to use within their
 *  initialize() method!
 *
 *  Note that register_functor method does not instantly create/fill the
 *  `m_functor` it only registers the address of `m_functor` and will fill it
 *  in the `start()` call of ther service. (This is well before any algorithm
 *  will call its operator())
 *
 *  This postponed initialization of the functors in the service's start() call
 *  is a design decision that will become more obvious a bit later.
 *
 *  When `start()` is called the FunctorFactory will do the following: (Note if
 *  you've already heard of the FunctorCache, this will be explained at the
 *  end, the following will first focus on the just in time compilation
 *  workflow)
 *
 *  1. Write the C++ representation of functors into a configurable amount
 *     (property: m_split) of temporary c++ files (see write_cpp_files
 *     function). The actual code that we will compile for each functor is
 *     generated in the do_register function. As you can see we wrap the actual
 *     c++ functor code in a bit of Gaudi::PluginService magic, and end up
 *     defining one variable per functor of the following form:
 *
 *       std::unique_ptr<Functors::AnyFunctor> factory_0x50805fa6b888b0a0(){
 *         return std::unique_ptr<Functors::AnyFunctor>{
 *           new Functors::Functor<int()>(::Functors::AcceptAll{})
 *         };
 *       }
 *       Gaudi::PluginService::DeclareFactory<Functors::AnyFunctor,
 *                                            Gaudi::PluginService::Factory<Functors::AnyFunctor*()>>
 *       functor_0x50805fa6b888b0a0{
 *         std::string{"0x50805fa6b888b0a0"},
 *         factory_0x50805fa6b888b0a0
 *       };
 *
 *     This is done to retrieve the functors in an easy way via the
 *     PluginService, see point 3 below. The first argument to the constructor
 *     is a hash that identifies this functor and is calculated based on the
 *     code of the functor(see do_register()). The second argument is a lambda
 *     which when called will return a unique_ptr to the newly created functor
 *     object that we are aiming to create. In Gaudi::PlugingService lingo, this
 *     is the factory function to create the registered component.
 *
 *  2. Invoke the functor_jitter python script (generated in
 *     FunctorCore/CMakeLists.txt) which will compile the c++ files into a
 *     shared library while using a configurable (property: m_jit_n_jobs)
 *     amount of threads. The compiler used by the functor_jitter is the same
 *     compiler + same sets of flags that were used to compile the Rec project
 *     during the build step. Compared to previous solutions that were using
 *     ROOT's cling to compile functors, this has the benefit that the code is
 *     properly optimized, vectorized, and overall indistinguishable from code
 *     that would have been built during the build step (e.g. the
 *     FunctorCache). If you look at the created cpp files from step 1. you
 *     will notice that they do not have any include statements. Instead the
 *     functor_jitter script invokes the compiler with the -include flag which
 *     will include a preprocessed header that was created during the build
 *     step. Check FunctorCore/CMakeLists.txt for more details on how this
 *     header is created. It is important that this header has sufficient
 *     includes to be able to correctly compile any functor that could be
 *     specified at configuration time. To have this preprocessed header was a
 *     design decision that enables us to make the JIT compilation during
 *     runtime independent of the system that we run on and its available
 *     system headers.
 *
 *  3. First we open (dlopen) the shared library we just created. This is where
 *     the previously mentioned Gaudi::PluginService magic comes into play. The
 *     opening of the shared library will call the constructors of the defined
 *     variables, which in turns registers the functors with the global
 *     Gaudi::PluginService. Using the hash of a functor which functions as a
 *     key to the PluginSetvice, we are at this point able to ask the
 *     PluginService to simply create the various functor objects we need and
 *     then bind them to the algorithms.
 *     (This is done at the end of the start() function. If you are wondering
 *     what do_copy does please have a look at the do_copy lambda in
 *     IFactory.h)
 *
 *
 *  Note, the location where the JIT shared library is created is configurable
 *  (property: m_jit_lib_dir) . Before the FunctorFactory starts steps 1-3 from
 *  above, it does check if the library already exists in that location, thus
 *  making reuse of this library possible across jobs. This only works for
 *  exact matches though as the library name is defined as a hash of the
 *  dependencies (functor_jitter+preprocessed header) and the content of the
 *  cpp files.
 *
 *
 *  The above explains how the FunctorFactory works in JIT mode, but we can
 *  also create a FunctorCache. This is a way to pre-compile the functors used
 *  in a certain configuration during the Project's build step. For an example,
 *  look at Rec/Phys/FunctorCache/CMakeLists.txt Given a certain options file
 *  we want to create a cache for, the procedure is similar to the older LoKi
 *  style functor caches. During the build step we start a gaudi job with
 *  specific options for the FunctorFactory such that we only create the cpp
 *  files and don't start any JIT compilation.
 *
 *  The generated cpp files are expected by cmake which takes care of
 *  configuring and building a shared library based on those files which we
 *  commonly only refer to as functorcache.
 *
 *  If using the cache is enabled, the do_register function will first check
 *  via the Gaudi::PluginService if the required functor is already in a
 *  functorcache and only registers the functor for JIT if it couldn't be
 *  found. This first lookup only considers proper functor caches configured
 *  via cmake, not previously built libraries via JIT
 *
 *
 *  Note on data dependencies:
 *  A functor can have data dependencies by inheriting from DataDepWrapper
 *  (Functors/Utilities.h) When a functor is created and then bound to an
 *  algorithm, it will automatically create the required DataHandles which it
 *  will need to have access to the TES objects in its operator(). This
 *  creation automatically registers the DataHandles with the algorithm that
 *  owns the functor.
 *
 *  The Problem with the above procedure is that, the creation and bind of
 *  functors and thus the registration of DataHandles doesn't happen until the
 *  `start()` call of the FunctorFactory. But data dependencies are resolved
 *  and checked by the HiveDataBroker during its initialize step. So at the
 *  time the HiveDataBroker interrogates an algorithm to determine its required
 *  inputs and outputs, the DataHandles of functors haven't yet been registered
 *  with the algorithm.
 *
 *  The solution to this problem has two steps:
 *  1. During configuration (PyConf), the data dependencies of functors are
 *     recognized and used to populate the owning algorithm's ExtraInputs
 *     property. This means the HiveDataBroker will get the correct "reply"
 *     when checking an algorithm's inputs and outputs.
 *  2. During the creation of a Functor's DataHandles, we check the owning
 *     algorithms ExtraInputs property to assert that this property already
 *     contains the TES location we are currently creating a DataHandle for.
 *     This check should be sufficient to catch any misconfiguration and throw
 *     meaningful error messages.
 *
 *
 *
 *  Notes on dlopen arguments: We use: RTLD_LOCAL | RTLD_LAZY | RTLD_DEEPBIND
 *  for a detailed explanation as to what these mean please check the linux
 *  documentation. The reasoning for the first two is easy: This showed
 *  slightly better performance and ensures that the C++ symbols in the functor
 *  libary can not have any impact on the global application. RTLD_DEEPBIND is
 *  important and best explained by example:
 *  1. A large functor cache is already built.
 *  2. Changes to a functor are made in Rec, e.g. let's say we change the
 *     operator of the X_COORDINATE.
 *  3. You launch a job with THOR_DISABLE_CACHE thinking everything should be
 *     fine because your functors are getting compiled with the changes from 2.
 *     included. But, if any other algorithm, loads a functor that is inside
 *     the functor cache of 1. (think e.g. Loki TrackFunctors loaded by proto
 *     particle algorithms) then the entire functor cache library will be
 *     loaded. And at the point where the new functor library from 3. gets opened
 *     with dlopen() some of the symbols that are part of our new libary, for
 *     example the operator of the X_COORDINATE functor, are already loaded in the
 *     global namespace, so the symbols that are already loaded have priority.
 *     RTLD_DEEPBIND avoids this problem by giving local symbols priority when
 *     opening a library.
 *
 *  **extra note** see below, we disable DEEPBIND if we are inside a address
 *  sanitizer build because of: Rec#391 &&
 *  https://github.com/google/sanitizers/issues/611
 */

namespace {

#if defined( __GNUC__ )
#  if defined( __SANITIZE_ADDRESS__ )
#    pragma GCC warning                                                                                                \
        "FunctorCache dlopen with DEEPBIND is disabled in sanitizer build. Please see Factory.cpp for more details. "
  constexpr auto dlopen_flags = RTLD_LOCAL | RTLD_LAZY;
#  else
  constexpr auto dlopen_flags = RTLD_LOCAL | RTLD_LAZY | RTLD_DEEPBIND;
#  endif
#elif defined( __clang__ )
#  if __has_feature( address_sanitizer )
#    pragma GCC warning                                                                                                \
        "FunctorCache dlopen with DEEPBIND is disabled in sanitizer build. Please see Factory.cpp for more details. "
  constexpr auto dlopen_flags = RTLD_LOCAL | RTLD_LAZY;
#  else
  constexpr auto dlopen_flags = RTLD_LOCAL | RTLD_LAZY | RTLD_DEEPBIND;
#  endif
#else
  constexpr auto dlopen_flags = RTLD_LOCAL | RTLD_LAZY | RTLD_DEEPBIND;
#endif

  /**
   * @brief helper function for LibHandle
   *
   * @param handle pointer to library on which we call dlclose
   */
  void lib_closer( void* handle ) {
    if ( handle != nullptr ) { dlclose( handle ); }
  };
  /**
   * unique_ptr specialization to hold the void* returned by dlopen which will
   * take care of calling dlclose() on the library upon destruction. Most
   * important for the scenario where we potentially reinitialize many times
   * and thus maybe create and open many functor libraries.
   */
  using LibHandle = std::unique_ptr<void, decltype( &lib_closer )>;

  std::ofstream open_stream( std::string filename ) {
    auto out = std::ofstream{ filename };
    if ( !out.is_open() ) {
      throw GaudiException{ "Failed to open file " + filename, "FunctorFactory", StatusCode::FAILURE };
    }
    return out;
  }

  std::string join_strs( std::vector<std::string> const& vs ) {
    auto s = std::stringstream{};
    GaudiUtils::details::ostream_joiner( s, vs, ", " );
    return s.str();
  }

} // namespace

// support for Gaudi::Property<std::filesystem::path> which can not be
// implemented yet in Gaudi because cling will crash due to
// https://github.com/root-project/root/issues/9670
namespace Gaudi::Parsers {
  StatusCode parse( std::filesystem::path& result, const std::string& input ) {
    result = std::filesystem::path{ input };
    return StatusCode::SUCCESS;
  }
} // namespace Gaudi::Parsers

/** @class FunctorFactory
 *
 *  This service does all the heavy lifting behind compiling functors into
 *  type-erased Functor<Out(In)> objects. It can do this either via on demand
 *  invoking the same compiler the project was compiled with, or by using a
 *  pre-compiled functor cache.
 */
struct FunctorFactory : public extends<Service, Functors::IFactory> {
  using extends::extends;

  void do_register( std::function<void( std::unique_ptr<Functors::AnyFunctor> )> do_copy, std::string_view functor_type,
                    ThOr::FunctorDesc const& desc, Gaudi::Algorithm const* owner ) override {
    // If we are already in RUNNING state we won't go through start() thus a
    // do_register call doesn't make sense as the library won't be compiled and
    // the Functor won't get resolved. See GaudiKernel/StateMachine.h for info
    // on possible states and transitions.
    if ( FSMState() == Gaudi::StateMachine::RUNNING ) {
      throw GaudiException{ "do_register needs to be called before start()", "FunctorFactory", StatusCode::FAILURE };
    }

    auto func_body =
        "return std::make_unique<" + std::string( functor_type ) + ">(" + substitute_literals( desc, "args" ) + ")";

    // Now we can calculate the hash
    auto const hash     = Functors::Cache::makeHash( func_body );
    auto const hash_str = Functors::Cache::hashToStr( hash );

    // Check via the PluginService if the functor is already present in a
    // prebuilt functor cache, if not disabled.
    if ( !m_disable_cache ) {
      auto cached_functor =
          ::Gaudi::PluginService::Factory<Functors::AnyFunctor*( Functors::Arguments const& )>::create( hash_str,
                                                                                                        desc );
      if ( cached_functor ) {
        do_copy( std::move( cached_functor ) );
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Functor: " << desc.repr << " with hash: " << hash_str << " found in cache." << endmsg;
        }
        // because we don't JIT this functor, the FunctorFactory doesn't need to
        // keep track of any info for this functor. We have already resolved it
        // completely so we can return early
        return;
      }
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "Functor: " << desc.repr << " with hash: " << hash_str << " registered for JIT." << endmsg;
    }
    // fmt::format turns {{ -> {
    constexpr std::string_view code_format = R"_(

::Gaudi::PluginService::DeclareFactory<Functors::AnyFunctor, ::Gaudi::PluginService::Factory<Functors::AnyFunctor*(Functors::Arguments const&)>>
functor_{0}{{
  std::string{{"{0}"}},
  []( [[maybe_unused]] Functors::Arguments const& args ) -> std::unique_ptr<Functors::AnyFunctor> {{
      {1};
  }}
}};
)_";

    auto cpp_code       = fmt::format( code_format, hash_str, func_body );
    auto owner_typename = owner->type() + '/' + owner->name();
    // representation to use when printing to message streams
    auto msg_repr = fmt::format( "{} registered by {}\n    Python repr: {}\n    CPP code: {}\n    literals: {}",
                                 hash_str, owner_typename, desc.repr, desc.code, join_strs( desc.literals ) );

    // See if we already JIT compiled this functor and can therefore reuse
    // the factory function that we got before
    auto iter = m_all.find( hash );
    if ( iter != m_all.end() ) {
      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Found already registered functor: " << desc.repr << endmsg; }

      if ( iter->second.cpp_code != cpp_code ) {
        error() << "Hash collision for hash: " << hash_str << " Try purging your functor cache" << endmsg;
        error() << "Code 1 " << iter->second.cpp_code << endmsg;
        error() << "Code 2 " << cpp_code << endmsg;
        throw GaudiException{ "do_register encountered a hash collision", "FunctorFactory", StatusCode::FAILURE };
      }
    } else {
      iter = m_all.emplace( hash, FunctorsWithSameCode{ cpp_code, {} } ).first;
    }
    iter->second.setters.push_back( FunctorSetter{ do_copy, msg_repr, desc, owner_typename } );
  }

  /**
   * @brief split functors in m_all into N cpp files and write those.
   *
   * Split strategy is controled by the value of m_split.
   * m_split < 0 -> split such that each file contains abs(m_split)
   * m_split > 0 -> split into m_split files, always creates m_split files even if fewer functors are registered.
   * m_split == 0 -> is mapped to 1 in the property update handler
   *
   * If splitting leads to uneven division, remaining functors are spread over the existing files.
   * Used pattern to name files: "FUNCTORS_FunctorFactory_{:04d}.cpp""
   *
   * FIXME what negative value is the sweet spot for JIT?
   *
   * @param dest_dir Destination directory files are written to.
   */
  void write_cpp_files( std::filesystem::path const& dest_dir, bool include_header ) const {
    auto const cpp_filename = dest_dir / "FUNCTORS_FunctorFactory_{:04d}.cpp";

    auto split = m_split > 0 ? m_split.value() : m_all.size() / std::abs( m_split );

    info() << "Writing cpp files for " << m_all.size() << " functors split in " << split << " files" << endmsg;

    std::vector<std::filesystem::path> files( split );
    for ( std::size_t i{ 0 }; i < split; ++i ) {
      files[i] = fmt::format( fmt::runtime( cpp_filename.string() ), i + 1 );
    }

    // integer division will give us the amount of functors per file.
    auto const functors_per_file = m_all.size() / split;
    // the remainder tells us how many files will get 1 extra functor.
    auto const remainder = m_all.size() % split;
    // NOTE if we have less functors than required by split (m_all.size() <
    // split) we still write all N=split files as the functor cache code in
    // cmake expects these files to be created

    // NOTE
    // m_all is a map indexed with the hash, which guarantees that the
    // iteration order is stable and does not depend on the order of
    // `register_functor` calls.
    auto functors_iter = m_all.begin();

    for ( std::size_t file_idx{ 0 }; file_idx < files.size(); ++file_idx ) {
      auto out = open_stream( files[file_idx] );
      auto log = open_stream( files[file_idx].replace_extension( ".log" ) );

      if ( include_header ) {
        // when building the functor cache we include the non-preprocessed header
        out << "#include \"Functors/JIT_includes.h\"\n";
      }
      out << "namespace {\n";
      std::size_t written_functors{ 0 };
      while ( functors_iter != m_all.end() && ++written_functors <= ( functors_per_file + ( file_idx < remainder ) ) ) {
        auto const& functors = functors_iter++->second;
        out << functors.cpp_code << '\n';

        // Write the owners of the functors in a .log file. We don't write them to the .cpp
        // because that would reduce the number of ccache hits.
        log << "===================================================\n";
        log << functors.cpp_code << '\n';
        for ( auto const& setter : functors.setters ) { log << "registered from " << setter.owner << '\n'; }
      }
      out << "\n}\n";
    }
  }

  StatusCode initialize() override {
    auto sc = Service::initialize();

    // we will fill this buffer with the content of functor_jitter and the
    // preprocessed header to then calcuate a hash and write that value into
    // m_header_hash which will be used to define a JIT library's filename
    std::stringstream buffer;

    // first we need to find the "functor_jitter" script on the runtime_path.
    // That env var looks like "path1:path2:path3...."
    auto const runtime_path         = System::getEnv( "PATH" );
    auto       begin                = 0U;
    auto       end                  = runtime_path.find( ':' );
    auto       found_functor_jitter = std::filesystem::path{};

    while ( end != runtime_path.npos ) {
      auto path = std::filesystem::path{ runtime_path.substr( begin, end - begin ) };
      path.append( "functor_jitter" );
      if ( std::filesystem::is_regular_file( path ) ) {
        found_functor_jitter = path;
        break;
      }
      // move new begin to just after the ":"
      begin = end + 1;
      end   = runtime_path.find( ':', begin );
    }
    // if we didn't find anything and break out of the while loop, we need to
    // check the last element
    if ( end == runtime_path.npos ) {
      auto path = std::filesystem::path{ runtime_path.substr( begin, end - begin ) };
      path.append( "/functor_jitter" );
      if ( std::filesystem::is_regular_file( path ) ) { found_functor_jitter = path; }
    }

    if ( found_functor_jitter.empty() ) {
      error() << "Could not find 'functor_jitter' executable on runtime path!" << endmsg;
      return StatusCode::FAILURE;
    }
    debug() << "Calculating hash of: " << found_functor_jitter << endmsg;
    buffer << std::ifstream{ found_functor_jitter }.rdbuf();

    // runtime environment variable which points to the preprocessed header
    auto const path_to_header = System::getEnv( "FUNCTORFACTORY_PREPROCESSED_HEADER" );
    if ( path_to_header == "UNKNOWN" ) {
      error() << "Could not retrieve path to preprocessed header from env var: FUNCTORFACTORY_PREPROCESSED_HEADER"
              << endmsg;
      return StatusCode::FAILURE;
    }

    buffer << std::ifstream{ path_to_header }.rdbuf();
    m_dependencies_hash = Functors::Cache::hashToStr( Functors::Cache::makeHash( buffer.str() ) );
    debug() << "Calculating hash of: " << path_to_header << endmsg;
    debug() << "FunctorFactory initialized with dependencies hash: " << m_dependencies_hash << endmsg;
    return sc;
  }

  StatusCode start() override {
    auto sc = Service::start();
    if ( m_all.empty() ) { return sc; }

    if ( msgLevel( MSG::DEBUG ) ) { debug() << m_all.size() << " functors were registered" << endmsg; }

    if ( m_disable_cache && m_disable_jit ) {
      // this combination only makes sense for creating a functor cache.
      write_cpp_files( m_jit_lib_dir, true );
      return sc;
    }

    if ( m_disable_cache ) {
      // if we don't want to use functors from the cache we need to manually
      // "disable" the ones that are already registered in Gaudi's PluginService
      // as the new DeclareFactory calls that get executed on the dlopen below,
      // won't override existing entries in the PluginService :(
      for ( auto const& entry : m_all ) {
        ::Gaudi::PluginService::Details::Registry::instance().erase( Functors::Cache::hashToStr( entry.first ) );
      }
    }

    if ( m_disable_jit ) {
      // m_all is not empty and contains only functors that are not in the cache (see do_register).
      // If we get here it makes no sense to proceed because compiled functors are not available.
      fatal()
          << "DisableJIT (THOR_DISABLE_JIT env var) is true but the following functors are not in the functor cache."
          << endmsg;
      for ( auto const& entry : m_all ) {
        for ( auto const& setter : entry.second.setters ) {
          error() << "    missing functor " << setter.msg_repr << endmsg;
        }
      }
      sc = StatusCode::FAILURE;
      return sc;
    }

    std::string full_lib_code;
    // m_all is a map indexed with the hash, which guarantees that the
    // iteration order is stable and does not depend on the order of
    // `register_functor` calls.
    for ( auto const& entry : m_all ) { full_lib_code += entry.second.cpp_code; }

    auto const content_hash  = Functors::Cache::hashToStr( Functors::Cache::makeHash( full_lib_code ) );
    auto const file_prefix   = "FunctorJitLib_" + m_dependencies_hash + "_" + content_hash;
    auto const lib_filename  = file_prefix + ".so";
    auto const lib_full_path = m_jit_lib_dir / lib_filename;
    // declare the variable in outer scope to be able to cleanup at the end.
    std::filesystem::path tmp_dir;

    // We first check if we have already compiled this library in a previous
    // execution of the same job by looking for full path to the lib inside the
    // FunctorJitLibDir (e.g. /tmp/libname.so)
    // dlopen arguments are explained at the top of this file
    if ( m_lib_handle = LibHandle{ dlopen( lib_full_path.c_str(), dlopen_flags ), lib_closer };
         m_lib_handle != nullptr ) {
      info() << "Reusing functor library: " << lib_full_path << endmsg;
    } else {
      // In a multi job scenario we want to avoid many jobs directly writing to
      // the same file and potentially causing problems. Thus, in JIT mode we
      // will create all files in a subdirectory that is unique for each
      // process and only at the end move the created library one directory up
      // into m_jit_lib_dir. If two jobs create the same lib and try to move at
      // the same time we are still save because the rename/move operation is
      // atomic in the sense that a dlopen call will either load the already
      // existing file or the newly renamed one. But it can't fail due to e.g.
      // the file not having been fully overwritten or similar weird things.
      tmp_dir = m_jit_lib_dir / ( file_prefix + "_" + std::to_string( getpid() ) );
      std::filesystem::remove_all( tmp_dir );
      std::filesystem::create_directory( tmp_dir );

      info() << "New functor library will be created: " << lib_full_path << endmsg;
      if ( msgLevel( MSG::DEBUG ) ) { debug() << "Based on generated C++ files in folder: " << tmp_dir << endmsg; }

      write_cpp_files( tmp_dir, false );

      info() << "Compilation will use " << m_jit_n_jobs.value() << " jobs." << endmsg;

      // functor_jitter is a shell script generated by cmake to invoke the
      // correct compiler with the correct flags see:
      // Phys/FunctorCore/CMakeLists.txt
      auto cmd = "functor_jitter " + std::to_string( m_jit_n_jobs ) + " " + tmp_dir + " " + lib_filename;

      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "Command that will be executed:\n" << cmd << endmsg; }

      auto start_time  = std::chrono::high_resolution_clock::now();
      auto return_code = std::system( cmd.c_str() );
      auto total_time =
          std::chrono::duration_cast<std::chrono::seconds>( std::chrono::high_resolution_clock::now() - start_time );
      if ( return_code != 0 ) {
        throw GaudiException{ "JIT compilation of functors failed!", "FunctorFactory", StatusCode::FAILURE };
      }
      info() << "Compilation of functor library took " << total_time.count() << " seconds" << endmsg;

      auto const lib_tmp_full_path = tmp_dir / lib_filename;
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << "Rename " << lib_tmp_full_path << " to " << lib_full_path << endmsg;
      }
      std::filesystem::rename( lib_tmp_full_path, lib_full_path );

      // dlopen arguments are explained at the top of this file
      m_lib_handle = LibHandle{ dlopen( lib_full_path.c_str(), dlopen_flags ), lib_closer };
    }

    if ( m_lib_handle == nullptr ) {
      throw GaudiException{ std::string( "dlopen Error:\n" ) + dlerror(), "FunctorFactory", StatusCode::FAILURE };
    }

    // at this point we have compiled the functors so now it is time to make
    // sure that we initialize each algorithm's functors
    for ( auto const& entry : m_all ) {
      for ( auto const& setter : entry.second.setters ) {
        // registration of the functors with the PluginService happens
        // automatically when the library is opened, thus we can now simply go
        // via the PluginService to get our functors
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Requesting functor with hash " << Functors::Cache::hashToStr( entry.first ) << " and literals "
                    << setter.literals << " from PluginService" << endmsg;
        }
        setter.do_copy( ::Gaudi::PluginService::Factory<Functors::AnyFunctor*( Functors::Arguments const& )>::create(
            Functors::Cache::hashToStr( entry.first ), setter.literals ) );
      }
    }

    // if we reach this point then everything probably went well. Thus if we
    // aren't running in DEBUG or more verbose, let's cleanup our temporary
    // files. if tmp_dir is empty we just loaded an existing lib and don't
    // need to cleanup.
    if ( !m_keep_temp && !tmp_dir.empty() ) { std::filesystem::remove_all( tmp_dir ); }
    return sc;
  }

private:
  struct FunctorSetter {
    std::function<void( std::unique_ptr<Functors::AnyFunctor> )> do_copy;
    std::string                                                  msg_repr;
    Functors::Arguments                                          literals;
    std::string                                                  owner;
  };
  struct FunctorsWithSameCode {
    std::string                cpp_code;
    std::vector<FunctorSetter> setters;
  };
  std::map<Functors::Cache::HashType, FunctorsWithSameCode> m_all;

  Gaudi::Property<bool> m_disable_jit{ this, "DisableJIT", System::getEnv( "THOR_DISABLE_JIT" ) != "UNKNOWN",
                                       "Do not lookup nor create a functor library in JitLibDir." };
  Gaudi::Property<bool> m_disable_cache{ this, "DisableCache", System::getEnv( "THOR_DISABLE_CACHE" ) != "UNKNOWN",
                                         "Do not use functors from the functor cache." };
  Gaudi::Property<std::filesystem::path> m_jit_lib_dir{ this, "JitLibDir", System::getEnv( "THOR_JIT_LIBDIR" ),
                                                        [this]( auto& /*unused*/ ) {
                                                          m_jit_lib_dir = ( ( m_jit_lib_dir.value() == "UNKNOWN" )
                                                                                ? std::filesystem::temp_directory_path()
                                                                                : m_jit_lib_dir.value() );
                                                        },
                                                        Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };
  Gaudi::Property<bool> m_keep_temp{ this, "JitKeepTemp", System::getEnv( "THOR_JIT_KEEP_TEMP" ) != "UNKNOWN",
                                     "Keep temporary files in JitLibDir" };

  // a value of -1 means we will use as all threads available. Note that this
  // property is connected to m_split. Thus, the actual amount of threads
  // started are m_split up a maxiumum of to m_jit_n_jobs threads.
  Gaudi::Property<int> m_jit_n_jobs{
      this, "JITNJobs", 1,
      [this]( auto& /*unused*/ ) {
        if ( int tmp{}; boost::conversion::try_lexical_convert( System::getEnv( "THOR_JIT_N_JOBS" ), tmp ) ) {
          m_jit_n_jobs = tmp;
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };
  // meaning of m_split is explained in write_cpp_files()
  // It can be set via either THOR_JIT_N_SPLITS or LOKI_GENERATE_CPPCODE.
  // Setting the loki env var also enables the cpp dump for the loki factories,
  // so we introduce THOR_JIT_N_SPLITS to be able to steer JIT splitting
  // without changing the behaviour of the loki factories. We still check for
  // the loki variable first though as that's the one that is set during the
  // functor cache creation.
  Gaudi::Property<int> m_split{ this, "Split", 1,
                                [this]( auto& /*unused*/ ) {
                                  using boost::conversion::try_lexical_convert;
                                  if ( int tmp{};
                                       try_lexical_convert( System::getEnv( "LOKI_GENERATE_CPPCODE" ), tmp ) ||
                                       try_lexical_convert( System::getEnv( "THOR_JIT_N_SPLITS" ), tmp ) ) {
                                    m_split = tmp;
                                  }
                                  // map 0 -> 1
                                  m_split = m_split == 0 ? 1 : m_split.value();
                                },
                                Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };

  // Gaudi::Property<std::filesystem::path>        tmp{this, "tmp", std::filesystem::path{"ab/cd"}};
  std::string m_dependencies_hash{};
  LibHandle   m_lib_handle{ nullptr, lib_closer };
};

DECLARE_COMPONENT( FunctorFactory )
