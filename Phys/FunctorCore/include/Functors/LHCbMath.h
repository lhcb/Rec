/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Functors/Adapters.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbMath/ParticleParams.h"
#include "LHCbMath/ValueWithError.h"

#include <functional>
#include <type_traits>
#include <utility>

#include "Math/VectorUtil.h"

/*
 * Functors that access information of Gaudi::Math
 */
namespace Functors::LHCbMath {

  /**
   * @brief scalarMomentum, access the scalar momentum by input.scalarMomentum()
   */
  constexpr auto scalarMomentum = GenericFunctor{
      "scalarMomentum", []( auto const& i ) -> decltype( i.scalarMomentum() ) { return i.scalarMomentum(); } };

  /**
   * @brief normed_dot_3dim, calculates the cosine of angle between two 3-vectors. If passed vectors are 4-vectors, the
   * functor will use only coordinate part.
   */
  constexpr auto normed_dot_3dim = TrivialFunctor{ "normed_dot_3dim", []( const auto& v1, const auto& v2 ) {
                                                    const auto dot = []( const auto& i, const auto& j ) {
                                                      return i.x() * j.x() + i.y() * j.y() + i.z() * j.z();
                                                    };
                                                    return dot( v1, v2 ) / sqrt( dot( v1, v1 ) * dot( v2, v2 ) );
                                                  } };

  /**
   * @brief boost_to, boost the first input LHCb::LinAlg::Vec<4> to the frame, where the second input
   * LHCb::LinAlg::Vec<4> rests
   */
  constexpr auto boost_to = TrivialFunctor{
      "boost_to", []( const auto& input, const auto& ref ) requires requires { LHCb::LinAlg::convert( input );
  LHCb::LinAlg::convert( ref );
} // namespace Functors::LHCbMath
{
  const auto result =
      ( ROOT::Math::VectorUtil::boost( LHCb::LinAlg::convert( input ), LHCb::LinAlg::convert( ref ).BoostToCM() ) );
  return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( result.X(), result.Y(), result.Z(), result.E() );
}
}
;

/**
 * @brief boost_from, boost the first input LHCb::LinAlg::Vec<4> from the frame, where the second input
 * LHCb::LinAlg::Vec<4> rests, to the lab frame
 */
constexpr auto boost_from = TrivialFunctor{
    "boost_from", []( const auto& input, const auto& ref ) requires requires { LHCb::LinAlg::convert( input );
LHCb::LinAlg::convert( ref );
}
{
  const auto result =
      ( ROOT::Math::VectorUtil::boost( LHCb::LinAlg::convert( input ), -LHCb::LinAlg::convert( ref ).BoostToCM() ) );
  return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( result.X(), result.Y(), result.Z(), result.E() );
}
}
;

/**
 * @brief mass, access the scalar momentum by input.invariantMass()
 */
constexpr auto invariantMass = GenericFunctor{
    "invariantMass", []( auto const& input ) -> decltype( input.invariantMass() ) { return input.invariantMass(); } };

namespace ValueWithError {
  /**
   * @brief Value, access the value of Gaudi::Math::ValueWithError
   */
  constexpr auto Value = TrivialFunctor{ "ValueWithError::Value", &Gaudi::Math::ValueWithError::value };

  /**
   * @brief Error, access the value of Gaudi::Math::ValueWithError
   */
  constexpr auto Error = TrivialFunctor{ "ValueWithError::Error", &Gaudi::Math::ValueWithError::error };

} // namespace ValueWithError

namespace ParticleParams {
  /**
   * @brief flightDistance, access the decay length of Gaudi::Math::ParticleParams
   */
  constexpr auto flightDistance =
      TrivialFunctor{ "ParticleParams::flightDistance", &Gaudi::Math::ParticleParams::flightDistance };

  /**
   * @brief ctau, access the c*tau of Gaudi::Math::ParticleParams
   */
  constexpr auto ctau = TrivialFunctor{ "ParticleParams::ctau", &Gaudi::Math::ParticleParams::ctau };

  /**
   * @brief lenPosCov, access the "Matrix" with correlation errors between position and decay length from
   * Gaudi::Math::ParticleParams
   */
  constexpr auto lenPosCov = TrivialFunctor{ "ParticleParams::lenPosCov", &Gaudi::Math::ParticleParams::lenPosCov };

  /**
   * @brief lenMomCov, access the "Matrix" with correlation errors between momentum and decay length from
   * Gaudi::Math::ParticleParams
   */
  constexpr auto lenMomCov = TrivialFunctor{ "ParticleParams::lenMomCov", &Gaudi::Math::ParticleParams::lenMomCov };

} // namespace ParticleParams

} // namespace Functors::LHCbMath
