/*****************************************************************************\
* (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ParticleUtils.h"
#include "Functors/Decay.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Relations/Relation1D.h"
#include "SelKernel/ParticleCombination.h"

/** @file  Adapters.h
 *  @brief Functors that adapt object-wise functors (e.g. PT) into
 *         collection-wise functors (e.g. scalar-sum-of-PT in CombinationCut)
 */
namespace Functors::detail {
  // throw exception whenever the object is not a composite of type LHCb::Particle
  inline void checkComposite( LHCb::Particle const& part ) {
    if ( part.isBasicParticle() ) {
      throw GaudiException{ "Functor applied to basic particle", "Functors::GenerationFromComposite",
                            StatusCode::FAILURE };
    }
  }
  template <FunctorFunction Transform, typename Reduce>
  class TransformReduce : public Function {
  protected:
    Transform m_transform;
    Reduce    m_reduce;

  public:
    /** Flag that this adapter expects to be invoked with an explicit mask as
     *  its first argument - iff the embedded Transform does so
     */
    constexpr static bool requires_explicit_mask = requires_explicit_mask_v<Transform>;

    constexpr TransformReduce( Transform transform, Reduce reduce )
        : m_transform{ std::move( transform ) }, m_reduce{ std::move( reduce ) } {}

    void bind( TopLevelInfo& top_level ) { detail::bind( m_transform, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [f = detail::prepared( m_transform, evtCtx, top_level ), r = m_reduce]( mask_arg_t, auto const& mask,
                                                                                       auto const& collection ) {
          using LHCb::Event::decayProducts;
          return Sel::transform_reduce( decayProducts( collection ), LHCb::cxx::bind_front( f, mask_arg, mask ), r );
        };
      } else {
        return [f = detail::prepared( m_transform, evtCtx, top_level ), r = m_reduce]( auto const& collection ) {
          using LHCb::Event::decayProducts;
          return Sel::transform_reduce( decayProducts( collection ), f, r );
        };
      }
    }
  };

  constexpr auto min_fn = []( auto const& a, auto const& b ) {
    using std::min;
    return min( a, b );
  };
  constexpr auto max_fn = []( auto const& a, auto const& b ) {
    using std::max;
    return max( a, b );
  };
} // namespace Functors::detail

namespace Functors::Adapters {
  template <typename F>
  struct Accumulate : detail::TransformReduce<F, std::plus<>> {
    constexpr Accumulate( F f ) : detail::TransformReduce<F, std::plus<>>{ std::move( f ), std::plus<>{} } {}
    auto name() const {
      return "Accumulate( " + detail::get_name( detail::TransformReduce<F, std::plus<>>::m_transform ) + " )";
    }
  };

  template <typename F>
  struct Minimum : detail::TransformReduce<F, decltype( detail::min_fn )> {
    constexpr Minimum( F f )
        : detail::TransformReduce<F, decltype( detail::min_fn )>{ std::move( f ), detail::min_fn } {}
    auto name() const {
      return "Minimum( " + detail::get_name( detail::TransformReduce<F, decltype( detail::min_fn )>::m_transform ) +
             " )";
    }
  };

  template <typename F>
  struct Maximum : detail::TransformReduce<F, decltype( detail::max_fn )> {
    constexpr Maximum( F f )
        : detail::TransformReduce<F, decltype( detail::max_fn )>{ std::move( f ), detail::max_fn } {}
    auto name() const {
      return "Maximum( " + detail::get_name( detail::TransformReduce<F, decltype( detail::max_fn )>::m_transform ) +
             " )";
    }
  };

  class Child : public Function {
    int m_idx{ 0 };

  public:
    constexpr Child( int idx ) : m_idx( idx ) {}

    auto name() const { return "Child( " + std::to_string( m_idx ) + " )"; }

    template <typename Combination>
    decltype( auto ) operator()( Combination const& combination ) const {
      using LHCb::Event::decayProducts;
      assert( 0 < m_idx &&
              static_cast<std::size_t>( m_idx ) <= decayProducts( Sel::Utils::deref_if_ptr( combination ) ).size() );
      return decayProducts( Sel::Utils::deref_if_ptr( combination ) )[m_idx - 1];
    }
  };

  template <std::size_t... idxs>
  struct SubCombination_t : Function {
    static_assert( ( ( idxs > 0 ) && ... ) );
    static_assert( sizeof...( idxs ) > 1 );
    auto name() const { return "SubCombination< " + ( ( std::to_string( idxs ) + ", " ) + ... ) + " >"; }

    template <typename Combination>
    auto operator()( Combination const& combination ) const {
      using LHCb::Event::decayProducts;
      using LHCb::Event::subCombination;
      return subCombination<( idxs - 1 )...>( decayProducts( combination ) );
    }
  };

  template <std::size_t... idxs>
  constexpr auto SubCombination = SubCombination_t<idxs...>{};

  /** @class BasicsFromComposite
   *  @brief Adapter that retrieves the basic particles from a composite one
   *  If the functor is applied to a basic particle, it will throw an exception
   */
  struct BasicsFromComposite : Function {

    /* Improve error messages. */
    constexpr auto name() const { return "BasicsFromComposite"; }

    auto operator()( LHCb::Particle const& p ) const {
      using LHCb::Utils::Particle::walk;
      detail::checkComposite( p );
      std::vector<const LHCb::Particle*> out;
      for ( const auto* b : walk( p ) )
        if ( b->isBasicParticle() ) out.push_back( b );
      return out;
    }

    auto operator()( const LHCb::Particle* p ) const { return ( *this )( *p ); }
  };

  /** @class DescendantsFromComposite
   *  @brief Adapter that retrieves all the descendants from a composite particle
   *  If the functor is applied to a basic particle, it will throw an exception
   */
  struct DescendantsFromComposite : Function {

    /* Improve error messages. */
    constexpr auto name() const { return "DescendantsFromComposite"; }

    // template <std::indirectly_readable T>
    template <typename T>
    auto operator()( const T& p ) const -> decltype( ( *this )( *p ) ) {
      return ( *this )( *p );
    }

    auto operator()( const LHCb::MCParticle& mcp ) const {
      std::vector<const LHCb::MCParticle*> children{};

      auto fill = [&children]( auto const& p, auto& callable ) -> void {
        // retrieve daughters from MCVertex
        if ( auto goodvtx = p.goodEndVertex(); goodvtx ) {
          for ( auto& pi : goodvtx->products() ) {
            if ( !pi ) continue;
            children.push_back( pi );
            callable( *pi, callable );
          }
        }
      };
      fill( mcp, fill );
      return children;
    }

    auto operator()( LHCb::Particle const& p ) const {
      detail::checkComposite( p );
      std::vector<LHCb::Particle const*> children{};
      using LHCb::Utils::Particle::walk;
      // note: walk (also) includes `p` as first element -- so we explicitly have to skip it
      for ( const auto* c : walk( p ) )
        if ( c != &p ) children.push_back( c );
      return children;
    }
  };

  /** @class GenerationFromComposite
   *  @brief Adapter that retrieves all the descendants from a composite particle
   *   belonging to a given generation
   *   If the functor is applied to a basic particle, it will throw an exception
   */
  struct GenerationFromComposite : Function {

    GenerationFromComposite( int generation ) : m_generation{ generation } { assert( m_generation > 0 ); }

    // template <std::indirectly_readable T>
    template <typename T>
    auto operator()( const T& p ) const -> decltype( ( *this )( *p ) ) {
      return ( *this )( *p );
    }

    auto operator()( LHCb::MCParticle const& mcp ) const {
      std::vector<const LHCb::MCParticle*> children;
      auto                                 fill = [&children]( auto& p, int gen, auto& callable ) -> void {
        if ( gen == 0 ) {
          children.push_back( &p );
        } else {
          if ( auto goodvtx = p.goodEndVertex(); goodvtx ) {
            for ( auto& pi : goodvtx->products() ) {
              if ( pi ) callable( *pi, gen - 1, callable );
            }
          }
        }
      };
      fill( mcp, m_generation, fill );
      return children;
    }

    auto operator()( const LHCb::Particle& p ) const {
      detail::checkComposite( p );
      std::vector<const LHCb::Particle*> children;
      auto fill = [&children]( LHCb::Particle const& p, int gen, auto& callable ) -> void {
        if ( gen == 0 ) {
          children.push_back( &p );
        } else {
          for ( auto const& pi : p.daughtersVector() )
            if ( pi ) callable( *pi, gen - 1, callable );
        }
      };
      fill( p, m_generation, fill );
      return children;
    }

  private:
    int m_generation;
  };

  /** @class ConvertToPOD
   *  @brief Simple adapter that calls .cast() on its argument.
   *  This is useful when outputing the result of a calculation that uses a
   *  some kind of numeric wrapper (e.g. SIMDWrapper, but it doesn't have to
   *  be) to some destination that only understands plain types (e.g. a ROOT
   *  TTree). Clearly this only makes sense if it has been arranged elsewhere
   *  that the functor return value is conceptually a scalar arithmetic value.
   */
  constexpr auto ConvertToPOD =
      TrivialFunctor{ "ConvertToPOD", []( auto d ) { return LHCb::Utils::as_arithmetic( d ); } };

} // namespace Functors::Adapters
