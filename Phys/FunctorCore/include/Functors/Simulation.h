/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/FunctorDefaults.h"
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Functors/Adapters.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/MCTaggingHelper.h"
#include "Kernel/ParticleProperty.h"
#include "MCInterfaces/IMCReconstructible.h"
#include <functional>
#include <type_traits>
#include <utility>

namespace Functors::detail {
  inline bool check_mask( int value, int mask ) { return ( value & mask ) == mask; };

} // namespace Functors::detail
namespace Functors::Simulation {

  /**
   * @brief Check against one bitmask
   */
  struct CheckMask : public Function {
    static constexpr auto name() { return "CheckMask"; }

    CheckMask( int mask ) : m_mask( mask ) {}

    int operator()( int const value ) const {
      using Functors::detail::check_mask;
      return check_mask( value, m_mask );
    }

  private:
    int m_mask;
  };

  /**
   * @brief Get the particle ID object of a particle
   */
  constexpr auto Particle_Id_Obj =
      TrivialFunctor{ "Particle_Id_Obj", []( auto const& p ) -> decltype( p.particleID() ) { return p.particleID(); },
                      []( auto const& p ) -> decltype( p->particleID() ) { return p->particleID(); } };

  /** @brief Particle ID of LHCb::Particle or LHCb::MCParticle.
   * Can be used together with F.MAP_INPUT and MC association relations table to obtain the TRUEID*/
  constexpr auto Particle_Id = TrivialFunctor{ "Particle_Id", []( auto const& p ) {
                                                using LHCb::Event::pid;
                                                return pid( p );
                                              } };

  /**
   * @brief return true if either LHCb::Particle or LHCb::MCParticle is hadron*/
  constexpr auto IsHadron = TrivialPredicate{ "IsHadron", &LHCb::ParticleID::isHadron };

  /**
   * @brief return true if either LHCb::Particle or LHCb::MCParticle is meson*/
  constexpr auto IsMeson = TrivialPredicate{ "IsMeson", &LHCb::ParticleID::isMeson };

  /**
   * @brief return true if either LHCb::Particle or LHCb::MCParticle is baryon*/
  constexpr auto IsBaryon = TrivialPredicate{ "IsBaryon", &LHCb::ParticleID::isBaryon };

  /**
   * @brief return true if either LHCb::Particle or LHCb::MCParticle is lepton*/
  constexpr auto IsLepton = TrivialPredicate{ "IsLepton", &LHCb::ParticleID::isLepton };

  /**
   * @brief Returns if particle contains a given quark*/
  struct HasQuark : public Predicate {

    HasQuark( std::string_view quark ) {
      using namespace std::string_view_literals;
      static const auto map = std::map{
          std::pair{ "u"sv, &LHCb::ParticleID::hasUp },           std::pair{ "d"sv, &LHCb::ParticleID::hasDown },
          std::pair{ "c"sv, &LHCb::ParticleID::hasCharm },        std::pair{ "s"sv, &LHCb::ParticleID::hasStrange },
          std::pair{ "t"sv, &LHCb::ParticleID::hasTop },          std::pair{ "b"sv, &LHCb::ParticleID::hasBottom },
          std::pair{ "b'"sv, &LHCb::ParticleID::hasBottomPrime }, std::pair{ "t'"sv, &LHCb::ParticleID::hasTopPrime } };
      auto it = map.find( quark );
      if ( it == map.end() ) {
        // throw an error if the quark is not recognized
        throw GaudiException{ fmt::format( "HasQuark argument '{}' not recognized", quark ), __PRETTY_FUNCTION__,
                              StatusCode::FAILURE };
      }
      m_fun = it->second;
    }

    template <typename Particle>
    bool operator()( Particle const& p ) const
      requires requires( Particle const& p, bool ( LHCb::ParticleID::*f )() const ) {
        ( Sel::Utils::deref_if_ptr( p ).particleID().*f )();
      }
    {
      return ( Sel::Utils::deref_if_ptr( p ).particleID().*m_fun )();
    }

  private:
    bool ( LHCb::ParticleID::*m_fun )() const = nullptr;
  };

  /** @brief General Category class that implements the main "operator()" function
   * that simply returns the input data of "int" type, asserting that it is different
   * to the pre-defined invalid value (e.g for basic particle).
   * The background category is actually determined, with
   * algorithm "MCTruthAndBkgCatAlg" (living in "Phys" package).
   *
   * @see Functors::Simulation::Category
   */
  constexpr auto Category = TrivialFunctor{ "Category", []( int const i ) { return i; } };

  namespace MC {
    /**
     * @brief Property, this functor will return the "int" type MCProperty for
     * an MC Particle, which contains the bitwise information.
     */

    struct Property_t : public Function {

      constexpr auto name() const { return "MC::Property"; }

      auto operator()( const LHCb::MCProperty& mc_track_info, const LHCb::MCParticle& mc_particle ) const {
        return ( mc_particle.particleID().threeCharge() != 0 )
                   ? Functors::Optional<int>{ mc_track_info.property( &mc_particle ) }
                   : std::nullopt;
      }

      // template <typename T1, std::indirectly_readable T2>
      template <typename T1, typename T2>
      auto operator()( const T1& p1, const T2& p2 ) const -> decltype( ( *this )( p1, *p2 ) ) {
        return ( *this )( p1, *p2 );
      }

      // template <std::indirectly_readable T1, typename T2>
      template <typename T1, typename T2>
      auto operator()( const T1& p1, const T2& p2 ) const -> decltype( ( *this )( *p1, p2 ) ) {
        return ( *this )( *p1, p2 );
      }
    };
    constexpr auto Property = Property_t{};

    /**
     * @brief ChargeReconstructible, return the reconstructible category
     * for charge MC Particle.
     *
     * The charge reconstructible categories:
     *
     *    -1  = No MC classification possible
     *     0  = Outside detector acceptance
     *     1  = In acceptance but not reconstructible
     *     2  = Reconstructible as a Long charged track
     *     3  = Reconstructible as a Downstream charged track
     *     4  = Reconstructible as an Upstream charged track
     *     5  = Reconstructible as a T charged track
     *     6  = Reconstructible as a VELO charged track
     *
     */
    constexpr auto ChargeReconstructible =
        TrivialFunctor{ "MC::ChargeReconstructible", []( int property ) {
                         using Functors::detail::check_mask;
                         auto rec = IMCReconstructible::RecCategory::NoClassification;
                         if ( property ) {
                           /// Acceptance
                           auto inAcc = ( check_mask( property, MCTrackInfo::flagMasks::maskAccT ) ||
                                          check_mask( property, MCTrackInfo::flagMasks::maskAccUT ) ||
                                          check_mask( property, MCTrackInfo::flagMasks::maskAccVelo ) );

                           /// Category
                           if ( inAcc ) {
                             if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVeloAndT ) )
                               rec = IMCReconstructible::RecCategory::ChargedLong;
                             else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVelo ) &&
                                       check_mask( property, MCTrackInfo::flagMasks::maskHasUT ) )
                               rec = IMCReconstructible::RecCategory::ChargedUpstream;
                             else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasT ) &&
                                       check_mask( property, MCTrackInfo::flagMasks::maskHasUT ) )
                               rec = IMCReconstructible::RecCategory::ChargedDownstream;
                             else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasVelo ) )
                               rec = IMCReconstructible::RecCategory::ChargedVelo;
                             else if ( check_mask( property, MCTrackInfo::flagMasks::maskHasT ) )
                               rec = IMCReconstructible::RecCategory::ChargedTtrack;
                           } else
                             rec = IMCReconstructible::RecCategory::OutsideAcceptance;
                         }
                         return rec;
                       } };

    /**
     * @brief functor accessing the parent MCParticle
     * @note The grandparent or ancestors can be accessed by chainning more Parent functors.
     */
    constexpr auto Mother = GenericFunctor{
        "MC::Mother",
        []( const LHCb::MCParticle& mcp ) {
          auto mother = mcp.mother();
          return mother ? Functors::Optional{ mother } : std::nullopt; // FIXME: optional pointer is a semantic stutter
        },
        []( const LHCb::MCVertex& mcv ) {
          auto mother = mcv.mother();
          return mother ? Functors::Optional{ mother } : std::nullopt; // FIXME: optional pointer is a semantic stutter
        } };

    /**
     * @brief functor accessing the origin vertex of a MCParticle
     * @note this functor does currently the same as Track::ReferencePoint as the referencePoint
     * of a MCParticle is its originVertex
     */
    constexpr auto OriginVertex =
        Functors::GenericFunctor{ "MCParticle::originVertex", &LHCb::MCParticle::originVertex };

    /**
     * @brief functor accessing the primary vertex of a MCParticle
     */
    constexpr auto PrimaryVertex =
        Functors::GenericFunctor{ "MCParticle::primaryVertex", &LHCb::MCParticle::primaryVertex };

    /**
     * @brief functor accessing the true tagging information of a MCParticle
     */
    constexpr auto OriginFlag =
        TrivialFunctor{ "MC::OriginFlag", []( const LHCb::MCParticle* tMC, const LHCb::MCParticle* bMC ) {
                         return LHCb::FlavourTagging::originType( *bMC, *tMC );
                       } };

    /**
     * @brief This functor return the first longlived ancestor of a MCParticle
     */
    struct FirstLongLivedAncestor : public Function {
      constexpr auto name() const { return "MC::FirstLongLivedAncestor"; }

      // 10^-15 seconds is between the lifetimes of the pi0 (considered prompt) and the tau (nonprompt).
      FirstLongLivedAncestor( const double minLifetime = 1e-7 * Gaudi::Units::ns ) : m_minLifetime( minLifetime ) {}

      void bind( TopLevelInfo& top_level ) {
        m_ppSvc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
      }

      auto operator()( const LHCb::MCParticle& mcp ) const {
        if ( !m_ppSvc ) {
          throw GaudiException{ "Can not initialize the LHCb::ParticlePropertySvc.",
                                "Functors::Simulation::FirstLongLivedAncestor", StatusCode::FAILURE };
        }

        const LHCb::MCParticle* parent = mcp.mother();
        while ( parent ) {
          auto pProp = m_ppSvc.value()->find( parent->particleID() );
          if ( pProp && pProp->lifetime() > m_minLifetime ) break;
          parent = parent->mother();
        }
        return parent ? Functors::Optional{ parent }
                      : std::nullopt; // FIXME: wrapping a pointer in an optional is a semantic stutter
      }

      auto operator()( const LHCb::MCParticle* p ) const { return Functors::and_then( p, *this ); }

    private:
      const double                                             m_minLifetime;
      std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> m_ppSvc;
    };

    /**
     * @brief functor accessing the lifetime of a MCParticle
     */
    constexpr auto LifeTime = GenericFunctor{
        "MC::LifeTime", []( const LHCb::MCParticle& mcp ) -> Functors::Optional<double> {
          const auto* end_vertex    = mcp.goodEndVertex();
          const auto* origin_vertex = mcp.originVertex();
          if ( !end_vertex || !origin_vertex ) return std::nullopt;
          const auto trueDist = end_vertex->position() - origin_vertex->position();
          const auto trueP    = mcp.momentum();
          return ( trueP.M() * trueDist.Dot( trueP.Vect() ) / trueP.Vect().mag2() ) / Gaudi::Units::c_light;
        } };

  } // namespace MC

  namespace MCHeader {
    /**
     * @brief get all primary vertices from the MCHeader
     */
    constexpr auto AllPVs = Functors::TrivialFunctor{ "MCHeader::AllPVs", &LHCb::MCHeader::primaryVertices };

    /**
     * @brief get the event time of the MCHeader
     */
    constexpr auto EvtTime = Functors::TrivialFunctor{ "MCHeader::EvtTime", &LHCb::MCHeader::evtTime };

    /**
     * @brief get the event number of the MCHeader
     */
    constexpr auto EvtNumber = Functors::TrivialFunctor{ "MCHeader::EvtNumber", &LHCb::MCHeader::evtNumber };

    /**
     * @brief get the run number of the MCHeader
     */
    constexpr auto RunNumber = Functors::TrivialFunctor{ "MCHeader::RunNumber", &LHCb::MCHeader::runNumber };
  } // namespace MCHeader

  namespace MCVertex {
    /**
     * @brief get the time of a MCVertex.
     */
    constexpr auto Time = Functors::GenericFunctor{ "MCVertex::Time", &LHCb::MCVertex::time };

    /**
     * @brief get the type of a MCVertex.
     */
    constexpr auto Type = Functors::GenericFunctor{ "MCVertex::Type", &LHCb::MCVertex::type };

    /**
     * @brief returns true if the MCVertex is a primary vertex.
     */
    constexpr auto IsPrimary = Functors::GenericFunctor{ "MCVertex::IsPrimary", &LHCb::MCVertex::isPrimary };

    /**
     * @brief returns true if the MCVertex is a decay vertex.
     */
    constexpr auto IsDecay = Functors::GenericFunctor{ "MCVertex::IsDecay", &LHCb::MCVertex::isDecay };

    /**
     * @brief functor accessing the decay products of a MCVertex
     */
    constexpr auto Products = Functors::GenericFunctor{ "MCVertex::Products", &LHCb::MCVertex::products };

  } // namespace MCVertex

} // namespace Functors::Simulation
