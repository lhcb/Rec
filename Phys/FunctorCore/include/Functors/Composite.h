/*****************************************************************************\
 * (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/FlavourTag.h"
#include "Event/State.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Functors/TrackLike.h"

#include <string>
#include <type_traits>

/** @file  Composite.h
 *  @brief Definitions of functors for composite-particle-like objects.
 */

/** @namespace Functors::Composite
 *
 *  Functors that make sense for composite particles (i.e. ones that have a
 *  vertex as well as a trajectory)
 */
namespace Functors::detail {
  template <typename FlavourTags, typename Data>
  std::optional<LHCb::Tagger> findMatchingFlavourTagger( FlavourTags const& ftags, Data const& d,
                                                         LHCb::Tagger::TaggerType const& inputtype ) {
    for ( auto const& ft : ftags ) { // loop over flavourtags in event (one per B candidate)
      const LHCb::Particle* bCand = ft->taggedB();
      if ( bCand->momentum() == d.momentum() &&
           bCand->referencePoint() == d.referencePoint() ) { // B matching check they're the same object
        for ( auto const& t : ft->taggers() ) {
          if ( t.type() == inputtype ) { return t; }
        }
      }
    }
    return {};
  }
} // namespace Functors::detail

namespace Functors::Composite {

  /**MTDOCACHI2**/
  template <int N>
  struct MotherTrajectoryDistanceOfClosestApproachChi2_t : public Function {
    static_assert( N >= 1, "Indices start from 1 for LoKi compatibility." );

    void bind( TopLevelInfo& top_level ) { m_dist_calc.emplace( top_level.algorithm() ); }

    template <typename VContainer, typename Particle>
    auto operator()( VContainer const& vertices, Particle const& composite ) const {
      auto const bestPV = Sel::getBestPV( composite, vertices );

      using LHCb::Event::decayProducts;
      auto const child_state = Sel::stateVectorFromComposite( decayProducts( composite )[N - 1] );

      using LHCb::Event::posCovMatrix;
      using LHCb::Event::referencePoint;
      using LHCb::Event::threeMomCovMatrix;
      using LHCb::Event::threeMomentum;
      using LHCb::Event::threeMomPosCovMatrix;

      auto const& composite_state = Sel::stateVectorComputations(
          endVertexPos( *bestPV ), threeMomentum( composite ), threeMomCovMatrix( composite ), posCovMatrix( *bestPV ),
          threeMomPosCovMatrix( composite ) );

      return m_dist_calc->stateDOCAChi2( composite_state, child_state );
    }

  private:
    std::optional<Functors::detail::DefaultDistanceCalculator_t> m_dist_calc;
  };

  template <int N>
  const auto MotherTrajectoryDistanceOfClosestApproachChi2 = MotherTrajectoryDistanceOfClosestApproachChi2_t<N>{};

  /** @brief Flight distance chi2 to the given vertex.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  // FIXME: making this a generic functor leads to an unexpected optional wrapper in the MVA functors???
  constexpr auto FlightDistanceChi2ToVertex =
      TrivialFunctor{ "FlightDistanceChi2ToVertex", []( auto const& vertex, auto const& composite ) {
                       auto const& comp = Sel::Utils::deref_if_ptr( composite );
                       return Sel::Utils::flightDistanceChi2( comp, vertex );
                     } };

  // First a helper for the momentum perpendicular to the flight vector
  template <typename Vertex_t, typename Composite>
  auto perpendicularMomentum( Vertex_t const& vtx, Composite const& comp ) {
    using LHCb::Event::endVertexPos;
    using LHCb::Event::threeMomentum;
    auto const d    = endVertexPos( comp ) - endVertexPos( vtx );
    auto const mom  = threeMomentum( comp );
    auto const perp = mom - d * ( dot( mom, d ) / d.mag2() );
    return perp.mag();
  }

  /** @brief Calculate the corrected mass for the given PV. */

  class CorrectedMass : public Function {
    float m_invisible_mass2 = 0;

  public:
    constexpr CorrectedMass( float invisible_mass = 0.0 ) : m_invisible_mass2( invisible_mass * invisible_mass ){};

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vtx, Composite const& composite ) const {

      using LHCb::Event::mass2;
      using std::sqrt;

      // Get the pT variable that we need
      auto const pt = perpendicularMomentum( vtx, composite );

      // Calculate the corrected mass
      return sqrt( mass2( composite ) + pt * pt ) +
             ( m_invisible_mass2 == 0 ? pt : sqrt( pt * pt + m_invisible_mass2 ) );
    }
  };

  /** @brief Calculate the corrected mass error for a given PV. */
  struct CorrectedMassError : public Function {
    static constexpr auto name() { return "CorrectedMassError"; }

    constexpr CorrectedMassError( float invisible_mass = 0.0 ) : m_invisible_mass2( invisible_mass * invisible_mass ){};

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertices, Composite const& composite ) const {

      using LHCb::Event::endVertexPos;
      using LHCb::Event::fourMomentum;
      using LHCb::Event::mass2;
      using LHCb::Event::momCovMatrix;
      using LHCb::Event::momPosCovMatrix;
      using LHCb::Event::posCovMatrix;
      using LHCb::Event::threeMomentum;
      using std::sqrt;

      auto const mom     = threeMomentum( composite );
      auto const fourmom = fourMomentum( composite );
      auto const sv      = endVertexPos( composite );
      auto const pv      = endVertexPos( vertices );
      auto const flight  = sv - pv;
      auto const pt      = perpendicularMomentum( vertices, composite );
      auto const A       = dot( mom, flight );
      auto const B       = flight.mag2();

      auto const posCov    = posCovMatrix( composite );
      auto const covFourP  = momCovMatrix( composite );
      auto const momPosCov = momPosCovMatrix( composite );

      auto const invSqrtMassPT        = 1. / sqrt( mass2( composite ) + pt * pt );
      auto const invSqrtMassMissingPT = 1. / sqrt( m_invisible_mass2 + pt * pt );
      auto const dMcorrdPT            = ( 0.5 * invSqrtMassPT * 2 * pt ) + ( 0.5 * invSqrtMassMissingPT * 2 * pt );

      // First calculate some derivatives
      auto const dMcd_SV = dMcorrdPT * ( 1 / pt ) * -0.5 * ( 2 * A * B * mom - A * A * 2 * flight ) / ( B * B );
      auto const dMcd_PV = dMcorrdPT * ( 1 / pt ) * -0.5 * ( -2 * A * B * mom + A * A * 2 * flight ) / ( B * B );
      auto const dMcdp   = -1 * invSqrtMassPT * A / B * flight + 1 / pt * ( mom - A / B * flight );
      auto const dMcdE   = invSqrtMassPT * E( fourmom );

      LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4> dMcdP4;
      dMcdP4( 0 ) = LHCb::Utils::as_arithmetic( dMcdp.X() );
      dMcdP4( 1 ) = LHCb::Utils::as_arithmetic( dMcdp.Y() );
      dMcdP4( 2 ) = LHCb::Utils::as_arithmetic( dMcdp.Z() );
      dMcdP4( 3 ) = LHCb::Utils::as_arithmetic( dMcdE );
      // -- the errors on the vertices
      auto const errsqVertex = similarity( dMcd_SV + dMcd_PV, posCov );

      auto const part1 = momPosCov.transpose() * dMcdP4;
      auto const part2 = dot( dMcd_SV, part1 );

      auto const errsqMom = similarity( dMcdP4, covFourP ) + 2 * part2;

      return sqrt( errsqVertex + errsqMom );
    }

  private:
    float m_invisible_mass2;
  };

  /** @brief Calculate the composite mass using the given child mass hypotheses. */
  struct MassWithHypotheses : public Function {
    /** Create a mass functor with a list of mass hypotheses that can be a mix
     *  of floating point values (in MeV) and names of particles. Particle
     *  names are translated into mass values using the ParticlePropertySvc.
     */

    template <typename Tuple>
    MassWithHypotheses( Tuple const& mass_inputs ) {
      m_inputs.reserve( std::tuple_size_v<Tuple> );
      std::apply(
          [&]( auto const&... i ) {
            // TODO: static_assert that std::decay_t<decltype(i)> is amongs the variant types...
            // TODO: somehow map double->float, and drop double... (use Gaudi::overload?)
            ( m_inputs.emplace_back( std::in_place_type<std::decay_t<decltype( i )>>, i ), ... );
          },
          mass_inputs );
    }

    template <typename CombinationType>
    auto operator()( CombinationType const& comb ) const {
      // Calculate the mass from the child 3-momenta and the given
      // mass hypotheses. Start by checking we have the correct number.
      using LHCb::Event::decayProducts;
      auto children    = decayProducts( comb );
      auto NumChildren = children.size();
      if ( m_masses.size() != m_inputs.size() ) {
        throw GaudiException{ "Functor has not been bound", __PRETTY_FUNCTION__, StatusCode::FAILURE };
      }
      if ( m_masses.size() != NumChildren ) {
        throw GaudiException{ "Mismatch between number of mass values given (" + std::to_string( m_masses.size() ) +
                                  ") and the number of children in the given object (" + std::to_string( NumChildren ) +
                                  ")",
                              __PRETTY_FUNCTION__, StatusCode::FAILURE };
      }
      using LHCb::Event::threeMomentum;
      using std::sqrt;
      using float_t = decltype( threeMomentum( children[0] ).mag2() );
      float_t E{ 0 };
      using three_t = decltype( threeMomentum( children[0] ) );
      three_t mom{ 0, 0, 0 };
      for ( const auto& [i, child] : LHCb::range::enumerate( children ) ) {
        E += sqrt( threeMomentum( child ).mag2() + m_masses[i] * m_masses[i] );
        mom = mom + threeMomentum( child );
      }
      return sqrt( E * E - mom.mag2() );
    }

    void bind( TopLevelInfo& top_level ) {
      // Avoid setting up the service if we don't need it (e.g. all hypotheses)
      // were specified numerically)
      std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> pp_svc{ std::nullopt };
      for ( auto const& i : m_inputs ) {
        std::visit( Gaudi::overload( [&]( float v ) { m_masses.push_back( v ); },
                                     [&]( double v ) { m_masses.push_back( static_cast<float>( v ) ); },
                                     [&]( std::string const& s ) {
                                       if ( !pp_svc )
                                         pp_svc.emplace( top_level.algorithm(), top_level.generate_property_name(),
                                                         "LHCb::ParticlePropertySvc" );
                                       auto const* pp = pp_svc.value()->find( s );
                                       if ( !pp ) {
                                         throw GaudiException{ "Couldn't get ParticleProperty for particle '" + s + "'",
                                                               "Functors::Composite::Mass::bind()",
                                                               StatusCode::FAILURE };
                                       }
                                       m_masses.push_back( pp->mass() );
                                     } ),
                    i );
      }
    }

    std::vector<float>                                    m_masses{};
    std::vector<std::variant<float, double, std::string>> m_inputs{};
  };

  /** @brief Return the input object's mass as defined by an accessor. */
  constexpr auto Mass = TrivialFunctor{ "Mass", []( auto const& particle ) {
                                         using LHCb::Event::mass2;
                                         using std::sqrt;
                                         return sqrt( mass2( particle ) );
                                       } };

  /** @brief Calculate the lifetime of the particle with respect to the vertex. */
  struct Lifetime_t : public Function {
    static constexpr auto name() { return "Lifetime"; }

    void bind( TopLevelInfo& top_level ) { m_lifetime_calc.bind( top_level.algorithm() ); }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertex, Composite const& composite ) const {
      return std::get<0>(
          m_lifetime_calc.Lifetime( Sel::Utils::deref_if_ptr( vertex ), Sel::Utils::deref_if_ptr( composite ) ) );
    }

  private:
    Functors::detail::DefaultLifetimeFitter_t m_lifetime_calc;
  };
  inline const auto Lifetime = Lifetime_t{};

  /** @brief Calculate the lifetime of the particle with respect to the vertex. */
  struct ComputeDecayLengthSignificance_t : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "ComputeDecayLengthSignificance"; }

    void bind( TopLevelInfo& top_level ) { m_lifetime_calc.bind( top_level.algorithm() ); }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertex, Composite const& composite ) const {
      auto const& comp = Sel::Utils::deref_if_ptr( composite );
      auto const& vtx  = Sel::Utils::deref_if_ptr( vertex );
      return m_lifetime_calc.DecayLengthSignificance( vtx, comp );
    }

  private:
    Functors::detail::DefaultLifetimeFitter_t m_lifetime_calc;
  };
  inline const auto ComputeDecayLengthSignificance = ComputeDecayLengthSignificance_t{};

  /** @brief Get the tagging decision from specific FlavourTag tagger. */
  class TaggingDecision : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingDecision"; }

    /** Constructor */
    TaggingDecision( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingDecision( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().decision() : LHCb::FlavourTag{}.decision();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  /** @brief Get the tagging mistag rate from specific FlavourTag tagger. */
  class TaggingMistag : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingMistag"; }

    /** Constructor */
    TaggingMistag( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingMistag( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().omega() : LHCb::FlavourTag{}.omega();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  /** @brief Get the tagging MVA output from specific FlavourTag tagger. */
  class TaggingMVAOutput : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingMVAOutput"; }

    /** Constructor */
    TaggingMVAOutput( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingMVAOutput( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().mvaValue() : LHCb::Tagger{}.mvaValue();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  namespace BTracking {

    namespace detail {
      template <typename Composite2TrackRelations, typename Composite>
      auto getTrack( Composite2TrackRelations const& relations, Composite const& composite ) {
        auto const& comp        = Sel::Utils::deref_if_ptr( composite );
        auto        track_range = relations.relations( &comp );
        return track_range.empty() ? std::nullopt : Functors::Optional{ track_range.front().to() };
      }
    } // namespace detail

    /** @brief Get heavy flavour track associated to composite. */
    constexpr auto Track = TrivialFunctor{
        "BTracking::Track",
        []( auto const& relations, auto const& composite ) -> decltype( detail::getTrack( relations, composite ) ) {
          return detail::getTrack( relations, composite );
        } };

    /** @brief Calculate the corrected mass using heavy flavour tracking info. */
    constexpr auto CorrectedMass = TrivialFunctor{
        "BTracking::CorrectedMass",
        []( auto const& relations, auto const& composite ) requires requires { detail::getTrack( relations, composite );
  } // namespace BTracking
  {
    using FType = SIMDWrapper::scalar::float_v;
    using Sel::Utils::mass2;
    using Sel::Utils::threeMomentum;

    auto track = detail::getTrack( relations, composite );
    if ( !track ) return FType{ 0 };

    // direction using first hit (or end vertex if not available, as saved in heavy flavour track)
    auto const state = track.value()->firstState();
    auto const d     = LHCb::LinAlg::Vec<FType, 3>{ state.tx(), state.ty(), 1. };

    // Get the pT variable that we need
    auto const mom  = threeMomentum( composite );
    auto const perp = mom - d * ( dot( mom, d ) / d.mag2() );
    auto const pt   = perp.mag();

    // Calculate the corrected mass
    return pt + sqrt( mass2( composite ) + pt * pt );
  }
}; // namespace Functors::Composite
} // namespace BTracking

} // namespace Functors::Composite
