/***************************************************************************** \
* (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration     *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/System.h"
#include "Relations/RelationWeighted1D.h"
#include "SelKernel/VertexRelation.h"

#include <array>
#include <tuple>

namespace Functors::Common {

  /**
   * @brief Functor to invoke call operator of input
   *
   * This functor is meant to e.g. access individual matrix values. Thus, we
   * only support integer values for now to not make this functor too complex.
   */
  class Call final : public Function {
    std::array<int, 2> m_indices;

  public:
    constexpr Call( int i, int j ) : m_indices{ i, j } {}
    constexpr Call( std::tuple<int, int> ij ) : m_indices{ std::get<0>( ij ), std::get<1>( ij ) } {}
    template <typename Data>
    auto operator()( Data const& d ) const {
      return Sel::Utils::deref_if_ptr( d )( m_indices[0], m_indices[1] );
    }
  };

  namespace details {
    template <typename T>
    decltype( auto ) wrap_if_ref( T&& in ) {
      if constexpr ( std::is_lvalue_reference_v<T> ) {
        return std::ref( in );
      } else {
        return std::forward<T>( in );
      }
    }
  } // namespace details

  /**
   * @brief Functor to forward input
   *
   * Used in composition, to forward the top_level input to underlying functor
   *
   * take care of lifetime issues. We want to forward if it is a reference but copy if it is a rvalue.
   * That's because forwarding a rvalue would likely yield to dangling references.
   * See e.g. the add_fwd functor in TestFunctors.cpp. The result of the PlusN{5} functor is forwarded to AddInputs,
   * but the forwarded result would go out of scope before AddInputs would be called.
   *
   * To achieve the wanted semantics we use reference wrapper and tuple. Note
   * that a tuple automatically decays the reference wrapper ->
   * make_typle(reference_wrapper<T0> ref, T1 val) -> tuple<T0&, T1>(ref, val).
   *
   * That's super useful because the downstream functor codebase already has
   * many type checks that aren't written to handle e.g.
   * reference_wrapper<LHCb::Particle>
   */

  constexpr auto ForwardArgs =
      TrivialFunctor{ "ForwardArgs", []( auto&&... d ) {
                       return std::make_tuple( details::wrap_if_ref( std::forward<decltype( d )>( d ) )... );
                     } };

  // even in the single argument case we use make tuple to decay the reference
  // wrapper a different option would be to have 2 overloads, (T0&&, ...)
  // returing by value and (T0&, ...) returning T&
  constexpr auto ForwardArg0 = TrivialFunctor{
      "ForwardArgs<0>",
      []<typename T>( T&& d, auto... ) {
        return std::make_tuple( details::wrap_if_ref( std::forward<T>( d ) ) );
      } // namespace Functors::Common
  };

  constexpr auto ForwardArg1 = TrivialFunctor{ "ForwardArgs<1>", []<typename T>( auto, T&& d, auto... ) {
                                                return std::make_tuple( details::wrap_if_ref( std::forward<T>( d ) ) );
                                              } };

  constexpr auto ForwardArg2 = TrivialFunctor{ "ForwardArgs<2>", []<typename T>( auto, auto, T&& d, auto... ) {
                                                return std::make_tuple( details::wrap_if_ref( std::forward<T>( d ) ) );
                                              } };

  /**
   * @brief std::get value by Index
   *
   * @tparam Index
   */
  template <int Index>
  struct Get_t final : Function {

    template <typename T>
    constexpr auto operator()( T&& tup ) const -> decltype( std::get<Index>( std::forward<T>( tup ) ) ) {
      return std::get<Index>( std::forward<T>( tup ) );
    }

    template <typename... Args>
    constexpr auto operator()( Args&&... args ) const {
      return ( *this )( ForwardArgs( std::forward<Args>( args )... ) );
    }
  };

  template <int Index>
  constexpr auto Get = Get_t<Index>{};

  /**
   * @brief Functor to return address of object
   */
  // clang format gets confused by the following lambdas...
  // clang-format off
  constexpr auto AddressOf =
      TrivialFunctor{"AddressOf",
                     []<typename T>( T& d ) -> void const* {
                          static_assert( !std::is_pointer_v<T> ); // make sure we don't take the address of a pointer!
                          return std::addressof( d ); },
                     []<typename T>( T* d )-> void const* {
                          static_assert( !std::is_pointer_v<T> ); // disallow pointers to pointers
                          return d;
                     },
                     []<typename T>( const SmartRef<T>& d ) -> void const* { return d.target(); }
      };
// clang-format on

/**
 * @brief Functor to trafo vecs into linalg vectors
 * FIXME make it such that we don't need this hack
 */
#if 0
  struct ToLinAlg_t final : Function {
    template <typename Data>
    Data operator()( Data&& d ) const { return std::forward<Data>( d ); }

    template <typename A, typename B>
    auto operator()( ROOT::Math::PositionVector3D<A, B> d ) const {
      return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{ d.x(), d.y(), d.z() };
    }
  };
  constexpr auto ToLinAlg = ToLinAlg_t{};
#else

  constexpr auto ToLinAlg =
      TrivialFunctor{ "ToLinAlg",
                      // warning we can't perfectly forward lvalues here.
                      // using decltype(auto) would lead to dangling references
                      []( auto&& d ) { return std::forward<decltype( d )>( d ); },
                      []<typename A, typename B>( ROOT::Math::PositionVector3D<A, B> d ) {
                        return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{ d.x(), d.y(), d.z() };
                      } };
#endif

  /**
   * @brief Functor to return X(input).
   */
  constexpr auto X_Coordinate = TrivialFunctor{ "X", []( auto const& d ) { return X( d ); } };

  /**
   * @brief Functor to return Y(input).
   */
  constexpr auto Y_Coordinate = TrivialFunctor{ "Y", []( auto const& d ) { return Y( d ); } };

  /**
   * @brief Functor to return Z(input).
   */
  constexpr auto Z_Coordinate = TrivialFunctor{ "Z", []( auto const& d ) { return Z( d ); } };

  /**
   * @brief Functor to return E(input).
   */
  constexpr auto E_Coordinate = TrivialFunctor{ "E", []( auto const& d ) { return E( d ); } };

  /**
   * @brief Functor to return input.phi().
   */
  constexpr auto Phi_Coordinate =
      TrivialFunctor{ "Phi", []( auto const& d ) -> decltype( d.phi() ) { return d.phi(); } };

  /**
   * @brief Functor to return input.eta().
   */
  constexpr auto Eta_Coordinate =
      TrivialFunctor{ "Eta", []( auto const& d ) -> decltype( d.eta() ) { return d.eta(); } };

  /**
   * @brief Functor to return input.rho().
   */
  constexpr auto Rho_Coordinate =
      TrivialFunctor{ "Rho", []( auto const& d ) -> decltype( d.rho() ) { return d.rho(); } };

  /**
   * @brief Functor to flatten outer_range<inner_range<T>> into vec<T>
   */
  namespace flatten_detail {
    template <typename T>
    concept RangeOfRanges = std::ranges::range<T> && std::ranges::range<std::ranges::range_value_t<T>>;
  }
  constexpr auto Flatten =
      TrivialFunctor{ "Flatten", []<flatten_detail::RangeOfRanges RangeOfRanges>( RangeOfRanges const& rr ) {
                       using value_t = std::ranges::range_value_t<std::ranges::range_value_t<RangeOfRanges>>;
                       std::vector<value_t> flattened;
                       // loop over the range of ranges
                       for ( auto& r : rr ) {
                         flattened.insert( flattened.end(), std::ranges::begin( r ), std::ranges::end( r ) );
                       }
                       return flattened;
                     } };

  /**
   * @brief Functor to return input.mag().
   */
  constexpr auto Magnitude =
      TrivialFunctor{ "Magnitude", []( auto const& d ) -> decltype( d.mag() ) { return d.mag(); } };

  /**
   * @brief Functor to return input/input.mag().
   */
  constexpr auto UnitVector =
      TrivialFunctor{ "UnitVector", []( auto const& d ) -> decltype( d / d.mag() ) { return d / d.mag(); } };

  /**
   * @brief Functor to return dot product of 2 inputs
   *
   * assumes dot(input1, input2) is defined
   */
  constexpr auto Dot = TrivialFunctor{
      "Dot", []( auto const& d1, auto const& d2 ) -> decltype( dot( d1, d2 ) ) { return dot( d1, d2 ); } };

  /**
   * @brief Functor to return normalized dot product of 2 inputs
   */

  // clang format gets confused by `requires` statements...
  // clang-format off
    constexpr auto NormedDot = TrivialFunctor{
        "NormedDot", []( auto const& d1, auto const& d2 )
                     requires requires {
                           dot( d1, d2 );
                           d1.mag2();
                           d2.mag2(); }
                     {
                        using std::sqrt;
                        return dot( d1, d2 ) / sqrt( d1.mag2() * d2.mag2() );
                     } ,
                     //  FIXME another hack because TrackCompactVertex 3-mom returns vec3...
                     []<typename A, typename B>(
                             LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> const& d1,
                             ROOT::Math::DisplacementVector3D<A, B> const&             r2 )
                     {
                        using std::sqrt;
                        decltype( d1 ) d2{r2.x(), r2.y(), r2.z()};
                        return dot( d1, d2 ) / sqrt( d1.mag2() * d2.mag2() );
                     }
        };
// clang-format on

/**
 * @brief Functor for adjusting the angle if greater than PI
 */
constexpr auto AdjustAngle =
    TrivialFunctor{ "AdjustAngle", []( auto const& d ) {
                     using std::abs, std::fmod, std::copysign;
                     auto angle = LHCb::Utils::as_arithmetic( d );
                     while ( angle > 2 * M_PI ) angle -= 2 * M_PI;
                     while ( angle < -2 * M_PI ) angle += 2 * M_PI;
                     return abs( angle ) > M_PI ? fmod( angle, M_PI ) - copysign( M_PI, angle ) : angle;
                   } };

/**
 * @brief Functor to return best pv.
 */
// decltype(auto) because PV could be heavy and we only want to forward the
// ref, right?
constexpr auto BestPV =
    GenericFunctor{ "BestPV",
                    []( auto const& vertices, auto const& tr ) -> decltype( Sel::getBestPV( tr, vertices ) ) {
                      return Sel::getBestPV( tr, vertices );
                    },
                    []( auto const& p ) -> decltype( p.pv().target() ) { return p.pv().target(); } };

/**
 * @brief Functor to return pv associated to object.
 */
constexpr auto OwnPV =
    GenericFunctor{ "OwnPV", []( auto const& p ) -> decltype( p.pv().target() ) { return p.pv().target(); } };

/**
 * @brief Functor to return of object has associated pv
 */
constexpr auto HasOwnPV =
    TrivialPredicate{ "HasOwnPV", []( auto const& p ) -> decltype( p.pv(), bool{} ) { return p.pv() != nullptr; },
                      []( auto const* p ) -> decltype( p->pv(), bool{} ) { return p && p->pv(); } };

/**
 * @brief Functor to return endvertex.
 */
constexpr auto EndVertex =
    GenericFunctor{ "EndVertex", []( const auto& v ) -> decltype( v.endVertex() ) { return v.endVertex(); },
                    []( const auto& v ) -> decltype( v.goodEndVertex() ) { return v.goodEndVertex(); } };

/**
 * @brief Functor to return endvertex.
 */
struct Position_t final : Function {

  template <typename HasAPosition>
  auto operator()( HasAPosition const& p ) const -> decltype( Sel::Utils::deref_if_ptr( p ).position() ) {
    return Sel::Utils::deref_if_ptr( p ).position();
  }
  // // hack around inconsistent interfaces, e.g endVertex returning a 3d point or
  // // vertex object and calling position on a 3d point doesn't work...
  template <typename T>
  auto operator()( LHCb::LinAlg::Vec<T, 3> const& p ) const {
    return p;
  }
  template <typename C, typename T>
  auto operator()( ROOT::Math::PositionVector3D<C, T> const& p ) const {
    return p;
  }

  auto operator()( LHCb::MCVertex const* p ) const {
    if ( p ) { return p->position(); }
    constexpr auto nan = std::numeric_limits<float>::quiet_NaN();
    return Gaudi::XYZPoint( nan, nan, nan );
  }

  template <typename T, std::size_t N>
  auto operator()( std::array<T const*, N> const& items ) const {
    using float_v = typename SIMDWrapper::type_map<LHCb::Event::details::instructionSet_for_<N>>::type::float_v;
    return std::apply(
        [this]( const auto&... i ) {
          constexpr auto nan       = std::numeric_limits<float>::quiet_NaN();
          const auto     invalid   = LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{ nan, nan, nan };
          auto const     into_vec3 = []( auto const& pos ) {
            return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{ pos.x(), pos.y(), pos.z() };
          };
          using LHCb::LinAlg::gather;
          return gather<float_v>( std::array{ ( i ? into_vec3( ( *this )( *i ) ) : invalid )... } );
        },
        items );
  }
};
constexpr auto Position = Position_t{};

constexpr auto ImpactParameter =
    TrivialFunctor{ "ImpactParameter", []( auto const& vertex_pos, auto const& track_chunk ) {
                     using Sel::Utils::impactParameterSquared;
                     return sqrt( impactParameterSquared( vertex_pos, track_chunk ) );
                   } };

constexpr auto ImpactParameterChi2 =
    TrivialFunctor{ "ImpactParameterChi2", []( auto const& vertex, auto const& track_chunk ) {
                     using Sel::Utils::impactParameterChi2;
                     return impactParameterChi2( track_chunk, vertex );
                   } };

constexpr auto ImpactParameterChi2ToVertex =
    TrivialFunctor{ "ImpactParameterChi2ToVertex",
                    []( auto const& vertices, auto const& particle ) {
                      // Get the associated PV -- this uses a link if it's available and
                      // computes the association if it's not.
                      using Sel::Utils::impactParameterChi2;
                      return impactParameterChi2( particle, Sel::getBestPV( particle, vertices ) );
                    },
                    []( auto const& particle ) {
                      // Get the associated PV -- this uses a link if it's available and
                      // computes the association if it's not.
                      using Sel::Utils::impactParameterChi2;
                      return particle.pv() ? impactParameterChi2( particle, particle.pv().target() ) : -1;
                    } };

constexpr auto ImpactParameterChi2ToOwnPV =
    TrivialFunctor{ "ImpactParameterChi2ToOwnPV", []( auto const& particle ) {
                     using Sel::Utils::impactParameterChi2;
                     return particle.pv() ? impactParameterChi2( particle, *particle.pv() ) : -1;
                   } };

template <typename... TESDepTypes>
struct TES final : public Function {

  // grammar.py turns list into vector so we just accept this for now
  // constructor efficiency isn't that important to us anyhow
  TES( std::vector<std::string> tes_locs ) {
    if ( tes_locs.size() != m_tes_locs.size() ) {
      throw GaudiException{ "TES Functor constructor expects " + std::to_string( m_tes_locs.size() ) +
                                " locations, but only got " + std::to_string( tes_locs.size() ),
                            name(), StatusCode::FAILURE };
    }
    for ( std::size_t i{ 0 }; i < m_tes_locs.size(); ++i ) { m_tes_locs[i] = std::move( tes_locs[i] ); }
  }

  // need this as per default this copy would be deleted because a copy of a
  // DataObjectReadHandle is deleted because AnyDataWrapper has a deleted copy.
  TES( TES<TESDepTypes...> const& other ) : m_tes_locs( other.m_tes_locs ) {}

  /** Set up the DataHandles, attributing the data dependencies to the given
   *  algorithm: set up each member of m_handles by calling .emplace() with the TES
   *  location and algorithm pointer.
   */
  void bind( TopLevelInfo& top_level ) {
    auto* alg = top_level.algorithm();
    if ( !alg ) throw GaudiException( "invoking bind without algorithm", __PRETTY_FUNCTION__, StatusCode::FAILURE );
    if ( alg->msgLevel( MSG::DEBUG ) ) { alg->debug() << "Init of DataHandles of Functor: " << name() << endmsg; }
    static_assert( std::is_base_of_v<IDataHandleHolder, std::remove_reference_t<decltype( *alg )>>,
                   "You must include the full declaration of the owning algorithm type!" );
    std::apply(
        [&]( auto&... h ) {
          auto loc = m_tes_locs.begin();
          ( init_data_handle( h.emplace( *loc++, alg ), alg ), ... );
        },
        m_handles );
  }

  /**
   * Retrieve the data dependencies from the TES and bake them into the
   * "prepared" functor that we return
   */
  auto prepare( EventContext const&, TopLevelInfo const& ) const {
    // Get a tuple of references to the data objects on the TES, and capture them
    // in a lambda which always returns them, and return that lambda
    return [deps = std::apply( [&]( auto&... h ) { return std::tie( deref( h )... ); }, m_handles )]( auto const&... ) {
      return deps;
    };
  }

  [[nodiscard]] std::string name() const {
    std::stringstream s;
    using GaudiUtils::operator<<;
    s << m_tes_locs;
    return "TES" + s.str();
  }

private:
  std::array<std::string, sizeof...( TESDepTypes )>               m_tes_locs;
  std::tuple<std::optional<DataObjectReadHandle<TESDepTypes>>...> m_handles;

  /**
   * @brief  Initialize a TES DataHandle and check that the owning algorithm
   * was configured correctly and already holds our input in ExtraInputs
   *
   * For more info on the logic please see the detailed explanation of how
   * functors obtain their data dependencies in the doc of the FunctorFactory.
   *
   * @param handle This handle will be initialized
   * @param alg Algorithm/Tool which owns this functor
   */
  template <typename T, typename Algorithm>
  void init_data_handle( DataObjectReadHandle<T>& handle, Algorithm* alg ) {
    if ( alg->msgLevel( MSG::DEBUG ) ) {
      alg->debug() << "  +  " << handle.objKey()
                   << " (will call init(): " << ( alg->FSMState() == Gaudi::StateMachine::INITIALIZED ) << ")"
                   << endmsg;
    }
    if ( alg->extraInputDeps().count( handle.objKey() ) == 0 ) {
      throw GaudiException{ "Usage of DataHandle[\"" + handle.objKey() +
                                "\"] in TES Functor requires that owning algorithm " + alg->name() +
                                " contains this TES location inside the ExtraInputs property. This is likely a "
                                "Configuration/PyConf bug!",
                            name(), StatusCode::FAILURE };
    }

    // DataObjectReadHandle has a protected `init()` so we need to call it
    // through it's base class. This is the same thing Gaudi::Algorithm does in
    // sysInitialize(). We do it here because this DataHandle is created inside
    // start(), at which point the step of initializing the handles of an
    // algorithm has already happened.
    // !! Exception !! if we are getting this functor from the cache then we
    // are already creating it in intialize(), and we need to skip the init()
    // call as it's also done in the sysInitialize() of the algorithm and it is
    // apparently forbidden to call init() twice on a DataHandle which is
    // checked via an assert in DataObjectHandleBase->init(). So we only run
    // init() here if the algorithm is already in an INITIALIZEDD state which
    // means this construction is happening inside start()
    if ( alg->FSMState() == Gaudi::StateMachine::INITIALIZED ) { static_cast<Gaudi::DataHandle*>( &handle )->init(); }
  }

  /** Check an optional data handle is initialised and doesn't return
   *  nullptr, then return a reference to the actual data.
   */
  template <typename T>
  auto const& deref( std::optional<T> const& handle ) const {
    if ( !handle ) {
      throw GaudiException{ "TES Functor called without being bound to an algorithm", name(), StatusCode::FAILURE };
    }
    auto data_ptr = handle->get();
    if ( !data_ptr ) { throw GaudiException{ "Functor got nullptr from its DataHandle", name(), StatusCode::FAILURE }; }
    return *data_ptr;
  }
};
/**
 * @brief Evaluates the abs of a quantity
 */
constexpr auto Abs = TrivialFunctor{ "Abs", []( auto const& d ) {
                                      using std::abs;
                                      return abs( d );
                                    } };

/**
 * @brief Evaluates the sqrt of a quantity
 */
constexpr auto Sqrt = TrivialFunctor{ "Sqrt", []( auto const& d ) {
                                       using std::sqrt;
                                       return sqrt( d );
                                     } };

/**
 * @brief return the set of relations associated to a relation table
 * */
constexpr auto Relations =
    GenericFunctor{ "Relations", []( auto const& table, auto const& from ) requires requires { table.relations( &from );
}
{
  const auto& range = table.relations( &from );
  return !range.empty() ? Functors::Optional{ std::move( range ) } : std::nullopt;
}
}
;

/**
 * @brief return the element on the FROM side of a relation
 * */
constexpr auto From =
    TrivialFunctor{ "From", []( auto const& relation ) -> decltype( relation.from() ) { return relation.from(); } };

/**
 * @brief return the element on the TO side of a relation
 * */
constexpr auto To =
    TrivialFunctor{ "To", []( auto const& relation ) -> decltype( relation.to() ) { return relation.to(); } };

/**
 * @brief return the element on the WEIGHT side of a relation
 * */
constexpr auto Weight = TrivialFunctor{
    "Weight", []( auto const& relation ) -> decltype( relation.weight() ) { return relation.weight(); } };

/**
 * @brief return true of the floating pointing numbers are close to each other
 * within some tolerance
 */
struct RequireClose final : Predicate {
  RequireClose( float abs_th = 1e-34, float rel_th = 1e-7 ) : m_abs( abs_th ), m_rel( rel_th / 2 ) {}

  template <typename Data>
  bool operator()( Data v1_d, Data v2_d ) const {
    auto const v1 = LHCb::Utils::as_arithmetic( v1_d );
    auto const v2 = LHCb::Utils::as_arithmetic( v2_d );
    using std::abs;
    using std::max;
    return abs( v1 - v2 ) < max( m_abs, m_rel * ( abs( v1 ) + abs( v2 ) ) );
  }

private:
  float m_abs, m_rel;
};

/**
 * @brief Evaluates the median of the elements in a container
 *        Note: the container is accepted _by value_ (i.e. a copy is made)
 *              as we need to (partially) sort (i.e. mutate it!) to find the median
 */
constexpr auto Median = TrivialFunctor{
    "Median", []( std::ranges::range auto range ) -> Functors::Optional<double> {
      if ( range.empty() ) return std::nullopt;
      size_t middle = range.size() / 2;
      std::nth_element( range.begin(), range.begin() + middle, range.end() );
      double median = static_cast<double>( range[middle] );
      if ( range.size() % 2 == 0 ) { // even - take the average of the two 'middle' elements
        median -= 0.5 * static_cast<double>(
                            range[middle] -
                            *std::max_element( range.begin(), range.begin() + middle ) ); // note: the rhs is always
                                                                                          // positive (or zero)
      }
      return median;
    } };

/**
 * @brief Evaluates the mean of a vector
 */
constexpr auto Mean = TrivialFunctor{ "Mean", []( std::ranges::range auto const& range ) -> Functors::Optional<double> {
                                       if ( range.empty() ) return std::nullopt;
                                       return std::accumulate( range.begin(), range.end(), 0.0 ) /
                                              range.size(); // accumulate as double to avoid overflow
                                     } };

} // namespace Functors::Common
