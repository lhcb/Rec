/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Optional.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/PluginService.h"
#include "GaudiKernel/ThreadLocalContext.h"
#include "Kernel/EventLocalUnique.h"
#include "Kernel/SynchronizedValue.h"
#include "Kernel/Traits.h"
#include "SelKernel/Utilities.h"

#define LHCB_FUNCTORS_INLINE [[gnu::always_inline]] inline

#include <typeinfo>

/** @file Core.h
 *  @brief Definition of the type-erased Functor<Output(Input)> type
 */
namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

/** @namespace Functors
 *
 *  Namespace for all new-style functors
 */
namespace Functors {

  /** @class AnyFunctor
   *  @brief Type-agnostic base class for functors, used in IFunctorFactory.
   *  @todo  Could consider using std::any's techniques for type-checking.
   */
  struct AnyFunctor {
    virtual ~AnyFunctor()                                                                  = default;
    virtual operator bool() const                                                          = 0;
    virtual void                                bind( Gaudi::Algorithm* owning_algorithm ) = 0;
    [[nodiscard]] virtual std::type_info const& rtype() const                              = 0;
  };

  /** @class TopLevelInfo
   *  @brief Small struct providing helpers for functor implementation.
   *
   *  This helper has two main functions. One is to be passed by non-const
   *  reference to the bind methods of functors. This is how the pointer to the
   *  owning algorithm is transmitted to individual functors' bind() methods.
   *  The second main function is that an instance of it is held by the
   *  top-level Functor<Out(In...)> wrapper and passed in by const reference
   *  when functors are invoked.
   */
  struct TopLevelInfo {
    /** Generate a unique property name that can be added to the owned
     *  algorithm. This is useful for functors that have to add, for example,
     *  ServiceHandles and use the functor configuration to propagate the
     *  relevant configuration.
     */
    std::string             generate_property_name();
    Gaudi::Algorithm*       algorithm() { return m_algorithm; }
    Gaudi::Algorithm const* algorithm() const { return m_algorithm; }
    void                    bind( Gaudi::Algorithm* alg ) { m_algorithm = alg; }
    void                    Warning( std::string_view message, std::size_t count = 1 ) const;

  private:
    Gaudi::Algorithm* m_algorithm{ nullptr };
    using MsgMap             = std::map<std::string, Gaudi::Accumulators::MsgCounter<MSG::WARNING>, std::less<>>;
    using SynchronizedMsgMap = LHCb::cxx::SynchronizedValue<MsgMap>;
    // We have to use std::unique_ptr here to make sure that this structure,
    // and the functors that hold it, remain moveable. SynchronizedValue
    // contains an std::mutex, which is not moveable.
    mutable std::unique_ptr<SynchronizedMsgMap> m_msg_counters{ std::make_unique<SynchronizedMsgMap>() };
  };

  /** @class Function
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All functors inherit from this class.
   */
  struct Function {};

  namespace detail {
    template <typename F>
    inline constexpr bool is_functor_function_v = std::is_base_of_v<Function, std::decay_t<F>>;
    template <typename F>
    concept FunctorFunction = is_functor_function_v<F>;
  } // namespace detail

  /** @class Predicate
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All predicates inherit from this class. A predicate is a function that
   *  returns a bool-like. A predicate can be wrapped up in the Filter functor
   *  to enable container-wise operations, and logical operations (and, or,
   *  etc.) are enabled for predicates but not for functions.
   */
  struct Predicate : public Function {};

  namespace detail {
    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_predicate_v = std::is_base_of_v<Predicate, std::decay_t<F>>;

    template <typename F>
    concept FunctorPredicate = is_functor_predicate_v<F>;
  } // namespace detail

  /** @class mask_arg_t
   *  @brief Tag type to indicate that a functor takes an explicit mask.
   *
   *  This tag can be used to indicate that the first argument given to a
   *  functor is a mask indicating the validity of the other functor arguments,
   *  which will be propagated to functors that need to know this mask. If no
   *  explicit mask is given, one will be determined from the other arguments.
   *
   *  For example,
   *    Functor<mask_v( Particle const& )>
   *  does not have an explicit mask, but one could be given by instead using
   *    Functor<mask_v( mask_arg_t, mask_v const&, Particle const& )>
   *
   *  For convenience, an instance of this type is also provided:
   *    Functors::mask_arg
   *  so the second signature could be invoked as:
   *    auto const my_prepared_functor = my_functor.prepare( evtCtx )
   *    my_prepared_functor( Functors::mask_arg, my_mask, my_particle )
   */
  struct mask_arg_t {
    explicit mask_arg_t() = default;
  };

  /** Convenient instance of Functors::mask_arg_t.
   */
  inline constexpr mask_arg_t mask_arg{};

  namespace detail {

    /** Call obj.bind( alg ) if obj has an appropriate bind method, otherwise do nothing. */
    template <detail::FunctorFunction Obj>
    LHCB_FUNCTORS_INLINE void bind( Obj& obj, [[maybe_unused]] TopLevelInfo& top_level ) {
      if constexpr ( requires { obj.bind( top_level ); } ) { obj.bind( top_level ); }
    }

    /** Helper to check if the given type has a static member bool named
     *  requires_explicit_mask, returns its value if so and false otherwise.
     */
    template <typename T>
    inline constexpr bool requires_explicit_mask_v = [] {
      if constexpr ( requires { T::requires_explicit_mask; } ) {
        return T::requires_explicit_mask;
      } else {
        return false;
      }
    }();

    /** Helper to check if the given type has a static member bool named
     *  is_optional_unwrapper, returns its value if so and false otherwise.
     */
    template <typename T>
    inline constexpr bool is_optional_unwrapper_v = [] {
      if constexpr ( requires { T::is_optional_unwrapper; } ) {
        return T::is_optional_unwrapper;
      } else {
        return false;
      }
    }();

    template <typename T>
    inline constexpr bool is_tuple_v = false;

    template <typename... Args>
    inline constexpr bool is_tuple_v<std::tuple<Args...>> = true;

    /** wrap a prepared functor and handle some invoke / visit
     *  and explicit mask details. There are five cases:
     *   - the functor does not want an explicit mask
     *   - the functor does want an explicit mask, and this was explicitly
     *     tagged/passed in 'args'
     *   - the functor does want an explicit mask, but this was not explicitly
     *     tagged/passed in 'args' and it must be derived from the values in
     *     'args'
     *   - the arguments are packed in a tuple: unpack, and forward
     *   - there are optionals amongst the arguments, and the functor does
     *     not opt-in to dealing with optionals itself
     */

    namespace prepped {

      /** Overload with an explicit mask tagged at the beginning of the
       *  parameter pack.
       */
      template <bool requires_explicit_mask, bool is_optional_unwrapper, typename F, typename Mask, typename... Args>
      LHCB_FUNCTORS_INLINE decltype( auto ) invoke( F&& f, mask_arg_t, Mask&& mask, Args&&... args ) {
        if constexpr ( !is_optional_unwrapper && ( is_our_optional_v<std::decay_t<Args>> || ... ) ) {
          return ( has_value( args ) && ... ) ? Functors::Optional{ invoke<requires_explicit_mask, true>(
                                                    std::forward<F>( f ), mask_arg, std::forward<Mask>( mask ),
                                                    get_value( std::forward<Args>( args ) )... ) }
                                              : std::nullopt;
        } else if constexpr ( requires_explicit_mask ) {
          return LHCb::invoke_or_visit( f, mask_arg, std::forward<Mask>( mask ), std::forward<Args>( args )... );
        } else {
          return LHCb::invoke_or_visit( f, std::forward<Args>( args )... );
        }
      }

      /** Overload with no explicit mask, if the wrapped functor requires one
       *  it is obtained from the rest of the parameter pack.
       */
      template <bool requires_explicit_mask, bool is_optional_unwrapper, typename F, typename... Args>
      LHCB_FUNCTORS_INLINE decltype( auto ) invoke( F&& f, Args&&... args ) {
        if constexpr ( !is_optional_unwrapper && ( is_our_optional_v<std::decay_t<Args>> || ... ) ) {
          return ( has_value( args ) && ... ) ? Functors::Optional{ invoke<requires_explicit_mask, true>(
                                                    f, get_value( std::forward<Args>( args ) )... ) }
                                              : std::nullopt;
        } else if constexpr ( requires_explicit_mask ) {
          return LHCb::invoke_or_visit( f, mask_arg,
                                        loop_mask( static_cast<std::remove_reference_t<Args>&>( args )... ),
                                        std::forward<Args>( args )... );
        } else {
          return LHCb::invoke_or_visit( f, std::forward<Args>( args )... );
        }
      }

      /** Overloads for a tuple of inputs
       */
      template <bool requires_explicit_mask, bool is_optional_unwrapper, typename F, typename Mask, typename... Args>
      LHCB_FUNCTORS_INLINE decltype( auto ) invoke( F&& f, mask_arg_t, Mask&& mask, std::tuple<Args...> args ) {
        return std::apply(
            [&]<typename... Arg>( Arg&&... arg ) {
              return invoke<requires_explicit_mask, is_optional_unwrapper>( f, mask_arg, std::forward<Mask>( mask ),
                                                                            std::forward<Arg>( arg )... );
            },
            std::move( args ) );
      }

      template <bool requires_explicit_mask, bool is_optional_unwrapper, typename F, typename... Args>
      LHCB_FUNCTORS_INLINE decltype( auto ) invoke( F&& f, std::tuple<Args...> args ) {
        return std::apply(
            [&]<typename... Arg>( Arg&&... arg ) {
              return invoke<requires_explicit_mask, is_optional_unwrapper>( f, std::forward<Arg>( arg )... );
            },
            std::move( args ) );
      }

    } // namespace prepped

    template <detail::FunctorFunction Functor>
    LHCB_FUNCTORS_INLINE auto prepared( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level ) {
      auto prepare = [&]( Functor const& f, EventContext const& evtCtx, TopLevelInfo const& top_level ) {
        // veto old-style code which may have different 'prepare' functions...
        static_assert(
            !requires { f.prepare(); }, "please implement prepare(evtCtx,top_level) instead of prepare()" );
        static_assert(
            !requires { f.prepare( top_level ); },
            "please implement prepare(evtCtx,top_level) instead of prepare(top_level)" );

        if constexpr ( requires { f.prepare( evtCtx, top_level ); } ) {
          return f.prepare( evtCtx, top_level );
        } else {
          return std::cref( f );
        }
      };
      return [g = prepare( f, evtCtx, top_level )]<typename... Args>( Args&&... args ) {
        return prepped::invoke<requires_explicit_mask_v<Functor>, is_optional_unwrapper_v<Functor>>(
            g, std::forward<Args>( args )... );
      };
    };
    template <detail::FunctorFunction Functor>
    auto prepared( Functor&&, EventContext const&, TopLevelInfo const& ) = delete;

  } // namespace detail

  /** @brief Type-erased wrapper around a generic functor.
   *
   *  This is a std::function-like type-erasing wrapper that is used to store
   *  and pass around functors without exposing the extremely verbose full types
   *  of [composite] functors. This is specialised as
   *  Functors::Functor<OutputType(InputType)>, which is rather similar to
   *  std::function<Output(Input)>, but includes the extra bind( ... ) method
   *  that is needed to set up functors' data dependencies and associate them
   *  with the owning algorithm.
   *
   *  @todo Consider adding some local storage so we can avoid dynamic allocation
   *        for "small" functors.
   */

  template <typename>
  class Functor;

  template <typename OutputType, typename... InputType>
  class Functor<OutputType( InputType... )> final : public AnyFunctor {
  public:
    using result_type = OutputType;

  private:
    /** @brief Type-erased wrapper around a generic prepared functor.
     *
     *  This is a std::function-like type-erasing wrapper that is used to store
     *  and pass around prepared functors without exposing their extremely
     *  verbose full types. This is very similar to std::function<Out(In)>, but
     *  it supports custom allocators -- in this case LHCb's event-local one.
     *
     *  @todo Consider adding some local storage so we can avoid dynamic allocation
     *        for "small" functors -- but that makes 'move' somewhat more cumbersome...
     */

    class prepared_type final {
      using result_type    = OutputType;
      using allocator_type = LHCb::Allocators::EventLocal<void>;

      struct VTable {
        void ( *delete_ )( void* )                              = []( void* ) {};
        result_type ( *invoke )( void const*, InputType... in ) = nullptr;

        // avoid making this a constructor to keep this class an aggregate
        template <typename F>
        LHCB_FUNCTORS_INLINE static VTable create() {
          return { .delete_ = []( void* p ) { delete static_cast<F*>( p ); },
                   .invoke  = []( void const* p, InputType... i ) -> result_type {
                     return static_cast<result_type>( ( *static_cast<F const*>( p ) )( i... ) );
                   } };
        }
      };

      void*  m_ptr    = nullptr;
      VTable m_vtable = {};

    public:
      /** Wrap the given functor up as a type-erased PreparedFunctor, using the
       *  given allocator. TODO: use the provided allocator
       */
      template <typename F>
      LHCB_FUNCTORS_INLINE prepared_type( F f, allocator_type ) // TODO: use allocator...
          : m_ptr{ new F{ std::move( f ) } }, m_vtable{ VTable::template create<F>() } {}

      ~prepared_type() { ( *m_vtable.delete_ )( m_ptr ); }

      prepared_type( prepared_type const& )           = delete;
      prepared_type operator=( prepared_type const& ) = delete;

      prepared_type( prepared_type&& rhs ) noexcept
          : m_ptr{ std::exchange( rhs.m_ptr, nullptr ) }, m_vtable{ std::exchange( rhs.m_vtable, VTable{} ) } {}

      prepared_type& operator=( prepared_type&& rhs ) noexcept {
        ( *m_vtable.delete_ )( m_ptr );
        m_ptr    = std::exchange( rhs.m_ptr, nullptr );
        m_vtable = std::exchange( rhs.m_vtable, VTable{} );
        return *this;
      }

      /** Invoke the prepared functor.
       */
      result_type operator()( InputType... in ) const {
        if ( !m_ptr || !m_vtable.invoke ) { throw std::runtime_error( "Empty PreparedFunctor<Out(In...)> called" ); }
        return ( *m_vtable.invoke )( m_ptr, in... );
      }

      /** Check if the prepared functor is invocable.
       */
      operator bool() const { return m_ptr != nullptr; }
    };

    struct VTable {
      void ( *delete_ )( void* )             = []( void* ) {};
      void ( *bind )( void*, TopLevelInfo& ) = nullptr;
      result_type ( *invoke )( void const*, EventContext const&, TopLevelInfo const&, InputType... in ) = nullptr;
      prepared_type ( *prepare )( void const*, EventContext const&, TopLevelInfo const& )               = nullptr;

      // avoid a constructor so this class remains an aggregate
      template <typename F>
      LHCB_FUNCTORS_INLINE static VTable create() {
        return {
            .delete_ = []( void* p ) { delete static_cast<F*>( p ); },
            .bind    = []( void* p, TopLevelInfo& top_level ) { detail::bind( *static_cast<F*>( p ), top_level ); },
            .invoke  = []( void const* p, EventContext const& evtCtx, TopLevelInfo const& top_level,
                          InputType... input ) -> result_type {
              // Prepare and call in one go, avoiding the overhead of .prepare
              static_assert(
                  requires( F const& f, InputType... in ) { detail::prepared( f, evtCtx, top_level )( in... ); } );
              return static_cast<result_type>(
                  detail::prepared( *static_cast<const F*>( p ), evtCtx, top_level )( input... ) );
            },
            .prepare = []( void const* p, EventContext const& evtCtx, TopLevelInfo const& top_level ) -> prepared_type {
              // Prepare the functor once and store it as a 'member' of a lambda that
              // invokes/visits it; return that lambda wrapped up in the type-erasing
              // wrapper type prepared_type (which takes a handle to the memory
              // resource in order to avoid dynamic allocation using the system
              // allocator)
              //
              // NOTE: we do not need a 'generic' prepared functor here -- we _know_ the types of the arguments that
              //       will be used to invoke this prepared functor -- hence the 'wrapping' could be short-circuited...
              // TODO: pass the In... types 'down the line' to detail::prepared and bake that into the returned lambda,
              //       and do so recursively!
              static_assert(
                  requires( F const& f, InputType... in ) { detail::prepared( f, evtCtx, top_level )( in... ); } );
              return { detail::prepared( *static_cast<F const*>( p ), evtCtx, top_level ),
                       LHCb::getMemResource( evtCtx ) };
            } };
      }
    };

    void*        m_ptr    = nullptr;
    VTable       m_vtable = {};
    TopLevelInfo m_top_level;

  public:
    /** @brief Construct an empty Functor object.
     *
     *  Note that an empty Functor will throw an exception if the function call
     *  operator is called.
     */
    Functor() = default;

    /** Construct a Functor object
     */
    template <typename F>
    LHCB_FUNCTORS_INLINE Functor( F func )
        : m_ptr{ new F{ std::move( func ) } }, m_vtable{ VTable::template create<F>() } {}

    ~Functor() { ( *m_vtable.delete_ )( m_ptr ); }

    Functor( const Functor& )            = delete;
    Functor& operator=( const Functor& ) = delete;

    /** Move constructor and assignment
     */
    Functor( Functor&& rhs ) noexcept
        : m_ptr{ std::exchange( rhs.m_ptr, nullptr ) }
        , m_vtable{ std::exchange( rhs.m_vtable, VTable{} ) }
        , m_top_level{ std::move( rhs.m_top_level ) } {}

    Functor& operator=( Functor&& rhs ) noexcept {
      ( *m_vtable.delete_ )( m_ptr );
      m_vtable    = std::exchange( rhs.m_vtable, VTable{} );
      m_ptr       = std::exchange( rhs.m_ptr, nullptr );
      m_top_level = std::move( rhs.m_top_level );
      return *this;
    }

    /** Fill this Functor with new contents.
     */
    template <typename F>
    Functor& operator=( F func ) {
      ( *m_vtable.delete_ )( m_ptr );
      m_ptr    = new F{ std::move( func ) };
      m_vtable = VTable::template create<F>();
      return *this;
    }

    /** @brief Bind the contained object to the given algorithm.
     *
     *  The contained object may have data dependencies, which must be bound to the algorithm
     *  that owns the functor. Calling a functor with data dependencies without binding it to
     *  an algorithm will cause an exception to be thrown.
     *
     *  @param owning_algorithm The algorithm to which the data dependency should be associated.
     *
     *  @todo Change the argument type to the minimal type needed to initialise a
     *        DataObjectReadHandle, Gaudi::Algorithm is over the top.
     */
    void bind( Gaudi::Algorithm* owning_algorithm ) override {
      m_top_level.bind( owning_algorithm );
      if ( !m_ptr || !m_vtable.bind ) { throw std::runtime_error( "Empty Functor<Out(In...)> prepared" ); }
      ( *m_vtable.bind )( m_ptr, m_top_level );
    }

    /** @brief Invoke the contained object.
     *
     * Invoke the contained object. If the Functor object is empty, or if the contained object
     * has data dependencies and has not been bound to an owning algorithm, an exception will
     * be thrown.
     *
     * @param input Input to the contained object
     * @return      Output of the contained object
     */
    result_type operator()( InputType... input ) const { return ( *this )( Gaudi::Hive::currentContext(), input... ); }

    /** @brief Invoke the contained object, using the given EventContext.
     */
    result_type operator()( EventContext const& evtCtx, InputType... input ) const {
      if ( !m_ptr || !m_vtable.invoke ) { throw std::runtime_error( "Empty Functor<Out(In...)> called" ); }
      return ( *m_vtable.invoke )( m_ptr, evtCtx, m_top_level, input... );
    }

    prepared_type prepare( EventContext const& evtCtx = Gaudi::Hive::currentContext() ) const {
      if ( !m_ptr || !m_vtable.prepare ) { throw std::runtime_error( "Empty Functor<Out(In...)> prepared" ); }
      return ( *m_vtable.prepare )( m_ptr, evtCtx, m_top_level );
    }

    /** Query whether the Functor is empty or not.
     *
     *  @return false if the Functor is empty, true if it is not
     */
    operator bool() const override { return m_ptr != nullptr; }

    /** Query the return type of the contained functor.
     */
    std::type_info const& rtype() const override { return typeid( result_type ); }
  };
} // namespace Functors
