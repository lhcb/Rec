/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/PrimaryVertices.h"
#include "Functors/Function.h"
#include "SelTools/DistanceCalculator.h"

#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/System.h"

#include <optional>
#include <type_traits>
#include <vector>

/** @file  Utilities.h
 *  @brief Helper classes for defining functors.
 */
namespace Functors::detail {

  /** Return the value of obj.name() if that exists, otherwise try to demangle
   *  the type name. */
  template <typename Derived>
  auto get_name( Derived const& obj ) -> decltype( obj.name() ) {
    return obj.name();
  }
  template <typename Derived>
  std::string get_name( Derived const& ) {
    return System::typeinfoName( typeid( Derived ) );
  }

  /** Default type of the TES container of PVs. */
  using DefaultPVContainer_t = LHCb::Event::PV::PrimaryVertexContainer;

  /** Default DistanceCalculator type. */
  using DefaultDistanceCalculator_t = Sel::DistanceCalculator;

  /** Default LifetimeFitter type. */
  using DefaultLifetimeFitter_t = Sel::LifetimeFitter;

  /** @brief Make std::array<T, N> from std::tuple<Ts...>
   *
   *  This is a useful utility for functors taking list-like arguments because,
   *  to avoid template parameter deduction ambiguities, the Python wrapper
   *  explicitly passes list-like arguments as std::tuple, but in the C++ it is
   *  often more convenient to express these arguments with std::array of some
   *  common type. Typical usage might be:
   *    template <std::size_t N>
   *    struct MyFunctor : public Function {
   *      using arg_t = std::variant<float, std::string>;
   *      template <typename ArgTup>
   *      MyFunctor( ArgTup&& arg_list )
   *      : m_args{detail::array_of<arg_t>( std::forward<ArgTup>( arg_list ) )}
   *      {}
   *    private:
   *      std::array<arg_t, N> m_args;
   *    };
   *
   *    template <typename... Ts>
   *    MyFunctor( std::tuple<Ts...>&& ) -> MyFunctor<sizeof...(Ts)>;
   *  Which allows a heterogeneous set of arguments in Python (e.g. [42, 'foo'])
   *  to be mapped onto std::array<arg_t, N> in C++, with N a compile-time
   *  constant deduced from the length of the argument list.
   *
   *  Note that each element of the input tuple is explicitly converted to the
   *  array element type T.
   */
  template <typename T, typename... Ts>
  constexpr std::array<T, sizeof...( Ts )> array_of( std::tuple<Ts...>&& tup ) {
    return std::apply(
        []( auto&&... x ) { return std::array<T, sizeof...( Ts )>{ { T{ std::forward<decltype( x )>( x ) }... } }; },
        std::forward<std::tuple<Ts...>>( tup ) );
  }

  /** @brief Make std::vector<T> from std::tuple<Ts...>
   *  std::vector analogue of std::array<T, N>, hopefully this will rarely be
   *  used as array_of<T>() exists and avoids dynamic allocation.
   */
  template <typename T, typename... Ts>
  constexpr std::vector<T> vector_of( std::tuple<Ts...>&& tup ) {
    std::vector<T> ret;
    ret.reserve( sizeof...( Ts ) );
    std::apply( [&ret]( auto&&... x ) { ( ret.emplace_back( std::forward<decltype( x )>( x ) ), ... ); },
                std::forward<std::tuple<Ts...>>( tup ) );
    return ret;
  }

  /** Helper function for ends_with **/
  inline bool ends_with( std::string_view s, std::string_view suffix ) {
    return s.size() >= suffix.size() && s.substr( s.size() - suffix.size() ) == suffix;
  }

  inline void require_suffix( const std::vector<std::string>& names, std::string_view suffix ) {
    auto i = std::find_if( names.begin(), names.end(), [=]( const auto& name ) { return !ends_with( name, suffix ); } );
    if ( i != names.end() ) {
      throw GaudiException{ "The specified name '" + ( *i ) + "' does not end with suffix '" + std::string{ suffix } +
                                "'. Please add it!",
                            "Functors::DecReportsFilter::check_suffix()", StatusCode::FAILURE };
    }
  }

} // namespace Functors::detail
