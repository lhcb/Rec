/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/SerializeSTL.h>
#include <string>
#include <vector>
//
#include <Gaudi/Parsers/Factory.h>

namespace Functors {
  namespace detail {
    struct Argument {
      std::string const& s;

      operator float() const { return std::stof( s ); } // can deal with "NaN"

      template <typename T>
      operator T() const {
        T arg{};
        using Gaudi::Parsers::parse;
        parse( arg, s ).orThrow( "bad literal: \'" + s + "\'", __PRETTY_FUNCTION__ );
        return arg;
      }
    };
  } // namespace detail

  class Arguments {
    std::vector<std::string> m_args;

  public:
    Arguments( std::vector<std::string> args ) : m_args{ std::move( args ) } {}
    [[nodiscard]] detail::Argument operator[]( int i ) const { return { m_args.at( i ) }; }
    friend std::ostream&           operator<<( std::ostream& os, Arguments const& args ) {
      using GaudiUtils::operator<<;
      return os << args.m_args;
    }
  };
} // namespace Functors
