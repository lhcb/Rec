/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/System.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/STLExtensions.h"

/** @file  Combination.h
 *  @brief Definitions of functors for tuple-likes of track-like objects.
 *
 *  Typical argument types for these functors are Sel::ParticleCombination and
 *  Sel::ParticleCombinationSpan.
 */

namespace Functors::detail {

  template <typename T>
  void throw_exception_if_basic( T const& p, std::string const& msg = "DistanceOfClosestApproach::operator()" ) {
    if constexpr ( requires { p.isBasicParticle(); } ) {
      if ( p.isBasicParticle() )
        throw GaudiException{ msg,
                              "Specified combination has no daughters. Note that the functor should only be applied to "
                              "composite (i.e. particles with daughters).",
                              StatusCode::FAILURE };
    }
  }

  struct max_t {
    template <typename T>
    T operator()( T const& i, T const& j ) const {
      using std::max;
      return max( i, j );
    }
  };
  constexpr auto maximum = max_t{};

  enum DOCAMethod { Distance, Chi2 };

  class IDistanceCalculatorHolder {

    class Parent {
      const void* m_parent                                                                                    = nullptr;
      const LHCb::DetDesc::ConditionContext& ( *m_get_condition_context )( void const*, EventContext const& ) = nullptr;
      MsgStream& ( *m_get_warning )( void const* )                                                            = nullptr;
      LHCb::DetDesc::ConditionAccessor<DetectorElement> m_geom; // yuck -- the right way is binding the
                                                                // condition into the tool... but that
                                                                // requires updating all 'other' tool users
                                                                // to use the right ToolHandle...
    public:
      template <typename T>
      Parent( T* ptr )
          : m_parent{ ptr }
          , m_get_condition_context{ []( const void* parent, const EventContext& ctx ) -> decltype( auto ) {
            return static_cast<T const*>( parent )->getConditionContext( ctx );
          } }
          , m_get_warning{ []( const void* parent ) -> decltype( auto ) {
            return static_cast<T const*>( parent )->warning();
          } }
          , m_geom( ptr, LHCb::standard_geometry_top ) {}

      MsgStream& warning() const { return ( *m_get_warning )( m_parent ); }

      auto* geometry( EventContext const& ctx ) const {
        return m_geom.get( ( *m_get_condition_context )( m_parent, ctx ) ).geometry();
      }
    };

    std::optional<detail::DefaultDistanceCalculator_t> m_dist;
    std::optional<Parent>                              m_parent;
    std::optional<ToolHandle<IDistanceCalculator>>     m_tool;
    std::string                                        m_tool_name;

  public:
    IDistanceCalculatorHolder( std::string instance ) : m_tool_name{ std::move( instance ) } {}

    IDistanceCalculatorHolder( IDistanceCalculatorHolder&& rhs ) : m_tool_name{ std::move( rhs.m_tool_name ) } {
      if ( m_parent.has_value() || m_dist.has_value() || m_tool.has_value() )
        throw GaudiException{ "DistanceOfClosestApproach(&&)", "attempt to move after bind", StatusCode::FAILURE };
    }
    IDistanceCalculatorHolder& operator=( IDistanceCalculatorHolder&& rhs ) {
      if ( m_parent.has_value() || m_dist.has_value() || m_tool.has_value() )
        throw GaudiException{ "DistanceOfClosestApproach(&&)", "attempt to move after bind", StatusCode::FAILURE };
      m_tool_name = std::move( rhs.m_tool_name );
      return *this;
    }
    IDistanceCalculatorHolder( IDistanceCalculatorHolder const& rhs ) : m_tool_name{ rhs.m_tool_name } {
      if ( m_parent.has_value() || m_dist.has_value() || m_tool.has_value() )
        throw GaudiException{ "DistanceOfClosestApproach(&&)", "attempt to copy after bind", StatusCode::FAILURE };
    }
    IDistanceCalculatorHolder& operator=( IDistanceCalculatorHolder const& rhs ) {
      if ( m_parent.has_value() || m_dist.has_value() || m_tool.has_value() )
        throw GaudiException{ "DistanceOfClosestApproach(&&)", "attempt to copy after bind", StatusCode::FAILURE };
      m_tool_name = rhs.m_tool_name;
      return *this;
    }

    void emplace( TopLevelInfo& top_level ) {
      m_dist.emplace( top_level.algorithm() );
      m_tool.emplace( m_tool_name, top_level.algorithm() );

      auto try_emplace = [&]( auto* parent ) {
        if ( parent ) {
          m_parent.emplace( parent );
          m_tool->retrieve().ignore();
        }
        return parent;
      };

      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<>*>( top_level.algorithm() ) ) ) return;
      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<GaudiTupleAlg>*>( top_level.algorithm() ) ) )
        return;

      throw GaudiException{ "DistanceOfClosestApproach::bind",
                            "attempting to bind to algorithm which is not a condition holder", StatusCode::FAILURE };
    }

    template <DOCAMethod method>
    auto prepare( EventContext const& ctx ) const {
      assert( m_tool.has_value() );
      assert( m_parent.has_value() );
      if ( !m_tool->get() ) { m_parent.value().warning() << "tool retrieval too late!!!" << endmsg; }
      return [tool = m_tool->get(), geom = m_parent->geometry( ctx ), parent = LHCb::get_pointer( m_parent ),
              dist_calc = LHCb::get_pointer( m_dist )]( const auto& p1, const auto& p2 ) {
        auto const* pp1 = &Sel::Utils::deref_if_ptr( p1 );
        auto const* pp2 = &Sel::Utils::deref_if_ptr( p2 );
        // this would be so much easier in C++20 as one could use `if constexpr ( requires{ ... } ) `
        if constexpr ( std::is_convertible_v<LHCb::Particle const*, decltype( pp1 )> &&
                       std::is_convertible_v<LHCb::Particle const*, decltype( pp2 )> ) { // TODO: just check whether
                                                                                         // tool->distance( ... ) can be
                                                                                         // called...
          if constexpr ( method == DOCAMethod::Distance ) {
            double distance{};
            tool->distance( pp1, pp2, distance, *geom ).ignore(); // TODO: flag error in counter...
            return distance;
          } else {
            double distance{};
            double chi2{};
            tool->distance( pp1, pp2, distance, chi2, *geom ).ignore(); // TODO: flag error in counter...
            return chi2;
          }
        } else {
          parent->warning() << " requested to use IDistanceCalculator, but that does not support the arguments -- "
                               "falling back to trivial state doca "
                            << System::typeinfoName( typeid( decltype( p1 ) ) ) << " "
                            << System::typeinfoName( typeid( decltype( p2 ) ) ) << " " << endmsg;
          if constexpr ( method == DOCAMethod::Distance ) {
            return dist_calc->particleDOCA( p1, p2 );
          } else {
            return dist_calc->particleDOCAChi2( p1, p2 );
          }
        }
      };
    }
  };

  //////   struct DistanceOfClosestApproach;

  template <int, int, DOCAMethod, typename DistanceCalculator>
  struct DistanceOfClosestApproach;

  template <int N, int M, DOCAMethod method>
  struct DistanceOfClosestApproach<N, M, method, IDistanceCalculator> : public Function {
    static_assert( N >= 1 && M >= 1, "Indices start from 1 for LoKi compatibility." );
    DistanceOfClosestApproach( std::string instance = "LoKi::DistanceCalculator" ) : m_dist{ std::move( instance ) } {}

    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const {
      return [calc = m_dist.template prepare<method>( evtCtx )]( const auto& combination_ptr ) {
        auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
        // check if basic particle and throw an useful exception for this doca functor
        Functors::detail::throw_exception_if_basic( combination, "DistanceOfClosestApproach::operator()" );
        using LHCb::Event::decayProducts;
        const auto& dp = decayProducts( combination );
        return calc( dp.template get<N - 1>(), dp.template get<M - 1>() );
      };
    }

  private:
    IDistanceCalculatorHolder m_dist;
  };

  template <int N, int M, DOCAMethod method>
  struct DistanceOfClosestApproach<N, M, method, detail::DefaultDistanceCalculator_t> : public Function {
    static_assert( N >= 1 && M >= 1, "Indices start from 1 for LoKi compatibility." );

    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level.algorithm() ); }

    template <typename CombinationType>
    auto operator()( CombinationType const& combination_ptr ) const {
      auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
      // check if basic particle and throw an useful exception for this doca functor
      Functors::detail::throw_exception_if_basic( combination, "DistanceOfClosestApproach::operator()" );
      using LHCb::Event::decayProducts;
      const auto& dp = decayProducts( combination );
      if constexpr ( method == DOCAMethod::Distance ) {
        return m_dist->particleDOCA( dp.template get<N - 1>(), dp.template get<M - 1>() );
      } else {
        return m_dist->particleDOCAChi2( dp.template get<N - 1>(), dp.template get<M - 1>() );
      }
    }

  private:
    std::optional<detail::DefaultDistanceCalculator_t> m_dist;
  };

  //////   struct MaxDistanceOfClosestApproach;

  template <DOCAMethod method, typename DistanceCalculator>
  struct MaxDistanceOfClosestApproach;

  template <DOCAMethod method>
  struct MaxDistanceOfClosestApproach<method, detail::DefaultDistanceCalculator_t> : public Function {
    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level.algorithm() ); }
    template <typename CombinationType>
    auto operator()( CombinationType const& combination_ptr ) const {
      auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
      // check if basic particle and throw an useful exception for this doca functor
      Functors::detail::throw_exception_if_basic( combination, "MaxDistanceOfClosestApproach::operator()" );
      using LHCb::Event::decayProducts;
      return decayProducts( combination )
          .pairwise_transform_reduce(
              [&]( const auto& i1, const auto& i2 ) {
                if constexpr ( method == DOCAMethod::Distance ) {
                  return m_dist->particleDOCA( i1, i2 );
                } else {
                  return m_dist->particleDOCAChi2( i1, i2 );
                }
              },
              maximum );
    }

  private:
    std::optional<detail::DefaultDistanceCalculator_t> m_dist;
  };

  template <DOCAMethod method>
  struct MaxDistanceOfClosestApproach<method, IDistanceCalculator> : public Function {
    MaxDistanceOfClosestApproach( std::string instance = "LoKi::DistanceCalculator" )
        : m_dist{ std::move( instance ) } {}

    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const {
      return [transform = m_dist.template prepare<method>( evtCtx )]( const auto& combination_ptr ) {
        auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
        // check if basic particle and throw an useful exception for this doca functor
        Functors::detail::throw_exception_if_basic( combination, "MaxDistanceOfClosestApproach::operator()" );
        using LHCb::Event::decayProducts;
        return decayProducts( combination ).pairwise_transform_reduce( transform, maximum );
      };
    }

  private:
    IDistanceCalculatorHolder m_dist;
  };

  //////   struct MaxDistanceOfClosestApproachCut;

  template <DOCAMethod, typename DistanceCalculator>
  class MaxDistanceOfClosestApproachCut;

  template <DOCAMethod method>
  class MaxDistanceOfClosestApproachCut<method, detail::DefaultDistanceCalculator_t> : public Predicate {
  public:
    MaxDistanceOfClosestApproachCut( float thresh ) : m_thresh( thresh ) {}

    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level.algorithm() ); }

    template <typename CombinationType>
    auto operator()( CombinationType const& combination_ptr ) const {
      auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
      // check if basic particle and throw an useful exception for this doca functor
      Functors::detail::throw_exception_if_basic( combination, "MaxDistanceOfClosestApproachCut::operator()" );
      using LHCb::Event::decayProducts;
      const auto& dp = decayProducts( combination );
      return dp.pairwise_none_of( [&]( const auto& i1, const auto& i2 ) {
        if constexpr ( method == DOCAMethod::Distance ) {
          return m_dist->particleDOCA( i1, i2 ) > m_thresh;
        } else {
          return m_dist->particleDOCAChi2( i1, i2 ) > m_thresh;
        }
      } );
    }

  private:
    std::optional<detail::DefaultDistanceCalculator_t> m_dist;
    float                                              m_thresh;
  };

  template <DOCAMethod method>
  class MaxDistanceOfClosestApproachCut<method, IDistanceCalculator> : public Predicate {
  public:
    MaxDistanceOfClosestApproachCut( float thresh, std::string instance = "LoKi::DistanceCalculator" )
        : m_dist{ std::move( instance ) }, m_thresh( thresh ) {}

    void bind( TopLevelInfo& top_level ) { m_dist.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const {
      return [calc = m_dist.template prepare<method>( evtCtx ), threshold = m_thresh]( const auto& combination_ptr ) {
        auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
        // check if basic particle and throw an useful exception for this doca functor
        Functors::detail::throw_exception_if_basic( combination, "MaxDistanceOfClosestApproachCut::operator()" );
        using LHCb::Event::decayProducts;
        return decayProducts( combination ).pairwise_none_of( [&]( const auto& i1, const auto& i2 ) {
          return calc( i1, i2 ) > threshold;
        } );
      };
    }

  private:
    IDistanceCalculatorHolder m_dist;
    float                     m_thresh;
  };

} // namespace Functors::detail

/** @namespace Functors::Combination
 *
 * TODO
 */
namespace Functors::Combination {

  // TODO: replace the functor below which take explicit indices by a composition: one functor which
  //      fetches the relevant children (Adapters::SubCombination), and then a 'binary' one which
  //      computes the relevant observable

  template <int N, int M>
  const auto SDistanceOfClosestApproach =
      detail::DistanceOfClosestApproach<N, M, detail::DOCAMethod::Distance, detail::DefaultDistanceCalculator_t>{};
  template <int N, int M>
  const auto DistanceOfClosestApproach =
      detail::DistanceOfClosestApproach<N, M, detail::DOCAMethod::Distance, IDistanceCalculator>{};
  template <int N, int M>
  const auto SDistanceOfClosestApproachChi2 =
      detail::DistanceOfClosestApproach<N, M, detail::DOCAMethod::Chi2, detail::DefaultDistanceCalculator_t>{};
  template <int N, int M>
  const auto DistanceOfClosestApproachChi2 =
      detail::DistanceOfClosestApproach<N, M, detail::DOCAMethod::Chi2, IDistanceCalculator>{};

  // TODO: replace functors below with one generic functor which computes the maximum of some other functor
  //       over all pairs in the combination, and then use composition

  const auto MaxSDistanceOfClosestApproach =
      detail::MaxDistanceOfClosestApproach<detail::DOCAMethod::Distance, detail::DefaultDistanceCalculator_t>{};
  const auto MaxDistanceOfClosestApproach =
      detail::MaxDistanceOfClosestApproach<detail::DOCAMethod::Distance, IDistanceCalculator>{};
  const auto MaxSDistanceOfClosestApproachChi2 =
      detail::MaxDistanceOfClosestApproach<detail::DOCAMethod::Chi2, detail::DefaultDistanceCalculator_t>{};
  const auto MaxDistanceOfClosestApproachChi2 =
      detail::MaxDistanceOfClosestApproach<detail::DOCAMethod::Chi2, IDistanceCalculator>{};

  // TODO: replace functors below with one generic functor which cuts on the the maximum of some other functor
  //       when applied over all pairs in the combination, and then use composition
  //
  using MaxSDistanceOfClosestApproachCut =
      detail::MaxDistanceOfClosestApproachCut<detail::DOCAMethod::Distance, detail::DefaultDistanceCalculator_t>;
  using MaxDistanceOfClosestApproachCut =
      detail::MaxDistanceOfClosestApproachCut<detail::DOCAMethod::Distance, IDistanceCalculator>;
  using MaxSDistanceOfClosestApproachChi2Cut =
      detail::MaxDistanceOfClosestApproachCut<detail::DOCAMethod::Chi2, detail::DefaultDistanceCalculator_t>;
  using MaxDistanceOfClosestApproachChi2Cut =
      detail::MaxDistanceOfClosestApproachCut<detail::DOCAMethod::Chi2, IDistanceCalculator>;

  /** @brief cos(angle) between two children.
   * note: Indices start from 1 for LoKi compatibility.
   */
  template <int N, int M>
    requires( N > 0 && M > 0 )
  struct CosAngleBetweenDecayProducts_t : Function {
    template <typename CombinationType>
    auto operator()( CombinationType const& combination_ptr ) const {
      auto const& combination = Sel::Utils::deref_if_ptr( combination_ptr );
      // check if basic particle and throw an useful exception for this doca functor
      Functors::detail::throw_exception_if_basic( combination, "CosAngleBetweenDecayProducts::operator()" );
      using LHCb::Event::decayProducts;
      using LHCb::Event::threeMomentum;
      using std::sqrt;
      const auto& dp = decayProducts( combination );
      auto        m1 = threeMomentum( dp.template get<N - 1>() );
      auto        m2 = threeMomentum( dp.template get<M - 1>() );
      return dot( m1, m2 ) / sqrt( m1.mag2() * m2.mag2() );
    }
  };

  template <int N, int M>
  constexpr auto CosAngleBetweenDecayProducts = CosAngleBetweenDecayProducts_t<N, M>{};

  constexpr auto Charge = TrivialFunctor{ "Charge", []( auto const& item ) {
                                           using LHCb::Event::charge;
                                           return charge( item );
                                         } };
} // namespace Functors::Combination
