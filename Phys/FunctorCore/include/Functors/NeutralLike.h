/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"
#include "Functors/PID.h"

namespace Functors::Neutral {

  /** @brief The below functors use the information stored in NeutralPID objects
   */

  template <typename R>
  struct NeutralPID_Accessor_t : TrivialFunctor<R ( LHCb::NeutralPID::* )() const> {
    template <size_t N>
    constexpr NeutralPID_Accessor_t( const char ( &name )[N], R ( LHCb::NeutralPID::*fn )() const )
        : TrivialFunctor<R ( LHCb::NeutralPID::* )() const>{ name, fn } {}
    using TrivialFunctor<R ( LHCb::NeutralPID::* )() const>::operator();
    auto operator()( LHCb::Particle const& p ) const {
      return Functors::and_then( p.proto(), &LHCb::ProtoParticle::neutralPID, *this );
    }
    auto operator()( LHCb::Particle const* p ) const { return Functors::and_then( p, *this ); }
  };

  constexpr auto IsPhoton    = NeutralPID_Accessor_t{ "Neutral::IsPhoton", &LHCb::NeutralPID::IsPhoton };
  constexpr auto IsNotH      = NeutralPID_Accessor_t{ "Neutral::IsNotH", &LHCb::NeutralPID::IsNotH };
  constexpr auto ShowerShape = NeutralPID_Accessor_t{ "Neutral::ShowerShape", &LHCb::NeutralPID::ShowerShape };
  constexpr auto NeutralE19  = NeutralPID_Accessor_t{ "Neutral::NeutralE19", &LHCb::NeutralPID::CaloNeutralE19 };
  constexpr auto NeutralE49  = NeutralPID_Accessor_t{ "Neutral::NeutralE49", &LHCb::NeutralPID::CaloNeutralE49 };
  constexpr auto Saturation  = NeutralPID_Accessor_t{ "Neutral::Saturation", &LHCb::NeutralPID::Saturation };
  constexpr auto NeutralHcal2Ecal =
      NeutralPID_Accessor_t{ "Neutral::NeutralHcal2Ecal", &LHCb::NeutralPID::CaloNeutralHcal2Ecal };
  constexpr auto NeutralEcal = NeutralPID_Accessor_t{ "Neutral::NeutralEcal", &LHCb::NeutralPID::CaloNeutralEcal };
  constexpr auto ClusterMass = NeutralPID_Accessor_t{ "Neutral::ClusterMass", &LHCb::NeutralPID::ClusterMass };

  constexpr auto NeutralID =
      Functors::PID::details::CellIDFunc{ &LHCb::ProtoParticle::neutralPID, &LHCb::NeutralPID::CaloNeutralID };

} // namespace Functors::Neutral
