/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Core/FloatComparison.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include <algorithm>
#include <cstddef>
// #include <GaudiKernel/reverse.h>

namespace Functors::Functional {

  /** @brief map functor over range.
   *
   * */
  template <detail::FunctorFunction F>
  struct Map final : Function {

    Map( F f ) : m_f{ std::move( f ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<F>;

    /* Improve error messages. */
    constexpr auto name() const { return "Map( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [pf = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>(
                   mask_arg_t, auto mask, Range const& range, auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          static_assert( std::is_invocable_v<decltype( pf ), mask_arg_t, decltype( mask ),
                                             std::ranges::range_value_t<Range>, decltype( input )...> );
          using result_t = std::remove_cvref_t<decltype( pf( mask_arg, mask, *begin( range ), input... ) )>;
          std::vector<result_t> out;
          out.reserve( range.size() );
          std::ranges::transform( range, std::back_inserter( out ),
                                  [&]( const auto& val ) { return pf( mask_arg, mask, val, input... ); } );

          return out;
        };
      } else {
        return [pf = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>( Range const& range,
                                                                                            auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          static_assert( std::is_invocable_v<decltype( pf ), std::ranges::range_value_t<Range>, decltype( input )...> );
          using result_t = std::remove_cvref_t<decltype( pf( *begin( range ), input... ) )>;
          std::vector<result_t> out;
          out.reserve( range.size() );
          std::ranges::transform( range, std::back_inserter( out ),
                                  [&]( const auto& val ) { return pf( val, input... ); } );

          return out;
        };
      }
    }

  private:
    F m_f;
  };

  /** @brief map predicate functor over range.
   *  Early stopping once a value is true
   * */
  template <detail::FunctorPredicate Pred>
  struct MapAnyOf final : Predicate {

    MapAnyOf( Pred f ) : m_f{ std::move( f ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<Pred>;
    static_assert( detail::is_functor_predicate_v<Pred>, "Functors::Functional::MapAnyOf must wrap a Predicate" );

    /* Improve error messages. */
    constexpr auto name() const { return "AnyOf( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [pf = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>(
                   mask_arg_t, auto mask, Range const& range, auto const&... input ) {
          using Sel::Utils::all;
          // the logic here is the same as in `prepare_(std::logical_or<>, ... )`
          auto done = !mask;
          for ( auto const& item : range ) {
            done = done || pf( mask_arg, !done, item, input... );
            if ( all( done ) ) break;
          }
          return mask && done;
        };
      } else {
        return [f = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>( Range const& range,
                                                                                           auto const&... input ) {
          return std::ranges::any_of( range, [&]( auto const& i ) { return f( i, input... ); } );
        };
      }
    }

  private:
    Pred m_f;
  };

  /** @brief map predicate functor over range.
   *  Early stopping if one value is false
   * */
  template <detail::FunctorPredicate Pred>
  struct MapAllOf final : public Predicate {

    MapAllOf( Pred f ) : m_f{ std::move( f ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<Pred>;
    static_assert( detail::is_functor_predicate_v<Pred>, "Functors::Functional::MapAllOf must wrap a Predicate" );

    /* Improve error messages. */
    constexpr auto name() const { return "AllOf( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [f = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>(
                   mask_arg_t, auto mask, Range const& range, auto const&... input ) {
          for ( auto const& item : range ) {
            using Sel::Utils::none;
            mask = mask && f( mask_arg, mask, item, input... );
            if ( none( mask ) ) break;
          }
          return mask;
        };
      } else {
        return [f = detail::prepared( m_f, evtCtx, top_level )]<std::ranges::range Range>( Range const& range,
                                                                                           auto const&... input ) {
          return std::ranges::all_of( range, [&]( auto const& i ) { return f( i, input... ); } );
        };
      }
    }

  private:
    Pred m_f;
  };

  /**
   * @brief return front element of range
   * */
  constexpr auto Front = TrivialFunctor{
      "Front",
      []<typename T>( T&& r ) -> decltype( std::forward<T>( r ).front() ) {
        return std::forward<T>( r ).front();
      } // namespace Functors::Functional
  };

  /**
   * @brief return back element of range
   * */
  constexpr auto Back = TrivialFunctor{ "Back", []<typename T>( T&& r ) -> decltype( std::forward<T>( r ).back() ) {
                                         return std::forward<T>( r ).back();
                                       } };

  /* *
   * @brief reverse the order of an range
   * */
  // constexpr auto Reverse = TrivialFunctor{"Reverse",[](auto&& r) { return reverse( std::forward<decltype(r)>(r) ); }
  // };
  constexpr auto Reverse =
      TrivialFunctor{ "Reverse", []<std::ranges::range Range>( Range&& range ) {
                       using std::ranges::rbegin;
                       using std::ranges::rend;
                       return std::vector<std::ranges::range_value_t<Range>>{ rbegin( range ), rend( range ) };
                     } };

  /* *
   * @brief return min element of range
   * */
  constexpr auto Min =
      TrivialFunctor{ "Min", []<std::ranges::range Range>( Range const& range ) {
                       using std::ranges::begin;
                       using std::ranges::end;
                       // FIXME doc.
                       return std::accumulate( begin( range ), end( range ),
                                               std::ranges::range_value_t<Range>{ std::numeric_limits<float>::max() },
                                               []( auto curr_min_value, auto value ) {
                                                 using std::min;
                                                 return min( curr_min_value, value );
                                               } );
                     } };

  /* *
   * @brief reduces range to minimum value that is not zero
   * */
  constexpr auto MinElementNotZero =
      TrivialFunctor{ "MinElementNotZero", []<std::ranges::range Range>( Range const& range ) {
                       using Sel::Utils::select;
                       using std::abs;
                       using std::min;
                       using std::ranges::begin;
                       using std::ranges::end;
                       return std::accumulate( begin( range ), end( range ),
                                               std::numeric_limits<std::ranges::range_value_t<Range>>::max(),
                                               []( auto curr_min_value, auto value ) {
                                                 return select( !LHCb::essentiallyZero( value ),
                                                                min( curr_min_value, value ), curr_min_value );
                                               } );
                     } };

  /* *
   * @brief return max element of range
   * */
  constexpr auto Max = TrivialFunctor{ "Max", []<std::ranges::range Range>( Range const& range ) {
                                        using std::ranges::begin;
                                        using std::ranges::end;
                                        // FIXME doc.
                                        return std::accumulate(
                                            begin( range ), end( range ),
                                            std::ranges::range_value_t<Range>{ std::numeric_limits<float>::lowest() },
                                            []( auto curr_max_value, auto value ) {
                                              using std::max;
                                              return max( curr_max_value, value );
                                            } );
                                      } };

  /* *
   * @brief return sum element of range
   * */
  constexpr auto Sum = TrivialFunctor(
      "Sum", []<std::ranges::range Range>( Range const& range, std::ranges::range_value_t<Range> init = 0 ) {
        using std::ranges::begin;
        using std::ranges::end;
        return std::accumulate( begin( range ), end( range ), std::ranges::range_value_t<Range>{ init } );
      } );

  /** @brief Return the entry from a range for which the provided functor returns the smallest value.
   *
   * */
  template <detail::FunctorFunction F>
  struct MinElement final : public Function {

    MinElement( F f ) : m_f{ std::move( f ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<F>;

    /* Improve error messages. */
    constexpr auto name() const { return "MinElement( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [pf = detail::prepared( m_f, evtCtx, top_level )](
                   mask_arg_t, auto mask, std::ranges::range auto const& range, auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          using std::next;
          using std::make_pair;

          if ( begin( range ) == end( range ) ) throw std::runtime_error( "Attempt to iterate over an empty range" );

          return std::accumulate( next( begin( range ) ), end( range ),
                                  make_pair( *begin( range ), pf( mask_arg, mask, *begin( range ), input... ) ),
                                  [&]( auto accumulator, auto current_entry ) {
                                    auto current_value = pf( mask_arg, mask, current_entry, input... );
                                    return current_value < accumulator.second
                                               ? make_pair( current_entry, current_value )
                                               : accumulator;
                                  } )
              .first;
        };
      } else {
        return [pf = detail::prepared( m_f, evtCtx, top_level )]( std::ranges::range auto const& range,
                                                                  auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          using std::next;
          using std::make_pair;

          if ( begin( range ) == end( range ) ) throw std::runtime_error( "Attempt to iterate over an empty range" );

          return std::accumulate( next( begin( range ) ), end( range ),
                                  make_pair( *begin( range ), pf( *begin( range ), input... ) ),
                                  [&]( auto accumulator, auto current_entry ) {
                                    auto current_value = pf( current_entry, input... );
                                    return current_value < accumulator.second
                                               ? make_pair( current_entry, current_value )
                                               : accumulator;
                                  } )
              .first;
        };
      }
    }

  private:
    F m_f;
  };

  /** @brief Return the entry among a set of relations for which the application of the functor gives maximum value.
   *
   * */
  template <detail::FunctorFunction F>
  struct MaxElement final : public Function {

    MaxElement( F f ) : m_f{ std::move( f ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<F>;

    /* Improve error messages. */
    constexpr auto name() const { return "MaxElement( " + detail::get_name( m_f ) + " )"; }

    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [pf = detail::prepared( m_f, evtCtx, top_level )](
                   mask_arg_t, auto mask, std::ranges::range auto const& range, auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          using std::next;
          using std::make_pair;

          if ( begin( range ) == end( range ) ) throw std::runtime_error( "Attempt to iterate over an empty range" );

          auto max_entry = std::accumulate(
              next( begin( range ) ), end( range ),
              make_pair( *begin( range ), pf( mask_arg, mask, *begin( range ), input... ) ),
              [&]( auto accumulator, auto current_entry ) {
                auto current_value = pf( mask_arg, mask, current_entry, input... );
                return current_value > accumulator.second ? make_pair( current_entry, current_value ) : accumulator;
              } );
          return max_entry.first;
        };
      } else {
        return [pf = detail::prepared( m_f, evtCtx, top_level )]( std::ranges::range auto const& range,
                                                                  auto const&... input ) {
          using std::ranges::begin;
          using std::ranges::end;
          using std::next;
          using std::make_pair;

          if ( begin( range ) == end( range ) ) throw std::runtime_error( "Attempt to iterate over an empty range" );

          auto max_entry = std::accumulate(
              next( begin( range ) ), end( range ), make_pair( *begin( range ), pf( *begin( range ), input... ) ),
              [&]( auto accumulator, auto current_entry ) {
                auto current_value = pf( current_entry, input... );
                return current_value > accumulator.second ? make_pair( current_entry, current_value ) : accumulator;
              } );
          return max_entry.first;
        };
      }
    }

  private:
    F m_f;
  };

  /**
   * @brief Cast the provided argument to the specified template argument
   * optional is empty
   * */
  template <typename T>
  constexpr auto CastTo =
      TrivialFunctor{ "CastTo", []<typename U>( U&& value ) -> decltype( static_cast<T>( std::forward<U>( value ) ) ) {
                       return static_cast<T>( std::forward<U>( value ) );
                     } };

  /**
   * @brief Return the value inside a Functors::Optional, or a default value if
   * optional is empty
   * */
  template <typename T>
  struct ValueOr final : public Function {
    ValueOr( T value ) : m_value{ std::move( value ) } {}

    // inform composedfunctor that this functor will turn an optional into a concrete value even if optional is not
    // engaged
    static constexpr bool is_optional_unwrapper = true;

    template <typename U>
    auto operator()( U&& opt_value ) const {
      if constexpr ( detail::is_our_optional_v<std::decay_t<U>> ) {
        if ( opt_value.has_value() ) { return std::forward<U>( opt_value ).value(); }

        // Expand tuple to initialize the default value, this is used to initialize classes such as
        // LHCb::LinAlg::Vec<float_v,N>
        using value_type_t = typename std::decay_t<U>::value_type;
        if constexpr ( detail::is_tuple_v<std::decay_t<T>> ) {
          static_assert( detail::is_constructible_from_tuple_v<value_type_t, T>,
                         "The return type must be constructible by the default args." );
          return std::make_from_tuple<value_type_t>( m_value );
        } else if constexpr ( std::is_enum_v<value_type_t> ) {
          static_assert( std::is_convertible_v<std::underlying_type_t<value_type_t>, T>,
                         "Return type value_t needs to be constructible from the member variable of type T" );
          return static_cast<value_type_t>( m_value );
        } else {
          static_assert( std::is_constructible_v<value_type_t, T>,
                         "Return type value_t needs to be constructible from the member variable of type T" );
          return static_cast<value_type_t>( m_value );
        }
      } else {
        // Forward the not optional input
        return std::forward<U>( opt_value );
      }
    }

  private:
    T m_value;
  };

  // make sure that we unwrap std::integral_constant<T,t> into T when deducing ValueOr's template argument
  template <typename T, T t>
  ValueOr( std::integral_constant<T, t> ) -> ValueOr<T>;

  /**
   * @brief Return the value from a map given a key
   * */
  template <typename Key>
  class ValueFromDict final : public Function {
    Key m_key;

  public:
    ValueFromDict( Key k ) : m_key{ std::move( k ) } {}

    template <typename Dict>
      requires requires( Dict d, Key k ) { d.find( k ) != d.end(); }
    auto operator()( Dict const& dict ) const {
      auto i = dict.find( m_key );
      return i != dict.end() ? Functors::Optional{ i->second } : std::nullopt;
    }
  };

  /**
   * @brief Return the value inside a Functors::Optional.
   * If nullopt it throws an error.
   * */
  struct Value_t final : public Function {

    // inform composedfunctor that this functor will turn an optional into a concrete value even if optional is not
    // engaged
    static constexpr bool is_optional_unwrapper = true;

    template <typename U>
    auto operator()( U&& opt_value ) const {
      /// check if its is optional
      if constexpr ( detail::is_our_optional_v<std::decay_t<U>> ) {
        if ( opt_value.has_value() ) // return the value if it exists
          return std::forward<U>( opt_value ).value();
        else // throw an exception if it does not
          throw GaudiException( "Functors::Value: Optional has no value. Consider using F.VALUE_OR functor instead.",
                                "Functors::Value", StatusCode::FAILURE );
      } else { // if it is not optional just return the value
        return std::forward<U>( opt_value );
      }
    }
  };

  constexpr auto Value = Value_t{};

  /**
   * @brief Return true if the input has a valid value.
   * */
  struct HasValue_t final : Predicate {

    // inform composedfunctor that this functor will turn an optional into a concrete value even if optional is not
    // engaged
    static constexpr bool is_optional_unwrapper = true;

    template <typename U>
    bool operator()( Optional<U> const& o ) const {
      return o.has_value();
    }
    template <typename U>
    bool operator()( std::optional<U> const& o ) const {
      return o.has_value();
    }
    template <typename U>
    bool operator()( SmartRef<U> const& o ) const {
      return o != nullptr;
    }
    bool operator()( void const* const& o ) const { return o != nullptr; }
    bool operator()( std::nullptr_t ) const { return false; }
    bool operator()( std::nullopt_t ) const { return false; }
    template <typename Arg>
      requires( !std::is_pointer_v<Arg> )
    bool operator()( Arg const& ) const {
      return true;
    }
  };

  constexpr auto HasValue = HasValue_t{};

  /**
   * @brief Count the number of elements in a range satisfying the specified predicate
   * */
  template <detail::FunctorPredicate Predicate>
  class CountIf final : public Function {
    Predicate m_predicate;

  public:
    CountIf( Predicate predicate ) : m_predicate{ std::move( predicate ) } {}

    constexpr static bool requires_explicit_mask = detail::requires_explicit_mask_v<Predicate>;

    void bind( TopLevelInfo& top_level ) { detail::bind( m_predicate, top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      if constexpr ( requires_explicit_mask ) {
        return [predicate = detail::prepared( m_predicate, evtCtx, top_level )]<std::ranges::range Range>(
                   mask_arg_t, auto mask, Range const& range ) {
          return std::ranges::count_if( range, LHCb::cxx::bind_front( predicate, mask_arg, mask ) );
        };
      } else {
        return [predicate = detail::prepared( m_predicate, evtCtx, top_level )](
                   std::ranges::range auto const& range ) { return std::ranges::count_if( range, predicate ); };
      }
    }
  };
} // namespace Functors::Functional
