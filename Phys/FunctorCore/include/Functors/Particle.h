/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Core/FloatComparison.h"
#include "Event/Particle_v2.h"
#include "Functors/Function.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ITisTos.h"
#include "Kernel/ParticleProperty.h"
#include "SelKernel/Utilities.h"
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/ServiceHandle.h>

/** @file  Particle.h
 *  @brief Definitions of functors for particle-like objects
 *  (common to both track-like and composite).
 */

namespace Functors::detail {

  template <typename T>
  constexpr bool is_legacy_particle =
      std::is_same_v<LHCb::Particle, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  template <typename T>
  using has_track_t = decltype( std::declval<T>().track() );
  template <typename T>
  constexpr bool has_track =
      Gaudi::cpp17::is_detected_v<has_track_t, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

  template <typename T>
  constexpr bool is_charged_basic =
      std::is_same_v<LHCb::Event::ChargedBasics, std::decay_t<std::remove_pointer_t<std::decay_t<T>>>>;

}; // namespace Functors::detail

/** @namespace Functors::Particle
 *
 *  Functors that make sense for all particles (both track-like
 *  and composite)
 */

namespace Functors::Particle {

  struct ParticlePropertyUser : public Function {
    /** Base class for functors that use ParticlePropertySvc
     *  to convert string ID into integer ID.
     */
    template <typename String>
      requires std::convertible_to<String, std::string>
    ParticlePropertyUser( String id_input ) : m_id{ static_cast<std::string>( id_input ) } {}

    template <typename ID>
      requires std::convertible_to<ID, LHCb::ParticleID>
    ParticlePropertyUser( ID id_input ) : m_id{ static_cast<LHCb::ParticleID>( id_input ) } {}

    void bind( TopLevelInfo& top_level ) {
      m_pp_svc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
    }

    template <typename... Data>
    decltype( auto ) operator()( Data&&... ) const {
      assert( m_pp_svc.has_value() );
      LHCb::ParticleProperty const* pp =
          std::visit( [&]( const auto& id ) { return m_pp_svc.value()->find( id ); }, m_id );
      if ( !pp )
        throw GaudiException{
            "Couldn't get ParticleProperty for particle '" +
                std::visit( Gaudi::overload( []( const std::string& s ) { return s; },
                                             []( const LHCb::ParticleID& i ) { return i.toString(); } ),
                            m_id ) +
                "'",
            "Functors::Particle::GetParticleProperty()", StatusCode::FAILURE };
      return *pp;
    }

  private:
    std::variant<std::string, LHCb::ParticleID>              m_id;
    std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> m_pp_svc{};
  };

  /** @brief get the track object from the charged basic particle
   * E.g. When requisting track momentum one can use this
   * functor in the composition via F.TRACK_PT = F.PT @ F.TRACK
   */
  struct GetTrack_t : public Function {
    // Perfect forward
    template <typename Data>
      requires( !detail::is_legacy_particle<Data> && !detail::has_track<Data> && !detail::is_charged_basic<Data> )
    decltype( auto ) operator()( Data&& value ) const {
      return std::forward<Data>( value );
    }

    // Legacy particles: LHCb::Particle and LHCb::ProtoParticle
    template <typename Data>
      requires( (detail::is_legacy_particle<Data> || detail::has_track<Data>) && !detail::is_charged_basic<Data> )
    auto operator()( Data&& d ) const {
      if constexpr ( detail::is_legacy_particle<Data> ) {
        auto const* pp  = Sel::Utils::deref_if_ptr( d ).proto();
        auto        trk = pp ? pp->track() : nullptr;
        return trk ? Functors::Optional{ trk }
                   : std::nullopt; // FIXME: wrapping a pointer in an optional is a semantic stutter
      } else {
        auto trk = Sel::Utils::deref_if_ptr( d ).track();
        return trk ? Functors::Optional{ trk }
                   : std::nullopt; // FIXME: wrapping a pointer in an optional is a semantic stutter
      };
    }

    // V2 LHCb::Event::ChargedBasics
    template <typename Data>
    auto operator()( Data const& d ) const -> decltype( d.tracks() ) {
      return d.tracks();
    }
  };
  constexpr auto GetTrack = GetTrack_t{};

  /** @brief get the protoparticle from the  particle
   * Needed because e.g. some Relation tables are made from ProtoParticles and not Particles...
   */
  constexpr auto GetProtoParticle = GenericFunctor{ "GetProtoParticle", &LHCb::Particle::proto };

  /** @brief covariance matrix for (px, py, pz)
   */
  constexpr auto threeMomCovMatrix = TrivialFunctor{ "threeMomCovMatrix", []( auto const& d ) {
                                                      using LHCb::Event::threeMomCovMatrix;
                                                      return threeMomCovMatrix( d );
                                                    } };

  /** @brief covariance matrix for (px, py, pz) x (x, y, z)
   * [(x,px), (x,py), (x,pz)]
   * [(y,px), (y,py), (y,pz)]
   * [(z,px), (z,py), (z,pz)]
   */
  constexpr auto threeMomPosCovMatrix = TrivialFunctor{ "threeMomPosCovMatrix", []( auto const& d ) {
                                                         using LHCb::Event::threeMomPosCovMatrix;
                                                         return threeMomPosCovMatrix( d );
                                                       } };

  /** @brief covariance matrix for (px, py, pz, pe) x (x, y, z)
   * [(x,px), (x,py), (x,pz), (x,pe)]
   * [(y,px), (y,py), (y,pz), (y,pe)]
   * [(z,px), (z,py), (z,pz), (z,pe)]
   */
  constexpr auto momPosCovMatrix = TrivialFunctor{ "momPosCovMatrix", []( auto const& d ) {
                                                    using LHCb::Event::momPosCovMatrix;
                                                    return momPosCovMatrix( d );
                                                  } };

  /** @brief covariance matrix for (x, y, z)
   */
  constexpr auto posCovMatrix = TrivialFunctor{ "posCovMatrix", []( auto const& d ) {
                                                 using LHCb::Event::posCovMatrix;
                                                 return posCovMatrix( d );
                                               } };

  /** @brief covariance matrix for (px, py, pz, pe)
   */
  constexpr auto momCovMatrix = TrivialFunctor{ "momCovMatrix", []( auto const& d ) {
                                                 using LHCb::Event::momCovMatrix;
                                                 return momCovMatrix( d );
                                               } };

  constexpr auto PPHasMuonInfo =
      TrivialPredicate{ "PPHasMuon", []( LHCb::ProtoParticle const& pp ) -> bool { return pp.muonPID(); },
                        []( LHCb::ProtoParticle const* pp ) -> bool { return pp && pp->muonPID(); } };

  constexpr auto PPHasRich =
      TrivialPredicate{ "PPHasRich", []( LHCb::ProtoParticle const& pp ) -> bool { return pp.richPID(); },
                        []( LHCb::ProtoParticle const* pp ) -> bool { return pp && pp->richPID(); } };

  /**
   * @brief Check if the "TriggerResult_t" object is TOS
   */
  constexpr auto IsTos = TrivialPredicate{ "IsTOS", &LHCb::detail::TisTosResult_t::TOS };

  /**
   * @brief Check if the "TriggerResult_t" object is TIS
   */
  constexpr auto IsTis = TrivialPredicate{ "IsTIS", &LHCb::detail::TisTosResult_t::TIS };

  /**
   * @brief Check if the "TriggerResult_t" object is TUS
   */
  constexpr auto IsTus = TrivialPredicate{ "IsTUS", &LHCb::detail::TisTosResult_t::TUS };

  /** @brief Check if the particle is a basic particle (not composite) */
  constexpr auto IsBasicParticle = TrivialPredicate{
      "IsBasicParticle", []( const auto& p ) -> decltype( p.isBasicParticle() ) { return p.isBasicParticle(); },
      []( const auto* p ) -> decltype( p->isBasicParticle() ) { return p && p->isBasicParticle(); } };

} // namespace Functors::Particle
