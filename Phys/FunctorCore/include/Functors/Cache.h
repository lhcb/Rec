/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <string>
#include <string_view>

/** @file  Cache.h
 *  @brief Utilities the pre-compiled functor cache.
 */

/** @namespace Functors::Cache
 *
 *  Namespace for helpers relating to the precompiled cache for new functors.
 */
namespace Functors::Cache {
  using HashType = std::size_t;

  /** @brief Thin wrapper around std::hash, used to find functors in the cache.
   */
  HashType makeHash( std::string_view data );

  /** @brief Generate string representation of hash
   */
  std::string hashToStr( HashType hash );
} // namespace Functors::Cache
