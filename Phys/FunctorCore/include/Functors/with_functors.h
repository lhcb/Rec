/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/STLExtensions.h"

#include <tuple>
#include <utility>

namespace Functors::detail {
  /** Type that we use to tag whether or not the functor factory service handle
   *  has been added to a class.
   */
  struct with_functor_factory_tag {};

  /** Type that actually adds the functor factory service handle, this should
   *  only be inherited from via the with_functor_factory alias below that
   *  protects against multiple inheritance.
   */
  template <typename base_t>
  struct add_functor_factory : public base_t, public with_functor_factory_tag {
    using base_t::base_t;
    Functors::IFactory& getFunctorFactory() {
      m_functor_factory.retrieve().ignore();
      return *m_functor_factory;
    }

  private:
    ServiceHandle<Functors::IFactory> m_functor_factory{ this, "FunctorFactory", "FunctorFactory" };
  };

  /** Pseudo-mixin that provides the a getFunctorFactory() accessor to the
   *  functor factory service. This takes care of adding only one property and
   *  one service handle, even if the mixin appears multiple times in the
   *  inheritance structure.
   */
  template <typename base_t>
  using with_functor_factory =
      std::conditional_t<std::is_base_of_v<with_functor_factory_tag, base_t>, base_t, add_functor_factory<base_t>>;

  /** Helper type used by with_functors and with_functor_maps to add only one
   *  Gaudi::Property for each unique property name, in case the given tag
   *  types include duplicate property names.
   */
  template <typename value_type, typename... Tags>
  struct property_holder {
    using Property_t = Gaudi::Property<value_type>;
    using tags_list  = boost::mp11::mp_list<Tags...>;
    // Assert that tags are unique
    static_assert( boost::mp11::mp_is_set<tags_list>::value );

    // If several of our tag types have the same PropertyName then we want to
    // map these onto a single Gaudi::Property object. To achieve that, we
    // fill an array of reference wrappers. If the PropertyName was already
    // added to 'owner' then we store a reference to that one, otherwise we
    // create a new property in 'm_property_storage' and reference that.
    template <typename OWNER>
    property_holder( OWNER* owner, value_type const& default_value )
        : m_property_refs{ init_property_refs( owner, default_value, std::index_sequence_for<Tags...>{} ) } {}

    template <std::size_t I>
    value_type const& get() const {
      static_assert( I < sizeof...( Tags ) );
      return m_property_refs[I].get();
    }

  private:
    template <std::size_t I, typename OWNER>
    std::reference_wrapper<Property_t const> init_property_ref( OWNER* owner, value_type const& default_value ) {
      std::string const PropertyName = boost::mp11::mp_at_c<tags_list, I>::PropertyName;
      if ( owner->hasProperty( PropertyName ) ) {
        // property was already added, let's check it was the correct type
        auto const& property_base = owner->getProperty( PropertyName );
        assert( property_base.type_info() );
        assert( *property_base.type_info() == typeid( value_type ) );
        return dynamic_cast<Property_t const&>( property_base );
      } else {
        // property wasn't already added, make one
        return m_property_storage[I].emplace( owner, PropertyName, default_value, [owner]( auto& ) {
          if ( owner->FSMState() < Gaudi::StateMachine::INITIALIZED ) { return; }
          owner->template decode<I>();
        } );
      }
    }

    template <typename OWNER, std::size_t... Idxs>
    auto init_property_refs( OWNER* owner, value_type const& default_value, std::index_sequence<Idxs...> ) {
      return std::array{ init_property_ref<Idxs>( owner, default_value )... };
    }

    std::array<std::optional<Property_t>, sizeof...( Tags )>                m_property_storage{};
    std::array<std::reference_wrapper<Property_t const>, sizeof...( Tags )> m_property_refs;
  };
} // namespace Functors::detail

/** @brief Add functors to an algorithm.
 */
template <typename base_t, typename... Tags>
struct with_functors : public Functors::detail::with_functor_factory<base_t> {
  using functor_tag_types = std::tuple<Tags...>;

private:
  using FunctorsTuple = std::tuple<Functors::Functor<typename Tags::Signature>...>;

  template <std::size_t... Is>
  void initialize( std::index_sequence<Is...> ) {
    ( decode<Is>(), ... );
  }

  template <std::size_t Idx>
  void decode() {
    this->getFunctorFactory().register_functor( this, std::get<Idx>( m_functors ), m_properties.template get<Idx>() );
  }

  // Storage for the decoded functors
  FunctorsTuple m_functors;

  // Storage for the Gaudi::Property objects; helper handles duplicate names
  friend struct Functors::detail::property_holder<ThOr::FunctorDesc, Tags...>;
  Functors::detail::property_holder<ThOr::FunctorDesc, Tags...> m_properties{ this, ThOr::Defaults::ALL };

private:
  template <typename tag_t>
  constexpr static std::size_t tag_index_v = boost::mp11::mp_find<functor_tag_types, tag_t>::value;

public:
  // Expose the base class constructtors
  using Functors::detail::with_functor_factory<base_t>::with_functor_factory;

  StatusCode initialize() override {
    return Functors::detail::with_functor_factory<base_t>::initialize().andThen(
        [&] { initialize( std::index_sequence_for<Tags...>{} ); } );
  }

  /** @brief Get the decoded functor corresponding to the given tag type.
   *  @todo  Extend this to despatch to getFunctor() of the parent class if it
   *         exists and we didn't know the given TagType.
   */
  template <typename TagType>
  auto const& getFunctor() const {
    return std::get<tag_index_v<TagType>>( m_functors );
  }

  /** @brief Get a tuple of lvalue references to decoded functors.
   */
  template <typename... TagTypes>
  auto getFunctors() const {
    return std::tie( std::get<tag_index_v<TagTypes>>( m_functors )... );
  }

  template <typename TagType>
  bool is_noop() const {
    return m_properties.template get<tag_index_v<TagType>>() == ThOr::Defaults::ALL;
  }
};
