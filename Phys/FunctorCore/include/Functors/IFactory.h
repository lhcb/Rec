/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/extend_interfaces.h"

namespace Gaudi {
  class Algorithm;
} // namespace Gaudi

namespace Functors {
  /** @file  IFactory.h
   *  @brief Interface to the service that JIT-compiles functors or loads them from the cache.
   *
   *  This defines the Functors::IFactory interface.
   */

  /** @class IFactory
   *  @brief Interface for turning strings into Functor<Out(In)> instances.
   */
  struct IFactory : extend_interfaces<IInterface> {
  private:
    /**
     */
    /**
     * @brief internal implementation method to register an input and
     * output-type-agnostic std::unique_ptr<AnyFunctor> object with the
     * factory
     *
     * @param  do_copy      lambda created in register_functor, used to first
     *                      cast and then copy the created functor into its
     *                      registered destination location.
     * @param  functor_type string representation of the type of the functor
     * @param  desc         ThOr::FunctorDesc object holding the functor code
     *                      and "pretty" representation.
     */
    virtual void do_register( std::function<void( std::unique_ptr<Functors::AnyFunctor> )> do_copy,
                              std::string_view functor_type, ThOr::FunctorDesc const& desc,
                              Gaudi::Algorithm const* owner ) = 0;

  public:
    DeclareInterfaceID( IFactory, 2, 1 );

    /** Factory method to register a C++ functor object to be created by this service.
     *
     * @param  owner   The algorithm that owns the functor, this is needed to
     *                 set up the functor's data dependencies correctly.
     * @param  functor Functor of type FType that will be set by in
     *                 the FunctorFactories start() call.
     *                 Note: The functor is not usable until after Factory->start()!!
     * @param  desc    ThOr::FunctorDesc object holding the functor code
     *                 and "pretty" representation.
     */
    template <typename FType>
    void register_functor( Gaudi::Algorithm* owner, FType& functor, ThOr::FunctorDesc const& desc ) {

      // This lambda is a helper to perform the actual initalization of the
      // algorithms' functor given the created AnyFunctor ptr from the
      // FunctorFactory. It does 2 things:
      // 1. It will remember the actual concrete type, e.g.
      //    Functors::Functor<bool()>, we need to cast the pointer to before we
      //    can perform the copy. Otherwise we wold run into the classic object
      //    slicing problem as we only invoke the base class' move constructor
      // 2. Remember the actual address  we need to copy into (functor&).
      // Note: we take ownership of the passed in pointer which is important because we
      // are going to move the guts of the passed in object into the registered
      // algorithm's functor
      auto do_copy = [owner, &functor]( std::unique_ptr<Functors::AnyFunctor> b ) {
        auto ftype_ptr = dynamic_cast<FType*>( b.get() ); // cast AnyFunctor* -> FType* (base -> derived)

        if ( !ftype_ptr ) {
          // This should only happen if you have a bug (e.g. you used a
          // SIMDWrapper type that has a different meaning depending on the
          // compilation flags in the stack/cling). We can't fix that at
          // runtime so let's just fail hard.
          throw GaudiException{ "Failed to cast factory return type (" +
                                    System::typeinfoName( typeid( decltype( *b ) ) ) + ") to desired type (" +
                                    System::typeinfoName( typeid( FType ) ) + "), rtype is (" +
                                    System::typeinfoName( b->rtype() ) + ") ",
                                "Functors::IFactory::register_functor( owner, functor, desc)", StatusCode::FAILURE };
        }
        functor = std::move( *ftype_ptr );
        functor.bind( owner );
      };
      do_register( do_copy, System::typeinfoName( typeid( FType ) ), desc, owner );
    }
  };
} // namespace Functors
