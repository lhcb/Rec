/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include <algorithm>
#include <tuple>

namespace Functors::Examples {
  constexpr auto TimesTwo = TrivialFunctor{ "TimesTwo", []( auto const& value ) { return value * 2; } };

  struct PlusN : public Function {
    PlusN( int n ) : N( n ) {}
    template <typename Data>
    auto operator()( Data const& value ) const {
      return value + N;
    }
    int N{ 0 };
  };

  struct GreaterThan : public Predicate {
    int m_v;
    GreaterThan( int v ) : m_v( v ) {}
    template <typename Data>
    bool operator()( Data const& value ) const {
      return value > m_v;
    }
  };

  // for reference see https://www.youtube.com/watch?v=-JPkvO1e1Gw
  constexpr auto ThorBeatsLoki = TrivialPredicate{ "ThorBeatsLoKi", []( auto const&... ) { return true; } };

  template <typename T>
  struct OptReturn : public Function {
    OptReturn( T n ) : m_n( n ) {}
    auto operator()( bool return_opt ) const { return return_opt ? Functors::Optional{ m_n } : std::nullopt; }
    T    m_n{};
  };

  // mainly to test the bind logic
  constexpr auto AddInputs = TrivialFunctor{ "AddInputs", []( auto const&... input ) { return ( input + ... ); } };

} // namespace Functors::Examples
