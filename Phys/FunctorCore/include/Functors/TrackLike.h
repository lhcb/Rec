/*****************************************************************************\
* (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Core/FloatComparison.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/Bremsstrahlung.h"
#include "Event/ProtoParticle.h"
#include "Event/StateParameters.h"
#include "Event/TrackEnums.h"
#include "Event/Track_v1.h"
#include "Event/Track_v3.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/HitPattern.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include <GaudiAlg/GaudiTupleAlg.h>

#include <string>
#include <type_traits>

/** @file  TrackLike.h
 *  @brief Definitions of functors for track-like objects.
 */

/** @namespace Functors::Track
 *
 *  This defines some basic functors that operate on track-like, or maybe more
 *  accurately charged-particle-like, objects. Both predicates and functions
 *  are defined in the same file for the moment.
 */

namespace Functors::Track::detail {
  template <typename T>
  auto chi2PerDoF( T const& x ) -> decltype( x.chi2PerDof() ) {
    return x.chi2PerDof();
  }
  template <typename T>
  auto chi2PerDoF( T const& x ) -> decltype( x.chi2() / x.nDoF() ) {
    return x.chi2() / x.nDoF();
  }

  /*
   * Helper for ProbNN
   */
  enum struct Pid { electron, muon, pion, kaon, proton, deuteron, ghost };

  template <typename StatePos_t, typename StateDir_t, typename VertexPos_t>
  [[gnu::always_inline]] inline auto impactParameterSquared( StatePos_t const& state_pos, StateDir_t const& state_dir,
                                                             VertexPos_t const& vertex ) {
    // TODO: handle one track, multiple vertices case..
    return ( state_pos - StatePos_t{ vertex } ).Cross( state_dir ).mag2() / state_dir.mag2();
  }

  /**
   * @brief helper for Extrapolate functor
   */
  class ITrackExtrapolatorHolder {

    class Geometry {
      const void* m_geom                                                                     = nullptr;
      const LHCb::DetDesc::ConditionContext& ( *m_cond )( void const*, EventContext const& ) = nullptr;
      MsgStream& ( *m_get_warning )( void const* )                                           = nullptr;
      LHCb::DetDesc::ConditionAccessor<DetectorElement> m_det;

    public:
      template <typename T>
      Geometry( T* ptr, const std::string& geom_name )
          : m_geom{ ptr }
          , m_cond{ []( const void* geom, const EventContext& ctx ) -> decltype( auto ) {
            return static_cast<T const*>( geom )->getConditionContext( ctx );
          } }
          , m_get_warning{ []( const void* geom ) -> decltype( auto ) {
            return static_cast<T const*>( geom )->warning();
          } }
          , m_det( ptr, geom_name ) {}

      MsgStream& warning() const { return ( *m_get_warning )( m_geom ); }

      auto* geometry( EventContext const& ctx ) const { return m_det.get( ( *m_cond )( m_geom, ctx ) ).geometry(); }
    };

    std::optional<Geometry>                       m_det;
    double                                        m_z;
    std::string                                   m_tool_name;
    std::string                                   m_geom_name;
    std::optional<ToolHandle<ITrackExtrapolator>> m_extrapolator;

  public:
    ITrackExtrapolatorHolder( double z, std::string tool_name, std::string geom_name = LHCb::standard_geometry_top )
        : m_z( z ), m_tool_name{ std::move( tool_name ) }, m_geom_name{ std::move( geom_name ) } {}

    ITrackExtrapolatorHolder( ITrackExtrapolatorHolder&& rhs )
        : m_z( rhs.m_z ), m_tool_name{ std::move( rhs.m_tool_name ) }, m_geom_name{ std::move( rhs.m_geom_name ) } {}

    ITrackExtrapolatorHolder& operator=( ITrackExtrapolatorHolder&& rhs ) {
      m_z         = rhs.m_z;
      m_tool_name = std::move( rhs.m_tool_name );
      m_geom_name = std::move( rhs.m_geom_name );
      return *this;
    }

    void emplace( TopLevelInfo& top_level ) {
      m_extrapolator.emplace( m_tool_name, top_level.algorithm() );

      auto try_emplace = [&]( auto* det ) {
        if ( det ) {
          m_det.emplace( det, m_geom_name );
          m_extrapolator->retrieve().ignore();
        }
        return det;
      };

      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<>*>( top_level.algorithm() ) ) ) return;
      if ( try_emplace( dynamic_cast<LHCb::DetDesc::AlgorithmWithCondition<GaudiTupleAlg>*>( top_level.algorithm() ) ) )
        return;

      throw GaudiException{ "TrackExtrapolator::bind", "attempting to bind an alorithm which is not a condition holder",
                            StatusCode::FAILURE };
    }
    auto prepare( EventContext const& ctx ) const {
      assert( m_extrapolator.has_value() );
      assert( m_det.has_value() );
      if ( !m_extrapolator->get() ) { m_det.value().warning() << "retrieval of extrapolator too late!!!" << endmsg; }
      return [tool = m_extrapolator->get(), geom = m_det->geometry( ctx ), det = LHCb::get_pointer( m_det ),
              z_pos = m_z]( auto const& tr ) {
        LHCb::State aState;
        auto const* track = &Sel::Utils::deref_if_ptr( tr );
        if constexpr ( std::is_convertible_v<LHCb::Track const*, decltype( track )> ) {
          tool->propagate( *track, z_pos, aState, Sel::Utils::deref_if_ptr( geom ) ).ignore();
          return aState;
        } else {
          det->warning() << " requested to use ITrackExtrapolator, but that does not support the argument -- "
                         << System::typeinfoName( typeid( decltype( track ) ) ) << " " << endmsg;
          return std::nullopt;
        }
      };
    }
  };

} // namespace Functors::Track::detail

namespace Functors::Track {

  /** @brief Retrieve state of track extrapolated to given z
   */
  struct Extrapolate : public Function {
    Extrapolate( float z, std::string tool_name = "TrackMasterExtrapolator" )
        : m_extrapolator{ z, std::move( tool_name ) } {}

    void bind( TopLevelInfo& top_level ) { m_extrapolator.emplace( top_level ); }

    auto prepare( EventContext const& evtCtx, TopLevelInfo const& ) const { return m_extrapolator.prepare( evtCtx ); }

  private:
    detail::ITrackExtrapolatorHolder m_extrapolator;
  };

  /**
   * @brief Retrieve const Container with pointers to all the states
   */
  constexpr auto States =
      GenericFunctor{ "States", []( auto const& d ) -> decltype( d.states() ) { return d.states(); } };

  /**
   * @brief Access state on v1 or v3 track by location.
   *
   */
  struct StateAt : public Function {

    static constexpr auto name() { return "StateAt"; }

    StateAt( LHCb::Event::Enum::State::Location location ) : m_loc{ location } {}
    StateAt( std::string location ) { parse( m_loc, location ).orThrow( "Unknown Location", __PRETTY_FUNCTION__ ); }

    auto operator()( const LHCb::Event::v1::Track& track ) const {
      const auto* state = track.stateAt( m_loc );
      return state ? Functors::Optional{ *state } : std::nullopt; // FIXME: an optional pointer is a semantic stutter
    }

    template <typename Track>
    auto operator()( const Track& track ) const
      requires requires( const Track& t, LHCb::Event::Enum::State::Location l ) {
        t.has_state( l );
        t.state( l );
      }
    {
      return track.has_state( m_loc ) ? Functors::Optional{ track.state( m_loc ) } : std::nullopt;
    }

    auto operator()( const LHCb::Event::v1::Track* track ) const { return ( *this )( *track ); }

  private:
    LHCb::Event::Enum::State::Location m_loc{};
  };

  /**
   * @brief Access LHCbIDs of track.
   *
   */
  constexpr auto LHCbIDs =
      GenericFunctor{ "LHCbIDs", []( auto const& d ) -> decltype( d.lhcbIDs() ) { return d.lhcbIDs(); } };

  /**
   * @brief Access HitPattern of LHCbIDs.
   *
   */
  constexpr auto HitPattern = TrivialFunctor{ "HitPattern", []( auto const& d ) { return LHCb::HitPattern( d ); } };

  /**
   * @brief Access number of A-side VP hits on HitPattern.
   *
   */
  constexpr auto nVPHitsA =
      chain( Functors::TrivialFunctor{ "HitPattern::numVeloA", &LHCb::HitPattern::numVeloA }, HitPattern, LHCbIDs );

  /**
   * @brief Access number of c-side VP hits on HitPattern.
   *
   */
  constexpr auto nVPHitsC =
      chain( Functors::TrivialFunctor{ "HitPattern::numVeloC", &LHCb::HitPattern::numVeloC }, HitPattern, LHCbIDs );

  /**
   * @brief Acces number of overlap VP hits on HitPattern.
   *
   */
  constexpr auto nVPOverlap = chain(
      Functors::TrivialFunctor{ "HitPattern::numVeloStationsOverlap", &LHCb::HitPattern::numVeloStationsOverlap },
      HitPattern, LHCbIDs );

  /**
   * @brief referencePoint, as defined by the referencePoint() accessor.
   * @note referencePoint is the position at which the object has its threeMomentum
   * defined
   */
  constexpr auto ReferencePoint = TrivialFunctor{ "ReferencePoint", []( auto const& d ) {
                                                   using LHCb::Event::referencePoint;
                                                   return referencePoint( d );
                                                 } };

  /**
   * @brief The TrackState is the first state on the track.
   *
   */
  constexpr auto TrackState = TrivialFunctor{ "TrackState", []( auto const& track ) {
                                               using LHCb::Event::trackState;
                                               return trackState( track );
                                             } };

  /** @brief slopes vector, as defined by the slopes() accessor.
   */
  constexpr auto Slopes = TrivialFunctor{ "Slopes", []( auto const& d ) {
                                           using LHCb::Event::slopes;
                                           return slopes( d );
                                         } };

  /** @brief FourMomentum vector e.g. (px, py, pz, E)
   */
  constexpr auto FourMomentum = TrivialFunctor{ "FourMomentum", []( auto const& d ) {
                                                 using LHCb::Event::fourMomentum;
                                                 return fourMomentum( d );
                                               } };

  /** @brief ThreeMomentum vector e.g. (px, py, pz)
   */
  constexpr auto ThreeMomentum = TrivialFunctor{ "ThreeMomentum", []( auto const& d ) {
                                                  using LHCb::Event::threeMomentum;
                                                  return threeMomentum( d );
                                                } };

  /** @brief Return covariance matrix of input
   */
  constexpr auto Covariance =
      GenericFunctor{ "Covariance", []( auto const& d ) -> decltype( d.covariance() ) { return d.covariance(); } };

  /** @brief Charge, as defined by the charge() accessor.
   */
  constexpr auto Charge =
      GenericFunctor{ "Charge", []( auto const& d ) -> decltype( d.charge() ) { return d.charge(); } };

  /** @brief HasBremAdded, as defined by the HasBremAdded accessor.
   */
  struct HasBremAdded : Predicate {
    auto operator()( LHCb::Particle const& p ) const {
      return !LHCb::essentiallyZero( p.info( LHCb::Particle::additionalInfo::HasBremAdded, 0 ) );
    }

    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
  };

  /// @brief InfoFn gets ExtraInfo if available
  template <typename T, bool just_deref = true>
    requires requires( T const& p ) {
      p.extraInfo();
      typename T::additionalInfo;
    }
  class InfoFn : public Function {
    T::additionalInfo m_info;

  public:
    constexpr InfoFn( T::additionalInfo info ) : m_info{ info } {}

    auto operator()( T const& p ) const {
      auto const& info = p.extraInfo();
      auto        i    = info.find( m_info );
      return i != info.end() ? Functors::Optional{ i->second } : std::nullopt;
    }

    // note: slightly complicated so that this also deals with `SmartRef`...
    template <typename U>
    auto operator()( U const& p ) const -> decltype( ( *this )( *p ) ) {
      if constexpr ( just_deref ) {
        if ( !p ) throw GaudiException( "trying to dereference nullptr", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        return ( *this )( *p );
      } else {
        return Functors::and_then( p, *this );
      }
    }
  };
  /// @brief VeloMatchChi2, as defined by the VeloMatchChi2 accessor.
  constexpr auto VeloMatchChi2 = InfoFn<LHCb::Particle, true>{ LHCb::Particle::additionalInfo::VeloMatchChi2 };
  /// @brief VeloMatchIP, as defined by the VeloMatchIP accessor.
  constexpr auto VeloMatchIP = InfoFn<LHCb::Particle, true>{ LHCb::Particle::additionalInfo::VeloMatchIP };

  /** @brief Number of degrees of freedom, as defined by the nDoF accessor.
   */
  constexpr auto nDoF = GenericFunctor{ "nDoF", []( auto const& d ) -> decltype( d.nDoF() ) { return d.nDoF(); } };

  /** @brief Get tracks from vertex fit
   */
  struct PVtracks : public Function {
    auto operator()( LHCb::RecVertex const* vtx ) const {
      static const SmartRefVector<LHCb::Track> s_empty = {};
      struct Wrapper {
        const SmartRefVector<LHCb::Track>* ptr = nullptr;
        auto                               begin() const { return ptr ? ptr->begin() : s_empty.begin(); }
        auto                               end() const { return ptr ? ptr->end() : s_empty.end(); }
        auto                               size() const { return ptr ? ptr->size() : 0; }
        bool                               empty() const { return !ptr || ptr->empty(); }
        explicit operator bool() const { return ptr != nullptr; }
      };
      return Wrapper{ vtx ? &vtx->tracks() : nullptr };
    }

    auto operator()( LHCb::VertexBase const* vertex ) const {
      return ( *this )( dynamic_cast<LHCb::RecVertex const*>( vertex ) );
    }
  };

  /** @brief chi^2/d.o.f., as defined by the chi2PerDoF accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  constexpr auto Chi2PerDoF = GenericFunctor{
      "Chi2PerDoF", []( auto const& d ) -> decltype( detail::chi2PerDoF( d ) ) { return detail::chi2PerDoF( d ); },
      []( LHCb::Particle const& d ) {
        auto pp  = d.proto();
        auto trk = ( pp ? pp->track() : nullptr );
        auto ev  = d.endVertex();
        return trk  ? Functors::Optional{ detail::chi2PerDoF( *trk ) }
               : ev ? Functors::Optional{ detail::chi2PerDoF( *ev ) }
                    : std::nullopt;
      } };

  /** @brief chi^2, as defined by the chi2 accessor.
   *
   * If the input is a legacy LHCb::Particle with a track, the track object's
   * accessor is used. If a track is not present but a vertex is the vertex's
   * accessor is used.
   */
  constexpr auto Chi2 = GenericFunctor{
      "Chi2",
      []( LHCb::Particle const& d ) {
        auto pp  = d.proto();
        auto trk = ( pp ) ? pp->track() : nullptr;
        auto ev  = d.endVertex();
        return trk ? Functors::Optional{ trk->chi2() } : ev ? Functors::Optional{ ev->chi2() } : std::nullopt;
      },
      []( auto const& d ) -> decltype( d.chi2() ) { return d.chi2(); } };

  /** @brief q/p, as defined by the qOverP accessor.
   */
  constexpr auto QoverP = GenericFunctor{
      "QoverP", []( const auto& t ) -> decltype( t.firstState().qOverP() ) { return t.firstState().qOverP(); },
      []( const auto& t ) -> decltype( t.qOverP() ) { return t.qOverP(); } };

  /** @brief Ghost probability, as defined by the ghostProbability accessor.
   */
  constexpr auto GhostProbability = GenericFunctor{
      "GhostProbability", []( auto const& d ) -> decltype( d.ghostProbability() ) { return d.ghostProbability(); } };

  class CombDLL : public Function {
    detail::Pid m_pid;

  public:
    constexpr CombDLL( detail::Pid pid ) : m_pid{ pid } {}

    template <typename T>
    auto operator()( const T& d ) const -> decltype( d.CombDLLe() ) {
      switch ( m_pid ) {
      case detail::Pid::electron:
        return d.CombDLLe();
      case detail::Pid::muon:
        return d.CombDLLmu();
      case detail::Pid::pion:
        return d.CombDLLpi();
      case detail::Pid::kaon:
        return d.CombDLLk();
      case detail::Pid::proton:
        return d.CombDLLp();
      case detail::Pid::deuteron:
        return d.CombDLLd();
      default:
        throw std::domain_error{ "impossible PID type" };
      }
      __builtin_unreachable();
    }

    template <typename T>
    auto operator()( const T* data ) const -> decltype( Functors::and_then( data, *this ) ) {
      return Functors::and_then( data, *this );
    }
    auto operator()( const LHCb::ProtoParticle& pp ) const {
      return Functors::and_then( pp.globalChargedPID(), *this );
    }
    auto operator()( const LHCb::Particle& p ) const { return Functors::and_then( p.proto(), *this ); }
  };

  /** @brief PIDmu, PIDp, PIDe, PIDk, PIDpi, as defined by the corresponding CombDLL variable
   */
  constexpr auto PIDmu = CombDLL{ detail::Pid::muon };
  constexpr auto PIDp  = CombDLL{ detail::Pid::proton };
  constexpr auto PIDe  = CombDLL{ detail::Pid::electron };
  constexpr auto PIDk  = CombDLL{ detail::Pid::kaon };
  constexpr auto PIDpi = CombDLL{ detail::Pid::pion };

  /** @brief ProbNN definition
   */
  class ProbNN : public Function {
    detail::Pid m_pid;

  public:
    constexpr ProbNN( detail::Pid pid ) : m_pid{ pid } {}

    auto operator()( const LHCb::GlobalChargedPID& d ) const {
      float prb = [&] {
        switch ( m_pid ) {
        case detail::Pid::electron:
          return d.ProbNNe();
        case detail::Pid::muon:
          return d.ProbNNmu();
        case detail::Pid::pion:
          return d.ProbNNpi();
        case detail::Pid::kaon:
          return d.ProbNNk();
        case detail::Pid::proton:
          return d.ProbNNp();
        case detail::Pid::deuteron:
          return d.ProbNNd();
        case detail::Pid::ghost:
          return d.ProbNNghost();
        }
        __builtin_unreachable();
      }();
      return prb != LHCb::GlobalChargedPID::DefaultProbNN ? Functors::Optional{ prb } : std::nullopt;
    }

    template <typename T>
    auto operator()( const T& d ) const -> decltype( d.template probNN<detail::Pid::electron>() ) {
      switch ( m_pid ) {
      case detail::Pid::electron:
        return d.template probNN<detail::Pid::electron>();
      case detail::Pid::muon:
        return d.template probNN<detail::Pid::muon>();
      case detail::Pid::pion:
        return d.template probNN<detail::Pid::pion>();
      case detail::Pid::kaon:
        return d.template probNN<detail::Pid::kaon>();
      case detail::Pid::proton:
        return d.template probNN<detail::Pid::proton>();
      case detail::Pid::deuteron:
        return d.template probNN<detail::Pid::deuteron>();
      case detail::Pid::ghost:
        return d.template probNN<detail::Pid::ghost>();
      default:
        throw std::domain_error{ "impossible PID type" };
      }
      __builtin_unreachable();
    }

    template <typename T>
    auto operator()( const T* data ) const -> decltype( Functors::and_then( data, *this ) ) {
      return Functors::and_then( data, *this );
    }
    auto operator()( const LHCb::Particle& p ) const {
      return Functors::and_then( p.proto(),
                                 static_cast<const LHCb::GlobalChargedPID* (LHCb::ProtoParticle::*)() const>(
                                     &LHCb::ProtoParticle::globalChargedPID ),
                                 *this );
    }
  };

  /** @brief The explicit definition for the probNN quantities
   */
  constexpr auto PROBNN_D     = ProbNN{ detail::Pid::deuteron };
  constexpr auto PROBNN_E     = ProbNN{ detail::Pid::electron };
  constexpr auto PROBNN_GHOST = ProbNN{ detail::Pid::ghost };
  constexpr auto PROBNN_K     = ProbNN{ detail::Pid::kaon };
  constexpr auto PROBNN_MU    = ProbNN{ detail::Pid::muon };
  constexpr auto PROBNN_PI    = ProbNN{ detail::Pid::pion };
  constexpr auto PROBNN_P     = ProbNN{ detail::Pid::proton };

  /** @brief nHits, as defined by the nHits accessor.
   */
  constexpr auto nHits = GenericFunctor{ "nHits", []( const auto& d ) -> decltype( d.nHits() ) { return d.nHits(); } };

  /** @brief number of VP hits
   */
  constexpr auto nVPHits =
      GenericFunctor{ "nVPHits", []( const auto& d ) -> decltype( d.nHits() ) { return d.nVPHits(); } };

  /** @brief number of UT hits
   */
  constexpr auto nUTHits =
      GenericFunctor{ "nUTHits", []( const auto& d ) -> decltype( d.nUTHits() ) { return d.nUTHits(); } };

  /** @brief number of FT hits
   */
  constexpr auto nFTHits =
      GenericFunctor{ "nFTHits", []( const auto& d ) -> decltype( d.nFTHits() ) { return d.nFTHits(); } };

  /** @brief Track history
   */
  constexpr auto History =
      GenericFunctor{ "History", []( const auto& d ) -> decltype( d.history() ) { return d.history(); } };

  /** @brief Track flag
   */
  constexpr auto Flag = GenericFunctor{ "Flag", []( const auto& d ) -> decltype( d.flag() ) { return d.flag(); } };

  class HasTrackFlag : public Predicate {
    LHCb::Event::Enum::Track::Flag m_f;

  public:
    constexpr HasTrackFlag( LHCb::Event::Enum::Track::Flag f ) : m_f{ f } {}
    static constexpr auto name() { return "HasTrackFlag"; }

    template <typename Data>
    bool operator()( Data const& d ) const
      requires requires( Data const& d, LHCb::Event::Enum::Track::Flag f ) {
        Sel::Utils::deref_if_ptr( d ).checkFlag( f );
      }
    {
      return Sel::Utils::deref_if_ptr( d ).checkFlag( m_f );
    }
  };
  constexpr auto IsClone    = HasTrackFlag{ LHCb::Event::Enum::Track::Flag::Clone };
  constexpr auto IsSelected = HasTrackFlag{ LHCb::Event::Enum::Track::Flag::Selected };
  constexpr auto IsInvalid  = HasTrackFlag{ LHCb::Event::Enum::Track::Flag::Invalid };

  /** @brief Track type
   */
  constexpr auto Type = GenericFunctor{ "Type", []( const auto& d ) -> decltype( d.type() ) { return d.type(); } };

  constexpr auto IsLong         = ( Type == LHCb::Event::Enum::Track::Type::Long );
  constexpr auto IsDownstream   = ( Type == LHCb::Event::Enum::Track::Type::Downstream );
  constexpr auto IsUpstream     = ( Type == LHCb::Event::Enum::Track::Type::Upstream );
  constexpr auto IsTtrack       = ( Type == LHCb::Event::Enum::Track::Type::Ttrack );
  constexpr auto IsVelo         = ( Type == LHCb::Event::Enum::Track::Type::Velo );
  constexpr auto IsVeloBackward = ( Type == LHCb::Event::Enum::Track::Type::VeloBackward );

  /** @brief Track hasT, the input of this functor must be the track type
   */
  constexpr auto HasT = TrivialPredicate{ "HasT", []( LHCb::Event::Enum::Track::Type t ) { return hasT( t ); } };

  /** @brief Track hasUT, the input of this functor must be the track type
   */
  constexpr auto HasUT = TrivialPredicate{ "HasUT", []( LHCb::Event::Enum::Track::Type t ) { return hasUT( t ); } };

  /** @brief Track hasVelo, the input of this functor must be the track type
   */
  constexpr auto HasVelo =
      TrivialPredicate{ "HasVelo", []( LHCb::Event::Enum::Track::Type t ) { return hasVelo( t ); } };

  /** @brief Number of expected Velo clusters from VELO 3D pattern recognition
   */

  constexpr auto nPRVelo3DExpect = GenericFunctor{
      "nPRVelo3DExpect",
      []( auto const& d ) -> decltype( static_cast<int>( std::nearbyint(
                              d.info( LHCb::Event::Enum::Track::AdditionalInfo::nPRVelo3DExpect, -1. ) ) ) ) {
        return static_cast<int>(
            std::nearbyint( d.info( LHCb::Event::Enum::Track::AdditionalInfo::nPRVelo3DExpect, -1. ) ) );
      } };

  /**
   * @brief MC_Reconstructed, return the reconstructed category
   * for MC Particle. The input of this functor must be reconstructed
   * object, such as LHCb::ProtoParticle and LHCb::Particle.
   *
   * For possible values of IMCReconstructed::RecCategory, see IMCReconstructed.h
   *
   */
  namespace detail {
    template <typename T>
    IMCReconstructed::RecCategory get_reconstructed_category( const T* rec )
      requires requires {
        rec->charge();
        rec->track();
      }
    {
      if ( !rec ) return IMCReconstructed::RecCategory::NotReconstructed;
      /// Neutral particle
      if ( !rec->charge() ) return IMCReconstructed::RecCategory::Neutral;
      /// Charged
      const auto* track = rec->track();
      if ( track && !track->checkFlag( LHCb::Track::Flags::Clone ) ) {
        switch ( track->type() ) {
        case LHCb::Event::Enum::Track::Type::Long:
          return IMCReconstructed::RecCategory::ChargedLong;
        case LHCb::Event::Enum::Track::Type::Downstream:
          return IMCReconstructed::RecCategory::ChargedDownstream;
        case LHCb::Event::Enum::Track::Type::Upstream:
          return IMCReconstructed::RecCategory::ChargedUpstream;
        case LHCb::Event::Enum::Track::Type::Unknown:
          return IMCReconstructed::RecCategory::CatUnknown;
        case LHCb::Event::Enum::Track::Type::Velo:
          return IMCReconstructed::RecCategory::ChargedVelo;
        case LHCb::Event::Enum::Track::Type::Ttrack:
          return IMCReconstructed::RecCategory::ChargedTtrack;
        case LHCb::Event::Enum::Track::Type::Muon:
          return IMCReconstructed::RecCategory::ChargedMuon;
        default: /* empty on purpose */;
        }
      }
      return IMCReconstructed::RecCategory::NotReconstructed;
    }
  } // namespace detail

  constexpr auto MC_Reconstructed =
      GenericFunctor{ "MC_Reconstructed",
                      []( LHCb::Particle const& d ) -> IMCReconstructed::RecCategory {
                        return detail::get_reconstructed_category( d.proto() );
                      },
                      []( auto const& d ) -> decltype( detail::get_reconstructed_category( &d ) ) {
                        return detail::get_reconstructed_category( &d );
                      } };

  /** @brief Wrapper around Particle/ChargedBasic/Track to access brem corrected information
   */
  constexpr auto Bremsstrahlung = GenericFunctor{
      "Bremsstrahlung", []<typename Data>( Data&& d ) requires( std::is_lvalue_reference_v<Data> &&
                                                                !std::is_pointer_v<std::remove_cvref_t<Data>> ){
                            return LHCb::Event::Bremsstrahlung::BremsstrahlungWrapper<std::remove_cvref_t<Data>>{ d };
} // namespace Functors::Track
}
;

/** @brief Vector of ADC values of UT clusters on track
 */
constexpr auto UTHitADCs = GenericFunctor{ "UTHitADCs", []( const LHCb::Event::v1::Track& track ) {
                                            // get the UT hits on the track
                                            const auto&               cls = track.utClusters();
                                            std::vector<unsigned int> adcs;
                                            adcs.reserve( cls.size() );
                                            std::transform( cls.begin(), cls.end(), std::back_inserter( adcs ),
                                                            []( const auto& cl ) { return cl.clusterCharge(); } );
                                            return adcs;
                                          } };

/** @brief Vector of sizes of UT clusters on track
 */
constexpr auto UTHitSizes = GenericFunctor{ "UTHitSizes", []( const LHCb::Event::v1::Track& track ) {
                                             // get the UT hits on the track
                                             const auto&               cls = track.utClusters();
                                             std::vector<unsigned int> sizes;
                                             sizes.reserve( cls.size() );
                                             std::transform( cls.begin(), cls.end(), std::back_inserter( sizes ),
                                                             []( const auto& cl ) { return cl.size(); } );
                                             return sizes;
                                           } };

} // namespace Functors::Track
