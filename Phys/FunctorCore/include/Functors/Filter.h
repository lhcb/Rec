/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/SOAZip.h"
#include "Functors/Function.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/NamedRange.h"
#include "GaudiKernel/SharedObjectsContainer.h"
#include "PrKernel/PrSelection.h"

namespace LHCb::Event::PV {
  struct PrimaryVertexContainer;
}

/** @file  Filter.h
 *  @brief Wrapper type providing container-wise operations.
 */
namespace Functors {
  namespace detail {
    template <SIMDWrapper::InstructionSet simd>
    static void check_simd( TopLevelInfo const& top_level ) {
      // Print a warning if the requested SIMD level (simd) corresponds to a
      // different *actual* SIMD level. Typically this happens when
      // JIT-compiling functors with Cling, which normally does not have
      // vector backends enabled, even if the application was built with them.
      // In this case the requested level (e.g. Best) might mean AVX2 in the
      // stack build but Scalar inside Cling.
      auto current_meaning = SIMDWrapper::type_map<simd>::instructionSet();
      auto stack_meaning   = SIMDWrapper::type_map<simd>::stackInstructionSet();
      if ( current_meaning != stack_meaning ) {
        top_level.Warning(
            "Stack:" + SIMDWrapper::instructionSetName( stack_meaning ) +
            ", Functor:" + SIMDWrapper::instructionSetName( current_meaning ) + ", instruction set mismatch (" +
            SIMDWrapper::instructionSetName( simd ) +
            " requested). ROOT/cling was not compiled with the same options as the stack, try the functor cache" );
      }
    }
    /** @brief Conditionally copy an std::vector.
     */
    template <typename T, std::invocable<T> F, typename A>
    std::vector<T, A> filter( F&& f, TopLevelInfo const&, std::vector<T, A> const& input ) {
      using LHCb::Utils::as_arithmetic;
      std::vector<T, A> out;
      out.reserve( input.size() );
      std::copy_if( input.begin(), input.end(), std::back_inserter( out ),
                    [&f]( auto const& x ) { return as_arithmetic( std::invoke( f, x ) ); } );
      return out;
    }

    /** @brief For PrimaryVertexContainer, use Pr::Selection. In fact, we could use this for vector as well */
    template <typename F>
    auto filter( F&& f, TopLevelInfo const&, LHCb::Event::PV::PrimaryVertexContainer const& input ) {
      return Pr::Selection{ LHCb::make_span( input.begin(), input.end() ), f };
    }

    /** @brief Filter a view of objects.
     *
     * The canonical instantiation of this template is when T =
     * LHCb::Particle. The filter then accepts some container of T in to a
     * non-owning view.
     */
    template <typename T, std::invocable<T const*> F>
    SharedObjectsContainer<T> filter( F&&                                              f, TopLevelInfo const&,
                                      Gaudi::NamedRange_<std::vector<const T*>> const& input ) {
      using LHCb::Utils::as_arithmetic;
      return { input.begin(), input.end(),
               [&f]( const auto* obj ) { return as_arithmetic( std::invoke( f, *obj ) ); } };
    }

    /** @brief Filter a particle combination
     */
    template <typename T, std::invocable<T const*> F, std::same_as<T>... Ts>
    SharedObjectsContainer<T> filter( F&& f, TopLevelInfo const&, LHCb::ParticleCombination<T, Ts...> const& input ) {
      using LHCb::Utils::as_arithmetic;
      return { input.begin(), input.end(),
               [&f]( const auto* obj ) { return as_arithmetic( std::invoke( f, *obj ) ); } };
    }

    /** @brief Filter a container is LHCb::Event::make_zip(...)-able
     *
     *  This overload only participates if LHCb::Event::make_zip( cont )
     *  is valid, so far this typically means LHCb::Event::SOACollection
     *  derived containers. During the transition period where both zip
     *  implementations exist, prefer the old one.
     */
    template <typename F, typename Container>
      requires LHCb::Event::is_zippable_v<Container>
    auto filter( F&& f, TopLevelInfo const& top_level, Container const& container ) {
      // Try and give an informative error message if we failed to use the same
      // instruction set in the stack compilation and with cling. Explicitly
      // use 'best' (even though it's the default)
      check_simd<SIMDWrapper::Best>( top_level );

      // Filter the container using the prepared functor
      return LHCb::Event::make_zip( container ).filter( std::forward<F>( f ) );
    }

    /** @brief Filter a LHCb::Event::Zip<A, B, Containers...> container
     */
    template <typename F, SIMDWrapper::InstructionSet def_simd, typename... PrTracks>
    auto filter( F&& f, TopLevelInfo const& top_level, LHCb::Event::Zip<def_simd, PrTracks...> const& input ) {
      // In this case the input type has a default SIMD size. We should respect
      // that, but try and give an informative error message in case that
      // setting means something different in the stack and inside Cling.
      check_simd<def_simd>( top_level );

      // Filter the container using the prepared functor
      return input.filter( std::forward<F>( f ) );
    }

    /** @brief Refine a Pr::Selection.
     */
    template <typename F, typename T>
    Pr::Selection<T> filter( F&& f, TopLevelInfo const&, Pr::Selection<T> const& input ) {
      return input.select( std::forward<F>( f ) );
    }

  } // namespace detail

  /** @class Filter
   *  @brief Functor that adapts a predicate to filter a container.
   *
   *  This defines the boilerplate for the different possible input container
   *  types. So far there are implementations for:
   *  @li Refining a Pr::Selection
   *  @li Copying a subset of an STL container, e.g. @p std::vector<T>
   *  @li Using the LHCb::Event::Zip family of types.
   */
  template <detail::FunctorPredicate F>
  struct Filter : public Function {
    Filter( F f ) : m_f{ std::move( f ) } {}

    /** Forward to the contained functor
     */
    void bind( TopLevelInfo& top_level ) { detail::bind( m_f, top_level ); }

    /** Prepare the contained functor and bake it into a new lambda that can
     *  filter containers using the pre-prepared functor.
     */
    auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const {
      return [&top_level, f = detail::prepared( m_f, evtCtx, top_level )]( auto const&... input ) {
        return detail::filter( f, top_level, input... );
      };
    }

  private:
    F m_f;
  };

  /** @brief Get the result type of filtering the given container.
   *
   *  Helper to get the type returned when filtering a container of type T with
   *  the ALL functor. This is useful for algorithms that need to deduce what
   *  Functor<Out( In )> type they should use when filtering some known input
   *  type.
   */
  template <typename T>
  using filtered_t = std::decay_t<decltype( Filter{ AcceptAll }.prepare(
      std::declval<EventContext>(), std::declval<TopLevelInfo>() )( std::declval<T>() ) )>;
} // namespace Functors
