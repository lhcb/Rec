/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/HltDecReports.h"
#include "Event/ODIN.h"
#include "Event/RecSummary.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/SynchronizedValue.h"
#include "SelKernel/Utilities.h"

// This is very important, otherwise bind() can give a **very** confusing error message
#include "Event/PrHits.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/ObjectContainerBase.h"

#include <string>
#include <type_traits>
#include <utility>

namespace Functors::detail {
  constexpr auto SizeOf =
      TrivialFunctor{ "TES::SizeOf", []( const auto& d ) -> decltype( d.size() ) { return d.size(); },
                      []( const DataObject& d ) -> int {
                        if ( auto container = dynamic_cast<ObjectContainerBase const*>( &d ); container ) {
                          return container->numberOfObjects();
                        }
                        if ( auto anydata = dynamic_cast<AnyDataWrapperBase const*>( &d ); anydata ) {
                          return anydata->size().value_or( -1 );
                        }
                        return -1;
                      } };

  template <typename Predicate>
  class DecReportsCachedFilter {
    Predicate                                                             m_predicate;
    mutable LHCb::cxx::SynchronizedValue<std::map<int, std::vector<int>>> m_cache;

    LHCb::span<int const> indices( LHCb::HltDecReports const& dec ) const {
      return m_cache.with_lock( [&]( std::map<int, std::vector<int>>& m ) {
        auto tck = dec.configuredTCK();
        auto i   = m.find( tck );
        if ( i != m.end() ) return LHCb::span<int const>( i->second ); // FAST PATH
        std::vector<int> idx;
        idx.reserve( dec.size() );
        for ( const auto& [i, l] : LHCb::range::enumerate( dec ) ) {
          if ( m_predicate( l.first ) ) idx.push_back( i );
        }
        auto r = m.emplace( tck, std::move( idx ) );
        assert( r.second );
        return LHCb::span<int const>( r.first->second );
      } );
    }

  public:
    DecReportsCachedFilter( Predicate pred ) : m_predicate{ std::move( pred ) } {}
    template <typename UnaryPredicate>
    bool operator()( LHCb::HltDecReports const& dec, UnaryPredicate&& unary ) const {
      int tck = dec.configuredTCK();
      if ( tck == 0 ) {
        return std::any_of( dec.begin(), dec.end(),
                            [&]( const auto& p ) { return std::invoke( unary, p.second ) && m_predicate( p.first ); } );
      }
      auto const& idx = indices( dec );
      return std::any_of( idx.begin(), idx.end(), [&unary, begin = dec.begin()]( int i ) {
        return std::invoke( unary, std::next( begin, i )->second );
      } );
    }
  };

} // namespace Functors::detail

/** @file  TES.h
 *  @brief Functors that implement basic TES interactions.
 */
namespace Functors::TES {
  /** @brief Get run number from ODIN */
  constexpr auto RunNumber = Functors::TrivialFunctor{ "Odin::runNumber", &LHCb::ODIN::runNumber };

  /** @brief Get event number from ODIN */
  constexpr auto EventNumber = Functors::TrivialFunctor{ "Odin::eventNumber", &LHCb::ODIN::eventNumber };

  /** @brief Get event type from ODIN */
  constexpr auto EventType = Functors::TrivialFunctor{ "Odin::eventType", &LHCb::ODIN::eventType };

  /** @brief Get the bunch crossing ID from ODIN. */
  constexpr auto BunchCrossingID = Functors::TrivialFunctor{ "Odin::bunchId", &LHCb::ODIN::bunchId };

  /** @brief Get the bunch crossing type from ODIN. */
  constexpr auto BunchCrossingType =
      Functors::TrivialFunctor{ "Odin::bunchCrossingType", &LHCb::ODIN::bunchCrossingType };

  /** @brief Get the Odin TCK from ODIN. */
  constexpr auto OdinTCK =
      Functors::TrivialFunctor{ "Odin::triggerConfigurationKey", &LHCb::ODIN::triggerConfigurationKey };

  /** @brief Get the GPS time from ODIN. */
  constexpr auto GpsTime = Functors::TrivialFunctor{ "Odin::gpsTime", &LHCb::ODIN::gpsTime };

  /** @brief Get information from RecSummary object. */
  struct RecSummaryInfo : public Function {
    /** Make some error messages more informative. */
    static constexpr auto name() { return "RecSummaryInfo"; }

    RecSummaryInfo( LHCb::RecSummary::DataTypes info_type ) : m_info_type{ info_type } {}

    RecSummaryInfo( int info_type ) : m_info_type{ static_cast<LHCb::RecSummary::DataTypes>( info_type ) } {}

    auto operator()( LHCb::RecSummary const& rec_summary ) const {
      const auto& summaryData = rec_summary.summaryData();
      auto        i           = summaryData.find( m_info_type );
      return summaryData.end() == i ? std::nullopt : Functors::Optional{ i->second };
    }

  private:
    LHCb::RecSummary::DataTypes m_info_type;
  };

  /** @brief Get the trigger configuration key (TCK) from the DecReports*/
  constexpr auto SelectionTCK = TrivialFunctor{ "TES::SelectionTCK", &LHCb::HltDecReports::configuredTCK };

  /** @brief Get a dictionary containing decision of list of selections lines from DecReports. */
  class SelectionDecision : public Predicate {
  public:
    /** Make some error messages more informative. */
    static constexpr auto name() { return "SelectionDecision"; }

    /** Constructor */
    SelectionDecision( std::string line_name ) : m_line_name( std::move( line_name ) ) {
      // check that the line name ends with "Decision"
      if ( !detail::ends_with( m_line_name, "Decision" ) )
        throw GaudiException{ "The line name does not end with 'Decision'!", "Functors::TES::SelectionDecision",
                              StatusCode::FAILURE };
    }

    auto operator()( LHCb::HltDecReports const& dec ) const {
      return Functors::and_then( dec.decReport( m_line_name ),
                                 []( const auto& rep ) { return static_cast<bool>( rep.decision() ); } );
    }

  private:
    std::string m_line_name;
  };

  class DecReportsFilter : public Predicate {
    struct ListFilter {
      ListFilter( std::vector<std::string> lines ) : m_lines( std::move( lines ) ) {
        detail::require_suffix( m_lines, "Decision" );
      }

      bool operator()( std::string const& name ) const {
        return std::find( m_lines.begin(), m_lines.end(), name ) != m_lines.end();
      }

    private:
      std::vector<std::string> m_lines;
    };
    detail::DecReportsCachedFilter<ListFilter> m_idx;

  public:
    static constexpr auto name() { return "DecReportsFilter"; }

    DecReportsFilter( std::vector<std::string> lines ) : m_idx{ ListFilter{ std::move( lines ) } } {}

    bool operator()( LHCb::HltDecReports const& dec ) const { return m_idx( dec, &LHCb::HltDecReport::decision ); }
  };

  class DecReportsRegExFilter : public Predicate {
    struct RegexFilter {
      std::regex re;
      bool       operator()( std::string const& name ) const { return std::regex_match( name, re ); }
    };
    detail::DecReportsCachedFilter<RegexFilter> m_idx;

  public:
    static constexpr auto name() { return "DecReportsRegExFilter"; }

    DecReportsRegExFilter( std::string const& regex ) : m_idx{ RegexFilter{ std::regex{ regex } } } {}

    bool operator()( LHCb::HltDecReports const& dec ) const { return m_idx( dec, &LHCb::HltDecReport::decision ); }
  };

  /** @brief Key of the KeyedObject (i.e. LHCb::Particle or LHCb::MCParticle).
   * Could be used together with F.MAP_INPUT and MC association relations table to get the TRUEKEY from MC association*/
  // FIXME: use GenericFunctor, and deal with the fallout due to returning optional in case of a pointer argument
  constexpr auto ObjectKey =
      TrivialFunctor{ "ObjectKey", []( auto const& d ) -> decltype( d.key() ) { return d.key(); },
                      []( auto const& d ) -> decltype( d->key() ) { return d->key(); } };

  /* @brief Returns the number of hits in the muon system station, region, and quarter specified
   */
  class NHitsInMuon final : public Function {
    unsigned int ref_station;
    unsigned int ref_region;

  public:
    NHitsInMuon( unsigned int station, unsigned int region ) : ref_station( station ), ref_region( region ) {
      assert( station < 4 && region < 4 && "Station or region not suppported." );
    }

    int operator()( LHCb::Pr::Hits<LHCb::Pr::HitType::Muon> const& muon_hits ) const {
      auto const& hits = muon_hits.hits( ref_station );
      return std::count_if( hits.begin(), hits.end(),
                            [r = ref_region]( const auto& hit ) { return hit.region() == r; } );
    }
  };

} // namespace Functors::TES
