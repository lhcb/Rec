/*****************************************************************************\
 * (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/IDecayFinder.h"
#include "LoKi/IMCDecay.h"

/** @file  Decay.h
 *  @brief Definitions of functors related to the decay chain.
 */
namespace Functors::detail {
  // function that checks if the number of caret symbol ("^") in the decay descriptor is 0 or 1
  inline void checkCaret( std::string_view decay ) {
    auto nCaret = std::count( decay.begin(), decay.end(), '^' );
    if ( nCaret > 1 ) {
      throw GaudiException( fmt::format( "The decay descriptor '{}' contains more than one caret symbol ('^')", decay ),
                            "Decay", StatusCode::FAILURE );
    }
  }
} // namespace Functors::detail

namespace Functors::Decay {

  /** @brief Use the DecayFinder algorithm to get the information related to the decay chain. */
  struct FindDecay final : public Function {
    FindDecay( std::string decay_desc, std::string tool_name = "DecayFinder" )
        : m_decay_desc{ std::move( decay_desc ) }, m_tool_name{ std::move( tool_name ) } {
      Functors::detail::checkCaret( m_decay_desc );
    }

    void bind( TopLevelInfo& top_level ) { m_find_decay.emplace( m_tool_name, top_level.algorithm() ); }

    auto operator()( const LHCb::Particle& part ) const {
      assert( m_find_decay.has_value() );
      if ( !m_find_decay->get() ) m_find_decay->retrieve().ignore();
      auto tool = m_find_decay->get();
      // Make a ConstVector with particle as input.
      // This conversion is needed because decay finder expects a vector of particles.
      LHCb::Particle::ConstVector in_v = { &part };
      LHCb::Particle::ConstVector out_v;
      bool found_particle = tool->findDecay( m_decay_desc, in_v, out_v ).isSuccess() && ( out_v.size() == 1 );
      return found_particle ? Functors::Optional{ out_v.front() }
                            : std::nullopt; // FIXME: an optional pointer is a semantic stutter
    }

    auto operator()( const LHCb::Particle* part ) const { return Functors::and_then( part, *this ); }

  private:
    std::string                             m_decay_desc;
    std::string                             m_tool_name;
    std::optional<ToolHandle<IDecayFinder>> m_find_decay;
  };

  /** @brief Use the DecayFinder algorithm to get the information related to the decay chain. */
  struct FindMCDecay final : public Function {
    FindMCDecay( std::string decay_desc, std::string tool_name = "LoKi::MCDecay" )
        : m_decay_desc{ std::move( decay_desc ) }, m_tool_name{ std::move( tool_name ) } {
      Functors::detail::checkCaret( m_decay_desc );
    }

    void bind( TopLevelInfo& top_level ) { m_find_decay.emplace( m_tool_name, top_level.algorithm() ); }

    auto operator()( const LHCb::MCParticle& part ) const {
      assert( m_find_decay.has_value() );
      if ( !m_find_decay->get() ) m_find_decay->retrieve().ignore();
      auto                          tool = m_find_decay->get();
      Decays::IMCDecay::Finder      finder( tool->tree( m_decay_desc ) );
      LHCb::MCParticle::ConstVector in_v = { &part };
      LHCb::MCParticle::ConstVector out_v;
      finder.findDecay( in_v.begin(), in_v.end(), out_v );
      bool found_particle = ( out_v.size() == 1 );
      return found_particle ? Functors::Optional{ out_v.front() }
                            : std::nullopt; // FIXME: an optional pointer is a semantic stutter
    }

    auto operator()( const LHCb::MCParticle* part ) const { return Functors::and_then( part, *this ); }

  private:
    std::string                                 m_decay_desc;
    std::string                                 m_tool_name;
    std::optional<ToolHandle<Decays::IMCDecay>> m_find_decay;
  };
} // namespace Functors::Decay
