/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/Calo/CaloCellID.h"
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/RichParticleIDType.h"

namespace Functors::PID {

  // RICH
  namespace details {
    class RichDLL : public Function {
      Rich::ParticleIDType m_pidtype;
      bool                 m_scaled = false;

    public:
      constexpr RichDLL( Rich::ParticleIDType pidtype, bool scaled = false )
          : m_pidtype{ pidtype }, m_scaled{ scaled } {}
      auto operator()( LHCb::Particle const& p ) const {
        auto const* pid = p.proto() ? p.proto()->richPID() : nullptr;
        if ( m_scaled ) {
          return pid ? Functors::Optional{ pid->scaledDLLForCombDLL( m_pidtype ) } : std::nullopt;
        } else {
          return pid ? Functors::Optional{ pid->particleDeltaLL( m_pidtype ) } : std::nullopt;
        }
      }

      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <typename... T>
    class RichPIDPredicate : public Predicate {
      bool ( LHCb::RichPID::*m_fn )( T... ) const;
      std::tuple<T...> m_args;

    public:
      constexpr RichPIDPredicate( bool ( LHCb::RichPID::*fn )( T... ) const, T... args )
          : m_fn{ fn }, m_args{ args... } {}
      bool operator()( LHCb::Particle const& p ) const {
        auto const* pid = p.proto() ? p.proto()->richPID() : nullptr;
        return pid ? std::apply( [&]( auto... args ) { return ( pid->*m_fn )( args... ); }, m_args ) : false;
      }
      bool operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };
  } // namespace details

  constexpr auto RichDLLe  = details::RichDLL{ Rich::ParticleIDType::Electron };
  constexpr auto RichDLLmu = details::RichDLL{ Rich::ParticleIDType::Muon };
  constexpr auto RichDLLp  = details::RichDLL{ Rich::ParticleIDType::Proton };
  constexpr auto RichDLLk  = details::RichDLL{ Rich::ParticleIDType::Kaon };
  constexpr auto RichDLLpi = details::RichDLL{ Rich::ParticleIDType::Pion };
  constexpr auto RichDLLd  = details::RichDLL{ Rich::ParticleIDType::Deuteron };
  constexpr auto RichDLLbt = details::RichDLL{ Rich::ParticleIDType::BelowThreshold };

  constexpr auto RichScaledDLLe  = details::RichDLL{ Rich::ParticleIDType::Electron, true };
  constexpr auto RichScaledDLLmu = details::RichDLL{ Rich::ParticleIDType::Muon, true };

  constexpr auto Rich1GasUsed = details::RichPIDPredicate{ &LHCb::RichPID::usedRich1Gas };
  constexpr auto Rich2GasUsed = details::RichPIDPredicate{ &LHCb::RichPID::usedRich2Gas };

  constexpr auto RichThresholdEl =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Electron };
  constexpr auto RichThresholdKa =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Kaon };
  constexpr auto RichThresholdMu =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Muon };
  constexpr auto RichThresholdPi =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Pion };
  constexpr auto RichThresholdPr =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Proton };
  constexpr auto RichThresholdDe =
      details::RichPIDPredicate{ &LHCb::RichPID::isAboveThreshold, Rich::ParticleIDType::Deuteron };

  // MUON
  //

  namespace details {
    template <typename Pred>
    class MuonPIDPredicate : public Predicate {
      Pred m_pred;

    public:
      constexpr MuonPIDPredicate( Pred pred ) : m_pred{ pred } {}

      bool operator()( LHCb::ProtoParticle const& p ) const {
        return Functors::and_then( p.LHCb::ProtoParticle::muonPID(), *this ).value_or( false );
      }
      bool operator()( LHCb::Particle const& p ) const {
        return Functors::and_then( p.proto(), *this ).value_or( false );
      }

      template <typename T>
      auto operator()( T const& p ) const -> decltype( m_pred( p ) ) {
        return m_pred( p );
      }

      template <typename T>
      auto operator()( T const* p ) const -> decltype( ( *this )( *p ) ) {
        return ( *this )( *p );
      }
    };
  } // namespace details

  /** @brief IsMuon, as defined by the accessor of the same name.
   */
  constexpr auto IsMuon =
      details::MuonPIDPredicate{ []( const auto& d ) -> decltype( d.IsMuon() ) { return d.IsMuon(); } };

  /** @brief InMuon to access the Muon Inacceptance information.
   */
  constexpr auto InAcceptance =
      details::MuonPIDPredicate{ []( auto const& d ) -> decltype( d.InAcceptance() ) { return d.InAcceptance(); } };

  /** @brief IsMuonTight, as defined by the accessor of the same name.
   */
  constexpr auto IsMuonTight =
      details::MuonPIDPredicate{ []( auto const& d ) -> decltype( d.IsMuonTight() ) { return d.IsMuonTight(); } };

  namespace details {
    template <typename R, typename Fun>
    class MuonPIDFunction : public Function {
      R ( LHCb::MuonPID::*m_mfn )() const;
      Fun m_fn;

    public:
      constexpr MuonPIDFunction( R ( LHCb::MuonPID::*mfn )() const, Fun fn ) : m_mfn{ mfn }, m_fn{ std::move( fn ) } {}

      auto operator()( LHCb::Particle const& p ) const {
        return Functors::and_then( p.proto(), &LHCb::ProtoParticle::muonPID, m_mfn );
      }

      template <typename T>
      auto operator()( T const& p ) const -> decltype( m_fn( p ) ) {
        return m_fn( p );
      }

      template <typename T>
      auto operator()( T const* p ) const -> decltype( ( *this )( *p ) ) {
        return ( *this )( *p );
      }
    };
  } // namespace details

  /** @brief Chi2Corr, as defined by the accessor of the same name. Chi2 taking into account correlations using only
   * hits from the Muon detector.
   */
  constexpr auto MuonChi2Corr = details::MuonPIDFunction{
      &LHCb::MuonPID::chi2Corr, []( const auto& d ) -> decltype( d.Chi2Corr() ) { return d.Chi2Corr(); } };

  /** @brief MuonLLMu, as defined by the accessor LLMu. Muon likelihood using only Muon detector information
   */
  constexpr auto MuonLLMu = details::MuonPIDFunction{
      &LHCb::MuonPID::MuonLLMu, []( const auto& d ) -> decltype( d.LLMu() ) { return d.LLMu(); } };

  /** @brief MuonLLBg, as defined by the accessor LLBg. Background likelihood using only Muon detector information
   */
  constexpr auto MuonLLBg = details::MuonPIDFunction{
      &LHCb::MuonPID::MuonLLBg, []( const auto& d ) -> decltype( d.LLBg() ) { return d.LLBg(); } };

  /** @brief MuonCatBoost, as defined by the accessor CatBoost. MVA output using only Muon detector and track
   * information
   */
  constexpr auto MuonCatBoost = details::MuonPIDFunction{
      &LHCb::MuonPID::muonMVA2, []( const auto& d ) -> decltype( d.CatBoost() ) { return d.CatBoost(); } };

  // CALO
  //
  using namespace LHCb::Event::Calo::v1;

  namespace details {

    template <typename T>
    class CaloPredicate : Predicate {
      T const* ( LHCb::ProtoParticle::*m_obj )() const;
      bool ( T::*m_fn )() const;

    public:
      constexpr CaloPredicate( T const* ( LHCb::ProtoParticle::*obj )() const, bool ( T::*fn )() const )
          : m_obj{ obj }, m_fn{ fn } {}

      bool operator()( LHCb::ProtoParticle const& p ) const {
        return Functors::and_then( ( p.*m_obj )(), m_fn ).value_or( false );
      }
      bool operator()( LHCb::Particle const& p ) const {
        return Functors::and_then( p.proto(), *this ).value_or( false );
      }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <typename T, typename R>
    class CaloFunction : Function {
      T const* ( LHCb::ProtoParticle::*m_obj )() const;
      bool ( T::*m_acc )() const;
      R ( T::*m_fn )() const;

    public:
      constexpr CaloFunction( T const* ( LHCb::ProtoParticle::*obj )() const, bool ( T::*acc )() const,
                              R ( T::*fn )() const )
          : m_obj{ obj }, m_acc{ acc }, m_fn{ fn } {}
      auto operator()( T const& pid ) const {
        return ( pid.*m_acc )() ? Functors::Optional{ ( pid.*m_fn )() } : std::nullopt;
      }
      auto operator()( LHCb::ProtoParticle const& p ) const { return Functors::and_then( ( p.*m_obj )(), *this ); }
      auto operator()( LHCb::Particle const& p ) const { return Functors::and_then( p.proto(), *this ); }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

    template <typename T>
    class CellIDFunc : Function {
      T const* ( LHCb::ProtoParticle::*m_obj )() const;
      LHCb::Detector::Calo::CellID ( T::*m_fn )() const;

    public:
      constexpr CellIDFunc( T const* ( LHCb::ProtoParticle::*obj )() const,
                            LHCb::Detector::Calo::CellID ( T::*fn )() const )
          : m_obj{ obj }, m_fn{ fn } {}
      auto operator()( LHCb::ProtoParticle const& p ) const {
        return Functors::and_then( ( p.*m_obj )(), m_fn ).value_or( LHCb::Detector::Calo::CellID{} );
      }
      auto operator()( LHCb::Particle const& p ) const {
        return Functors::and_then( p.proto(), *this ).value_or( LHCb::Detector::Calo::CellID{} );
      }
      auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
    };

  } // namespace details

  constexpr auto InEcal = details::CaloPredicate{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal };
  constexpr auto InHcal = details::CaloPredicate{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal };

  constexpr auto InBrem  = details::CaloPredicate{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem };
  constexpr auto HasBrem = details::CaloPredicate{ &LHCb::ProtoParticle::bremInfo, &BremInfo::HasBrem };

  constexpr auto EcalPIDe =
      details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal, &CaloChargedPID::EcalPIDe };
  constexpr auto EcalPIDmu = details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal,
                                                    &CaloChargedPID::EcalPIDmu };
  constexpr auto ElectronShowerEoP = details::CaloFunction{
      &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal, &CaloChargedPID::ElectronShowerEoP };
  constexpr auto ElectronShowerDLL = details::CaloFunction{
      &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal, &CaloChargedPID::ElectronShowerDLL };
  constexpr auto ElectronMatch  = details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal,
                                                        &CaloChargedPID::ElectronMatch };
  constexpr auto ElectronEnergy = details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InEcal,
                                                         &CaloChargedPID::ElectronEnergy };
  constexpr auto ElectronID = details::CellIDFunc{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::ElectronID };
  constexpr auto ClusterID  = details::CellIDFunc{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::ClusterID };

  constexpr auto HcalPIDe =
      details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal, &CaloChargedPID::HcalPIDe };
  constexpr auto HcalPIDmu = details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal,
                                                    &CaloChargedPID::HcalPIDmu };
  constexpr auto HcalEoP =
      details::CaloFunction{ &LHCb::ProtoParticle::caloChargedPID, &CaloChargedPID::InHcal, &CaloChargedPID::HcalEoP };

  constexpr auto BremEnergy =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremEnergy };
  constexpr auto BremPIDe =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremPIDe };
  constexpr auto BremBendCorr =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremBendingCorrection };
  constexpr auto BremHypoMatch =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremHypoMatch };
  constexpr auto BremHypoEnergy =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremHypoEnergy };
  constexpr auto BremHypoDeltaX =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremHypoDeltaX };
  constexpr auto BremTrackBasedEnergy =
      details::CaloFunction{ &LHCb::ProtoParticle::bremInfo, &BremInfo::InBrem, &BremInfo::BremTrackBasedEnergy };
  constexpr auto BremHypoID = details::CellIDFunc{ &LHCb::ProtoParticle::bremInfo, &BremInfo::BremHypoID };

  struct ClusterMatch_t : Function {
    Functors::Optional<float> operator()( LHCb::ProtoParticle const& p ) const {
      if ( p.track() ) {
        return Functors::and_then( p.caloChargedPID(), &LHCb::Event::Calo::CaloChargedPID::ClusterMatch )
            .value_or( invalid_value ); // FIXME: drop `.value_or(invalid_value)`
      } else {
        return Functors::and_then( p.neutralPID(), &LHCb::NeutralPID::CaloTrMatch );
      }
    }
    auto operator()( LHCb::Particle const& p ) const { return Functors::and_then( p.proto(), *this ); }
    auto operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
  };
  constexpr auto ClusterMatch = ClusterMatch_t{};

  namespace CaloCellID {

    constexpr auto All    = Functors::TrivialFunctor{ "CaloCellID::All", &LHCb::Detector::Calo::CellID::all };
    constexpr auto Area   = Functors::TrivialFunctor{ "CaloCellID::Area", &LHCb::Detector::Calo::CellID::area };
    constexpr auto Row    = Functors::TrivialFunctor{ "CaloCellID::Row", &LHCb::Detector::Calo::CellID::row };
    constexpr auto Column = Functors::TrivialFunctor{ "CaloCellID::Col", &LHCb::Detector::Calo::CellID::col };

  } // namespace CaloCellID

} // namespace Functors::PID
