/***************************************************************************** \
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Functors/Function.h>
#include <GaudiKernel/Algorithm.h>
#include <GaudiKernel/DataObjID.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <GaudiKernel/GaudiException.h>
// this is where standard_geometry_top is defined
#include <DetDesc/IDetectorElement.h>
#if USE_DD4HEP
// This code is DD4hep specific
#  include <Detector/LHCb/DeLHCb.h>
#  include <LbDD4hep/IDD4hepSvc.h>
#else
#  include <Detector/LHCb/InteractionRegion.h>
#  include <Detector/LHCb/LHCInfo.h>
#  include <Detector/LHCb/SMOGInfo.h>
#endif
#include <optional>
#include <string>

namespace Functors::Detector {

#if USE_DD4HEP
  struct DeLHCb_t final : public Function {
    DeLHCb_t() = default;
    DeLHCb_t( const DeLHCb_t& ) {
      if ( m_condCtx )
        throw GaudiException{ "DeLHCb Functor can not be copied after it is bound to an algorithm",
                              "Functors::Detector::DeLHCb", StatusCode::FAILURE };
    }
    DeLHCb_t& operator=( const DeLHCb_t& rhs ) = delete;

    /** Set up the DataHandles, attributing the data dependencies to the given
     *  algorithm, using .emplace() with the Condition location and algorithm pointer.
     */
    void bind( TopLevelInfo& top_level ) {
      m_condCtx.emplace( DataObjID{ LHCb::Det::LbDD4hep::IDD4hepSvc::DefaultSliceLocation }, top_level.algorithm() );
      // This is a workaround for DataObjectReadHandle::init being protected.
      static_cast<Gaudi::DataHandle*>( &*m_condCtx )->init();
    }

    /**
     * Retrieve the data dependencies from the Condition and bake them into the
     * "prepared" functor that we return
     */
    auto prepare( EventContext const&, TopLevelInfo const& ) const {
      // Note: getting the a condition key from a string is really this complicated
      static const dd4hep::Condition::key_type delhcb_key = [] {
        auto const& path     = LHCb::standard_geometry_top;
        const auto  colonPos = path.find_first_of( ':' );
        if ( colonPos == path.npos )
          throw GaudiException{ "invalid path", "Functors::Detector::DeLHCb", StatusCode::FAILURE };
        const auto det_name  = path.substr( 0, colonPos );
        const auto cond_name = path.substr( colonPos + 1 );
        return dd4hep::ConditionKey::KeyMaker{ dd4hep::detail::hash32( det_name ), cond_name }.hash;
      }();

      if ( !m_condCtx ) {
        throw GaudiException{ "DeLHCb Functor called without being bound to an algorithm", "Functors::Detector::DeLHCb",
                              StatusCode::FAILURE };
      }

      // Ideally we should pass EventContext to DataObjectReadHandle::get, but the API was never implemented
      auto& slice = *m_condCtx->get();
      // Get the DeLHCb instance from the current conditions slice
      // and bake this into a new lambda that we return
      return [de = LHCb::Detector::DeLHCb{ slice->pool->get( delhcb_key ) }]( auto const&... ) -> decltype( auto ) {
        return de;
      };
    }

  private:
    std::optional<DataObjectReadHandle<LHCb::Det::LbDD4hep::IDD4hepSvc::DD4HepSlicePtr>> m_condCtx;
  };

#else

  struct DeLHCb_t final : public Function {
    auto operator()( auto&&... ) const {
      throw GaudiException{ "DeLHCb Functor is specific to DD4HEP -- you are using DetDesc -- please switch platforms "
                            "or remove call to this functor",
                            "Functors::Detector::DeLHCb", StatusCode::FAILURE };
      struct FakeDetDescDeLHCb {
        std::optional<LHCb::Detector::InteractionRegion> interactionRegion() const { return {}; }
        std::optional<LHCb::Detector::LHCInfo>           lhcInfo() const { return {}; }
        std::optional<LHCb::Detector::SMOGInfo>          SMOG() const { return {}; }
      };
      return FakeDetDescDeLHCb{};
    }
  };

#endif

  inline const auto DeLHCb = DeLHCb_t{};

  namespace details {
    template <typename F>
    class DeLHCInfoFunctor final : public Function {
      F fun;

    public:
      constexpr DeLHCInfoFunctor( F f ) : fun{ std::move( f ) } {}
#if USE_DD4HEP
      auto operator()( LHCb::Detector::DeLHCb const& de ) const { return Functors::and_then( de.lhcInfo(), fun ); }
#else
      auto operator()( auto const& ) const {
        throw GaudiException{
            "DeSMOGInfoFunctor is specific to DD4HEP -- you are using DetDesc -- please switch platforms "
            "or remove call to this functor",
            "Functors::Detector::DeSMOGInfoFunctor", StatusCode::FAILURE };
        return Functors::and_then( std::optional<LHCb::Detector::LHCInfo>{}, fun );
      }
#endif
    };

    template <typename F>
    class DeSMOGInfoFunctor final : public Function {
      F fun;

    public:
      constexpr DeSMOGInfoFunctor( F f ) : fun{ std::move( f ) } {}
#if USE_DD4HEP
      auto operator()( LHCb::Detector::DeLHCb const& de ) const {
        return Functors::and_then( de.SMOG(),
                                   [&]( const auto& info ) { return static_cast<int>( std::invoke( fun, info ) ); } );
      }
#else
      auto operator()( auto const& ) const {
        throw GaudiException{
            "DeSMOGInfoFunctor is specific to DD4HEP -- you are using DetDesc -- please switch platforms "
            "or remove call to this functor",
            "Functors::Detector::DeSMOGInfoFunctor", StatusCode::FAILURE };
        return std::optional<int>{};
      }
#endif
    };
  } // namespace details

  // Get the fill number from LHC condition
  constexpr auto FillNumber = details::DeLHCInfoFunctor{ &LHCb::Detector::LHCInfo::fillnumber };

  // Get the LHC energy condition
  constexpr auto LHCEnergy = details::DeLHCInfoFunctor{ &LHCb::Detector::LHCInfo::lhcenergy };

  // Get the LHCb clock phase from LHC condition
  constexpr auto LHCbClockPhase = details::DeLHCInfoFunctor{ &LHCb::Detector::LHCInfo::lhcbclockphase };

  // Get the SMOG injection mode from SMOG condition
  constexpr auto SMOGInjectionMode = details::DeSMOGInfoFunctor{ &LHCb::Detector::SMOGInfo::injection_mode };

  // Get the SMOG injected gas
  constexpr auto SMOGInjectedGas = details::DeSMOGInfoFunctor{ &LHCb::Detector::SMOGInfo::injected_gas };

  // Get the SMOG stable injection flag
  constexpr auto SMOGStableInjection = details::DeSMOGInfoFunctor{ &LHCb::Detector::SMOGInfo::stable_injection };

} // namespace Functors::Detector
