/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/Optional.h"
#include "Functors/type_name.h"
#include "Gaudi/Algorithm.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/Utilities.h"
#include <cmath>
#include <type_traits>
#include <utility>
#if __has_cpp_attribute( no_unique_address )
#  define LHCB_HAS_NO_UNIQUE_ADDRESS [[no_unique_address]]
#else
#  define LHCB_HAS_NO_UNIQUE_ADDRESS
#endif

namespace Functors::detail {

  template <typename T>
  LHCB_FUNCTORS_INLINE constexpr auto fwd_as_flattened_tuple( T&& val ) -> decltype( auto ) {
    if constexpr ( detail::is_tuple_v<std::decay_t<T>> ) {
      return std::forward<T>( val );
    } else {
      return std::forward_as_tuple( std::forward<T>( val ) );
    }
  }

  template <typename T1, typename T2, typename... Ts>
  LHCB_FUNCTORS_INLINE constexpr auto fwd_as_flattened_tuple( T1&& t1, T2&& t2, Ts&&... ts ) -> decltype( auto ) {
    return std::tuple_cat( fwd_as_flattened_tuple( t1 ), fwd_as_flattened_tuple( t2 ),
                           fwd_as_flattened_tuple( ts )... );
  }

  template <typename T, typename Tuple>
  inline constexpr bool is_constructible_from_tuple_v = false;

  template <typename T, typename... Args>
  inline constexpr bool is_constructible_from_tuple_v<T, std::tuple<Args...>> = std::is_constructible_v<T, Args...>;

#define LHCB_HAS_TUPLET

  /* Tuplet is a very minimal C++20 tuple class:
   * It is an almost drop-in replacement for `std::tuple` _that only supports `apply`_.
   * There is no `get`, no `tuple_element`, no comparisons, nothing else!
   */

  static constexpr auto make_tuplet_ = []<typename... T>( T&&... data ) {
    // A lambda has _one_ call operator implemenation - but we need both a const and a non-const one
    // So const-ness must be propagated explicitly: add an extra argument whose type indicates
    // whether to subsequently cast-away const or not...
    return [... m_ = std::forward<T>( data )]<typename Self>( Self*, auto&& g ) -> decltype( auto ) {
      if constexpr ( std::is_const_v<Self> ) {
        return g( m_... );
      } else {
        constexpr auto unconst = []<typename U>( const U& u ) -> decltype( auto ) { return const_cast<U&>( u ); };
        return g( unconst( m_ )... );
      }
    };
  };

  template <typename Payload>
  class Tuplet {
    Payload m_data;

  public:
    template <typename... Item>
    constexpr Tuplet( Item&&... item ) : m_data{ make_tuplet_( std::forward<Item>( item )... ) } {}

    template <typename Action>
    friend constexpr auto apply( Action&& action, Tuplet& tpl ) -> decltype( auto ) {
      return tpl.m_data( static_cast<void*>( nullptr ), std::forward<Action>( action ) );
    }
    template <typename Action>
    friend constexpr auto apply( Action&& action, Tuplet const& tpl ) -> decltype( auto ) {
      return tpl.m_data( static_cast<void const*>( nullptr ), std::forward<Action>( action ) );
    }
  };

  template <typename... Item>
  Tuplet( Item&&... ) -> Tuplet<decltype( make_tuplet_( std::declval<Item>()... ) )>;

} // namespace Functors::detail

/** @file  Function.h
 *  @brief Helper types and operator overloads for composition of functions.
 */

namespace Functors {

  template <typename... Fn>
  class TrivialFunctor : public Function, private Fn... {
    std::string_view m_name;

  public:
    // note that the `const char (&name)[N]` is chosen as argument to make sure that
    // it is a literal -- as then `m_name` will be valid for the duration of its lifetime
    template <size_t N>
    constexpr TrivialFunctor( const char ( &name )[N], Fn... fn ) : Fn{ std::move( fn ) }..., m_name{ name } {}
    constexpr auto name() const { return m_name; }
    using Fn::operator()...;
  };

  template <typename... Fn, size_t N>
  TrivialFunctor( const char ( &name )[N], Fn&&... ) -> TrivialFunctor<std::decay_t<Fn>...>;

  template <typename R, typename T>
  class TrivialFunctor<R ( T::* )() const> : public Function {
    R ( T::*m_fn )() const  = nullptr;
    std::string_view m_name = {};

  public:
    template <size_t N>
    constexpr TrivialFunctor( const char ( &name )[N], R ( T::*fn )() const ) : m_fn{ fn }, m_name{ name } {}
    constexpr auto name() const { return m_name; }
    R              operator()( T const& obj ) const { return ( obj.*m_fn )(); }
  };

  template <typename R, typename... Args>
  class TrivialFunctor<R ( * )( Args... )> : public Function {
    R ( *m_fn )( Args... )  = nullptr;
    std::string_view m_name = {};

  public:
    template <size_t N>
    constexpr TrivialFunctor( const char ( &name )[N], R ( *fn )( Args... ) ) : m_fn{ fn }, m_name{ name } {}
    constexpr auto name() const { return m_name; }
    R              operator()( Args... obj ) const { return ( *m_fn )( obj... ); }
  };

  // GenericFunctor is a generalization of TrivialFunctor: it adds methods which allow the functor
  // to be called with pointers and SmartRefs. The return value is wrapped in an optional in order
  // to handle the case the pointer/SmartRef does not point at a valid object -- unless the return type
  // is a pointer, in which case a nullptr is returned.
  template <typename... Fn>
  struct GenericFunctor : TrivialFunctor<Fn...> {
    using TrivialFunctor<Fn...>::TrivialFunctor;
    using TrivialFunctor<Fn...>::operator();
    TrivialFunctor<Fn...> const& as_base() const { return *this; }

    template <typename T, typename... Args>
      requires requires( TrivialFunctor<Fn...> const& fun, const T* t, const Args&... args ) { fun( *t, args... ); }
    auto operator()( const T* t, const Args&... args ) const {
      if constexpr ( std::is_pointer_v<decltype( as_base()( *t, args... ) )> ) {
        return t ? as_base()( *t, args... ) : nullptr;
      } else {
        return t ? Functors::Optional{ as_base()( *t, args... ) } : std::nullopt;
      }
    }
    template <typename T, typename U, typename... Args>
      requires requires( TrivialFunctor<Fn...> const& fun, const T& t, const U* u, const Args&... args ) {
        fun( t, *u, args... );
      }
    auto operator()( const T& t, const U* u, const Args&... args ) const {
      if constexpr ( std::is_pointer_v<decltype( as_base()( t, *u, args... ) )> ) {
        return u ? as_base()( t, *u, args... ) : nullptr;
      } else {
        return u ? Functors::Optional{ as_base()( t, *u, args... ) } : std::nullopt;
      }
    }

    // explicitly deal with SmartRef-s
    template <typename T, typename... Args>
      requires requires( TrivialFunctor<Fn...> const& fun, const SmartRef<T>& t, const Args&... args ) {
        fun( *t, args... );
      }
    auto operator()( const SmartRef<T>& t, const Args&... args ) const {
      return ( *this )( t.target(), args... );
    }

    template <typename T, typename U, typename... Args>
      requires requires( TrivialFunctor<Fn...> const& fun, const T& t, const SmartRef<U>& u, const Args&... args ) {
        fun( t, *u, args... );
      }
    auto operator()( const T& t, const SmartRef<U>& u, const Args&... args ) const {
      return ( *this )( t, u.target(), args... );
    }
  };
  template <typename... Fn, size_t N>
  GenericFunctor( const char ( &name )[N], Fn&&... ) -> GenericFunctor<std::decay_t<Fn>...>;

  template <typename... Fn>
  class TrivialPredicate : public Predicate, private Fn... {
    std::string_view m_name;

  public:
    // note that the `const char (&name)[N]` is chosen as argument to make sure that
    // it is a literal -- as then `m_name` will be valid for the duration of its lifetime
    template <size_t N>
    constexpr TrivialPredicate( const char ( &name )[N], Fn... fn ) : Fn{ std::move( fn ) }..., m_name{ name } {}
    constexpr auto name() const { return m_name; }
    using Fn::operator()...;
  };

  template <typename... Fn, size_t N>
  TrivialPredicate( const char ( &name )[N], Fn&&... ) -> TrivialPredicate<std::decay_t<Fn>...>;

  template <typename R, typename T>
  class TrivialPredicate<R ( T::* )() const> : public Predicate {
    R ( T::*m_fn )() const  = nullptr;
    std::string_view m_name = {};

  public:
    template <size_t N>
    constexpr TrivialPredicate( const char ( &name )[N], R ( T::*fn )() const ) : m_fn{ fn }, m_name{ name } {}
    constexpr auto name() const { return m_name; }
    bool           operator()( T const& obj ) const { return static_cast<bool>( ( obj.*m_fn )() ); }
  };

  template <typename R, typename... Args>
  class TrivialPredicate<R ( * )( Args... )> : public Predicate {
    R ( *m_fn )( Args... )  = nullptr;
    std::string_view m_name = {};

  public:
    template <size_t N>
    constexpr TrivialPredicate( const char ( &name )[N], R ( *fn )( Args... ) ) : m_fn{ fn }, m_name{ name } {}
    constexpr auto name() const { return m_name; }
    bool           operator()( Args const&... obj ) const { return static_cast<bool>( ( *m_fn )( obj... ) ); }
  };

  /** @class NumericValue
   *  @brief Trivial function that holds a numeric constant.
   *
   *  This is the type that is generated for the RHS of expressions such as
   *  "ETA > 2".
   */
  template <typename PODtype>
  struct NumericValue : public Function {
    constexpr NumericValue( PODtype value ) : m_value( value ) {}
    template <typename... Input>
    constexpr PODtype operator()( Input&&... ) const {
      return m_value;
    }

  private:
    PODtype m_value;
  };

  template <typename T>
  NumericValue( T ) -> NumericValue<std::decay_t<T>>;
  template <typename T, T v>
  NumericValue( std::integral_constant<T, v> ) -> NumericValue<T>;

  namespace detail {
    /** Use our own helper to define in one place which types we allow to be
     *  promoted to NumericValue.
     */
    template <typename F>
    concept FunctorConstant = std::is_arithmetic_v<std::decay_t<F>> || std::is_enum_v<std::decay_t<F>>;

  } // namespace detail

  /** @class AcceptAll
   *  @brief Trivial functor that always returns true.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  constexpr auto AcceptAll = TrivialPredicate{ "All", []<typename... Data>( Data&&... ) {
                                                if constexpr ( sizeof...( Data ) > 0 ) {
                                                  using FirstType =
                                                      std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
                                                  if constexpr ( requires { FirstType::mask_true(); } ) {
                                                    return FirstType::mask_true();
                                                  } else {
                                                    return true;
                                                  }
                                                } else {
                                                  return true;
                                                }
                                              } };

  /** @class AcceptNone
   *  @brief Trivial functor that always returns false.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  constexpr auto AcceptNone = TrivialPredicate{ "None", []<typename... Data>( Data&&... ) {
                                                 if constexpr ( sizeof...( Data ) > 0 ) {
                                                   using FirstType =
                                                       std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
                                                   if constexpr ( requires { FirstType::mask_true(); } ) {
                                                     return !FirstType::mask_true();
                                                   } else {
                                                     return false;
                                                   }
                                                 } else {
                                                   return false;
                                                 }
                                               } };

  /** @class Identity
   *  @brief Trivial functor that returns the same value.
   */
  constexpr auto Identity =
      TrivialFunctor{ "Identity", []<typename T>( T&& t ) -> decltype( auto ) { return std::forward<T>( t ); } };

  /** @class Logger
   *  @brief Trivial functor that prints the argument, and then returns it
   */
  class Logger : public Function {
    std::string m_format;

  public:
    Logger( std::string format ) : m_format{ std::move( format ) } {}

    auto prepare( EventContext const&, TopLevelInfo const& top_level ) const {
      return [alg = top_level.algorithm(), format = std::string_view{ m_format }]<typename T>( T&& t ) -> T&& {
        using GaudiUtils::operator<<;
        std::stringstream s;
        s << t;
        alg->always() << fmt::format( fmt::runtime( format ), fmt::arg( "arg_type", type_name<T>() ),
                                      fmt::arg( "arg", s.view() ) )
                      << endmsg;
        return std::forward<T>( t );
      };
    }
  };

  /** @class Column
  Instantiated with a "label" , when acting on object T returns T("label")
   */
  struct Column_t : public Function {
    Column_t( std::string label ) : m_label{ std::move( label ) } {}

    template <typename T>
    constexpr decltype( auto ) operator()( const T& t ) const
      requires requires( const T& t, std::string const& s ) { t( s ); }
    {
      return t( m_label );
    }

  private:
    std::string m_label{ "" };
  };
  namespace detail {

    namespace composition {
      // tag structs to steer the composition of functors
      struct chain final {};
      struct bind  final {};
    } // namespace composition

    template <typename, FunctorPredicate... F>
    LHCB_FUNCTORS_INLINE constexpr auto prepare_( std::logical_and<>, EventContext const& evtCtx,
                                                  TopLevelInfo const& top_level, F const&... f ) {
      static_assert( sizeof...( f ) > 1 );

      return [... pf = prepared( f, evtCtx, top_level )]( mask_arg_t, auto mask, auto&&... input ) {
        using Sel::Utils::any; // for the scalar case
        // Operator{}( ... ) would not short-circuit in this case
        // Short circuit vectorised cuts as well as scalar ones -- use a fold expression do to so,
        // which has the right short-circuit behavior. Basically: as long as mask contains any valid
        // entries, update mask (by invoking the next functor for those  entries which are still valid),
        // and check again -- until mask no longer has any valid entries, i.e. any(mask) becomes false
        ( any( mask ) && ... && ( mask = mask && pf( mask_arg, mask, input... ), any( mask ) ) );
        return mask;
      };
    };

    template <typename, FunctorPredicate F0, FunctorPredicate F1>
    LHCB_FUNCTORS_INLINE constexpr auto prepare_( std::logical_or<>, EventContext const& evtCtx,
                                                  TopLevelInfo const& top_level, F0 const& f0, F1 const& f1 ) {
      return [f0 = prepared( f0, evtCtx, top_level ), f1 = prepared( f1, evtCtx, top_level )]( mask_arg_t, auto mask,
                                                                                               auto&&... input ) {
        // Operator{}( ... ) would not short-circuit in this case
        // we start with 'first' set to all entries that should _not_ be checked, and then
        // we _add_ those which pass the first fs (and thus don't have to be checked anymore) to first
        auto const first = !mask || f0( mask_arg, mask, std::forward<decltype( input )>( input )... );
        // Short circuit vectorised cuts as well as scalar ones
        using Sel::Utils::all; // for the scalar case
        // if nothing left to check (i.e. all _possible_ elements passed the first fs), return the original mask
        if ( all( first ) ) return mask;
        // first now contains all elements that are either invalid, or have already passed, so !first
        // corresponds to all valid, not yet passed elements - so use that as mask to second fs.
        // We then update first with the ones that passed the second fs. At this point, first contains
        // all elements which are either invalid, or have passed one of the two fs), and then combine
        // with 'all valid' (i.e. mask) to get all valid _passed_ elements
        // Basically, mask the RHS so it only has to calculate the bits that would
        // make a difference to the return value.
        return mask && ( first || f1( mask_arg, !first, std::forward<decltype( input )>( input )... ) );
      };
    };

    namespace for_chain_only {
      template <typename F, typename Arg>
      LHCB_FUNCTORS_INLINE constexpr auto operator<<=( F&& f, Arg&& arg ) -> decltype( auto ) {
        return f( std::forward<Arg>( arg ) );
      }
    } // namespace for_chain_only

    template <typename, FunctorFunction... F>
    LHCB_FUNCTORS_INLINE constexpr auto prepare_( composition::chain, EventContext const& evtCtx,
                                                  TopLevelInfo const& top_level, F const&... f ) {
      return [... pf = prepared( f, evtCtx, top_level )]<typename... Input>( mask_arg_t, auto mask,
                                                                             Input&&... input ) -> decltype( auto ) {
        using for_chain_only::operator<<=;
        return ( LHCb::cxx::bind_front( pf, mask_arg, mask ) <<= ... <<=
                 std::forward_as_tuple( std::forward<Input>( input )... ) );
      };
    };

    template <typename, FunctorFunction F0, FunctorFunction... F>
    LHCB_FUNCTORS_INLINE constexpr auto prepare_( composition::bind, EventContext const& evtCtx,
                                                  TopLevelInfo const& top_level, F0 const& f0, F const&... f ) {
      return [f0 = prepared( f0, evtCtx, top_level ), ... pf = prepared( f, evtCtx, top_level )]<typename... Input>(
                 mask_arg_t, auto mask, Input&&... input ) -> decltype( auto ) {
        return f0( mask_arg, mask, fwd_as_flattened_tuple( pf( mask_arg, mask, std::forward<Input>( input )... )... ) );
      };
    };

    template <typename NewBase, typename Operator, FunctorFunction... F>
    LHCB_FUNCTORS_INLINE constexpr auto prepare_( Operator op, EventContext const& evtCtx,
                                                  TopLevelInfo const& top_level, const F&... f ) {
      return [op = std::move( op ), ... pf = prepared( f, evtCtx, top_level )]( mask_arg_t, auto mask,
                                                                                auto&&... input ) -> decltype( auto ) {
        // Evaluate all the functors and let the given Operator combine the results.
        return [&op]<typename... I>( I&&... i ) {
          // If one (or more) of the functors has an optional return, we have to propagate it
          if constexpr ( ( is_our_optional_v<std::decay_t<I>> || ... ) ) {
            // Predicate should not return optional values.
            if constexpr ( std::is_same_v<NewBase, Predicate> ) {
              return ( has_value( i ) && ... ) ? op( get_value( std::forward<I>( i ) )... ) : false;
            } else {
              return ( has_value( i ) && ... ) ? Functors::Optional{ op( get_value( std::forward<I>( i ) )... ) }
                                               : std::nullopt;
            }
          } else {
            return op( std::forward<I>( i )... );
          }
        }( pf( mask_arg, mask, input... )... );
      };
    };

    /** @class Composed
     *  @brief Workhorse for producing a new functor from existing ones.
     *
     *  This class is used to implement all generic composed function operations:
     *  @li arithmetric operators (+, -, *, /, %), two functions -> function.
     *  @li comparison operators (>, <, etc.), two functions -> predicate.
     *  @li logical operators (&, |), two predicates -> predicate.
     *
     */

    template <typename NewBase, typename Operator, typename Fs>
    struct Composed : NewBase {

      /** Flag that this functor explicitly receives/handles a mask giving the
       *  current validity of the arguments.
       */
      constexpr static bool requires_explicit_mask = true;

      constexpr Composed( Operator op, Fs fs ) : m_fs{ std::move( fs ) }, m_op{ std::move( op ) } {}

      /** Prepare the composite functor for use: return a new lambda that
       *  holds the prepared functors and the knowledge (encoded by Operator)
       *  of how to combine them into the result of the new (composed)
       * functor.
       */
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const& {
        return apply( [&]( auto const&... f ) { return detail::prepare_<NewBase>( m_op, evtCtx, top_level, f... ); },
                      m_fs );
      }

      // FIXME the below is not a proper fix, it just makes it harder to
      // mess up. the current implementation doesn't support this behaviour
      // so we assert it here. take the following example:
      //
      // Functors::NumericValue make1( 1 );
      // auto prepared_f = (make1 + make1).prepare(evtCtx, top_level);
      // std::cout << prepared_f(true) << std::endl;
      //
      // prepared_f is a lambda that (probably) contains references to the functors
      // owned by the Composed instance created by the operator+, but that
      // object has since gone out of scope.
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) && = delete;

      /** Contained functors may have data-dependencies that need to be
       *  associated to the owning algorithm.
       */
      void bind( TopLevelInfo& top_level ) {
        apply( [&top_level]( auto&... f ) { ( detail::bind( f, top_level ), ... ); }, m_fs );
      }

    private:
      Fs                                  m_fs;
      LHCB_HAS_NO_UNIQUE_ADDRESS Operator m_op;
    };

    /** Helper function to make a Composed instance that deduces the actual
     *  functor types from its argument types, but leaves us to specify the
     *  other details explicitly.
     */
    template <typename InputBase, typename NewBase, typename Operator, FunctorFunction... F>
    LHCB_FUNCTORS_INLINE constexpr auto compose( Operator&& op, F&&... f ) {
      static_assert( ( std::is_base_of_v<InputBase, std::decay_t<F>> && ... ) );
#ifdef LHCB_HAS_TUPLET
      auto fns = detail::Tuplet{ std::forward<F>( f )... };
#else
      auto fns = std::make_tuple( std::forward<F>( f )... );
#endif
      return Composed<NewBase, std::decay_t<Operator>, decltype( fns )>{ std::forward<Operator>( op ),
                                                                         std::move( fns ) };
    }

    /** Helper for checking if our overloaded operators should be considered.
     *
     *  The condition is that either both LHS and RHS are our functions, or one
     *  of them is and the other side of the expression is an arithmetic
     *  constant that we can promote to a NumericValue.
     */
    template <typename F>
    concept FunctorOperatorArg = ( FunctorFunction<F> || FunctorConstant<F> );

    template <typename F1, typename F2>
    concept FunctorOperatorArgs =
        (FunctorFunction<F1> && FunctorOperatorArg<F2>) || ( FunctorFunction<F2> && FunctorOperatorArg<F1> );
    template <typename F1, typename F2, typename F3>
    concept FunctorOperatorArgs3 = (FunctorFunction<F1> && FunctorOperatorArg<F2> && FunctorOperatorArg<F3>) ||
                                   (FunctorFunction<F2> && FunctorOperatorArg<F3> && FunctorOperatorArg<F1>) ||
                                   ( FunctorFunction<F3> && FunctorOperatorArg<F1> && FunctorOperatorArg<F2> );

    /** Helper to promote arithmetic constants to NumericValue instances while
     *  not doing anything to functors.
     */
    template <FunctorConstant F>
    LHCB_FUNCTORS_INLINE constexpr auto promote( F&& f ) {
      return NumericValue{ f };
    }
    template <FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto promote( F&& f ) -> decltype( auto ) {
      return std::forward<F>( f );
    }

    /** Helper to make a composed functor from a set of arguments that can be a
     *  mix of arithmetic constants and functor expressions. Arithmetic
     *  constants are promoted to NumericValue instances using promote().
     */
    template <typename NewBase, typename Operator, FunctorOperatorArg F1, FunctorOperatorArg F2>
    LHCB_FUNCTORS_INLINE constexpr auto promote_and_compose( Operator&& op, F1&& f1, F2&& f2 ) {
      if constexpr ( FunctorConstant<F1> && FunctorConstant<F2> ) {
        // FIXME: avoid this in generated code...
        return NumericValue{ op( f1, f2 ) };
      } else if constexpr ( FunctorConstant<F1> ) {
        return compose<Function, NewBase>( [op = std::forward<Operator>( op ), a1 = f1](
                                               auto&& a2 ) { return op( a1, std::forward<decltype( a2 )>( a2 ) ); },
                                           std::forward<F2>( f2 ) );
      } else if constexpr ( FunctorConstant<F2> ) {
        return compose<Function, NewBase>( [op = std::forward<Operator>( op ), a2 = f2](
                                               auto&& a1 ) { return op( std::forward<decltype( a1 )>( a1 ), a2 ); },
                                           std::forward<F1>( f1 ) );
      } else {
        return compose<Function, NewBase>( std::forward<Operator>( op ), std::forward<F1>( f1 ),
                                           std::forward<F2>( f2 ) );
      }
    }

    template <typename NewBase, typename Operator, FunctorOperatorArg... Args>
    LHCB_FUNCTORS_INLINE constexpr auto promote_and_compose( Operator&& op, Args&&... args ) {
      return compose<Function, NewBase>( std::forward<Operator>( op ), promote( std::forward<Args>( args ) )... );
    }

    /** SFINAE helper for defining boolean (bitwise...) logic overloads.
     */

  } // namespace detail

  /** operator+ overload covering all three combinations of function/arithmetic
   *
   *  This turns two functions (hence the first Function) into a new
   *  function (hence the second Function). The other operator overloads
   *  defined here are similar.
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator+( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::plus<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator- overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator-( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::minus<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator* overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator*( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::multiplies<>{}, std::forward<F1>( lhs ),
                                                  std::forward<F2>( rhs ) );
  }

  /** operator/ overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator/( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::divides<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator% overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator%( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::modulus<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** Unary operator- overload. */
  template <detail::FunctorFunction F>
  LHCB_FUNCTORS_INLINE constexpr auto operator-( F&& f ) {
    return detail::compose<Functor, Functor>( []( auto&& arg ) { return -std::forward<decltype( arg )>( arg ); },
                                              std::forward<F>( f ) );
  }

  /** operator| overload for turning two predicates into another one.
   *
   *  detail::promote_and_compose is not used here because we don't support
   *  logical operations between predicates and arithmetic constants.
   */
  template <detail::FunctorPredicate F1, detail::FunctorPredicate F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator|( F1&& lhs, F2&& rhs ) {
    return detail::compose<Predicate, Predicate>( std::logical_or<>{}, std::forward<F1>( lhs ),
                                                  std::forward<F2>( rhs ) );
  }

  /** operator& overload for turning 2 predicates into one. */
  template <detail::FunctorPredicate F1, detail::FunctorPredicate F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator&( F1&& lhs, F2&& rhs ) {
    return detail::compose<Predicate, Predicate>( std::logical_and<>{}, std::forward<F1>( lhs ),
                                                  std::forward<F2>( rhs ) );
  }

  /** operator~ overload, creates a new functor representing the logical
   *  negation of the given functor.
   */
  template <detail::FunctorPredicate F>
  LHCB_FUNCTORS_INLINE constexpr auto operator~( F&& obj ) {
    return detail::compose<Predicate, Predicate>( std::logical_not<>{}, std::forward<F>( obj ) );
  }

  /** operator< overload covering all three combinations of function/arithmetic
   *
   *  This, and the others below, transforms two Functions (hence Function)
   *  into a Predicate (hence the 3rd template argument).
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator<( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Predicate>( std::less<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator> overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator>( F1&& lhs, F2&& rhs ) {
    return operator<( std::forward<F2>( rhs ), std::forward<F1>( lhs ) );
  }

  /** operator<= overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator<=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Predicate>( std::less_equal<>{}, std::forward<F1>( lhs ),
                                                   std::forward<F2>( rhs ) );
  }

  /** operator>= overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator>=( F1&& lhs, F2&& rhs ) {
    return operator<=( std::forward<F2>( rhs ), std::forward<F1>( lhs ) );
  }

  /** operator== overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator==( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Predicate>( std::equal_to<>{}, std::forward<F1>( lhs ),
                                                   std::forward<F2>( rhs ) );
  }

  /** operator!= overload covering all three combinations of function/arithmetic
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto operator!=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Predicate>( std::not_equal_to<>{}, std::forward<F1>( lhs ),
                                                   std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise AND between two contained_functor_value objects.
   *
   * It's really operator& overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto bitwise_and( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::bit_and<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise OR between two contained_functor_value objects.
   *
   * It's really operator| overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
  LHCB_FUNCTORS_INLINE constexpr auto bitwise_or( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<Function>( std::bit_or<>{}, std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** Wrapper for function composition of functors
   *
   * given functors f and g and input x this return f(g(x))
   *
   * flattening like chain(chain(A,B),C) -> chain(A,B,C) is no longer done on
   * the C++ side but instead we already do this in python to avoid creating
   * many many intermediate types that cost alot of time and memory during
   * compliation, especially when debug info is turned on
   *
   */
  template <detail::FunctorFunction F1, detail::FunctorFunction... Fs>
  constexpr auto chain( F1&& func, Fs&&... funcs ) {
    // if the the last functor in the chain is a predicate then the resulting
    // composed functor should be one too.
    using NewBase_t = std::conditional_t<std::is_base_of_v<Predicate, std::decay_t<F1>>, Predicate, Function>;
    return detail::compose<Function, NewBase_t>( detail::composition::chain{}, std::forward<F1>( func ),
                                                 std::forward<Fs>( funcs )... );
  }

  template <detail::FunctorFunction F1, detail::FunctorFunction... Fs>
  constexpr auto bind( F1&& bound_functor, Fs&&... funcs ) {
    static_assert( sizeof...( Fs ) > 0, "need at least two functors" );
    // if the functor we bind is a predicate then the resulting composed functor should be one too.
    using NewBase_t = std::conditional_t<std::is_base_of_v<Predicate, std::decay_t<F1>>, Predicate, Function>;

    return detail::compose<Function, NewBase_t>( detail::composition::bind{}, std::forward<F1>( bound_functor ),
                                                 std::forward<Fs>( funcs )... );
  }

  /** operator& overload for turning multiple predicates into one. */

  template <detail::FunctorPredicate... Fs>
  LHCB_FUNCTORS_INLINE constexpr auto all_of( Fs&&... funcs ) {
    return detail::compose<Predicate, Predicate>( std::logical_and<>{}, std::forward<Fs>( funcs )... );
  }

  /** @namespace Functors::math
   *
   *  Mathematical functions that act on new style functors.
   */
  namespace math {
    /** Wrapper for std::pow( contained_functor_value, arg ).
     *
     *  e.g. math::pow( PT, 2 ) is a functor that returns the squared PT
     */
    template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
    LHCB_FUNCTORS_INLINE constexpr auto pow( F1&& f1, F2&& f2 ) {
      return detail::promote_and_compose<Function>(
          []( auto&& a1, auto&& a2 ) {
            using LHCb::Utils::as_arithmetic;
            using std::pow;
            return pow( as_arithmetic( a1 ), as_arithmetic( a2 ) );
          },
          std::forward<F1>( f1 ), std::forward<F2>( f2 ) );
    }

    /** Wrapper for std::log( contained_functor_value ).
     */
    template <detail::FunctorOperatorArg F>
    LHCB_FUNCTORS_INLINE constexpr auto log( F&& f ) {
      if constexpr ( detail::FunctorConstant<F> ) {
        using std::log;
        return NumericValue{ log( f ) };
      } else {
        return detail::compose<Function, Function>(
            []( auto x ) {
              using std::log;
              return log( x );
            },
            std::forward<F>( f ) );
      }
    }

    /** Wrapper for std::exp( contained_functor_value ).
     */
    template <detail::FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto exp( F&& f ) {
      return detail::compose<Function, Function>(
          []( auto x ) {
            using std::exp;
            return exp( x );
          },
          std::forward<F>( f ) );
    }

    /** Wrapper for std::sqrt( contained_functor_value ).
     */
    template <detail::FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto sqrt( F&& f ) {
      return detail::compose<Function, Function>(
          []( auto x ) {
            using std::sqrt;
            return sqrt( x );
          },
          std::forward<F>( f ) );
    }

    /** Wrapper for std::acos( contained_functor_value ).
     */
    template <detail::FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto arccos( F&& f ) {
      return detail::compose<Function, Function>(
          []( auto x ) {
            using std::acos;
            using LHCb::Utils::as_arithmetic;
            return acos( as_arithmetic( x ) );
          },
          std::forward<F>( f ) );
    }

    /** Wrapper for (cond) ? true_fun : false_fun.
     */
    template <detail::FunctorPredicate Pred, detail::FunctorFunction F2, detail::FunctorFunction F3>
    LHCB_FUNCTORS_INLINE constexpr auto where( Pred&& cond, F2&& true_fun, F3&& false_fun ) {
      return detail::promote_and_compose<Function>(
          []( auto&& arg1, auto arg2, auto arg3 ) { return arg1 ? arg2 : arg3; }, std::forward<Pred>( cond ),
          std::forward<F2>( true_fun ), std::forward<F3>( false_fun ) );
    }

    /** Wrapper for std::abs( contained_functor_value ).
     */
    template <detail::FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto abs( F&& f ) {
      return detail::compose<Function, Function>(
          []( auto x ) {
            using std::abs;
            return abs( x );
          },
          std::forward<F>( f ) );
    }

    /** Wrapper for Similarity( covMatrix, vec ).
     */
    template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
    LHCB_FUNCTORS_INLINE constexpr auto similarity( F1&& vec, F2&& covMatrix ) {
      return detail::promote_and_compose<Function>(
          []( auto&& vec, auto&& cov ) {
            return similarity( std::forward<decltype( vec )>( vec ), std::forward<decltype( cov )>( cov ) );
          },
          std::forward<F1>( vec ), std::forward<F2>( covMatrix ) );
    }

    /** More efficient shorthand for (f > x) && (f < y) that only evaluates
     *  the value (f) once.
     */
    template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArg F2, detail::FunctorOperatorArgs3<F1, F2> F3>
    LHCB_FUNCTORS_INLINE constexpr auto in_range( F1&& f1, F2&& f2, F3&& f3 ) {
      if constexpr ( detail::FunctorConstant<F1> && detail::FunctorConstant<F3> ) {
        // presumably the most common case
        return detail::compose<Function, Predicate>(
            [min = f1, max = f3]( auto&& val ) { return ( val > min ) && ( val < max ); }, std::forward<F2>( f2 ) );
      } else {
        return detail::promote_and_compose<Predicate>(
            []( auto&& min, auto&& val, auto&& max ) { return ( val > min ) && ( val < max ); }, std::forward<F1>( f1 ),
            std::forward<F2>( f2 ), std::forward<F3>( f3 ) );
      }
    }

    /** Predicate returning true if bitwise AND between two contained_functor_value objects is non-zero.
     */
    template <detail::FunctorOperatorArg F1, detail::FunctorOperatorArgs<F1> F2>
    LHCB_FUNCTORS_INLINE constexpr auto test_bit( F1&& lhs, F2&& rhs ) {
      return bitwise_and( std::forward<F1>( lhs ), std::forward<F2>( rhs ) ) != 0;
    }

    /* Sign function*/
    // Returns 1 if x > 0, -1 if x < 0,   FIXME?? and 0 if x is zero.
    template <detail::FunctorFunction F>
    LHCB_FUNCTORS_INLINE constexpr auto sign( F&& f ) {
      return detail::compose<Function, Function>(
          []( auto arg ) {
            using std::copysign;
            return copysign( 1.f, arg );
          },
          std::forward<F>( f ) );
    };

  } // namespace math
} // namespace Functors
