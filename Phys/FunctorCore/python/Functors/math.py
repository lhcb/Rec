###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""@module Functors
Mathematical functions for use in functor expressions.
"""

from Functors.common import top_level_namespace
from Functors.grammar import Functor, WrappedBoundFunctor


def log(functor):
    """Wrap the math::log functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.log(PT) > 1.0
        ('operator>( ::Functors::math::log( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) ), float{1.0} )', ['float{1.0}'], '( ::Functors::math::log( ( RHO_COORDINATE @ THREEMOMENTUM ) ) > 1.0 )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::log", functor)


def exp(functor):
    """Wrap the math::exp functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.exp(PT)
        ('::Functors::math::exp( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) )', '::Functors::math::exp( ( RHO_COORDINATE @ THREEMOMENTUM ) )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::exp", functor)


def pow(functor, power):
    """Wrap the math::pow functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.pow(PT, 0.2)
        ('::Functors::math::pow( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ), float{0.2} )', ['float{0.2}'], '::Functors::math::pow( ( RHO_COORDINATE @ THREEMOMENTUM ), 0.2 )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::pow", functor, power)


def similarity(vec, cov_matrix):
    """Wrap for "Similarity" functor. Given vector "vec" of length "n" and covariance matrix of size "nxn", similarity computes: vec.transpose() * cov_matrix * vec

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import THREEMOMENTUM, THREE_MOM_COV_MATRIX, P
        >>> fmath.similarity(THREEMOMENTUM/P, THREE_MOM_COV_MATRIX)
        ('::Functors::math::similarity( operator/( ::Functors::Track::ThreeMomentum, Functors::chain( ::Functors::Common::Magnitude, ::Functors::Track::ThreeMomentum ) ), ::Functors::Particle::threeMomCovMatrix )', '::Functors::math::similarity( ( THREEMOMENTUM / ( MAGNITUDE @ THREEMOMENTUM ) ), THREE_MOM_COV_MATRIX )')
    """
    return WrappedBoundFunctor(
        top_level_namespace + "math::similarity", vec, cov_matrix
    )


def sign(functor):
    """Wrap the math::sign functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.sign(PT) > 0
        ('operator>( ::Functors::math::sign( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) ), int{0} )', ['int{0}'], '( ::Functors::math::sign( ( RHO_COORDINATE @ THREEMOMENTUM ) ) > 0 )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::sign", functor)


def sqrt(functor):
    """Wrap the math::sqrt functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.sqrt(PT)
        ('::Functors::math::sqrt( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) )', '::Functors::math::sqrt( ( RHO_COORDINATE @ THREEMOMENTUM ) )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::sqrt", functor)


def arccos(functor):
    """Wrap the math::arccos functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.arccos(PT)
        ('::Functors::math::arccos( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) )', '::Functors::math::arccos( ( RHO_COORDINATE @ THREEMOMENTUM ) )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::arccos", functor)


def where(condition, functor_a, functor_b):
    """Wrap the math::where functor.
    If condition is true, return functor_a result is returned, else return functor_b result.

    Example usage::
        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.where(PT > 0.5, 2 * PT, PT)
        ('::Functors::math::where( operator>( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ), float{0.5} ), operator*( int{2}, Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) ), Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) )', ['float{0.5}', 'int{2}'], '::Functors::math::where( ( ( RHO_COORDINATE @ THREEMOMENTUM ) > 0.5 ), ( 2 * ( RHO_COORDINATE @ THREEMOMENTUM ) ), ( RHO_COORDINATE @ THREEMOMENTUM ) )')
    """
    return WrappedBoundFunctor(
        top_level_namespace + "math::where", condition, functor_a, functor_b
    )


def abs(functor):
    """Wrap the math::abs functor.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import PT
        >>> fmath.abs(PT)
        ('::Functors::math::abs( Functors::chain( ::Functors::Common::Rho_Coordinate, ::Functors::Track::ThreeMomentum ) )', '::Functors::math::abs( ( RHO_COORDINATE @ THREEMOMENTUM ) )')
    """
    return WrappedBoundFunctor(top_level_namespace + "math::abs", functor)


def in_range(min_val, functor, max_val):
    """Wrap the math::in_range functor.

    This is efficient and only evaluates the functor once.

    Example usage::

        >>> import Functors.math as fmath
        >>> from Functors import MASS
        >>> fmath.in_range(1500, MASS, 2500)
        ('::Functors::math::in_range( int{1500}, ::Functors::Composite::Mass, int{2500} )', ['int{1500}', 'int{2500}'], '::Functors::math::in_range( 1500, MASS, 2500 )')
    """
    return WrappedBoundFunctor(
        top_level_namespace + "math::in_range", min_val, functor, max_val
    )


def test_bit(functor_a, functor_b):
    return WrappedBoundFunctor(
        top_level_namespace + "math::test_bit", functor_a, functor_b
    )


# The Gaudi testing machinery assumes that any python function with test in its name is a test. The next line avoids the spurious testing of the test_bit functor
test_bit.__test__ = False


def boost_to(v1, ref):
    """Boost v1 4-vector into the rest frame of ref 4-vector"""
    # return WrappedBoundFunctor('LHCbMath::boost_to', v1, ref)
    return Functor(
        "BOOST_TO",
        "LHCbMath::boost_to",
        "Boosts a 4-vector into the rest frame of another 4-vector",
    ).bind(v1, ref)


def boost_from(v1, ref):
    """Boost v1 4-vector from the rest frame of ref 4-vector"""
    return Functor(
        "BOOST_FROM",
        "LHCbMath::boost_from",
        "Boosts a 4-vector from the rest frame of another 4-vector",
    ).bind(v1, ref)
