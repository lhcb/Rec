/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <algorithm>
#include <vector>

#include <boost/mp11.hpp>

#include "Event/Particle.h"
#include "Kernel/GetDecay.h"
#include "Kernel/ICheckOverlap.h"
#include "Kernel/IDecodeSimpleDecayString.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/IParticlePropertySvc.h"
#include "TrackKernel/PrimaryVertexUtils.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "SelTools/Utilities.h"

#include "LHCbAlgs/Transformer.h"

#include "Functors/with_functors.h"

namespace Combiner {
  namespace constants {
    // TODO optimise based on real-world usage
    static constexpr auto max_outcontainer_size = 200;
  } // namespace constants

  // Get the name of the Mth combination cut of an N-particle combiner
  // Very ugly, but not worth the effort to do something beautiful and constexpr
  template <std::size_t N, std::size_t M>
  std::string combinationCutName() {
    static_assert( N >= 2 );
    static_assert( M + 1 < N );
    std::string name{ "Combination" };
    if ( M + 2 < N ) {
      // M = 0 => "12"
      // M = 1 => "123"
      // etc.
      name += "12";
      for ( auto i = 1ul; i <= M; ++i ) { name += std::to_string( i + 2 ); }
    }
    name += "Cut";
    return name;
  }

  // Tag types for combination functors
  template <typename... Ts>
  struct CombTypes {
    static constexpr auto N = sizeof...( Ts );
    static_assert( N >= 2 );

    // Adapted from https://stackoverflow.com/a/45172773/596068
    template <template <typename...> class C, std::size_t M, typename... T, std::size_t... I>
    static std::enable_if_t<( M <= sizeof...( T ) ), C<std::tuple_element_t<I, std::tuple<T...>>...>>
        sub( std::index_sequence<I...> );

    template <template <typename...> class C, std::size_t M, typename... T>
    using subpack = decltype( sub<C, M, T...>( std::make_index_sequence<M>{} ) );

    // Helper to return a ParticleCombination with the first M elements of the
    // parameter pack `Ts`
    template <std::size_t M>
    using Combination = subpack<LHCb::ParticleCombination, M, Ts...>;

    // Combination12Cut (M = 0), Combination123Cut (M = 1), ..., CombinationCut (M = N-1)
    template <std::size_t M>
    struct Cut {
      static_assert( M < N );
      using Signature                 = bool( Combination<M + 2> const& );
      inline static auto PropertyName = combinationCutName<N, M>();
    };
  };

  // Tag types for composite functors (acting on post-vertex-fit particles)
  template <typename T>
  struct CompositeTypes {
    struct Cut {
      using Signature                    = bool( T const& );
      constexpr static auto PropertyName = "CompositeCut";
    };
  };

  namespace types {
    using Particle_t = LHCb::Particle;
    using Vertex_t   = std::remove_pointer<decltype( LHCb::Particle{}.endVertex() )>::type;

    using OutVertices_t  = Vertex_t::Container;
    using OutParticles_t = Particle_t::Container;
    using Output_t       = std::tuple<OutParticles_t, OutVertices_t>;
    using FilterOutput_t = std::tuple<bool, OutParticles_t, OutVertices_t>;
    using CompositeType  = CompositeTypes<Particle_t>;

    template <typename T>
    using ToRange = typename T::Range;

    template <typename... Ts>
    struct base_helper {
      // Assert that all members of the parameter pack have the same type and
      // that that type is LHCb::Particle
      using first_t = std::tuple_element_t<0, std::tuple<Ts...>>;
      static_assert( std::conjunction_v<std::is_same<first_t, Ts>...> );
      static_assert( std::conjunction_v<std::is_same<first_t, Particle_t>> );

      // Shorthand
      using FilterTransform =
          LHCb::Algorithm::MultiTransformerFilter<Output_t( ToRange<Ts> const&..., LHCb::PrimaryVertices const&,
                                                            DetectorElement const& ),
                                                  LHCb::Algorithm::Traits::usesConditions<DetectorElement>>;

      // Bake T and N into aliases that we can pass to mp_product.
      template <std::size_t M>
      using CombCut = typename CombTypes<Ts...>::template Cut<M>;
      template <typename M>
      using _CombCut = CombCut<M::value>;
      // Make with_functors<FilterTransform<Ts...>,
      //                    [cartesian expansion of CombinationCut<A>...],
      //                    CompositeCut>
      // Where A is an integer from 0 to N-2
      using type = boost::mp11::mp_append<
          with_functors<FilterTransform>,
          boost::mp11::mp_product<_CombCut,
                                  boost::mp11::mp_from_sequence<std::make_index_sequence<sizeof...( Ts ) - 1>>>,
          boost::mp11::mp_list<CompositeType::Cut>>;
    };

    template <typename... Ts>
    using base_t = typename base_helper<Ts...>::type;
  } // namespace types

  class CheckOverlap {
  public:
    /**
     * @brief Specialised for 2-Combination alternative to CheckOverlap tool avoiding allocations.
     * (see https://gitlab.cern.ch/lhcb/MooreOnline/-/issues/77#note_7996796)
     *
     * @details reserve space for 16 leaves in the decay tree of the particle in the combination
     *          number is arbitrary, should be large enough to basically never re-allocate
     */
    CheckOverlap( const ICheckOverlap* tool = nullptr ) : m_tool{ tool } {
      if ( !m_tool ) { m_protos.reserve( 16 ); }
    }

    /**
     * @brief Check if particles have common ancestors.
     *
     * @return true if overlapping
     * @return false if no common ancestors
     */
    bool findOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2 ) {
      if ( !particle1 || !particle2 ) {
        throw GaudiException( "Particle is nullptr.", __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
      if ( m_tool ) { return m_tool->foundOverlap( particle1, particle2 ); }
      return findOverlapImpl( particle1, particle2 );
    }

  private:
    /**
     * @brief Search for common ancestors among all the two particles' proto particles.
     */
    bool findOverlapImpl( const LHCb::Particle* particle1, const LHCb::Particle* particle2 ) {

      if ( particle1->isBasicParticle() && particle2->isBasicParticle() ) {
        return findOverlapBasics( particle1->proto(), particle2->proto() );
      }

      m_protos.clear();
      collectAllProtos( particle1 );
      collectAllProtos( particle2 );

      for ( auto proto1 = m_protos.begin(); proto1 != m_protos.end(); ++proto1 ) {
        for ( auto proto2 = std::next( proto1 ); proto2 != m_protos.end(); ++proto2 ) {
          if ( findOverlapBasics( *proto1, *proto2 ) ) { return true; }
        }
      }
      return false;
    }

    /**
     * @brief Check if basic proto particles overlap.
     *
     * @return true if pointer to protoparticle is same or underlying track is same
     * @return false else
     */
    bool findOverlapBasics( const LHCb::ProtoParticle* proto1, const LHCb::ProtoParticle* proto2 ) const {
      // We should only perform a track comparison with non-null track pointers,
      // else we will determine neutrals to overlap with each other (they have nullptr tracks)
      // to avoid this it's enough to check one proto for nullptr track
      return ( proto1 == proto2 ) || ( proto1->track() && ( proto1->track() == proto2->track() ) ) ||
             ( proto1->neutralPID() && ( proto1->neutralPID() == proto2->neutralPID() ) );
    }

    /**
     * @brief Recursively get all leaves (proto particles) from particle's ancestors.
     *
     */
    void collectAllProtos( const LHCb::Particle* particle ) {
      if ( particle->isBasicParticle() ) {
        assert( particle->proto() );
        m_protos.push_back( particle->proto() );
      } else {
        for ( const auto* p : particle->daughtersVector() ) { collectAllProtos( p ); }
      }
    }

  private:
    const ICheckOverlap* m_tool{ nullptr };
    // storage for all proto particles related to the particles
    // one goal of this implementation is to reuse this memory instead of
    // allocating at every overlap check like the CheckOverlap tool
    std::vector<const LHCb::ProtoParticle*> m_protos{};
  };

} // namespace Combiner

template <typename... InputTypes>
class NBodyParticleCombiner final : public Combiner::types::base_t<InputTypes...> {
public:
  /// Number of children in the decay tree we'll be reconstructing
  static constexpr auto N = sizeof...( InputTypes );
  using base_t            = typename Combiner::types::base_t<InputTypes...>;
  using KeyValue          = typename base_t::KeyValue;
  template <typename T>
  using InputContainer = typename Combiner::types::ToRange<T>;
  template <std::size_t M>
  using CombCut      = typename Combiner::types::base_helper<InputTypes...>::template CombCut<M>;
  using CompositeCut = typename Combiner::types::CompositeType::Cut;
  // Container type used to store indices into the input containers
  using index_vector_t = std::vector<int>;

  NBodyParticleCombiner( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t{ name,
                pSvcLocator,
                input_names( std::index_sequence_for<InputTypes...>{} ),
                { KeyValue{ "OutputParticles", "" }, KeyValue{ "OutputVertices", "" } } } {}

  /** @brief Return an array of names for our N input properties.
   *
   * Returns Input1, ..., InputN, starting from 1 to match the indices shown in
   * the combination cuts (e.g. Combination12Cut).
   */
  template <std::size_t... Is>
  static auto input_names( std::index_sequence<Is...> ) {
    return std::make_tuple( KeyValue{ "Input" + std::to_string( Is + 1 ), "" }..., KeyValue{ "PrimaryVertices", "" },
                            KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } );
  }

  StatusCode initialize() override {
    StatusCode sc = base_t::initialize();
    if ( sc.isFailure() ) { return sc; }

    // We will parse at most two decays to reconstruct: one if the descriptor is
    // just `...`, and two if its `[...]cc`
    m_decays = Decays::decays( m_decay_descriptor, &*m_decay_descriptor_decoder );
    if ( m_decays.size() < 1 ) {
      throw GaudiException( "CHECK YOUR DESCRIPTOR: '" + m_decay_descriptor + "' cannot be parsed.", this->name(),
                            StatusCode::FAILURE );
    }
    if ( m_decays.size() > 2 ) {
      throw GaudiException( "CHECK YOUR DESCRIPTOR: '" + m_decay_descriptor +
                                "' doesn't fit the condition that max 2 decays are allowed in one descriptor.",
                            this->name(), StatusCode::FAILURE );
    }

    // For each decay to reconstruct, validate that:
    // 1. Number of children in descriptor == `N`
    // 2. Identical PIDs appear consecutively
    // 3. the CC's don't create duplications of combiners
    std::vector<std::map<int, int>> children_content;
    for ( auto const& decay : m_decays ) {
      std::map<int, int> decay_children;
      auto const&        children = decay.children();
      if ( children.size() != N ) {
        throw std::runtime_error( "Cannot combine " + std::to_string( children.size() ) + " children in a " +
                                  std::to_string( N ) + "-body combiner" );
      }
      std::set<int> pids{ children[0].pid().pid() };
      for ( uint i = 1; i < N; ++i ) {
        int pid = children[i].pid().pid();
        if ( pids.count( pid ) && children[i - 1].pid().pid() != pid ) {
          throw std::runtime_error( std::string{ "Children with idential PID " } +
                                    "must appear consecutively in the decay descriptor; e.g.:\n" +
                                    "YES: B+ -> pi+ pi+ pi-\n" + "NO:  B+ -> pi+ pi- pi+\n" );
        }
        pids.emplace( pid );
      }

      for ( auto const& child : children ) { decay_children[child.pid().pid()]++; }

      // checking the overlap of the children content between the descriptors
      for ( auto& child_map : children_content ) {
        bool no_op_flag = true;
        for ( auto const& child_element : decay_children ) {
          if ( child_element.second != child_map[child_element.first] ) {
            no_op_flag = false;
            break;
          }
        }
        if ( no_op_flag ) {
          this->warning() << "CHECK YOUR DESCRIPTOR: charge combination in '" + m_decay_descriptor +
                                 "' leads to duplication!!";
        }
      }
      children_content.push_back( decay_children );
    }
    return sc;
  }

  Combiner::types::FilterOutput_t operator()( InputContainer<InputTypes> const&... particles,
                                              LHCb::PrimaryVertices const& pvs,
                                              DetectorElement const&       lhcb ) const override {
    // Create a parameter pack for manipulating the N-1 combination cuts
    return combine( particles..., *lhcb.geometry(), pvs, std::make_index_sequence<N - 1>{} );
  }

private:
  struct combinatorics {
    std::size_t candidates{ 0 };
    std::size_t combinations{ 0 };
  };

  template <std::size_t... ICombinationCuts>
  Combiner::types::FilterOutput_t combine( InputContainer<InputTypes> const&... particles,
                                           IGeometryInfo const& geometry, LHCb::PrimaryVertices const& pvs,
                                           std::index_sequence<ICombinationCuts...> ) const {
    using namespace Combiner::types;

    // Retrieve the combination and composite cuts
    auto const& unprepared_combination_cuts = this->template getFunctors<CombCut<ICombinationCuts>...>();
    auto const& unprepared_composite_cut    = this->template getFunctor<CompositeCut>();

    // Create the output, reserving enough space for the 'typical' number of
    // passing candidates
    Output_t out;
    std::get<OutParticles_t>( out ).reserve( Combiner::constants::max_outcontainer_size );
    std::get<OutVertices_t>( out ).reserve( Combiner::constants::max_outcontainer_size );

    process_decays( particles..., out,
                    std::tuple{ std::get<ICombinationCuts>( unprepared_combination_cuts ).prepare()... },
                    unprepared_composite_cut.prepare(), geometry, pvs,
                    // Buffer counters to reduce atomic operations
                    std::tuple{ m_npassed_combination_cuts[ICombinationCuts].buffer()... },
                    m_npassed_vertex_fit.buffer(), m_npassed_composite_cut.buffer(),
                    // Generate some integer packs for convenience
                    std::index_sequence<ICombinationCuts...>{}, std::index_sequence_for<InputTypes...>{} );

    const auto filter_passed = !std::get<OutParticles_t>( out ).empty();
    m_npassed += filter_passed;
    if ( filter_passed ) m_ncand_per_event += std::get<OutParticles_t>( out ).size();
    return std::tuple_cat( std::make_tuple( filter_passed ), std::move( out ) );
  }

  template <typename CombinationCuts, typename CompositeCut, typename Counter, typename Counters,
            std::size_t... ICombinationCuts, std::size_t... IInput>
  void process_decays( InputContainer<InputTypes> const&... particles, Combiner::types::Output_t& storage,
                       CombinationCuts combination_cuts, CompositeCut composite_cut, IGeometryInfo const& geometry,
                       LHCb::PrimaryVertices const& pvs, Counters npassed_combination_cuts, Counter npassed_vertex_fit,
                       Counter npassed_composite_cut, std::index_sequence<ICombinationCuts...>,
                       std::index_sequence<IInput...> ) const {
    // Cannot use a structured binding (auto& [ps, vs] = storage) because Clang
    // cannot compile the vertex fit lambda when it uses a variable from a
    // structured binding (see LLVM bug #39963)
    auto& output_particles = std::get<Combiner::types::OutParticles_t>( storage );
    auto& output_vertices  = std::get<Combiner::types::OutVertices_t>( storage );

    // keep track of the number of accepted candidates and break as soon as there are more than m_max_combinations
    combinatorics per_event{};

    if ( this->msgLevel( MSG::DEBUG ) ) {
      ( ( this->debug() << "Container #" << IInput << ": " << &( particles )
                        << "; first element: " << ( particles.empty() ? nullptr : particles.front() ) << endmsg ),
        ... );
    }
    // Record input container sizes
    ( ( m_ninput_particles[IInput] += particles.size() ), ... );

    // Lambda that handles N-body combinations once we've got that far
    // `indices`: indices into the input containers of the members of this
    //            N-body combination
    // `decay_index`: indices into `m_decays` of the decay descriptor
    //                       this N-body combination was produced from
    auto do_vertex_fit = [&]( std::array<int, N> const& indices, const int decay_index ) {
      // Prepare for the vertex fit
      const auto& pid      = m_decays[decay_index].mother().pid();
      auto        particle = std::make_unique<Combiner::types::Particle_t>( pid );
      auto        vertex   = std::make_unique<Combiner::types::Vertex_t>();
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Promoting " << N << "-body combination to composite, "
                      << "indices = " << indices << ", decay index = " << decay_index << ", parent PID = " << pid.pid()
                      << endmsg;
      }

      // Load the children from the input containers
      LHCb::Particle::ConstVector children{ [&]( auto const& container, auto const& index ) {
        return container[index];
      }( particles, std::get<IInput>( indices ) )... };
      // Run the fit
      vertex->setNDoF( -1 );
      auto const fit_success = ( m_vertex_fitter->combine( children, *particle, *vertex, geometry ) ).isSuccess();
      // Record the fit success statistics.
      npassed_vertex_fit += fit_success;
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Vertex fit " << ( fit_success ? "passed" : "failed" ) << endmsg;
      }

      if ( fit_success ) {
        // MomentumCombiner and ParticleAdder will not set the endVertex pointer since they don't actually run a vertex
        // fit. We could then as well not store the vertex.
        if ( vertex.get() != particle->endVertex() ) vertex.reset();

        Sel::Utils::setPVForComposite( *particle, vertex.get(), pvs );

        auto const composite_cut_success = composite_cut( *particle );
        npassed_composite_cut += composite_cut_success;
        per_event.candidates += composite_cut_success ? 1u : 0u;
        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Composite cut " << ( fit_success ? "passed" : "failed" ) << endmsg;
        }
        if ( composite_cut_success ) {
          output_particles.add( particle.release() );
          if ( vertex ) output_vertices.add( vertex.release() );
        }
      }
    };

    for ( auto n_decay = 0u; n_decay < m_decays.size(); ++n_decay ) {
      auto const& decay = m_decays[n_decay];
      // Collect indices of particles in the input containers that satisfy
      // the constraints imposed by the decay descriptor. e.g. if the 0th
      // input container is a mix of D0 and D~0 then get the indices of the
      // D0s. If the Nth input is the same as the N-1th input then returns a
      // null std::optional indicating that a "triangle" loop should be used
      // TODO(AP) could hoist this outside the loop for the first iteration, and on
      // the second iteration we can re-use the indices in the case where the
      // PID is the same (the child is self-conjugate)
      std::array const matching_indices_storage{ get_indices<IInput>( particles..., decay )... };
      // See if we can shortcut -- if we got zero entries for any compulsory
      // input then we can abort processing here
      if ( std::any_of( matching_indices_storage.begin(), matching_indices_storage.end(),
                        []( auto const& opt_indices ) { return opt_indices.has_value() && opt_indices->empty(); } ) ) {
        continue;
      }
      // Cache which list of indices we will actually use in each slot
      std::array const matching_indices{ [&]( std::size_t InputIndex ) {
        // get_indices indicated that we can use the previous (InputIndex - 1) container
        bool const do_triangle_loop = !matching_indices_storage[InputIndex].has_value();
        if ( do_triangle_loop ) {
          // Look in indices below InputIndex for a populated entry in matching_indices_storage
          assert( InputIndex > 0 );
          for ( std::size_t index_to_try = InputIndex - 1;; --index_to_try ) {
            if ( matching_indices_storage[index_to_try].has_value() ) {
              // Found the match
              return std::pair{ do_triangle_loop, std::cref( matching_indices_storage[index_to_try].value() ) };
            }
            assert( index_to_try != 0 );
          }
          assert( false ); // we should never get here
        }
        return std::pair{ do_triangle_loop, std::cref( matching_indices_storage[InputIndex].value() ) };
      }( IInput )... };

      // Defer to another function to actually generate the combinations
      process_combinations( particles..., n_decay, matching_indices, do_vertex_fit, combination_cuts,
                            npassed_combination_cuts, geometry, std::index_sequence<ICombinationCuts...>{},
                            std::make_index_sequence<sizeof...( ICombinationCuts ) - 1>{}, per_event );
    }
  }

  template <typename ArrayOfIndexVectors, typename CombinationCuts, typename CombinationCutCounters, typename VertexFit,
            std::size_t... ICombinationCuts, std::size_t... ICombinationCutsMinusOne>
  void process_combinations( InputContainer<InputTypes> const&... particles, int n_decay,
                             ArrayOfIndexVectors const& matching_indices, VertexFit& do_vertex_fit,
                             CombinationCuts const& combination_cuts, CombinationCutCounters& npassed_combination_cuts,
                             IGeometryInfo const& geometry, std::index_sequence<ICombinationCuts...>,
                             std::index_sequence<ICombinationCutsMinusOne...>,
                             combinatorics& /* has to be passed by reference! */ per_event ) const {
    // Indicies into the first container
    auto const& indices0 = matching_indices[0].second.get();
    // Indicies into the second container
    auto const& indices1 = matching_indices[1].second.get();
    // Whether we should do a 'triangle loop' to generate all 2-body combinations of the above indicies
    bool const do_triangle_loop = matching_indices[1].first;
    // The logic in get_indices should never mark the first container for the triangle loop (it has no preceeding
    // container)
    assert( !matching_indices[0].first );
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "indices0.size() == " << indices0.size() << ", indices1.size() == " << indices1.size()
                    << ", do_triangle_loop == " << do_triangle_loop << endmsg;
    }
    auto check_overlap = Combiner::CheckOverlap{ m_check_overlap.get() };
    // We will need these objects for all combinations passing the overlap cut,
    // which is likely to be most combinations, so create them once outside the
    // loop
    auto const& comb_cut         = std::get<0>( combination_cuts );
    auto&       npassed_comb_cut = std::get<0>( npassed_combination_cuts );

    auto const inputs = std::tuple{ particles... };
    for ( auto i0 = 0u; i0 < indices0.size(); ++i0 ) {
      // Load the particles matching the first child of the current decay
      // descriptor from the first container
      auto const p0_index = indices0[i0];
      auto const p0       = std::get<0>( inputs )[p0_index];
      for ( auto i1 = do_triangle_loop ? i0 + 1 : 0; i1 < indices1.size(); ++i1 ) {
        auto const       p1_index = indices1[i1];
        auto const       p1       = std::get<1>( inputs )[p1_index];
        std::array const combination_indices{ p0_index, p1_index };
        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Processing 2-body combination; indices = " << combination_indices << endmsg;
        }

        const auto has_overlap = check_overlap.findOverlap( p0, p1 );

        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Overlap check " << ( has_overlap ? "failed" : "passed" ) << endmsg;
        }

        auto       combination     = LHCb::ParticleCombination<InputTypes...>{ p0, p1 }; // todo: remove template args
        auto const passed_comb_cut = !has_overlap && comb_cut( combination );
        npassed_comb_cut += passed_comb_cut;
        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Combination12Cut " << ( passed_comb_cut ? "passed" : "failed" ) << endmsg;
        }
        ++per_event.combinations;
        if ( per_event.combinations >= m_max_combinations ) { break; }
        if ( !passed_comb_cut ) { continue; }

        if constexpr ( N == 2 ) {
          // We now have a fully selected N-body combination, so run the vertex fit
          std::invoke( do_vertex_fit, combination_indices, n_decay );
        } else {
          // Otherwise, we need to add another particle to these combinations
          extend_combination( particles..., n_decay, matching_indices, combination_indices, do_vertex_fit,
                              combination_cuts, npassed_combination_cuts, geometry, std::make_index_sequence<2>{},
                              per_event, check_overlap );
        }
        if ( per_event.candidates >= m_max_candidates ) break;
      }
      if ( per_event.combinations >= m_max_combinations ) {
        ++m_many_combinations;
        break;
      }
      if ( per_event.candidates >= m_max_candidates ) {
        ++m_many_candidates;
        break;
      }
    }
  }

  /** Add one more child to an M-body combination.
   *
   * The `IParticle` index has the size of the M-body combinations which have
   * already been built and survived the corresponding combination cuts.
   *
   * `NBodies` is the size of the incoming combinations, i.e. the B-body
   * combinations that we will try to promote to an M+1-body combination.  If
   * the promotion is successful and M == N the combination will enter the
   * vertex fit, otherwise this method recursively extends the new combination
   * until that condition is reached.
   */
  template <std::size_t... IParticle, typename CombinationCuts, typename VertexFit, typename CombinationCutCounters,
            std::size_t NBodies, typename ArrayOfIndexVectors>
  void extend_combination( InputContainer<InputTypes> const&... particles, int n_decay,
                           ArrayOfIndexVectors const&      matching_indices,
                           std::array<int, NBodies> const& current_combination_indices, VertexFit const& do_vertex_fit,
                           CombinationCuts const& combination_cuts, CombinationCutCounters& npassed_combination_cuts,
                           IGeometryInfo const&       geometry, std::index_sequence<IParticle...>,
                           combinatorics& /*by ref!*/ per_event, Combiner::CheckOverlap& check_overlap ) const {
    // Combination indices are the indices into input containers of the elements
    // of the current combination
    // NBodies is the size of the combination we've been given and that we will
    // try to extend
    static_assert( NBodies == sizeof...( IParticle ) );
    static_assert( NBodies >= 2 );
    static_assert( NBodies < N );
    static_assert( std::tuple_size_v<CombinationCuts> == N - 1 );
    static_assert( std::tuple_size_v<CombinationCutCounters> == N - 1 );
    // Size of the combination we will build in this call
    constexpr std::size_t NBodiesExtended = NBodies + 1;
    // Get the index of the combination cut that will act on this type
    constexpr std::size_t CombCutIndex = NBodies + 1 - 2;

    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "Promoting " << NBodies << "-body combination, indices = " << current_combination_indices
                    << endmsg;
    }
    bool const            do_triangle_loop = std::get<0>( matching_indices[NBodies] );
    index_vector_t const& new_indices      = std::get<1>( matching_indices[NBodies] );
    // If the previous container is identical (i.e. do_triangle_loop is true)
    // then we only need to consider candidates after the last candidate in the
    // current combination
    std::size_t const first_index = [&]() -> std::size_t {
      if ( do_triangle_loop ) {
        return std::distance( new_indices.begin(), std::upper_bound( new_indices.begin(), new_indices.end(),
                                                                     current_combination_indices.back() ) );
      } else {
        return 0;
      }
    }();
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "indices" << NBodies << ".size() = " << new_indices.size()
                    << ", do_triangle_loop = " << do_triangle_loop << ", starting index = " << first_index << endmsg;
    }

    // We will need these objects for all combinations passing the overlap cut,
    // which is likely to be most combinations, so create them once outside the
    // loop
    auto const& comb_cut         = std::get<CombCutIndex>( combination_cuts );
    auto&       npassed_comb_cut = std::get<CombCutIndex>( npassed_combination_cuts );

    // Loop over the new additions to the input combinations
    auto const inputs = std::tuple{ particles... };
    for ( auto i = first_index; i < new_indices.size(); ++i ) {
      auto const                             pN1_index = new_indices[i];
      auto const                             pN1       = std::get<NBodies>( inputs )[pN1_index];
      std::array<int, NBodiesExtended> const extended_combination_indices{
          []( auto const index ) { return index; }( std::get<IParticle>( current_combination_indices ) )...,
          pN1_index };
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Processing " << NBodiesExtended
                      << "-body combination; indices = " << extended_combination_indices << endmsg;
      }
      // Update the combination children with the current set of particles
      auto combination = LHCb::ParticleCombination<InputTypes...>{
          std::get<IParticle>( inputs )[std::get<IParticle>( extended_combination_indices )]..., pN1 };

      // We only need to check the overlap between each child in the current
      // N-body object and the current particle (i.e. we don't need to check
      // the overlap *within* the N-body object; that has already been done)
      const auto has_overlap = ( check_overlap.findOverlap( &combination.template get<IParticle>(), pN1 ) || ... );

      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Overlap check " << ( has_overlap ? "failed" : "passed" ) << endmsg;
      }

      auto const passed_comb_cut = !has_overlap && comb_cut( combination );
      npassed_comb_cut += passed_comb_cut;
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << Combiner::combinationCutName<N, NBodies - 2>() << " cut "
                      << ( passed_comb_cut ? "passed" : "failed" ) << endmsg;
      }
      ++per_event.combinations;
      if ( per_event.combinations >= m_max_combinations ) { break; }
      if ( !passed_comb_cut ) { continue; }

      if constexpr ( NBodiesExtended == N ) {
        // We now have a fully selected N-body combination, so run the vertex fit
        std::invoke( do_vertex_fit, extended_combination_indices, n_decay );
      } else {
        // We need to add another particle to these combinations
        extend_combination( particles..., n_decay, matching_indices, extended_combination_indices, do_vertex_fit,
                            combination_cuts, npassed_combination_cuts, geometry,
                            std::make_index_sequence<NBodiesExtended>{}, per_event, check_overlap );
      }
      if ( per_event.candidates >= m_max_candidates ) break;
    }
  }

  // Collect relevant indices from the input containers, based on pid+charge as defined in the decay descriptor
  template <std::size_t IInput>
  auto get_indices( InputContainer<InputTypes> const&... particles, Decays::Decay const& decay ) const {
    using IterableInputTuple             = std::tuple<InputContainer<InputTypes>...>;
    auto const                    inputs = std::tuple{ particles... };
    std::optional<index_vector_t> opt_indices{ std::nullopt };
    auto const&                   input = std::get<IInput>( inputs );
    if constexpr ( IInput > 0 ) {
      // Not the first input, we need to figure out if this might be one of a
      // contiguous series of decay descriptor children of the same type. In
      // that case we have to generate a different kind of loop, and we have
      // to check that we were configured with a matching sequence of
      // identical inputs (mild EWWW :()
      if constexpr ( std::is_same_v<std::tuple_element_t<IInput - 1, IterableInputTuple>,
                                    std::tuple_element_t<IInput, IterableInputTuple>> ) {
        if ( decay.children()[IInput].pid() == decay.children()[IInput - 1].pid() && !m_allow_different_inputs ) {
          // Yes, in this case we should insist that the `IInput`th and
          // `IInput-1`th inputs were, in fact, the same...
          // There are some cases in which we don't want to enforce this (for example
          // when long-lived particles are involved). This can be swtiched-off via
          // the flag m_allow_different_inputs
          if ( !( std::get<IInput>( inputs ) == std::get<IInput - 1>( inputs ) ) ) {
            throw GaudiException{
                "Got contiguous children with the same PIDs, but the corresponding algorithm inputs "
                "were not the same. If this is intentional, please set AllowDiffInputsForSameIDChildren = True. ",
                "NBodyCombiner<name>", StatusCode::FAILURE };
          }
          // In this case there is no need to come up with a new list of
          // indices, we will make a "triangular" (need a better name) loop
          // using the indices from IInput - 1.
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << "Not looking at container " << &input << " as the previous container is identical"
                          << endmsg;
          }
          return opt_indices;
        }
      }
    }
    // We didn't fall into the special case, so we just loop through and
    // store indices. In this case we need an actual vector to return them in
    opt_indices.emplace();
    opt_indices->reserve( input.size() );
    auto const target_pid = decay.children()[IInput].pid().pid();
    auto       index      = 0;
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "Looking at container = " << &input << ", size = " << input.size() << " for PID = " << target_pid
                    << endmsg;
    }
    for ( auto const& particle : input ) {
      if ( this->msgLevel( MSG::VERBOSE ) ) {
        this->debug() << "Looking at container " << &input << " element " << &particle << " index " << index << "... ";
      }
      if ( particle->particleID().pid() == target_pid ) {
        opt_indices->push_back( index );
        if ( this->msgLevel( MSG::VERBOSE ) ) { this->debug() << "Matched" << endmsg; }
      } else {
        if ( this->msgLevel( MSG::VERBOSE ) ) { this->debug() << "Did not match" << endmsg; }
      }
      ++index;
    }
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "Container " << &input << " has " << opt_indices->size() << " matches: " << *opt_indices
                    << endmsg;
    }
    return opt_indices;
  }

  template <typename C, std::size_t... Ms>
  static std::array<C, sizeof...( Ms )> make_input_particle_counters( NBodyParticleCombiner* ptr,
                                                                      std::index_sequence<Ms...> ) {
    // No space in InputN to match the property name
    return { C{ ptr, "Input" + std::to_string( Ms + 1 ) + " size" }... };
  }

  template <typename C, std::size_t... Ms>
  static std::array<C, sizeof...( Ms )> make_combination_cut_counters( NBodyParticleCombiner* ptr,
                                                                       std::index_sequence<Ms...> ) {
    return { C{ ptr, "# passed " + Combiner::combinationCutName<N, Ms>() }... };
  }

  ToolHandle<IDecodeSimpleDecayString> m_decay_descriptor_decoder{ this, "DecayDecoder", "DecodeSimpleDecayString" };
  ToolHandle<ICheckOverlap>            m_check_overlap{ this, "OverlapTool", "" };
  ToolHandle<IParticleCombiner>        m_vertex_fitter{ this, "ParticleCombiner", "ParticleVertexFitter" };

  // get particle pids from names:
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropSvc{ this, "ParticlePropertySvc",
                                                               "LHCb::ParticlePropertySvc" };

  Gaudi::Property<std::string> m_decay_descriptor{ this, "DecayDescriptor", {}, "The decay topology to reconstruct." };
  // By defalut, don't allow contiguous children with the same PID to have different algorithm inputs
  Gaudi::Property<bool> m_allow_different_inputs{ this, "AllowDiffInputsForSameIDChildren", false };
  /// Limit the maximum number of candidates that pass the CompositeCut in an events
  Gaudi::Property<std::size_t> m_max_candidates{ this, "MaxComposites", 2000 };
  // Limit the maximum number of N-body combinations
  Gaudi::Property<std::size_t> m_max_combinations{ this, "MaxCombinations", 50e6 };
  /// Store decay objects parsed from the descriptor (this will be a maximum of
  /// two, in the case of a `[]cc` decay descriptor)
  std::vector<Decays::Decay> m_decays;

  using counter_t = Gaudi::Accumulators::BinomialCounter<>;
  /// Number of objects in each input container
  mutable std::array<Gaudi::Accumulators::SummingCounter<unsigned int>, N> m_ninput_particles{
      make_input_particle_counters<Gaudi::Accumulators::SummingCounter<unsigned int>>(
          this, std::make_index_sequence<N>{} ) };
  /// Number of combinations passing the combination cut; note that the overlap
  /// check *is counted* as a 'combination cut'
  mutable std::array<counter_t, N - 1> m_npassed_combination_cuts{
      make_combination_cut_counters<counter_t>( this, std::make_index_sequence<N - 1>{} ) };
  /// Number of successfully fitted combinations
  mutable counter_t m_npassed_vertex_fit{ this, "# passed vertex fit" };
  /// Number of post-vertex-fit particles passing the composite cut
  mutable counter_t                                     m_npassed_composite_cut{ this, "# passed CompositeCut" };
  mutable Gaudi::Accumulators::StatCounter<>            m_ncand_per_event{ this, "candidate multiplicity" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_many_candidates{
      this, "Too many composites. Stopping the combination.", 1 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_many_combinations{
      this, "Too many combinations. Stopping the combination.", 1 };
  /// Number of operator() calls with at least one candidate produced
  mutable counter_t m_npassed{ this, "# passed" };
};
