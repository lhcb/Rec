/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Functors/with_functors.h"
#include "Kernel/GetDecay.h"
#include "Kernel/IDecodeSimpleDecayString.h"
#include "SelKernel/ParticleCombination.h"
#include "SelKernel/Utilities.h"
#include "VertexFit/ParticleVertexFitter.h"

#include "Event/Particle_v2.h"
#include "Event/UniqueIDGenerator.h"
#include "Kernel/AllocatorUtils.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/Utils.h"

#include "LHCbAlgs/Transformer.h"

#include <tuple>

namespace ThOr::detail::Combiner {

  template <SIMDWrapper::InstructionSet Backend>
  using mask_b_v = typename SIMDWrapper::type_map_t<Backend>::mask_v;

  using Output_t = LHCb::Event::Composites;

  // Shorthand for below.
  template <typename... Ts>
  using FilterTransform = LHCb::Algorithm::MultiTransformerFilter<
      std::tuple<Output_t>( EventContext const&, LHCb::UniqueIDGenerator const&, Ts const&... ),
      Gaudi::Functional::Traits::BaseClass_t<LHCb::DetDesc::AlgorithmWithCondition<>>>;

  // Get the name of the Mth combination cut of an N-particle combiner
  // Very ugly, but not worth the effort to do something beautiful and constexpr
  template <std::size_t N, std::size_t M>
  std::string combinationCutName() {
    static_assert( N >= 2 );
    static_assert( M + 1 < N );
    std::string name{ "Combination" };
    if ( M + 2 < N ) {
      // M = 0 => "12"
      // M = 1 => "123"
      // etc.
      name += "12";
      for ( auto i = 1ul; i <= M; ++i ) { name += std::to_string( i + 2 ); }
    }
    name += "Cut";
    return name;
  }

  // Tag types for functors/dumping new
  template <SIMDWrapper::InstructionSet Backend, typename... Ts>
  struct Comb {
    using tup_ts = typename std::tuple<Ts...>;
    template <int I>
    using el_t = typename std::tuple_element_t<I, tup_ts>;
    // el_t<0> might be some variant<A, B>, in which
    // case there would be a visit() somewhere inside the combiner algorithm
    // and then the combination cut signatures would be things like
    // ParticleCombination<variant<AProxy, BProxy>, ...>
    // whereas if el_t<0> = A and el_t<1> = B, the
    // signature would be something like
    // ParticleCombination<AProxy, BProxy, ...>

    // If T = el_t<I> is variant<Ts...> this is tuple<Ts...>, otherwise it is
    // tuple<T>
    template <std::size_t I>
    using variant_types_t = typename LHCb::is_variant<el_t<I>>::tuple_type;

    // turn
    //   tuple<T, U, ...>
    // into
    //   variant<proxy_t<Backend, Behaviour, T>, proxy_t<Backend, Behaviour, U>, ...>
    // and strip the outer variant<...> if there is only one element
    template <LHCb::Pr::ProxyBehaviour Behaviour>
    struct get_proxy {
      template <typename T>
      using fn = typename LHCb::Event::simd_zip_t<Backend, T>::template zip_proxy_type<Behaviour>;
    };
    template <typename T>
    using unwrap_if_single = std::conditional_t<boost::mp11::mp_size<T>::value == 1, boost::mp11::mp_front<T>, T>;
    template <std::size_t I, LHCb::Pr::ProxyBehaviour Behaviour>
    using variant_proxies_t = unwrap_if_single<
        boost::mp11::mp_rename<boost::mp11::mp_transform_q<get_proxy<Behaviour>, variant_types_t<I>>, LHCb::variant>>;
    template <std::size_t I>
    using load_proxy_t = variant_proxies_t<I, LHCb::Pr::ProxyBehaviour::Contiguous>;
    template <std::size_t I>
    using bcast_proxy_t = variant_proxies_t<I, LHCb::Pr::ProxyBehaviour::ScalarFill>;
    template <std::size_t I>
    using gather_proxy_t = variant_proxies_t<I, LHCb::Pr::ProxyBehaviour::ScatterGather>;

    template <typename T>
    using help = gather_proxy_t<T::value>;

    template <std::size_t M>
    using Combination = std::conditional_t<
        M == 2, Sel::ParticleCombination<bcast_proxy_t<0>, gather_proxy_t<1>>,
        boost::mp11::mp_rename<
            boost::mp11::mp_append<
                boost::mp11::mp_product<help, boost::mp11::mp_from_sequence<std::make_index_sequence<M - 1>>>,
                Sel::ParticleCombination<bcast_proxy_t<M - 1>>>,
            Sel::ParticleCombination>>;

    // Combination12Cut (M = 0), Combination123Cut (M = 1), ..., CombinationCut (M = N-1)
    template <std::size_t M>
    struct Cut {
      using Signature = mask_b_v<Backend>( Functors::mask_arg_t, mask_b_v<Backend> const&, Combination<M + 2> const& );
      inline static auto const PropertyName = combinationCutName<sizeof...( Ts ), M>();
    };
  };

  template <SIMDWrapper::InstructionSet Backend = SIMDWrapper::Scalar>
  struct CompositeCut {
    using particle_t =
        typename LHCb::Event::simd_zip_t<Backend,
                                         Output_t>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
    using Signature = mask_b_v<Backend>( Functors::mask_arg_t, mask_b_v<Backend> const&, particle_t const& );
    constexpr static auto PropertyName = "CompositeCut";
  };

  template <SIMDWrapper::InstructionSet SIMDBackend, typename... Ts>
  struct base_helper {
    // For the scalar backend we have all cuts once, with a vector backend we
    // have them all twice -- once as vector cuts and once as scalar fallbacks
    // This makes an mp_list of integral_constant<InstructionSet, X> types with
    // either one or two members.
    using backend_ts =
        std::conditional_t<SIMDBackend == SIMDWrapper::Scalar,
                           boost::mp11::mp_list_c<SIMDWrapper::InstructionSet, SIMDBackend>,
                           boost::mp11::mp_list_c<SIMDWrapper::InstructionSet, SIMDBackend, SIMDWrapper::Scalar>>;

    // Bake T and N into aliases that we can pass to mp_product.
    template <typename M, typename Backend>
    using CombCut = typename Comb<Backend::value, Ts...>::template Cut<M::value>;
    template <typename Backend>
    using CompCut = CompositeCut<Backend::value>;
    // Make with_functors<FilterTransform<Ts...>,
    //                    [cartesian expansion of CombCut<A, B>...],
    //                    CompCut<B>...>
    // Where A is an integer from 0 to N-2 and B is the set of SIMD backends
    using type = boost::mp11::mp_append<
        with_functors<FilterTransform<Ts...>>,
        boost::mp11::mp_product<CombCut, boost::mp11::mp_from_sequence<std::make_index_sequence<sizeof...( Ts ) - 1>>,
                                backend_ts>,
        boost::mp11::mp_product<CompCut, backend_ts>>;
  };

  template <SIMDWrapper::InstructionSet SIMDBackend, typename... Ts>
  using base_t = typename base_helper<SIMDBackend, Ts...>::type;

  template <typename T0, typename T1>
  auto have_overlap( T0 const& p0, T1 const& p1 ) {
    if constexpr ( Sel::Utils::isBasicParticle_v<T0> && Sel::Utils::isBasicParticle_v<T1> ) {
      // track-like/track-like
      return p0.unique_id() == p1.unique_id();
    } else if constexpr ( Sel::Utils::isBasicParticle_v<T0> && !Sel::Utils::isBasicParticle_v<T1> ) {
      // track-like/composite
      using mask_t = decltype( p0.unique_id() == p0.unique_id() );
      mask_t mask{ false };
      for ( auto i = 0u; i < p1.numDescendants(); ++i ) {
        mask = mask | ( p1.descendant_unique_id( i ) == p0.unique_id() );
      }
      return mask;
    } else if constexpr ( !Sel::Utils::isBasicParticle_v<T0> && Sel::Utils::isBasicParticle_v<T1> ) {
      // composite/track-like
      return have_overlap( p1, p0 );
    } else {
      // composite/composite
      using mask_t = decltype( p0.descendant_unique_id( 0 ) == p1.descendant_unique_id( 0 ) );
      mask_t mask{ false };

      // with SIMD types there doesn't seem to be a way to short-circuit some cases
      for ( auto i = 0u; i < p0.numDescendants(); ++i )
        for ( auto j = 0u; j < p1.numDescendants(); ++j )
          // check that p0 and p1 do not have common descendants
          mask = mask | ( p0.descendant_unique_id( i ) == p1.descendant_unique_id( j ) );

      return mask;
    }
  }

  // didn't think about whether this works properly for negative/floating point values
  template <auto multiple, std::enable_if_t<std::is_integral_v<decltype( multiple )>, int> = 0>
  constexpr auto roundUp( decltype( multiple ) x ) {
    return ( ( x + multiple - 1 ) / multiple ) * multiple;
  }

  template <SIMDWrapper::InstructionSet Backend, std::size_t... Particle, typename BufferShuffles, typename BufferSizes,
            typename Indices, typename DescriptorIndices = std::false_type>
  auto extract_indices( std::index_sequence<Particle...>, BufferShuffles& buffer_shuffles, BufferSizes& buffer_sizes,
                        Indices& all_indices, [[maybe_unused]] DescriptorIndices descriptor_indices = {} ) {
    using simd_t = typename SIMDWrapper::type_map_t<Backend>;
    using mask_v = typename simd_t::mask_v;
    using int_v  = typename simd_t::int_v;
    // See whether or not we handle the last parameter
    constexpr bool process_descriptor_indices = !std::is_same_v<DescriptorIndices, std::false_type>;
    // What will we return?
    using ret_t = std::conditional_t<process_descriptor_indices,
                                     std::tuple<mask_v, std::array<int_v, sizeof...( Particle )>, int_v>,
                                     std::tuple<mask_v, std::array<int_v, sizeof...( Particle )>>>;
    // Work out the index into the various containers and collections of
    // combination cuts/counters. The offset of 2 comes because the first
    // invocation of this function is on 2-body combinations.
    constexpr auto Index = sizeof...( Particle ) - 2;
    // e.g. if Index = 0 it means std::get<0>( indices ) contains
    // buffer_sizes[0] 2-body combinations and this function should return
    // some int_v with a SIMD vector of them and shuffle the remaining values
    // down
    auto& size    = std::get<Index>( buffer_sizes );
    auto& indices = std::get<Index>( all_indices );
    // Make a return value and then modify it in-place
    ret_t ret;
    auto& mask = std::get<0>( ret );
    mask       = simd_t::loop_mask( 0, size );
    if ( !size ) { return ret; }
    // Load the values that we want to return
    auto& indices_chunk = std::get<1>( ret );
    // Indices from `indices`
    ( ( indices_chunk[Particle] = int_v{ indices[Particle].data() } ), ... );
    if constexpr ( process_descriptor_indices ) {
      // If we got a final argument, take a chunk of that too
      std::get<2>( ret ) = int_v{ descriptor_indices.get().data() };
    }
    // Replace invalid entries with the minimum valid index. We know `size`
    // is nonzero, so there must be at least one valid index.
    ( ( indices_chunk[Particle] = select( mask, indices_chunk[Particle], indices_chunk[Particle].hmin( mask ) ) ),
      ... );
    if constexpr ( process_descriptor_indices ) {
      std::get<2>( ret ) = select( mask, std::get<2>( ret ), std::get<2>( ret ).hmin( mask ) );
    }
    // We're finished with preparing the return value. Now remove the things we loaded from our inputs if need be
    if ( size > simd_t::size ) {
      // Scalar loop shuffling O(1) index we didn't consume to the front of
      // current_indices
      for ( auto i = simd_t::size; i < size; ++i ) {
        ( ( indices[Particle][i - simd_t::size] = indices[Particle][i] ), ... );
        if constexpr ( process_descriptor_indices ) {
          descriptor_indices.get()[i - simd_t::size] = descriptor_indices.get()[i];
        }
      }
      size -= simd_t::size;
      if constexpr ( Backend != SIMDWrapper::Scalar ) { std::get<Index>( buffer_shuffles ) += size; }
    } else {
      size = 0;
    }
    // All done
    return ret;
  }

  /** Helper that wraps std::visit and supports visitors with inhomogeneous
   *  return types by returning the [narrowest possible] variant of those
   *  types.
   */
  template <typename Visitor, typename... Variants>
  decltype( auto ) visit_returning_variant( Visitor&& vis, Variants&&... variants ) {
    // Evaluate all the possible return types for this visitor with these
    // variants, deduplicate and return as variant<U...>
    using ret_t =
        boost::mp11::mp_unique<boost::mp11::mp_product_q<boost::mp11::mp_bind_front<std::invoke_result_t, Visitor&&>,
                                                         std::decay_t<Variants>...>>;
    return visit(
        [&]( auto&&... x ) -> ret_t {
          return std::invoke( std::forward<Visitor>( vis ), std::forward<decltype( x )>( x )... );
        },
        std::forward<Variants>( variants )... );
  }

  /** Helper that maps variant<Ts...> -> variant<Us...> and Ts... -> U
   */
  template <typename Visitor, typename... Variants>
  decltype( auto ) invoke_or_visit_returning_variant( Visitor&& vis, Variants&&... variants ) {
    if constexpr ( LHCb::are_all_variants_v<std::decay_t<Variants>...> ) {
      // std::visit should both be found by ADL
      return visit_returning_variant( std::forward<Visitor>( vis ), std::forward<Variants>( variants )... );
    } else {
      return std::invoke( std::forward<Visitor>( vis ), std::forward<Variants>( variants )... );
    }
  }
} // namespace ThOr::detail::Combiner

namespace ThOr {
  using Sel::Utils::popcount;
  /** @class Combiner CombKernel/ThOrCombiner.h
   *  @brief Generic vectorised N-body combiner using ThOr functors.
   *
   *  @tparam SIMDBackend Vector backend to target. This will only actually be
   *                      used if all functors are available from a cache. If
   *                      this requirement is not met the scalar backend will
   *                      be used.
   *  @tparam InputTypes  Input particle container types consumed by the
   *                      combiner. These may be LHCb::variant<...> types if
   *                      more generic behaviour is needed. This implies a
   *                      small-ish runtime performance cost.
   *
   *  The ThOr combiner aims to be the workhorse for HLT2 and sprucing
   *  selections in Run 3. It should replace CombineParticles and the
   *  DaVinci::N{N}BodyDecays algorithms from Run 1 and Run 2. The ThOr
   *  combiner uses the ThOr functor framework to configure and apply selection
   *  requirements, and supports explicit vectorisation over particle
   *  combinations (as specified by the SIMDBackend template parameter).
   *
   *  Like the older algorithms, the combiner takes a "decay descriptor" string
   *  specifying the decay chain to be constructed. There are several different
   *  types/parsers of "decay descriptors" in the LHCb software, the one used
   *  here is the simple version used by the older particle combiners, not the
   *  more complicated version used in DecayTreeTuple, MCDecayTreeTuple and
   *  friends. The simple syntax allows a single particle to decay to several
   *  other particles:
   *    D0 -> KS0 K+ pi-
   *  with an optional charge conjugation at the top level:
   *    [D0 -> KS0 K+ pi-]cc
   *  meaning both:
   *    D0  -> KS0 K+ pi-
   *    D~0 -> KS0 K- pi+
   *
   *  This means that a single instance of the combiner algorithm will create
   *  either one (D0) or two (D0 and D~0) parent particle types. The current
   *  implementation imposes one additional requirement: if there are multiple
   *  child particles with the *same* ID, they must be grouped together. For
   *  example:
   *    D+ -> pi+ pi+ pi- (GOOD)
   *    D+ -> pi+ pi- pi+ (FORBIDDEN)
   *  This is related to the logic for generating combinations, which can avoid
   *  generating both (pi+(N), pi+(M)) and (pi+(M), pi+(N)) and having to
   *  remove one of them as a duplicate.
   *
   *  The algorithm logic assumes (for performance purposes, this should not
   *  affect the correctness of results) that the rarest children are listed
   *  first in the decay descriptor. For example, it should be more efficient
   *  to write
   *    D0 -> KS0 K+ pi-
   *  instead of
   *    D0 -> pi- K+ KS0
   *  assuming that on average the number of KS0 in each event is smaller than
   *  the number of K+, which is itself smaller than the number of pi-. This
   *  ordering is helpful for two reasons:
   *   - Firstly, if the selection cut applied to (KS0, K+) rejects all such
   *     combinations in the event then the (assumed large number of) pi- need
   *     not be considered at all in at event.
   *   - Secondly, when using non-scalar SIMD backends, some choices must be
   *     made about which "axis" is vectorised over. Concretely, in the above
   *     example and assuming a SIMD width of 8, do we try and combine 1 KS0
   *     with 8 different K+ at once, or combine 8 different KS0 with 1 K+?
   *     Insofar as KS0 are rare, the first option is better.
   *
   *  The algorithm is templated on the type and number of input containers, so
   *  a different template instantiation is needed for each SIMD backend and
   *  combination of child types. An instantiation with N children takes
   *  several different selection expressions:
   *    Combination12Cut  -> cut applied to 2-particle tuples (KS0 K+)
   *    Combination123Cut -> cut applied to 3-particle tuples (only for N >=4)
   *    ...
   *    CombinationCut    -> cut applied to N-particle tuples (KS0 K+ pi-)
   *    CompositeCut      -> cut applied to fitted N-particle composite
   *                         particles (D0)
   *  and N input locations: Input1 ... Input{N}.
   *
   *  If (see above) an input container is used for multiple children, as in
   *    D+ -> pi+ pi+ K-
   *  then the same container (of charged pions) must be given both as Input1
   *  and Input2, with Input3 pointing to a container of charged kaons.
   *
   *  The algorithm supports two "modes of operation" for the input container
   *  types. It can be instantiated explicitly:
   *    Combiner<SIMDBackend, ChargedBasics, ChargedBasics>
   *  for a 2-body combiner supporting decays such as:
   *    D0 -> K- pi+
   *  this "mode" is the simplest, and most computationally efficient, option,
   *  but comes at the price of having to explicitly instantiate and compile
   *  all desired permutations of child types.
   *  The second "mode" is to define std::variant-like types:
   *    using Particles = LHCb::variant<ChargedBasics, Composites>
   *  and instantiate the combiner using these:
   *    Combiner<SIMDBackend, Particles, Particles>
   *  to create an algorithm that can handle all permutations, in this case:
   *    Combiner<SIMDBackend, ChargedBasics, ChargedBasics>
   *    Combiner<SIMDBackend, ChargedBasics, Composites>
   *    Combiner<SIMDBackend, Composites, ChargedBasics>
   *    Combiner<SIMDBackend, Composites, Composites>
   *  at the cost of some runtime overhead, and a [trivial] separate algorithm
   *  to create a Particles object from a ChargedBasics or Composites object
   *  and put it on the TES.
   *
   *  @todo Address https://gitlab.cern.ch/lhcb/Phys/-/issues/12
   *  @todo Address https://gitlab.cern.ch/lhcb/Phys/-/issues/13
   *  @todo Address https://gitlab.cern.ch/lhcb/Phys/-/issues/14
   *  @todo Address https://gitlab.cern.ch/lhcb/Phys/-/issues/15
   *  @todo Address https://gitlab.cern.ch/lhcb/Phys/-/issues/16
   */
  template <SIMDWrapper::InstructionSet SIMDBackend, typename... InputTypes>
  struct Combiner final : public detail::Combiner::base_t<SIMDBackend, InputTypes...> {
    static constexpr auto N = sizeof...( InputTypes );
    using base_t            = typename detail::Combiner::base_t<SIMDBackend, InputTypes...>;
    using KeyValue          = typename base_t::KeyValue;
    using Output_t          = detail::Combiner::Output_t;
    using VertexFitter      = Sel::Fitters::ParticleVertex;
    // Bake T and N into some shorthand aliases
    template <std::size_t M, SIMDWrapper::InstructionSet Backend>
    using Combination = typename detail::Combiner::Comb<Backend, InputTypes...>::template Combination<M>;
    template <std::size_t M, SIMDWrapper::InstructionSet Backend>
    using CombCut = typename detail::Combiner::Comb<Backend, InputTypes...>::template Cut<M>;

    template <std::size_t... Is>
    static auto input_names( std::index_sequence<Is...> ) {
      return std::make_tuple( KeyValue{ "InputUniqueIDGenerator", LHCb::UniqueIDGeneratorLocation::Default },
                              KeyValue{ "Input" + std::to_string( Is + 1 ), "" }... );
    }

    Combiner( const std::string& name, ISvcLocator* pSvcLocator )
        : base_t{ name, pSvcLocator, input_names( std::index_sequence_for<InputTypes...>{} ),
                  KeyValue{ "Output", "" } } {}

    StatusCode initialize() override {
      return base_t::initialize().andThen( [&] {
        m_decays = Decays::decays( m_decaydescriptor, &*m_decoder );
        for ( auto const& decay : m_decays ) {
          auto const& children = decay.children();
          if ( children.size() != N ) {
            throw std::runtime_error( std::to_string( children.size() ) + " children in a " + std::to_string( N ) +
                                      "body combiner? what the hell!" );
          }

          std::set<int> pids{ children[0].pid().pid() };
          for ( uint i = 1; i < N; ++i ) {
            int pid = children[i].pid().pid();
            if ( pids.count( pid ) && children[i - 1].pid().pid() != pid ) {
              throw std::runtime_error( std::string{ "If you have multiple daughters of same PID, " } +
                                        "please put them consecutively:\nB0 -> pi+ pi- pi+ BAD\n" +
                                        "B0 -> pi+ pi+ pi- GOOD\n" );
            }
            pids.emplace( pid );
          }
        }
      } );
    }

    std::tuple<bool, Output_t> operator()( EventContext const& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen,
                                           InputTypes const&... raw_in ) const override {
      // Create a parameter pack for manipulating the N-1 combination cuts
      return dispatch( evtCtx, unique_id_gen, raw_in..., std::make_index_sequence<N - 1>{} );
    }

  private:
    // Runtime despatch to either the vectorised or scalar implementation
    template <std::size_t... ICombinationCuts>
    std::tuple<bool, Output_t> dispatch( EventContext const& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen,
                                         InputTypes const&... raw_in, std::index_sequence<ICombinationCuts...> ) const {
      // Try to get the vectorised versions of the combination and composite cuts
      auto const  vector_comb_cuts     = this->template getFunctors<CombCut<ICombinationCuts, SIMDBackend>...>();
      auto const& vector_composite_cut = this->template getFunctor<detail::Combiner::CompositeCut<SIMDBackend>>();
      // See if we got **all** the vectorised versions
      if ( ( std::get<ICombinationCuts>( vector_comb_cuts ) && ... ) && vector_composite_cut ) {
        // Yes, do all the hard work using the vectorised versions
        return do_the_work<SIMDBackend>( evtCtx, unique_id_gen, raw_in..., vector_comb_cuts, vector_composite_cut,
                                         std::index_sequence<ICombinationCuts...>{} );
      } else {
        // Presumably there was a cache miss, despatch to the implementation with JIT-able cuts
        if ( this->msgLevel( MSG::DEBUG ) ) { this->debug() << "Falling back on scalar implementation" << endmsg; }
        return do_the_work<SIMDWrapper::Scalar>(
            evtCtx, unique_id_gen, raw_in...,
            this->template getFunctors<CombCut<ICombinationCuts, SIMDWrapper::Scalar>...>(),
            this->template getFunctor<detail::Combiner::CompositeCut<SIMDWrapper::Scalar>>(),
            std::index_sequence<ICombinationCuts...>{} );
      }
    }

    // This is the meat of the implementation, inside here the SIMD backend is
    // known. An integer pack satisfying sizeof...(ICombinationCuts) == N - 1
    // is passed in for convenience
    template <SIMDWrapper::InstructionSet Backend, std::size_t... ICombinationCuts>
    std::tuple<bool, Output_t> do_the_work(
        EventContext const& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen, InputTypes const&... raw_in,
        std::tuple<Functors::Functor<typename CombCut<ICombinationCuts, Backend>::Signature> const&...>
            unprepared_combination_cuts,
        Functors::Functor<typename detail::Combiner::CompositeCut<Backend>::Signature> const& unprepared_composite_cut,
        std::index_sequence<ICombinationCuts...> ) const {
      using simd_t = typename SIMDWrapper::type_map_t<Backend>;
      // Get an iterable version of the input if need be -- this should be a noop if the input is already a zip
      // If some of the inputs are variants, map those onto new variants (of zips)
      std::tuple const in{ detail::Combiner::invoke_or_visit_returning_variant(
          []( auto const& raw ) { return LHCb::Event::make_zip<Backend>( raw ); }, raw_in )... };

      // Prepare the output storage
      auto const memResource = LHCb::getMemResource( evtCtx );
      // TODO make the scheduler provide a second memory resource that is reset
      //      after every algorithm and use that here
      auto const algLocalMemResource = memResource;
      std::tuple ret{ false, Output_t{ unique_id_gen, Zipping::generateZipIdentifier(), memResource } };
      // Can't use a structured binding here because we need to capture these
      // names in lambdas below (and clang doesn't let you do that...)
      auto& [filter_pass, storage] = ret;
      // Estimate the required output capacity based on what we've seen so far
      {
        // Number of events processed so far
        auto const num_events = std::max( 10ul, m_output_container_reallocated.nFalseEntries() );
        // Number of accepted composite particles so far
        auto const num_parts = std::max( 100ul, m_npassed_composite_cut.nTrueEntries() );
        // Average number of accepted composites in an event, and a fudge...
        constexpr auto fudge_factor = 4;
        storage.reserve( fudge_factor * num_parts / num_events );
        // TODO I think it would be better to just use some 99th (or whatever)
        //      percentile value of the final container size, rather than
        //      taking some multiple of the number of input tracks or maximum
        //      number of combinations. Also note that this is going to need
        //      some exclusions in the tests, as using a rolling estimate will
        //      introduce instabilities in e.g. counters that record how many
        //      times the container had to reallocate.
      }
      // Store the initial capacity so we can record whether or not we had to
      // reallocate during processing. Note that SOACollection does **not**
      // guarantee that this is the same as the value we reserved, it will in
      // fact typically be a bit more
      auto const initial_capacity = storage.capacity();

      // Temporary storage used as a buffer before the mother cut
      // TODO do we really need this? maybe we can just fill directly into the
      //      end of the real output container and then permute in-place
      Output_t tmp_storage{ unique_id_gen, storage.zipIdentifier(), algLocalMemResource };
      tmp_storage.reserve( simd_t::size );

      // To avoid this function being too monolithic, defer to another one to
      // loop over decay descriptors and generate/cut on/fit the combinations
      process_decays<Backend>(
          storage, tmp_storage, in, algLocalMemResource,
          // Prepare functors, in case that's expensive
          std::tuple{ std::get<ICombinationCuts>( unprepared_combination_cuts ).prepare( evtCtx )... },
          unprepared_composite_cut.prepare( evtCtx ),
          // Buffer counters to reduce atomic operations
          std::tuple{ m_buffer_shuffles[ICombinationCuts].buffer()... },
          std::tuple{ m_combination_cut_loads[ICombinationCuts].buffer()... },
          std::tuple{ m_combination_cut_ideal_loads[ICombinationCuts].buffer()... }, m_composite_cut_load.buffer(),
          std::tuple{ m_npassed_combination_cuts[ICombinationCuts].buffer()... }, m_npassed_vertex_fit.buffer(),
          m_npassed_composite_cut.buffer(),
          // Generate some integer packs for convenience
          std::index_sequence<ICombinationCuts...>{}, std::index_sequence_for<InputTypes...>{} );

      // Record if we had to reallocate the output container
      m_output_container_reallocated += bool{ storage.capacity() > initial_capacity };

      filter_pass = !storage.empty();
      return ret;
    }

    using index_vector_t = std::vector<int, LHCb::Allocators::EventLocal<int>>;

    // Collect relevant indices from the input containers, based on pid+charge as defined in the decay descriptor
    template <std::size_t IInput, typename IterableInputTuple>
    auto get_indices( IterableInputTuple const& inputs, Decays::Decay const& decay,
                      LHCb::Allocators::MemoryResource* memResource ) const {
      // In principle a narrower datatype could be used here, but SIMDWrapper doesn't support that...
      std::optional<index_vector_t> opt_indices{ std::nullopt };
      if constexpr ( IInput > 0 ) {
        // Not the first input, we need to figure out if this might be one of a
        // contiguous series of decay descriptor children of the same type. In
        // that case we have to generate a different kind of loop, and we have
        // to check that we were configured with a matching sequence of
        // identical inputs (mild EWWW :()
        if constexpr ( std::is_same_v<std::tuple_element_t<IInput - 1, IterableInputTuple>,
                                      std::tuple_element_t<IInput, IterableInputTuple>> ) {
          if ( decay.children()[IInput].pid() == decay.children()[IInput - 1].pid() && !m_allow_different_inputs ) {
            // Yes, in this case we should insist that the `IInput`th and
            // `IInput-1`th inputs were, in fact, the same...
            // There are some cases in which we don't want to enforce this (for example
            // when long-lived particles are involved). This can be swtiched-off via
            // the flag m_allow_different_inputs
            if ( !( std::get<IInput>( inputs ) == std::get<IInput - 1>( inputs ) ) ) {
              throw GaudiException{
                  "Got contiguous children with the same PIDs, but the corresponding algorithm inputs "
                  "were not the same. If this is intentional, please set AllowDiffInputsForSameIDChildren = True.",
                  "ThOr::{name}Combiner", StatusCode::FAILURE };
            }
            // In this case there is no need to come up with a new list of
            // indices, we will make a "triangular" (need a better name) loop
            // using the indices from IInput - 1.
            return opt_indices;
          }
        }
      }
      // We didn't fall into the special case, so we just loop through and
      // store indices. In this case we need an actual vector to return them in
      opt_indices.emplace( memResource );
      // The input containers might be variants...
      LHCb::invoke_or_visit(
          [&]( auto const& input ) {
            std::size_t indices_size{ 0 };
            // TODO can we use SOACollection here to hide this magic?
            opt_indices->resize( input.size() + std::decay_t<decltype( input )>::default_simd_t::size - 1 );
            for ( auto const& particles : input ) {
              auto const mask = particles.loop_mask() && ( particles.pid() == decay.children()[IInput].pid().pid() );
              // Maybe it's not worth explicitly vectorising this...
              particles.indices().compressstore( mask, std::next( opt_indices->data(), indices_size ) );
              indices_size += popcount( mask );
            }
            opt_indices->resize( indices_size );
          },
          std::get<IInput>( inputs ) );
      return opt_indices;
    }

    static auto exception( std::string message ) {
      return GaudiException{ std::move( message ), "ThOr::Combiner<T>", StatusCode::FAILURE };
    }

    /** This is a separate function just to keep the overhead/bookkeeping of
     *  interacting with Gaudi, the functor framework, etc. away from the meat
     *  of the combination generation.
     */
    template <SIMDWrapper::InstructionSet Backend, typename IterableInputTuple, typename CombinationCuts,
              typename CompositeCut, typename Counter, typename Counters, typename ShuffleCounters,
              std::size_t... ICombinationCuts, std::size_t... IInput>
    void process_decays( Output_t& storage, Output_t& tmp_storage, IterableInputTuple const& in,
                         LHCb::Allocators::MemoryResource* algLocalMemResource, CombinationCuts combination_cuts,
                         CompositeCut composite_cut, ShuffleCounters buffer_shuffles, Counters combination_cut_loads,
                         Counters combination_cut_ideal_loads, Counter composite_cut_load,
                         Counters npassed_combination_cuts, Counter npassed_vertex_fit, Counter npassed_composite_cut,
                         std::index_sequence<ICombinationCuts...>, std::index_sequence<IInput...> ) const {
      using simd_t = typename SIMDWrapper::type_map_t<Backend>;
      using int_v  = typename simd_t::int_v;
      using mask_v = typename simd_t::mask_v;
      // Buffer depth: 2 * simd_t::size - 1 would be sufficient (the assumption
      // is that any addition to the buffer is immediately followed by a
      // "consume if size >= simd_t::size" statement, so *before* an addition
      // there will be a maximum of simd_t::size - 1 entries already there and
      // a maximum of simd_t::size entries will be added. Take a wild guess
      // that actually subtracting - 1 would be a pessimisation... (need a
      // benchmark!)
      constexpr auto buffer_depth = 2 * simd_t::size;
      // When we consolidate the indices of N-particle combinations before the
      // vertex fit/cut, also store the index of the relevant decay descriptor.
      // This can then be used to get e.g. the PID the new composite should be
      // labelled with. This array stores those descriptor indices.
      std::array<int, buffer_depth> combinationN_decay_indices{};
      // Store the occupancy of the different buffers. e.g. for N = 3 the two
      // independent sizes are "number of 2-body combinations waiting to be
      // promoted to 3-body combinations" and "number of 3-body combinations
      // waiting to be fitted".
      std::array<std::size_t, N - 1> buffer_sizes{};
      // Count the number of {2-body .. N-body} combinations that were
      // considered; this is used to calculate the idealised SIMD utilisation
      std::array<std::size_t, N - 1> considered_combinations{};
      // Buffers that we track combinations in. +2 because the 0th combination
      // cut is applied to a 2-track combination and so on.
      std::tuple<std::array<std::array<int, buffer_depth>, ICombinationCuts + 2>...> combination_indices{};

      auto tmp_storage_view = LHCb::Event::make_zip<Backend>( tmp_storage );
      // Lambda that handles N-body combinations once we've got that far
      // `mask`: validity of the following arguments
      // `indices`: indices into the input containers of the members of this
      //            N-body combination
      // `descriptor_indices`: indices into `m_decays` of the decay descriptor
      //                       this N-body combination was produced from
      auto do_vertex_fit = [&]( mask_v const& mask, std::array<int_v, N> const& indices,
                                int_v const& descriptor_indices ) {
        // Tasks:
        // - run a vertex fit
        // - apply the composite cut to surviving entries
        // - store surviving entries in the output container

        // Get the PIDs that we should assign to the new composites
        int_v const new_composite_pids = [&]() {
          std::array<int, simd_t::size> tmp_data;
          descriptor_indices.store( tmp_data.data() );
          // Replace decay descriptor indices with PDG IDs of the new parents
          LHCb::Utils::unwind<0, simd_t::size>(
              [&]( auto i ) { tmp_data[i] = m_decays[tmp_data[i]].mother().pid().pid(); } );
          return int_v{ tmp_data.data() };
        }();
        if ( !tmp_storage.empty() ) { throw exception( "tmp_storage wasn't empty" ); }
        // Run a vertex fit and store the result in tmp_storage
        auto const fit_success_mask = m_vertex_fitter.fitComposite(
            tmp_storage, mask, new_composite_pids,
            Sel::ParticleCombination{ detail::Combiner::invoke_or_visit_returning_variant(
                [&]( auto const& container ) { return container.gather( indices[IInput], mask ); },
                std::get<IInput>( in ) )... } );
        if ( tmp_storage.size() != simd_t::size ) {
          throw exception( "tmp_storage didn't have simd_t::size elements." );
        }
        std::size_t const fit_success_count = popcount( fit_success_mask );
        // Record the fit success statistics.
        npassed_vertex_fit += { fit_success_count, std::size_t( popcount( mask ) ) };

        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Fit " << popcount( mask ) << ' ' << N << "-body combinations (" << mask
                        << "), indices = " << indices << ", new_pids = " << new_composite_pids << ", "
                        << fit_success_count << " succeeded (" << fit_success_mask << ")" << endmsg;
        }

        // Check that some fits succeeded. Because we consolidate before the
        // vertex fit, and the fit should rarely fail, the mask should normally
        // be quite well-populated. The exception might be the last call in each
        // event, where we are tidying up the leftovers.
        if ( fit_success_count ) {
          // Last fit_count elements of tmp_storage are valid particles that we can apply the composite cut to
          auto const& particle_chunk = *tmp_storage_view.begin();
          // Record how well we're SIMDifying the composite cut
          if constexpr ( Backend != SIMDWrapper::Scalar ) { composite_cut_load += { fit_success_count, simd_t::size }; }
          // Apply the composite cut to those particles
          auto const composite_cut_mask =
              fit_success_mask && composite_cut( Functors::mask_arg, fit_success_mask, particle_chunk );
          npassed_composite_cut += { std::size_t( popcount( composite_cut_mask ) ), fit_success_count };
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << popcount( composite_cut_mask ) << '/' << fit_success_count << " passed composite cut ("
                          << composite_cut_mask << ')' << endmsg;
          }
          // Copy the particles that matched into the actual storage
          storage.template copy_back<simd_t>( tmp_storage, 0, composite_cut_mask );
        }
        // Clear the temporary storage again
        tmp_storage.clear();
      };

      // For simplicity, we don't try and share too much work between different
      // decay descriptors. Hopefully we're not giving up on too much there.
      // With the simple descriptors we support here then there will only be 1
      // or 2 descriptors, with the second one being a top-level charge
      // conjugation of the first one.
      // The exception to this rule is that combination_indices are outside the
      // loop, and we allow the composite cut to execute with a mix of candidates
      // from the two descriptors, which should allow the SIMD utilisation to be
      // increased slightly.
      for ( int n_decay = 0; n_decay < int( m_decays.size() ); ++n_decay ) {
        auto const& decay = m_decays[n_decay];
        // Collect indices of particles in the input containers that satisfy
        // the constraints imposed by the decay descriptor. e.g. if the 0th
        // input container is a mix of D0 and D~0 then get the indices of the
        // D0s. If the Nth input is the same as the N-1th input then returns a
        // null std::optional indicating that a "triangle" loop should be used
        std::array const matching_indices_storage{ get_indices<IInput>( in, decay, algLocalMemResource )... };
        // See if we can shortcut -- if we got zero entries for any compulsory
        // input then we can abort processing here
        if ( std::any_of(
                 matching_indices_storage.begin(), matching_indices_storage.end(),
                 []( auto const& opt_indices ) { return opt_indices.has_value() && opt_indices->empty(); } ) ) {
          // OK, there is no way we can produce any candidates matching this
          // decay descriptor
          // TODO we could also short-circuit if we know we need to take >1 candidate from a container of size 1 etc.
          continue;
        }
        // Cache which list of indices we will actually use in each slot
        std::array const matching_indices{ [&]( std::size_t InputIndex ) {
          bool const do_triangle_loop = !matching_indices_storage[InputIndex].has_value();
          if ( do_triangle_loop ) {
            // Look in indices below InputIndex for a populated entry in matching_indices_storage
            assert( InputIndex > 0 );
            for ( std::size_t index_to_try = InputIndex - 1;; --index_to_try ) {
              if ( matching_indices_storage[index_to_try].has_value() ) {
                // Found the match
                return std::pair{ do_triangle_loop, std::cref( matching_indices_storage[index_to_try].value() ) };
              }
              assert( index_to_try != 0 );
            }
            assert( false ); // we should never get here
          }
          return std::pair{ do_triangle_loop, std::cref( matching_indices_storage[InputIndex].value() ) };
        }( IInput )... };

        // Defer to another function to actually generate the combinations
        process_combinations<Backend>( in, n_decay, matching_indices, do_vertex_fit, combination_cuts, buffer_shuffles,
                                       combination_cut_loads, combination_cut_ideal_loads, npassed_combination_cuts,
                                       combination_indices, combinationN_decay_indices, buffer_sizes,
                                       considered_combinations, std::index_sequence<ICombinationCuts...>{},
                                       std::make_index_sequence<sizeof...( ICombinationCuts ) - 1>{} );
      }
      // We are now done with the main processing loop, but some (< SIMD width)
      // entries may be left in the **last** element of combination_indices,
      // waiting to have the vertex fit run on them.
      call_vertex_fit<Backend>( do_vertex_fit, buffer_shuffles, combination_indices, combinationN_decay_indices,
                                buffer_sizes );

      // There shouldn't be anything left any of the index arrays now
      assert( ( ( buffer_sizes[ICombinationCuts] == 0 ) && ... ) );

      // Fill the ideal counter with what we could have achieved by
      // consolidating indices. 1st element = useful combinations, 2nd element
      // = useful combinations rounded up to a multiple of simd_t::size
      if constexpr ( Backend != SIMDWrapper::Scalar ) {
        ( ( std::get<ICombinationCuts>( combination_cut_ideal_loads ) +=
            { std::get<ICombinationCuts>( considered_combinations ),
              detail::Combiner::roundUp<simd_t::size>( std::get<ICombinationCuts>( considered_combinations ) ) } ),
          ... );
      }

      this->debug() << npassed_composite_cut.nTrueEntries() << '/' << npassed_composite_cut.nEntries()
                    << " composites passed cut" << endmsg;
    }

    template <SIMDWrapper::InstructionSet Backend, typename IterableInputTuple, typename ArrayOfIndexVectors,
              typename CombinationCuts, typename BufferShuffleCounters, typename CombinationCutLoadCounters,
              typename CombinationCutCounters, typename CombinationIndices, typename CombinationDescriptors,
              typename VertexFit, std::size_t... ICombinationCuts, std::size_t... ICombinationCutsMinusOne>
    void process_combinations(
        IterableInputTuple const& in, int n_decay, ArrayOfIndexVectors const& matching_indices,
        VertexFit& do_vertex_fit, CombinationCuts const& combination_cuts, BufferShuffleCounters& buffer_shuffles,
        CombinationCutLoadCounters& combination_cut_loads, CombinationCutLoadCounters& combination_cut_ideal_loads,
        CombinationCutCounters& npassed_combination_cuts, CombinationIndices& combination_indices,
        CombinationDescriptors& combinationN_decay_indices, std::array<std::size_t, N - 1>& buffer_sizes,
        std::array<std::size_t, N - 1>& considered_combinations, std::index_sequence<ICombinationCuts...>,
        std::index_sequence<ICombinationCutsMinusOne...> ) const {
      using simd_t = typename SIMDWrapper::type_map_t<Backend>;
      using int_v  = typename simd_t::int_v;

      // TODO consider separating the nested loop from the one that actually
      //      applies the combination12 cut? or use the same flip-flop logic
      //      here as is used later?
      auto const& comb12cut        = std::get<0>( combination_cuts );
      auto const& indices0         = matching_indices[0].second.get();
      auto const& indices1         = matching_indices[1].second.get();
      bool const  do_triangle_loop = matching_indices[1].first;
      assert( !matching_indices[0].first ); // do_triangle_loop
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "indices0.size() == " << indices0.size() << ", indices1.size() == " << indices1.size()
                      << ", do_triangle_loop == " << do_triangle_loop << endmsg;
      }
      for ( auto i0 = 0; i0 < int( indices0.size() ); ++i0 ) {
        auto const p0_inds    = indices0[i0];
        auto const particles0 = detail::Combiner::invoke_or_visit_returning_variant(
            [&]( auto const& container ) { return container.scalar_fill( p0_inds ); }, std::get<0>( in ) );
        for ( auto i1 = do_triangle_loop ? i0 + 1 : 0; i1 < int( indices1.size() ); i1 += simd_t::size ) {
          using comb12_t             = Combination<2, Backend>;
          auto const particles1_mask = simd_t::loop_mask( i1, indices1.size() );
          // Note that this is only *not* reading from beyond the end of
          // indices1 because of the convoluted .resize() in get_indices()
          auto const p1_inds    = int_v{ std::next( indices1.data(), i1 ) };
          auto const particles1 = detail::Combiner::invoke_or_visit_returning_variant(
              [&]( auto const& container ) { return container.gather( p1_inds, particles1_mask ); },
              std::get<1>( in ) );
          // The "triangle" inefficiency comes in from the `loop_mask`:
          //         d
          //       c d
          //     b c d
          //  -----------
          // | A B C D . |
          // | A B C .   |
          // | A B .     |
          // | A .       |
          // | .         |
          //  -----------
          //
          // (the ASCII art was totally worth it)
          //
          //   a b c d
          //   a b c d
          //  ---------         ---------
          // | A B C D |  vs.  | A A A A |
          // | A B C D |       | B B B B |
          //  ---------         ---------
          //
          // in the non-triangle case (e.g. 2 kaons, 4 pions, simd_t::size == 4)
          // We aim for the RHS based on the assumption that the rarest inputs
          // come first in the descriptor/inputs.
          // TODO we could add a counter that indicates whether this ordering is
          //      actually respected?
          // TODO in case we have variants this is probably not the smartest way to handle the overlap checks
          auto const overlap_mask = particles1_mask && LHCb::invoke_or_visit(
                                                           []( auto const& p0, auto const& p1 ) {
                                                             return !detail::Combiner::have_overlap( p0, p1 );
                                                           },
                                                           particles0, particles1 );
          // TODO consider consolidating indices here to increase SIMD
          //      utilisation, for now just short-circuit if we can
          std::size_t const prec12_count = popcount( overlap_mask );
          if ( !prec12_count ) { continue; }
          std::get<0>( considered_combinations ) += prec12_count;
          auto const mask =
              overlap_mask && comb12cut( Functors::mask_arg, overlap_mask, comb12_t{ particles0, particles1 } );
          std::size_t const c12cut_count = popcount( mask );
          // Record statistics
          if constexpr ( Backend != SIMDWrapper::Scalar ) {
            std::get<0>( combination_cut_loads ) += { prec12_count, simd_t::size };
          }
          std::get<0>( npassed_combination_cuts ) += { c12cut_count, prec12_count };
          // Short-circuit if we can
          if ( !c12cut_count ) { continue; }
          auto& combination12_indices      = std::get<0>( combination_indices );
          auto& combination12_indices_size = std::get<0>( buffer_sizes );
          p1_inds.compressstore( mask, &combination12_indices[1][combination12_indices_size] );
          int_v{ p0_inds }.store( &combination12_indices[0][combination12_indices_size] );
          // If we just made N-body combinations, store the index of their decay descriptor
          if constexpr ( N == 2 ) {
            // Populate `combinationN_decay_indices` with `n_decay`
            int_v{ n_decay }.store( &combinationN_decay_indices[combination12_indices_size] );
          }
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << "Stored " << c12cut_count << " 2-body combinations" << endmsg;
          }
          combination12_indices_size += c12cut_count;
          // If we don't have a full SIMD unit of 'comb12's, hold off
          if ( combination12_indices_size < simd_t::size ) { continue; }
          // Promote a load of 2-body combinations into 3+-body ones, or if
          // N = 2 then just do the vertex fit
          if constexpr ( N == 2 ) {
            call_vertex_fit<Backend>( do_vertex_fit, buffer_shuffles, combination_indices, combinationN_decay_indices,
                                      buffer_sizes );
          } else {
            // This has to take the vertex-fit-related stuff too, because it will
            // call call_vertex_fit internally eventually.
            add_more_tracks<Backend>( in, n_decay, matching_indices, do_vertex_fit, combination_cuts, buffer_shuffles,
                                      combination_cut_loads, combination_cut_ideal_loads, npassed_combination_cuts,
                                      combination_indices, combinationN_decay_indices, buffer_sizes,
                                      considered_combinations, std::make_index_sequence<2>{} );
          }
        }
      }
      // Flush the buffers of <N-body combinations
      ( add_more_tracks<Backend>( in, n_decay, matching_indices, do_vertex_fit, combination_cuts, buffer_shuffles,
                                  combination_cut_loads, combination_cut_ideal_loads, npassed_combination_cuts,
                                  combination_indices, combinationN_decay_indices, buffer_sizes,
                                  considered_combinations, std::make_index_sequence<ICombinationCutsMinusOne + 2>{} ),
        ... );

      // Check that all but the last buffer is empty (we didn't flush that one,
      // because we didn't try to call the vertex fit/cut)
      assert( ( ( buffer_sizes[ICombinationCutsMinusOne] == 0 ) && ... ) );
    }

    template <SIMDWrapper::InstructionSet Backend, typename VertexFit, typename BufferShuffleCounters,
              typename CombinationIndices, typename CombinationDecayIndices>
    void call_vertex_fit( VertexFit const& do_vertex_fit, BufferShuffleCounters& buffer_shuffles,
                          CombinationIndices& combination_indices, CombinationDecayIndices& combinationN_decay_indices,
                          std::array<std::size_t, N - 1>& buffer_sizes ) const {
      static_assert( N - 1 == std::tuple_size_v<CombinationIndices> );
      static_assert( N - 1 == std::tuple_size_v<BufferShuffleCounters> );
      // Extract a SIMD vector worth of indices from `combination_indices` and
      // shuffle the remaining values down to the start of the vector
      auto const [mask, particle_indices, descriptor_indices] =
          detail::Combiner::extract_indices<Backend>( std::make_index_sequence<N>{}, buffer_shuffles, buffer_sizes,
                                                      combination_indices, std::ref( combinationN_decay_indices ) );
      static_assert( std::tuple_size_v<decltype( particle_indices )> == N );
      // Short circuit if we can
      if ( none( mask ) ) { return; }
      // Do an actual vertex fit, finally...
      std::invoke( do_vertex_fit, mask, particle_indices, descriptor_indices );
    }

    /** Process a SIMD vector width worth of NBodies-body combinations defined
     *  by indices stored in 'combination_indices'. If NBodies < N this means
     *  creating (NBodies+1)-body combinations. If NBodies == N this means
     *  running the vertex fit and applying the composite cut.
     *
     *  Only the first two template parameters need to be template parameters,
     *  everything else **could** be written out explicitly...
     */
    template <SIMDWrapper::InstructionSet Backend, std::size_t... Particle, typename CombinationCuts,
              typename IterableInput, typename VertexFit, typename CombinationCutCounters,
              typename CombinationCutLoadCounters, typename CombinationIndices, typename CombinationDecayIndices,
              typename BufferShuffleCounters, typename ArrayOfIndexVectors>
    void
    add_more_tracks( IterableInput const& in, int n_decay, ArrayOfIndexVectors const& matching_indices,
                     VertexFit const& do_vertex_fit, CombinationCuts const& combination_cuts,
                     BufferShuffleCounters& buffer_shuffles, CombinationCutLoadCounters& combination_cut_loads,
                     CombinationCutLoadCounters& combination_cut_ideal_loads,
                     CombinationCutCounters& npassed_combination_cuts, CombinationIndices& combination_indices,
                     CombinationDecayIndices& combinationN_decay_indices, std::array<std::size_t, N - 1>& buffer_sizes,
                     std::array<std::size_t, N - 1>& considered_combinations, std::index_sequence<Particle...> ) const {
      // Sanity checks...
      constexpr auto NBodies = sizeof...( Particle );
      static_assert( NBodies >= 2 );
      static_assert( NBodies < N );
      static_assert( std::tuple_size_v<CombinationCuts> == N - 1 );
      static_assert( std::tuple_size_v<CombinationIndices> == N - 1 );
      static_assert( std::tuple_size_v<BufferShuffleCounters> == N - 1 );
      static_assert( std::tuple_size_v<CombinationCutCounters> == N - 1 );
      static_assert( std::tuple_size_v<CombinationCutLoadCounters> == N - 1 );
      // Useful aliases
      using simd_t = typename SIMDWrapper::type_map_t<Backend>;
      using int_v  = typename simd_t::int_v;
      using mask_v = typename simd_t::mask_v;
      // Extract the chunk of indices that we'll operate on
      auto const [mask, indices] = detail::Combiner::extract_indices<Backend>(
          std::index_sequence<Particle...>{}, buffer_shuffles, buffer_sizes, combination_indices );
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Promoting " << popcount( mask ) << " (" << mask << ") " << NBodies
                      << "-body combinations [indices = " << indices << ']' << endmsg;
      }
      // Short-circuit if possible
      if ( none( mask ) ) { return; }
      // Our task is to make (NBodies+1)-body combinations from NBodies-body
      // combinations.
      //
      // We only generate combination where the Nth index > (N-1)th index
      // etc. (single input container), so work out where in the container we
      // have to start looping.
      bool const            do_triangle_loop = std::get<0>( matching_indices[NBodies] );
      index_vector_t const& new_indices      = std::get<1>( matching_indices[NBodies] );
      int const             first_index      = [&, &mask = mask, &indices = indices]() -> int {
        if ( do_triangle_loop ) {
          // find the index in `new_indices` of the first value larger than `min_last_index`
          return std::distance( new_indices.begin(), std::upper_bound( new_indices.begin(), new_indices.end(),
                                                                                        indices.back().hmin( mask ) ) );
        } else {
          return 0;
        }
      }();
      // if ( this->msgLevel( MSG::DEBUG ) ) {
      //   this->debug() << "first_index = " << first_index << ", do_triangle_loop = " << do_triangle_loop
      //                 << ", new_indices.size() = " << new_indices.size() << endmsg;
      // }
      // Get the type of an (NBodies+1)-body combination
      using new_comb_t = Combination<NBodies + 1, Backend>;
      // Get the index of the combination cut that will act on this type
      constexpr std::size_t CombCutIndex = NBodies + 1 - 2;
      // Loop over the new additions to the combinations
      for ( auto i = first_index; i < int( new_indices.size() ); ++i ) {
        // Make sure the indices satisfy Nth > ... > 2nd > 1st
        // If we're making an X+1-body combination from an X-body one,
        // indices.back() are the indices of the last member of the X-body
        // one
        new_comb_t const comb{ detail::Combiner::invoke_or_visit_returning_variant(
                                   [inds = indices[Particle], mask = mask]( auto const& container ) {
                                     return container.gather( inds, mask );
                                   },
                                   std::get<Particle>( in ) )...,
                               detail::Combiner::invoke_or_visit_returning_variant(
                                   [&]( auto const& container ) { return container.scalar_fill( new_indices[i] ); },
                                   std::get<NBodies>( in ) ) };

        mask_v const loop_mask = do_triangle_loop ? mask && ( new_indices[i] > indices.back() ) : mask;
        // I don't think this is the smartest way of handling the overlap check in case we are using variants
        mask_v const overlap_mask =
            loop_mask &&
            ( LHCb::invoke_or_visit(
                  []( auto const& p0, auto const& p1 ) { return !detail::Combiner::have_overlap( p0, p1 ); },
                  comb.template get<Particle>(), comb.template get<NBodies>() ) &&
              ... );
        auto const&       comb_cut   = std::get<CombCutIndex>( combination_cuts );
        mask_v const      cut_mask   = overlap_mask && comb_cut( Functors::mask_arg, overlap_mask, comb );
        std::size_t const mask_count = popcount( overlap_mask );
        std::size_t const cut_count  = popcount( cut_mask );
        if constexpr ( Backend != SIMDWrapper::Scalar ) {
          std::get<CombCutIndex>( combination_cut_loads ) += { mask_count, simd_t::size };
          std::get<CombCutIndex>( considered_combinations ) += mask_count;
        }
        std::get<CombCutIndex>( npassed_combination_cuts ) += { cut_count, mask_count };
        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Made " << NBodies + 1 << "-body combination with indices";
          ( ( this->debug() << ' ' << indices[Particle] ), ... );
          this->debug() << ' ' << int_v{ new_indices[i] } << ". loop_mask = " << loop_mask
                        << ", overlap_mask = " << overlap_mask << ", cut_mask = " << cut_mask << endmsg;
        }
        if ( !cut_count ) { continue; }
        // Store indices of the new X+1-body combination
        auto& next_size    = std::get<CombCutIndex>( buffer_sizes );
        auto& next_indices = std::get<CombCutIndex>( combination_indices );
        // Store the indices of the surviving X-body combination members
        ( indices[Particle].compressstore( cut_mask, &next_indices[Particle][next_size] ), ... );
        // Store the index of the final element that we just added.
        int_v{ new_indices[i] }.store( &next_indices.back()[next_size] );
        // If we just made N-body combinations, store the index of their decay descriptor
        if constexpr ( NBodies + 1 == N ) {
          // Populate `combinationN_decay_indices` with `n_decay`
          int_v{ n_decay }.store( &combinationN_decay_indices[next_size] );
        }
        next_size += cut_count;
        // If our collection of (NBodies+1)-body combinations has at least a
        // whole SIMD vector worth of elements, process a chunk now.
        if ( next_size < simd_t::size ) { continue; }
        if constexpr ( NBodies + 1 == N ) {
          // If we just made the N-body combinations then despatch to the vertex fit
          call_vertex_fit<Backend>( do_vertex_fit, buffer_shuffles, combination_indices, combinationN_decay_indices,
                                    buffer_sizes );

        } else {
          // Otherwise, we need to add another track to these combinations
          add_more_tracks<Backend>( in, n_decay, matching_indices, do_vertex_fit, combination_cuts, buffer_shuffles,
                                    combination_cut_loads, combination_cut_ideal_loads, npassed_combination_cuts,
                                    combination_indices, combinationN_decay_indices, buffer_sizes,
                                    considered_combinations, std::make_index_sequence<NBodies + 1>{} );
        }
      }
    }

    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeCombinationCutCounters( Combiner* ptr, std::index_sequence<Ms...> ) {
      return { C{ ptr, "# passed " + detail::Combiner::combinationCutName<N, Ms>() }... };
    }
    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeCombinationCutLoadCounters( Combiner* ptr, std::index_sequence<Ms...> ) {
      return { C{ ptr, detail::Combiner::combinationCutName<N, Ms>() + " SIMD utilisation" }... };
    }
    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeCombinationCutIdealLoadCounters( Combiner* ptr,
                                                                               std::index_sequence<Ms...> ) {
      return { C{ ptr, detail::Combiner::combinationCutName<N, Ms>() + " ideal SIMD utilisation" }... };
    }
    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeBufferShuffleCounters( Combiner* ptr, std::index_sequence<Ms...> ) {
      return { C{ ptr, "# shuffled in " + std::to_string( Ms + 2 ) + "-particle buffer" }... };
    }
    /// Number of combinations passing the combination cut
    using counter_t         = Gaudi::Accumulators::BinomialCounter<>;
    using shuffle_counter_t = Gaudi::Accumulators::AveragingCounter<>;
    mutable std::array<counter_t, N - 1> m_npassed_combination_cuts{
        makeCombinationCutCounters<counter_t>( this, std::make_index_sequence<N - 1>{} ) };
    /// SIMD utilisation of the combination cuts
    mutable std::array<counter_t, N - 1> m_combination_cut_loads{
        makeCombinationCutLoadCounters<counter_t>( this, std::make_index_sequence<N - 1>{} ) };
    mutable std::array<counter_t, N - 1> m_combination_cut_ideal_loads{
        makeCombinationCutIdealLoadCounters<counter_t>( this, std::make_index_sequence<N - 1>{} ) };
    /// Number of indices shuffled
    mutable std::array<shuffle_counter_t, N - 1> m_buffer_shuffles{
        makeBufferShuffleCounters<shuffle_counter_t>( this, std::make_index_sequence<N - 1>{} ) };
    /// Number of successfully fitted combinations
    mutable counter_t m_npassed_vertex_fit{ this, "# passed vertex fit" };
    /// Number of fitted vertices passing composite cut
    mutable counter_t m_npassed_composite_cut{ this, "# passed CompositeCut" };
    /// How often we had to reallocate
    mutable counter_t m_output_container_reallocated{ this, "# output reallocations" };
    /// SIMD utilisation of the composite cuts
    mutable counter_t m_composite_cut_load{ this, "CompositeCut SIMD utilisation" };
    // decay descriptors
    ToolHandle<IDecodeSimpleDecayString> m_decoder{ this, "DecayDecoder", "DecodeSimpleDecayString" };
    // decays
    std::vector<Decays::Decay> m_decays;
    // descriptors
    Gaudi::Property<std::string> m_decaydescriptor{ this, "DecayDescriptor", "PleaseConfigureMe!",
                                                    "Please provide a decay descriptor!" };
    // By defalut, don't allow contiguous children with the same PID to have different algorithm inputs
    Gaudi::Property<bool> m_allow_different_inputs{ this, "AllowDiffInputsForSameIDChildren", false };

    // vertex fitter
    VertexFitter m_vertex_fitter{ this };
  };

  template <typename... Ts>
  using CombinerBest = Combiner<SIMDWrapper::Best, Ts...>;

  template <typename... Ts>
  using CombinerScalar = Combiner<SIMDWrapper::Scalar, Ts...>;
} // namespace ThOr
