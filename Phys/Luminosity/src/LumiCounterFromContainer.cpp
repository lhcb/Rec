/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <cstdint>

#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "Event/PrVeloTracks.h"
#include "Event/PrimaryVertices.h"
#include "Event/RecVertex.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track_v1.h"
#include "PrKernel/PrSelection.h"

/** @class LumiCounterFromContainer<T>
 *
 * @brief Create a HltLumiSummary object filled with the size of a single container.
 *
 * Takes a single container as input, and outputs an HltLumiSummary object,
 * which is essentially a map, with a single key-value pair. The key is taken
 * as the CounterName property, and the value is taken as the length of the
 * input container.
 *
 * The template parameter T is the input container type.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 *
 */
template <typename ContainerType>
class LumiCounterFromContainer : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const ContainerType& )> {
public:
  using base_class = LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const ContainerType& )>;
  LumiCounterFromContainer( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, { "InputContainer", "" }, { "OutputSummary", "" } ) {}

  StatusCode initialize() override {
    return base_class::initialize().andThen( [&] {
      if ( m_counterName.value().empty() ) {
        this->error() << "LumiCounter is missing a name" << endmsg;
        return StatusCode::FAILURE;
      }
      return StatusCode::SUCCESS;
    } );
  }

  LHCb::HltLumiSummary operator()( const ContainerType& container ) const override {
    auto nobjects = container.size();
    m_nobjects += nobjects;
    LHCb::HltLumiSummary summary{};
    summary.addInfo( m_counterName.value(), nobjects );

    return summary;
  }

private:
  Gaudi::Property<std::string>           m_counterName{ this, "CounterName", "", "Luminosity counter name" };
  Gaudi::Property<std::vector<unsigned>> m_counterMax{
      this, "CounterMax", { 0xffff }, "Required maximum value of the counter" };
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {},
      "Optional shift and scale factors used to process counter when encoding to a LumiSummary bank" };

  /// Records container sizes
  mutable Gaudi::Accumulators::StatCounter<std::uint64_t> m_nobjects{ this, "Nb objects" };
};

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Event::v1::Track::Container>,
                           "LumiCounterFromContainer__Track_v1" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Pr::Velo::Tracks>, "LumiCounterFromContainer__PrVeloTracks" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::RecVertex::Container>,
                           "LumiCounterFromContainer__RecVertex_v1" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Event::v2::RecVertices>,
                           "LumiCounterFromContainer__RecVertex_v2" )

DECLARE_COMPONENT_WITH_ID( LumiCounterFromContainer<LHCb::Event::PV::PrimaryVertexContainer>,
                           "LumiCounterFromContainer__PrimaryVertices" )
