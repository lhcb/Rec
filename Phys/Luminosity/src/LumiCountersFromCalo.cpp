/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Event/CaloDigits_v2.h"
#include "Event/HltLumiSummary.h"

/** @class LumiCountersFromCalo
 *
 * @brief Create an HltLumiSummary object filled with counters based on a Calo::Digits object.
 *
 * Takes a single Calo::Digits as input and outputs an HltLumiSummary object,
 * which maps counter names to values. Currently, a single counter is filled,
 * corresponding to the total energy summed over all cells.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromCalo
    : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Event::Calo::Digits& )> {
public:
  LumiCountersFromCalo( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, { "InputContainer", "" }, { "OutputSummary", "" } ) {}

  LHCb::HltLumiSummary operator()( LHCb::Event::Calo::Digits const& digits ) const override {
    LHCb::HltLumiSummary summary{};

    for ( auto const& name : m_counterNames ) {
      if ( name == "EtotZSup" ) {
        auto e = 0.;
        for ( auto d : digits ) { e += d.energy(); }
        summary.addInfo( m_counterBaseName + name, e );
      } else {
        throw GaudiException( "Unknown counter " + name, __PRETTY_FUNCTION__, StatusCode::FAILURE );
      }
    }

    return summary;
  }

private:
  // In principle, the name can be taken from the Digits. However, this does not work if all cells are zero-suppressed.
  Gaudi::Property<std::string> m_counterBaseName{ this, "CounterBaseName", "", "Prefix for luminosity counter names" };
  Gaudi::Property<std::vector<std::string>> m_counterNames{
      this, "CounterNames", { "EtotZSup" }, "Names of the counters" };
  Gaudi::Property<std::vector<unsigned>> m_counterMax{
      this, "CounterMax", { 0x3fffff }, "Required maximum values of the counters" };
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      { { "EtotZSup", { 0x10000, 0.0667 } } },
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank" };
};

DECLARE_COMPONENT( LumiCountersFromCalo )
