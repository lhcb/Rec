/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"

/** @class LumiCountersFromRich
 *
 * @brief Create an HltLumiSummary object filled with counters based on a DAQ::DecodedData object.
 *
 * Takes a single DAQ::DecodedData as input and outputs an HltLumiSummary object,
 * which maps counter names to values.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromRich
    : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const Rich::Future::DAQ::DecodedData& )> {
public:
  LumiCountersFromRich( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, { "InputContainer", "" }, { "OutputSummary", "" } ) {}

  LHCb::HltLumiSummary operator()( Rich::Future::DAQ::DecodedData const& data ) const override {

    // init temporary counter name
    std::string counterName = "";
    // create a map of counter names to hits vector
    std::map<std::string, int> counterMap;
    // assign as keys the counter names and init the values to 0
    for ( auto i = 0u; i < m_counterNames.size(); ++i ) { counterMap[m_counterBaseName + m_counterNames[i]] = 0; }

    // Loop over RICHes
    for ( const auto rich : Rich::detectors() ) {

      // data for this RICH
      const auto& rD = data[rich];

      // sides per RICH
      for ( const auto& pD : rD ) {
        // PD modules per side
        for ( const auto& mD : pD ) {

          // PDs per module
          for ( const auto& PD : mD ) {

            // PD ID
            const auto pdID = PD.pdID();
            if ( pdID.isValid() ) {
              // Vector of SmartIDs

              const auto irich   = pdID.rich();
              const auto ipanel  = pdID.panel();
              const auto icolumn = pdID.panelLocalModuleColumn();
              const auto ipdm    = pdID.columnLocalModuleNum();
              // compose the counter name using hit info
              counterName = std::to_string( irich + 1 ) + "S" + std::to_string( ipanel + 1 ) + "C" +
                            std::to_string( icolumn ) + "M" + std::to_string( ipdm ) + "Hits";
              const auto& rawIDs = PD.smartIDs();

              // Do we have any hits
              if ( !rawIDs.empty() ) {

                // check if the counter name is in counterNames vector
                if ( !( std::find( m_counterNames.begin(), m_counterNames.end(), counterName ) ==
                        m_counterNames.end() ) ) {
                  // increment the counter value by the number of hits
                  counterMap[m_counterBaseName + counterName] += rawIDs.size();
                } // end if counter name is in counterNames vector
              }   // end if we have hits
            }     // end if pdID is valid
          }       // end loop over PDs per module
        }         // end loop over PD modules per side
      }           // end loop over sides per RICH
    }             // end loop over RICHes

    LHCb::HltLumiSummary summary{};

    for ( auto i = 0u; i < m_counterNames.size(); ++i ) {
      summary.addInfo( m_counterBaseName + m_counterNames[i], counterMap[m_counterBaseName + m_counterNames[i]] );
    }

    return summary;
  }

private:
  Gaudi::Property<std::string> m_counterBaseName{ this, "CounterBaseName", "", "Prefix for luminosity counter names" };
  Gaudi::Property<std::vector<std::string>> m_counterNames{
      this,
      "CounterNames",
      { "1S1C6M2Hits", "1S1C6M3Hits", "1S1C7M2Hits", "1S1C7M3Hits", "1S1C8M2Hits", "1S1C8M3Hits", "1S1C9M2Hits",
        "1S1C9M3Hits", "1S2C6M2Hits", "1S2C6M3Hits", "1S2C7M2Hits", "1S2C7M3Hits", "1S2C8M2Hits", "1S2C8M3Hits",
        "1S2C9M2Hits", "1S2C9M3Hits", "2S1C5M1Hits", "2S1C5M2Hits", "2S1C5M3Hits", "2S1C5M4Hits", "2S1C6M1Hits",
        "2S1C6M2Hits", "2S1C6M3Hits", "2S1C6M4Hits", "2S1C7M1Hits", "2S1C7M2Hits", "2S1C7M3Hits", "2S1C7M4Hits",
        "2S1C8M1Hits", "2S1C8M2Hits", "2S1C8M3Hits", "2S1C8M4Hits", "2S2C5M1Hits", "2S2C5M2Hits", "2S2C5M3Hits",
        "2S2C5M4Hits", "2S2C6M1Hits", "2S2C6M2Hits", "2S2C6M3Hits", "2S2C6M4Hits", "2S2C7M1Hits", "2S2C7M2Hits",
        "2S2C7M3Hits", "2S2C7M4Hits", "2S2C8M1Hits", "2S2C8M2Hits", "2S2C8M3Hits", "2S2C8M4Hits" },
      "Names of the counters" };
  Gaudi::Property<std::vector<unsigned>> m_counterMax{
      this,
      "CounterMax",
      { 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023,
        1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023,
        1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023, 1023 },
      "Required maximum values of the counters" };
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {},
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank" };
};

DECLARE_COMPONENT( LumiCountersFromRich )
