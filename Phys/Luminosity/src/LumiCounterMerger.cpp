/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/MergingTransformer.h"

#include "Event/HltLumiSummary.h"

/** @class LumiCounterMerger
 *
 * @brief N-to-1 merger of HltLumiSummary objects.
 *
 * Merges N HltLumiSummary objects in to a single, new HltLumiSummary object.
 * An HltLumiSummary is effectively a map from a key to a integer counter; if
 * multiple inputs share a key, the output will merge these by summing them.
 *
 * @see LumiCounterFromContainer<T> for creating summary objects.
 *
 */
class LumiCounterMerger : public LHCb::Algorithm::MergingTransformer<LHCb::HltLumiSummary(
                              const Gaudi::Functional::vector_of_const_<LHCb::HltLumiSummary>& )> {
public:
  LumiCounterMerger( const std::string& name, ISvcLocator* pSvc )
      : MergingTransformer( name, pSvc, { "InputSummaries", {} }, { "OutputSummary", "" } ) {}

  LHCb::HltLumiSummary
  operator()( const Gaudi::Functional::vector_of_const_<LHCb::HltLumiSummary>& summaries ) const override {
    LHCb::HltLumiSummary::ExtraInfo summary{};

    // Merge all input summaries into one map, summing overlapping keys
    for ( const auto& s : summaries ) {
      for ( const auto& item : s.extraInfo() ) { summary.update( item.first, summary[item.first] + item.second ); }
    }

    LHCb::HltLumiSummary output{};
    output.setExtraInfo( summary );
    return output;
  }
};

DECLARE_COMPONENT( LumiCounterMerger )
