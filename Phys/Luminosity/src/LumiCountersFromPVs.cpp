/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "Event/PrimaryVertices.h"

/** @class LumiCountersFromPVs
 *
 * @brief Create an HltLumiSummary object filled with counters based on a DAQ::DecodedData object.
 *
 * Takes a single DAQ::DecodedData as input and outputs an HltLumiSummary object,
 * which maps counter names to values.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromPVs
    : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Event::PV::PrimaryVertexContainer& )> {
public:
  using base_class =
      LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Event::PV::PrimaryVertexContainer& )>;

  LumiCountersFromPVs( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, { "InputContainer", "" }, { "OutputSummary", "" } ) {}

  LHCb::HltLumiSummary operator()( LHCb::Event::PV::PrimaryVertexContainer const& vertices ) const override {

    // init temporary counter name
    std::string counterName = "";
    // create a map of counter names to hits vector
    std::map<std::string, double> counterMap;
    // assign as keys the counter names and init the values to 0
    for ( auto i = 0u; i < m_counterNames.size(); ++i ) { counterMap[m_counterBaseName + m_counterNames[i]] = 0; }

    counterMap[m_counterBaseName + "Vertices"] = vertices.size();

    auto vertexIndex{ 0u };

    for ( auto vertex : vertices ) {
      auto pos  = vertex.position();
      auto rho  = std::sqrt( pos.x() * pos.x() + pos.y() * pos.y() );
      auto absz = std::abs( pos.z() );
      if ( absz < 300.f * Gaudi::Units::mm && rho < 3.f * Gaudi::Units::mm ) {
        ++counterMap[m_counterBaseName + "FiducialVertices"];
      }
      // use the sum of vertex Z co-ordinates modulo the number of vertices to pseudo-randomly choose a vertex to record
      vertexIndex += static_cast<int>( absz );
    }

    if ( vertices.size() > 0u ) {
      auto vtx                                  = vertices[vertexIndex % vertices.size()].position();
      counterMap[m_counterBaseName + "VertexX"] = vtx.x();
      counterMap[m_counterBaseName + "VertexY"] = vtx.y();
      counterMap[m_counterBaseName + "VertexZ"] = vtx.z();
    }

    LHCb::HltLumiSummary summary{};

    for ( auto i = 0u; i < m_counterNames.size(); ++i ) {
      summary.addInfo( m_counterBaseName + m_counterNames[i], counterMap[m_counterBaseName + m_counterNames[i]] );
    }

    return summary;
  }

private:
  Gaudi::Property<std::string> m_counterBaseName{ this, "CounterBaseName", "", "Prefix for luminosity counter names" };
  Gaudi::Property<std::vector<std::string>>                         m_counterNames{ this,
                                                            "CounterNames",
                                                                                    {
                                                                "Vertices",
                                                                "FiducialVertices",
                                                                "VertexX",
                                                                "VertexY",
                                                                "VertexZ",
                                                            },
                                                            "Names of the counters" };
  Gaudi::Property<std::vector<unsigned>>                            m_counterMax{ this,
                                                       "CounterMax",
                                                                                  {
                                                           33,
                                                           33,
                                                           0x3fff,
                                                           0x3fff,
                                                           0x3fff,
                                                       },
                                                       "Required maximum values of the counters" };
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {
          { "VertexX", { 0x2000, 1638.4 } },
          { "VertexY", { 0x2000, 1638.4 } },
          { "VertexZ", { 0x2000, 16.384 } },
      },
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank" };
};

DECLARE_COMPONENT( LumiCountersFromPVs )
