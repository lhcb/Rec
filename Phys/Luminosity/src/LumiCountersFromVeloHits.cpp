/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Detector/VP/VPChannelID.h"
#include "Event/HltLumiSummary.h"
#include "Event/PrHits.h"

/** @class LumiCountersFromVeloHits
 *
 * @brief Create an HltLumiSummary object filled with counters based on a LHCb::Pr::VP::Hits.
 *
 * Takes a single LHCb::Pr::VP::Hits as input and outputs an HltLumiSummary object,
 * which maps counter names to values.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromVeloHits
    : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Pr::VP::Hits& )> {
public:
  using base_class = LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Pr::VP::Hits& )>;

  LumiCountersFromVeloHits( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, { "InputContainer", "" }, { "OutputSummary", "" } ) {}

  LHCb::HltLumiSummary operator()( LHCb::Pr::VP::Hits const& data ) const override {

    // init temporary counter name
    std::string counterName = "";
    // create a map of counter names to hits vector
    std::map<std::string, int> counterMap;
    // assign as keys the counter names and init the values to 0
    for ( auto i = 0u; i < m_counterNames.size(); ++i ) { counterMap[m_counterBaseName + m_counterNames[i]] = 0; }

    for ( const auto hit : data.scalar() ) {
      LHCb::Detector::VPChannelID cid{
          static_cast<unsigned>( hit.template get<LHCb::Pr::VP::VPHitsTag::ChannelId>().cast() ) };
      unsigned sensor_id = to_unsigned( cid.sensor() );
      // stations 0-25
      unsigned station_id = sensor_id / 8;
      // inner: 0, outer: 1
      unsigned region_id = sensor_id % 2;
      counterName        = "HitsS" + std::to_string( station_id );
      if ( region_id == 1 ) {
        counterName += "Outer";
      } else {
        counterName += "Inner";
      }

      ++counterMap[m_counterBaseName + counterName];

      auto bin{ 0 };
      for ( auto stationEdge : m_binEdges ) {
        if ( station_id <= stationEdge ) break;
        ++bin;
      }
      counterName = "HitsBin" + std::to_string( bin );
      if ( region_id == 1 ) {
        counterName += "Outer";
      } else {
        counterName += "Inner";
      }

      ++counterMap[m_counterBaseName + counterName];
    }

    LHCb::HltLumiSummary summary{};

    for ( auto i = 0u; i < m_counterNames.size(); ++i ) {
      summary.addInfo( m_counterBaseName + m_counterNames[i], counterMap[m_counterBaseName + m_counterNames[i]] );
    }

    return summary;
  }

private:
  Gaudi::Property<std::string> m_counterBaseName{ this, "CounterBaseName", "", "Prefix for luminosity counter names" };
  Gaudi::Property<std::vector<std::string>> m_counterNames{
      this,
      "CounterNames",
      {
          "HitsBin0Inner", "HitsBin0Outer", "HitsBin1Inner", "HitsBin1Outer", "HitsBin2Inner", "HitsBin2Outer",
          "HitsBin3Inner", "HitsBin3Outer", "HitsS0Inner",   "HitsS0Outer",   "HitsS1Inner",   "HitsS1Outer",
          "HitsS2Inner",   "HitsS2Outer",   "HitsS3Inner",   "HitsS3Outer",   "HitsS4Inner",   "HitsS4Outer",
          "HitsS5Inner",   "HitsS5Outer",   "HitsS6Inner",   "HitsS6Outer",   "HitsS7Inner",   "HitsS7Outer",
          "HitsS8Inner",   "HitsS8Outer",   "HitsS9Inner",   "HitsS9Outer",   "HitsS10Inner",  "HitsS10Outer",
          "HitsS11Inner",  "HitsS11Outer",  "HitsS12Inner",  "HitsS12Outer",  "HitsS13Inner",  "HitsS13Outer",
          "HitsS14Inner",  "HitsS14Outer",  "HitsS15Inner",  "HitsS15Outer",  "HitsS16Inner",  "HitsS16Outer",
          "HitsS17Inner",  "HitsS17Outer",  "HitsS18Inner",  "HitsS18Outer",  "HitsS19Inner",  "HitsS19Outer",
          "HitsS20Inner",  "HitsS20Outer",  "HitsS21Inner",  "HitsS21Outer",  "HitsS22Inner",  "HitsS22Outer",
          "HitsS23Inner",  "HitsS23Outer",  "HitsS24Inner",  "HitsS24Outer",  "HitsS25Inner",  "HitsS25Outer",
      },
      "Names of the counters" };
  Gaudi::Property<std::vector<unsigned>> m_counterMax{
      this,
      "CounterMax",
      {
          8000, 5000, 21000, 10000, 11000, 6000, 9000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
          3000, 3000, 3000,  3000,  3000,  3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
          3000, 3000, 3000,  3000,  3000,  3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
          3000, 3000, 3000,  3000,  3000,  3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
      },
      "Required maximum values of the counters" };
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {},
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank" };
  Gaudi::Property<std::vector<double>> m_binEdges{
      this, "binEdges", { 3, 13, 19 }, "Bin edges for hit counters in ranges of station number" };
};

DECLARE_COMPONENT( LumiCountersFromVeloHits )
