/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"
#include "Event/SOAUtils.h"
#include "Event/ZipUtils.h"
#include "Kernel/AllocatorUtils.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/Traits.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/TypeMapping.h"
#include "SelKernel/Utilities.h"
#include "TrackKernel/PrimaryVertexUtils.h"
#include <iostream>
#include <type_traits>

/** @file  VertexRelation.h
 *  @brief Types and functions for associating physics objects to vertices.
 */

/** @class BestVertexRelation
 *  @brief Stores a link to a vertex.
 *
 * This class is intended to be used to store a link to the 'best' associated
 * [primary] vertex. It is basically just an index, but can be extended to
 * allow more validation and caching of frequently-accessed quantities.
 *
 * @todo Add a zip-compatible-family ID. Base this on a specialisation of
 *       Zipping::ExportedSelection<> for the case that there is exactly one
 *       index.
 */
template <typename Index_t, typename Float_t>
class BestVertexRelation {
  Index_t m_index{ invalid_index };
  Float_t m_discriminant{ 0.f };

public:
  /** WARNING std::numeric_limits is very dangerous in generic code like this,
   *          as std::numeric_limits<T>::max() returns T() for unspecialised T
   *  NOTE    It would be nice if this was constexpr, but constructors such as
   *          SIMDWrapper::avx2::int_v::int_v(int) are not constexpr, and
   *          cannot easily be made constexpr without the SIMD intrinsics they
   *          use internally being made constexpr.
   */
  static_assert( std::numeric_limits<Index_t>::is_specialized );
  inline static auto const invalid_index = std::numeric_limits<Index_t>::max();

  BestVertexRelation( Index_t index, Float_t discriminant ) : m_index{ index }, m_discriminant{ discriminant } {}
  Index_t     index() const { return m_index; }
  Float_t     discriminant() const { return m_discriminant; }
  friend auto operator==( BestVertexRelation const& lhs, BestVertexRelation const& rhs ) {
    return lhs.index() == rhs.index() &&
           abs( lhs.discriminant() - rhs.discriminant() ) < std::numeric_limits<float>::epsilon();
  }
};

// SOACollection to hold vertex relations:
namespace Tag {
  struct Index : LHCb::Event::int_field {};
  struct Discriminant : LHCb::Event::float_field {};

  template <typename T>
  using best_vertex_relation_t = LHCb::Event::SOACollection<T, Index, Discriminant>;
} // namespace Tag

struct BestVertexRelations : Tag::best_vertex_relation_t<BestVertexRelations> {
  using base_t = typename Tag::best_vertex_relation_t<BestVertexRelations>;
  using base_t::base_t;

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  struct BestVertexRelationProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
    using base_t = typename LHCb::Event::Proxy<simd, behaviour, ContainerType>;
    using base_t::Proxy;
    using simd_t  = SIMDWrapper::type_map_t<simd>;
    using int_v   = typename simd_t::int_v;
    using float_v = typename simd_t::float_v;

    [[nodiscard]] auto index() const { return this->template get<Tag::Index>(); }
    [[nodiscard]] auto discriminant() const { return this->template get<Tag::Discriminant>(); }
    [[nodiscard]] auto bestPV() const { return BestVertexRelation{ index(), discriminant() }; }

    [[nodiscard]] auto set( int_v index, float_v discriminant ) const {
      this->template field<Tag::Index>().set( index );
      this->template field<Tag::Discriminant>().set( discriminant );
    }
  };
  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  using proxy_type = BestVertexRelationProxy<simd, behaviour, ContainerType>;

  // For functors
  using value_type = BestVertexRelationProxy<LHCb::Event::resolve_instruction_set_v<SIMDWrapper::InstructionSet::Best>,
                                             LHCb::Pr::ProxyBehaviour::Contiguous, BestVertexRelations>;
};

namespace Sel {

  template <class T>
  struct is_v1_particle : std::false_type {};

  template <>
  struct is_v1_particle<LHCb::Particle> : std::true_type {};

  template <typename S>
  inline constexpr bool is_v1_particle_v = is_v1_particle<S>::value;

  // Helper to determine whether a type is a LHCb::Particle
  template <typename S>
  using requireNotV1Particle = std::enable_if_t<!is_v1_particle_v<S>>;

  /** @fn    calculateBestVertex
   *  @brief Calculate the 'best' vertex for the given [vector of] object[s] in
   *         the given container.
   *
   * For now we hardcode that this is based on impact parameter. If you
   * pass in a vector type, a vector type will be returned.
   *
   * @todo Also propagate an identifier saying something about what class of
   *       containers the index is valid for.
   *
   * @param obj      Object (track, vertex, particle, ...) to be associated.
   * @param vertices Container of vertices. The caller is responsible for ensuring this
   *                 is not an empty container.
   *
   * @return Structure containing an index into the vertex container, plus some
   *         extra information for validation and optimisation.
   */
  template <typename Object, typename VContainer>
  auto calculateBestVertex( Object const& obj, VContainer const& vertices ) {
    // We want to know the index of the vertex (in 'vertices') that minimises
    // impactParameter. This seems awkward to write using STL algorithms
    // without evaluating impactParameter() an excessive number of times.
    if ( vertices.empty() ) {
      // To avoid having to handle the case that there are no vertices in "many"
      // different places, instead define that this is the caller's responsibility.
      throw GaudiException{ "Empty vertex container passed to Sel::calculateBestVertex()", "Sel::calculateBestVertex",
                            StatusCode::FAILURE };
    }

    // First, deduce the float-like and int-like types that we will use
    using Sel::Utils::impactParameterSquared;
    using float_t =
        decltype( impactParameterSquared( endVertexPos( Sel::Utils::deref_if_ptr( vertices.front() ) ), obj ) );
    using int_t = typename LHCb::type_map<float_t>::int_t;

    static_assert( std::numeric_limits<int_t>::is_specialized );
    static_assert( std::numeric_limits<float_t>::is_specialized );
    auto        min_index = std::numeric_limits<int_t>::max();
    auto        min_ip2   = std::numeric_limits<float_t>::max();
    std::size_t vertex_index{ 0 };
    for ( const auto& vertex : vertices ) {
      auto ip2  = impactParameterSquared( endVertexPos( Sel::Utils::deref_if_ptr( vertex ) ), obj );
      auto mask = ip2 < min_ip2; // caution: nans can mess with that logic
      min_ip2   = select( mask, ip2, min_ip2 );
      min_index = select( mask, int_t( vertex_index ), min_index );
      ++vertex_index;
    }

    // FIXME what do we do in case all ip calculations failed?
    // min_index is then still on numeric_limits<int>::max()
    auto min_index_san = select( min_index == std::numeric_limits<int_t>::max(), 0, min_index );

    // Return the index of the 'best' vertex, and the ip with respect to it
    return BestVertexRelation{ min_index_san, sqrt( min_ip2 ) };
  }

  template <typename T>
  LHCb::VertexBase const* to_pointer( T&& t ) {
    if constexpr ( std::is_pointer_v<std::remove_reference_t<T>> ) {
      return t;
    } else if constexpr ( std::is_lvalue_reference_v<T> ) {
      return &t;
    } else {
      static_assert( !std::is_same_v<T, T>, "to_pointer should not be called with type T" );
      return nullptr;
    }
  }

  /** @fn    calculateBestVertices
   *  @brief Produce a container of vertex relations for the given objects to
   *         the given vertices
   *
   *  @param objects  Container of objects (tracks, particles, ...) for which
   *                  relations are to be calculated.
   *  @param vertices Container of vertices to which the relations will refer.
   *
   *  @return BestVertexRelations object containing the new relations.
   */
  template <typename OContainer, typename VContainer>
  BestVertexRelations calculateBestVertices( OContainer const& objects, VContainer const& vertices ) {
    BestVertexRelations relations{ objects.zipIdentifier(), objects.get_allocator() };
    relations.reserve( objects.size() );

    for ( auto const& object : objects.simd() ) {
      auto const& bestPV   = calculateBestVertex( object, vertices );
      auto const& relation = relations.emplace_back( objects.size() );
      relation.set( bestPV.index(), bestPV.discriminant() );
    }

    return relations;
  }

  /** @fn    getBestPVRel
   *  @brief Get relation to the 'best' one of the given primary vertices.
   *
   * If the given object has a .bestPV() method, check that the result is
   * compatible with the given vertex container and return the result.
   * Otherwise calculate the best vertex in the given container using
   * calculateBestVertex() and return that result. The caller has to apply
   * the result to the vertex container themself, but we should guarantee
   * compatibility.
   * @todo This should perform a compatibility test between the saved relation
   *       and the given container. If this fails, a new relation should be
   *       calculated. The family identifier used for this check could also be
   *       used to identify an empty relation.
   */
  template <typename Object, typename VContainer>
  auto getBestPVRel( Object const& obj, VContainer const& vertices ) {
    if constexpr ( requires { obj.bestPV(); } ) {
      auto rel = obj.bestPV();
      // TODO: replace this with a check that rel is compatible with vertices
      if ( ( true ) ) { return rel; }
    }
    return calculateBestVertex( obj, vertices );
  }

  /** @fn    getBestPV
   *  @brief Get a reference to the 'best' one of the given vertices for LHCb::Particle.
   *
   *  Defers to getBestPVRel for all the hard work, but then returns a
   *  reference to the given vertex instead of the relation itself.
   */
  template <typename VContainer>
  decltype( auto ) getBestPV( LHCb::Particle const& obj, VContainer const& vertices ) {

    auto rel = getBestPVRel( obj, vertices );
    // rel could be a relation or a reference_wrapper around one
    // either way then std::cref( rel ).get() is a const reference
    // to a relation
    auto const& cref_rel = std::cref( rel ).get();
    auto        index    = cref_rel.index();
    using Sel::Utils::any;
    if ( any( index == std::decay_t<decltype( cref_rel )>::invalid_index ) ) {
      // This shouldn't happen, but better to provide a useful error message.
      throw GaudiException{ "Invalid vertex index", "Sel::getBestPV", StatusCode::FAILURE };
    }
    // index could be int or int_v, if it's int_v, size should be one for v1 Particle
    if constexpr ( requires { index.size(); } ) { // FIXME has_size -> is_int_v
      assert( index.size() == 1 );
      return to_pointer( vertices[index.cast()] );
    } else {
      return to_pointer( vertices[index] ); // vertex const&
    }
  }

  /** @fn    getBestPV
   *  @brief Get a reference to the 'best' one of the given vertices.
   *
   *  Defers to getBestPVRel for all the hard work, but then returns a
   *  reference to the given vertex instead of the relation itself.
   */
  template <typename Object, typename VContainer, typename = requireNotV1Particle<Object>>
  decltype( auto ) getBestPV( Object const& obj, VContainer const& vertices ) {

    auto rel = getBestPVRel( obj, vertices );
    // rel could be a relation or a reference_wrapper around one
    // either way then std::cref( rel ).get() is a const reference
    // to a relation
    auto const& cref_rel = std::cref( rel ).get();
    auto        index    = cref_rel.index();
    using Sel::Utils::any;
    if ( any( index == std::decay_t<decltype( cref_rel )>::invalid_index ) ) {
      // This shouldn't happen, but better to provide a useful error message.
      throw GaudiException{ "Invalid vertex index", "Sel::getBestPV", StatusCode::FAILURE };
    }
    // index could be int or int_v, if it's int_v, we need
    // to return multiple vertices
    if constexpr ( requires { index.size(); } ) { // FIXME has_size -> is_int_v
      if constexpr ( index.size() > 1 ) {
        std::array<int, index.size()> idxs;
        index.store( idxs );
        std::array<typename VContainer::value_type const*, index.size()> bestVertices{};
        int n_valid = popcount( Functors::detail::loop_mask( obj ) );
        assert( static_cast<size_t>( n_valid ) <= index.size() );
        for ( int i = 0; i != n_valid; ++i ) { bestVertices[i] = &vertices[idxs[i]]; }
        for ( int i = n_valid; i != index.size(); ++i ) { bestVertices[i] = nullptr; }
        return bestVertices; // array of vertex pointers

      } else {

        return to_pointer( vertices[index.cast()] );
      }
    } else {

      return to_pointer( vertices[index] ); // vertex const&
    }
  }

  /// Template specialization such that the code above also works for KeyedContainer<RecVertex>
  template <typename Object, typename T, typename = requireNotV1Particle<Object>>
  decltype( auto ) getBestPV( Object const& obj, KeyedContainer<T> const& vertices ) {
    return getBestPV( obj, LHCb::ObjectContainerVectorView<KeyedContainer<T>>{ vertices } );
  }

  /// Template specialization such that the code above also works for KeyedContainer<RecVertex>
  template <typename T>
  auto getBestPV( LHCb::Particle const& obj, KeyedContainer<T> const& vertices ) -> decltype( obj.pv().target() ) {
    return getBestPV( obj, LHCb::ObjectContainerVectorView<KeyedContainer<T>>{ vertices } );
  }
} // namespace Sel
