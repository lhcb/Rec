#ifndef LIGHTWEIGHT_NETWORK_CONFIG_HH
#define LIGHTWEIGHT_NETWORK_CONFIG_HH

// MIT License

// Copyright (c) 2017 Daniel Hay Guest and lwtnn contributors

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

// NNLayerConfig is the "low level" configuration, which should
// contain everything needed to create a "Stack" or "Graph".
//
#include "NNLayerConfig.h"

// The code below is to configure the "high level" interface.  These
// structures are used to initalize the "LightweightNeuralNetwork" and
// "LightweightGraph".

#include <map>
#include <string>
#include <vector>

namespace lwt {

  typedef std::map<std::string, std::vector<double>> VectorMap;

  struct Input {
    std::string name;
    double      offset;
    double      scale;
  };

  // feed forward structure
  //
  // Note that this isn't technically JSON-dependant, the name is mostly
  // historical
  struct JSONConfig {
    std::vector<LayerConfig>           layers;
    std::vector<Input>                 inputs;
    std::vector<std::string>           outputs;
    std::map<std::string, double>      defaults;
    std::map<std::string, std::string> miscellaneous;
  };

  // graph structures
  struct InputNodeConfig {
    std::string                        name;
    std::vector<Input>                 variables;
    std::map<std::string, std::string> miscellaneous;
    std::map<std::string, double>      defaults;
  };

  struct OutputNodeConfig {
    std::vector<std::string> labels;
    size_t                   node_index;
  };

  struct GraphConfig {
    std::vector<InputNodeConfig>            inputs;
    std::vector<InputNodeConfig>            input_sequences;
    std::vector<NodeConfig>                 nodes;
    std::map<std::string, OutputNodeConfig> outputs;
    std::vector<LayerConfig>                layers;
  };
} // namespace lwt

#endif // LIGHTWEIGHT_NETWORK_CONFIG_HH
