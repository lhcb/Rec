/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IBackgroundCategory.h"
#include "Relations/Relations.h"

#include "boost/algorithm/string.hpp"

/** @class BackgroundCategoryViaRelations BackgroundCategoryViaRelations.h
 *
 *  A tool to read a relations table of Particle->BackgroundCategory and
 *  return the results. Only implements the category method for now.
 *
 *  InputTable : give the TES location of the relations table. This should be
 *               a relations table Particle*->int, created by the algorithm
 *               Particle2BackgroundCategoryRelationsAlg.
 *
 *  @author Vladimir Gligorov
 *  @date   2009-11-27
 */
class BackgroundCategoryViaRelations : public GaudiTool, virtual public IBackgroundCategory {

private:
  typedef std::pair<const LHCb::Particle*, const LHCb::MCParticle*> DaughterAndPartnerPair;
  typedef std::vector<DaughterAndPartnerPair>                       DaughterAndPartnerVector;

public:
  BackgroundCategoryViaRelations( const std::string& type, const std::string& name, const IInterface* parent );

public:
  IBackgroundCategory::Result category( const LHCb::Particle* reconstructed_mother,
                                        const LHCb::Particle* headP = nullptr ) override;

public:
  const LHCb::MCParticle* origin( const LHCb::Particle* ) override {
    Error( "The origin method is not applicable to this BackgroundCategory implementation" ).ignore();
    return 0;
  }

  const DaughterAndPartnerVector getDaughtersAndPartners( const LHCb::Particle* ) override {
    Error( "The getDaughtersAndPartners method is not applicable to this BackgroundCategory implementation" ).ignore();
    return DaughterAndPartnerVector( 0 );
  }

private:
  /// Get the TES location of an object.
  const std::string b2cLocation( const DataObject* pObject ) const {
    std::string loc =
        ( !pObject ? "Null DataObject" : ( pObject->registry() ? pObject->registry()->identifier() : "UnRegistered" ) );
    // Form the relations TES location for this Particles location
    boost::replace_all( loc, "/Particles", "/P2BCRelations" );
    // return
    return loc;
  }

private:
  typedef LHCb::Relation1D<LHCb::Particle, int> TableP2BC;

  Gaudi::Property<std::vector<std::string>> m_P2BCLocation{ this, "InputTable", {} };
};

BackgroundCategoryViaRelations::BackgroundCategoryViaRelations( const std::string& type, const std::string& name,
                                                                const IInterface* parent )
    : GaudiTool( type, name, parent ) {
  IBackgroundCategory::m_cat[-1]   = "Undefined";
  IBackgroundCategory::m_cat[0]    = "Signal";
  IBackgroundCategory::m_cat[10]   = "QuasiSignal";
  IBackgroundCategory::m_cat[20]   = "FullyRecoPhysBkg";
  IBackgroundCategory::m_cat[30]   = "Reflection";
  IBackgroundCategory::m_cat[40]   = "PartRecoPhysBkg";
  IBackgroundCategory::m_cat[50]   = "LowMassBkg";
  IBackgroundCategory::m_cat[60]   = "Ghost";
  IBackgroundCategory::m_cat[63]   = "Clone";
  IBackgroundCategory::m_cat[66]   = "Hierarchy";
  IBackgroundCategory::m_cat[70]   = "FromPV";
  IBackgroundCategory::m_cat[80]   = "AllFromSamePV";
  IBackgroundCategory::m_cat[100]  = "FromDifferentPV";
  IBackgroundCategory::m_cat[110]  = "bbar";
  IBackgroundCategory::m_cat[120]  = "ccbar";
  IBackgroundCategory::m_cat[130]  = "uds";
  IBackgroundCategory::m_cat[1000] = "LastGlobal";

  declareInterface<IBackgroundCategory>( this );
}

IBackgroundCategory::Result BackgroundCategoryViaRelations::category( const LHCb::Particle* reconstructed_mother,
                                                                      const LHCb::Particle* headP ) {
  // locations in JOs
  std::vector<std::string> locs = m_P2BCLocation;

  // add particle dependent locations
  if ( reconstructed_mother ) { locs.push_back( b2cLocation( reconstructed_mother->parent() ) ); }
  if ( headP && headP != reconstructed_mother ) { locs.push_back( b2cLocation( headP->parent() ) ); }

  // std::sort( locs.begin(), locs.end() );
  // locs.erase( std::unique(locs.begin(),locs.end()), locs.end() );

  // Loop over the relations locations and try and find a match
  int check = 0; // check for existence of relations table
  for ( std::vector<std::string>::const_iterator item = locs.begin(); item != locs.end(); ++item ) {
    // Try and load the table
    const std::string& address = *item;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Trying to find table '" << address << "'" << endmsg;
    const TableP2BC* table = getIfExists<TableP2BC>( address, false );
    if ( !table ) { table = getIfExists<TableP2BC>( address ); }
    if ( table ) {
      check += 1;
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Found table ... " << endmsg;

      // Try and use it ...
      const TableP2BC::Range range = table->relations( reconstructed_mother );

      // For the moment we only allow one category per particle so if more than one category is
      // related return undefined, else just return the category we find
      if ( range.empty() || range.size() > 1 ) {
        continue; // to next table
      } else {
        // Found so return
        const IBackgroundCategory::categories cat = static_cast<IBackgroundCategory::categories>( range.begin()->to() );
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "  -> Found match ... " << cat << endmsg;
        return { cat, Good };
      }
    }
  }

  // If we have not found anything else to return yet we return undefined,
  if ( check > 0 ) return { Undefined, Good };
  return { Undefined, Bad };
}

// Declaration of the Tool Factory
DECLARE_COMPONENT( BackgroundCategoryViaRelations )
