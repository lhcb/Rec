/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/DaVinciStringUtils.h"
#include "Kernel/IBackgroundCategory.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Relations/Relations.h"

#include "boost/algorithm/string.hpp"

/** @class Particle2BackgroundCategoryRelationsAlg Particle2BackgroundCategoryRelationsAlg.h
 *
 *  Algorithm which takes as input a TES container of Particles, gets the BackgroundCategory
 *  for each one, and writes a Relations table between these Particles and their
 *  BackgroundCategories to the TES. The table is Particle*->int.
 *
 *  InputLocations : The TES locations of the particles for which the table is to be written.
 *                   The table is written to InputLocations-"/Particles"+"/P2BCRelations"
 *
 *  @author V. Gligorov
 *  @date   2009-11-30
 */
class Particle2BackgroundCategoryRelationsAlg : public GaudiAlgorithm {

public:
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode initialize() override; ///< Algorithm initialization
  StatusCode execute() override;    ///< Algorithm execution

private:
  /// relations type
  typedef LHCb::Relation1D<LHCb::Particle, int> CatRelations;

private:
  /** Returns the full location of the given object in the Data Store
   *
   *  @param pObj Data object
   *
   *  @return Location of given data object
   */
  inline std::string objectLocation( const DataObject* pObj ) const {
    return ( !pObj ? "" : ( pObj->registry() ? pObj->registry()->identifier() : "" ) );
  }

  /// Save the background category information for all particles at the given location
  StatusCode backCategoriseParticles( const std::string& location ) const;

  /// Save the background category information for the given particle
  StatusCode backCategoriseParticle( const LHCb::Particle* particle, const unsigned int recurCount = 0 ) const;

  /// Get the relations object for a given Particle
  CatRelations* catRelations( const LHCb::Particle* particle ) const;

private:
  Gaudi::Property<std::vector<std::string>> m_particleLocations{ this, "Inputs", {}, "Particle locations to process" };
  Gaudi::Property<bool> m_fullTree{ this, "FullTree", false, "Flag to turn on processing the full tree" };

  IBackgroundCategory* m_bkg{ nullptr };

  typedef std::map<std::string, CatRelations*> CatMap;
  /// Mapping between particle locations and relations object
  mutable CatMap m_catMap;
};

StatusCode Particle2BackgroundCategoryRelationsAlg::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] {
    // FIXME Use ToolHandle here in principle, except it won't compile as this call is not thread safe !
    m_bkg = tool<IBackgroundCategory>( "BackgroundCategory", this );
    return StatusCode::SUCCESS;
  } );
}

StatusCode Particle2BackgroundCategoryRelationsAlg::execute() {
  // Check that we have an input location
  if ( m_particleLocations.empty() ) { return Error( "No particle location(s) provided" ); }

  // Clear the map of relations
  m_catMap.clear();

  for ( std::vector<std::string>::const_iterator iLoc = m_particleLocations.begin(); iLoc != m_particleLocations.end();
        ++iLoc ) {
    const StatusCode sc = backCategoriseParticles( *iLoc );
    if ( sc.isFailure() ) { return Error( "Problem BackgroundCategorizing '" + *iLoc + "'", sc ); }
  }

  return StatusCode::SUCCESS;
}

StatusCode Particle2BackgroundCategoryRelationsAlg::backCategoriseParticles( const std::string& location ) const {
  // Check that we have an input location
  if ( location.empty() ) { return Error( "No Particle TES location provided" ); }

  // Get the input particles
  const LHCb::Particle::Range myParticles = getIfExists<LHCb::Particle::Range>( location );
  // Check that this returns something
  if ( myParticles.empty() ) { return StatusCode::SUCCESS; }

  // Loop over the Particles and save the relations
  StatusCode sc = StatusCode::SUCCESS;
  for ( LHCb::Particle::Range::const_iterator iP = myParticles.begin(); iP != myParticles.end(); ++iP ) {
    if ( sc ) sc = backCategoriseParticle( *iP );
  }

  // return
  return sc;
}

StatusCode Particle2BackgroundCategoryRelationsAlg::backCategoriseParticle( const LHCb::Particle* particle,
                                                                            const unsigned int    recurCount ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // protect against too many recursive calls...
  // (should never ever get this deep, but to just to be safe ...)
  if ( recurCount > 99999 ) {
    return Warning( "Recursive limit in backCategoriseParticle reached. Aborting Particle tree scan", sc );
  }

  // protect against null pointers
  if ( particle ) {

    // relate this particle
    CatRelations* catRel = catRelations( particle );
    if ( catRel ) {
      // Get the background category
      const int thisCat = static_cast<int>( m_bkg->category( particle ).category );

      // Save it
      catRel->i_relate( particle, thisCat ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      // printout
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << "Particle key=" << particle->key() << " PID=" << particle->particleID() << " "
                  << objectLocation( particle->parent() ) << " BackCat=" << thisCat << endmsg;
      }
    }

    // if requested, scan the daughters as well
    if ( m_fullTree ) {
      for ( SmartRefVector<LHCb::Particle>::const_iterator iD = particle->daughters().begin();
            iD != particle->daughters().end(); ++iD ) {
        sc = backCategoriseParticle( *iD, recurCount + 1 );
        if ( sc.isFailure() ) return sc;
      }
    }
  }

  // return
  return sc;
}

Particle2BackgroundCategoryRelationsAlg::CatRelations*
Particle2BackgroundCategoryRelationsAlg::catRelations( const LHCb::Particle* particle ) const {
  // TES location for the Particle
  const std::string pLocation = objectLocation( particle->parent() );

  // Does a relations object already exist ?
  CatMap::const_iterator iM = m_catMap.find( pLocation );
  if ( iM == m_catMap.end() ) {
    // Make a new relations object
    CatRelations* catRelations = new CatRelations();

    // Form TES location for the new object
    std::string rLoc = pLocation;
    boost::replace_all( rLoc, "/Particles", "/P2BCRelations" );

    // Save to the TES
    put( catRelations, rLoc );

    // Update the map
    m_catMap[pLocation] = catRelations;

    // return the new object
    return catRelations;
  }

  // Return the saved object
  return iM->second;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Particle2BackgroundCategoryRelationsAlg )
