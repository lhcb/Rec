/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticle2MCWeightedAssociator.h" // Interface
#include "Kernel/Particle2MCAssociatorBase.h"
#include "LoKi/PhysMCTypes.h"

/** @class P2MCPFromProtoP P2MCPFromProtoP.h
 *
 *  ProtoParticle to MC association tool
 *
 *  @author Juan PALACIOS
 *  @date   2009-03-31
 */
class P2MCPFromProtoP : public Particle2MCAssociatorBase {

public:
  /// Standard constructor
  using Particle2MCAssociatorBase::Particle2MCAssociatorBase;

private:
  Particle2MCParticle::ToVector relatedMCPsImpl( const LHCb::Particle*                particle,
                                                 const LHCb::MCParticle::ConstVector& mcParticles ) const override;

  Gaudi::Property<std::vector<std::string>> m_PP2MC{
      this,
      "Locations",
      { "Relations/" + LHCb::ProtoParticleLocation::Charged, "Relations/" + LHCb::ProtoParticleLocation::Upstream,
        "Relations/" + LHCb::ProtoParticleLocation::Neutrals },
      "Protoparticle locations (without /Event)" }; ///< standard addresses (without /Event)
};

//=============================================================================
Particle2MCParticle::ToVector P2MCPFromProtoP::relatedMCPsImpl( const LHCb::Particle*                particle,
                                                                const LHCb::MCParticle::ConstVector& mcps ) const {
  Particle2MCParticle::ToVector associations;
  if ( ( particle->isBasicParticle() ) && particle->proto() ) {
    for ( const std::string& address : m_PP2MC ) {
      LoKi::Types::TablePP2MC* table = getIfExists<LoKi::Types::TablePP2MC>( address, false );
      if ( !table ) { table = getIfExists<LoKi::Types::TablePP2MC>( address, true ); }
      if ( table ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Adding table " << address << endmsg;
        for ( auto const& rel : table->relations( particle->proto() ) ) {
          associations.push_back( MCAssociation( rel.to(), rel.weight() ) );
        }
      } else {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Unable to add table " << address << endmsg;
      }
    }
  } else {
    Warning( "Composite or ProtoParticle-less association not implemented!", StatusCode::SUCCESS, 10 ).ignore();
  }

  // check if the associated MCPs are in the input container, if not,
  // remove the association!
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Calling Particle2MCParticle::FilterMCAssociations ..." << endmsg;
  return Particle2MCParticle::FilterMCAssociations( associations, mcps );
}
//=============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( P2MCPFromProtoP )
//=============================================================================
