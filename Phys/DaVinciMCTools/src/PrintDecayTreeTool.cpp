/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/Particle.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IPrintDecay.h"
#include "Kernel/IPrintDecayTreeTool.h"
#include "Kernel/ParticleProperty.h"
#include "fmt/format.h"
#include <map>
#include <set>

using namespace Gaudi::Units;

//-----------------------------------------------------------------------------
// Implementation file for class : PrintDecayTreeTool
//
// 29/03/2001 : Olivier Dormond
//-----------------------------------------------------------------------------

/** @class PrintDecayTreeTool PrintDecayTreeTool.h
 *
 *  This is an implementation of the  IPrintDecayTreeTool.
 *  It is based on Olivier Dormond's DebugTool.
 *
 *  In the following function a maxDepth is provided with a default value
 *  of -1. That way the tool "PrintDepth" property is used.
 *  @todo write documentation stating what the options do.
 *
 *  @author Olivier Dormond
 *  @author Juan Palacios juancho@nikhef.nl
 *  @date   10/10/2007
 */
class PrintDecayTreeTool final : public extends<GaudiTool, IPrintDecayTreeTool> {

public:
  /// Standard Constructor
  using extends::extends;

  /**
   *   Print decay tree for a given particle.
   *   Here, a maxDepth is provided with a default value
   *   of -1. The default uses the value set by the
   *   "PrintDepth" property.
   */
  void printTree( const LHCb::Particle* mother, int maxDepth = -1 ) const override;

  void printTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const override;

  void printTree( const LHCb::Particle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const override;

  void printAsTree( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::Particle::Range& particles, Particle2MCLinker* assoc ) const override;

  void printAsList( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc ) const override;

private:
  /// Information Types
  enum struct InfoKeys { Name, E, M, P, Pt, Px, Py, Pz, Vx, Vy, Vz, theta, phi, eta, idcl, chi2, PK, PPK };

private:
  void printHeader( MsgStream& log, bool mcfirst, bool associated ) const;

  std::set<std::string> printInfo( const std::string& prefix, const LHCb::MCParticle* part, Particle2MCLinker* assoc,
                                   MsgStream& log ) const;

  std::set<std::string> printInfo( const std::string& prefix, const LHCb::Particle* part, Particle2MCLinker* assoc,
                                   MsgStream& log ) const;

  std::set<std::string> printDecayTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc,
                                        const std::string& prefix, int depth, MsgStream& log ) const;

  std::set<std::string> printDecayTree( const LHCb::Particle* mother, Particle2MCLinker* assoc,
                                        const std::string& prefix, int depth, MsgStream& log ) const;

  void printUsedContainers( MsgStream& log, std::set<std::string> containers ) const;

  /// Get TES location for an object
  template <class TYPE>
  std::string tesLocation( const TYPE obj ) const {
    return ( obj && obj->parent() && obj->parent()->registry() ? obj->parent()->registry()->identifier() : "NotInTES" );
  }

  unsigned int tesCode( const std::string& loc ) const {
    static std::map<std::string, unsigned int> tesLocs;
    static unsigned int                        lastTESCode( 0 );
    if ( tesLocs.find( loc ) == tesLocs.end() ) { tesLocs[loc] = lastTESCode++; }
    return tesLocs[loc];
  }

private:
  ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{
      this, "ParticlePropertyService", "LHCb::ParticlePropertySvc" };     ///< Reference to particle property service
  Gaudi::Property<int>         m_depth{ this, "PrintDepth", 999 };        ///< Depth of printing for tree
  Gaudi::Property<int>         m_treeWidth{ this, "TreeWidth", 20 };      ///< width of the tree drawing
  Gaudi::Property<int>         m_fWidth{ this, "FieldWidth", 10 };        ///< width of the data fields
  Gaudi::Property<int>         m_fPrecision{ this, "FieldPrecision", 2 }; ///< precision of the data fields
  Gaudi::Property<std::string> m_arrow{ this, "Arrow", "+-->" };          ///< arrow drawing
  std::vector<InfoKeys>        m_keys{ 0 };                               ///< The list of information to print
  Gaudi::Property<std::string> m_information{
      this, "Information", "Name E M P Pt phi Vz PK PPK",
      [&]( auto const& ) {
        m_keys.clear();
        std::size_t oldpos = 0, pos;
        do {
          pos = m_information.value().find( ' ', oldpos );
          std::string    tok( m_information, oldpos, pos - oldpos );
          constexpr auto tbl = std::array{ std::pair{ "Name", InfoKeys::Name }, std::pair{ "E", InfoKeys::E },
                                           std::pair{ "M", InfoKeys::M },       std::pair{ "P", InfoKeys::P },
                                           std::pair{ "Pt", InfoKeys::Pt },     std::pair{ "Px", InfoKeys::Px },
                                           std::pair{ "Py", InfoKeys::Py },     std::pair{ "Pz", InfoKeys::Pz },
                                           std::pair{ "Vx", InfoKeys::Vx },     std::pair{ "Vy", InfoKeys::Vy },
                                           std::pair{ "Vz", InfoKeys::Vz },     std::pair{ "theta", InfoKeys::theta },
                                           std::pair{ "phi", InfoKeys::phi },   std::pair{ "eta", InfoKeys::eta },
                                           std::pair{ "IDCL", InfoKeys::idcl }, std::pair{ "chi2", InfoKeys::chi2 },
                                           std::pair{ "PK", InfoKeys::PK },     std::pair{ "PPK", InfoKeys::PPK } };
          auto           i   = std::find_if( tbl.begin(), tbl.end(), [&]( const auto& l ) { return tok == l.first; } );
          if ( i != tbl.end() ) {
            m_keys.push_back( i->second );
          } else {
            error() << "Unknown output key '" << tok << "'. Ignoring it." << endmsg;
          }
          oldpos = ( pos != std::string::npos ? pos + 1 : pos );
        } while ( pos != std::string::npos );
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } }; ///< The specification of the values to print
  std::string             m_energyUnitName{ "MeV" };                ///< Unit for energies, momenta and masses
  Gaudi::Property<double> m_energyUnit{
      this, "EnergyUnit", MeV,
      [&]( const auto& ) {
        if ( m_energyUnit <= 0 ) {
          err() << "You have chosen a unit for energies: " << m_energyUnit << endmsg;
          throw GaudiException( "bad energy unit", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        constexpr auto tbl = std::array{ std::pair{ TeV, "TeV" }, std::pair{ GeV, "GeV" }, std::pair{ MeV, "MeV" } };
        auto i = std::find_if( tbl.begin(), tbl.end(), [&]( const auto& e ) { return e.first == m_energyUnit; } );
        if ( i != tbl.end() ) {
          m_energyUnitName = i->second;
        } else {
          warning() << "You have chosen a non-standard unit for energies: " << m_energyUnit << endmsg;
          m_energyUnitName = "???";
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };
  std::string             m_lengthUnitName{ "mm" }; ///< Unit for distances
  Gaudi::Property<double> m_lengthUnit{
      this, "LengthUnit", mm,
      [&]( const auto& ) {
        if ( m_lengthUnit <= 0 ) {
          err() << "You have chosen a bad unit for length: " << m_lengthUnit << endmsg;
          throw GaudiException( "bad length unit", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        constexpr auto tbl = std::array{ std::pair{ mm, "mm" }, std::pair{ cm, "cm" }, std::pair{ m, "m" } };
        auto i = std::find_if( tbl.begin(), tbl.end(), [&]( const auto& e ) { return e.first == m_lengthUnit; } );
        if ( i != tbl.end() ) {
          m_lengthUnitName = i->second;
        } else {
          warning() << "You have chosen a non-standard unit for lengths: " << m_lengthUnit << endmsg;
          m_lengthUnitName = "??";
        }
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true }

  }; /// Unit for distances
};

// Declaration of the AlgTool Factory

DECLARE_COMPONENT( PrintDecayTreeTool )

//=============================================================================
void PrintDecayTreeTool::printHeader( MsgStream& log, bool mcfirst, bool associated ) const {
  static const std::string mctitle = " MCParticle ";
  static const std::string title   = " Particle ";
  const auto*              title1  = ( mcfirst ? &mctitle : &title );
  const auto*              title2  = ( mcfirst ? &title : &mctitle );

  bool name_key = std::any_of( m_keys.begin(), m_keys.end(), [&]( const auto& i ) { return i == InfoKeys::Name; } );
  int  n_keys   = m_keys.size() - ( name_key ? 1 : 0 );
  int  width1   = n_keys * m_fWidth + ( name_key ? m_treeWidth.value() : 0 );
  int  width2   = n_keys * m_fWidth + ( name_key ? m_fWidth.value() : 0 );
  int  left1    = ( width1 - title1->length() - 2 ) / 2;
  int  right1   = width1 - title1->length() - 2 - left1;
  int  left2    = ( width2 - title2->length() - 2 ) / 2;
  int  right2   = width2 - title2->length() - 2 - left2;

  if ( left1 >= 0 ) log << '<' << std::string( left1, '-' );
  log << *title1;
  if ( right1 >= 0 ) log << std::string( right1, '-' ) << '>';

  if ( associated ) {
    if ( left2 >= 0 ) log << '<' << std::string( left2, '-' );
    log << *title2;
    if ( right2 >= 0 ) log << std::string( right2, '-' ) << '>';
  }
  log << endmsg;

  for ( const auto i : m_keys ) switch ( i ) {
    case InfoKeys::Name:
      log << std::setw( m_treeWidth ) << "Name";
      break;
    case InfoKeys::E:
      log << std::setw( m_fWidth ) << "E";
      break;
    case InfoKeys::M:
      log << std::setw( m_fWidth ) << "M";
      break;
    case InfoKeys::P:
      log << std::setw( m_fWidth ) << "P";
      break;
    case InfoKeys::Pt:
      log << std::setw( m_fWidth ) << "Pt";
      break;
    case InfoKeys::Px:
      log << std::setw( m_fWidth ) << "Px";
      break;
    case InfoKeys::Py:
      log << std::setw( m_fWidth ) << "Py";
      break;
    case InfoKeys::Pz:
      log << std::setw( m_fWidth ) << "Pz";
      break;
    case InfoKeys::Vx:
      log << std::setw( m_fWidth ) << "Vx";
      break;
    case InfoKeys::Vy:
      log << std::setw( m_fWidth ) << "Vy";
      break;
    case InfoKeys::Vz:
      log << std::setw( m_fWidth ) << "Vz";
      break;
    case InfoKeys::theta:
      log << std::setw( m_fWidth ) << "theta";
      break;
    case InfoKeys::phi:
      log << std::setw( m_fWidth ) << "phi";
      break;
    case InfoKeys::eta:
      log << std::setw( m_fWidth ) << "eta";
      break;
    case InfoKeys::idcl:
      log << std::setw( m_fWidth ) << "ID CL";
      break;
    case InfoKeys::chi2:
      log << std::setw( m_fWidth ) << "chi2";
      break;
    case InfoKeys::PK:
      log << std::setw( m_fWidth ) << "P(C/K)";
      break;
    case InfoKeys::PPK:
      log << std::setw( m_fWidth ) << "PP(C/K)";
      break;
    }
  if ( associated )
    for ( const auto i : m_keys ) switch ( i ) {
      case InfoKeys::Name:
        log << std::setw( m_fWidth ) << "Name";
        break;
      case InfoKeys::E:
        log << std::setw( m_fWidth ) << "E";
        break;
      case InfoKeys::M:
        log << std::setw( m_fWidth ) << "M";
        break;
      case InfoKeys::P:
        log << std::setw( m_fWidth ) << "P";
        break;
      case InfoKeys::Pt:
        log << std::setw( m_fWidth ) << "Pt";
        break;
      case InfoKeys::Px:
        log << std::setw( m_fWidth ) << "Px";
        break;
      case InfoKeys::Py:
        log << std::setw( m_fWidth ) << "Py";
        break;
      case InfoKeys::Pz:
        log << std::setw( m_fWidth ) << "Pz";
        break;
      case InfoKeys::Vx:
        log << std::setw( m_fWidth ) << "Vx";
        break;
      case InfoKeys::Vy:
        log << std::setw( m_fWidth ) << "Vy";
        break;
      case InfoKeys::Vz:
        log << std::setw( m_fWidth ) << "Vz";
        break;
      case InfoKeys::theta:
        log << std::setw( m_fWidth ) << "theta";
        break;
      case InfoKeys::phi:
        log << std::setw( m_fWidth ) << "phi";
        break;
      case InfoKeys::eta:
        log << std::setw( m_fWidth ) << "eta";
        break;
      case InfoKeys::idcl:
        log << std::setw( m_fWidth ) << "ID CL";
        break;
      case InfoKeys::chi2:
        log << std::setw( m_fWidth ) << "chi2";
        break;
      case InfoKeys::PK:
        log << std::setw( m_fWidth ) << "Pkey";
        break;
      case InfoKeys::PPK:
        log << std::setw( m_fWidth ) << "PPkey";
        break;
      }
  log << endmsg;

  for ( const auto i : m_keys ) switch ( i ) {
    case InfoKeys::Name:
      log << std::setw( m_treeWidth ) << " ";
      break;
    case InfoKeys::E:
    case InfoKeys::M:
    case InfoKeys::P:
    case InfoKeys::Pt:
    case InfoKeys::Px:
    case InfoKeys::Py:
    case InfoKeys::Pz:
      log << std::setw( m_fWidth ) << m_energyUnitName;
      break;
    case InfoKeys::Vx:
    case InfoKeys::Vy:
    case InfoKeys::Vz:
      log << std::setw( m_fWidth ) << m_lengthUnitName;
      break;
    case InfoKeys::theta:
      log << std::setw( m_fWidth ) << "mrad";
      break;
    case InfoKeys::phi:
      log << std::setw( m_fWidth ) << "mrad";
      break;
    case InfoKeys::eta:
      log << std::setw( m_fWidth ) << "prap";
      break;
    case InfoKeys::idcl:
    case InfoKeys::chi2:
    case InfoKeys::PK:
    case InfoKeys::PPK:
      log << std::setw( m_fWidth ) << " ";
      break;
    }
  if ( associated )
    for ( const auto i : m_keys ) switch ( i ) {
      case InfoKeys::Name:
        log << std::setw( m_fWidth ) << " ";
        break;
      case InfoKeys::E:
      case InfoKeys::M:
      case InfoKeys::P:
      case InfoKeys::Pt:
      case InfoKeys::Px:
      case InfoKeys::Py:
      case InfoKeys::Pz:
        log << std::setw( m_fWidth ) << m_energyUnitName;
        break;
      case InfoKeys::Vx:
      case InfoKeys::Vy:
      case InfoKeys::Vz:
        log << std::setw( m_fWidth ) << m_lengthUnitName;
        break;
      case InfoKeys::theta:
        log << std::setw( m_fWidth ) << "mrad";
        break;
      case InfoKeys::phi:
        log << std::setw( m_fWidth ) << "mrad";
        break;
      case InfoKeys::eta:
        log << std::setw( m_fWidth ) << "prap";
        break;
      case InfoKeys::idcl:
      case InfoKeys::chi2:
      case InfoKeys::PK:
      case InfoKeys::PPK:
        log << std::setw( m_fWidth ) << " ";
        break;
      }
  log << endmsg;
}
//=============================================================================
std::set<std::string> PrintDecayTreeTool::printInfo( const std::string& prefix, const LHCb::MCParticle* part,
                                                     Particle2MCLinker* assoc, MsgStream& log ) const {
  const LHCb::ParticleProperty* p      = m_ppSvc->find( part->particleID() );
  const LHCb::MCVertex*         origin = part->originVertex();
  std::set<std::string>         usedTesLocations;
  usedTesLocations.insert( tesLocation( part ) );

  for ( const auto i : m_keys ) {
    switch ( i ) {
    case InfoKeys::Name: {
      std::string p_name = p ? p->particle() : "N/A";
      int         p_len  = p_name.length();
      if ( prefix.length() == 0 ) {
        if ( p_len > m_treeWidth ) p_len = m_treeWidth - 1;
        log << p_name << std::string( m_treeWidth - p_len - 1, ' ' );
      } else {
        if ( p_len > (int)( m_treeWidth - prefix.length() - m_arrow.length() ) )
          p_len = m_treeWidth - prefix.length() - m_arrow.length() - 1;
        log << prefix.substr( 0, prefix.length() - 1 ) << m_arrow.value() << p_name
            << std::string( m_treeWidth - prefix.length() - m_arrow.length() - p_len, ' ' );
      }
    } break;
    case InfoKeys::E:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().e() / m_energyUnit;
      break;
    case InfoKeys::M:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().M() / m_energyUnit;
      break;
    case InfoKeys::P:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().Vect().R() / m_energyUnit;
      break;
    case InfoKeys::Pt:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().Pt() / m_energyUnit;
      break;
    case InfoKeys::Px:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().px() / m_energyUnit;
      break;
    case InfoKeys::Py:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().py() / m_energyUnit;
      break;
    case InfoKeys::Pz:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().pz() / m_energyUnit;
      break;
    case InfoKeys::Vx:
      if ( origin )
        log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().x() / m_lengthUnit;
      else
        log << std::setw( m_fWidth ) << " N/A ";
      break;
    case InfoKeys::Vy:
      if ( origin )
        log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().y() / m_lengthUnit;
      else
        log << std::setw( m_fWidth ) << " N/A ";
      break;
    case InfoKeys::Vz:
      if ( origin )
        log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().z() / m_lengthUnit;
      else
        log << std::setw( m_fWidth ) << " N/A ";
      break;
    case InfoKeys::theta:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().theta() / mrad;
      break;
    case InfoKeys::phi:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().phi() / mrad;
      break;
    case InfoKeys::eta:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().Eta();
      break;
    case InfoKeys::idcl:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << 1.0;
      break;
    case InfoKeys::chi2:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << 1.0;
      break;
    case InfoKeys::PK:
    case InfoKeys::PPK:
      break;
    }
  }

  if ( assoc ) {
    const auto* reco = assoc->range( part ).try_front();
    if ( reco ) {
      const auto         pLoc  = tesLocation( reco );
      const auto         ppLoc = tesLocation( reco->proto() );
      std::ostringstream mess;
      for ( const auto i : m_keys ) {
        switch ( i ) {
        case InfoKeys::Name: {
          p                  = m_ppSvc->find( reco->particleID() );
          std::string p_name = p ? p->particle() : "N/A";
          int         p_len  = p_name.length();
          if ( p_len > m_fWidth - 1 ) p_len = m_fWidth - 2;
          log << ' ' << p_name << std::string( m_fWidth - 1 - p_len, ' ' );
        } break;
        case InfoKeys::E:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().e() / m_energyUnit;
          break;
        case InfoKeys::M:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().M() / m_energyUnit;
          break;
        case InfoKeys::P:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
              << reco->momentum().Vect().R() / m_energyUnit;
          break;
        case InfoKeys::Pt:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().Pt() / m_energyUnit;
          break;
        case InfoKeys::Px:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().px() / m_energyUnit;
          break;
        case InfoKeys::Py:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().py() / m_energyUnit;
          break;
        case InfoKeys::Pz:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().pz() / m_energyUnit;
          break;
        case InfoKeys::Vx:
          if ( origin )
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
                << reco->referencePoint().x() / m_lengthUnit;
          else
            log << std::setw( m_fWidth ) << " N/A ";
          break;
        case InfoKeys::Vy:
          if ( origin )
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
                << reco->referencePoint().y() / m_lengthUnit;
          else
            log << std::setw( m_fWidth ) << " N/A ";
          break;
        case InfoKeys::Vz:
          if ( origin )
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
                << reco->referencePoint().z() / m_lengthUnit;
          else
            log << std::setw( m_fWidth ) << " N/A ";
          break;
        case InfoKeys::theta:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().theta() / mrad;
          break;
        case InfoKeys::phi:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().phi() / mrad;
          break;
        case InfoKeys::eta:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().Eta();
          break;
        case InfoKeys::idcl:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->confLevel();
          break;
        case InfoKeys::chi2:
          if ( reco->proto() ) {
            if ( reco->proto()->track() ) {
              log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->proto()->track()->chi2PerDoF();
            } else {
              log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << -1.;
            }
          } else if ( reco->endVertex() ) {
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
                << reco->endVertex()->chi2() / reco->endVertex()->nDoF();
          } else {
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << -2;
          }
          break;
        case InfoKeys::PK:
          usedTesLocations.insert( pLoc );
          mess.str( "" );
          mess << tesCode( pLoc ) << "/" << reco->key();
          log << fmt::format( "{:>{}}", mess.str(), m_fWidth.value() );
          break;
        case InfoKeys::PPK:
          if ( reco->proto() ) {
            usedTesLocations.insert( ppLoc );
            mess.str( "" );
            mess << tesCode( ppLoc ) << "/" << reco->proto()->key();
            log << fmt::format( "{:>{}}", mess.str(), m_fWidth.value() );
          } else {
            log << fmt::format( "{:>{}}", "N/A", m_fWidth.value() );
          }
          break;
        }
      }
    } else {
      log << "  No associated particle";
    }
  }
  log << endmsg;
  return usedTesLocations;
}
//=============================================================================
std::set<std::string> PrintDecayTreeTool::printInfo( const std::string& prefix, const LHCb::Particle* reco,
                                                     Particle2MCLinker* assoc, MsgStream& log ) const {
  // get Particle info
  auto                  p = m_ppSvc->find( reco->particleID() );
  std::set<std::string> usedTesLocations;

  const auto         pLoc  = tesLocation( reco );
  const auto         ppLoc = tesLocation( reco->proto() );
  std::ostringstream mess;

  for ( const auto i : m_keys ) {
    switch ( i ) {
    case InfoKeys::Name: {
      std::string p_name = p ? p->particle() : "N/A";
      int         p_len  = p_name.length();
      if ( prefix.length() == 0 ) {
        if ( p_len > m_treeWidth ) p_len = m_treeWidth - 1;
        log << p_name << std::string( m_treeWidth - p_len - 1, ' ' );
      } else {
        if ( p_len > (int)( m_treeWidth - prefix.length() - m_arrow.length() ) )
          p_len = m_treeWidth - prefix.length() - m_arrow.length() - 1;
        log << prefix.substr( 0, prefix.length() - 1 ) << m_arrow.value() << p_name
            << std::string( m_treeWidth - prefix.length() - m_arrow.length() - p_len, ' ' );
      }
    } break;
    case InfoKeys::E:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().e() / m_energyUnit;
      break;
    case InfoKeys::M:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().M() / m_energyUnit;
      break;
    case InfoKeys::P:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().Vect().R() / m_energyUnit;
      break;
    case InfoKeys::Pt:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().Pt() / m_energyUnit;
      break;
    case InfoKeys::Px:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().px() / m_energyUnit;
      break;
    case InfoKeys::Py:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().py() / m_energyUnit;
      break;
    case InfoKeys::Pz:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().pz() / m_energyUnit;
      break;
    case InfoKeys::Vx:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->referencePoint().x() / m_lengthUnit;
      break;
    case InfoKeys::Vy:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->referencePoint().y() / m_lengthUnit;
      break;
    case InfoKeys::Vz:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->referencePoint().z() / m_lengthUnit;
      break;
    case InfoKeys::theta:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().theta() / mrad;
      break;
    case InfoKeys::phi:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().phi() / mrad;
      break;
    case InfoKeys::eta:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->momentum().Eta();
      break;
    case InfoKeys::idcl:
      log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->confLevel();
      break;
    case InfoKeys::chi2:
      if ( reco->proto() ) {
        if ( reco->proto()->track() ) {
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << reco->proto()->track()->chi2PerDoF();
        } else
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << -1.;
      } else if ( reco->endVertex() ) {
        log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
            << reco->endVertex()->chi2() / reco->endVertex()->nDoF();
      } else {
        log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << -2;
      }
      break;
    case InfoKeys::PK:
      usedTesLocations.insert( pLoc );
      mess.str( "" );
      mess << tesCode( pLoc ) << "/" << reco->key();
      log << fmt::format( "{:>{}}", mess.str(), m_fWidth.value() );
      break;
    case InfoKeys::PPK:
      if ( reco->proto() ) {
        usedTesLocations.insert( ppLoc );
        mess.str( "" );
        mess << tesCode( ppLoc ) << "/" << reco->proto()->key();
        log << fmt::format( "{:>{}}", mess.str(), m_fWidth.value() );
      } else {
        log << fmt::format( "{:>{}}", "N/A", m_fWidth.value() );
      }
      break;
    }
  }

  if ( assoc ) {
    const LHCb::MCParticle* part = assoc->range( reco ).try_front();
    if ( part ) {
      for ( const auto i : m_keys ) {
        switch ( i ) {
        case InfoKeys::Name: {
          p                  = m_ppSvc->find( reco->particleID() );
          std::string p_name = p ? p->particle() : "N/A";
          int         p_len  = p_name.length();
          if ( p_len > m_fWidth - 1 ) p_len = m_fWidth - 2;
          log << ' ' << p_name << std::string( m_fWidth - 1 - p_len, ' ' );
        } break;
        case InfoKeys::E:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().e() / m_energyUnit;
          break;
        case InfoKeys::M:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().M() / m_energyUnit;
          break;
        case InfoKeys::P:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision )
              << part->momentum().Vect().R() / m_energyUnit;
          break;
        case InfoKeys::Pt:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().Pt() / m_energyUnit;
          break;
        case InfoKeys::Px:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().px() / m_energyUnit;
          break;
        case InfoKeys::Py:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().py() / m_energyUnit;
          break;
        case InfoKeys::Pz:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().pz() / m_energyUnit;
          break;
        case InfoKeys::Vx: {
          const LHCb::MCVertex* origin = part->originVertex();
          if ( origin )
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().x() / m_lengthUnit;
          else
            log << std::setw( m_fWidth ) << " N/A ";
        } break;
        case InfoKeys::Vy: {
          const LHCb::MCVertex* origin = part->originVertex();
          if ( origin ) {
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().y() / m_lengthUnit;
          } else {
            log << std::setw( m_fWidth ) << " N/A ";
          }
        } break;
        case InfoKeys::Vz: {
          const LHCb::MCVertex* origin = part->originVertex();
          if ( origin ) {
            log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << origin->position().z() / m_lengthUnit;
          } else {
            log << std::setw( m_fWidth ) << " N/A ";
          }
        } break;
        case InfoKeys::theta:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().theta() / mrad;
          break;
        case InfoKeys::phi:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().phi() / mrad;
          break;
        case InfoKeys::eta:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << part->momentum().Eta();
          break;
        case InfoKeys::idcl:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << 1.0;
          break;
        case InfoKeys::chi2:
          log << std::setw( m_fWidth ) << std::setprecision( m_fPrecision ) << 1.0;
          break;
        case InfoKeys::PK:
        case InfoKeys::PPK:
          break;
        }
      }
    } else {
      log << "  No associated particle";
    }
  }
  log << endmsg;
  return usedTesLocations;
}
//=============================================================================
void PrintDecayTreeTool::printTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc, int maxDepth ) const {
  if ( maxDepth == -1 ) { maxDepth = m_depth; }

  if ( !mother ) {
    Error( "printTree called with NULL MCParticle" ).ignore();
    return;
  }

  printHeader( info(), true, assoc != nullptr );

  info().setf( std::ios::fixed, std::ios::floatfield );
  std::set<std::string> usedTesLocations = printDecayTree( mother, assoc, "", maxDepth, info() );
  info() << endmsg;
  printUsedContainers( info(), usedTesLocations );
  info() << endmsg;
}
//=============================================================================
void PrintDecayTreeTool::printAsTree( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc ) const {
  printHeader( info(), true, assoc != nullptr );

  info().setf( std::ios::fixed, std::ios::floatfield );
  for ( const auto i : particles ) {
    if ( !i->originVertex() || !i->originVertex()->mother() ) printDecayTree( i, assoc, "", m_depth, info() );
  }
  info() << endmsg;
}
//=============================================================================
std::set<std::string> PrintDecayTreeTool::printDecayTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc,
                                                          const std::string& prefix, int depth, MsgStream& log ) const {
  std::set<std::string> usedTesLocations;
  usedTesLocations.insert( tesLocation( mother ) );

  usedTesLocations.merge( printInfo( prefix, mother, assoc, log ) );

  if ( depth ) {
    for ( const auto& v : mother->endVertices() ) {
      for ( const auto& dau : v->products() ) {
        if ( ( dau == v->products().back() ) && ( v == mother->endVertices().back() ) ) {
          usedTesLocations.merge( printDecayTree( dau, assoc, prefix + ' ', depth - 1, log ) );
        } else {
          usedTesLocations.merge( printDecayTree( dau, assoc, prefix + '|', depth - 1, log ) );
        }
      }
    }
  }
  return usedTesLocations;
}
//=============================================================================
void PrintDecayTreeTool::printTree( const LHCb::Particle* mother, int maxDepth ) const {
  printTree( mother, nullptr, maxDepth );
}
//=============================================================================
void PrintDecayTreeTool::printTree( const LHCb::Particle* mother, Particle2MCLinker* assoc, int maxDepth ) const {
  if ( maxDepth == -1 ) { maxDepth = m_depth; }

  if ( !mother ) {
    err() << "printTree called with NULL Particle" << endmsg;
    return;
  }

  printHeader( info(), false, assoc != nullptr );

  info().setf( std::ios::fixed, std::ios::floatfield );
  std::set<std::string> usedTesLocations = printDecayTree( mother, assoc, "", maxDepth, info() );
  info() << endmsg;
  printUsedContainers( info(), usedTesLocations );
  info() << endmsg;
}
//=============================================================================
std::set<std::string> PrintDecayTreeTool::printDecayTree( const LHCb::Particle* mother, Particle2MCLinker* assoc,
                                                          const std::string& prefix, int depth, MsgStream& log ) const {
  std::set<std::string> usedTesLocations = printInfo( prefix, mother, assoc, log );

  if ( depth ) {
    // if ( !mother->endVertex() ) return; // fails for pi0 -> gamma gamma candidates
    for ( const auto& prod : mother->daughters() ) {
      if ( prod != mother->daughters().back() ) {
        usedTesLocations.merge( printDecayTree( prod, assoc, prefix + '|', depth - 1, log ) );
      } else {
        usedTesLocations.merge( printDecayTree( prod, assoc, prefix + ' ', depth - 1, log ) );
      }
    }
  }
  return usedTesLocations;
}
//=============================================================================
void PrintDecayTreeTool::printAsList( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc ) const {
  printHeader( info(), true, assoc != nullptr );

  for ( auto const& i : particles ) { printInfo( "", i, assoc, info() ); }
  info() << endmsg;
}
//=============================================================================
void PrintDecayTreeTool::printAsList( const LHCb::Particle::Range& particles, Particle2MCLinker* assoc ) const {
  printHeader( info(), false, assoc != nullptr );

  for ( const auto i : particles ) { printInfo( "", i, assoc, info() ); }
  info() << endmsg;
}
//=============================================================================
void PrintDecayTreeTool::printUsedContainers( MsgStream& log, std::set<std::string> usedTesLocations ) const {
  log << "Used TES locations :-" << endmsg;
  for ( const auto& i : usedTesLocations ) log << fmt::format( " {:6} = '{}'", tesCode( i ), i ) << endmsg;
}
//=============================================================================
