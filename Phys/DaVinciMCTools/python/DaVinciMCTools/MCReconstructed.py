###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from array import array
from typing import Union

import Functors as F  # type: ignore[import]
from Functors.grammar import BoundFunctor  # type: ignore[import]
from Gaudi.Configuration import INFO  # type: ignore[import]
from PyConf.Algorithms import MCReconstructedAlg  # type: ignore[import]
from PyConf.dataflow import DataHandle  # type: ignore[import]
from PyConf.reading import get_pp2mcp_relations, tes_root  # type: ignore[import]


class MCReconstructed:
    """
    `MCReconstructed` is a helper class to create all possible functors
    for a reconstructed track associated to an `LHCb::MCParticle` or `LHCb::MCParticle``s
    (e.g. HasT, HasUT, HasVelo) as member variables.
    It internally configures and calls the `MCReconstructedAlg` algorithm in Phys/MCAssociation.

    The call function `__call__(a_functor)` can be used to apply a given functor to the associated track. See example below.

    (Since the method tuple->farray() only supports vector<double> currently, all array will convert to array of floats.)

    Args:
        input_mcparticles(DataHandle): `LHCb::MCParticle` data handle (TES).
        use_best_mcmatch (bool, optional): If False, associates the reconstructed track
            to all the MCParticles rather than the best matched MCParticle. Defaults to True.
        relations_charged (str, DataHandle, optional): TES locations to the pre-existing relations (Particle -> MCParticle)
            for charged particles. Defaults to "Relations/ChargedPP2MCP".
        relations_neutral (str, DataHandle, optional): TES locations to the pre-existing relations (Particle -> MCParticle)
            for neutral particles. Defaults to "Relations/NeutralPP2MCP".
        output_level (int, optional): Standard Gaudi Algorithm `OutputLevel`.
            Defaults to `Gaudi.Configuration.INFO`.

    Example:
        # Create the helper class
        reconstructed_track = MCReconstructed(MCParticles_TES)

        # Use predefined functor
        allvariables['hasT'] = reconstructed_track.HasT

        # If a new track-like functor becomes available, then we can use the __call__ method
        allvariables['hasT'] = reconstructed_track(<F.NEW_TRACK_FUNCTOR>)
    """

    def __init__(
        self,
        input_mcparticles: DataHandle,
        use_best_mcmatch: bool = True,
        relations_charged: Union[DataHandle, str] = "Relations/ChargedPP2MCP",
        relations_neutral: Union[DataHandle, str] = "Relations/NeutralPP2MCP",
        output_level: int = INFO,
    ):
        root_in_tes = tes_root()

        ChargedPP2MCP = (
            get_pp2mcp_relations(f"{root_in_tes}/{relations_charged}")
            if isinstance(relations_charged, str) and root_in_tes
            else relations_charged
        )
        NeutralPP2MCP = (
            get_pp2mcp_relations(f"{root_in_tes}/{relations_neutral}")
            if isinstance(relations_neutral, str) and root_in_tes
            else relations_neutral
        )

        # Save setup
        self.use_best_mcmatch = use_best_mcmatch

        # Load Algorithm
        self.Algorithm = MCReconstructedAlg(
            Input=input_mcparticles,
            ChargedPP2MCP=ChargedPP2MCP,
            NeutralPP2MCP=NeutralPP2MCP,
            OutputLevel=output_level,
            use_best_mcmatch=use_best_mcmatch,
        )

        # Alias the outputs
        # Table for Charged MCP -> Reconstructed_Track
        self.MC2Reconstructed = self.Algorithm.MC2Reconstructed

        if self.use_best_mcmatch:
            wrap = lambda f, d: F.VALUE_OR(d) @ f
        else:
            wrap = lambda f, d: F.VALUE_OR(array("i", [d])) @ f

        # Predefined track related functors
        self.TrackType = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKTYPE @ F.TRACK), -1
        )
        self.History = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKHISTORY @ F.TRACK), -1
        )
        self.Flag = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKFLAG @ F.TRACK), -1
        )
        self.nDoF = wrap(self.__call__(F.VALUE_OR(-1) @ F.NDOF @ F.TRACK), -1)
        self.ReferencePoint_X = self.__call__(
            F.VALUE_OR(F.NaN) @ F.REFERENCEPOINT_X @ F.TRACK
        )
        self.ReferencePoint_Y = self.__call__(
            F.VALUE_OR(F.NaN) @ F.REFERENCEPOINT_Y @ F.TRACK
        )
        self.ReferencePoint_Z = self.__call__(
            F.VALUE_OR(F.NaN) @ F.REFERENCEPOINT_Z @ F.TRACK
        )
        self.Chi2 = self.__call__(F.CHI2 @ F.TRACK)
        self.Chi2DOF = self.__call__(F.CHI2DOF @ F.TRACK)
        self.GhostProbability = self.__call__(F.GHOSTPROB)
        self.HasT = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKHAST @ F.TRACK), -1
        )
        self.HasUT = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKHASUT @ F.TRACK), -1
        )
        self.HasVelo = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.TRACKHASVELO @ F.TRACK), -1
        )
        self.nHits = wrap(self.__call__(F.VALUE_OR(-1) @ F.NHITS @ F.TRACK), -1)
        self.nVPHits = wrap(self.__call__(F.VALUE_OR(-1) @ F.NVPHITS @ F.TRACK), -1)
        self.nUTHits = wrap(self.__call__(F.VALUE_OR(-1) @ F.NUTHITS @ F.TRACK), -1)
        self.nFTHits = wrap(self.__call__(F.VALUE_OR(-1) @ F.NFTHITS @ F.TRACK), -1)
        self.QOVERP = self.__call__(F.VALUE_OR(F.NaN) @ F.QOVERP @ F.TRACK)

        # Predefined no track related functors
        self.Reconstructed = wrap(
            self.__call__(F.VALUE_OR(-1) @ F.CAST_TO_INT @ F.MC_RECONSTRUCTED), -1
        )
        self.PIDmu = self.__call__(F.PID_MU)
        self.PIDpi = self.__call__(F.PID_PI)
        self.PIDk = self.__call__(F.PID_K)
        self.PIDp = self.__call__(F.PID_P)
        self.PIDe = self.__call__(F.PID_E)

        self.nRecoTracks = None
        self.MCAssoc_Weights = None
        if not self.use_best_mcmatch:
            # Sizes
            self.nRecoTracks = F.VALUE_OR(-1) @ F.MAP_INPUT_SIZE(self.MC2Reconstructed)

            # MC association Weights
            self.MCAssoc_Weights = F.MAP_WEIGHT(Relations=self.MC2Reconstructed)

    # Flexible interface for possible update
    def __call__(self, Functor: BoundFunctor):
        """
        Apply a specified functor to the reconstructed object associated to an `LHCb::MCParticle`.

        Args:
            Functor (BoundFunctor): The functor to be applied.

        Returns:
            BoundFunctor: A functor that returns the array of results.

        Example:
            mcreconstructed_alg = MCReconstructed(MCParticles_TES)
            variables['hasT'] = F.VALUE_OR(-1) @ mcreconstructed_alg(F.VALUE_OR(-1) @ F.TRACKHAST @ F.TRACK)
        """

        if self.use_best_mcmatch:
            return F.MAP_INPUT(Functor, self.MC2Reconstructed)
        else:
            return F.REVERSE_RANGE @ F.MAP_INPUT_ARRAY(Functor, self.MC2Reconstructed)
