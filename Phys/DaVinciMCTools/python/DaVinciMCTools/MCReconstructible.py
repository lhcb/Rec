###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import warnings

import Functors as F  # type: ignore[import]
from PyConf.dataflow import DataHandle  # type: ignore[import]


class MCReconstructible:
    """
    `MCReconstructible` is a helper class to create all possible functors
    for MC tracks (e.g. HasT, HasUT, HasVelo) as member variables.
    This class is used typically by the related `MCReconstructible` functor collection.

    The call function `__call__(mc_property)` can check if the MC track has a specified property. See example below.

    Notes:
        Boole stores the track information into an `LHCb::MCProperty` object. We just refer to it as `MCProperty`.
        The `MCProperty` is indeed an `MCParticle->int` map, and the `int` has the bitwise information about the track
        of each `LHCb::MCParticle`. The class `LHCb::MCTrackInfo` defines several bitwise masks for this `int`.
        We use those masks to access the track information of MC Particles and calculate the reconstructible category.
        So the `mc_property(int)` in this class refers to the corresponding int of an MC Particle stored in the `MCProperty`.

    Args:
        input_mctrackinfo (DataHandle): the `LHCb::MCTrackInfo` handle (TES).

    Example:
        from PyConf.reading import get_mc_track_info
        mc_trackinfo = MCReconstructible(get_mc_track_info())
        allvariables['HasT'] = mc_trackinfo.HasT
        # can use the __call_ function to check if the MC track has a specified property
        allvariables['HasProperty'] = mc_trackinfo(Property)
    """

    def __init__(self, input_mctrackinfo: DataHandle):
        # Save the input
        self.input_mctrackinfo = input_mctrackinfo

        # Load flag
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            import cppyy  # type: ignore[import]
        self._Flag = cppyy.gbl.MCTrackInfo.flagMasks

        # Create base functor
        self.Property = F.MC_PROPERTY(self.input_mctrackinfo)

        # Has sub-detector hits
        # https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/MCEvent/include/Event/MCTrackInfo.h
        self.HasVelo = self.__call__(self._Flag.maskHasVelo)
        self.HasVeloAndT = self.__call__(self._Flag.maskHasVeloAndT)
        self.HasT = self.__call__(self._Flag.maskHasT)
        self.HasT1 = self.__call__(self._Flag.maskHasT1)
        self.HasT2 = self.__call__(self._Flag.maskHasT2)
        self.HasT3 = self.__call__(self._Flag.maskHasT3)
        self.HasT1X = self.__call__(self._Flag.maskT1X)
        self.HasT2X = self.__call__(self._Flag.maskT2X)
        self.HasT3X = self.__call__(self._Flag.maskT3X)
        self.HasT1S = self.__call__(self._Flag.maskT1S)
        self.HasT2S = self.__call__(self._Flag.maskT2S)
        self.HasT3S = self.__call__(self._Flag.maskT3S)
        self.HasUT = self.__call__(self._Flag.maskHasUT)
        self.HasUT1 = self.__call__(self._Flag.maskUT1)
        self.HasUT2 = self.__call__(self._Flag.maskUT2)
        # Within sub-detector acceptance
        # https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/MCEvent/include/Event/MCTrackInfo.h
        self.AccVelo = self.__call__(self._Flag.maskAccVelo)
        self.AccVeloAndT = self.__call__(self._Flag.maskAccVeloAndT)
        self.AccT = self.__call__(self._Flag.maskAccT)
        self.AccT1 = self.__call__(self._Flag.maskAccT1)
        self.AccT2 = self.__call__(self._Flag.maskAccT2)
        self.AccT3 = self.__call__(self._Flag.maskAccT3)
        self.AccT1X = self.__call__(self._Flag.maskAccT1X)
        self.AccT2X = self.__call__(self._Flag.maskAccT2X)
        self.AccT3X = self.__call__(self._Flag.maskAccT3X)
        self.AccT1S = self.__call__(self._Flag.maskAccT1S)
        self.AccT2S = self.__call__(self._Flag.maskAccT2S)
        self.AccT3S = self.__call__(self._Flag.maskAccT3S)
        self.AccUT = self.__call__(self._Flag.maskAccUT)
        self.AccUT1 = self.__call__(self._Flag.maskAccUT1)
        self.AccUT2 = self.__call__(self._Flag.maskAccUT2)
        self.multVelo = self.__call__(self._Flag.maskMultVelo)

        # Reconstructible
        self.Reconstructible = (
            F.VALUE_OR(-1)
            @ F.MC_RECONSTRUCTIBLE
            @ F.MC_PROPERTY(self.input_mctrackinfo)
        )

    def __call__(self, mc_property: int) -> F.grammar.FunctorBase:
        """
        Check if the MC track has a specified property.

        Args:
            mc_property (int): the property to be retrieved.

        Example:
            from PyConf.reading get_mc_track_info
            mc_trackinfo = MCReconstructible(get_mc_track_info())
            allvariables['HasProperty'] = mc_trackinfo(Property)
        """
        return (
            F.VALUE_OR(-1)
            @ F.MC_TRACKINFO(mc_property)
            @ F.MC_PROPERTY(self.input_mctrackinfo)
        )
