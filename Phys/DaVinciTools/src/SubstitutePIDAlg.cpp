/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/// Common files
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "GaudiAlg/Transformer.h"
#include "Kernel/DecayTree.h"
#include "Kernel/ParticleProperty.h"
#include "Relations/Relation1D.h"
#include <Gaudi/Accumulators.h>
#include <GaudiAlg/FunctionalUtilities.h>

// Interfaces
#include "Kernel/IDecayFinder.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticlePropertySvc.h"

using Gaudi::Functional::Traits::useLegacyGaudiAlgorithm;

namespace SubsPIDAlg {
  template <typename RangeType>
  struct Signature {
    using Input_t     = RangeType;
    using Output_t    = RangeType;
    using Selection_t = RangeType;
    using Element_t =
        typename LHCb::Event::simd_zip_t<SIMDWrapper::Scalar,
                                         Input_t>::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
    using Relation_t  = LHCb::Relation1D<const Element_t*, const Element_t*>;
    using AlgOutput_t = std::tuple<Output_t, Selection_t, Relation_t>;
    using AlgInput_t  = Input_t;
    using MultiTransformer =
        Gaudi::Functional::MultiTransformer<AlgOutput_t( const AlgInput_t& ), useLegacyGaudiAlgorithm>;
    inline static const bool is_v1 = false;
  };

  template <>
  struct Signature<LHCb::Particle::Range> {
    using Input_t     = LHCb::Particle::Range;
    using Output_t    = LHCb::Particle::Container;
    using Selection_t = LHCb::Particle::Selection;
    using Element_t   = LHCb::Particle;
    using Relation_t  = LHCb::Relation1D<const Element_t*, const Element_t*>;
    using AlgOutput_t = std::tuple<Output_t, Selection_t, Relation_t>;
    using AlgInput_t  = Input_t;
    using MultiTransformer =
        Gaudi::Functional::MultiTransformer<AlgOutput_t( const AlgInput_t& ), useLegacyGaudiAlgorithm>;
    inline static const bool is_v1 = true;
  };

}; // namespace SubsPIDAlg

// ============================================================================
/** @class SubstitutePIDAlg
 *  An algorithm replace a mass hypothesis of certain decay components by PID
 *  substitution.
 *
 * Use:
 *  @code
 *      from PyConf.Algorithms import SubstitutePIDAlg_Particles
 *
 *      Substitution_PhiG = SubstitutePIDAlg_Particles(
 *          name  = 'Substitution_PhiG',
 *          Input = B_Data,
 *          Decays = ['B0 -> ( K*(892)0 -> K+ K- ) gamma'],
 *          Substitute = {
 *              ' B0 -> ( ^K*(892)0 -> K+ K- ) gamma' : 'phi(1020)',
 *              '^B0 -> (  K*(892)0 -> K+ K- ) gamma' : 'B_s0',
 *          }
 *      )
 *
 *  @endcode
 *
 *  @param Input Location of particles.
 *  @param Decays Decay descriptors.
 *  @param Substitute Substitution map.
 *  @returns AllParticles: Location of all particles and their daughter particles.
 *  @returns Particles: Location of principal particles.
 *  @returns Relation: Relation table from original particle to substituted particle.
 *
 *
 *  @author Jiahui Zhuo jiahui.zhuo@cern.ch
 *  @date 2022-09-16
 */

using Substitute_t = std::map<std::string, std::string>;

template <typename T>
class SubstitutePIDAlg_Base final : public SubsPIDAlg::Signature<T>::MultiTransformer {
public:
  // Constructor
  SubstitutePIDAlg_Base( const std::string& name, ISvcLocator* pSvc )
      : SubsPIDAlg::Signature<T>::MultiTransformer(
            name, pSvc, { typename SubsPIDAlg::Signature<T>::MultiTransformer::KeyValue{ "Input", "" } },
            { typename SubsPIDAlg::Signature<T>::MultiTransformer::KeyValue{ "AllParticles", "" },
              typename SubsPIDAlg::Signature<T>::MultiTransformer::KeyValue{ "Particles", "" },
              typename SubsPIDAlg::Signature<T>::MultiTransformer::KeyValue{ "Relation", "" } } ) {}
  // Initialization
  virtual StatusCode initialize() override;
  /// main function
  typename SubsPIDAlg::Signature<T>::AlgOutput_t
  operator()( const typename SubsPIDAlg::Signature<T>::AlgInput_t& parts ) const override;

private:
  /// properties
  Gaudi::Property<std::vector<std::string>>           m_decays{ this, "Decays", {}, "Decay descriptors." };
  Gaudi::Property<std::map<std::string, std::string>> m_substitute{ this, "Substitute", {}, "Substitution map." };
  /// service
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{ this, "ParticleProperty",
                                                                   "LHCb::ParticlePropertySvc" };
  /// tools
  ToolHandle<IDecayFinder>         m_decay_finder{ this, "DecayFinder", "DecayFinder" };
  ToolHandle<IParticleDescendants> m_descendants{ this, "Descendants", "ParticleDescendants" };

  // counters (event and ghost)
  mutable Gaudi::Accumulators::Counter<> m_num_event{ this, "#Events" };
  mutable Gaudi::Accumulators::Counter<> m_num_tree{ this, "#Substituted tree" };
};

/// Initialization
template <typename T>
StatusCode SubstitutePIDAlg_Base<T>::initialize() {
  this->verbose() << "Initialising SubstitutePIDAlg algorithm" << endmsg;
  return SubsPIDAlg::Signature<T>::MultiTransformer::initialize().andThen( [&]() -> StatusCode {
    if ( !m_particlePropertySvc ) { return this->Error( "Unable to access the Particle Property Service." ); };
    if ( !m_decay_finder ) { return this->Error( "Unable to create the new decay finder." ); };
    if ( !m_descendants ) { return this->Error( "Unable to create the ParticleDescendants tool." ); };
    if ( m_decays.empty() ) { return this->Error( "The decay descriptors can not be empty." ); };
    if ( m_substitute.empty() ) { return this->Error( "The substitution map can not be empty." ); };
    return StatusCode::SUCCESS;
  } );
}

void RunSubstitution_Particles( const std::map<const LHCb::Particle*, std::tuple<int, double>>& substitutions,
                                LHCb::Particle*                                                 particle ) {
  if ( !particle ) return;

  // End of recursive
  if ( particle->isBasicParticle() && substitutions.count( particle ) ) {
    const auto p0     = particle->momentum();
    const auto target = substitutions.at( particle );
    //
    const auto PID = std::get<0>( target );
    const auto M1  = std::get<1>( target );
    const auto E1  = std::sqrt( p0.P2() + M1 * M1 );
    //
    particle->setParticleID( LHCb::ParticleID( PID ) );
    particle->setMomentum( Gaudi::LorentzVector( p0.Px(), p0.Py(), p0.Pz(), E1 ) );
    particle->setMeasuredMass( M1 );
    return;
  };

  // Loop all daughters
  auto&                daughters = particle->daughters();
  Gaudi::LorentzVector FourMomentum;
  for ( auto& daughter : daughters ) {
    RunSubstitution_Particles( substitutions, const_cast<LHCb::Particle*>( daughter.data() ) );
    FourMomentum += daughter->momentum();
  };

  // Get total correction
  if ( !daughters.empty() ) {
    particle->setMomentum( FourMomentum );
    particle->setMeasuredMass( FourMomentum.M() );
  };

  // Set PID for mother if needed
  if ( substitutions.count( particle ) ) {
    const auto target = substitutions.at( particle );
    const auto PID    = std::get<0>( target );
    particle->setParticleID( std::move( LHCb::ParticleID( PID ) ) );
  };
};

template <typename T>
typename SubsPIDAlg::Signature<T>::AlgOutput_t
SubstitutePIDAlg_Base<T>::operator()( const typename SubsPIDAlg::Signature<T>::AlgInput_t& particles ) const {
  /// Algorithm for v1 particle
  typename SubsPIDAlg::Signature<T>::Output_t    output_particles;
  typename SubsPIDAlg::Signature<T>::Selection_t output_selection;
  typename SubsPIDAlg::Signature<T>::Relation_t  output_relations;

  /// Preselection
  std::vector<const LHCb::Particle*> input_vector( particles.begin(), particles.end() );
  std::vector<const LHCb::Particle*> selected_particles;
  for ( const auto& decay : m_decays ) {
    std::vector<const LHCb::Particle*> found_particles;
    StatusCode                         sc = m_decay_finder->findDecay( decay, input_vector, found_particles );
    if ( sc.isFailure() )
      throw GaudiException( "Could not find any candidates with the specified descriptor. Please check!",
                            "SubstitutePIDAlg", StatusCode::FAILURE );
    selected_particles.insert( selected_particles.end(), found_particles.begin(), found_particles.end() );
  };

  /// Remove duplications
  std::sort( selected_particles.begin(), selected_particles.end() );
  auto end_unique = std::unique( selected_particles.begin(), selected_particles.end() );
  selected_particles.erase( end_unique, selected_particles.end() );

  /// Copy the whole tree to output
  std::vector<LHCb::Particle*> mother_vector;
  for ( const auto particle : selected_particles ) {
    if ( !particle ) continue;

    // Clone the tree (and all descendants)
    LHCb::DecayTree tree( *particle );

    // Store the tree (and all descendants)
    const auto daughters = m_descendants->descendants( particle );
    if ( this->msgLevel( MSG::VERBOSE ) )
      this->verbose() << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endmsg;
    for ( auto& daughter : daughters ) {
      auto cloned_daughter = const_cast<LHCb::Particle*>( tree.findClone( *daughter ) );
      output_particles.insert( cloned_daughter );
      output_relations.relate( daughter, cloned_daughter ).ignore();
      if ( this->msgLevel( MSG::VERBOSE ) )
        this->verbose() << "Adding daughter: ptr = " << cloned_daughter
                        << ", PID = " << cloned_daughter->particleID().pid() << endmsg;
    };
    auto cloned_particle = tree.release();
    output_particles.insert( cloned_particle );
    output_selection.insert( cloned_particle );
    output_relations.relate( particle, cloned_particle ).ignore();
    mother_vector.push_back( cloned_particle );
    if ( this->msgLevel( MSG::VERBOSE ) ) {
      this->verbose() << "Adding particle: ptr = " << cloned_particle
                      << ", PID = " << cloned_particle->particleID().pid() << endmsg;
      this->verbose() << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endmsg;
    };
  };

  /// Load the PID susbtitution
  std::map<const LHCb::Particle*, std::tuple<int, double>> substitutions;

  std::vector<const LHCb::Particle*> output_vector( output_particles.begin(), output_particles.end() );
  for ( const auto& [decay_descriptor, particle_name] : m_substitute ) {
    // Get PID and mass
    auto property = m_particlePropertySvc->find( particle_name );
    if ( !property )
      throw GaudiException( std::string( "The particle name \"" ) + particle_name + std::string( "\" is not valid." ),
                            "SubstitutePIDAlg", StatusCode::FAILURE );
    auto pid  = property->particleID().pid();
    auto mass = property->mass();

    // Find the target
    std::vector<const LHCb::Particle*> selected_vector;
    m_decay_finder->findDecay( decay_descriptor, output_vector, selected_vector ).ignore();

    // Save the substitution rule
    if ( this->msgLevel( MSG::VERBOSE ) )
      this->verbose() << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endmsg;
    for ( auto& selected : selected_vector ) {
      substitutions.insert_or_assign( selected, std::make_tuple( pid, mass ) );
      if ( this->msgLevel( MSG::VERBOSE ) )
        this->verbose() << "Substitution: original = " << selected->particleID().pid() << ", target = " << pid
                        << endmsg;
    };
    if ( this->msgLevel( MSG::VERBOSE ) )
      this->verbose() << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endmsg;
  };

  /// Run substitution
  for ( auto& mother : mother_vector ) {
    RunSubstitution_Particles( substitutions, mother );
    ++m_num_tree;
  };

  /// Update counter
  ++m_num_event;

  // Mark substitution
  return std::make_tuple( std::move( output_particles ), std::move( output_selection ), output_relations );
}

DECLARE_COMPONENT_WITH_ID( SubstitutePIDAlg_Base<LHCb::Particle::Range>, "SubstitutePIDAlg_Particles" )

// The v2 particle support will be added once the new DecayFinder can works with v2 particle.

// DECLARE_COMPONENT_WITH_ID( SubstitutePIDAlg_Base<LHCb::Event::Composites>, "SubstitutePIDAlg_Composites" )
// DECLARE_COMPONENT_WITH_ID( SubstitutePIDAlg_Base<LHCb::Event::ChargedBasics>, "SubstitutePIDAlg_ChargedBasics" )
