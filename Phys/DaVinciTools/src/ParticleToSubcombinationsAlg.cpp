/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IGeometryInfo.h"

#include "Event/Particle.h"
#include "Event/Vertex.h"

#include "Kernel/IParticleCombiner.h"
#include "Kernel/ParticleID.h"
#include "Relations/Relation1D.h"

#include "LoKi/PhysExtract.h"

#include <cassert>

namespace {
  using relation_table_t = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;
  using in_parts         = LHCb::Particle::Range;
  using out_parts_t      = LHCb::Particles;
  using out_vertices_t   = LHCb::Vertices;
  using alg_out_t        = std::tuple<out_parts_t, out_vertices_t, relation_table_t>;
} // namespace

/**
 *
 *  Simple algortih to turn a decay into (2body) sub-decays,
 *  outputting as well a relation of the mother to the new combinations.
 *
 *  All 'basic' particles are obtained from the decay tree,
 *  such that even combinations of nested particles are considered.
 *
 *  The subcombinations can be fitted using a vertex fitter, configured
 *  via the particle combiner toolhandle. If not fit is desired,
 *  one can use the 'ParticleAdder' combiner instead.
 *
 *  A standard use-case for this is that in combination with HltTisTos
 *  information of 2-body combinations of  a multibody decay.
 *
 *  @author Laurent Dufour <laurent.dufour@cern.ch>
 */
class ParticleToSubcombinationsAlg
    : public Gaudi::Functional::MultiTransformer<alg_out_t( in_parts const&, DetectorElement const& ),
                                                 LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  // ==========================================================================

  ParticleToSubcombinationsAlg( const std::string& name, ISvcLocator* pSvc )
      : MultiTransformer( name, pSvc,
                          { KeyValue{ "Input", "" }, KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                          {
                              KeyValue{ "OutputParticles", "" },
                              KeyValue{ "OutputVertices", "" },
                              KeyValue{ "OutputRelations", "" },
                          } ) {}

  alg_out_t operator()( in_parts const& parts, DetectorElement const& lhcb ) const override {
    ++m_eventCount;

    // output
    out_parts_t      parts_out;
    out_vertices_t   vertices;
    relation_table_t output_relations;

    m_candidateCount += parts.size();
    for ( const LHCb::Particle* orig_p : parts ) {
      if ( orig_p->isBasicParticle() ) continue;

      ++m_candidateCount;

      LHCb::Particle::ConstVector basic_particles;

      //  Could make it possible to apply more cuts
      //  in here, but let's keep it simple.
      LoKi::Extract::particles( orig_p, std::back_inserter( basic_particles ),
                                [&]( const LHCb::Particle* p ) { return p->isBasicParticle(); } );

      for ( unsigned int p_a_id = 0; p_a_id < basic_particles.size() - 1; ++p_a_id ) {
        for ( unsigned int p_b_id = p_a_id + 1; p_b_id < basic_particles.size(); ++p_b_id ) {
          auto                        particle1 = new LHCb::Particle( orig_p->particleID() );
          LHCb::Particle::ConstVector children;
          children.reserve( 2 ); // Reserve space for 2 children
          children.push_back( basic_particles[p_a_id] );
          children.push_back( basic_particles[p_b_id] );

          auto vtx    = orig_p->endVertex()->clone();
          auto combSc = m_combiner->combine( children, *particle1, *vtx, *lhcb.geometry() );
          if ( combSc.isFailure() ) ++m_failedVertexFitCount;

          // required to add to a keyed container first before
          // being able to relate to other particle
          parts_out.insert( particle1 );
          vertices.insert( vtx );

          output_relations.relate( orig_p, particle1 ).ignore();
        }
      }
    }

    m_nCombinations += parts_out.size();
    return std::make_tuple( std::move( parts_out ), std::move( vertices ), output_relations );
  }

private:
  ToolHandle<IParticleCombiner> m_combiner{ this, "ParticleCombiner", "ParticleVertexFitter" };

  // counters
  mutable Gaudi::Accumulators::Counter<>                 m_failedVertexFitCount{ this, "# failed vertex fit" };
  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{ this, "Events" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_candidateCount{ this, "Input Particles" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nCombinations{ this, "Combinations created" };
};

DECLARE_COMPONENT( ParticleToSubcombinationsAlg )
