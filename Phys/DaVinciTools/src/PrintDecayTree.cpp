/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IPrintDecay.h"
#include "LHCbAlgs/Consumer.h"

/**
 *  The simplified version of the algorithm PrintTree,
 *  which deals only with the reconstructed particles
 *  @see PrintTree
 *  @see IPrintDecay
 *  @author Vanya BELYAEV Ivan.Belayev@cern.ch
 *  @date 2008-03-30
 *  Adapted to Gaudi functional
 *  @author Patrick Koppenburg
 *  @date 2020-12-17
 */
class PrintDecayTree : public LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range& )> {
public:
  PrintDecayTree( const std::string& name, ISvcLocator* pSvc ) : Consumer( name, pSvc, { KeyValue{ "Input", "" } } ) {}
  void operator()( const LHCb::Particle::Range& parts ) const override {
    for ( const auto* p : parts ) { m_printDecay->printTree( p, -1 ); }
    if ( !parts.empty() ) {
      ++m_eventCount;
      m_candidateCount += parts.size();
    }
  }

private:
  ToolHandle<IPrintDecay> m_printDecay = { this, "PrintDecayTreeTool", "PrintDecayTreeTool" };

  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{ this, "Events" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_candidateCount{ this, "Candidates" };
};

/// declare the factory (needed for instantiation)
DECLARE_COMPONENT( PrintDecayTree )
