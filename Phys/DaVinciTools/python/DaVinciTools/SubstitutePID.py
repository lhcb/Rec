###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import re

import Functors as F
from Functors.grammar import BoundFunctor
from Gaudi.Configuration import ALL, DEBUG, INFO, VERBOSE
from PyConf.Algorithms import SubstitutePIDAlg_Particles
from PyConf.dataflow import DataHandle

from .SimplifiedDecayParser import SimplifiedDecayParser


class SubstitutePID:
    """
    Python tool to help with the configuration of the PID substitution `SubstitutePIDAlg_Particles`
    and the creation of functors to access the necessary information.

    .. note::
        You can configure the mass hypothesis subsitution using the following syntax:
            ' Old_PID{{New_PID}} '

        The '[]CC' syntax is not yet supported in the SubstitutePID tool, you have to
        specify the substitution rule explicitly for each CC case.
        e.g.
        '[B+ -> K+ K+ K-{{pi-}}]CC' => ['B+ -> K+ K+ K-{{pi-}}', 'B- -> K- K- K+{{pi+}}']

    Args:
        name (str): name of the substitution algorithm, will printed in the Gaudi.
        substitutions (list): substitution rules, using the substitution syntax.
        input_particles (DataHandle): the input_particles data handle (TES)
        output_level (int, optional): standard Gaudi Algorithm OutputLevel.
        allow_duplicate_substitutepid_instances (bool, optional): When running multiple instances of DecayTreeFitter (DTF) with substitutePID, such as one with a primary vertex (PV) constraint and another with a mass constraint, the same instance of substitutePID gets defined.This can cause failures in PyConf. To avoid these failures, enable this option to allow duplicate substitutePID instances to be used with different DTF constraints.
        Example:
        # Create the tool
        Subs_PhiG = SubstitutePID(
            name = 'Subs_PhiG',
            input_particles = B_Data,
            substitutions = [
                'B0{{B_s0}} -> ( K*(892)0{{phi(1020)}} ->  K+ K- ) gamma'
            ]
        )

        Subs_KstG = SubstitutePID(
            name = 'Subs_KstG',
            input_particles = B_Data,
            substitutions = [
                'B0 -> ( K*(892)0 -> K+ K-{{pi-}} ) gamma'
            ]
        )

        # Get the energy of the substituted particle
        allvariables['ENERGY'] = F.ENERGY
        allvariables['PhiG_ENERGY'] = Subs_PhiG( F.ENERGY )
        allvariables['KstG_ENERGY'] = Subs_KstG( F.ENERGY )
    """

    def __init__(
        self,
        name: str,
        input_particles: DataHandle,
        substitutions: list[str],
        allow_duplicate_substitutepid_instances: bool = False,
        output_level: int = INFO,
    ):
        # Check
        if not substitutions:
            raise ValueError("The 'substitutions' rules can't be empty.")

        # Expand subsitutions if needed
        expanded_subsitutions = []
        for substitution in substitutions:
            if substitution.lower().find("]cc") != -1:
                expanded_subsitutions += SimplifiedDecayParser(substitution).expand_cc()
            else:
                expanded_subsitutions += [substitution]

        # Extend subsitituions
        subsitution_map = self._get_subsitution_map(expanded_subsitutions, output_level)

        # Find all possible decays
        decays = []
        for decay_descriptor, _ in subsitution_map.items():
            simple_descriptor = decay_descriptor.replace("^", " ")
            repeated = False
            for decay in decays:
                if decay.replace(" ", "") == simple_descriptor.replace(" ", ""):
                    repeated = True
                    break
            if not repeated:
                decays.append(simple_descriptor)

        # Create gaudi algorithm
        self.Algorithm = SubstitutePIDAlg_Particles(
            name=name,
            Input=input_particles,
            Decays=decays,
            Substitute=subsitution_map,
            allow_duplicate_instances_with_distinct_names=allow_duplicate_substitutepid_instances,
            OutputLevel=output_level,
        )

        # Store the algorithm result
        self.AllParticles = self.Algorithm.AllParticles
        self.Particles = self.Algorithm.Particles
        self.Relation = self.Algorithm.Relation  # Particle -> PID Substituted Particle

    def __call__(self, Functor: BoundFunctor):
        """
        Apply a specified functor to the resultant particle
        obtained from substituting the PID.

        Args:
            Functor (BoundFunctor): the functor to be applied

        Return:
            BoundFunctor: result functor

        Example:
            variables['SUBS_CHI2DOF'] = Subs(F.CHI2DOF)
        """
        return F.MAP_INPUT(Functor, self.Relation)

    def _get_subsitution_map(self, substitutions: list, output_level: int):
        if output_level in {DEBUG, VERBOSE, ALL}:
            print("==============================")
            print(
                "SubstitutePID will extend the substitution rules to the following substitution map:"
            )
            print("{")

        substitution_map = {}

        # Add all substitutions rules
        for substitution in substitutions:
            substitution_map.update(
                SimplifiedDecayParser(substitution).create_substitute_pid_map()
            )

        # debug output
        if output_level in {DEBUG, VERBOSE, ALL}:
            for key, value in substitution_map.items():
                print(f"\t '{key}' : '{value}',")
            print("}")
            print("==============================")

        return substitution_map
