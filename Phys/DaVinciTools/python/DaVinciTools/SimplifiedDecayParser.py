###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import copy
import itertools
import json
import re
from typing import Callable, Union

from PartProp.ParticleTable import ParticleTable

# Global configuration
_decay_syntaxes = [
    ("(", ")"),
    ("[", "]CC"),
]
_decay_syntaxes_arrow = [
    "-->",
    "==>",
    "-x>",
    "--x>",
    "=x>",
    "==x>",
    "->",
    "=>",
]
_decay_mark = "^"

_decay_syntaxes_name = [s + e for s, e in _decay_syntaxes]
_decay_syntaxes_start = [k for k, _ in _decay_syntaxes]
_decay_syntaxes_end = [k for _, k in _decay_syntaxes]
_decay_syntaxes_start_to_end = {s: e for s, e in _decay_syntaxes}
_decay_syntaxes_name_to_content = {
    n: (s, e)
    for n, s, e in zip(_decay_syntaxes_name, _decay_syntaxes_start, _decay_syntaxes_end)
}
_decay_keywords = (
    _decay_syntaxes_start + _decay_syntaxes_end + _decay_syntaxes_arrow + [_decay_mark]
)
_parenthesis = [
    ("(", ")"),
    ("[", "]"),
]
_parenthesis_start = [k for k, _ in _parenthesis]
_parenthesis_end = [k for _, k in _parenthesis]
_parenthesis_start_to_end = {s: e for s, e in _parenthesis}

#
# Parser design:
#     Descriptor(str) -> Tokens(list[str]) -> Elements(list) -> Nodes(dict)
#


#
# Tokens
#
def _format_parenthesis_r(data: str, offset: int = 0, start: str = "") -> tuple:
    """
    Recursive function to format parentheses in a decay descriptor.

    This function adds additional spaces to parentheses that belong to a decay
    and ignores the parentheses that belong to the particle name. The function
    iterates through each character in the `data` string and recursively formats
    nested parentheses.

    Args:
        data (str): The input decay descriptor.
        offset (int, optional): The position in the string to start processing from.
            Defaults to 0.

    Returns:
        tuple: A tuple containing the formatted decay descriptor (str) and
            the offset (int) at which the closing parenthesis for the current
            recursion level was found.
    """
    output = ""
    while offset < len(data):
        # If parenthesis starts
        if data[offset] in _parenthesis_start:
            _start = data[offset]
            _end = _parenthesis_start_to_end[_start]

            # Check if it should be expand
            expand = (
                (offset == 0)
                or (data[offset - 1] == " ")
                or (data[offset - 1] in _decay_syntaxes_start)
                or (data[offset - 1] in _decay_syntaxes_end)
                or (data[offset - 1] in _decay_syntaxes_arrow)
                or (data[offset - 1] == _decay_mark)
            )

            # Get the content of this parenthesis
            offset, content = _format_parenthesis_r(data, offset + 1, _start)

            if (offset >= len(data)) and data[-1] != _end:
                raise RuntimeError(
                    f"The descriptor '{data}' does not have balanced brackets. Please check!"
                )

            # ]CC need extra treatment
            if (
                _end == "]"
                and (offset + 2) <= len(data)
                and data[offset : offset + 2] == "CC"
            ):
                offset += 2
                _end = "]CC"

            # Add extra spaces
            if expand:
                output += f" {_start} {content} {_end} "
            else:
                output += f"{_start}{content}{_end}"
        elif data[offset] in _parenthesis_end:
            if not start:
                raise RuntimeError(
                    f"The descriptor '{data}' does not have balanced brackets. Please check!"
                )
            return offset + 1, output
        else:
            output += data[offset]
            offset += 1

    return offset + 1, output


def _format_parenthesis(data: str) -> str:
    """
    Interface function to format parentheses in a decay descriptor.

    This function serves as an interface to `_format_parenthesis_r`, providing
    a cleaner API for direct usage. It initiates the recursive formatting of
    parentheses and returns the resulting string without the additional offset
    information.

    Args:
        data (str): The input decay descriptor.

    Returns:
        str: Formatted decay descriptor.
    """
    return _format_parenthesis_r(data)[1]


def _tokenize(descriptor: str) -> list[str]:
    """
    Tokenize the decay descriptor into tokens for further processing.

    This function breaks down the input decay descriptor into smaller parts,
    or tokens, to facilitate subsequent processing steps.

    Args:
        descriptor (str): The input decay descriptor.

    Returns:
        list[str]: A list of tokens extracted from the decay descriptor.
    """
    if "]cc" in descriptor:
        raise RuntimeError(
            "Please don't use []cc, use []CC instead to indicate charge conjugation in decay descriptors."
        )
    for keyword in _decay_keywords:
        # The parenthesis can be part of particle name, we have to format it more carefully
        if keyword in ["(", ")", "[", "]CC"]:
            continue
        if keyword in _decay_syntaxes_arrow:
            continue

        # Format rest of syntaxes
        descriptor = descriptor.replace(keyword, f" {keyword} ")

    def repl(match):
        # '-->', '==>', '-x>', '--x>', '=x>', '==x>', '->', '=>',
        if match.group(0) == "-->":
            return " --> "
        elif match.group(0) == "==>":
            return " ==> "
        elif match.group(0) == "-x>":
            return " -x> "
        elif match.group(0) == "--x>":
            return " --x> "
        elif match.group(0) == "=x>":
            return " -x> "
        elif match.group(0) == "==x>":
            return " ==x> "
        elif match.group(0) == "->":
            return " -> "
        elif match.group(0) == "=>":
            return " => "

    descriptor = re.sub(r"-->|==>|-x>|--x>|=x>|==x>|->|=>", repl, descriptor)
    descriptor = re.sub(r"\]\s*CC", "]CC", descriptor)
    descriptor = _format_parenthesis(descriptor)
    return descriptor.split()


def _check_bracket_closing_r(
    tokens: list[str], offset: int = 0, start_sign: str = ""
) -> int:
    while offset < len(tokens):
        # If we found an syntax start sign
        if tokens[offset] in _decay_syntaxes_start:
            # Enter to the syntax
            offset = _check_bracket_closing_r(
                tokens=tokens, offset=offset + 1, start_sign=tokens[offset]
            )
        # If we found an sntax close sign
        elif tokens[offset] in _decay_syntaxes_end:
            if (start_sign != "") and (
                tokens[offset] == _decay_syntaxes_start_to_end[start_sign]
            ):
                return offset + 1
            else:
                dumpped = " ".join(tokens)
                raise RuntimeError(
                    f"The descriptor '{dumpped}' does not have balanced brackets. Please check!"
                )
        # Else we continue
        else:
            offset += 1

    if start_sign:
        dumpped = " ".join(tokens)
        raise RuntimeError(
            f"The descriptor '{dumpped}' does not have balanced brackets. Please check!"
        )

    return offset


# Check if all brackets are closed correctly
def _check_bracket_closing(tokens: list[str]) -> bool:
    """
    Perform a simple check to ensure all brackets are correctly closed.

    Args:
        tokens (list[str]): The input decay descriptor tokens.

    Returns:
        bool: True if all brackets are correctly closed, False otherwise.
    """
    offset = _check_bracket_closing_r(tokens)
    return offset == len(tokens)


#
# Elements
#
def _parse_tokens_into_elements_r(
    tokens: list[str], offset: int = 0, syntax_sign: str = ""
) -> tuple:
    result = []
    mark = ""
    while offset < len(tokens):
        # If it's a mark
        if tokens[offset] == _decay_mark:
            if mark:
                dumpped = " ".join(tokens)
                raise RuntimeError(
                    f"The descriptor '{dumpped}' contains invalid marks. Please check!"
                )
            mark = tokens[offset]
            offset += 1
        # If we found an syntax start sign
        elif tokens[offset] in _decay_syntaxes_start:
            start_sign = tokens[offset]
            end_sign = _decay_syntaxes_start_to_end[start_sign]
            # Enter to the syntax
            offset, element = _parse_tokens_into_elements_r(
                tokens=tokens, offset=offset + 1, syntax_sign=start_sign
            )
            # Output
            result += [[f"{mark}{start_sign}"] + element + [end_sign]]
            if mark:
                mark = ""
        # If we found an sntax close sign
        elif tokens[offset] in _decay_syntaxes_end:
            if mark:
                dumpped = " ".join(tokens)
                raise RuntimeError(
                    f"The descriptor '{dumpped}' contains mark in the list, this is not a valid syntax. Please check!"
                )
            if tokens[offset] == _decay_syntaxes_start_to_end[syntax_sign]:
                return offset + 1, result
            else:
                dumpped = " ".join(tokens)
                raise RuntimeError(
                    f"The descriptor '{dumpped}' does not have balanced brackets. Please check!"
                )
        # Else we continue
        else:
            result += [f"{mark}{tokens[offset]}"]
            if mark:
                mark = ""
            offset += 1
    return offset, result


def _parse_tokens_into_elements(tokens: list[str]) -> list:
    """
    Parse tokens into elements for constructing the decay structure.

    This function processes the list of tokens and organizes them into a
    structured format. It identifies and groups different parts of the decay
    descriptor such as particles, decays, and syntax markers.

    Args:
        tokens (list[str]): A list of tokens representing the decay descriptor.

    Returns:
        list: A structured representation of the decay elements, which may include
            nested lists to represent different levels of decay processes.
    """
    return _parse_tokens_into_elements_r(tokens)[1]


def _flatten_list_structure(list_structure: list) -> list[str]:
    """
    Flatten a nested list structure into a single list.

    This function is used to convert a nested list structure, which may represent
    various levels of a decay process, into a flat list. This is useful for
    creating a simplified representation of the decay process.

    Args:
        list_structure (list): A possibly nested list structure representing parts
                            of the decay process.

    Returns:
        list[str]: A flat list containing all elements from the nested structure.
    """
    flat_list = []
    for item in list_structure:
        if isinstance(item, list):
            flat_list.extend(_flatten_list_structure(item))
        else:
            flat_list.append(item)
    return flat_list


def _dump_elements_to_string(elements: list) -> str:
    """
    Convert a list of elements into a string representation.

    This function takes a list of elements, which could include particles,
    decay processes, and syntax markers, and converts them into a string. This
    is useful for displaying the decay process in a human-readable format.

    Args:
        elements (list): A list of elements representing parts of a decay process.

    Returns:
        str: A string representation of the list of elements.
    """
    flat_list = _flatten_list_structure(elements)
    return " ".join(flat_list)


#
# Nodes
#
def _make_node(
    type: str = "",
    name: str = "",
    mother: dict = {},
    members: list = [],
    syntax: str = "",
    arrow: str = "",
    mark: str = "",
) -> dict:
    """
    Create a node dictionary with given properties.

    Args:
        type (str, optional): The type of the node (e.g., 'Particle', 'Decay').
        name (str, optional): The name of the particle or decay process.
        mother (dict, optional): The 'mother' node in a decay process.
        members (list, optional): A list of member nodes in the decay process.
        syntax (str, optional): The syntax marker associated with the node.
        arrow (str, optional): The arrow symbol used in decay representation.
        mark (str, optional): Additional markers associated with the node.

    Returns:
        dict: A dictionary representing a node with the specified properties.
    """
    node = {}
    if type:
        node["type"] = type
    if name:
        node["name"] = name
    if syntax:
        node["syntax"] = syntax
    if arrow:
        node["arrow"] = arrow
    if mark:
        node["mark"] = mark
    if mother:
        node["mother"] = mother
    if members:
        node["members"] = members

    return node


def _parse_elements_into_node(elements: list) -> dict:
    """
    Convert a list of elements into a detailed node structure.

    This function processes a list of elements, which represent different parts
    of a decay process, and constructs a detailed node structure. This structure
    provides a comprehensive representation of the decay, including particles,
    decay, and their hierarchical relationships.

    Args:
        elements (list): A list of elements representing the decay process.

    Returns:
        dict: A node structure representing the detailed decay process.
    """

    # Useful functions
    def __parse_input(input: Union[str, list], syntax: str = "", mark: str = ""):
        if isinstance(input, str):
            if input.startswith(_decay_mark):
                mark = _decay_mark
                input = input[1:]
            parsed_result = _make_node(
                type="Particle", name=input, syntax=syntax, mark=mark
            )
        elif isinstance(input, list):
            parsed_result = _parse_elements_into_node(input)
            if mark:
                parsed_result["mark"] = mark
        else:
            dumpped = _dump_elements_to_string(elements)
            raise RuntimeError(
                f"The decay '{dumpped}' can not be parsed correctly. Please check!"
            )
        return parsed_result

    # Input type check
    if not isinstance(elements, list):
        dumpped = _dump_elements_to_string(elements)
        raise RuntimeError(
            f"The decay '{dumpped}' can not be parsed correctly. Please check!"
        )

    # Output container
    result = None

    # Get mark
    mark = ""
    if isinstance(elements[0], str) and elements[0].startswith(_decay_mark):
        mark = _decay_mark
        elements[0] = elements[0][1:]

    # Get syntax
    syntax = ""
    if (
        isinstance(elements[0], str)
        and isinstance(elements[-1], str)
        and elements[0] in _decay_syntaxes_start
        and elements[-1] in _decay_syntaxes_end
    ):
        syntax = elements[0] + elements[-1]
        elements = elements[1:-1]

    # Get arrow
    arrow = (None, "")
    for idx, element in enumerate(elements):
        if element in _decay_syntaxes_arrow:
            if arrow[1] == "":
                arrow = (idx, element)
            else:
                dumpped = _dump_elements_to_string(elements)
                raise RuntimeError(
                    f"The decay '{dumpped}' has more than one arrow. Please check!"
                )

    # Case 1: is a decay (has arrow)
    if arrow[0]:
        arrow_position, arrow_string = arrow
        mother_elements = elements[:arrow_position]
        daughters_elements = elements[arrow_position + 1 :]

        # Mother can only be single element
        if len(mother_elements) > 1:
            dumpped = _dump_elements_to_string(elements)
            raise RuntimeError(
                f"The decay '{dumpped}' has more than one mother particle. Please check!"
            )

        # Parse mother
        mother_result = __parse_input(mother_elements[0], mark=mark)

        # Parse daughters
        daughters_result = [
            __parse_input(daughter_elements) for daughter_elements in daughters_elements
        ]

        # Cross check if results exist
        if not daughters_result:
            dumpped = _dump_elements_to_string(elements)
            raise RuntimeError(f"The decay '{dumpped}' has not products. Please check!")

        # Add to results
        result = _make_node(
            type="Decay",
            mother=mother_result,
            members=daughters_result,
            arrow=arrow_string,
            syntax=syntax,
        )
    # Case 2: is a list of particles
    elif len(elements) > 1:
        list_result = [__parse_input(element, mark=mark) for element in elements]
        result = _make_node(type="List", members=list_result, syntax=syntax)
    # Case 3: is a particle
    else:
        result = __parse_input(elements[0], syntax=syntax, mark=mark)

    return result


def _dump_node_to_string(node: dict) -> str:
    """
    Convert a decay node structure into its string representation.

    Args:
        node (dict): A dictionary representing a node in the decay process.

    Returns:
        str: A string representation of the node, reflecting the decay process.
    """
    # Avoid the change of initial node
    node = copy.deepcopy(node)

    # Helper function: wrap the result with syntax
    def __dump_with_syntax(content: str, mark: str = ""):
        if "syntax" in node and node["syntax"] in _decay_syntaxes_name_to_content:
            syntax_start, syntax_end = _decay_syntaxes_name_to_content[node["syntax"]]
            return f"{mark}{syntax_start} {content} {syntax_end}"
        else:
            return f"{mark}{content}"

    # Cross check (should never happen)
    if "type" not in node:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has not type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )

    # If is an particle
    if node["type"] == "Particle":
        if "mark" in node:
            mark = node["mark"]
        else:
            mark = ""
        return __dump_with_syntax(node["name"], mark=mark)
    elif node["type"] == "Decay":
        # Cross check (should never happen)
        if not (("mother" in node) and ("members" in node) and ("arrow" in node)):
            dumpped = json.dumps(node, indent=4)
            raise RuntimeError(
                "The following node has invalid structure, this is impossible. Please contact to the expert:\n"
                + dumpped
            )

        # If mother is marked, we mark in syntax
        if "mark" in node["mother"]:
            mark = node["mother"]["mark"]
            del node["mother"]["mark"]
        else:
            mark = ""

        # Get mother str
        mother_str = _dump_node_to_string(node["mother"])
        # Get daughters strs
        daughters_strs = [_dump_node_to_string(daugher) for daugher in node["members"]]
        daughters_str = " ".join(daughters_strs)
        # Get arrow str
        arrow_str = node["arrow"]
        # Make Decay str
        decay_str = f"{mother_str} {arrow_str} {daughters_str}"

        return __dump_with_syntax(decay_str, mark)
    elif node["type"] == "List":
        members_strs = [_dump_node_to_string(member) for member in node["members"]]
        members_str = " ".join(members_strs)
        return __dump_with_syntax(members_str)
    else:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has unknown type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )


def _charge_conjugate_node(node: dict, make_cc: Callable[[str], str]) -> dict:
    """
    Apply charge conjugation to a decay node.

    Args:
        node (dict): The decay node to which charge conjugation is to be applied.
        make_cc (Callable[[str], str]): A function that takes a particle name and
                                        returns its charge-conjugated counterpart.

    Returns:
        dict: A new decay node representing the charge-conjugated decay process.
    """
    # Function body r

    # Cross check (should never happen)
    if "type" not in node:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has not type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )

    # Result container
    result = copy.deepcopy(node)

    if node["type"] == "Particle":
        result["name"] = make_cc(result["name"])
    elif node["type"] == "Decay":
        # Cross check (should never happen)
        if not (("mother" in node) and ("members" in node) and ("arrow" in node)):
            dumpped = json.dumps(node, indent=4)
            raise RuntimeError(
                "The following node has invalid structure, this is impossible. Please contact to the expert:\n"
                + dumpped
            )

        # CC the mother
        result["mother"] = _charge_conjugate_node(result["mother"], make_cc)

        # CC the daughters
        daughters_results = [
            _charge_conjugate_node(daugher, make_cc) for daugher in node["members"]
        ]
        result["members"] = daughters_results
    elif node["type"] == "List":
        members_results = [
            _charge_conjugate_node(member, make_cc) for member in node["members"]
        ]
        result["members"] = members_results
    else:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has unknown type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )

    return result


def _expand_cc_in_node(node: dict, make_cc: Callable[[str], str]) -> list[dict]:
    """
    Expand charge conjugation in a decay node, handling '[]CC' syntax.

    Args:
        node (dict): The decay node to be expanded.
        make_cc (Callable[[str], str]): A function that applies charge conjugation to a particle.

    Returns:
        list[dict]: A list of decay nodes, including both the original and charge-conjugated decays.
    """

    # Output container
    output = []

    # Deal with []CC syntax
    if "syntax" in node and node["syntax"] == "[]CC":
        original_node = copy.deepcopy(node)
        original_node["syntax"] = "()"
        cc_node = _charge_conjugate_node(original_node, make_cc)
        return _expand_cc_in_node(original_node, make_cc) + _expand_cc_in_node(
            cc_node, make_cc
        )

    # If is a particle
    elif node["type"] == "Particle":
        output.append(copy.deepcopy(node))

    # If is a decay
    elif node["type"] == "Decay":
        # Get mother result
        mother_result = _expand_cc_in_node(node["mother"], make_cc)
        # Get daughters results
        daughters_results = [
            _expand_cc_in_node(daugher, make_cc) for daugher in node["members"]
        ]
        # Combine all possible results
        all_results = [mother_result] + daughters_results
        for combination in itertools.product(*all_results):
            new_node = copy.deepcopy(node)
            new_node["mother"] = combination[0]
            new_node["members"] = list(combination[1:])
            output.append(new_node)

    # If is a list
    elif node["type"] == "List":
        members_results = [
            _expand_cc_in_node(member, make_cc) for member in node["members"]
        ]
        output = []
        for combination in itertools.product(*members_results):
            new_node = copy.deepcopy(node)
            new_node["members"] = combination
            output.append(new_node)

    # Raise a error
    else:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has unknown type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )

    return output


def _iterate_over_all_particles_and_label(
    node: dict, iterate_function: Callable[[dict], str]
) -> list[tuple[str, dict]]:
    """
    Iterate over all particles in a decay node and apply a labeling function.

    Args:
        node (dict): The decay node to iterate over.
        iterate_function (Callable[[dict], str]): A function to apply to each particle node.
                                                It should return a label or modification for the particle.

    Returns:
        list[tuple[str, dict]]: A list of tuples, each containing a label and the modified decay node.
    """
    # Output container
    output = []

    # If is particle
    if node["type"] == "Particle":
        new_node = copy.deepcopy(node)
        label = iterate_function(new_node)
        if label:
            output += [(label, new_node)]

    # If is decay
    elif node["type"] == "Decay":
        # Get member results
        new_mother_results = _iterate_over_all_particles_and_label(
            node["mother"], iterate_function
        )
        new_daughters_results = [
            _iterate_over_all_particles_and_label(daughter, iterate_function)
            for daughter in node["members"]
        ]

        # Add mother results
        for new_mother_label, new_mother_node in new_mother_results:
            new_node = copy.deepcopy(node)
            new_node["mother"] = new_mother_node
            output += [(new_mother_label, new_node)]

        # Add daughter results
        for daughter_idx, new_daughter_results in enumerate(new_daughters_results):
            for new_daughter_label, new_daughter_node in new_daughter_results:
                new_node = copy.deepcopy(node)
                new_node["members"][daughter_idx] = new_daughter_node
                output += [(new_daughter_label, new_node)]

    # If is list
    elif node["type"] == "List":
        # Get member results
        new_members_results = [
            _iterate_over_all_particles_and_label(member, iterate_function)
            for member in node["members"]
        ]
        # Add member results
        for member_idx, new_member_results in enumerate(new_members_results):
            for new_member_label, new_member_node in new_member_results:
                new_node = copy.deepcopy(node)
                new_node["members"][member_idx] = new_member_node
                output += [(new_member_label, new_node)]
    else:
        dumpped = json.dumps(node, indent=4)
        raise RuntimeError(
            "The following node has unknown type, this is impossible. Please contact to the expert:\n"
            + dumpped
        )
    return output


def _apply_to_all_nodes(node: dict, apply_function: Callable[[dict], None]) -> None:
    """
    Apply a given function to all nodes in a decay structure.

    Args:
        node (dict): The root decay node to start applying the function from.
        apply_function (Callable[[dict], None]): A function that performs an operation on a node.
                                                This function should not return anything.
    """
    if node["type"] == "Particle":
        apply_function(node)
    elif node["type"] == "Decay":
        apply_function(node)
        _apply_to_all_nodes(node["mother"], apply_function)
        for daughter in node["members"]:
            _apply_to_all_nodes(daughter, apply_function)
    elif node["type"] == "List":
        apply_function(node)
        for daughter in node["members"]:
            _apply_to_all_nodes(daughter, apply_function)


def _apply_to_all_particles(node: dict, apply_function: Callable[[dict], None]) -> None:
    """
    Apply a given function to all particle nodes in a decay structure.

    Args:
        node (dict): The root decay node to start applying the function from.
        apply_function (Callable[[dict], None]): A function that performs an operation on a particle node.
                                                This function should not return anything.
    """

    def apply_to_particle(this_node: dict):
        if this_node["type"] == "Particle":
            apply_function(this_node)

    _apply_to_all_nodes(node, apply_to_particle)


def _create_substitute_pid_map(node: dict) -> dict:
    """
    Create a map of particle substitutions from a decay node.

    Args:
        node (dict): The decay node from which to extract particle substitutions.

    Returns:
        dict: A dictionary mapping original particle names to their substitutions.
    """

    def mark_substitutions(node: dict) -> str:
        subs = re.match(r"(.+)\{\{(.+?)\}\}", node["name"])
        if subs:
            subs_from = subs.group(1)
            subs_to = subs.group(2)
            node["name"] = subs_from
            node["mark"] = "^"
            return subs_to
        else:
            return ""

    def clean_substitutions(node: dict) -> None:
        subs = re.match(r"(.+)\{\{(.+?)\}\}", node["name"])
        if subs:
            subs_from = subs.group(1)
            node["name"] = subs_from

    # Get substitutions
    substitutions = _iterate_over_all_particles_and_label(node, mark_substitutions)

    # Clean up other substitutions
    for _, subs_node in substitutions:
        _apply_to_all_particles(subs_node, clean_substitutions)

    return {
        _dump_node_to_string(subs_node): subs_label
        for subs_label, subs_node in substitutions
    }


class SimplifiedDecayParser:
    """
    `SimplifiedDecayParser` is a helper class designed to parse decay descriptors. It can be used to expand the `[]CC` expression or to create a specific map between particles and decay descriptors

    Args:
        decay_descriptor(str): The input decay descriptor.

    Example:
        >>> SimplifiedDecayParser('[B+ -> K+ K+ K-{{pi-}}]CC').expand_cc()
        [' B+ -> K+ K+ K-{{pi-}} ', ' B- -> K- K- K+{{pi+}} ']
    """

    def __init__(self, decay_descriptor):
        # Initialize particle table
        self.m_particle_table = ParticleTable()

        # Save the decay descriptor
        self.m_decay_descriptor = decay_descriptor

        # First we tokenize it
        self.m_tokens = _tokenize(self.m_decay_descriptor)

        if not _check_bracket_closing(self.m_tokens):
            dumpped = " ".join(self.m_tokens)
            raise RuntimeError(
                f"The descriptor '{dumpped}' does not have balanced brackets. Please check!"
            )

        # Parse tokens into elements
        self.m_elements = _parse_tokens_into_elements(self.m_tokens)

        # Parse elements into nodes
        self.m_node = _parse_elements_into_node(self.m_elements)

    def make_cc(self, particle: str) -> str:
        """
        Apply charge conjugation to a given particle name.

        Args:
            particle (str): The name of the particle to which charge conjugation is to be applied.

        Raises:
            ValueError: If the given particle is not valid or does not have a charge-conjugated counterpart.

        Returns:
            str: The charge-conjugated name of the particle.
        """
        result = ""

        # If it's marked
        if particle.startswith("^"):
            result = "^"
            particle_content = particle[1:]
        else:
            particle_content = particle

        # If it's substitution, we have to cc both particles (from and to)
        subs = re.match(r"(.+)\{\{(.+?)\}\}", particle_content)
        if subs:
            subs_from = subs.group(1)
            subs_to = subs.group(2)
            subs_from_result = self.m_particle_table.cc(subs_from)
            subs_to_result = self.m_particle_table.cc(subs_to)
            if not subs_from_result:
                raise ValueError(f"The particle '{subs_from}' is not a valid particle.")
            if not subs_to_result:
                raise ValueError(f"The particle '{subs_to}' is not a valid particle.")
            result += f"{subs_from_result}{{{{{subs_to_result}}}}}"
        else:
            cc_particle = self.m_particle_table.cc(particle_content)
            if not cc_particle:
                raise ValueError(
                    f"The particle '{particle_content}' is not a valid particle."
                )
            result += cc_particle

        return result

    def expand_cc(self) -> list[str]:
        """
        Expand the charge-conjugate expression (`[]CC`)

        Returns:
            list[str]: The list of expanded expressions

        Example:
            >>> SimplifiedDecayParser('[B+ -> K+ K+ K-{{pi-}}]CC').expand_cc()
            [' B+ -> K+ K+ K-{{pi-}} ', ' B- -> K- K- K+{{pi+}} ']
        """

        node = copy.deepcopy(self.m_node)

        expanded_nodes = _expand_cc_in_node(node, self.make_cc)

        results = [_dump_node_to_string(copy.deepcopy(n)) for n in expanded_nodes]

        return [r.replace("  ", " ") for r in results]

    def list_cc_in_self_conjugate(self) -> list[str]:
        """
        Identifies particles that are self-conjugated and marked with `[]CC` in a decay.

        Returns:
            list[str]: A list of particle names that are marked with `[]CC` and are self-conjugated.
        """

        def is_self_conjugate(node: dict) -> bool:
            if node["type"] == "Particle":
                return node["name"] == self.make_cc(node["name"])
            elif node["type"] == "Decay":
                return is_self_conjugate(node["mother"])

        output_cc_in_self_conjugate = []

        def check_cc_in_self_conjugate(node: dict) -> None:
            if (
                "syntax" in node
                and node["syntax"] == "[]CC"
                and is_self_conjugate(node)
            ):
                if node["type"] == "Particle":
                    output_cc_in_self_conjugate.append(node["name"])
                elif node["type"] == "Decay":
                    output_cc_in_self_conjugate.append(node["mother"]["name"])

        node = copy.deepcopy(self.m_node)

        _apply_to_all_nodes(node, check_cc_in_self_conjugate)

        return output_cc_in_self_conjugate

    def mapping(
        self,
        mapping_function: Callable[[dict], str],
        clean_function: Callable[[dict], None],
    ) -> dict:
        """
        Create a map to decay nodes based on a custom mapping function.

        This method applies a custom mapping function to all particles in the decay
        descriptor, creating a mapping of labels to corresponding decay nodes.

        Args:
            mapping_function (Callable[[dict], str]): A function that labels or tags a particle node.
            clean_function (Callable[[dict], None]): A function that cleans or resets the state of a particle node.

        Returns:
            dict: A dictionary where keys are labels or tags, and values are the corresponding decay nodes.
        """

        node = copy.deepcopy(self.m_node)

        # Get mapping results
        mapping_results = _iterate_over_all_particles_and_label(node, mapping_function)

        # Clean rest of nodes
        for _, mapping_node in mapping_results:
            _apply_to_all_particles(mapping_node, clean_function)

        # Create the map

        return {
            _dump_node_to_string(mapping_node): mapping_label
            for mapping_label, mapping_node in mapping_results
        }

    def create_substitute_pid_map(self) -> dict:
        """
        Generate a mapping of particle substitutions specified in the decay descriptor.

        This method examines the decay descriptor for any specified particle substitutions
        (using a special syntax) and creates a map representing these substitutions. It
        is useful for tracking particle replacements in the decay process.

        Returns:
            dict: A map of original particle names to their respective substitutions.
        """

        def mark_substitutions(node: dict) -> str:
            subs = re.match(r"(.+)\{\{(.+?)\}\}", node["name"])
            if subs:
                subs_from = subs.group(1)
                subs_to = subs.group(2)
                node["name"] = subs_from
                node["mark"] = "^"
                return subs_to
            else:
                return ""

        def clean_substitutions(node: dict) -> None:
            subs = re.match(r"(.+)\{\{(.+?)\}\}", node["name"])
            if subs:
                subs_from = subs.group(1)
                node["name"] = subs_from

        return self.mapping(mark_substitutions, clean_substitutions)

    def dump(self) -> str:
        """
        Dump the parsed decay descriptor to str

        Returns:
            str: Dumped decay descriptor
        """
        return _dump_node_to_string(copy.deepcopy(self.m_node))
