/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RecVertex.h"
#include "LHCbAlgs/FilterPredicate.h"

namespace LHCb {

  /**
   *  Algorithm that selects events with a given range of
   *  Reconstructed PVs. In particular useful for
   *  requiring at least 1 PV.
   *  To be used in any sequencer
   *
   *  @code
   *  c = CheckPV("OnePV")
   *  c.MinPVs = 1
   *  # c.MaxPVs = 3 # uncomment for up to 3 PVs
   *  @endcode
   *
   *  @author Patrick KOPPENBURG
   *  @date   2004-11-18
   */
  class CheckPV : public Algorithm::FilterPredicate<bool( RecVertices const& )> {

  public:
    CheckPV( const std::string& name, ISvcLocator* pSvcLocator )
        : FilterPredicate( name, pSvcLocator, { "PVLocation", RecVertexLocation::Primary } ) {}
    StatusCode initialize() override;
    bool       operator()( RecVertices const& PV ) const override;

  private:
    Gaudi::Property<int>  m_minPV{ this, "MinPVs", 1, "Minimum nuber of PVs required" };
    Gaudi::Property<int>  m_maxPV{ this, "MaxPVs", -1, "Maximum nuber of PVs required. -1 : no cut." };
    Gaudi::Property<bool> m_print{ this, "Print", false, "Print number of PVs" };

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_PVsCounter{ this, "PVs" };
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_eventsCounter{ this, "Events" };
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( CheckPV, "CheckPV" )

} // namespace LHCb

StatusCode LHCb::CheckPV::initialize() {
  return FilterPredicate::initialize().andThen( [&] {
    if ( msgLevel( MSG::DEBUG ) ) {
      if ( m_minPV > 0 && m_maxPV > 0 ) {
        debug() << "will select events with between " << m_minPV << " and " << m_maxPV << " reconstructed PVs"
                << endmsg;
      } else if ( m_minPV > 0 ) {
        debug() << "will select events with " << m_minPV << " or more reconstructed PVs" << endmsg;
      } else if ( m_minPV == 0 ) {
        debug() << "will select events with no reconstructed PVs" << endmsg;
      }
    }
  } );
}

bool LHCb::CheckPV::operator()( RecVertices const& PV ) const {
  int  n  = PV.size();
  bool ok = false;
  if ( m_print ) { always() << "There are " << n << " primary vertices." << endmsg; }
  m_PVsCounter += n;
  ok = ( n >= m_minPV );             // more than m_minPV
  if ( m_maxPV >= 0 ) {              // some maximum?
    ok = ( ok && ( n <= m_maxPV ) ); // less than m_maxPV
  }
  m_eventsCounter += ok;
  if ( msgLevel( MSG::DEBUG ) ) {
    if ( ok )
      debug() << "Event accepted because there are " << n << " primary vertices." << endmsg;
    else
      debug() << "Event rejected because there are " << n << " primary vertices." << endmsg;
  }
  return ok;
}
