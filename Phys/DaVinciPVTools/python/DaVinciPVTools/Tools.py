###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Shortcuts for long names of templated tools
@author Juan Palacios juan.palacios@nikhef.nl
@date
"""

P2PVWithIP = "GenericParticle2PVRelator__p2PVWithIP_OfflineDistanceCalculatorName_"
P2PVWithIPChi2 = (
    "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_"
)
OnlineP2PVWithIP = "GenericParticle2PVRelator__p2PVWithIP_OnlineDistanceCalculatorName_"
OnlineP2PVWithIPChi2 = (
    "GenericParticle2PVRelator__p2PVWithIPChi2_OnlineDistanceCalculatorName_"
)
