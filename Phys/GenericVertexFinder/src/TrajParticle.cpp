/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrajParticle.h"

#include "Core/FloatComparison.h"

namespace {
  double sqr( double x ) { return x * x; }

  double densityFromState( const LHCb::State& state ) {
    // we'll use some sort of density along the z-axis to sort the
    // tracks. the motivation for the following formula is: assume
    // the zcoordinate can be measured both with the intersection in
    // the xz and in the yz plane. now take a weighted average. (we
    // should think a bit more about this, but not today.)
    return sqr( state.tx() ) / state.covariance()( 0, 0 ) + sqr( state.ty() ) / state.covariance()( 1, 1 );
  }

  size_t numVeloDaughters( const LHCb::Particle& particle ) {
    // count the number of daughters with velo information
    size_t rc( 0 );
    if ( particle.proto() && particle.proto()->track() ) {
      rc = particle.proto()->track()->hasVelo() ? 1 : 0;
    } else {
      for ( const auto& dau : particle.daughters() ) rc += numVeloDaughters( *dau );
    }
    return rc;
  }

  enum TypeForSorting { Long, Upstream, Velo, CompositeWithVelo, CompositeOther, Downstream, Other };

  constexpr auto typeForSorting( LHCb::Track::Types i ) {
    switch ( i ) {
    case LHCb::Track::Types::Velo:
      return TypeForSorting::Velo;
    case LHCb::Track::Types::Long:
      return TypeForSorting::Long;
    case LHCb::Track::Types::Upstream:
      return TypeForSorting::Upstream;
    case LHCb::Track::Types::Downstream:
      return TypeForSorting::Downstream;
    default:
      return TypeForSorting::Other;
    }
    return TypeForSorting::Other;
  }

  template <std::size_t... Is>
  constexpr auto init_array_helper( std::index_sequence<Is...> ) {
    constexpr int N = sizeof...( Is );
    return std::array<TypeForSorting, N>{ typeForSorting( static_cast<LHCb::Track::Types>( Is ) )... };
  }

  constexpr auto typeForSortingMap() {
    constexpr int N = static_cast<int>( LHCb::Track::Types::Last );
    return init_array_helper( std::make_index_sequence<N>{} );
  }

  int typeForSortingMapped( LHCb::Track::Types i ) {
    // const int typemap[] = { TypeForSorting::Other, TypeForSorting::Velo, TypeForSorting::Velo, TypeForSorting::Long,
    // TypeForSorting::Upstream, TypeForSorting::Downstream, TypeForSorting::Other, TypeForSorting::Other,
    // TypeForSorting::Other,TypeForSorting::Other,TypeForSorting::Other} ;
    constexpr static auto typemap = typeForSortingMap();
    return typemap[int( i )];
  }
} // namespace

namespace LHCb {

  TrajParticle::TrajParticle( const LHCb::Track& track, const ZTrajectory& traj, OwnsTraj ownstraj )
      : m_particle{ nullptr }
      , m_track{ &track }
      , m_traj{ &traj, bool( ownstraj ) }
      , m_pT{ track.firstState().pt() }
      , m_hasVeloHits{ m_track->hasVelo() }
      , m_typeForSorting{ typeForSortingMapped( track.type() ) } {
    {
      const LHCb::State* stfirstm = m_track->stateAt( LHCb::State::Location::FirstMeasurement );
      m_beginZ                    = stfirstm ? stfirstm->z() : 9999;
    }
    {
      const LHCb::State* state = track.stateAt( LHCb::State::Location::ClosestToBeam );
      m_density                = densityFromState( state ? *state : track.firstState() );
    }
  }

  TrajParticle::TrajParticle( const LHCb::Particle& p, const LHCb::Track& track, const ZTrajectory& traj,
                              OwnsTraj ownstraj )
      : TrajParticle( track, traj, ownstraj ) {
    m_particle = &p;
  }

  TrajParticle::TrajParticle( const LHCb::Particle& p, const ZTrajectory& traj, OwnsTraj ownstraj )
      : m_particle{ &p }
      , m_traj{ &traj, bool( ownstraj ) }
      , m_beginZ{ m_particle->referencePoint().z() }
      , m_beginZCov{ m_particle->posCovMatrix()( 2, 2 ) }
      , m_pT{ p.momentum().Pt() } {
    m_density        = densityFromState( state( m_beginZ ) );
    auto numvelo     = numVeloDaughters( p );
    m_typeForSorting = numvelo >= 2 ? TypeForSorting::CompositeWithVelo : TypeForSorting::CompositeOther;
  }

  double TrajParticle::zBeam() const {
    const LHCb::Track* trk = track();
    double             zbeam( 0 );
    const LHCb::State* beamstate( 0 );
    if ( trk && ( beamstate = trk->stateAt( LHCb::State::Location::ClosestToBeam ) ) ) {
      zbeam = beamstate->z();
    } else {
      LHCb::State               state = trajectory().state( 0 );
      const Gaudi::TrackVector& vec   = state.stateVector();
      double                    z     = state.z();
      // check on division by zero (track parallel to beam line!)
      if ( !essentiallyZero( vec[2] ) || !essentiallyZero( vec[3] ) ) {
        z -= ( vec[0] * vec[2] + vec[1] * vec[3] ) / ( vec[2] * vec[2] + vec[3] * vec[3] );
      }
      // don't go outside the sensible volume
      zbeam = std::min( std::max( z, -100 * Gaudi::Units::cm ), 200 * Gaudi::Units::cm );
    }
    return zbeam;
  }
} // namespace LHCb
