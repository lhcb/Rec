/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrajParticleVertex.h"
namespace LHCb {

  size_t TrajParticleVertex::numTracks( LHCb::Track::Types type ) const {
    return std::count_if( daughters.begin(), daughters.end(),
                          [type]( const TrajParticle* dau ) { return dau->track() && dau->track()->type() == type; } );
  }

  double TrajParticleVertex::mass() const {
    std::vector<double> masshypos( vertex->nTracks(), 0 );
    return vertex->mass( masshypos );
  }
} // namespace LHCb
