/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ParticleTrajProvider.h"
#include "Event/LinearStateZTraj.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackKernel/TrackTraj.h"

namespace {

  LHCb::State compositeParticleToState( const LHCb::Particle& particle ) {
    // This replaces the function "Particle2State::particle2State"
    // which is just wrong if the result is to be used in a vertex
    // fit. See also ParticleVertexFitter.

    LHCb::State  state;
    const auto&  p4 = particle.momentum();
    const auto&  px = p4.Px();
    const auto&  py = p4.Py();
    const auto&  pz = p4.Pz();
    const double tx = px / pz;
    const double ty = py / pz;

    double p = p4.P();
    state.setTx( tx );
    state.setTy( ty );
    const int Q = particle.particleID().pid() > 0 ? 1 : -1;
    state.setQOverP( Q / p );
    const Gaudi::XYZPoint& pos = particle.endVertex()->position();
    state.setX( pos.x() );
    state.setY( pos.y() );
    state.setZ( pos.z() );

    // For the computation of the Jacobian it is important to understand the following.
    //  x' = x + (z' - z) * tx
    // --> dx/dz = - tx ;
    //
    // Notation:
    //      x = (x,y)
    //      tx = (tx,ty,q/p)
    //      J_{5,6} = ( Jxpos      Jxmom )
    //                ( Jtxpos     Jtxmom )
    ROOT::Math::SMatrix<double, 2, 3> Jxpos;
    Jxpos( 0, 0 ) = Jxpos( 1, 1 ) = 1;
    Jxpos( 0, 2 )                 = -tx;
    Jxpos( 1, 2 )                 = -ty;
    ROOT::Math::SMatrix<double, 3, 3> Jtxmom;
    Jtxmom( 0, 0 )      = 1 / pz;   // dtx/dpx
    Jtxmom( 1, 1 )      = 1 / pz;   // dty/dpy
    Jtxmom( 0, 2 )      = -tx / pz; // dtx/dpz
    Jtxmom( 1, 2 )      = -ty / pz; // dty/dpz
    const double dqopdp = -Q / ( p * p );
    Jtxmom( 2, 0 )      = dqopdp * px / p; // dqop/dpx
    Jtxmom( 2, 1 )      = dqopdp * py / p; // dqop/dpy
    Jtxmom( 2, 2 )      = dqopdp * pz / p; // dqop/dpz

    // perhaps it would be more efficient to fill one jacobian and one
    // covariance and then use similatiry once, but I don't think
    // so...
    const auto&            momposcov = particle.posMomCovMatrix(); // it has the wrong name: it is mompos not posmom
    const Gaudi::Matrix2x3 xtxcov  = Jxpos * Transpose( momposcov.Sub<Gaudi::Matrix3x3>( 0, 0 ) ) * Transpose( Jtxmom );
    const Gaudi::SymMatrix2x2 xcov = Similarity( Jxpos, particle.posCovMatrix() );
    const Gaudi::SymMatrix3x3 txcov = Similarity( Jtxmom, particle.momCovMatrix().Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
    auto&                     cov   = state.covariance();
    for ( int i = 0; i < 2; ++i )
      for ( int j = 0; j <= i; ++j ) cov( i, j ) = xcov( i, j );
    for ( int i = 0; i < 2; ++i )
      for ( int j = 0; j < 3; ++j ) cov( i, j + 2 ) = xtxcov( i, j );
    for ( int i = 0; i < 3; ++i )
      for ( int j = 0; j <= i; ++j ) cov( i + 2, j + 2 ) = txcov( i, j );
    return state;
  }
} // namespace

std::unique_ptr<LHCb::TrajParticle> ParticleTrajProvider::trajectory( ITrackStateProvider const& stateprovider,
                                                                      LHCb::Track const&         track,
                                                                      IGeometryInfo const&       geometry ) {
  const auto* traj = stateprovider.trajectory( track, geometry );
  return traj ? std::make_unique<LHCb::TrajParticle>( track, *traj, LHCb::TrajParticle::OwnsTraj{ false } )
              : std::unique_ptr<LHCb::TrajParticle>{};
}

std::unique_ptr<LHCb::TrajParticle> ParticleTrajProvider::trajectory( ITrackStateProvider const& stateprovider,
                                                                      LHCb::Particle const&      particle,
                                                                      IGeometryInfo const&       geometry ) {
  std::unique_ptr<LHCb::TrajParticle> rc;
  if ( particle.proto() && particle.proto()->track() ) {
    const LHCb::Track* track = particle.proto()->track();
    rc                       = trajectory( stateprovider, *track, geometry );
    if ( rc ) rc->setParticle( particle );
  } else if ( particle.endVertex() && particle.endVertex()->nDoF() > 0 &&
              particle.endVertex()->covMatrix()( 0, 0 ) > 0 && particle.endVertex()->covMatrix()( 0, 0 ) < 1e6 ) {

    // create a state from the particle
    LHCb::State state = compositeParticleToState( particle );

    // Create a trajectory that's just a straight line
    const auto* traj = new LHCb::LinearStateZTraj{ state };
    rc               = std::make_unique<LHCb::TrajParticle>( particle, *traj, LHCb::TrajParticle::OwnsTraj{ true } );
  }
  return rc;
}
