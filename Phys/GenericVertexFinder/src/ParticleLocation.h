/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/StatusCode.h"

#include <boost/algorithm/string/erase.hpp>

#include <iomanip>
#include <iostream>
#include <string>

namespace Kernel {
  class ParticleLocation {
  private:
    mutable std::string m_value;
    bool                m_configured{ false };

  public:
    ParticleLocation( std::string value = {} ) : m_value{ std::move( value ) } {}
    bool               empty() const { return m_value.empty(); }
    std::string&       value() { return m_value; }
    const std::string& value() const { return m_value; }
    std::string        name() const {
      if ( !m_configured ) configure();
      return m_value;
    }
    std::string particles() const { return name() + "/Particles"; }
    std::string vertices() const { return name() + "/decayVertices"; }

  private:
    void configure() const { boost::algorithm::erase_last( m_value, "/Particles" ); }
  };

  // provide support for Gaudi::Property<ParticleLocation>
  inline std::string   toString( const ParticleLocation& loc ) { return loc.value(); }
  inline std::ostream& toStream( const ParticleLocation& loc, std::ostream& os ) {
    return os << std::quoted( toString( loc ), '\'' );
  }
  inline StatusCode parse( ParticleLocation& loc, const std::string& input ) {
    loc.value() = input;
    return StatusCode::SUCCESS;
  }
  // and allow printout..
  inline std::ostream& operator<<( std::ostream& os, const ParticleLocation& s ) { return toStream( s, os ); }

} // namespace Kernel
