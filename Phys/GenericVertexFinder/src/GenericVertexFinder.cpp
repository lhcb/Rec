/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "GenericVertexFinder.h"
#include "Event/TrackFunctor.h"
#include "ParticleTrajProvider.h"

//-----------------------------------------------------------------------------
// Implementation file for class GenericVertexFinder
//
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenericVertexFinder )

/// Various utilities in anonymous namespace
namespace {

  template <class T>
  void deleteAll( T& t ) {
    std::for_each( t.begin(), t.end(), TrackFunctor::deleteObject() );
  }

  const auto SeedPtSorter = []( const LHCb::TrajParticle* lhs, const LHCb::TrajParticle* rhs ) {
    return lhs->typeForSorting() < rhs->typeForSorting() ||
           ( lhs->typeForSorting() == rhs->typeForSorting() && lhs->pT() > rhs->pT() );
  };

  const auto SeedDensitySorter = []( const LHCb::TrajParticle* lhs, const LHCb::TrajParticle* rhs ) {
    return lhs->typeForSorting() < rhs->typeForSorting() ||
           ( lhs->typeForSorting() == rhs->typeForSorting() && lhs->density() > rhs->density() );
  };

  const auto SeedBeginZSorter = []( const LHCb::TrajParticle* lhs, const LHCb::TrajParticle* rhs ) {
    return lhs->typeForSorting() < rhs->typeForSorting() ||
           ( lhs->typeForSorting() == rhs->typeForSorting() && lhs->beginZ() > rhs->beginZ() );
  };
} // namespace

GenericVertexFinder::TrajParticles GenericVertexFinder::sortInputTracks( const OwnedTrajParticles& tracks ) const {

  // copy the input list. flag all tracks as unused.
  GenericVertexFinder::TrajParticles seedtracks{ tracks.size() };
  std::transform( tracks.begin(), tracks.end(), seedtracks.begin(),
                  []( const OwnedTrajParticles::value_type& tr ) { return tr.get(); } );

  // sort the seed tracks
  switch ( m_sortingStrategy ) {
  case SortingStrategy::Pt:
    std::sort( seedtracks.begin(), seedtracks.end(), SeedPtSorter );
    break;
  case SortingStrategy::Density:
    std::sort( seedtracks.begin(), seedtracks.end(), SeedDensitySorter );
    break;
  default:
    std::sort( seedtracks.begin(), seedtracks.end(), SeedBeginZSorter );
  }

  // we may need to reset the mother flags
  for ( auto& tr : seedtracks ) tr->setMother( nullptr );
  return seedtracks;
}

GenericVertexFinder::TrajVertices GenericVertexFinder::findVertices( const OwnedTrajParticles& tracks ) const {
  // clean the output list
  GenericVertexFinder::TrajVertices vertices;

  // copy the input list. flag all tracks as unused.
  std::vector<const LHCb::TrajParticle*> seedtracks = sortInputTracks( tracks );

  size_t N = seedtracks.size();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of input trajectories: " << tracks.size() << endmsg;

  // make combinations of 'unused' tracks. note that in the loop over
  // 'j' the track in loop 'i' can be flagged as used.
  for ( size_t i = 0; i < N; ++i )
    for ( size_t j = i + 1; j < N && !seedtracks[i]->used(); ++j )
      if ( !seedtracks[j]->used() && seedtracks[i]->nCommonLhcbIDs( *seedtracks[j] ) <= m_maxNumCommonHits ) {

        auto recvertex = createSeed( *( seedtracks[i] ), *( seedtracks[j] ) );
        if ( recvertex ) {

          // flag added tracks as used
          recvertex->setAsMother();
          auto         seedvertex    = recvertex->vertex.get();
          const double zseed         = seedvertex->position().z();
          const double ztolerance    = std::sqrt( 100. + 25 * seedvertex->covMatrix()( 2, 2 ) );
          const double zfirsthitveto = zseed - ztolerance;

          // Here we do the following:
          // - collect all tracks that fit to the seed
          // - order them in how well they fit
          // - now add them one by one, checking every time that
          //   the new track still fits
          // This is a bit expensive, but we can speedup later.
          typedef std::pair<size_t, double> TrackWithDeltaChi2;
          std::vector<TrackWithDeltaChi2>   trackswithdchi2;
          for ( size_t k = 0; k < N; ++k )
            if ( !seedtracks[k]->used() && ( !m_rejectUpstreamHits || ( zfirsthitveto < seedtracks[k]->beginZ() ) ) ) {
              // this call will also take care that the track state is cached at the right place
              double dchi2 = seedtracks[k]->chi2( *( seedvertex ) );
              if ( dchi2 < 2 * m_maxDeltaVertexChi2 ) {
                // fudge factor '2', to take into account it can
                // actually go down once we have added other
                // tracks
                trackswithdchi2.emplace_back( k, dchi2 );
              }
            }

          // sort them
          std::sort( trackswithdchi2.begin(), trackswithdchi2.end(),
                     []( const std::pair<size_t, double>& lhs, const std::pair<size_t, double>& rhs ) {
                       return lhs.second < rhs.second;
                     } );

          // Now add the tracks. T omake this fast, we don't actually
          // refit the vertex with all tracks every time, but only
          // keep the 'summary', namely position, covariance and total
          // chi2. Then afterall tracks have been identified, we once
          // more fit the vertex with all tracks.
          // and add them
          Gaudi::XYZPoint     position     = seedvertex->position();
          Gaudi::SymMatrix3x3 weightmatrix = seedvertex->weightMatrix();
          Gaudi::SymMatrix3x3 covmatrix; // = seedvertex->covMatrix();
          for ( auto& ktrk : trackswithdchi2 ) {
            const LHCb::TrajParticle* track           = seedtracks[ktrk.first];
            const LHCb::State&        state           = track->state();
            auto                      tmpposition     = position;
            auto                      tmpweightmatrix = weightmatrix;
            double dchi2 = LHCb::TrackVertexUtils::addToVertex( state, tmpposition, tmpweightmatrix, covmatrix );
            if ( dchi2 < m_maxDeltaVertexChi2 ) {
              position     = tmpposition;
              weightmatrix = tmpweightmatrix;
              // store the track with the vertex
              recvertex->daughters.push_back( track );
              track->setMother( recvertex.get() );
            }
          }
          // did we actually add anything? if so, refit the vertex
          // with all tracks. This doesn't actually change vertex
          // position or chi2, but we need it to get the correlations
          // with the momenta.
          if ( recvertex->daughters.size() > 2 ) {
            for ( unsigned int k = 2; k < recvertex->daughters.size(); ++k )
              seedvertex->addTrack( recvertex->daughters[k]->state() );
            // what would we do if it fails? we don't have any fallback anymore ...
            seedvertex->fit();
          }

          // add to output list
          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "Created vertex with " << recvertex->daughters.size() << " tracks." << endmsg;
          vertices.push_back( std::move( recvertex ) );
        }
      }

  // merge vertices that are close
  mergeVertices( vertices );

  // Should we add left-over tracks to vertices, using a looser criterion
  if ( msgLevel( MSG::DEBUG ) ) {
    auto Nused = std::count_if( seedtracks.begin(), seedtracks.end(), []( const auto* t ) { return t->used(); } );
    debug() << "Number of used tracks: " << Nused << " out of " << tracks.size() << endmsg;
    debug() << "Number of vertices: " << vertices.size() << endmsg;
  }
  return vertices;
}

//=============================================================================
// Merge vertices that are close
//=============================================================================

void GenericVertexFinder::mergeVertices( TrajVertices& vertices ) const {
  // Merge mutually consistent vertices
  if ( m_minVertexVertexChi2 > 0 ) {
    size_t Ntp = vertices.size();
    for ( size_t jtp = 0; jtp < Ntp; ++jtp ) {
      for ( size_t itp = 0; itp < jtp; ++itp ) {
        auto& ivertex = vertices[itp];
        auto& jvertex = vertices[jtp];
        if ( ivertex && jvertex ) {
          Gaudi::SymMatrix3x3 invcov = ivertex->vertex->covMatrix() + jvertex->vertex->covMatrix();
          invcov.InvertChol();
          Gaudi::Vector3 res{ ivertex->vertex->position().x() - jvertex->vertex->position().x(),
                              ivertex->vertex->position().y() - jvertex->vertex->position().y(),
                              ivertex->vertex->position().z() - jvertex->vertex->position().z() };
          double         dchi2 = ROOT::Math::Similarity( res, invcov );
          if ( dchi2 < m_minVertexVertexChi2 ) {
            double ichi2 = ivertex->vertex->chi2();
            double jchi2 = jvertex->vertex->chi2();
            double z     = ivertex->vertex->position().z();
            // create a common vertex and fit it
            auto commonvertex = std::make_unique<LHCb::TrackStateVertex>( *ivertex->vertex );
            for ( const auto& dau : jvertex->daughters ) commonvertex->addTrack( dau->state( z ) );
            LHCb::TrackStateVertex::FitStatus fitstatus = commonvertex->fit();
            if ( fitstatus == LHCb::TrackStateVertex::FitSuccess &&
                 commonvertex->chi2() - ichi2 - jchi2 < m_minVertexVertexChi2 ) {

              if ( msgLevel( MSG::DEBUG ) ) {
                debug() << "Merged two vertices: " << dchi2 << " " << commonvertex->chi2() - ichi2 - jchi2 << endmsg;
              }
              for ( size_t jdau = 0; jdau < jvertex->daughters.size(); ++jdau ) {
                ivertex->daughters.push_back( jvertex->daughters[jdau] );
                jvertex->daughters[jdau]->setMother( ivertex.get() );
              }
              ivertex->vertex = std::move( commonvertex );
              jvertex.reset();
            }
          }
        }
      }
    }
    TrajVertices::iterator newend = std::remove_if(
        vertices.begin(), vertices.end(), []( const std::unique_ptr<LHCb::TrajParticleVertex>& v ) { return !v; } );
    vertices.erase( newend, vertices.end() );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "::mergedVertices removed " << Ntp - vertices.size() << " out of " << Ntp << " vertices." << endmsg;
    }
  }
}

//=============================================================================
// Create a seed vertex from two seed tracks
//=============================================================================

std::unique_ptr<LHCb::TrajParticleVertex> GenericVertexFinder::createSeed( const LHCb::TrajParticle& track1,
                                                                           const LHCb::TrajParticle& track2 ) const {
  std::unique_ptr<LHCb::TrajParticleVertex> seed;
  if ( track1.nCommonLhcbIDs( track2 ) <= m_maxNumCommonHits ) {

    // get the trajectories
    const auto& trajA = track1.trajectory();
    const auto& trajB = track2.trajectory();

    // compute the poca
    double           mupos( 0 ), muneg( 0 );
    Gaudi::XYZVector deltaX;
    StatusCode       sc = pocatool().minimize( trajA, mupos, trajB, muneg, deltaX, 0.001 * Gaudi::Units::mm );
    if ( msgLevel( MSG::DEBUG ) )
      verbose() << "Found two tracks. poca says " << sc << " " << deltaX.R() << " " << 0.5 * ( mupos + muneg )
                << endmsg;

    if ( sc.isSuccess() ) {
      // make some cuts
      double           z        = 0.5 * ( mupos + muneg );
      double           distance = deltaX.R();
      Gaudi::XYZVector dirA     = trajA.direction( mupos );
      Gaudi::XYZVector dirB     = trajB.direction( mupos );
      double           costheta = dirA.Dot( dirB ) / std::sqrt( dirA.Mag2() * dirB.Mag2() );

      bool isVeloCombi = track1.hasVeloHits() && track2.hasVeloHits();
      if ( ( distance < m_distanceCutLong || ( distance < m_distanceCutDownstream && !isVeloCombi ) ) && m_zmin < z &&
           z < m_zmax && std::abs( costheta ) < cos( m_minSeedOpeningAngle ) ) {
        // get two states, compute the chi2 with our 'quick' method
        const LHCb::State& stateA = track1.state( z );
        const LHCb::State& stateB = track2.state( z );
        double             chi2   = LHCb::TrackVertexUtils::vertexChi2( stateA, stateB );
        if ( chi2 < m_maxSeedVertexChi2 ) {

          // OK, vertex candidate.
          auto seedvertex = std::make_unique<LHCb::TrackStateVertex>();
          seedvertex->addTrack( trajA.state( z ) );
          seedvertex->addTrack( trajB.state( z ) );
          LHCb::TrackStateVertex::FitStatus fitstatus = seedvertex->fit();
          double                            zseed     = seedvertex->position().z();

          if ( msgLevel( MSG::DEBUG ) )
            verbose() << "Accepted combi, now running vertex fit: " << fitstatus << " " << seedvertex->chi2() << " "
                      << zseed << " " << track1.beginZ() << " " << track2.beginZ() << endmsg;

          double ztolerance    = std::sqrt( 100. + 25 * seedvertex->covMatrix()( 2, 2 ) );
          double zfirsthitveto = zseed - ztolerance;

          if ( fitstatus == LHCb::TrackStateVertex::FitSuccess && seedvertex->chi2() < m_maxSeedVertexChi2 &&
               m_zmin < zseed && zseed < m_zmax &&
               ( !m_rejectUpstreamHits || ( zfirsthitveto < track1.beginZ() && zfirsthitveto < track2.beginZ() ) ) ) {
            if ( msgLevel( MSG::DEBUG ) ) verbose() << "Accepted seed: " << seedvertex->position() << endmsg;

            // OK we are in business
            seed = std::make_unique<LHCb::TrajParticleVertex>();
            seed->daughters.push_back( &track1 );
            seed->daughters.push_back( &track2 );
            seed->vertex = std::move( seedvertex );
          }
        } else {
          if ( msgLevel( MSG::DEBUG ) ) verbose() << "Failed vertex chi2 cut: " << chi2 << endmsg;
        }
      } else {
        if ( msgLevel( MSG::DEBUG ) )
          verbose() << "Failed preselection: " << distance << " " << z << " " << costheta << endmsg;
      }
    }
  }
  return seed;
}

//=============================================================================
// Various forms appearing in the interface
//=============================================================================
IGenericVertexFinder::ParticleContainer GenericVertexFinder::findVertices( LHCb::Particle::ConstVector const& particles,
                                                                           IGeometryInfo const& geometry ) const {
  // prepare the input
  OwnedTrajParticles trajparticles;
  trajparticles.reserve( particles.size() );

  // extract trajectories from particles
  for ( const auto& p : particles ) {
    auto traj = ParticleTrajProvider::trajectory( *m_stateprovider, *p, geometry );
    if ( traj ) trajparticles.push_back( std::move( traj ) );
  }

  // call the vertexing
  auto trajvertices = findVertices( trajparticles );

  // fill the output. this is quite tricky
  ParticleContainer compositeparticles;
  compositeparticles.reserve( trajvertices.size() );
  std::transform( trajvertices.begin(), trajvertices.end(), std::back_inserter( compositeparticles ),
                  [this]( const auto& ver ) {
                    auto                          statevertex = ver->vertex.get();
                    ParticleContainer::value_type p;
                    p.particle = std::make_unique<LHCb::Particle>( LHCb::ParticleID{ 421 } );
                    p.vertex   = std::make_unique<LHCb::Vertex>( statevertex->position() );
                    p.particle->setEndVertex( p.vertex.get() );
                    this->fillParticleFromTrajVertex( *( p.particle ), *ver );
                    return p;
                  } );
  // return the vertices
  return compositeparticles;
}

//=============================================================================
IGenericVertexFinder::RecVertexContainer GenericVertexFinder::findVertices( LHCb::Track::Range const& tracks,
                                                                            IGeometryInfo const&      geometry ) const {
  // prepare the input
  OwnedTrajParticles trajparticles;
  trajparticles.reserve( tracks.size() );

  // extract trajectories from particles
  for ( const auto& track : tracks ) {
    auto traj = ParticleTrajProvider::trajectory( *m_stateprovider, *track, geometry );
    if ( traj ) trajparticles.push_back( std::move( traj ) );
  }

  // call the vertexing
  auto trajvertices = findVertices( trajparticles );
  // fill the output (should use std::transform)
  IGenericVertexFinder::RecVertexContainer vertices;
  vertices.reserve( trajvertices.size() );
  std::transform( trajvertices.begin(), trajvertices.end(), std::back_inserter( vertices ), []( const auto& iver ) {
    const auto& statevertex = iver->vertex.get();
    auto        recvertex   = std::make_unique<LHCb::RecVertex>( statevertex->position() );
    recvertex->setChi2AndDoF( statevertex->chi2(), statevertex->nDoF() );
    recvertex->setCovMatrix( statevertex->covMatrix() );
    for ( const auto& dau : iver->daughters ) recvertex->addToTracks( dau->track() );
    return recvertex;
  } );

  // return the vertices
  return vertices;
}

//=============================================================================
void GenericVertexFinder::fillParticleFromTrajVertex( LHCb::Particle&                 particle,
                                                      const LHCb::TrajParticleVertex& vertex ) const {
  LHCb::Vertex* pvertex = const_cast<LHCb::Vertex*>( particle.endVertex() );
  if ( pvertex ) {
    pvertex->setPosition( vertex.vertex->position() );
    pvertex->setChi2AndDoF( vertex.vertex->chi2(), vertex.vertex->nDoF() );
    pvertex->setCovMatrix( vertex.vertex->covMatrix() );
    SmartRefVector<LHCb::Particle> daughters;
    const auto&                    vd = vertex.daughters;
    std::transform( vd.begin(), vd.end(), std::back_inserter( daughters ),
                    []( const auto& d ) { return d->particle(); } );
    pvertex->setOutgoingParticles( daughters );
  }

  particle.setReferencePoint( vertex.vertex->position() );
  particle.setPosCovMatrix( vertex.vertex->covMatrix() );

  // now add the daughters to particle and vertex
  std::vector<double> masshypos;
  for ( const auto& dautraj : vertex.daughters ) {
    const LHCb::Particle* daughter = dautraj->particle();
    particle.addToDaughters( daughter );
    // FIXME: to compute the total p4 we assign a mass to every
    // trajectory. This is a bit nonsense when that trajectory isn't a
    // track. Unfortunately, it is hard to get it right without
    // rewriting the vertex fit. We should consider to use the new
    // ParticleVertexFitter.
    const LHCb::Track* dautrk = dautraj->track();
    double             mass( 0 );
    if ( dautrk ) {
      // a value of -1 flags to the vertex fit not to use the mass
      if ( m_skipVeloTracksForMass && dautrk->type() == LHCb::Track::Types::Velo )
        mass = -1;
      else
        mass = daughter->measuredMass();
    } else {
      mass = daughter->momentum().M();
    }
    masshypos.push_back( mass );
  }

  // compute the mother momentum. here we need to hack something
  // such that we can remove velo-only tracks. it is probably
  // easiest to refit the vertex.
  Gaudi::SymMatrix7x7  cov7x7 = vertex.vertex->covMatrix7x7( masshypos );
  Gaudi::LorentzVector p4     = vertex.vertex->p4( masshypos );
  particle.setMomentum( p4 );
  particle.setMomCovMatrix( cov7x7.Sub<Gaudi::SymMatrix4x4>( 3, 3 ) );
  particle.setPosMomCovMatrix( cov7x7.Sub<Gaudi::Matrix4x3>( 3, 0 ) );
}
