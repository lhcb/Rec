/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GenericVertexFinder.h"

/** @class GenericVertexFinderTwo GenericVertexFinderTwo.cpp
 *
 *
 *  @author Wouter Hulsbergen
 */

class GenericVertexFinderTwo : public GenericVertexFinder {
public:
  /// Use base class constructors
  using GenericVertexFinder::GenericVertexFinder;

  /// find vertices in a set of trajectories
  using GenericVertexFinder::findVertices;
  TrajVertices findVertices( const OwnedTrajParticles& tracks ) const override;

protected:
  /// Add a track to a vertex, provided it passes chisq cuts
  bool tryAndAdd( const LHCb::TrajParticle& track, LHCb::TrajParticleVertex& vertex ) const;

protected:
  Gaudi::Property<bool> m_downweightNonLong{ this, "DownweightNonLong", true };
  Gaudi::Property<bool> m_pruneSeeds{ this, "PruneSeeds", true };
};

//-----------------------------------------------------------------------------
// Implementation file for class : GenericVertexFinderTwo
//
// 2007-10-08 : Wouter HULSBERGEN
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenericVertexFinderTwo )

//=============================================================================
// Main execution
//=============================================================================

namespace {

  struct TwoProngSeed {
    std::unique_ptr<LHCb::TrajParticleVertex> trajvertex;
    const LHCb::TrajParticle*                 trackA() const { return trajvertex->daughters[0]; }
    const LHCb::TrajParticle*                 trackB() const { return trajvertex->daughters[1]; }
    double                                    density;
    friend bool operator<( const TwoProngSeed& lhs, const TwoProngSeed& rhs ) { return lhs.density > rhs.density; }
  };
} // namespace

// README: proposal for new approach to seeding: it is very slow, but
// should hopefully lead to the best efficiency.
//
// - make all reasonable two track combinations. apply a chi2
//   cut. compute the density. this is the really slow part because it
//   goes as N^2
// - sort them by 'density'
// - begin at the start of the list. select the seed:
//    * mark both tracks as used
//    * store index to seed in list of selected seeds.
// - go to the next seed:
//   # commented, because the following is crap
//   #* if either of its tracks marked as used
//   #  --> clearly, that means that the /other/ track fits better to a
//   #   selected seed than to any future seed (because of ordering)
//   #  --> mark the track as used (but don't add it to seed yet,
//   #    because you don't want to keep track of which seed!)
//   * if neither of the tracks is marked as used, select the seed.
// - continue like this till end of list.
// - in order to clean up:
//    - first unmark all tracks
//    - then re-mark all tracks that appear in seeds

// we now have a set of seeds that do not overlap. they may be
// consistent with a single vertex. however, we'll merge them only at
// the end: it is better to start with too many seeds, than with too
// few.

// note that the number of seeds is at most half the number of
// tracks. furthermore, the more seeds there are, the less unused
// tracks. in fact, we can do full combinatorics again!:
//   - start with the first unused track
//   - try it with every seed
//   - pick the best match, add to vertex.
//   - repeat until all tracks are either used, or not consistent with
//     any vertex.
//
// if you don't want to get a bias for small vertices, you may want to
// leave a refit of the entire vertex to the end, and keep using the
// seed position.

// finally, try to merge vertices. the minimum vertex distance should
// be quite large, because we'll have some 'splitting' bias.

GenericVertexFinder::TrajVertices GenericVertexFinderTwo::findVertices( const OwnedTrajParticles& tracks ) const {
  // clean the output list
  GenericVertexFinder::TrajVertices vertices;

  // copy the input list. flag all tracks as unused.
  std::vector<const LHCb::TrajParticle*> seedtracks = sortInputTracks( tracks );

  size_t N = seedtracks.size();
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of input trajectories: " << tracks.size() << endmsg;

  // make combinations of all tracks. this is where we'll die in combinatorics :-)
  std::vector<TwoProngSeed> seedtwoprongs;
  for ( size_t i = 0; i < N; ++i )
    for ( size_t j = i + 1; j < N; ++j ) {
      auto tv = createSeed( *( seedtracks[i] ), *( seedtracks[j] ) );
      if ( tv ) {
        // now compute the weight
        TwoProngSeed twoprong;
        twoprong.trajvertex = std::move( tv );
        auto trajvertex     = twoprong.trajvertex.get();
        // now compute the 'density'

        // P(x,y,z) = exp[ - chi2 / 2 ] / (2 * sqrt( det(V) ) )
        // --> logP = - chi2 /2 - log(2) - 0.5*log( det(V) )
        // --> logP \propto - chi2 - log(det(V))
        double detV;
        /*bool OK =*/trajvertex->vertex->covMatrix().Det2( detV );
        // std::cout << trajvertex->vertex->covMatrix() << "determinant: "
        // 	  << detV << " " << determinant(trajvertex->vertex->covMatrix()) << std::endl ;
        const double norm = 1e-6; // abritrary norm
        twoprong.density  = -trajvertex->vertex->chi2() - std::log( detV / norm );
        // shall we give tracks that are not long an extra penalty?
        if ( m_downweightNonLong && trajvertex->numTracks( LHCb::Track::Types::Long ) != 2 )
          twoprong.density -= m_maxSeedVertexChi2;
        seedtwoprongs.push_back( std::move( twoprong ) );
      }
    }

  if ( msgLevel( MSG::DEBUG ) ) verbose() << "number of created twoprongs: " << seedtwoprongs.size() << endmsg;

  // now sort the twoprong by weight
  std::sort( seedtwoprongs.begin(), seedtwoprongs.end() );

  // now create a list of mutually compatible twoprongs using this order
  for ( auto& seed : seedtwoprongs ) {
    // std::cout << "Processing seed: "
    // 	      << std::setw(2) << std::distance(seedtwoprongs.begin(),iseed) << " "
    //  	      << &(*iseed) << " " << iseed->trajvertex << " "
    //  	      << iseed->trajvertex->vertex << " "
    //  	      << std::setw(14) << iseed->trackA()->mother() << " "
    //  	      << std::setw(14) << iseed->trackB()->mother() << " "
    // 	      << std::setw(3) << iseed->trajvertex->numTracks(LHCb::Track::Long) << " "
    // 	      << std::setw(11) << iseed->trajvertex->mass() << " "
    // 	      << std::setw(11) << iseed->trajvertex->vertex->chi2() << " "
    // 	      << std::setw(11) << iseed->density + iseed->trajvertex->vertex->chi2() << " "
    // 	      << std::sqrt(iseed->trajvertex->vertex->covMatrix()(2,2)) << std::endl ;
    if ( !seed.trackA()->used() && !seed.trackB()->used() ) {
      // flag the daughters as used
      seed.trajvertex->setAsMother();
      vertices.push_back( std::move( seed.trajvertex ) );
    } else {
      // if one of the track was used, try to add the unsed track to
      // the vertex of the other track
      if ( m_maxDeltaVertexChi2 > 0 && m_pruneSeeds && ( !seed.trackA()->used() || !seed.trackB()->used() ) ) {
        // add the unused track to the vertex of the other track.
        const LHCb::TrajParticle* usedtrack   = seed.trackA();
        const LHCb::TrajParticle* unusedtrack = seed.trackB();
        if ( unusedtrack->used() ) std::swap( usedtrack, unusedtrack );
        tryAndAdd( *unusedtrack, *( usedtrack->mother() ) );
      }
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "number of selected vertices: " << vertices.size() << endmsg;
    auto Nused = std::count_if( seedtracks.begin(), seedtracks.end(), []( const auto* t ) { return t->used(); } );
    debug() << "Number of used tracks is now: " << Nused << " out of " << tracks.size() << endmsg;
  }

  // All mutually compatible seeds survived and we added most of the
  // tracks. We can make one more loop to try the rest.
  if ( m_maxDeltaVertexChi2 > 0 ) {
    for ( const auto& track : seedtracks )
      if ( track->used() ) {
        LHCb::TrajParticleVertex* bestvertex{ nullptr };
        double                    bestdist2{ 0 }; /*, bestchi2(0) ;*/
        for ( auto& vertex : vertices ) {
          LHCb::State state = track->state( vertex->vertex->position().z() );
          double      chi2 =
              LHCb::TrackVertexUtils::vertexChi2( state, vertex->vertex->position(), vertex->vertex->covMatrix() );
          if ( chi2 < m_maxDeltaVertexChi2 ) {
            // no need to correct for angle since it is the same angle for all vertices
            double dx    = state.x() - vertex->vertex->position().x();
            double dy    = state.y() - vertex->vertex->position().y();
            double dist2 = dx * dx + dy * dy;
            if ( !bestvertex || dist2 < bestdist2 ) {
              bestdist2  = dist2;
              bestvertex = vertex.get();
              // bestchi2 = chi2 ;
            }
          }
        }
        if ( bestvertex ) { /*bool success =*/
          tryAndAdd( *track, *bestvertex );
        }
      }
  }

  // merge close vertices, if possible
  mergeVertices( vertices );

  // count the number of used tracks
  if ( msgLevel( MSG::DEBUG ) ) {
    auto Nused = std::count_if( seedtracks.begin(), seedtracks.end(), []( const auto* t ) { return t->used(); } );
    debug() << "Number of used tracks: " << Nused << " out of " << tracks.size() << endmsg;
    debug() << "Number of vertices: " << vertices.size() << endmsg;
  }
  return vertices;
}

// This routine tries to add a track to a vertex. If it succeeds, it
// updater the vertex, flags the track as use, and returns true. If
// not, if updates the veto and returns false.

bool GenericVertexFinderTwo::tryAndAdd( const LHCb::TrajParticle& track, LHCb::TrajParticleVertex& recvertex ) const {
  if ( track.used() ) error() << "track was already used!" << endmsg;
  bool success = false;
  // if( !track.veto( recvertex ) ) {
  double z             = recvertex.vertex->position().z();
  double ztolerance    = std::sqrt( 100. + 25 * recvertex.vertex->covMatrix()( 2, 2 ) );
  double zfirsthitveto = z - ztolerance;
  if ( !m_rejectUpstreamHits || zfirsthitveto < track.beginZ() ) {
    // quick method to evaluate delta-chi2:
    double dchi2 = track.chi2( *( recvertex.vertex ) );
    if ( dchi2 < m_maxDeltaVertexChi2 ) {
      // now perform a full fit. FIXME: too expensive. just add it!
      LHCb::State newstate  = track.state( z );
      auto        newvertex = std::make_unique<LHCb::TrackStateVertex>( *( recvertex.vertex ) );
      newvertex->addTrack( newstate );
      if ( newvertex->fit() == LHCb::TrackStateVertex::FitSuccess &&
           newvertex->chi2() - recvertex.vertex->chi2() < m_maxDeltaVertexChi2 ) {
        track.setMother( &recvertex );
        recvertex.daughters.push_back( &track );
        // delete recvertex.vertex ;
        recvertex.vertex = std::move( newvertex );
        success          = true;
      }
    }
  }
  return success;
}
