/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef GenericVertexFinder_H
#define GenericVertexFinder_H

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IGenericVertexFinder.h"
#include "Kernel/ITrajPoca.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrajParticle.h"
#include "TrajParticleVertex.h"

/** @class GenericVertexFinder GenericVertexFinder.h
 *
 * Gaudi::Tool that searches for vertices in a list of input tracks or
 * particles. There are two implementations
 * (GenericVertexFinder,GenericVertexFinderTwo) that differ in search
 * strategy, but in the end give quite similar results. The strategy
 * for GenericVertexFinder is
 * 1) sort input particles
 * 2) find the first combination that satisfies the 'seed' cuts
 * 3) add unused particles that fit
 * 4) repeat until all input particles used or no seeds can be found anymore
 * 5) merge vertices that are close
 *
 *  @author Wouter Hulsbergen
 **/

class GenericVertexFinder : public extends<GaudiTool, IGenericVertexFinder> {
public:
  /// typedefs etc
  using OwnedTrajParticles = std::vector<std::unique_ptr<LHCb::TrajParticle>>;
  using TrajParticles      = std::vector<const LHCb::TrajParticle*>;
  using TrajVertices       = std::vector<std::unique_ptr<LHCb::TrajParticleVertex>>;
  enum SortingStrategy { Pt = 0, Density, ZFirstHit };

  /// Standard constructor
  using extends::extends;

  /// Implementation of interface
  ParticleContainer  findVertices( LHCb::Particle::ConstVector const& particles,
                                   IGeometryInfo const&               geometry ) const override final;
  RecVertexContainer findVertices( LHCb::Track::Range const& tracks,
                                   IGeometryInfo const&      geometry ) const override final;

protected:
  /// Main routine
  virtual TrajVertices findVertices( const OwnedTrajParticles& tracks ) const;

  /// Merge vertices that are close
  void mergeVertices( TrajVertices& vertices ) const;

  /// Create a seed vertex from two tracks
  std::unique_ptr<LHCb::TrajParticleVertex> createSeed( const LHCb::TrajParticle& track1,
                                                        const LHCb::TrajParticle& track2 ) const;

  /// Sort input tracks according to some criterion
  TrajParticles sortInputTracks( const OwnedTrajParticles& tracks ) const;

  /// Fill the particle from the trajvertex result
  void fillParticleFromTrajVertex( LHCb::Particle& particle, const LHCb::TrajParticleVertex& vertex ) const;
  /// Retrieve pointer to pocatool
  const ITrajPoca& pocatool() const { return *m_pocatool; }

protected:
  PublicToolHandle<ITrackStateProvider> m_stateprovider{ "TrackStateProvider" };
  PublicToolHandle<ITrajPoca>           m_pocatool{ "TrajPoca" };
  Gaudi::Property<unsigned int>         m_maxNumCommonHits{ this, "MaxNumCommonHits", 1,
                                                    "Maximum number of common hits for seed tracks" };
  Gaudi::Property<double> m_zmin{ this, "ZMin", -300 * Gaudi::Units::mm, "Minimum z position for vertex seed" };
  Gaudi::Property<double> m_zmax{ this, "Zmax", 2800 * Gaudi::Units::mm, "Maximum z position for vertex seed" };
  Gaudi::Property<double> m_distanceCutDownstream{ this, "MaxDistanceDownstream", 5 * Gaudi::Units::mm,
                                                   "Maximum doca for seed of downstream tracks" };
  Gaudi::Property<double> m_distanceCutLong{ this, "MaxDistanceLong", 1 * Gaudi::Units::mm,
                                             "Maximum doca for seed of long tracks" };
  Gaudi::Property<double> m_minSeedOpeningAngle{ this, "MinSeedOpeningAngle", 0.005,
                                                 "Minimum opening angle for two tracks making a seed" };
  Gaudi::Property<double> m_maxSeedVertexChi2{ this, "MaxSeedVertexChi2", 4, "Maximum chi2 for seed vertex (1 DOF)" };
  Gaudi::Property<double> m_maxDeltaVertexChi2{ this, "MaxVertexDeltaChi2", 8,
                                                "Maximum chi2 for track to seed (2 DOFs)" };
  Gaudi::Property<double> m_minVertexVertexChi2{ this, "MinVertexVertexChi2", 10,
                                                 "Minimum chi2 for vertex-vertex separation (3 DOFs)" };
  Gaudi::Property<bool>   m_rejectUpstreamHits{ this, "RejectUpstreamHits", true,
                                              "Reject tracks that have hits upstream of the vertex" };
  Gaudi::Property<int>    m_sortingStrategy{ this, "SortingStrategy", SortingStrategy::Density,
                                          "Strategy used for sorting tracks" };
  Gaudi::Property<bool>   m_skipVeloTracksForMass{ this, "SkipVeloTracksForMass", true,
                                                 "Flag to specify if velo-only tracks should be skipped in vertex p4" };
};

#endif
