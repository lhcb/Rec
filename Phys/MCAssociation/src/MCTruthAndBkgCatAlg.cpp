/*****************************************************************************\
* (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Kernel/IBackgroundCategory.h"
#include "Kernel/IParticle2MCAssociator.h"
#include "Kernel/IParticleDescendants.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"

namespace MCAsscAlg {
  /// Relation tables
  using Table_P2MC     = LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>;
  using Table_P2BKGCAT = LHCb::Relation1D<LHCb::Particle, int>;

  /// Algorithm input and output types
  using alg_in  = LHCb::Particle::Range;
  using alg_out = std::tuple<Table_P2MC, Table_P2BKGCAT, LHCb::MCParticle::Selection>;

  /// Base class typedef
  using MultiTransformer = LHCb::Algorithm::MultiTransformer<alg_out( const alg_in& )>;
} // namespace MCAsscAlg

// ============================================================================
/** @class MCTruthAndBkgCatAlg
 *  Algorithm that invokes MC associator and background category tools to return two relations
 *  tables: one that maps particle to mc particle and other mapping particle to background category.
 *  Can be used stand-alone or in conjuction with MAP_INPUT functor
 *
 *  @param Input Location of particles
 *  @returns MCAssocTable location of relation between particle and mc particle
 *  @returns BkgCatTable location of relation between particle and background category
 *
 * Use:
 *  @code
 *    from PyConf.Algorithms import MCTruthAndBkgCatAlg
 *    import Functors as F
 *    #configure the algorithm with input location to particles
 *    #and associator and background category tools
 *    mctruth   = MCTruthAndBkgCatAlg(Input=input_particles,
 *                  DaVinciSmartAssociator = dv_assc,
 *                  MCMatchObjP2MCRelator = mcrel_assc,
 *                  BackgroundCategory = bkg_cat,
 *                  BackgroundCategoryViaRelations = bkg_cat_via_rel,
 *                  ParticleDescendants = part_desc)
 *
 *    #define true id functor
 *    TrueID_fun = F.MAP_INPUT(Functor=F.PARTICLE_ID,
 *                             Relations=mctruth.MCAssocTable)
 *
 *    #define background category functor
 *    BKGCAT_fun = F.BKGCAT(Relations=mctruth.BkgCatTable)
 *  @endcode
 *
 *  @author Abhijit Mathad
 *  @date 2021-12-19
 */
class MCTruthAndBkgCatAlg : public MCAsscAlg::MultiTransformer {
public:
  /// constructor
  MCTruthAndBkgCatAlg( const std::string& name, ISvcLocator* pSvc );

  /// initialise
  virtual StatusCode initialize() override;

  /// main function
  MCAsscAlg::alg_out operator()( const MCAsscAlg::alg_in& parts ) const override;

private:
  /// properties
  Gaudi::Property<bool> m_filter{ this, "FilterMCP", true,
                                  "Filter the truth-matched MCParticles. It will fill the output TruthMatchedMCP." };

  /// mc associator tools and a vector holding them
  ToolHandle<IParticle2MCAssociator> m_dv_assc{ this, "DaVinciSmartAssociator", "DaVinciSmartAssociator" };
  /// bkg category tools and a vector holding them
  mutable ToolHandle<IBackgroundCategory>              m_bkg_cat_via_rel{ this, "BackgroundCategoryViaRelations",
                                                             "BackgroundCategoryViaRelations" };
  mutable ToolHandle<IBackgroundCategory>              m_bkg_cat{ this, "BackgroundCategory", "BackgroundCategory" };
  mutable std::vector<ToolHandle<IBackgroundCategory>> m_bkg_cats;
  /// tool to get the descendants of the particle
  ToolHandle<IParticleDescendants> m_descendants{ this, "ParticleDescendants", "ParticleDescendants" };
  // counters (event and ghost)
  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{ this, "Events" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_particles{ this, "Particles" };
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_ghosts{ this, "Ghosts" };
};

/// constructor
MCTruthAndBkgCatAlg::MCTruthAndBkgCatAlg( const std::string& name, ISvcLocator* pSvc )
    : MultiTransformer(
          name, pSvc, { KeyValue{ "Input", "" } },
          { KeyValue{ "MCAssocTable", "" }, KeyValue{ "BkgCatTable", "" }, KeyValue{ "TruthMatchedMCP", "" } } ) {}

/// initialise
StatusCode MCTruthAndBkgCatAlg::initialize() {
  this->verbose() << "Initialising MCTruthAndBkgCatAlg algorithm" << endmsg;
  return MCAsscAlg::MultiTransformer::initialize().andThen( [&]() -> StatusCode {
    if ( !m_dv_assc ) {
      throw GaudiException( "No MC associators loaded.", "MCTruthAndBkgCatAlg::initialize", StatusCode::FAILURE );
    }

    m_bkg_cats.push_back( m_bkg_cat_via_rel );
    m_bkg_cats.push_back( m_bkg_cat );

    return StatusCode::SUCCESS;
  } );
}

/// main function
MCAsscAlg::alg_out MCTruthAndBkgCatAlg::operator()( const MCAsscAlg::alg_in& parts ) const {
  if ( msgLevel( MSG::VERBOSE ) ) this->verbose() << "Making relation tables" << endmsg;
  /// make relation tables: particle -> mcparticle
  MCAsscAlg::Table_P2MC p2mcp_out;
  /// make relation tables: particle -> bkg category
  MCAsscAlg::Table_P2BKGCAT p2bkgcat_out;
  /// make clone outputs
  LHCb::MCParticle::Selection mcp_out;

  for ( const auto& p : parts ) {
    /// make a flattened vector of input particle and its descendants
    LHCb::Particle::ConstVector parts_in; /// input tree, flattened
    parts_in.push_back( p );              /// input mother
    LHCb::Particle::ConstVector dauts_in = m_descendants->descendants( p );
    parts_in.insert( parts_in.end(), dauts_in.begin(), dauts_in.end() ); /// insert descendants

    /// loop over input particle + its descendants
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " -> Looping over the flatted vector of a single input particle and its descendants" << endmsg;
    for ( const auto& inP : parts_in ) {
      /// create a null pointer to mcp
      const LHCb::MCParticle* mcp = nullptr;

      /// find the corresponding mc particle
      /// search through all the associators to get the non-null pointer to mc particle
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> The particle ID of the input particle (for MC association) is " << inP->particleID().pid()
                  << endmsg;

      mcp = m_dv_assc->relatedMCP( inP );

      /// fill the relation table
      if ( mcp ) {

        if ( m_filter ) {
          auto index = mcp_out.index( mcp );
          if ( index == -1 ) {
            mcp_out.insert( mcp );
            index = mcp_out.index( mcp );
          };

          if ( msgLevel( MSG::VERBOSE ) ) {
            const auto mcp_saved = static_cast<const LHCb::MCParticle*>( mcp_out.containedObject( index ) );
            verbose() << " -> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endmsg;
            verbose() << " -> MCP fitler is ON, more info:                                      " << endmsg;
            verbose() << " -> Original MCP: PID = " << mcp->particleID().pid() << ", Ptr = " << mcp << endmsg;
            verbose() << " -> Output MCP: Idx = " << index << ", Ptr = " << mcp_saved << endmsg;
            verbose() << " -> <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endmsg;
          };
        };

        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << " -> The ID of the associated MCP is " << mcp->particleID().pid() << endmsg;
        // Think the below is SAFE to do, since the memory address that "mcp" points to wasn't dynamically allocated.
        // An alternate would be to clone MCParticle, dump in a container, get pointer to dumped object and
        // then build relation table, persisting the container. However if the container goes out of scope or copied,
        // we run into trouble.
        p2mcp_out.relate( inP, mcp ).ignore(); /// relate particle to mcp
      } else {
        ++m_ghosts;
      }

      /// get the bkg category
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Getting the BackgroundCategory" << endmsg;
      if ( !inP->isBasicParticle() ) {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << " -> The particle ID of the input particle (for BKGCAT): " << inP->particleID().pid() << endmsg;
        IBackgroundCategory::categories cat = IBackgroundCategory::Undefined;
        for ( auto& ibkgcat : m_bkg_cats ) {
          cat = ( ibkgcat->category( inP ) ).category;
          if ( cat != IBackgroundCategory::Undefined ) break;
        }
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> BackgroundCategory decision is " << cat << endmsg;
        p2bkgcat_out.relate( inP, cat ).ignore();
      }
      // update particle counter
      ++m_particles;
    }
  }
  // update event counter
  if ( !parts.empty() ) ++m_eventCount;
  if ( msgLevel( MSG::VERBOSE ) ) this->verbose() << "Done making relation tables" << endmsg;
  return std::make_tuple( p2mcp_out, p2bkgcat_out, std::move( mcp_out ) );
}

DECLARE_COMPONENT( MCTruthAndBkgCatAlg )
