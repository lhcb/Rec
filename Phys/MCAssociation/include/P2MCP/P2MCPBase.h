/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef P2MCP_P2MCPBASE_H
#define P2MCP_P2MCPBASE_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
// local
#include "P2MCP/IP2MCP.h" // Interface

/** @class P2MCPBase P2MCPBase.h P2MCP/P2MCPBase.h
 *
 *  Common implementation for descendants of IParticle2MCAssociator.
 *  Mainly inline helper methods for common implementation of host of
 *  similar methods in the interface.
 *  Set of methods is self-consistent. Derived classes only need to implement
 *  method
 *  @code
 *  bool isMatched(const LHCb::Particle*, const LHCb::MCParticle)
 *  @endcode
 *  and
 *
 *  @author Juan PALACIOS
 *  @date   2009-01-30
 */
class GAUDI_API P2MCPBase : public extends1<GaudiTool, IP2MCP> {
public:
  /// Standard constructor
  P2MCPBase( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode finalize() override;

  virtual ~P2MCPBase();

  const LHCb::MCParticle* relatedMCP( const LHCb::Particle* particle ) const override;

  const LHCb::MCParticle* operator()( const LHCb::Particle* particle ) const override;

  const LHCb::MCParticle* relatedMCP( const LHCb::Particle*, const std::string& mcParticleLocation ) const override;

  const LHCb::MCParticle* relatedMCP( const LHCb::Particle*                particle,
                                      const LHCb::MCParticle::ConstVector& mcParticles ) const override;

  const LHCb::MCParticle* relatedMCP( const LHCb::Particle*              particle,
                                      const LHCb::MCParticle::Container& mcParticles ) const override;

  P2MCP::DecayLines relatedMCPs( const LHCb::Particle* particle ) const override;

  P2MCP::DecayLines relatedMCPs( const LHCb::Particle* particle, const std::string& mcParticleLocation ) const override;

  P2MCP::DecayLines relatedMCPs( const LHCb::Particle*                particle,
                                 const LHCb::MCParticle::ConstVector& mcParticles ) const override;

  P2MCP::DecayLines relatedMCPs( const LHCb::Particle*              particle,
                                 const LHCb::MCParticle::Container& mcParticles ) const override;

  bool isMatched( const LHCb::Particle* particle, const LHCb::MCParticle* mcParticle ) const override;

private:
  inline LHCb::MCParticle::Container* i_MCParticles( const std::string& location ) const {
    return ( exist<LHCb::MCParticle::Container>( location ) ) ? get<LHCb::MCParticle::Container>( location ) : 0;
  }

  template <typename Iter>
  const LHCb::MCParticle* i_bestMCP( const LHCb::Particle* particle, Iter begin, Iter end ) const {
    P2MCP::DecayLines trees = i_relatedMCPs( particle, begin, end );
    return ( trees.empty() ) ? 0 : trees[0].back();
  }

  template <typename Iter>
  P2MCP::DecayLines i_relatedMCPs( const LHCb::Particle* particle, Iter begin, Iter end ) const {
    if ( 0 != particle ) {
      LHCb::MCParticle::ConstVector mcps;
      for ( Iter iMCP = begin; iMCP != end; ++iMCP ) {
        const bool match = isMatched( particle, *iMCP );
        if ( match ) mcps.push_back( *iMCP );
      }
      return P2MCP::DecayLines( mcps );
    } else {
      Warning( "No particle!" ).ignore();
      return P2MCP::DecayLines();
    }
  }

private:
  std::string m_defMCLoc;
};
#endif // P2MCP_P2MCPBASE_H
