/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "Event/ParticleUtils.h"
#include "JetUtils.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <algorithm>
#include <cmath>

/*

Receives a vector of LHCb::Particle containers, basic  or composite, outputs a list of particles
removing duplicities (no protoparticle is used more than once)

For each composite particle, if the protoparticles associated to their
daughters are not in the list of used protoparticles yet, they are added and the composite particle is saved in the
output list.

For each basic particle, if the protoparticle associated to it is not in the list of used protoparticles,
it is added there and the particle saved in the list of output particles.

The used protparticles are separated in neutral, positive ans negative charges containers, to speed up processing.

TODO: The test performed to avoid double counting is done by using the particle's address.
We plan to include LoKi::CheckOverlap for next version.

*/

namespace LHCb {

  namespace {
    class ProtoSet {
      std::array<std::set<const ProtoParticle*>, 3> protos;
      static constexpr int                          idx( int charge ) { return charge == 0 ? 0 : charge < 0 ? 1 : 2; }

    public:
      auto insert( const Particle& pp ) {
        if ( !pp.proto() ) {
          throw GaudiException( "Particle has no ProtoParticle", __PRETTY_FUNCTION__, StatusCode::FAILURE );
        }
        return protos[idx( pp.charge() )].insert( pp.proto() );
      }
      bool contains( const Particle& p ) const {
        auto const& s = protos[idx( p.charge() )];
        return s.find( p.proto() ) != s.end();
      }
    };
  } // namespace

  using inputs = const Gaudi::Functional::vector_of_const_<Particles>;

  class ParticleFlowMaker : public Algorithm::MergingTransformer<Particle::Selection( inputs const& )> {

  private:
    mutable Gaudi::Accumulators::Counter<>     m_count{ this, "00: # Number of Events" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputChargedsCounter{ this, "01: # Basic Charged Input Particles" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputNeutralsCounter{ this, "02: # Basic Neutral Input Particles" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbInputCompositesCounter{ this, "03: # Composite Input Particles" };

    mutable Gaudi::Accumulators::StatCounter<> m_nbChargedsCounter{ this, "04: # Basic Charged Output Particles" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbNeutralsCounter{ this, "05: # Basic Neutral Output Particles" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbCompositesCounter{ this, "06: # Composite Output Particles" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbParticleCounter{ this, "07: # Merged Output Particles" };

    mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedBasicsCounter{
        this, "08: # ProtoParticles from Basic Particles rejected" };
    mutable Gaudi::Accumulators::StatCounter<> m_nbRejectedCompositesCounter{
        this, "09: # ProtoParticles from Composite Particles rejected" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nbBasicChargedWithNANs{
        this, "10: Basic Charged has NAN/INF information", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nbBasicNeutralWithNANs{
        this, "11: Basic Neutral has NAN/INF information", 0 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nbCompositePartWithNANs{
        this, "12: Composite has NAN/INF information", 0 };

  public:
    /// Constructor.
    ParticleFlowMaker( const std::string& name, ISvcLocator* svc )
        : MergingTransformer( name, svc, { "Inputs", {} }, { "Output", "Phys/ParticleFlow/Particles" } ) {}

    // Main method
    Particle::Selection operator()( inputs const& Inputs ) const override {

      ++m_count;

      // Input can be in any order. Separate into categories (composites, basic_positives, basic_negatives and
      // neutrals). Already process particles whose daughter shoud be rejected

      Particle::ConstVector basics, composites;

      for ( const auto& Input : Inputs ) {
        if ( Input.empty() ) { continue; }
        // Enough to interrogate the first particle in each Input list. Assumes same type
        // of Particles in each Input
        const bool isBasic = ( *Input.begin() )->isBasicParticle();
        auto*      parts   = ( isBasic ? &basics : &composites );
        parts->reserve( parts->size() + Input.size() );
        for ( const auto prt : Input ) { parts->emplace_back( prt ); }
      }
      m_nbInputCompositesCounter += composites.size();

      Particle::Selection prts;

      // Now process the composite particles
      ProtoSet protos{};
      for ( const auto prt : composites ) {
        auto dtrs = LHCb::Utils::Particle::getBasics( *prt );
        // A composite Particle is not added to the output list if at least one of their
        // daughther protoparticles is already used
        if ( JetAccessories::partIsFinite( *prt ) ) {
          if ( LHCb::Utils::Particle::details::any_of( dtrs,
                                                       [&]( const auto& dtr ) { return protos.contains( *dtr ); } ) ) {
            // Some protoparticle from this composite particle was already used
          } else {
            for ( const auto dtr : dtrs ) { protos.insert( *dtr ); }
            prts.insert( prt ); // Insert composite particles to the output list
          }
        } else {
          ++m_nbCompositePartWithNANs;
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "Composite particle with NAN/INF data found in '" << JetAccessories::particleLocation( *prt )
                    << "'" << endmsg;
          }
        }
      }
      const auto nbCompositesOut      = prts.size();
      const auto nbRejectedComposites = composites.size() - prts.size();
      m_nbRejectedCompositesCounter += nbRejectedComposites;
      m_nbCompositesCounter += nbCompositesOut;

      // Now look at the basic particles ...
      std::size_t nbChargedsIn     = 0;
      std::size_t nbNeutralsIn     = 0;
      std::size_t nbNeutralsOut    = 0;
      std::size_t nbChargedsOut    = 0;
      std::size_t nbRejectedBasics = 0;

      for ( const auto prt : basics ) {
        const bool isNeutral = ( prt->charge() == 0 );
        if ( JetAccessories::partIsFinite( *prt ) ) {
          ++( isNeutral ? nbNeutralsIn : nbChargedsIn );
          if ( protos.insert( *prt ).second ) {
            prts.insert( prt );
            ++( isNeutral ? nbNeutralsOut : nbChargedsOut );
          } else {
            ++nbRejectedBasics;
          }
        } else {
          ++( isNeutral ? m_nbBasicNeutralWithNANs : m_nbBasicChargedWithNANs );
          if ( msgLevel( MSG::DEBUG ) ) {
            debug() << "Basic particle with NAN/INF data found in '" << JetAccessories::particleLocation( *prt ) << "'"
                    << endmsg;
          }
        }
      }

      m_nbInputChargedsCounter += nbChargedsIn;
      m_nbInputNeutralsCounter += nbNeutralsIn;
      m_nbNeutralsCounter += nbNeutralsOut;
      m_nbChargedsCounter += nbChargedsOut;
      m_nbRejectedBasicsCounter += nbRejectedBasics;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Output: #composites: " << nbCompositesOut << " #chargeds: " << nbChargedsOut
                << " #neutrals: " << nbNeutralsOut << " Rejected composites: " << nbRejectedComposites
                << " Rejected basics: " << nbRejectedBasics << endmsg;
        debug() << "Number of output particles: " << prts.size() << endmsg;
      }

      m_nbParticleCounter += prts.size();

      return prts;
    }
  }; // namespace LHCb

  DECLARE_COMPONENT_WITH_ID( ParticleFlowMaker, "ParticleFlowMaker" )

} // namespace LHCb
