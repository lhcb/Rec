/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/
#pragma once
#include "Event/Particle.h"

namespace LHCb::JetAccessories {

  inline auto partIsFinite( const LHCb::Particle& p ) {
    return ( std::isfinite( p.momentum().Px() ) && //
             std::isfinite( p.momentum().Py() ) && //
             std::isfinite( p.momentum().Pz() ) && //
             std::isfinite( p.momentum().E() ) );
  }

  inline auto particleLocation( const LHCb::Particle& p ) {
    const auto pObj = p.parent();
    return ( !pObj ? "Null DataObject !" : ( pObj->registry() ? pObj->registry()->identifier() : "UnRegistered" ) );
  }
} // namespace LHCb::JetAccessories
