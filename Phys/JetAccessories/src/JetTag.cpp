/*****************************************************************************\
 * * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/
#include "Event/Particle.h"
#include "Kernel/IRelatedPVFinder.h"
#include "LHCbAlgs/Transformer.h"

/*
Receives lists of jets and return only the jets that are tagged by the list of tags.
The tagging condition is DeltaR<ConeSizeTag (default: 0.5)
*/

class JetTag : public LHCb::Algorithm::Transformer<LHCb::Particle::Selection(
                   const LHCb::Particle::Range&, const LHCb::Particle::Range&, const LHCb::RecVertices& )> {

  /// Tool to RelatedPVFinder
  ToolHandle<IRelatedPVFinder> m_prtVtxTool = {
      this, "RelatedPVFinder",
      "GenericParticle2PVRelator__p2PVWithIPChi2_OfflineDistanceCalculatorName_/P2PVWithIPChi2" };
  Gaudi::Property<float> m_ConeSizeTag{ this, "ConeSizeTag", 0.5, "Cone size to tag jets." };
  Gaudi::Property<bool>  m_UseFlightDirection{ this, "UseFlightDirection", false,
                                              "Flag to use flight direction if tag is composite." };

public:
  /// Constructor.
  JetTag( const std::string& name, ISvcLocator* svc )
      : Transformer( name, svc, { KeyValue{ "Jets", "" }, KeyValue{ "Tags", "" }, KeyValue{ "PVLocation", "" } },
                     { "Output", "" } ) {}

  // Main method
  LHCb::Particle::Selection operator()( const LHCb::Particle::Range& Jets, const LHCb::Particle::Range& Tags,
                                        const LHCb::RecVertices& PrimaryVertices ) const override {
    LHCb::Particle::Selection prts{ Jets.begin(), Jets.end(), [&]( const auto& jet ) {
                                     auto jetphi = jet->momentum().Phi();
                                     auto jeteta = jet->momentum().Eta();
                                     return std::any_of( Tags.begin(), Tags.end(), [&]( const auto& tag ) {
                                       auto tagphi = tag->momentum().Phi();
                                       auto tageta = tag->momentum().Eta();
                                       if ( !tag->isBasicParticle() && m_UseFlightDirection ) {
                                         if ( auto vrt = m_prtVtxTool->relatedPV( tag, PrimaryVertices ); vrt ) {
                                           // if particle is composite and flag to use flight direction is true
                                           Gaudi::XYZVector vec = tag->endVertex()->position() - vrt->position();
                                           tagphi               = vec.Phi();
                                           tageta               = vec.Eta();
                                         }
                                       }
                                       auto dphi = tagphi - jetphi;
                                       auto deta = tageta - jeteta;
                                       while ( dphi > M_PI ) dphi -= 2 * M_PI;
                                       while ( dphi <= -M_PI ) dphi += 2 * M_PI;
                                       return dphi * dphi + deta * deta < m_ConeSizeTag * m_ConeSizeTag;
                                     } );
                                   } };
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of output particles: " << prts.size() << endmsg;
    return prts;
  }
};

DECLARE_COMPONENT( JetTag )
