/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STL & STD
// ============================================================================
#include <csignal>
#include <string>
// ============================================================================
// Gaudi
// ============================================================================
#include "GaudiAlg/GaudiTool.h"
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#include "Kernel/ICheckOverlap.h"
// ============================================================================
// PhysEvent
// ============================================================================
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
// ============================================================================
// LHCbMathq
// ============================================================================
#include "LHCbMath/EqualTo.h"
// ============================================================================
// DaVinciTypes
// ============================================================================
#include "Kernel/HashIDs.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class CheckOverlap CheckOverlap.h
   *
   *  Tool to check if more than one particle originate
   *  form the same protoparticle.
   *
   *  Specific configuration properties
   *  - Type1CloneSlopeXDifference : slope-X  difference to identify ``clone type-1''
   *  - Type1CloneSlopeYDifference : slope-Y  difference to identify ``clone type-1''
   *  - Type2CloneSlopeDifference  : slope    difference to identify ``clone type-2''
   *  - Type2CloneQoverPDifference : Q/p      difference to identify ``clone type-2''
   *  - TrackOverlap :  overlap criteria for tracks (percentage of common hits)
   *  - CaloOverlap  :  overlap criteria for calos  (percentage of common hits)
   *
   *  Default values for "differences" are taken from Matt Needham:
   *  @code
   *  int type1Clone(float tx1, float tx2, float ty1, float ty2) {
   *   return ((TMath::Abs(tx1 -tx2) < 0.0004) && (TMath::Abs(ty1 -ty2) < 0.0002));
   *  }
   *  int type2Clone(float tx1, float tx2, float ty1, float ty2, float qDivp1, float qDivp2  ){
   *   return ((TMath::Abs(tx1 -tx2) < 0.005) && (TMath::Abs(ty1 -ty2) < 0.005) && TMath::Abs(qDivp1 - qDivp2) < 1e-6);
   *  }
   *  @endcode
   *  (with the reference to Wouter Hulsbergen's studies:
   *  @see https://indico.cern.ch/event/134170/contributions/135558/attachments/104261/148751/clones.pdf
   *
   *  If parameter is outside its "physical" range, the corresponding
   *  criterion is not used to identify overlaps
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date   2017-01-20
   */
  // ==========================================================================
  class CheckOverlap : public extends1<GaudiTool, ICheckOverlap> {
  public:
    // ========================================================================
    /// Standard constructor
    CheckOverlap( const std::string& type, const std::string& name, const IInterface* parent )
        : base_class( name, type, parent )
        , m_clone_type1_slopeX( 0.0004 ) // clone "Type1" slope difference
        , m_clone_type1_slopeY( 0.0002 ) // clone "Type1" slope difference
        , m_clone_type2_slope( 0.005 )   // clone "Type2" slope difference
        , m_clone_type2_qOverP( 1.e-6 )  // clone "Type2" q/P   difference
        , m_overlap_tracks( 0.70 )       // overlap criteria for tracks
        , m_overlap_calos( 0.50 )        // overlap criteria for calo
    {
      declareProperty( "Type1CloneSlopeXDifference",
                       m_clone_type1_slopeX, // clone "Type1" slope difference
                       "Slope-X difference to identify ``Type1-clone'' [ignore, if negative]" );
      declareProperty( "Type1CloneSlopeYDifference",
                       m_clone_type1_slopeY, // clone "Type1" slope difference
                       "Slope-Y difference to identify ``Type1-clone'' [ignore, if negative]" );
      declareProperty( "Type2CloneSlopeDifference",
                       m_clone_type2_slope, // clone "Type2" slope difference
                       "Slope difference to identify ``Type2-clone'' [ignore, if negative]" );
      declareProperty( "Type2CloneQoverPDifference",
                       m_clone_type2_qOverP, // clone "Type2" slope difference
                       "Q/p   difference to identify ``Type2-clone'' [ignore, if negative]" );
      declareProperty( "TrackOverlap",
                       m_overlap_tracks, // overlap criteria for tracks
                       "Overlap criteria for tracks (LHCb::HashID::overlap) [ignore if not 0<p<=1]" );
      declareProperty( "CaloOverlap",
                       m_overlap_calos, // overlap criteria for calo
                       "Overlap criteria for calo   (LHCb::HashID::overlap) [ignore if not 0<p<=1]" );
    }
    /// Destructor
    virtual ~CheckOverlap() {}
    // ========================================================================
  public:
    // ========================================================================
    /** Check for duplicate use of a protoparticle to produce particles.
     *  Argument: parts is a vector of pointers to particles.
     *  Create an empty vector of pointers to protoparticles.
     *  Call the real check method.
     */
    bool foundOverlap( const LHCb::Particle::ConstVector& parts ) const override;
    // ========================================================================
    /** Check for duplicate use of a protoparticle to produce particles.
     *  Arguments: particle1 up to particle4 are pointers to particles.
     *  Create a ParticleVector and fill it with the input particles.
     *  Create an empty vector of pointers to protoparticles.
     *  Call the real check method.
     */
    bool foundOverlap( const LHCb::Particle* ) const override;
    bool foundOverlap( const LHCb::Particle*, const LHCb::Particle* ) const override;
    bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle* ) const override;
    bool foundOverlap( const LHCb::Particle*, const LHCb::Particle*, const LHCb::Particle*,
                       const LHCb::Particle* ) const override;
    // ========================================================================
    /**  Check for duplicate use of a protoparticle to produce decay tree of
     *   any particle in vector. Removes found particles from vector.
     */
    StatusCode removeOverlap( LHCb::Particle::ConstVector& ) const override;
    // ========================================================================
  private:
    // ========================================================================
    /// overlap of two particles
    inline bool overlap( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const;
    /// overlap of two proto-particles
    inline bool overlap( const LHCb::ProtoParticle* p1, const LHCb::ProtoParticle* p2 ) const;
    /// overlap of two calo hypos
    inline bool overlap( const LHCb::CaloHypo* h1, const LHCb::CaloHypo* h2 ) const;
    /// overlap of two calo clusters
    inline bool overlap( const LHCb::CaloCluster* c1, const LHCb::CaloCluster* c2 ) const;
    /// check overlap of any particle from the range with the particle
    template <class PARTICLE>
    inline PARTICLE overlapped( PARTICLE begin, PARTICLE end, const LHCb::Particle* p ) const {
      // trivia:
      if ( nullptr == p ) { return end; } // NULLs are not overlapping
      for ( ; begin != end; ++begin ) {
        if ( overlap( p, *begin ) ) { return begin; }
      }
      return end;
    }
    // check overlaps in the container
    template <class PARTICLE>
    inline PARTICLE overlapped( PARTICLE begin, PARTICLE end ) const {
      //  trivia:
      if ( begin == end ) { return end; } // nothing to overlap
      for ( ; begin != end; ++begin ) {
        const LHCb::Particle* p = *begin;
        if ( nullptr == p ) { continue; }
        PARTICLE ov = overlapped( begin + 1, end, p );
        if ( ov != end ) { return ov; } // found the first overlap!
      }
      return end;
    }
    // ========================================================================
  private:
    // ========================================================================
    /// clone "Type1" slope-X difference
    double m_clone_type1_slopeX; // clone "Type1" slope-X difference
    /// clone "Type1" slope-Y difference
    double m_clone_type1_slopeY; // clone "Type1" slope-Y difference
    /// clone "Type2" slope difference
    double m_clone_type2_slope; // clone "Type2" slope difference
    /// clone "Type2" q/P difference
    double m_clone_type2_qOverP; // clone "Type2" q/P   difference
    /// overlap criteria for tracks
    double m_overlap_tracks; // overlap criteria for tracks
    /// overlap criteria for calo
    double m_overlap_calos; // overlap criteria for calo
    // ========================================================================
  }; // End of clas LoKi::CheckOverlap
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::CheckOverlap )
// ============================================================================
// check overlap of two particles
// ============================================================================
inline bool LoKi::CheckOverlap::overlap( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const {
  /// trivia:
  if ( nullptr == p1 ) { return false; } // NULLs are not overlapping
  if ( nullptr == p2 ) { return false; } // NULLs are not overlapping
  if ( p1 == p2 ) { return true; }       // the same particlle
  //
  const SmartRefVector<LHCb::Particle>& ds1 = p1->daughters();
  if ( !ds1.empty() ) { return ds1.end() != overlapped( ds1.begin(), ds1.end(), p2 ); }
  //
  const SmartRefVector<LHCb::Particle>& ds2 = p2->daughters();
  if ( !ds2.empty() ) { return ds2.end() != overlapped( ds2.begin(), ds2.end(), p1 ); }
  //
  // here we compare two basic particle
  const LHCb::ProtoParticle* pp1 = p1->proto();
  const LHCb::ProtoParticle* pp2 = p2->proto();
  //
  return overlap( pp1, pp2 );
}
// ==========================================================================
// check overlap of two CaloClusters
// ==========================================================================
inline bool LoKi::CheckOverlap::overlap( const LHCb::CaloCluster* cc1, const LHCb::CaloCluster* cc2 ) const {
  // // trivia:
  // if ( nullptr == cc1 ) { return false ; } // nulls are not overlapping
  // if ( nullptr == cc2 ) { return false ; } // nulls are not overlapping
  // if ( cc1     == cc2 ) { return true  ; } // the same proto
  // //
  // // it is enough to compare seeds:
  // return cc1->seed() == cc2->seed() ;
  //
  return nullptr == cc1   ? false
         : nullptr == cc2 ? false
         : cc1 == cc2     ? true
                          : cc1->seed() == cc2->seed(); // for clusters it is enought to
                                                    // compare seed
}
// ==========================================================================
// check overlap of two CaloHypos
// ==========================================================================
inline bool LoKi::CheckOverlap::overlap( const LHCb::CaloHypo* ch1, const LHCb::CaloHypo* ch2 ) const {
  // trivia:
  if ( nullptr == ch1 ) { return false; } // nulls are not overlapping
  if ( nullptr == ch2 ) { return false; } // nulls are not overlapping
  if ( ch1 == ch2 ) { return true; }      // the same proto
  //
  const LHCb::CaloPosition* cp1 = ch1->position();
  const LHCb::CaloPosition* cp2 = ch2->position();
  //
  if ( nullptr != cp1 && nullptr != cp2 ) {
    // check ``exact numeric comparison'': it is safe here ...
    static const LHCb::Math::Equal_To<double>         s_equal{};
    static const LHCb::Math::Equal_To<Gaudi::Vector2> s_equal_v2{};
    static const LHCb::Math::Equal_To<Gaudi::Vector3> s_equal_v3{};
    //
    if ( cp1 == cp2 ) {
      return true;
    } else if ( s_equal( cp1->z(), cp2->z() ) &&
                ( s_equal_v2( cp1->center(), cp2->center() ) || s_equal_v3( cp1->parameters(), cp2->parameters() ) ) ) {
      return true;
    }
  }
  //
  const SmartRefVector<LHCb::CaloCluster>& cc1 = ch1->clusters();
  const SmartRefVector<LHCb::CaloCluster>& cc2 = ch2->clusters();
  //
  // different CaloHypo
  if ( cc1.size() != cc2.size() ) { return false; }
  // check clusters one-by-one
  for ( const LHCb::CaloCluster* c1 : cc1 ) {
    for ( const LHCb::CaloCluster* c2 : cc2 ) {
      if ( overlap( c1, c2 ) ) { return true; }
    }
  }
  //
  return false;
}
// ==========================================================================
// check overlap of two protoparticles
// ==========================================================================
inline bool LoKi::CheckOverlap::overlap( const LHCb::ProtoParticle* pp1, const LHCb::ProtoParticle* pp2 ) const {
  // trivia:
  if ( nullptr == pp1 ) { return false; } // nulls are not overlapping
  if ( nullptr == pp2 ) { return false; } // nulls are not overlapping
  if ( pp1 == pp2 ) { return true; }      // the same proto
  //
  const LHCb::Track* t1 = pp1->track();
  const LHCb::Track* t2 = pp2->track();

  //
  // trival cases:
  //
  // - the same track
  if ( nullptr != t1 && t1 == t2 ) { return true; }
  // - track vs calo
  if ( nullptr != t1 && nullptr == t2 && !pp2->calo().empty() ) { return false; }
  // - calo  vs track
  if ( nullptr != t2 && nullptr == t1 && !pp1->calo().empty() ) { return false; }
  //
  // track vs track
  if ( nullptr != t1 && nullptr != t2 ) {
    //
    if ( t1->charge() != t2->charge() ) { return false; }
    //
    // trivial ``exact'' comparison for few basic properties
    static const LHCb::Math::Equal_To<double>           s_equal_1{};
    static const LHCb::Math::Equal_To<Gaudi::XYZVector> s_equal_2{};
    static const LHCb::Math::Equal_To<Gaudi::XYZPoint>  s_equal_3{};
    //
    if ( s_equal_1( t1->chi2(), t2->chi2() ) && s_equal_1( t1->phi(), t2->phi() ) &&
         s_equal_2( t1->momentum(), t2->momentum() ) && s_equal_3( t1->position(), t2->position() ) ) {
      return true;
    }
    //
    const LHCb::State* s1 = t1->stateAt( LHCb::State::Location::ClosestToBeam );
    if ( nullptr == s1 ) { s1 = t1->stateAt( LHCb::State::Location::FirstMeasurement ); }
    if ( nullptr == s1 ) { s1 = &( t1->firstState() ); }
    const LHCb::State* s2 = &( t2->closestState( s1->z() ) );
    //
    // first try ``exact comparison''
    if ( s_equal_1( s1->z(), s2->z() ) && s_equal_1( s1->x(), s2->x() ) && s_equal_1( s1->y(), s2->y() ) &&
         s_equal_1( s1->tx(), s2->tx() ) && s_equal_1( s1->ty(), s2->ty() ) ) {
      return true;
    }
    //
    // Now it is a time for Matt's recipe:
    //  - clone type-1
    if ( 0 < m_clone_type1_slopeX && 0 < m_clone_type1_slopeY &&
         std::abs( s1->tx() - s2->tx() ) <= m_clone_type1_slopeX &&
         std::abs( s1->ty() - s2->ty() ) <= m_clone_type1_slopeY ) {
      return true;
    }
    //
    //  - clone type-2
    if ( 0 < m_clone_type2_slope && 0 < m_clone_type2_qOverP ) {
      return std::abs( s1->tx() - s2->tx() ) <= m_clone_type2_slope &&
             std::abs( s1->ty() - s2->ty() ) <= m_clone_type2_slope &&
             std::abs( s1->qOverP() - s2->qOverP() ) <= m_clone_type2_qOverP;
    }
    //
    // check with percentage of common LHCbIDs
    if ( 0 < m_overlap_tracks && m_overlap_tracks <= 1 ) {
      const std::pair<double, double> ov = LHCb::HashIDs::overlap( t1, t2 );
      return m_overlap_tracks <= std::max( ov.first, ov.second );
    }
    //
    // Warning ( "Unclassified track overlap, ignore it" ).ignore() ;
    return false;
  }
  // calo vs calo
  if ( nullptr == t1 && !pp1->calo().empty() && nullptr == t2 && !pp2->calo().empty() ) {
    //
    const SmartRefVector<LHCb::CaloHypo>& chs1 = pp1->calo();
    const SmartRefVector<LHCb::CaloHypo>& chs2 = pp2->calo();
    //
    // compare CaloHypos one by one:
    // it it safe since here tracks are nulls...  (no confusion with bremms)
    for ( const LHCb::CaloHypo* ch1 : chs1 ) {
      for ( const LHCb::CaloHypo* ch2 : chs2 ) {
        if ( overlap( ch1, ch2 ) ) { return true; }
      }
    }
    //
    if ( 0 < m_overlap_calos && m_overlap_calos <= 1 ) {
      const std::pair<double, double> ov = LHCb::HashIDs::overlap( pp1, pp2 );
      return m_overlap_calos <= std::max( ov.first, ov.second );
    }
    //
    // Warning ( "Unclassified calo overlap, ignore it" ).ignore() ;
    return false;
  }
  //
  // weird case: should never happens
  // Warning ( "Error in overlap, ignore it" ).ignore() ;
  return false;
}
// ============================================================================
bool LoKi::CheckOverlap::foundOverlap( const LHCb::Particle* particle ) const {
  if ( nullptr == particle ) { return false; } // NULLs are not overlapping
  const SmartRefVector<LHCb::Particle>& ds = particle->daughters();
  return ds.end() != overlapped( ds.begin(), ds.end() );
}
// ============================================================================
bool LoKi::CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2 ) const {
  return overlap( particle1, particle2 );
}
// ============================================================================
bool LoKi::CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2,
                                       const LHCb::Particle* particle3 ) const {
  return overlap( particle1, particle2 ) || overlap( particle1, particle3 ) || overlap( particle2, particle3 );
}
// ============================================================================
bool LoKi::CheckOverlap::foundOverlap( const LHCb::Particle* particle1, const LHCb::Particle* particle2,
                                       const LHCb::Particle* particle3, const LHCb::Particle* particle4 ) const {
  return overlap( particle1, particle2 ) || overlap( particle1, particle3 ) || overlap( particle1, particle4 ) ||
         //
         overlap( particle2, particle3 ) || overlap( particle2, particle4 ) ||
         //
         overlap( particle3, particle4 );
}
// ============================================================================
/*  Check for duplicate use of a protoparticle to produce particles.
 *  Argument: parts is a vector of pointers to particles.
 *  Create an empty vector of pointers to protoparticles.
 *  Call the real check method.
 */
// ============================================================================
bool LoKi::CheckOverlap::foundOverlap( const LHCb::Particle::ConstVector& parts ) const {
  return parts.end() != overlapped( parts.begin(), parts.end() );
}
// ============================================================================
/*   Check for duplicate use of a protoparticle to produce decay tree of
 *   any particle in vector. Removes found particles from vector.
 */
// ============================================================================
StatusCode LoKi::CheckOverlap::removeOverlap( LHCb::Particle::ConstVector& v ) const {
  LHCb::Particle::ConstVector::iterator ov = overlapped( v.begin(), v.end() );
  while ( v.end() != ov ) {
    v.erase( ov );
    ov = overlapped( v.begin(), v.end() );
  }
  return StatusCode::SUCCESS;
}
// ============================================================================
// The END
// ============================================================================
