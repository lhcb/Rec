/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PREAMBULO_H
#  define LOKI_PREAMBULO_H 1
// ============================================================================
// Incldue files
// ============================================================================
// STD & STL
// ============================================================================
#  include <string>
#  include <vector>
// ============================================================================
namespace {
  // ==========================================================================
  /// construct the preambulo string
  inline std::string _preambulo( const std::vector<std::string>& lines ) {
    std::string result;
    for ( std::vector<std::string>::const_iterator iline = lines.begin(); lines.end() != iline; ++iline ) {
      if ( lines.begin() != iline ) { result += "\n"; }
      result += ( *iline );
    }
    return result;
  }
  // ==========================================================================
} //                                                 end of anonymous namespace
// ============================================================================
// The END
// ============================================================================
#endif // COMPONENTS_PREAMBULO_H
// ============================================================================
