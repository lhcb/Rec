/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Kernel/IParticleDictTool.h"
#include "Kernel/IParticleValue.h" // Interface

#include "LoKi/IHybridFactory.h"
#include "LoKi/PhysTypes.h"

#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/VectorMap.h"

namespace LoKi {

  namespace Hybrid {

    /** @class DictValue DictValue.h Components/DictValue.h
     * Accessing one field of the dictionary
     * used to construct cuts on dictionary values produced by DictTransform
     * in collaboration with the VALUE adapter
     * always check if there already exists a LoKi functor to get the variable directly
     *
     *  @author Sebastian Neubert
     *  @date   2013-08-05
     */
    class DictValue : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IParticleValue> {
    public:
      using extends::extends;

      /** get the value
       *  @see IParticleValue
       *  @param particle the particle which the dictionary tool-chain will operate on
       *  @return value
       */
      double operator()( const LHCb::Particle* particle ) const override;

    private:
      ConditionAccessor<DetectorElement> m_lhcb{ this, "StandardGeometryTop", LHCb::standard_geometry_top };
      ToolHandle<IParticleDictTool>      m_source{ this, "Source", "", "Type/Name for Source Dictionary Tool" };
      Gaudi::Property<std::string>       m_key{ this, "Key", "", "name of the variable we want to access" };
    };

  } // namespace Hybrid

} // namespace LoKi

/*  Fill the tuple.
 *  @see IParticleTupleTool
 *  @param top      the top particle of the decay.
 *  @param particle the particle about which some info are filled.
 *  @param head     prefix for the tuple column name.
 *  @param tuple    the tuple to fill
 *  @return status code
 */
double LoKi::Hybrid::DictValue::operator()( const LHCb::Particle* particle ) const {
  /// call IParticleDictTool to aquire the dictionary
  /// request the dictionary of variables from the source
  IParticleDictTool::DICT dict;
  m_source->fill( particle, dict, *m_lhcb.get().geometry() ).ignore();
  // return the value for the given key
  return dict[m_key.value()];
}

/// Declaration of the Tool Factory
DECLARE_COMPONENT( LoKi::Hybrid::DictValue )
