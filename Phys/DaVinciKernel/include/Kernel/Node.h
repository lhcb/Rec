/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef NODE_H
#define NODE_H 1

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include <iostream>
#include <memory>
#include <regex>
#include <string>
#include <vector>

namespace detail {
  using tuple_nodeprop_t = std::tuple<std::string, bool, bool>;
  using ptr_of_ptrv1     = std::shared_ptr<const LHCb::Particle* const>;
  using ptr_of_ptrv1mc   = std::shared_ptr<const LHCb::MCParticle* const>;
} // namespace detail

namespace Decay {
  class Node {
  public:
    /// constructors
    Node();
    Node( const std::string& node_name );
    Node( const int& pid, const bool& hasCC, const bool& isMarked );
    Node( const detail::ptr_of_ptrv1& particle );
    Node( const detail::ptr_of_ptrv1mc& mcparticle );

    /// copy constructor
    Node( const Node& node );
    /// copy assignment operator
    Node& operator=( const Node& node );

    /// get charge conjugated node
    Node getChargeConjugate( bool reversehasCC = false ) const;
    /// reverse the has CC flag
    void reversehasCC() { m_hasCC = !m_hasCC; }
    /// get the possibilities of the node i.e. [mu+]CC should give vec(mu+, mu-)
    std::vector<Node> getPossibilities() const;
    /// get particle name
    std::string name() const { return getNameFromPid( m_pid ); }

    /* getters */
    /// get particle id
    int pid() const { return m_pid; }
    /// get has CC flag
    bool hasCC() const { return m_hasCC; }
    /// get is marked flag with ^
    bool isMarked() const { return m_isMarked; }

    /* setters */
    /// set pid
    void setpid( const int& pid ) { m_pid = pid; }
    /// set CC flag
    void sethasCC( const bool& cc ) { m_hasCC = cc; }
    /// set marked flag
    void setisMarked( const bool& marked ) { m_isMarked = marked; }

    /* operator */
    /// operator << implementations
    friend std::ostream& operator<<( std::ostream& os, const Node& node );
    /// operator == implementations
    friend bool operator==( const Node& lhs, const Node& rhs );
    /// operator != implementations
    friend bool operator!=( const Node& lhs, const Node& rhs );
    /// non-templated class implementation, comparaison between a Particle and a Node
    template <class PARTICLE>
    friend bool operator==( const Node& lhs, const PARTICLE* const rhs );
    template <class PARTICLE>
    friend bool operator!=( const Node& lhs, const PARTICLE* const rhs ) {
      return !( lhs == rhs );
    }
    /// non-templated class implementation, comparaison between a Particle and a Node
    template <class PARTICLE>
    friend bool operator==( const PARTICLE* const lhs, const Node& rhs );
    template <class PARTICLE>
    friend bool operator!=( const PARTICLE* const lhs, const Node& rhs ) {
      return !( lhs == rhs );
    }

  private:
    /// check if node is marked with ^  (invoked in constructor)
    bool checkisMarked( const std::string& node_name ) const;
    /// check if node has CC (invoked in constructor)
    bool checkhasCC( const std::string& node_name ) const;
    /// prune the node from operators and returns the particle name only (invoked in constructor)
    detail::tuple_nodeprop_t pruneNodeName( const std::string& node_name ) const;
    /// get pid from name
    int getPidFromName( const std::string& node_name ) const;
    /// get pid from name
    std::string getNameFromPid( const int& pid ) const;
    /// get antiparticle pid
    int getAntiParticlePid( const int& pid ) const;

    /// node ID
    int m_pid;
    /// check if node has CC
    bool m_hasCC;
    /// node marked with ^
    bool m_isMarked;
    /// messaging service
    ServiceHandle<IMessageSvc> m_msgSvc{ "MessageSvc", "MessageSvcName" };
    /// particle property service
    //"LHCb::" prefix in string since its defined in namespace!
    ServiceHandle<LHCb::IParticlePropertySvc> m_ppSvc{ "LHCb::ParticlePropertySvc", "LHCb::ParticlePropertySvcName" };
  };
} // namespace Decay

namespace Decay {

  Node::Node() : m_pid{ 0 }, m_hasCC{ false }, m_isMarked{ false } {}

  Node::Node( const std::string& node_name ) {
    detail::tuple_nodeprop_t tuple_nodeprops = pruneNodeName( node_name );
    m_pid                                    = getPidFromName( std::get<0>( tuple_nodeprops ) );
    m_hasCC                                  = std::get<1>( tuple_nodeprops );
    m_isMarked                               = std::get<2>( tuple_nodeprops );
  }

  Node::Node( const detail::ptr_of_ptrv1& particle )
      : m_pid{ ( *particle.get() )->particleID().pid() }, m_hasCC{ false }, m_isMarked{ false } {}

  Node::Node( const detail::ptr_of_ptrv1mc& mcparticle )
      : m_pid{ ( *mcparticle.get() )->particleID().pid() }, m_hasCC{ false }, m_isMarked{ false } {}

  // constructor for Node with pid, hasCC and isMarked
  Node::Node( const int& pid, const bool& hasCC, const bool& isMarked )
      : m_pid{ pid }, m_hasCC{ hasCC }, m_isMarked{ isMarked } {}

  Node::Node( const Node& node ) { // copy constructor
    m_pid      = node.m_pid;
    m_hasCC    = node.m_hasCC;
    m_isMarked = node.m_isMarked;
  }

  Node& Node::operator=( const Node& node ) { // copy assignment operator
    if ( &node == this ) return *this;
    m_pid      = node.m_pid;
    m_hasCC    = node.m_hasCC;
    m_isMarked = node.m_isMarked;
    return *this;
  }

  Node Node::getChargeConjugate( bool reversehasCC ) const { // get charge conjugated node
    // make a copy of the current node
    Node node_cc{ *this };
    // set the pid of the antiparticle
    node_cc.setpid( getAntiParticlePid( m_pid ) );
    // reverse the hasCC flag (if requested) as it can help with
    // the recursive calling of operator==
    if ( reversehasCC ) node_cc.reversehasCC();
    return node_cc;
  }

  int Node::getAntiParticlePid( const int& pid ) const {
    // get the particle property of the node
    const auto* part_prop = m_ppSvc->find( LHCb::ParticleID( pid ) ); // ptr to LHCb::ParticleProperty
    if ( !part_prop )
      throw GaudiException{ "Could not interpret the particle ID " + std::to_string( pid ) +
                                ". Please check your descriptor.",
                            "Decay::Node::getChargeConjugate()", StatusCode::FAILURE };

    // get antiparticle pid of the current node
    const auto* part_prop_cc = part_prop->antiParticle();
    if ( !part_prop_cc )
      throw GaudiException{ "Could not find antiparticle of the particle with ID " + std::to_string( pid ) +
                                ". Please check your descriptor.",
                            "Decay::Node::getChargeConjugate()", StatusCode::FAILURE };

    return ( part_prop_cc->pid() ).pid();
  }

  bool Node::checkisMarked( const std::string& node_name ) const {
    // output true if the expression has a "^"
    return std::regex_search( node_name, std::regex{ "\\^" } ); // matches a hat
  };

  bool Node::checkhasCC( const std::string& node_name ) const {
    // output true if the expression has "CC"
    return std::regex_search( node_name, std::regex{ "C{2}" } ); // matches 2 C's in a row
  }

  /// returns a vector of node possibilities based on the CC property of the Node
  std::vector<Node> Node::getPossibilities() const {
    // make a empty vector of nodes
    std::vector<Node> vectorOfNodes;
    // set hasCC property to false for all possibilities because we want vector of "simple" nodes
    bool hasCC{ false };
    if ( m_hasCC ) {
      // if has cc reserve two nodes in memory
      vectorOfNodes.reserve( 2 );
      vectorOfNodes.emplace_back( m_pid, hasCC, m_isMarked );
      vectorOfNodes.emplace_back( getAntiParticlePid( m_pid ), hasCC, m_isMarked );
      return vectorOfNodes;
    }
    vectorOfNodes.reserve( 1 );
    vectorOfNodes.emplace_back( m_pid, hasCC, m_isMarked );
    return vectorOfNodes;
  }

  detail::tuple_nodeprop_t Node::pruneNodeName( const std::string& node_name ) const { // TODO: make sure this
                                                                                       // is well
                                                                                       // documented
    std::string pruned_node_name;
    bool        hasCC    = checkhasCC( node_name );
    bool        isMarked = checkisMarked( node_name );
    // cleans the nodes from operators and returns the particle name only
    if ( hasCC && isMarked ) {                 // if node has self CC and is marked
      std::regex  expression( "\\^"            // hat
                              "\\s*\\["        // any whitespaces and a square bracket
                              "\\s*(\\S+)\\s*" // any whitespace then one or more non-whitespace character(s)
                                               // that is matched "(\\S+)" then any whitespace
                              "\\]\\s*CC"      // closing square bracket then any whitespace and then a CC
       );
      std::smatch what;
      std::regex_search( node_name, what, expression );
      pruned_node_name = what.str( 1 );
    } else if ( hasCC && !isMarked ) {         // if node has self CC
      std::regex  expression( "\\["            // square bracket
                              "\\s*(\\S+)\\s*" // any whitespace then one or more non-whitespace character(s)
                                               // that is matched "(\\S+)" then any whitespace
                              "\\]\\s*CC"      // closing square bracket then any whitespace and then a CC
       );
      std::smatch what;
      std::regex_search( node_name, what, expression );
      pruned_node_name = what.str( 1 );
    } else if ( !hasCC && isMarked ) {         // if node is marked
      std::regex  expression( "\\^"            // hat
                              "\\s*(\\S+)\\s*" // any whitespace then one or more non-whitespace character(s)
                                               // that is matched "(\\S+)" then any whitespace
       );
      std::smatch what;
      std::regex_search( node_name, what, expression );
      pruned_node_name = what.str( 1 );
    } else {
      std::regex  expression( "^\\s*"  // any initial whitespaces
                              "(\\S+)" // matches particle name
                              "\\s*$"  // any final whitespaces
       );
      std::smatch what;
      std::regex_search( node_name, what, expression );
      pruned_node_name = what.str( 1 );
    }
    return std::make_tuple( pruned_node_name, hasCC, isMarked );
  }

  int Node::getPidFromName( const std::string& name ) const {
    const auto* part_prop = m_ppSvc->find( name ); // ptr to LHCb::ParticleProperty
    if ( !part_prop )
      throw GaudiException{ "Could not interpret the node name " + name + ". Please check your descriptor.",
                            "Decay::Node::getPidFromName()", StatusCode::FAILURE };
    return ( part_prop->pid() ).pid();
  }

  std::string Node::getNameFromPid( const int& pid ) const {
    const auto* part_prop = m_ppSvc->find( LHCb::ParticleID( pid ) ); // ptr to LHCb::ParticleProperty
    if ( !part_prop )
      throw GaudiException{ "Could not interpret the particle ID " + std::to_string( pid ) +
                                ". Please check your descriptor.",
                            "Decay::Node::getNameFromPid()", StatusCode::FAILURE };
    return part_prop->name();
  }

  std::ostream& operator<<( std::ostream& os, const Node& node ) {
    if ( node.m_hasCC ) {
      if ( node.m_isMarked )
        os << "^[" << node.name() << "]CC";
      else
        os << "[" << node.name() << "]CC";
    } else if ( node.m_isMarked )
      os << "^" << node.name();
    else
      os << node.name();
    // MsgStream log( m_msgSvc.get(), "Decay::Node" );
    // log << MSG::INFO << "Node: Defining boost regex: " << endmsg;
    return os;
  }

  bool operator==( const Node& lhs, const Node& rhs ) {
    if ( lhs.m_pid == rhs.m_pid ) return true;

    bool reversehasCC = true;
    if ( lhs.m_hasCC ) {
      // Return a copy of this node with charged conjugate node and hasCC flag set to false
      // hasCC flag has to be false otherwise stuck in infinite loop
      Node lhs_cc{ lhs.getChargeConjugate( reversehasCC ) };
      return rhs == lhs_cc;
    } else if ( rhs.m_hasCC ) {
      // Return a copy of this node with charged conjugate node and hasCC flag set to false
      //  hasCC flag has to be false otherwise stuck in infinite loop
      Node rhs_cc{ rhs.getChargeConjugate( reversehasCC ) };
      return lhs == rhs_cc;
    }
    return false;
  }

  bool operator!=( const Node& lhs, const Node& rhs ) { return !( lhs == rhs ); }

  template <class PARTICLE>
  bool operator==( const Node& lhs, const PARTICLE* const rhs ) {
    if ( lhs.m_pid == ( rhs->particleID() ).pid() ) return true;
    return false;
  }

  template <class PARTICLE>
  bool operator==( const PARTICLE* const lhs, const Node& rhs ) {
    if ( ( lhs->particleID() ).pid() == rhs.m_pid ) return true;
    return false;
  }

} // namespace Decay

#endif // NODE_H
