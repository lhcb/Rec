###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import GaudiPython
import pytest
from Configurables import DDDBConf, MessageSvc
from GaudiPython.Bindings import AppMgr

Decay = GaudiPython.gbl.Decays.Decay


@pytest.fixture(scope="module")
def toolsvc():
    MessageSvc(OutputLevel=1)
    DDDBConf()
    app = AppMgr()
    toolsvc = app.toolSvc()
    app.initialize()
    return toolsvc


@pytest.fixture
def decoder(toolsvc):
    return toolsvc.create(
        "DecodeSimpleDecayString", interface=GaudiPython.gbl.IDecodeSimpleDecayString
    )


def test_descriptor_intact(decoder):
    descriptor = "[B+ -> K+ pi- mu+ e- p+ gamma pi0]cc"

    decoder.setDescriptor(descriptor)
    dec = Decay()

    sc = decoder.getDecay(dec)
    print(dec, ":", sc.message())
    assert sc
    sc = decoder.getDecay_cc(dec)
    print(dec, ":", sc.message())
    assert sc


def test_descriptor_bad_daughter(decoder):
    descriptor = "[B+ -> K+ pi- mu+ e- unicorn gamma pi0]cc"

    with pytest.raises(GaudiPython.gbl.GaudiException):
        decoder.setDescriptor(descriptor)


def test_descriptor_bad_mother(decoder):
    descriptor = "[squirrel -> K+ pi- mu+ e- p+ gamma pi0]cc"

    with pytest.raises(GaudiPython.gbl.GaudiException):
        decoder.setDescriptor(descriptor)
