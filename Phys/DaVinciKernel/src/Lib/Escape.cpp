/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "Kernel/Escape.h"
#include "boost/regex.hpp"
// ============================================================================
/*  Replace all special characters from particle name
 */
// ============================================================================
namespace Decays {

  std::string escape( const std::string& input ) {

    const int size = input.size();
    if ( size == 0 ) return "";

    // all chars must be matched, or they fall in the catchall
    const char* expr = "([[:word:]])" // 1 : allowed chars
                       "|^(~)"        // 2 : starting ~
                       "|([~])"       // 3 : then ok
                       "|(\\*)"       // 4 : star
                       "|([+])"       // 5 : +
                       "|([-])"       // 6 : -
                       "|(.)";        // 7 : default catchall.

    const char* replace = "(?1$1)"
                          "(?2anti)"
                          "(?3~)"
                          "(?4st)"
                          "(?5plus)"
                          "(?6minus)"
                          "(?7_)";

    std::string  ret;
    boost::regex e( expr );
    ret = boost::regex_replace( input, e, replace, boost::match_default | boost::format_all );

    // remove double __ and trailing and leading _
    e.assign( "(^_+)|(_+$)|(_{2,})" );
    ret = boost::regex_replace( ret, e, "(?1)(?2)(?3_)", boost::match_default | boost::format_all );

    return ret;
  }
  // ==========================================================================
} // end of namespace Decays
// ============================================================================
// The END
// ============================================================================
