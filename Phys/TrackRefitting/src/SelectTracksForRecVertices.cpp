/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 * Creates a track selection with references to the tracks that
 * composed the primary vertices.
 *
 * @author Laurent Dufour
 */

#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiKernel/ISvcLocator.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  template <class TrackContainer>
  void addTracks( LHCb::RecVertex const& vertex, TrackContainer& tracks ) {
    for ( const auto& t : vertex.tracks() )
      if ( tracks.index( t ) < 0 ) tracks.insert( t );
  }

  using Output_t      = LHCb::Track::Selection;
  using In_t          = Gaudi::Functional::vector_of_const_<LHCb::RecVertices>;
  using Transformer_t = LHCb::Algorithm::MergingTransformer<Output_t( In_t const& )>;
} // namespace

namespace LHCb {
  class SelectTracksForRecVertices final : public Transformer_t {
  public:
    mutable Gaudi::Accumulators::AveragingCounter<> m_tracksPerEvent{ this, "Average # tracks selected per event" };

    SelectTracksForRecVertices( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer_t( name, pSvcLocator, { "Inputs", {} }, { KeyValue{ "OutputTracks", "" } } ) {}

    Output_t operator()( In_t const& lists ) const override {
      Output_t outputTracks{};

      for ( const auto& pvs : lists ) {
        for ( const auto& pv : pvs ) { addTracks( *pv, outputTracks ); }
      }

      m_tracksPerEvent += outputTracks.size();

      return outputTracks;
    }
  };

  DECLARE_COMPONENT_WITH_ID( SelectTracksForRecVertices, "SelectTracksForRecVertices" )
} // namespace LHCb
