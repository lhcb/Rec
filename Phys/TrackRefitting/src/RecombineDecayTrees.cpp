/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 * Recombines tracks together to the same DecayTree as is provided,
 * using a relation table.
 *
 * The primary use-case for this is track refitting, in which the
 * tracks themselves have been refitted and again the decay tree
 * (e.g. Bs -> J/psi phi) must be reconstructed through a refit
 * of the vertices.
 *
 * The particles created are *new* particles, by design:
 * The result of the vertex fit can be very different from what
 * the original particle had, and it can even fail.
 *
 * The track *refitting* itself is delegated outside of this algorithm,
 * and the overlap based on LHCbIDs is used to associate the tracks.
 * This has as benefit that this configuration is only done in one place,
 * but does make the overall process a bit slower (as this overlap computation
 * has to happen).
 *
 * The output also contains a relation table of the original particle
 * to the new.
 *
 * There is one true particle container created: it contains all particles
 * that are constructed with the new protos. Another Particle::Selection is
 * provided, which contains only the heads of the decay trees.
 * In practice, the users of this tool are probably most interested in
 * this latter container, "OutputParticles", coming out of this algorithm.
 *
 * When provided with a non-empty list of PVs, also the PV association is
 * repeated for the daughters. This updates the bestPV pointer of the
 * particle objects.
 *
 * @author Laurent Dufour
 */
#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiKernel/ISvcLocator.h"

#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "Kernel/IParticle2State.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/ISelectiveBremAdder.h"

#include "SelTools/Utilities.h"

#include "TrackKernel/PrimaryVertexUtils.h"

namespace {
  using OutputRelationTable   = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;
  using WeightedRelationTable = LHCb::RelationWeighted1D<LHCb::Particle, LHCb::Track, float>;

  using Output_t = std::tuple<LHCb::Particle::Selection, LHCb::Particle::Container, LHCb::ProtoParticles,
                              LHCb::Vertices, OutputRelationTable, OutputRelationTable>;
  using MultiTransformer_t =
      LHCb::Algorithm::MultiTransformer<Output_t( LHCb::Particle::Range const&, LHCb::PrimaryVertices const&,
                                                  LHCb::Track::Range const&, WeightedRelationTable const&,
                                                  DetectorElement const& ),
                                        LHCb::Algorithm::Traits::usesConditions<DetectorElement>>;
} // namespace

namespace LHCb {
  class RecombineDecayTrees final : public MultiTransformer_t {
  public:
    RecombineDecayTrees( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer_t( name, pSvcLocator,
                              { KeyValue{ "InputParticles", "" }, KeyValue{ "InputPVs", "" },
                                KeyValue{ "InputTracks", "" }, KeyValue{ "InputTrackRelations", "" },
                                KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                              {
                                  KeyValue{ "OutputParticles", "" },
                                  KeyValue{ "OutputChildren", "" },
                                  KeyValue{ "OutputProtoParticles", "" },
                                  KeyValue{ "OutputVertices", "" },
                                  KeyValue{ "OutputRelationsOldToNew", "" },
                                  KeyValue{ "OutputRelationsNewToOld", "" },
                              } ) {}

    Output_t operator()( LHCb::Particle::Range const& decays, LHCb::PrimaryVertices const& pvs,
                         LHCb::Track::Range const& refittedTracks, WeightedRelationTable const& trackRelations,
                         DetectorElement const& lhcb ) const override {
      Output_t               myOutput{};
      std::set<unsigned int> keys;

      auto& outputDecays            = std::get<0>( myOutput );
      auto& outputParticleContainer = std::get<1>( myOutput );
      auto& outputProtoParticles    = std::get<2>( myOutput );
      auto& outputVertices          = std::get<3>( myOutput );
      auto& outputRelationsOldToNew = std::get<4>( myOutput );
      auto& outputRelationsNewToOld = std::get<5>( myOutput );

      for ( const auto& decay : decays ) {
        LHCb::Particle* newDecay = nullptr;

        // special case: we encounter a basic particle ( no need to build any vertex )
        if ( decay->isBasicParticle() ) {
          newDecay = exchangeBasicParticle( decay, pvs, refittedTracks, trackRelations, outputParticleContainer,
                                            outputProtoParticles, outputRelationsOldToNew, outputRelationsNewToOld );
        } else {
          newDecay = exchangeDecaytree( decay, pvs, refittedTracks, trackRelations, lhcb, outputParticleContainer,
                                        outputProtoParticles, outputVertices, outputRelationsOldToNew,
                                        outputRelationsNewToOld );
        }

        outputDecays.insert( newDecay );
      }

      return myOutput;
    }

  private:
    LHCb::Particle* exchangeDecaytree( LHCb::Particle const* decayTree, LHCb::PrimaryVertices const& pvs,
                                       LHCb::Track::Range const&    refittedTracks,
                                       WeightedRelationTable const& trackRelations, DetectorElement const& lhcb,
                                       LHCb::Particles& particleContainer, LHCb::ProtoParticles& outputProtoParticles,
                                       LHCb::Vertices& outputVertices, OutputRelationTable& outputRelationsOldToNew,
                                       OutputRelationTable& outputRelationsNewToOld ) const {
      LHCb::Particle* newParticle = decayTree->clone();
      LHCb::Vertex*   newVertex   = decayTree->endVertex()->clone();

      LHCb::Particle::ConstVector daughters;

      for ( const auto& daughter : decayTree->daughters() ) {
        if ( daughter->isBasicParticle() ) {
          auto newDaughter =
              exchangeBasicParticle( daughter, pvs, refittedTracks, trackRelations, particleContainer,
                                     outputProtoParticles, outputRelationsOldToNew, outputRelationsNewToOld );
          daughters.push_back( newDaughter );
        } else {
          auto newDaughter = exchangeDecaytree( daughter, pvs, refittedTracks, trackRelations, lhcb, particleContainer,
                                                outputProtoParticles, outputVertices, outputRelationsOldToNew,
                                                outputRelationsNewToOld );
          daughters.push_back( newDaughter );
        }
      }

      auto fit_success = m_combiner->combine( daughters, *newParticle, *newVertex, *lhcb.geometry() ).isSuccess();
      m_n_passed_vertex_fit += fit_success;

      if ( fit_success ) { Sel::Utils::setPVForComposite( *newParticle, newVertex, pvs ); }

      particleContainer.insert( newParticle );
      outputVertices.insert( newVertex );

      outputRelationsOldToNew.relate( decayTree, newParticle ).ignore();
      outputRelationsNewToOld.relate( newParticle, decayTree ).ignore();

      return newParticle;
    }

    LHCb::Particle* exchangeBasicParticle( LHCb::Particle const* particle, LHCb::PrimaryVertices const& pvs,
                                           LHCb::Track::Range const& /* implicitly used by relation table? */,
                                           WeightedRelationTable const& trackRelations,
                                           LHCb::Particles&             particleContainer,
                                           LHCb::ProtoParticles&        outputProtoParticles,
                                           OutputRelationTable&         outputRelationsOldToNew,
                                           OutputRelationTable&         outputRelationsNewToOld ) const {
      LHCb::Particle* newParticle = particle->clone();

      // For particles without a track, such as neutrals,
      // we don't do anything.
      if ( !particle->proto() || !particle->proto()->track() ) {
        particleContainer.insert( newParticle );

        outputRelationsOldToNew.relate( particle, newParticle ).ignore();
        outputRelationsNewToOld.relate( newParticle, particle ).ignore();

        return newParticle;
      }

      // see if the particle has a reference
      float              bestWeight    = 0.0;
      LHCb::Track const* refittedTrack = nullptr;

      for ( const auto& relation : trackRelations.relations( particle ) ) {
        if ( relation.weight() > bestWeight ) {
          refittedTrack = relation.to();
          bestWeight    = relation.weight();
        }
      }

      if ( bestWeight < m_weightThreshold || refittedTrack == nullptr ) {
        ++m_msg_could_not_find_track;

        particleContainer.insert( newParticle );

        outputRelationsOldToNew.relate( particle, newParticle ).ignore();
        outputRelationsNewToOld.relate( newParticle, particle ).ignore();

        return newParticle;
      }

      LHCb::ProtoParticle* newProto = particle->proto()->clone();
      newProto->setTrack( refittedTrack );

      newParticle->setProto( newProto );

      // Find the appropriate state and use it to define this object's kinematics
      const LHCb::State* state = Sel::Utils::defaultStateForParticle( *refittedTrack );
      // Backup
      if ( !state ) { state = &( refittedTrack->firstState() ); }

      m_particle_from_state_tool->state2Particle( *state, *newParticle )
          .orElse( [&] {
            ++m_fail_to_fill_from_state;
            // do nothing - in this case, we fall back to the original
          } )
          .ignore();

      // When the old particle had bremsstrahlung added, add it again
      if ( Sel::Utils::hasBrem( *particle ) ) {
        // Need to force, as the particle still has the flag
        // from before
        auto success = m_bremAdder->addBrem( *newParticle, true );

        if ( success )
          ++m_n_addedBrems;
        else
          ++m_n_failedBrem;
      }

      // When we are given a non-empty PV container, again run the association;
      // as we could have re-fitted the PVs as well.
      if ( pvs.size() ) {
        const auto& pv = LHCb::bestPV( pvs, newParticle->referencePoint(), newParticle->momentum() );
        if ( pv ) newParticle->setPV( pv );
      }

      outputProtoParticles.insert( newProto );
      particleContainer.insert( newParticle );
      outputRelationsOldToNew.relate( particle, newParticle ).ignore();
      outputRelationsNewToOld.relate( newParticle, particle ).ignore();

      return newParticle;
    }

    Gaudi::Property<float> m_weightThreshold{ this, "WeightThreshold", 0.85 };

    ToolHandle<IParticle2State>     m_particle_from_state_tool = { this, "Particle2StateTool", "Particle2State" };
    ToolHandle<ISelectiveBremAdder> m_bremAdder{ this, "BremAdder", "SelectiveBremAdder" };
    ToolHandle<IParticleCombiner>   m_combiner{ this, "ParticleCombiner", "ParticleVertexFitter" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_could_not_find_track{
        this, "Could not find matching track in relation table for particle" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_fail_to_fill_from_state{
        this, "Failed to fill Particle from State" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_n_failedBrem{
        this, "Failed to re-add bremsstrahlung to particle" };
    mutable Gaudi::Accumulators::Counter<>     m_n_addedBrems{ this, "# Re-added the bremsstrahlung to particle" };
    mutable Gaudi::Accumulators::StatCounter<> m_n_passed_vertex_fit{ this, "# passed vertex fit" };
  };

  DECLARE_COMPONENT_WITH_ID( RecombineDecayTrees, "RecombineDecayTrees" )
} // namespace LHCb
