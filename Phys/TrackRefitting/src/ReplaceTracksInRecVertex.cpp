/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 * Replaces tracks in a vertex using the provided
 * relation table, and refits using the provided fitter
 * tool.
 *
 * The primary use-case for this is track refitting, in which the
 * tracks themselves have been refitted (with an updated alignment),
 * and the PVs must be updated through a refit of the vertices.
 *
 * The algorithm presented here takes RecVertices as input, which primarily
 * exist within the offline enviroment.
 *
 * The output also contains a relation table of the original PV
 * to the new, and vice versa.
 *
 * @author Laurent Dufour
 */
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "GaudiKernel/ISvcLocator.h"

#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation2D.h"
#include "Relations/RelationWeighted1D.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "TrackInterfaces/IPVFitter.h"
#include "TrackKernel/PrimaryVertexUtils.h"

#include "Kernel/STLExtensions.h"

namespace {
  using OutputRelationTableRecVertex = LHCb::Relation2D<LHCb::RecVertex, LHCb::RecVertex>;
  using Output_RV_t                  = std::tuple<LHCb::RecVertices, OutputRelationTableRecVertex>;

  using WeightedRelationTable = LHCb::RelationWeighted1D<LHCb::Track, LHCb::Track, float>;

  using MultiTransformer_RV_t =
      LHCb::Algorithm::MultiTransformer<Output_RV_t( LHCb::RecVertices const&, LHCb::Track::Range const&,
                                                     WeightedRelationTable const&, DetectorElement const& ),
                                        LHCb::DetDesc::usesConditions<DetectorElement>>;

  std::pair<LHCb::Track const*, float> associatedTrack( LHCb::Track const*           track,
                                                        WeightedRelationTable const& trackRelations ) {
    // see if the track has a reference
    float              bestWeight    = 0.0;
    LHCb::Track const* refittedTrack = nullptr;

    for ( const auto& relation : trackRelations.relations( track ) ) {
      if ( relation.weight() > bestWeight ) {
        refittedTrack = relation.to();
        bestWeight    = relation.weight();
      }
    }

    return { refittedTrack, bestWeight };
  }
} // namespace

namespace LHCb {
  /** AOS variant **/
  class ReplaceTracksInRecVertex final : public MultiTransformer_RV_t {

  public:
    ReplaceTracksInRecVertex( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer_RV_t( name, pSvcLocator,
                                 { KeyValue{ "InputVertices", "" }, KeyValue{ "InputTracks", "" },
                                   KeyValue{ "InputTrackRelations", "" },
                                   KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                                 {
                                     KeyValue{ "OutputVertices", "" },
                                     KeyValue{ "OutputRelations", "" },
                                 } ) {}

    Output_RV_t operator()( LHCb::RecVertices const&     pvs, LHCb::Track::Range const&,
                            WeightedRelationTable const& trackRelations, DetectorElement const& lhcb ) const override {
      Output_RV_t myOutput{};

      // used for refitting the PVs later
      // has to be non-const, so cannot use constexpr...
      std::vector<LHCb::Track const*> emptyTrackList;

      auto& outputVertices    = std::get<0>( myOutput );
      auto& outputRelations2D = std::get<1>( myOutput );

      outputVertices.reserve( pvs.size() );

      for ( const auto& pv : pvs ) {
        LHCb::RecVertex::TrackWithWeightVector weightVector;

        auto newPV = std::make_unique<RecVertex>( *pv );
        newPV->clearTracks();

        for ( const auto& [pTrack, pv_track_weight] : pv->tracksWithWeights() ) {
          const auto& [trackTo, associationWeight] = associatedTrack( pTrack, trackRelations );
          if ( !trackTo || associationWeight < m_weightThreshold.value() ) {
            // NOTE: This could go wrong - we do not check for the
            // number of tracks left in the PV...

            ++m_msg_could_not_find_track;
            continue;
          }

          weightVector.push_back( { trackTo, pv_track_weight } );
        }

        newPV->setTracksWithWeights( weightVector );

        if ( m_refitPV.value() ) {
          // FIXME: there should be some more elegant way to give a span
          // of 'the first part' of pairs in a vector?
          std::vector<const LHCb::Track*> pv_tracks;
          pv_tracks.reserve( weightVector.size() );
          std::transform( weightVector.begin(), weightVector.end(), std::back_inserter( pv_tracks ),
                          []( const auto& track_weight_pair ) { return track_weight_pair.first; } );

          StatusCode fitResult = m_fitter->fitVertex( newPV->position(), pv_tracks, *( newPV.get() ), emptyTrackList,
                                                      *( lhcb.geometry() ) );

          if ( !fitResult.isSuccess() ) {
            if ( m_onlyKeepConverged.value() ) {
              ++m_msg_pv_fit_fail_and_removed;
              continue;
            } else
              ++m_msg_pv_fit_fail_but_kept;
          }
        }

        // insert and relate.
        // you need the pointer both to release,
        // and only then you can make the relation
        auto* newPVPtr = newPV.release();
        outputVertices.insert( newPVPtr );
        StatusCode relationResult = outputRelations2D.relate( newPVPtr, pv );

        if ( !relationResult.isSuccess() ) { ++m_msg_relation_add_fail; }
      }

      return myOutput;
    }

  private:
    ToolHandle<IPVFitter> m_fitter{ this, "PVFitter", "RecVertexAsExtendedPrimaryVertexFitter" };

    Gaudi::Property<float> m_weightThreshold{ this, "WeightThreshold", 0.85 };
    Gaudi::Property<bool>  m_refitPV{ this, "ReFitPV", true };

    // false is the default behaviour of HLT2
    Gaudi::Property<bool> m_onlyKeepConverged{ this, "OnlyKeepConvergedFits", false };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_could_not_find_track{
        this, std::string( "Could not find matching track in relation table for PV track. " ) +
                  "This can happen now-and-then if you are refitting tracks with a drastically different alignment, "
                  "and the track fit failed." };

    mutable Gaudi::Accumulators::MsgCounter<MSG::INFO> m_msg_pv_fit_fail_but_kept{
        this, "PV fit failed, but the vertex is kept nonetheless (behaviour as HLT2)", 1 };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_pv_fit_fail_and_removed{
        this, "Refitted PV fit failed, removed the vertex from the container as OnlyKeepConvergedFits is enabled" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_msg_relation_add_fail{ this,
                                                                                 "Adding of relationship failed" };
  };

  DECLARE_COMPONENT_WITH_ID( ReplaceTracksInRecVertex, "ReplaceTracksInRecVertex" )
} // namespace LHCb
