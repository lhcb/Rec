/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 * Creates a track selection with references to the tracks that
 * composed the input particles, along with relations between
 * the particle and the track.
 *
 * Example use-case: get the tracks from a particle to explicitly
 * include / remove them from a PV fit, or refit the tracks
 * with a different B-field map.
 *
 * For any intended manipulations on the level of particles,
 * such as refitting the decay tree, the ParticleRefitter
 * should be used instead, as the relation table
 * provided does not carry over trivially to e.g. vertex fitting...
 *
 * One could further generalise the class to also support
 * proto particles, or different particle containers, but
 * mind that one has to work with keyed containers for the
 * relation tables to work.
 *
 * @author Laurent Dufour
 */

#include "Event/Particle.h"
#include "Event/Track.h"
#include "GaudiKernel/ISvcLocator.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "LoKi/PhysExtract.h"
#include "Relations/Relation1D.h"

namespace {
  using RelationTable = LHCb::Relation1D<LHCb::Particle, LHCb::Track>;

  template <class TrackContainer>
  void addTracksAndRelate( LHCb::Particle::Range const& particles, TrackContainer& tracks, RelationTable& relations ) {
    // make sure the list of particles includes the children

    LHCb::Particle::ConstVector basicParticles;
    for ( const auto& p : particles )
      LoKi::Extract::particles( p, std::back_inserter( basicParticles ),
                                [&]( const auto& p ) { return p->isBasicParticle() && p->proto(); } );

    for ( const auto& particle : basicParticles ) {
      if ( particle->proto()->track() ) {
        // see if the track is already in the container
        if ( tracks.index( particle->proto()->track() ) < 0 ) tracks.insert( ( particle->proto()->track() ) );

        relations.relate( particle, ( particle->proto()->track() ) ).ignore();
      }
    }
  }

  using Output_t                  = std::tuple<LHCb::Track::Selection, RelationTable>;
  using In_t                      = Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>;
  using MergingMultiTransformer_t = LHCb::Algorithm::MergingMultiTransformer<Output_t( In_t const& )>;
} // namespace

namespace LHCb {
  class SelectTracksForParticles final : public MergingMultiTransformer_t {
  public:
    mutable Gaudi::Accumulators::AveragingCounter<> m_tracksPerEvent{ this, "Average tracks copied per event" };

    SelectTracksForParticles( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingMultiTransformer_t( name, pSvcLocator, { "Inputs", {} },
                                     { KeyValue{ "OutputTracks", "" }, KeyValue{ "Relations", "" } } ) {}

    Output_t operator()( In_t const& lists ) const override {
      Output_t myOutput{};

      auto& [outputTracks, relationTable] = myOutput;

      for ( const auto& particle_list : lists ) { addTracksAndRelate( particle_list, outputTracks, relationTable ); }
      m_tracksPerEvent += outputTracks.size();

      return myOutput;
    }
  };

  DECLARE_COMPONENT_WITH_ID( SelectTracksForParticles, "SelectTracksForParticles" )
} // namespace LHCb
