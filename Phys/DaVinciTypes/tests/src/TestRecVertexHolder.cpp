/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RecVertex.h"
#include "Kernel/RecVertexHolder.h"
#include "LHCbAlgs/Producer.h"

namespace LHCb {

  /**
   *  Tests for RecVertexHolder class.
   *  Creates some dummy PVs, puts them on the TES,
   *  makes RecVertexHolders with them, tests them.
   *
   *  @author Juan Palacios
   *  @date   2010-12-06
   */
  class TestRecVertexHolder : public Algorithm::Producer<RecVertex::Container()> {
  public:
    TestRecVertexHolder( const std::string& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, { "ClonePVLocation", "Rec/Vertex/ClonePVs" } ){};

    RecVertex::Container operator()() const override;

  private:
    RecVertexHolder       clonePV( const RecVertex* pv ) const;
    const RecVertexHolder returnPV( const RecVertex* pv ) const;
    const RecVertexHolder createPV() const;
    void                  testRecVertexHolder( const RecVertexHolder pv ) const;
    bool                  inTES( const RecVertex* PV ) const;
  };

  DECLARE_COMPONENT_WITH_ID( TestRecVertexHolder, "TestRecVertexHolder" )

} // namespace LHCb

LHCb::RecVertex::Container LHCb::TestRecVertexHolder::operator()() const {
  info() << "Creating dummy RecVertices" << endmsg;
  RecVertex::Container pvs{};
  for ( int i = 0; i < 10; i++ ) { pvs.insert( new RecVertex() ); }
  info() << "Found " << pvs.size() << " PVs" << endmsg;
  info() << "Clone and test PVs" << endmsg;
  RecVertex::Container clonePVs{};
  for ( auto const& pv : pvs ) {
    const RecVertexHolder pvHolder = clonePV( pv );
    if ( !inTES( pvHolder ) ) { clonePVs.insert( const_cast<RecVertex*>( pvHolder.vertex() ) ); }
  }
  return clonePVs;
}

LHCb::RecVertexHolder LHCb::TestRecVertexHolder::clonePV( const LHCb::RecVertex* pv ) const {
  return RecVertexHolder( new RecVertex( *pv ) );
}

const LHCb::RecVertexHolder LHCb::TestRecVertexHolder::returnPV( const LHCb::RecVertex* pv ) const {
  return RecVertexHolder( pv );
}

const LHCb::RecVertexHolder LHCb::TestRecVertexHolder::createPV() const { return RecVertexHolder( new RecVertex() ); }

void LHCb::TestRecVertexHolder::testRecVertexHolder( [[maybe_unused]] const LHCb::RecVertexHolder pvHolder ) const {
  // test cast to RecVertex.
  info() << "Test cast to RecVertex*" << endmsg;
  assert( (RecVertex const*)pvHolder == pvHolder.vertex() );
  // test -> operator
  info() << "Test operator ->" << endmsg;
  assert( pvHolder->isPrimary() == pvHolder.vertex()->isPrimary() );
  info() << "Test null ptr and operator!()" << endmsg;
  assert( pvHolder->technique() == pvHolder.vertex()->technique() );
  assert( !RecVertexHolder() );
}

bool LHCb::TestRecVertexHolder::inTES( const LHCb::RecVertex* PV ) const { return ( 0 != PV && 0 != PV->parent() ); }
