###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import TestRecVertexHolder
from PyConf.application import ApplicationOptions, configure, configure_input
from PyConf.control_flow import CompositeNode

options = ApplicationOptions(_enabled=False)
options.dddb_tag = "run3/trunk"
options.conddb_tag = "master"
options.geometry_version = "run3/trunk"
options.conditions_version = "master"
options.input_type = "NONE"
options.evt_max = 10
options.simulation = False
config = configure_input(options)

config.update(
    configure(
        options,
        CompositeNode("test", [TestRecVertexHolder(name="TestRecVertexHolder")]),
    )
)
