###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciTypes
-----------------
#]=======================================================================]

gaudi_add_library(DaVinciTypesLib
    SOURCES
        src/DecayTree.cpp
        src/HashIDs.cpp
        src/RecVertexHolder.cpp
        src/TreeCloners.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::PhysEvent
            LHCb::RecEvent
            LHCb::RelationsLib
            LHCb::TrackEvent
)

gaudi_add_module(DaVinciTypes
    SOURCES
        tests/src/TestRecVertexHolder.cpp
    LINK
        DaVinciTypesLib
        Gaudi::GaudiAlgLib
        LHCb::RecEvent
)

gaudi_add_dictionary(DaVinciTypesDict
    HEADERFILES dict/DaVinciTypesDict.h
    SELECTION dict/DaVinciTypes.xml
    LINK DaVinciTypesLib
)

gaudi_add_tests(QMTest)
