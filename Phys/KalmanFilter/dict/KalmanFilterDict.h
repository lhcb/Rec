/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef DICT_KALMANFILTERDICT_H
#  define DICT_KALMANFILTERDICT_H 1
// ============================================================================
// Include files
// ============================================================================
#  include "KalmanFilter/ErrorCodes.h"
#  include "KalmanFilter/FastVertex.h"
#  include "KalmanFilter/ParticleTypes.h"
#  include "KalmanFilter/VertexFit.h"
#  include "KalmanFilter/VertexFitWithTracks.h"
#  include "Kernel/IParticleClassifier.h"
// ============================================================================
/** @file
 *  Helper file to build Reflex dictionry for Kalman filter package
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   2011-01-13
 */
// ============================================================================
namespace {
  // ==========================================================================
  struct KalmanFilter_Instantiations {
    // ========================================================================
    LoKi::KalmanFilter::Entries    m_entries;
    LoKi::KalmanFilter::TrEntries4 m_entries4;
    // ========================================================================
  };
  // ==========================================================================
} // namespace
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // DICT_KALMANFILTERDICT_H
// ============================================================================
