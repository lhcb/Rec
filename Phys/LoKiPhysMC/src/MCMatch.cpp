/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKiPhysMC
// ============================================================================
#include "LoKi/MCMatch.h"
#include "LoKi/MCMatchObj.h"
// ============================================================================
/** @file
 *
 * Implementation file for class LoKi::MCMatch
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-11
 */
// ============================================================================
/// Standard constructor from the object and reporter
// ============================================================================
LoKi::MCMatch::MCMatch( const LoKi::MCMatchObj* object ) : LoKi::Interface<LoKi::MCMatchObj>( object ) {}
// ============================================================================
/// destructor
// ============================================================================
LoKi::MCMatch::~MCMatch() {}
// ============================================================================
/// implicit conversion to the pointer
// ============================================================================
LoKi::MCMatch::operator const LoKi::MCMatchObj*() const { return getObject(); }
// ============================================================================
// The END
// ============================================================================
