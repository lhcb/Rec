/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKiPhysMC
// ============================================================================
#include "LoKi/PhysMCParticles.h"
#include "LoKi/MCMatch.h"
#include "LoKi/MCMatchObj.h"
// ============================================================================
/** @file
 *
 *  Implementation file for fuctions form the file LoKi/PhysMCParticles.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-13
 */
// ============================================================================

// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param particle  pointer to particle object
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LHCb::Particle* particle )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( particle )
    , m_match( match ) {}
// ============================================================================
/*  constructor a
 *  @param match MCMatch object (working horse)
 *  @param range "container" of particles
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LoKi::Types::Range& range )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range container of particles
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LHCb::Particle::Vector& range )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range container of particles
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LHCb::Particle::ConstVector& range )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range container of particles
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LoKi::Keeper<LHCb::Particle>& range )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range container of particles
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::MCMatch& match, const LoKi::UniqueKeeper<LHCb::Particle>& range )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param particle  pointer to particle object
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LHCb::Particle* particle, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( particle )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param range "container" of particles
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::Types::Range& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param range container of particles
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LHCb::Particle::Vector& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param range container of particles
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LHCb::Particle::ConstVector& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param range container of particles
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::Keeper<LHCb::Particle>& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param range container of particles
 *  @param match MCMatch object (working horse)
 */
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::UniqueKeeper<LHCb::Particle>& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
    , LoKi::Keeper<LHCb::Particle>( range )
    , m_match( match ) {}
// ============================================================================
//  copy constructor
// ============================================================================
LoKi::PhysMCParticles::RCTruth::RCTruth( const LoKi::PhysMCParticles::RCTruth& right )
    : LoKi::AuxFunBase( right )
    , LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate( right )
    , LoKi::Keeper<LHCb::Particle>( right )
    , m_match( right.m_match ) {}
// ============================================================================
//  MANDATORY : virtual destructor destructor
// ============================================================================
LoKi::PhysMCParticles::RCTruth::~RCTruth() {}
// ============================================================================
//  MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::PhysMCParticles::RCTruth* LoKi::PhysMCParticles::RCTruth::clone() const { return new RCTruth( *this ); }
// ============================================================================
//  MANDATORY: the only one essential method ("function")
// ============================================================================
LoKi::PhysMCParticles::RCTruth::result_type
LoKi::PhysMCParticles::RCTruth::operator()( LoKi::PhysMCParticles::RCTruth::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid Particle! return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  if ( empty() ) {
    Warning( "Empty container of RC, return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  if ( !m_match.validPointer() ) {
    Error( "MCMatchObj* is invalid! return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  //
  return end() != m_match->match( begin(), end(), p );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::PhysMCParticles::RCTruth::fillStream( std::ostream& s ) const { return s << "RCTRUTH"; }
// ============================================================================

// ============================================================================
// The END
// ============================================================================
