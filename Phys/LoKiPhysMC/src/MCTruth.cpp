/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKiPhysMC
// ============================================================================
#include "LoKi/MCTruth.h"
#include "LoKi/MCMatch.h"
#include "LoKi/MCMatchObj.h"
// ============================================================================
/** @file
 *
 *  Implementation file for fuctions form the file LoKi/PhysMCParticles.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-13
 */
// ============================================================================
namespace {
  // ==========================================================================
  const LoKi::MCMatchObj* const s_MCMATCH = 0;
  // ==========================================================================
} // namespace
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param particle  pointer to MC particle object
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LHCb::MCParticle* particle )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( particle )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LoKi::MCTypes::MCRange& range )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LHCb::MCParticle::Vector& range )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LHCb::MCParticle::ConstVector& range )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LoKi::Keeper<LHCb::MCParticle>& range )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCMatch& match, const LoKi::UniqueKeeper<LHCb::MCParticle>& range )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param particle  pointer to MC particle object
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LHCb::MCParticle* particle, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( particle )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::MCTypes::MCRange& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LHCb::MCParticle::Vector& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LHCb::MCParticle::ConstVector& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range.begin(), range.end() )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::Keeper<LHCb::MCParticle>& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range )
    , m_match( match ) {}
// ============================================================================
/*  constructor
 *  @param match MCMatch object (working horse)
 *  @param range range of MC particles
 */
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::UniqueKeeper<LHCb::MCParticle>& range, const LoKi::MCMatch& match )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::Keeper<LHCb::MCParticle>( range )
    , m_match( match ) {}
// ============================================================================
//  copy constructor
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth( const LoKi::PhysMCParticles::MCTruth& right )
    : LoKi::AuxFunBase( right )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Predicate( right )
    , LoKi::Keeper<LHCb::MCParticle>( right )
    , m_match( right.m_match ) {}
// ============================================================================
//  MANDATORY: virual destructor
// ============================================================================
LoKi::PhysMCParticles::MCTruth::~MCTruth() {}
// ============================================================================
//  MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::PhysMCParticles::MCTruth* LoKi::PhysMCParticles::MCTruth::clone() const { return new MCTruth( *this ); }
// ============================================================================
//  MANDATORY: the only one essential method ("function")
// ============================================================================
bool LoKi::PhysMCParticles::MCTruth::match( LoKi::PhysMCParticles::MCTruth::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid Particle! return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  if ( empty() ) {
    Warning( "Empty container of MC, return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  if ( !m_match.validPointer() ) {
    Error( "LoKi::MCMatch is invalid! return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  if ( m_match->empty() ) {
    Warning( "Empty list of Relation Tables, return 'False'" ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    return false; // RETURN
  }
  //
  return end() != m_match->match( p, begin(), end() );
}
// ============================================================================
//  OPTIONAL: the specific printout
// ============================================================================
std::ostream& LoKi::PhysMCParticles::MCTruth::fillStream( std::ostream& s ) const { return s << "MCTRUTH"; }
// ============================================================================
// protected defauls contructor
// ============================================================================
LoKi::PhysMCParticles::MCTruth::MCTruth()
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate(), LoKi::Keeper<LHCb::MCParticle>(), m_match( s_MCMATCH ) {}
// ============================================================================
// set new mc match object
// ============================================================================
void LoKi::PhysMCParticles::MCTruth::setMatch( const LoKi::MCMatch& m ) const {
  LoKi::MCMatch& tmp = const_cast<LoKi::MCMatch&>( m_match );
  tmp                = m;
}
// ============================================================================
// clear the list of particles
// ============================================================================
LoKi::PhysMCParticles::MCTruth::Keeper& LoKi::PhysMCParticles::MCTruth::storage() const {
  const Keeper& keeper = *this;
  return const_cast<Keeper&>( keeper );
}
// ============================================================================
// The END
// ============================================================================
