/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiKernel/SystemOfUnits.h"

#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/CaloCluster.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/ProtoParticle.h"

#include "Relations/RelationWeighted1D.h"

#include "LoKi/BasicFunctors.h"
#include "LoKi/IMCHybridFactory.h"
#include "LoKi/MCParticleCuts.h"
#include "LoKi/MCTypes.h"

namespace LoKi {

  /** @class MCTruthPhoton
   *  Dedicated algorithm to create
   *  the relation table   ProtoPatricle --> MCParticle
   *  for photons using the custom criteria
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date 2017-02-06
   */
  class MCTruthPhoton : public GaudiAlgorithm {
  public:
    /// standard algorithm
    MCTruthPhoton( const std::string& name, ISvcLocator* pSvc )
        : GaudiAlgorithm( name, pSvc )
        /// input neutral protoparticles
        , m_input( LHCb::ProtoParticleLocation::Neutrals )
        // default name of output relation table
        , m_output( "Relations/Rec/ProtoP/" + name )
        // maximal accepted chi2  (very loose...)
        , m_chi2( 4.0 )      // maximal accepted chi2 (very loose)
                             /// interval for Emc/Erec
        , m_eRatio( 0.2, 2 ) // interval for Emc/Erec
                             // the preambulo
        , m_preambulo()
        //  the factory
        , m_factory( "LoKi::Hybrid::MCTool/MCFACTORY" )
        //
        , m_mccut_str( "MCALL" )
        , m_mccut( LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant( true ) ) {
      declareProperty( "Input", m_input,
                       "Input locations of neutral protoparticles: '" +
                           std::string( LHCb::ProtoParticleLocation::Neutrals ) + "'" );
      declareProperty( "Output", m_output, "Output location of PP->MCP relation table: 'Relations/Rec/ProtoP/<NAME>'" );
      declareProperty( "MaxChi2", m_chi2, "Maximal accepted chi2-distance" );
      declareProperty( "EnergyFraction", m_eRatio, "Interval for Emc/Erec" );
      declareProperty( "Factory", m_factory, "Type/Name for C++/Python Hybrid Factory" )
          ->declareUpdateHandler( &LoKi::MCTruthPhoton::propHandler, this );
      // the preambulo
      declareProperty( "Preambulo", m_preambulo, "The preambulo to be used for Bender/Python script" )
          ->declareUpdateHandler( &LoKi::MCTruthPhoton::propHandler, this );
      declareProperty( "Code", m_mccut_str, "Code to select ``good''photons" )
          ->declareUpdateHandler( &LoKi::MCTruthPhoton::propHandler, this );
    }

    MCTruthPhoton()                       = delete;
    MCTruthPhoton( const MCTruthPhoton& ) = delete;

  public:
    StatusCode initialize() override;
    StatusCode execute() override;

    /// the update handler
    void propHandler( Gaudi::Details::PropertyBase& /* p */ ) {
      if ( Gaudi::StateMachine::INITIALIZED > FSMState() ) { return; }
      StatusCode sc = initVar();
      Assert( sc.isSuccess(), "Unable to set 'Code'", sc );
    }
    /// initialize variables
    StatusCode initVar();

  private:
    inline std::pair<double, double> xy_pos( const LHCb::MCParticle* mcp, const double z ) const {
      if ( nullptr == mcp ) { return std::make_pair( -1 * Gaudi::Units::km, -1 * Gaudi::Units::km ); }
      static const Gaudi::XYZPoint s_null{ 0, 0, 0 };
      const LHCb::MCVertex*        mcv = mcp->originVertex();
      const Gaudi::XYZPoint&       p0  = ( nullptr == mcv ) ? s_null : mcv->position();
      const Gaudi::LorentzVector&  vct = mcp->momentum();
      // trivial linear extrapolation:
      const double x = p0.x() + vct.Px() * ( z - p0.z() ) / vct.Pz();
      const double y = p0.y() + vct.Py() * ( z - p0.z() ) / vct.Pz();
      return std::make_pair( x, y );
    }

    inline double match( const LHCb::MCParticle* mcp, const LHCb::CaloPosition* pos, const double delta = 0 ) {
      if ( nullptr == mcp || nullptr == pos ) { return false; }
      double mcx, mcy;
      std::tie( mcx, mcy ) = xy_pos( mcp, pos->z() );
      const Gaudi::Vector2 v2( mcx, mcy );
      Gaudi::SymMatrix2x2  icov2{ pos->spread() };
      if ( 0 < delta ) {
        const double d2 = delta * delta;
        icov2( 0, 0 ) += d2;
        icov2( 1, 1 ) += d2;
      }
      // use Manuel's inverter:
      const bool ok = icov2.InvertChol();
      if ( !ok ) {
        Warning( "Can't invert ``spread''-matrix" ).ignore();
        return false;
      }
      //
      // const double xp = pos->parameters()[0] ;
      // const double yp = pos->parameters()[1] ;
      // return ROOT::Math::Similarity ( v2 - Gaudi::Vector2( xp , yp ) , icov2 ) ;
      //
      return ROOT::Math::Similarity( v2 - pos->center(), icov2 );
    }

    /// input neutral protoparticles
    std::string m_input;
    /// the output TES location of relation table
    std::string m_output;
    /// maximal accepted chi2-distance
    double m_chi2; // maximal accepted chi2-distance
    /// accepted interval for Emc/Erec
    std::pair<double, double> m_eRatio; // accepted interval for Emc/Erec
    /// preambulo
    std::vector<std::string> m_preambulo; // preambulo
    /// the factory
    std::string m_factory; // the factory
    /// criteria for MC photon selection
    std::string        m_mccut_str; // criteria for MC photon selection
    LoKi::Types::MCCut m_mccut;

    mutable Gaudi::Accumulators::StatCounter<size_t> m_goodMCCounter{ this, "#good-MC" };
    mutable Gaudi::Accumulators::BinomialCounter<>   m_goodMCFoundCounter{ this, "#good-MC-found" };
    mutable Gaudi::Accumulators::StatCounter<size_t> m_linksCounter{ this, "#links" };
  };
} // namespace LoKi

StatusCode LoKi::MCTruthPhoton::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize();
  if ( sc.isFailure() ) { return sc; }
  return initVar();
}

namespace {
  /// construct the preambulo string
  inline std::string _preambulo_( const std::vector<std::string>& lines ) {
    std::string result;
    for ( std::vector<std::string>::const_iterator iline = lines.begin(); lines.end() != iline; ++iline ) {
      if ( lines.begin() != iline ) { result += "\n"; }
      result += ( *iline );
    }
    return result;
  }
} //                                                end of anonymous namespace

StatusCode LoKi::MCTruthPhoton::initVar() {
  // locate the factory
  IMCHybridFactory* factory = tool<IMCHybridFactory>( m_factory, this );
  //
  StatusCode sc = factory->get( m_mccut_str, m_mccut, _preambulo_( m_preambulo ) );
  if ( sc.isFailure() ) { return Error( "Unable to decode: \"" + m_mccut_str + "\"", sc ); }
  //
  // add some "protection"
  m_mccut = LoKi::Cuts::MCVALID && ( LoKi::Cuts::MCID == "gamma" ) && m_mccut;
  //
  release( factory ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  //
  if ( 0 >= m_chi2 ) { return Error( "Invalid chi2 cut is specified!" ); }
  if ( m_eRatio.first < m_eRatio.second && ( 1 < m_eRatio.first || 1 > m_eRatio.second ) ) {
    return Error( "Weird setting of ``EnergyFraction''" );
  }
  //
  return StatusCode::SUCCESS;
}

StatusCode LoKi::MCTruthPhoton::execute() {
  // ProtoParticle -> MCParticle relations   (MC output)
  typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> Table;

  // create the output relation table and put it into TES
  Table* table = new Table( 20 );
  put( table, m_output );

  // 1) get neutral protoparticles
  const LHCb::ProtoParticle::Container* pps = get<LHCb::ProtoParticle::Container>( m_input );
  if ( nullptr == pps || pps->empty() ) {
    return Warning( "Empty input container of ProtoParticles", StatusCode::SUCCESS );
  }

  // 2) select good MC-photons
  LHCb::MCParticle::ConstVector mcgood;
  mcgood.reserve( 20 );
  // get input MC-particles
  const LHCb::MCParticle::Container* mcall = get<LHCb::MCParticle::Container>( LHCb::MCParticleLocation::Default );

  std::copy_if( mcall->begin(), mcall->end(), std::back_inserter( mcgood ), std::cref( m_mccut ) );

  std::stable_sort( mcgood.begin(), mcgood.end() );
  LHCb::MCParticle::ConstVector::iterator idup = std::unique( mcgood.begin(), mcgood.end() );
  mcgood.erase( idup, mcgood.end() );

  m_goodMCCounter += mcgood.size();
  m_goodMCFoundCounter += !mcgood.empty();

  if ( mcgood.empty() ) { return Warning( "No good MC-photons are found", StatusCode::SUCCESS ); }

  for ( const LHCb::ProtoParticle* pp : *pps ) {
    // 1) skip invalid or charged
    if ( nullptr == pp || nullptr != pp->track() ) { continue; } // CONTINUE

    // 2) find proper CaloHypo object
    const LHCb::CaloHypo*                 hypo  = nullptr;
    const SmartRefVector<LHCb::CaloHypo>& hypos = pp->calo();
    for ( const LHCb::CaloHypo* h : hypos ) {
      if ( 0 != h && LHCb::CaloHypo::Photon == h->hypothesis() ) {
        hypo = h;
        break;
      }
    }
    if ( nullptr == hypo ) { continue; }

    // 3) get CaloPosition for the hypos
    const LHCb::CaloPosition* pos = hypo->position();
    // 3') make a try to get CaloPosition from the first CaloCluster
    if ( nullptr == pos ) {
      // get the position from cluster
      const SmartRefVector<LHCb::CaloCluster>& clusters = hypo->clusters();
      if ( clusters.empty() ) { continue; } // CONTINUE
      const LHCb::CaloCluster* cluster = clusters.front();
      if ( nullptr == cluster ) { continue; } // CONTNINUE
      pos = &( cluster->position() );
    }

    // 4) make a loop over selected MC-photons
    for ( const LHCb::MCParticle* mcp : mcgood ) {
      // 5) skip nulls
      if ( nullptr == mcp ) { continue; } // skip NULLs

      // 6) (fast) check Emc/Ecalo ratio (if specified)
      if ( m_eRatio.first < m_eRatio.second ) {
        const double eR = mcp->momentum().E() / pos->e();
        if ( eR < m_eRatio.first || eR > m_eRatio.second ) { continue; }
      }

      // 7) perform chi2-match
      const double chi2 = match( mcp, pos, 1 * Gaudi::Units::cm );

      // 8) check chi2-matching quality
      if ( m_chi2 < chi2 ) { continue; }

      // 9) add the entry into the relation table using fast "i_push"
      table->i_push( pp, mcp, pos->e() / mcp->momentum().E() );

    } //                                      End of the loop over MC particles
  }   //                                      End of the loop over protoparticles

  ///  10) sort relation table
  table->i_sort(); //  IMPORTANT: sort relation table!

  /// some statistics
  m_linksCounter += table->relations().size();

  return StatusCode::SUCCESS; // RETURN
}

/// Declaration of the Factory
DECLARE_COMPONENT( LoKi::MCTruthPhoton )
