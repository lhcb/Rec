/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiAlg/GaudiAlgorithm.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"

#include "Relations/IRelationWeightedBase.h"
#include "Relations/IsConvertible.h"

#include "Kernel/Particle2MCLinker.h"
#include "Kernel/Particle2MCMethod.h"

namespace LoKi {

  /** @class P2MCBase
   *
   *  Helper base class for decoding of Linkers into Relation Tables
   *
   *  This file is a part of LoKi project -
   *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2007-02-24
   */
  class P2MCBase : public GaudiAlgorithm {
  public:
    // the linker from Orsay
    typedef Object2MCLinker<LHCb::Particle> MCLinker;

    /// initialize the algorithm
    StatusCode initialize() override;
    /// finalize   the algorithm
    StatusCode finalize() override;

  protected:
    /// standard constructor
    P2MCBase( const std::string& name, ISvcLocator* pSvc );
    /// virtual destructor
    virtual ~P2MCBase();

  public:
    // handler for inputs
    void inputsHandler( Gaudi::Details::PropertyBase& ); ///< handler for inputs
    // handler for method
    void methodHandler( Gaudi::Details::PropertyBase& ); ///< handler for method

  private:
    // default constructor is disabled
    P2MCBase();
    // copy  constructor is disabled
    P2MCBase( const P2MCBase& );
    // assignement operator is disabled
    P2MCBase& operator=( const P2MCBase& );

    mutable Gaudi::Accumulators::StatCounter<size_t> m_particlesCounter{ this, "#particles" };
    mutable Gaudi::Accumulators::StatCounter<size_t> m_linksCounter{ this, "#links" };

  protected:
    /// copy links from linker to relation table
    template <class TABLE>
    inline StatusCode fillTable( TABLE* table );

    typedef std::vector<std::string> Addresses;
    // list of inputs
    SimpleProperty<Addresses> m_inputs; ///< list of inputs
    // the method
    IntegerProperty m_method; ///<  the method
    // output table
    std::string m_output; ///<  output Relation table
    // linker object
    MCLinker* m_linker; ///< linker object
  };
} //                                                      end of namespace LoKi

#define INHERITS( T1, T2 )                                                                                             \
  ( Relations::IsConvertible<const T1*, const T2*>::value && !Relations::IsConvertible<const T1*, const void*>::same )

// ============================================================================
// anonymous namespace to hide some implementation detailes
// ============================================================================
namespace {
  /// just the forward decalrations
  template <class TABLE, bool>
  struct _iGet;

  /** @struct _iGet
   *  The helper structure to copy the information from linker
   *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
   */
  template <class TABLE, bool>
  struct _iGet {
    void copy( LoKi::P2MCBase::MCLinker* linker, const LHCb::Particle* particle, TABLE* table ) const {
      if ( !linker || !table ) return;
      for ( const auto& [mc, weight] : linker->range( particle ) ) table->i_push( particle, &mc, weight );
    }
  };

  /// dispatch for non-weighted relations
  template <class TABLE>
  struct _iGet<TABLE, false> {
    void copy( LoKi::P2MCBase::MCLinker* linker, const LHCb::Particle* particle, TABLE* table ) const {
      if ( !linker || !table ) return;
      for ( const auto& [mc, weight] : linker->range( particle ) ) table->i_push( particle, &mc );
    }
  };

  /** @struct iGet
   *  The actual helper structure to copy the information from linker
   *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
   */
  template <class TABLE>
  struct iGet : public _iGet<TABLE, INHERITS( TABLE, IRelationWeightedBase )> {};
} //                                                 end of anonymous namespace

// ============================================================================
/// copy links from linker to relation table
// ============================================================================
template <class TABLE>
inline StatusCode LoKi::P2MCBase::fillTable( TABLE* table ) {
  Assert( 0 != table, "Invalid Relation Table" );
  // create linker if needed
  if ( 0 == m_linker ) { m_linker = new MCLinker( this, m_method, m_inputs ); }
  // just for convinience
  const StatusCode OK = StatusCode::SUCCESS;
  // Helper object to copy links
  iGet<TABLE> getter;
  // for statistics
  size_t nParticles = 0;
  // loop over input linkers
  const Addresses& addresses = m_inputs;
  for ( Addresses::const_iterator iaddr = addresses.begin(); addresses.end() != iaddr; ++iaddr ) {
    const std::string loc = ( *iaddr ) + "/Particles";
    //
    LHCb::Particle::Range range;
    if ( exist<LHCb::Particle::Range>( loc ) ) {
      range = get<LHCb::Particle::Range>( loc );
    } else if ( exist<LHCb::Particle::Range>( *iaddr ) ) {
      range = get<LHCb::Particle::Range>( *iaddr );
    } else {
      Error( "No valid LHCb::Particle::Range found for '" + ( *iaddr ) + "'", OK ).ignore();
      continue; // CONTINUE
    }
    // for statistics
    nParticles += range.size();
    // loop over all particles
    for ( LHCb::Particle::Range::iterator ipart = range.begin(); range.end() != ipart; ++ipart ) {
      const LHCb::Particle* p = *ipart;
      if ( 0 == p ) { continue; } // CONTINUE
      // copy the links
      getter.copy( m_linker, p, table ); // ATTENTION i_push
    }                                    // end f the loop over particles in the container
    //
  } // end of the loop over input containers

  // MANDATORY call of i_sort after i_push!
  table->i_sort(); // MANDATORY i_sort
  // simple check
  if ( table->i_relations().empty() ) { Warning( "Empty relation table!" ).ignore(); }
  // a bit of statistics
  if ( msgLevel( MSG::DEBUG ) ) {
    m_particlesCounter += nParticles;
    m_linksCounter += table->i_relations().size();
  }

  return StatusCode::SUCCESS;
}
