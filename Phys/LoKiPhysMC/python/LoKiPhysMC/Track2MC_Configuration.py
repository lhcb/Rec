###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @file
#  The configuration file to run Track --> MC 'on-demand'
#
#   This file is a part of LoKi project -
#     "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
#  contributions and advices from G.Raven, J.van Tilburg,
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @see Kernel/Track2MC.h
#  @see LoKi::Track2MC
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date   2008-11-04
# =============================================================================
"""
#  The configuration file to run Track --> MC 'on-demand'

This file is a part of LoKi project -
\"C++ ToolKit  for Smart and Friendly Physics Analysis\"

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
contributions and advices from G.Raven, J.van Tilburg,
A.Golutvin, P.Koppenburg have been used in the design.
"""

# =============================================================================
__author__ = " Vanya BELYAEV Ivan.Belyaev@nikhef.nl "
# =============================================================================

from Configurables import DataOnDemandSvc, LoKi__Track2MC
from Gaudi.Configuration import *

alg = LoKi__Track2MC()

dod = DataOnDemandSvc()
dod.AlgMap["Relations/Rec/Track/Default"] = alg.getFullName()

# =============================================================================
# The END
# =============================================================================
