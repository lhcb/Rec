/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PHYSMCPARTICLES_H
#  define LOKI_PHYSMCPARTICLES_H 1
// ============================================================================
// Include files
// ============================================================================
// LokiPhysMC
// ============================================================================
#  include "LoKi/MCTruth.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-20
 */
// ============================================================================
namespace LoKi {
  // =========================================================================
  /// forward declarations
  class MCMatch;
  // ==========================================================================
  namespace PhysMCParticles {
    // ========================================================================
    /** @class RCTruth PhysMCParticles.h LoKi/PhysMCParticles.h
     *
     *  Helper and useful function to be used to check the matching of
     *  LHCb::Particle and some LHCb::MCParticle
     *
     *  @see LoKi::MCMatchObj
     *  @see LoKi::MCMatch
     *  @see LHCb::Particle
     *  @see LHCb::MCParticle
     *  @see LoKi::Cuts::RCTRUTH
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date   2003-01-28
     */
    class RCTruth : public LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate,
                    public LoKi::Keeper<LHCb::Particle> {
    public:
      // ======================================================================
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param particle  pointer to particle object
       */
      RCTruth( const LoKi::MCMatch& match, const LHCb::Particle* particle );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param range "container" of particles
       */
      RCTruth( const LoKi::MCMatch& match, const LoKi::Types::Range& range );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param range container of particles
       */
      RCTruth( const LoKi::MCMatch& match, const LHCb::Particle::Vector& range );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param range container of particles
       */
      RCTruth( const LoKi::MCMatch& match, const LHCb::Particle::ConstVector& range );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param range container of particles
       */
      RCTruth( const LoKi::MCMatch& match, const LoKi::Keeper<LHCb::Particle>& range );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param range container of particles
       */
      RCTruth( const LoKi::MCMatch& match, const LoKi::UniqueKeeper<LHCb::Particle>& range );
      /** constructor
       *  @param match MCMatch object (working horse)
       *  @param begin begin iterator of any sequence of particles
       *  @param end   end   iterator of any sequence of particles
       */
      template <class PARTICLE>
      RCTruth( const LoKi::MCMatch& match, PARTICLE begin, PARTICLE end )
          : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
          , LoKi::Keeper<LHCb::Particle>( begin, end )
          , m_match( match ) {}
      /** constructor
       *  @param particle  pointer to particle object
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LHCb::Particle* particle, const LoKi::MCMatch& match );
      /** constructor
       *  @param range "container" of particles
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LoKi::Types::Range& range, const LoKi::MCMatch& match );
      /** constructor
       *  @param range container of particles
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LHCb::Particle::Vector& range, const LoKi::MCMatch& match );
      /** constructor
       *  @param range container of particles
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LHCb::Particle::ConstVector& range, const LoKi::MCMatch& match );
      /** constructor
       *  @param range container of particles
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LoKi::Keeper<LHCb::Particle>& range, const LoKi::MCMatch& match );
      /** constructor
       *  @param range container of particles
       *  @param match MCMatch object (working horse)
       */
      RCTruth( const LoKi::UniqueKeeper<LHCb::Particle>& range, const LoKi::MCMatch& match );
      /** constructor
       *  @param begin begin iterator of any sequence of particles
       *  @param end   end   iterator of any sequence of particles
       *  @param match MCMatch object (working horse)
       */
      template <class PARTICLE>
      RCTruth( PARTICLE begin, PARTICLE end, const LoKi::MCMatch& match )
          : LoKi::BasicFunctors<const LHCb::MCParticle*>::Predicate()
          , LoKi::Keeper<LHCb::Particle>( begin, end )
          , m_match( match ) {}
      /// copy constructor
      RCTruth( const RCTruth& right );
      /// MANDATORY : virtual destructor destructor
      virtual ~RCTruth();
      /// MANDATORY: clone method ("virtual constructor")
      RCTruth* clone() const override;
      /// MANDATORY: the only one essential method ("function")
      result_type operator()( argument ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    public:
      // ======================================================================
      /** add additional particle to the
       *  list of Particles to be matched
       *  @param p particle to be added
       */
      RCTruth& add( const LHCb::Particle* p ) {
        addObject( p );
        return *this;
      }
      /** add additional particles to the
       *  list of MCParticles to be matched
       *  @param range new range of MC particles
       */
      RCTruth& add( const LoKi::Types::Range& range ) { return add( range.begin(), range.end() ); }
      /** add few additional particles to the list of
       *  Particles  to be matched
       *  @param first "begin" iterator of sequence
       *  @param last
       */
      template <class PARTICLE>
      RCTruth& add( PARTICLE first, PARTICLE last ) {
        addObjects( first, last );
        return *this;
      }
      // ======================================================================
    private:
      // ======================================================================
      // default constructor is disabled
      RCTruth();
      // assignement is disabled
      RCTruth& operator=( const RCTruth& );
      // ======================================================================
    private:
      // ======================================================================
      /// MC-match object
      LoKi::MCMatch m_match; // MC-match object
      // ======================================================================
    };
    // ========================================================================
    namespace Particles {
      // ======================================================================
      /// import the type into the proper namespace
      typedef LoKi::PhysMCParticles::MCTruth MCTruth;
      // ======================================================================
    } // namespace Particles
    // ========================================================================
    namespace MCParticles {
      // ======================================================================
      /// import the type into proper namespace
      typedef LoKi::PhysMCParticles::RCTruth RCTruth;
      // ======================================================================
    } // namespace MCParticles
    // ========================================================================
  } // namespace PhysMCParticles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PHYSMCPARTICLES_H
// ============================================================================
