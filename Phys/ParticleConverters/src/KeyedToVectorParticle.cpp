/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {
  namespace Converters {
    struct RangeToVectorParticle
        : public LHCb::Algorithm::Transformer<std::vector<LHCb::Particle>( const LHCb::Particle::Range& )> {
      RangeToVectorParticle( const std::string& name, ISvcLocator* pSvcLocator )
          : Transformer( name, pSvcLocator, { KeyValue{ "Input", "" } }, KeyValue{ "Output", "" } ) {}
      std::vector<LHCb::Particle> operator()( const LHCb::Particle::Range& in ) const override {
        std::vector<LHCb::Particle> out;
        for ( auto* i : in ) { out.emplace_back( *i ); }
        return out;
      }
    };
    DECLARE_COMPONENT( RangeToVectorParticle )
  } // namespace Converters
} // namespace LHCb
