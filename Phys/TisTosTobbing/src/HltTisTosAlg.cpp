/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/Particle.h"
#include "Kernel/IParticleDescendants.h"
#include "Kernel/IParticleTisTos.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include <mutex>

namespace detail {
  /// Define input type signatures
  template <typename Particles>
  struct InputSignature {
    /// Value type held in the Particles container
    using ValueType = typename Particles::value_type;
  };

  /// Define input type signatures (v1)
  template <>
  struct InputSignature<LHCb::Particle::Range> {
    /// Value type held in the Particles container
    using ValueType = LHCb::Particle;
  };

  using map_t     = std::map<std::string, LHCb::detail::TisTosResult_t>;
  using vec_str_t = std::vector<std::string>;
  template <typename Particles>
  using Table_t = LHCb::Relation1D<typename InputSignature<Particles>::ValueType, map_t>;
  template <typename Particles>
  using base_class_hlt1_t = LHCb::Algorithm::Transformer<Table_t<Particles>(
      const Particles&, const LHCb::HltSelReports&, const LHCb::HltDecReports& )>;
  template <typename Particles>
  using base_class_hlt2_t =
      LHCb::Algorithm::Transformer<Table_t<Particles>( const Particles&, const LHCb::HltDecReports& )>;

  /// Remove the suffix "Decision" from the line name
  std::string_view remove_dec_suffix( std::string_view s ) {
    std::string_view suffix{ "Decision" };
    if ( s.size() >= suffix.size() && s.compare( s.size() - suffix.size(), s.npos, suffix ) == 0 )
      s.remove_suffix( suffix.size() );
    return s;
  }

  /// check if data handle corresponds to the line
  template <typename Particles>
  using datahandle_t = std::unique_ptr<DataObjectReadHandle<Particles>>;

  template <typename Particles>
  using vec_datahandle_t = std::vector<std::unique_ptr<DataObjectReadHandle<Particles>>>;

  template <typename Particles>
  void fill_trigger_cands_from_datahandle( const vec_datahandle_t<Particles>& data_handles,
                                           const std::string& line_decision, Particles& trigger_cands ) {
    const auto& suffix = std::string( detail::remove_dec_suffix( line_decision ) ) + std::string( "/" );
    for ( const auto& cand_handle : data_handles ) {
      if ( cand_handle->objKey().find( suffix ) != std::string::npos ) {
        trigger_cands = cand_handle->get();
        break;
      }
    }
    return;
  }
} // namespace detail

/** @class Hlt1TisTosAlg
 * @brief Algorithm used to store TIS/TOS decision with respect to specified set of trigger lines.
 *
 * Given a user-specified list of trigger lines and reconstructed particle, the algorithm outputs a relation table.
 * The relation table relates the reconstructed particle to a map with trigger line names as keys and TIS/TOS
 * decision as values. The TOB value
 * can be obtained together with line decision i.e. "decision && !tis && !tos". The TPS value is not stored at the
 * moment, see discussion (https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3115#note_5999516).
 *
 * @param Input location of particles
 * @param Input location of HltSelReports object
 * @param Input location of HltDecReports object
 * @returns Output location of relation table, P2TisTosTable, which relates reconstructed particles to a map of TIS/TOS
 * line decisions
 *
 * Usage:
 *
 * @code
 *    from PyConf.Algorithms import Hlt1TisTosAlg
 *    from PyConf.Tools import ParticleTisTos
 *    import Functors as F
 *
 *    # Configure the ParticleTisTos tool.
 *    # For full list of tunable property
 *    #     - see:
 *      https://gitlab.cern.ch/lhcb/Rec/-/blob/2cbd84983db8720d53e9e307e7e0b4ee829504af/Phys/TisTosTobbing/src/lib/TisTos.cpp#L44-60.
 *    #     - see:
 *      https://gitlab.cern.ch/lhcb/Rec/-/blob/c93052bca860db463f5403c0e30b5339b02ecb37/Phys/TisTosTobbing/src/lib/ParticleTisTos.cpp#L40-45
 *    particletistos = ParticleTisTos(TOSFracFT=0.7, TISFracUT=0.01)
 *
 *    # Configure the Hlt1TisTosAlg algorithm
 *    hlttistosalg = Hlt1TisTosAlg(name="HltTisTosAlgName",
 *                     InputParticles=force_location('path/to/particles'),
 *                     SelReports=make_allen_selreports(),
 *                     TriggerLines=["Hlt1_Line1Decision", "Hlt1_Line1Decision"],
 *                     ParticleTisTosTool=particletistos)
 *    # Get the relation table (Reco particle -> Map of TIS/TOS line decisions)
 *    TisTosRelations = hlttistosalg.P2TisTosTable  # Relations
 *
 *    # Get a functor
 *    is_tos = F.IS_TOS(line, TisTosRelations)
 * @endcode
 *
 * @see IParticleTisTos
 * @see ITisTos
 * @see ParticleTisTos
 * @see TisTos
 * @see FunTuple
 */
template <typename Particles>
class Hlt1TisTosAlg : public detail::base_class_hlt1_t<Particles> {
public:
  using KeyValue = typename detail::base_class_hlt1_t<Particles>::KeyValue;
  Hlt1TisTosAlg( const std::string& name, ISvcLocator* pSvc )
      : detail::base_class_hlt1_t<Particles>{
            name,
            pSvc,
            { KeyValue{ "InputParticles", "" }, KeyValue{ "SelReports", "" }, KeyValue{ "DecReports", "" } },
            { KeyValue{ "P2TisTosTable", "" } } } {}
  StatusCode initialize() override;

  detail::Table_t<Particles> operator()( const Particles& input_particles, const LHCb::HltSelReports& sel_reports,
                                         const LHCb::HltDecReports& dec_reports ) const override;

private:
  /// Gaudi property to specify the trigger lines
  Gaudi::Property<detail::vec_str_t> m_line_decisions{
      this, "TriggerLines", {}, "List of trigger lines for which TIS/TOS information is required." };
  /// Configured IParticleTisTos tool to compute TIS/TOS decisions.
  /// Tool must be mutable as all the methods of this tool are non-const.
  /// When the tools become thread-safe probably move to a functor rather
  /// than this algorithm (see discussion: https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3115#note_6001452)
  mutable ToolHandle<IParticleTisTos> m_tistos{ this, "ParticleTisTosTool", "ParticleTisTos" };
  /// Lock needs to be acquired during `operator()` to avoid race conditions when using the IParticleTisTos tool
  mutable std::mutex m_lock;
  /// Tool get all descendants of a given particle
  ToolHandle<IParticleDescendants> m_descendants{ this, "ParticleDescendants", "ParticleDescendants" };
};

template <typename Particles>
StatusCode Hlt1TisTosAlg<Particles>::initialize() {
  return detail::base_class_hlt1_t<Particles>::initialize().andThen( [&]() -> StatusCode {
    /// Check that the tool handle has been loaded correctly
    if ( !m_tistos )
      throw GaudiException{ "The tool IParticleTisTos has not been configured properly and is null. Please check!",
                            "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// Check that the line decisions are non-empty
    if ( m_line_decisions.empty() )
      throw GaudiException{ "No Hlt1 line decisions have been specified to store the TIS/TOS results. Please check!",
                            "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// check that the m_line_decisions have a suffix "Decision" if not throw as error
    for ( auto& line : m_line_decisions )
      if ( line.find( "Decision" ) == std::string::npos )
        throw GaudiException{ "The line decision " + line + " does not have a suffix 'Decision'. Please check!",
                              "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    return StatusCode::SUCCESS;
  } );
}

template <typename Particles>
detail::Table_t<Particles> Hlt1TisTosAlg<Particles>::operator()( const Particles&           input_particles,
                                                                 const LHCb::HltSelReports& sel_reports,
                                                                 const LHCb::HltDecReports& dec_reports ) const {
  /// Loop over the input particles and make a flattened vector containing particles and all its daughters
  /// This is needed since TIS/TOS can be stored for a daughter of a particle
  /// Note: If one does not want to loop over all particles at TES location can put a filter before running this
  /// algorithm.
  LHCb::Particle::ConstVector parts_in;
  for ( const auto& p : input_particles ) {
    parts_in.push_back( p ); /// input mother
    LHCb::Particle::ConstVector dauts_in = m_descendants->descendants( p );
    parts_in.insert( parts_in.end(), dauts_in.begin(), dauts_in.end() );
  }

  /// Create an instance of relations table i.e. map of particle -> TIS/TOS line decisions
  detail::Table_t<Particles> reco2tistos;

  /// Loop over the flattened input particles with a lock to avoid race conditions when interacting with m_tistos tool.
  /// Also fill the relations table.
  std::scoped_lock guard{ m_lock };
  for ( const auto* input_particle : parts_in ) {
    /// Clear the vector holding LHCbIDs of reconstructed particles (i.e. clear "m_Signal" vector of type
    /// "std::vector<LHCbID>")
    m_tistos->setSignal();
    /// Add to "signal" vector (i.e. get LHCbIDs and put them into "m_Signal" vector)
    /// If composite loop over daughters and add them to signal
    m_tistos->addToSignal( *input_particle );
    /// Creat a map <string, bool> for a given particle
    detail::map_t map_tistos;
    /// define number of valid lines
    unsigned n_valid_lines = 0;
    /// Loop over the user specified decisions
    for ( const std::string& line_decision : m_line_decisions ) {
      /// get the pointer to decision report
      const auto dec_ptr = dec_reports.decReport( line_decision );

      /// if the hlt line decision does not exist simply continue (no entry in dictionary)
      if ( !dec_ptr ) continue;

      /// if the decision was not true then continue
      if ( !dec_ptr->decision() ) continue;

      /// Get the HltObjectSummary for the specified line decision
      const LHCb::HltObjectSummary* sel = sel_reports.selReport( line_decision );
      /// Throw GaudiException if the HltObjectSummary is null
      if ( !sel )
        throw GaudiException{ "The selection report for " + line_decision + " is null. Please check!",
                              "Hlt1TisTosAlg::operator()", StatusCode::FAILURE };
      /// Store TIS/TOS/TUS decision in the map
      LHCb::detail::TisTosResult_t tistostus;
      tistostus.setTOS( m_tistos->tos( *sel ) );
      tistostus.setTIS( m_tistos->tis( *sel ) );
      tistostus.setTUS( m_tistos->tus( *sel ) ); // tps | tos
      /// store the result
      map_tistos[line_decision] = tistostus;
      n_valid_lines++;

      /// print the map_tistos
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "TIS/TOS/TUS for " << line_decision << " for particle " << input_particle->particleID().pid()
                      << " is TIS: " << tistostus.TIS() << " TOS: " << tistostus.TOS() << " TUS: " << tistostus.TUS()
                      << endmsg;
      }
    }
    /// Clear the vector holding LHCbIDs of reconstructed particles again
    m_tistos->setSignal();
    /// Throw GaudiException if size of map_tistos is not equal to m_line_decisions.size()*2
    if ( map_tistos.size() != n_valid_lines )
      throw GaudiException{ "Size of the map containing TIS/TOS information is not equal to (# of specified trigger "
                            "lines). Please check!",
                            "Hlt1TisTosAlg::operator()", StatusCode::FAILURE };
    /// Relate the particle to the map only if the map is not empty
    if ( !map_tistos.empty() ) reco2tistos.relate( input_particle, map_tistos ).ignore();
  }
  return reco2tistos;
};

/** @class Hlt2TisTosAlg
 * @brief Algorithm used to store TIS/TOS decision with respect to specified set of trigger lines.
 *
 * Given a user-specified list of trigger lines and reconstructed particle, the algorithm outputs a relation table.
 * The relation table relates the reconstructed particle to a map with trigger line names as keys and TIS/TOS
 * decision as values. The TOB value
 * can be obtained together with line decision i.e. "decision && !tis && !tos". The TPS value is not stored at the
 * moment, see discussion (https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3115#note_5999516).
 *
 * @param Input location of spruce particles
 * @param Input TES location of all Hlt2 trigger candidates
 * @param Input location of HltDecReports object
 * @returns Output location of relation table, P2TisTosTable, which relates reconstructed particles to a map of TIS/TOS
 * line decisions
 *
 * Usage:
 *
 * @code
 *    from PyConf.Algorithms import Hlt2TisTosAlg
 *    from PyConf.Tools import ParticleTisTos
 *    import Functors as F
 *
 *    # Configure the ParticleTisTos tool.
 *    # For full list of tunable property
 *    #     - see:
 *      https://gitlab.cern.ch/lhcb/Rec/-/blob/2cbd84983db8720d53e9e307e7e0b4ee829504af/Phys/TisTosTobbing/src/lib/TisTos.cpp#L44-60.
 *    #     - see:
 *      https://gitlab.cern.ch/lhcb/Rec/-/blob/c93052bca860db463f5403c0e30b5339b02ecb37/Phys/TisTosTobbing/src/lib/ParticleTisTos.cpp#L40-45
 *    particletistos = ParticleTisTos(TOSFracFT=0.7, TISFracUT=0.01)
 *
 *    # Configure the Hlt2TisTosAlg algorithm
 *    hlttistosalg = Hlt2TisTosAlg(name="HltTisTosAlgName",
 *                     InputParticles=force_location('path/to/particles'),
 *                     TriggerParticles=["Hlt2_Line1/Particles", "Hlt2_Line2/Particles"],
 *                     TriggerLines=["Hlt2_Line1Decision", "Hlt1_Line1Decision"],
 *                     ParticleTisTosTool=particletistos)
 *    # Get the relation table (Reco particle -> Map of TIS/TOS line decisions)
 *    TisTosRelations = hlttistosalg.P2TisTosTable  # Relations
 *
 *    # Get a functor
 *    is_tos = F.IS_TOS(line, TisTosRelations)
 * @endcode
 *
 * @see IParticleTisTos
 * @see ITisTos
 * @see ParticleTisTos
 * @see TisTos
 * @see FunTuple
 */

template <typename Particles>
class Hlt2TisTosAlg : public detail::base_class_hlt2_t<Particles> {
public:
  using KeyValue = typename detail::base_class_hlt2_t<Particles>::KeyValue;
  Hlt2TisTosAlg( const std::string& name, ISvcLocator* pSvc )
      : detail::base_class_hlt2_t<Particles>{ name,
                                              pSvc,
                                              { KeyValue{ "InputParticles", "" }, KeyValue{ "DecReports", "" } },
                                              { KeyValue{ "P2TisTosTable", "" } } } {}
  StatusCode                 initialize() override;
  detail::Table_t<Particles> operator()( const Particles&           input_particles,
                                         const LHCb::HltDecReports& dec_reports ) const override;

private:
  /// Gaudi property to specify the trigger lines
  Gaudi::Property<detail::vec_str_t> m_line_decisions{
      this, "TriggerLines", {}, "List of trigger lines for which TIS/TOS information is required." };
  /// Configured IParticleTisTos tool to compute TIS/TOS decisions.
  /// Tool must be mutable as all the methods of this tool are non-const.
  /// When the tools become thread-safe probably move to a functor rather
  /// than this algorithm (see discussion: https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/3115#note_6001452)
  mutable ToolHandle<IParticleTisTos> m_tistos{ this, "ParticleTisTosTool", "ParticleTisTos" };
  /// Lock needs to be acquired during `operator()` to avoid race conditions when using the IParticleTisTos tool
  mutable std::mutex m_lock;
  /// Tool get all descendants of a given particle
  ToolHandle<IParticleDescendants> m_descendants{ this, "ParticleDescendants", "ParticleDescendants" };
  /// vector of Hlt2 trigger candidates
  Gaudi::Property<detail::vec_str_t> m_candidateList{
      this, "TriggerParticles", {}, "List of TES locations to the output of Hlt2 candidates" };
  /// vector of data handles for the Hlt2 trigger candidates (this gets filled in initialize)
  detail::vec_datahandle_t<Particles> m_candidateHandles;
};

template <typename Particles>
StatusCode Hlt2TisTosAlg<Particles>::initialize() {
  return detail::base_class_hlt2_t<Particles>::initialize().andThen( [&]() -> StatusCode {
    /// Check that the tool handle has been loaded correctly
    if ( !m_tistos )
      throw GaudiException{ "The tool IParticleTisTos has not been configured properly and is null. Please check!",
                            "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// Check that the line decisions are non-empty
    if ( m_line_decisions.empty() )
      throw GaudiException{ "No Hlt2 line decisions have been specified to store the TIS/TOS results. Please check!",
                            "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// Check that the m_candidateList is non-empty
    if ( m_candidateList.empty() )
      throw GaudiException{
          "No Hlt2 trigger candidate's TES location has been specified to store the TIS/TOS results. Please check!",
          "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// Check that the m_line_decisions and m_candidateList have the same size
    if ( m_line_decisions.size() != m_candidateList.size() )
      throw GaudiException{
          "The number of Hlt2 line decisions and Hlt2 trigger candidate's TES locations do not match. Please check!",
          "Hlt1TisTosAlg::initialize()", StatusCode::FAILURE };

    /// check that there is one-to-one correspondence between TES location names (vec of strings) and specified lines
    /// decisions (vector of strings) i.e. check that the line decision name is a substring of only one and only one of
    /// the TES location names if not throw an exception
    for ( auto& line : m_line_decisions ) {
      const auto& suffix = std::string( detail::remove_dec_suffix( line ) ) + std::string( "/" );
      const auto  n_matched_locations =
          std::count_if( m_candidateList.begin(), m_candidateList.end(), [&suffix, this]( const auto& location ) {
            auto match = location.find( suffix ) != location.npos;
            this->debug() << "Checking if " << location << " contains " << suffix << ". It seems to be "
                          << std::to_string( match ) << endmsg;
            return match;
          } );

      if ( n_matched_locations != 1 ) {
        throw GaudiException{
            "The line decision " + line + " does not have a one-to-one correspondence between the TES locations." +
                " The number of matches are " + std::to_string( n_matched_locations ) + ". Please check!",
            "Hlt2TisTosAlg::initialize()", StatusCode::FAILURE };
      }
    }

    /// reserve the memory for the data handles
    m_candidateHandles.reserve( m_candidateList.size() );
    /// loop over the TES locations and create the data handles and check if they are valid
    for ( auto const& location : m_candidateList ) {
      // auto candHandle = std::make_unique<DataObjectReadHandle<LHCb::Particles>>(location, this);
      auto candHandle = std::make_unique<DataObjectReadHandle<Particles>>( location, this );
      if ( !candHandle->isValid() ) {
        throw GaudiException{ "The location " + location + " is not valid. Please check!",
                              "Hlt2TisTosAlg::initialize()", StatusCode::FAILURE };
      }
      m_candidateHandles.push_back( std::move( candHandle ) );
    }

    return StatusCode::SUCCESS;
  } );
}

template <typename Particles>
detail::Table_t<Particles> Hlt2TisTosAlg<Particles>::operator()( const Particles&           input_particles,
                                                                 const LHCb::HltDecReports& dec_reports ) const {
  /// Loop over the input particles and make a flattened vector containing particles and all its daughters
  /// This is needed since TIS/TOS can be stored for a daughter of a particle
  /// Note: If one does not want to loop over all particles at TES location can put a filter before running this
  /// algorithm.
  LHCb::Particle::ConstVector parts_in;
  for ( const auto& p : input_particles ) {
    parts_in.push_back( p ); /// input mother
    LHCb::Particle::ConstVector dauts_in = m_descendants->descendants( p );
    parts_in.insert( parts_in.end(), dauts_in.begin(), dauts_in.end() );
  }

  /// Create an instance of relations table i.e. map of particle -> TIS/TOS line decisions
  detail::Table_t<Particles> reco2tistos;

  /// Loop over the flattened input particles with a lock to avoid race conditions when interacting with m_tistos tool.
  /// Also fill the relations table.
  std::scoped_lock guard{ m_lock };
  for ( const auto* input_particle : parts_in ) {
    /// Clear the vector holding LHCbIDs of reconstructed particles (i.e. clear "m_Signal" vector of type
    /// "std::vector<LHCbID>")
    m_tistos->setSignal();
    /// Add to "signal" vector (i.e. get LHCbIDs and put them into "m_Signal" vector)
    /// If composite loop over daughters and add them to signal
    m_tistos->addToSignal( *input_particle );
    /// Creat a map <string, bool> for a given particle
    detail::map_t map_tistos;
    /// define number of valid lines
    unsigned n_valid_lines = 0;
    /// Loop over the user specified decisions
    for ( const std::string& line_decision : m_line_decisions ) {

      /// get the pointer to decision report
      const auto dec_ptr = dec_reports.decReport( line_decision );

      /// if the hlt line decision does not exist simply continue (no entry in dictionary)
      if ( !dec_ptr ) continue;

      /// if the decision was not true then continue
      if ( !dec_ptr->decision() ) continue;

      /// get the pointer to the particles
      Particles trigger_cands;
      detail::fill_trigger_cands_from_datahandle<Particles>( m_candidateHandles, line_decision, trigger_cands );
      if ( trigger_cands.empty() ) {
        throw GaudiException{ "Couldnt locate the trigger candidates for line " + line_decision + ". Please check!",
                              "Hlt2TisTosAlg::operator()", StatusCode::FAILURE };
      }

      // loop over the candidates and store the TIS/TOS/TUS decision
      bool tis = false;
      bool tos = false;
      bool tus = false;
      std::for_each( trigger_cands.begin(), trigger_cands.end(), [&]( const auto* cand ) {
        tis |= m_tistos->tis( *cand );
        tos |= m_tistos->tos( *cand );
        tus |= m_tistos->tus( *cand ); // tps | tos
        this->debug() << "TIS/TOS/TUS for " << line_decision << " for particle " << input_particle->particleID().pid()
                      << " and for candidate " << cand->particleID().pid() << " is TIS: " << tis << " TOS: " << tos
                      << " TUS: " << tus << endmsg;
      } );
      // Store TIS/TOS/TUS decision in the map
      LHCb::detail::TisTosResult_t tistostus;
      tistostus.setTOS( tos );
      tistostus.setTIS( tis );
      tistostus.setTUS( tus );
      // store the result
      map_tistos[line_decision] = tistostus;
      n_valid_lines++;

      /// print the map_tistos
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "TIS/TOS/TUS for " << line_decision << " for particle " << input_particle->particleID().pid()
                      << " is TIS: " << tistostus.TIS() << " TOS: " << tistostus.TOS() << " TUS: " << tistostus.TUS()
                      << endmsg;
      }
    }
    /// Clear the vector holding LHCbIDs of reconstructed particles again
    m_tistos->setSignal();
    /// Throw GaudiException if size of map_tistos is not equal to m_line_decisions.size()*2
    if ( map_tistos.size() != n_valid_lines )
      throw GaudiException{ "Size of the map containing TIS/TOS information is not equal to (# of specified trigger "
                            "lines). Please check!",
                            "Hlt1TisTosAlg::operator()", StatusCode::FAILURE };
    /// Relate the particle to the map only if the map is not empty
    if ( !map_tistos.empty() ) reco2tistos.relate( input_particle, map_tistos ).ignore();
  }
  return reco2tistos;
};

DECLARE_COMPONENT_WITH_ID( Hlt1TisTosAlg<LHCb::Particle::Range>, "Hlt1TisTosAlg" )
DECLARE_COMPONENT_WITH_ID( Hlt2TisTosAlg<LHCb::Particle::Range>, "Hlt2TisTosAlg" )
