/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Kernel/Particle2MCLinker.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"
#include "boost/algorithm/string/replace.hpp"

namespace detail {
  /// Define input type signatures
  template <typename MCParticles>
  struct InputSignature {
    /// Value type held in the MCParticles container
    using ValueType = typename MCParticles::value_type;
  };

  /// Define input type signatures (v1)
  template <>
  struct InputSignature<LHCb::MCParticle::Range> {
    /// Value type held in the MCParticles container
    using ValueType = LHCb::MCParticle;
  };

  using map_t     = std::map<std::string, int>;
  using vec_str_t = std::vector<std::string>;
  template <typename MCParticles>
  using Table_t = LHCb::Relation1D<typename InputSignature<MCParticles>::ValueType, map_t>;
  template <typename MCParticles>
  using base_class_hlt1_t = LHCb::Algorithm::Transformer<Table_t<MCParticles>(
      const MCParticles&, const LHCb::HltDecReports&, const LHCb::LinksByKey&, const LHCb::HltSelReports& )>;
  template <typename MCParticles>
  using base_class_hlt2_t = LHCb::Algorithm::Transformer<Table_t<MCParticles>(
      const MCParticles&, const LHCb::HltDecReports&, const LHCb::LinksByKey& )>;

  std::string_view remove_decision_suffix( std::string_view s ) {
    std::string_view suffix{ "Decision" };
    if ( s.size() >= suffix.size() && s.compare( s.size() - suffix.size(), s.npos, suffix ) == 0 )
      s.remove_suffix( suffix.size() );
    return s;
  }

  /// Functions for true sim eff matching

  using IDvec                 = std::vector<LHCb::LHCbID>;
  using IDsByTrack            = std::vector<IDvec>;
  using WeightedIDsByMCPTrack = std::vector<std::map<LHCb::LHCbID, float>>;
  bool match_to_true_signal( const IDsByTrack&            ids_by_candidate_track,
                             const WeightedIDsByMCPTrack& weighted_ids_by_mcp_track, const float match_threshold ) {
    /* Takes the LHCb IDs from a true signal candidate (weighted_ids_by_mcp_track) and the LHCb IDs
       from the reconstructed candidate (either via sel reports (HLT1; see detail::lhcbids_from_hos)
       or via the persisted candidates (HLT2; see detail::lhcbids_from_reco_particle)) and attempts
       to match them on a track-by-track basis, where detail::match_threshold or more of the reco
       track LHCbIDs must be present in one of the MCP's tracks.*/

    if ( ( weighted_ids_by_mcp_track.size() == 0 ) || ( ids_by_candidate_track.size() == 0 ) ) {
      return false; // Can't be matched if either of these are the case
    }

    auto reco_track_matches_to_mcp = [&weighted_ids_by_mcp_track, &match_threshold]( const IDvec& reco_track_IDs ) {
      if ( reco_track_IDs.size() == 0 ) { return false; }
      for ( const auto& mcp_track_IDs : weighted_ids_by_mcp_track ) {
        float n_matching_IDs{ 0.f };
        for ( const auto& id : reco_track_IDs ) {
          const auto iter = mcp_track_IDs.find( id );
          if ( iter != mcp_track_IDs.end() ) { n_matching_IDs += iter->second; }
        }
        float reco_match_frac = n_matching_IDs / reco_track_IDs.size();
        if ( reco_match_frac >= match_threshold ) { return true; }
      }
      return false;
    };

    return std::all_of( ids_by_candidate_track.begin(), ids_by_candidate_track.end(),
                        [&]( const auto& ids_this_track ) { return reco_track_matches_to_mcp( ids_this_track ); } );
  }

  void lhcbids_from_mcparticle( const LHCb::MCParticle& mcp, const LHCb::LinksByKey& mc2IdLink,
                                WeightedIDsByMCPTrack& weighted_ids_by_mcp_track ) {
    // Recursive function to iterate over each child of the MCParticle and collect the LHCbIDs in map

    // Add this track's LHCbIDs to the map
    std::map<LHCb::LHCbID, float> this_track_map_of_ids{};
    mc2IdLink.applyToAllLinks( [&this_track_map_of_ids, &mcp]( unsigned int id, int mcPartKey, float weight ) {
      if ( mcPartKey == mcp.key() ) { this_track_map_of_ids.insert( { LHCb::LHCbID( id ), weight } ); }
    } );
    if ( this_track_map_of_ids.size() > 0 ) { weighted_ids_by_mcp_track.emplace_back( this_track_map_of_ids ); }

    // Now iterate through children and add their LHCbIDs to the map
    // Use the MCParticle::endVertices method to get vertices on this track.
    const auto& endVertices = mcp.endVertices();
    for ( const auto& decayVertex : endVertices ) {
      if ( decayVertex->isDecay() ) {
        const auto& decayProducts = decayVertex->products();
        for ( const auto& childParticle : decayProducts ) {
          lhcbids_from_mcparticle( *childParticle, mc2IdLink, weighted_ids_by_mcp_track );
        }
      }
    }
  }

  bool lhcbids_from_reco_particle( const LHCb::Particle& part, IDsByTrack& bucket_of_ids ) {
    // Recursive function to extract all the LHCbIDs from a persisted trigger candidate

    if ( part.isBasicParticle() ) {
      if ( part.charge() == 0 ) {
        // Neutral particles have no track and therefore cannot be matched in this way. See MooreAnalysis#21.
        bucket_of_ids.emplace_back();
        return false;
      } else {
        bucket_of_ids.emplace_back( part.proto()->track()->lhcbIDs() );
        return true;
      }
    } else {
      bool        match_status = true;
      const auto& children     = part.daughters();
      for ( const auto& child : children ) { match_status &= lhcbids_from_reco_particle( *child, bucket_of_ids ); }
      return match_status; // dont use std::all_of as want all iterations to be completed regardless of return value
    }
  }

  void lhcbids_from_hos( const SmartRef<LHCb::HltObjectSummary>& hltObjectSummary, IDsByTrack& bucket_of_ids ) {
    // Recursive function to iterate over each track of the trigger candidate and collect the LHCbIDs in bucket_of_ids

    const auto& sub = hltObjectSummary->substructure();

    if ( hltObjectSummary->summarizedObjectCLID() == 10030 ) {
      // Vertex-like summary -> should be made of tracks and further vertices -> pass on the substructure...
      for ( const auto& subsummary : sub ) { lhcbids_from_hos( subsummary, bucket_of_ids ); }
    } else if ( hltObjectSummary->summarizedObjectCLID() == 10010 ) {
      // Track-like -> add to the bucket.
      bucket_of_ids.emplace_back( hltObjectSummary->lhcbIDs() );

      // NOTE it seems that the lhcbIDs() and lhcbIDsFlattened() method do the same thing, or the case where they differ
      // is unknown to me so we'll put this to catch when this case arises:
      assert( hltObjectSummary->lhcbIDs() == hltObjectSummary->lhcbIDsFlattened() );
    } else if ( hltObjectSummary->summarizedObjectCLID() != 2003 ) {
      // 2003 is CaloCluster - see LHCb/Event/RecEvent/include/Event/CaloCluster.h
      // This algorithm only matches track hits, so ignore 2003
      // Not expecting anything else in the SelReport, so fail if find anything else
      throw GaudiException{ "Dont know how to handle object with summarizedObjectCLID " +
                                std::to_string( hltObjectSummary->summarizedObjectCLID() ),
                            "Hlt1TrueSimEffAlg::operator()", StatusCode::FAILURE };
    }
  }

} // namespace detail

template <typename MCParticles>
class Hlt1TrueSimEffAlg : public detail::base_class_hlt1_t<MCParticles> {
public:
  using KeyValue = typename detail::base_class_hlt1_t<MCParticles>::KeyValue;
  Hlt1TrueSimEffAlg( const std::string& name, ISvcLocator* pSvc )
      : detail::base_class_hlt1_t<MCParticles>{ name,
                                                pSvc,
                                                { KeyValue{ "InputMCParticles", "" }, KeyValue{ "DecReports", "" },
                                                  KeyValue{ "MC2IDLink", "" }, KeyValue{ "SelReports", "" } },
                                                { KeyValue{ "P2TrueSimEffTable", "" } } } {}
  StatusCode initialize() override;

  detail::Table_t<MCParticles> operator()( const MCParticles& input_mcps, const LHCb::HltDecReports& dec_reports,
                                           const LHCb::LinksByKey&    mcp_id_link,
                                           const LHCb::HltSelReports& sel_reports ) const override;

private:
  Gaudi::Property<detail::vec_str_t> m_line_decisions{
      this, "TriggerLines", {}, "List of trigger lines for which TrueSim effs are required." };
  Gaudi::Property<float> m_match_threshold{ this, "match_fraction", 0.7,
                                            "Match fraction of LHCbIDs between trigger candidate and MCParticle. "
                                            "Obtained from dedicated study; see HltEfficiencyChecker documentation." };
};

template <typename MCParticles>
StatusCode Hlt1TrueSimEffAlg<MCParticles>::initialize() {
  return detail::base_class_hlt1_t<MCParticles>::initialize().andThen( [&]() -> StatusCode {
    /// Check that the line decisions are non-empty
    if ( m_line_decisions.empty() )
      throw GaudiException{ "No Hlt line decisions have been specified. Please check!",
                            "Hlt1TrueSimEffAlg::initialize()", StatusCode::FAILURE };

    /// check that the m_line_decisions have a suffix "Decision" if not throw as error
    for ( auto& line : m_line_decisions )
      if ( line.find( "Decision" ) == std::string::npos )
        throw GaudiException{ "The line decision " + line + " does not have a suffix 'Decision'. Please check!",
                              "Hlt1TrueSimEffAlg::initialize()", StatusCode::FAILURE };

    return StatusCode::SUCCESS;
  } );
}

template <typename MCParticles>
detail::Table_t<MCParticles>
Hlt1TrueSimEffAlg<MCParticles>::operator()( const MCParticles& input_mcps, const LHCb::HltDecReports& dec_reports,
                                            const LHCb::LinksByKey&    mcp_id_link,
                                            const LHCb::HltSelReports& sel_reports ) const {
  /// Create an instance of relations table i.e. map of particle -> TrueSim decisions
  detail::Table_t<MCParticles> mcp2truesimeffs;

  for ( const auto* input_mcp : input_mcps ) {

    // Get the LHCbIDs from the MCParticle
    detail::WeightedIDsByMCPTrack weighted_mcp_IDs{};
    detail::lhcbids_from_mcparticle( *input_mcp, mcp_id_link, weighted_mcp_IDs );

    detail::map_t map_effs;
    for ( const std::string& line_decision : m_line_decisions ) {
      map_effs[line_decision] = false; // Start from this assumption; change to true if all below logic is affirmative

      const auto dec_ptr = dec_reports.decReport( line_decision );
      if ( !dec_ptr )
        throw GaudiException{ "The HltDecReport for the line " + line_decision + " is null. Please check!",
                              "Hlt1TrueSimEffAlg::operator()", StatusCode::FAILURE };

      if ( !dec_ptr->decision() ) {
        // No point looking for candidates if trigger didn't fire
        continue;
      }

      const auto hltObjectSummary = sel_reports.selReport( line_decision );
      if ( !hltObjectSummary )
        throw GaudiException{ "No HltSelReport found for " + line_decision +
                                  " even though trigger fired. Please check!",
                              "Hlt1TrueSimEffAlg::operator()", StatusCode::FAILURE };

      if ( hltObjectSummary->summarizedObjectCLID() != 1 ) {
        throw GaudiException{
            "Not a selection summary. Dont know what to do with this object with summarizedObjectCLID " +
                std::to_string( hltObjectSummary->summarizedObjectCLID() ),
            "Hlt1TrueSimEffAlg::operator()", StatusCode::FAILURE };
      }

      const auto candidate_ids = [&]() {
        std::vector<detail::IDsByTrack> ret{};
        for ( const auto& subsummary : hltObjectSummary->substructure() ) {
          detail::IDsByTrack this_cand_ids{};
          detail::lhcbids_from_hos( subsummary, this_cand_ids );
          ret.emplace_back( this_cand_ids );
        }
        return ret;
      }();

      map_effs[line_decision] =
          std::any_of( candidate_ids.begin(), candidate_ids.end(), [&]( const auto& this_cand_ids ) {
            return detail::match_to_true_signal( this_cand_ids, weighted_mcp_IDs, m_match_threshold );
          } );
    }

    mcp2truesimeffs.relate( input_mcp, map_effs ).ignore();
  }
  return mcp2truesimeffs;
}

template <typename MCParticles>
class Hlt2TrueSimEffAlg : public detail::base_class_hlt2_t<MCParticles> {
public:
  using KeyValue = typename detail::base_class_hlt2_t<MCParticles>::KeyValue;
  Hlt2TrueSimEffAlg( const std::string& name, ISvcLocator* pSvc )
      : detail::base_class_hlt2_t<MCParticles>{
            name,
            pSvc,
            { KeyValue{ "InputMCParticles", "" }, KeyValue{ "DecReports", "" }, KeyValue{ "MC2IDLink", "" } },
            { KeyValue{ "P2TrueSimEffTable", "" } } } {}
  StatusCode initialize() override;

  detail::Table_t<MCParticles> operator()( const MCParticles& input_mcps, const LHCb::HltDecReports& dec_reports,
                                           const LHCb::LinksByKey& mcp_id_link ) const override;

private:
  Gaudi::Property<detail::vec_str_t> m_line_decisions{
      this, "TriggerLines", {}, "List of trigger lines for which TrueSim effs are required." };
  Gaudi::Property<detail::vec_str_t> m_candidateList{
      this, "CandidateLocations", {}, "List of TES locations for reconstructed candidates" };
  Gaudi::Property<float> m_match_threshold{ this, "match_fraction", 0.7,
                                            "Match fraction of LHCbIDs between trigger candidate and MCParticle. "
                                            "Obtained from dedicated study; see HltEfficiencyChecker documentation." };
  std::vector<std::unique_ptr<DataObjectReadHandle<LHCb::Particle::Range>>> m_candidateHandles;
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>                     m_neutral_warning{
      this, "Cannot match neutral particle to true MC signal (no reco'd track); TrueSim efficiencies will be skipped "
                                "for this candidate." };
};

template <typename MCParticles>
StatusCode Hlt2TrueSimEffAlg<MCParticles>::initialize() {
  return detail::base_class_hlt2_t<MCParticles>::initialize().andThen( [&]() -> StatusCode {
    if ( m_line_decisions.empty() )
      throw GaudiException{ "No Hlt line decisions have been specified. Please check!",
                            "Hlt2TrueSimEffAlg::initialize()", StatusCode::FAILURE };

    /// check that the m_line_decisions have a suffix "Decision" if not throw as error
    for ( auto& line : m_line_decisions )
      if ( line.find( "Decision" ) == std::string::npos )
        throw GaudiException{ "The line decision " + line + " does not have a suffix 'Decision'. Please check!",
                              "Hlt2TrueSimEffAlg::initialize()", StatusCode::FAILURE };

    for ( auto const& location : m_candidateList ) {
      m_candidateHandles.push_back( std::make_unique<DataObjectReadHandle<LHCb::Particle::Range>>( location, this ) );
      bool found_line = std::any_of( m_line_decisions.begin(), m_line_decisions.end(), [&]( const auto& line ) {
        return location.find( detail::remove_decision_suffix( line ) ) != location.npos;
      } );
      if ( !found_line ) {
        throw GaudiException{ "Passed trigger candidates at " + location +
                                  " which dont match to any of the lines in m_line_decisions.",
                              "Hlt2TrueSimEffAlg::initialize()", StatusCode::FAILURE };
      }
    }

    return StatusCode::SUCCESS;
  } );
}

template <typename MCParticles>
detail::Table_t<MCParticles> Hlt2TrueSimEffAlg<MCParticles>::operator()( const MCParticles&         input_mcps,
                                                                         const LHCb::HltDecReports& dec_reports,
                                                                         const LHCb::LinksByKey& mcp_id_link ) const {
  /// Create an instance of relations table i.e. map of particle -> TrueSimEff decisions
  detail::Table_t<MCParticles> mcp2truesimeffs;

  for ( const auto* input_mcp : input_mcps ) {

    // Get the LHCbIDs from the MCParticle
    detail::WeightedIDsByMCPTrack weighted_mcp_IDs{};
    detail::lhcbids_from_mcparticle( *input_mcp, mcp_id_link, weighted_mcp_IDs );

    detail::map_t map_effs;
    for ( const std::string& line_decision : m_line_decisions ) {
      map_effs[line_decision] = false; // Start from this assumption; change to true if all below logic is affirmative

      const auto dec_ptr = dec_reports.decReport( line_decision );
      if ( !dec_ptr )
        throw GaudiException{ "The HltDecReport for the line " + line_decision + " is null. Please check!",
                              "Hlt2TrueSimEffAlg::operator()", StatusCode::FAILURE };

      if ( !dec_ptr->decision() ) {
        // No point looking for candidates if trigger didn't fire
        continue;
      }

      auto candidate_handle = std::find_if(
          m_candidateHandles.begin(), m_candidateHandles.end(), [&line_decision]( const auto& cand_handle ) {
            return cand_handle->isValid() and
                   cand_handle->objKey().find( detail::remove_decision_suffix( line_decision ) ) != std::string::npos;
          } );
      if ( candidate_handle == m_candidateHandles.end() ) {
        throw GaudiException{ "Couldnt locate the trigger candidates for line " + line_decision + ". Please check!",
                              "Hlt2TrueSimEffAlg::operator()", StatusCode::FAILURE };
      }

      std::vector<detail::IDsByTrack> candidate_ids{};
      for ( const auto& cand : ( *candidate_handle )->get() ) {
        if ( !detail::lhcbids_from_reco_particle( *cand, candidate_ids.emplace_back() ) ) ++m_neutral_warning;
      }

      map_effs[line_decision] =
          std::any_of( candidate_ids.begin(), candidate_ids.end(), [&]( const auto& this_cand_ids ) {
            return detail::match_to_true_signal( this_cand_ids, weighted_mcp_IDs, m_match_threshold );
          } );
    }

    mcp2truesimeffs.relate( input_mcp, map_effs ).ignore();
  }
  return mcp2truesimeffs;
}

DECLARE_COMPONENT_WITH_ID( Hlt1TrueSimEffAlg<LHCb::MCParticle::Range>, "Hlt1TrueSimEffAlg" )
DECLARE_COMPONENT_WITH_ID( Hlt2TrueSimEffAlg<LHCb::MCParticle::Range>, "Hlt2TrueSimEffAlg" )
