/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/IDistanceCalculator.h"
#include "Kernel/ILifetimeFitter.h"

#include "LoKi/Constants.h"
#include "LoKi/GetTools.h"
#include "LoKi/Particles16.h"
#include "LoKi/Particles20.h"
#include "LoKi/Particles4.h"
#include "LoKi/PhysSources.h"
#include "LoKi/Vertices0.h"

#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/ToStream.h"

/**
 *  The implementation file for functions form the file LoKi/Particles20.h
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2008-01-16
 */

/// anonymous namespace to hide some pure technical stuff
namespace {

  /// "invalid" pointer
  const IDistanceCalculator* const s_IGEO = nullptr;
  /// "invalid" pointer
  const ILifetimeFitter* const s_LTIME = nullptr;

  /** @var s_IPTOOL
   *  the instance of "invalid" IP-tool
   */
  const LoKi::Vertices::ImpactParamTool s_IPTOOL( s_IGEO );

  /// "invalid" vertex
  const LHCb::VertexBase* const s_VERTEX = nullptr;

  /// the finder for the primary vertices:
  const LoKi::Vertices::IsPrimary s_PRIMARY = LoKi::Vertices::IsPrimary();

  /// the default name of Lifetime fitter:
  const std::string s_LIFETIME = "LoKi::LifetimeFitter/lifetime:PUBLIC";

} // end of anonymous namespace

// Default Constructor
LoKi::Particles::IsBestPVValid::IsBestPVValid( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) ), LoKi::AuxDesktopBase( algorithm ) {}

LoKi::Particles::IsBestPVValid* LoKi::Particles::IsBestPVValid::clone() const { return new IsBestPVValid( *this ); }

LoKi::Particles::IsBestPVValid::result_type
LoKi::Particles::IsBestPVValid::operator()( LoKi::Particles::IsBestPVValid::argument p ) const {
  if ( 0 != p ) { return ( 0 != bestVertex( p, desktop()->geometry() ) ); } // RETURN
  Error( "Invalid Particle, return false" ).ignore();
  return false; // RETURN
}

std::ostream& LoKi::Particles::IsBestPVValid::fillStream( std::ostream& s ) const { return s << "BPVVALID()"; }

// the default constructor: create the object in invalid state
LoKi::Particles::CosineDirectionAngleWithTheBestPV::CosineDirectionAngleWithTheBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::CosineDirectionAngle( s_VERTEX ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::CosineDirectionAngleWithTheBestPV* LoKi::Particles::CosineDirectionAngleWithTheBestPV::clone() const {
  return new CosineDirectionAngleWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::CosineDirectionAngleWithTheBestPV::result_type
LoKi::Particles::CosineDirectionAngleWithTheBestPV::operator()(
    LoKi::Particles::CosineDirectionAngleWithTheBestPV::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return dira( p );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::CosineDirectionAngleWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVDIRA";
}

// the default constructor: create the object in invalid state
LoKi::Particles::TzWithTheBestPV::TzWithTheBestPV( const IDVAlgorithm* algorithm, const double mass )
    : LoKi::AuxFunBase( std::tie( algorithm, mass ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::Tz( s_VERTEX ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::TzWithTheBestPV* LoKi::Particles::TzWithTheBestPV::clone() const {
  return new TzWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::TzWithTheBestPV::result_type
LoKi::Particles::TzWithTheBestPV::operator()( LoKi::Particles::TzWithTheBestPV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return tz( *p );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::TzWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVTZ(" << mass() << ")";
}

// the default constructor, creates the object in invalid state
LoKi::Particles::ImpParWithTheBestPV::ImpParWithTheBestPV( const IDVAlgorithm* algorithm, const std::string& geo )
    : LoKi::AuxFunBase( std::tie( algorithm, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, s_VERTEX, s_IPTOOL )
    , m_geo( geo ) {
  if ( 0 == tool() && gaudi() ) { setTool( LoKi::GetTools::distanceCalculator( *this, geo ) ); }
}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::ImpParWithTheBestPV* LoKi::Particles::ImpParWithTheBestPV::clone() const {
  return new ImpParWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::ImpParWithTheBestPV::result_type
LoKi::Particles::ImpParWithTheBestPV::operator()( LoKi::Particles::ImpParWithTheBestPV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the IDistanceCalculator from IDVAlgorithm
  if ( 0 == tool() ) { setTool( LoKi::GetTools::distanceCalculator( *this, geo() ) ); }
  // check it!
  Assert( 0 != tool(), "No valid IDistanceCalculator is found" );
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return ip( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::ImpParWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVIP ('" << m_geo << "')";
}

// the default constructor, creates the object in invalid state
LoKi::Particles::ImpParChi2WithTheBestPV::ImpParChi2WithTheBestPV( const IDVAlgorithm* algorithm,
                                                                   const std::string&  geo )
    : LoKi::AuxFunBase( std::tie( algorithm, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpParWithTheBestPV( algorithm, geo ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::ImpParChi2WithTheBestPV* LoKi::Particles::ImpParChi2WithTheBestPV::clone() const {
  return new ImpParChi2WithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::ImpParChi2WithTheBestPV::result_type
LoKi::Particles::ImpParChi2WithTheBestPV::operator()( LoKi::Particles::ImpParChi2WithTheBestPV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the IDistanceCalculator from IDVAlgorithm
  if ( 0 == tool() ) { setTool( LoKi::GetTools::distanceCalculator( *this, geo() ) ); }
  // check it!
  Assert( 0 != tool(), "No valid IDistanceCalculator is found" );
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return ipchi2( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::ImpParChi2WithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVIPCHI2 ('" << geo() << "')";
}

/*  constructor from the source and nickname or full type/name of
 *  IDistanceCalculator tool
 *  @see IDVAlgorithm::distanceCalculator
 *  @param source the source
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParWithSource::MinImpParWithSource(
    const IDVAlgorithm* algorithm, const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source& source,
    const std::string& geo )
    : LoKi::AuxFunBase( std::tie( source, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpPar( algorithm, LHCb::VertexBase::ConstVector(), s_IPTOOL )
    , m_source( source )
    , m_geo( geo ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::MinImpParWithSource* LoKi::Particles::MinImpParWithSource::clone() const {
  return new MinImpParWithSource( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinImpParWithSource::result_type
LoKi::Particles::MinImpParWithSource::operator()( LoKi::Particles::MinImpParWithSource::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the IDistanceCalculator from IDVAlgorithm
  if ( 0 == tool() ) { setTool( LoKi::GetTools::distanceCalculator( *this, geo() ) ); }
  // check it!
  Assert( 0 != tool(), "No valid IDistanceCalculator is found" );
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::VertexBase> KEEPER;
    const KEEPER&                                keep1 = *this;
    KEEPER&                                      keep2 = const_cast<KEEPER&>( keep1 );

    // clear the list of vertices
    keep2.clear();
    // get the primary vertices from the source
    LHCb::VertexBase::ConstVector primaries = source()(); // NB!
    // fill the functor with primary vertices:
    keep2.addObjects( primaries.begin(), primaries.end() );
    if ( empty() ) { Warning( "Empty list of vertices is loaded!" ).ignore(); }
    // update the event:
    setEvent();
  }
  // use the functor
  return mip( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinImpParWithSource::fillStream( std::ostream& s ) const {
  return s << "MIPSOURCE ('" << source() << ", " << geo() << "')";
}

/* the "default" constructor,
 *  gets the IDistanceCalculator tool from IDVAlgorithm by nickname or
 *  by full type/name
 *  @see IDVAlgorithm::distanceCalculator
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParDV::MinImpParDV( const IDVAlgorithm* algorithm, const std::string& geo )
    : LoKi::AuxFunBase( std::tie( algorithm, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParWithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, s_PRIMARY ), geo ) {}

/*  the constructor form the vertex selection functor and
 *  the name/nickname of IDistanceCalculator tool from IDVAlgorithm
 *  @see IDVAlgorithm::distanceCalculator
 *  @param
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParDV::MinImpParDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cuts,
                                           const std::string& geo )
    : LoKi::AuxFunBase( std::tie( algorithm, cuts, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParWithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, cuts ), geo ) {}

/*  the constructor form the vertex selection functor and
 *  the name/nickname of IDistanceCalculator tool from IDVAlgorithm
 *  @see IDVAlgorithm::distanceCalculator
 *  @param
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParDV::MinImpParDV( const IDVAlgorithm* algorithm, const std::string& geo,
                                           const LoKi::PhysTypes::VCuts& cuts )
    : LoKi::AuxFunBase( std::tie( algorithm, geo, cuts ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParWithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, cuts ), geo ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::MinImpParDV* LoKi::Particles::MinImpParDV::clone() const { return new MinImpParDV( *this ); }

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinImpParDV::fillStream( std::ostream& s ) const {
  return s << "MIPDV('" << geo() << "')";
}

/*  constructor from the source and nickname or full type/name of
 *  IDistanceCalculator tool
 *  @see IDVAlgorithm::distanceCalculator
 *  @param source the source
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParChi2WithSource::MinImpParChi2WithSource(
    const IDVAlgorithm* algorithm, const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source& source,
    const std::string& geo )
    : LoKi::AuxFunBase( std::tie( source, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParWithSource( algorithm, source, geo ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::MinImpParChi2WithSource* LoKi::Particles::MinImpParChi2WithSource::clone() const {
  return new MinImpParChi2WithSource( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinImpParChi2WithSource::result_type
LoKi::Particles::MinImpParChi2WithSource::operator()( LoKi::Particles::MinImpParChi2WithSource::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the IDistanceCalculator from IDVAlgorithm
  if ( 0 == tool() ) { setTool( LoKi::GetTools::distanceCalculator( *this, geo() ) ); }
  // check it!
  Assert( 0 != tool(), "No valid IDistanceCalculator is found" );
  // check the event
  if ( !sameEvent() ) {
    typedef LoKi::UniqueKeeper<LHCb::VertexBase> KEEPER;
    const KEEPER&                                keep1 = *this;
    KEEPER&                                      keep2 = const_cast<KEEPER&>( keep1 );

    // clear the list of vertices
    keep2.clear();
    // get the primary vertices from the source
    LHCb::VertexBase::ConstVector primaries = source()(); // NB!!
    // fill the functor with primary vertices:
    keep2.addObjects( primaries.begin(), primaries.end() );
    if ( empty() ) { Error( "Empty list of vertices is loaded!" ).ignore(); }
    // update the event:
    setEvent();
  }
  // use the functor
  return mipchi2( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinImpParChi2WithSource::fillStream( std::ostream& s ) const {
  return s << "MIPCHI2SOURCE ('" << source() << ", " << geo() << "')";
}

/* the "default" constructor,
 *  gets the IDistanceCalculator tool from IDVAlgorithm by nickname or
 *  by full type/name
 *  @see IDVAlgorithm::distanceCalculator
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParChi2DV::MinImpParChi2DV( const IDVAlgorithm* algorithm, const std::string& geo )
    : LoKi::AuxFunBase( std::tie( algorithm, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParChi2WithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, s_PRIMARY ),
                                                geo ) {}

/*  the constructor,
 *  gets the IDistanceCalculator tool from IDVAlgorithm by nickname or
 *  by full type/name
 *  @see IDVAlgorithm::distanceCalculator
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParChi2DV::MinImpParChi2DV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cuts,
                                                   const std::string& geo )
    : LoKi::AuxFunBase( std::tie( algorithm, cuts, geo ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParChi2WithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, cuts ), geo ) {}

/*  the  constructor,
 *  gets the IDistanceCalculator tool from IDVAlgorithm by nickname or
 *  by full type/name
 *  @see DVAlgorithm::distanceCalculator
 *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
 */
LoKi::Particles::MinImpParChi2DV::MinImpParChi2DV( const IDVAlgorithm* algorithm, const std::string& geo,
                                                   const LoKi::PhysTypes::VCuts& cuts )
    : LoKi::AuxFunBase( std::tie( algorithm, geo, cuts ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MinImpParChi2WithSource( algorithm, LoKi::Vertices::SourceDesktop( algorithm, cuts ), geo ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::MinImpParChi2DV* LoKi::Particles::MinImpParChi2DV::clone() const {
  return new MinImpParChi2DV( *this );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinImpParChi2DV::fillStream( std::ostream& s ) const {
  return s << "MIPCHI2DV('" << geo() << "')";
}

// the default constructor
LoKi::Particles::VertexDistanceDV::VertexDistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::VertexDistance( s_VERTEX ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexDistanceDV* LoKi::Particles::VertexDistanceDV::clone() const {
  return new LoKi::Particles::VertexDistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexDistanceDV::result_type
LoKi::Particles::VertexDistanceDV::operator()( LoKi::Particles::VertexDistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* poiunts to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return distance( p ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexDistanceDV::fillStream( std::ostream& s ) const { return s << "BPVVD"; }

// the default constructor
LoKi::Particles::VertexSignedDistanceDV::VertexSignedDistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::VertexSignedDistance( s_VERTEX ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexSignedDistanceDV* LoKi::Particles::VertexSignedDistanceDV::clone() const {
  return new LoKi::Particles::VertexSignedDistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexSignedDistanceDV::result_type
LoKi::Particles::VertexSignedDistanceDV::operator()( LoKi::Particles::VertexSignedDistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* poiunts to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return signedDistance( p ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexSignedDistanceDV::fillStream( std::ostream& s ) const { return s << "BPVVDSIGN"; }

// the default constructor
LoKi::Particles::VertexDotDistanceDV::VertexDotDistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::VertexDotDistance( s_VERTEX ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexDotDistanceDV* LoKi::Particles::VertexDotDistanceDV::clone() const {
  return new LoKi::Particles::VertexDotDistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexDotDistanceDV::result_type
LoKi::Particles::VertexDotDistanceDV::operator()( LoKi::Particles::VertexDotDistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* poiunts to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return distance( p ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexDotDistanceDV::fillStream( std::ostream& s ) const { return s << "BPVVDDOT"; }

// the default constructor
LoKi::Particles::VertexChi2DistanceDV::VertexChi2DistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::VertexChi2Distance( s_VERTEX ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexChi2DistanceDV* LoKi::Particles::VertexChi2DistanceDV::clone() const {
  return new LoKi::Particles::VertexChi2DistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexChi2DistanceDV::result_type
LoKi::Particles::VertexChi2DistanceDV::operator()( LoKi::Particles::VertexChi2DistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* poiunts to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return chi2( p ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexChi2DistanceDV::fillStream( std::ostream& s ) const { return s << "BPVVDCHI2"; }

// constructor
LoKi::Particles::LifeTimeDV::LifeTimeDV( const IDVAlgorithm* algorithm, const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTime( algorithm, algorithm ? algorithm->lifetimeFitter() : s_LTIME, s_VERTEX, chi2 )
    , m_fit( s_LIFETIME ) {
  // check the tool
  if ( 0 == tool() && gaudi() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
}

// constructor
LoKi::Particles::LifeTimeDV::LifeTimeDV( const IDVAlgorithm* algorithm, const std::string& fit, const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, fit, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTime( algorithm, algorithm ? algorithm->lifetimeFitter( fit ) : s_LTIME, s_VERTEX, chi2 )
    , m_fit( fit ) {
  // check the tool
  if ( 0 == tool() && gaudi() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::LifeTimeDV* LoKi::Particles::LifeTimeDV::clone() const {
  return new LoKi::Particles::LifeTimeDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::LifeTimeDV::result_type
LoKi::Particles::LifeTimeDV::operator()( LoKi::Particles::LifeTimeDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidTime" ).ignore();
    return LoKi::Constants::InvalidTime;
  }
  // check the tool
  if ( 0 == tool() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
  // check the fitter
  Assert( 0 != tool(), "No Valid ILifetimeFitter is availabe" );
  // get the vertex from desktop
  const LHCb::VertexBase* vx = bestVertex( p, desktop()->geometry() );
  if ( 0 == vx ) {
    Error( "LHCb::VertexBase* points to NULL, return InvalidTime" ).ignore();
    return LoKi::Constants::InvalidTime;
  }
  setVertex( vx );
  // use the functor
  return lifeTime( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::LifeTimeDV::fillStream( std::ostream& s ) const {
  s << " BPVLTIME('" << fitter() << "'";
  if ( 0 < chi2cut() ) { s << "," << chi2cut(); }
  return s << ") ";
}

// constructor
LoKi::Particles::LifeTimeChi2DV::LifeTimeChi2DV( const IDVAlgorithm* algorithm, const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm, chi2 ) {}

// constructor
LoKi::Particles::LifeTimeChi2DV::LifeTimeChi2DV( const IDVAlgorithm* algorithm, const std::string& fit,
                                                 const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, fit, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm, fit, chi2 ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::LifeTimeChi2DV* LoKi::Particles::LifeTimeChi2DV::clone() const {
  return new LoKi::Particles::LifeTimeChi2DV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::LifeTimeChi2DV::result_type
LoKi::Particles::LifeTimeChi2DV::operator()( LoKi::Particles::LifeTimeChi2DV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  // check the tool
  if ( 0 == tool() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
  // check the fitter
  Assert( 0 != tool(), "No Valid ILifetimeFitter is availabe" );
  // get the vertex from desktop
  const LHCb::VertexBase* vx = bestVertex( p, desktop()->geometry() );
  if ( 0 == vx ) {
    Error( "LHCb::VertexBase* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  setVertex( vx );
  // use the functor
  return lifeTimeChi2( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::LifeTimeChi2DV::fillStream( std::ostream& s ) const {
  s << " BPVLTCHI2('" << fitter() << "'";
  if ( 0 < chi2cut() ) { s << "," << chi2cut(); }
  return s << ") ";
}

// constructor
LoKi::Particles::LifeTimeSignedChi2DV::LifeTimeSignedChi2DV( const IDVAlgorithm* algorithm, const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeChi2DV( algorithm, chi2 ) {}

// constructor
LoKi::Particles::LifeTimeSignedChi2DV::LifeTimeSignedChi2DV( const IDVAlgorithm* algorithm, const std::string& fit,
                                                             const double chi2 )
    : LoKi::AuxFunBase( std::tie( algorithm, fit, chi2 ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeChi2DV( algorithm, fit, chi2 ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::LifeTimeSignedChi2DV* LoKi::Particles::LifeTimeSignedChi2DV::clone() const {
  return new LoKi::Particles::LifeTimeSignedChi2DV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::LifeTimeSignedChi2DV::result_type
LoKi::Particles::LifeTimeSignedChi2DV::operator()( LoKi::Particles::LifeTimeSignedChi2DV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  // check the tool
  if ( 0 == tool() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
  // check the fitter
  Assert( 0 != tool(), "No Valid ILifetimeFitter is availabe" );
  // get the vertex from desktop
  const LHCb::VertexBase* vx = bestVertex( p, desktop()->geometry() );
  if ( 0 == vx ) {
    Error( "LHCb::VertexBase* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  setVertex( vx );
  // use the functor
  return lifeTimeSignedChi2( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::LifeTimeSignedChi2DV::fillStream( std::ostream& s ) const {
  s << " BPVLTSIGNCHI2('" << fitter() << "'";
  if ( 0 < chi2cut() ) { s << "," << chi2cut(); }
  return s << ") ";
}

// constructor
LoKi::Particles::LifeTimeFitChi2DV::LifeTimeFitChi2DV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm ) {}

// constructor
LoKi::Particles::LifeTimeFitChi2DV::LifeTimeFitChi2DV( const IDVAlgorithm* algorithm, const std::string& fit )
    : LoKi::AuxFunBase( std::tie( algorithm, fit ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm, fit ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::LifeTimeFitChi2DV* LoKi::Particles::LifeTimeFitChi2DV::clone() const {
  return new LoKi::Particles::LifeTimeFitChi2DV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::LifeTimeFitChi2DV::result_type
LoKi::Particles::LifeTimeFitChi2DV::operator()( LoKi::Particles::LifeTimeFitChi2DV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  // check the tool
  if ( 0 == tool() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
  // check the fitter
  Assert( 0 != tool(), "No Valid ILifetimeFitter is availabe" );
  // get the vertex from desktop
  const LHCb::VertexBase* vx = bestVertex( p, desktop()->geometry() );
  if ( 0 == vx ) {
    Error( "LHCb::VertexBase* points to NULL, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  setVertex( vx );
  // use the functor
  return lifeTimeFitChi2( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::LifeTimeFitChi2DV::fillStream( std::ostream& s ) const {
  return s << "BPVLTFITCHI2('" << fitter() << "')";
}

// constructor
LoKi::Particles::LifeTimeErrorDV::LifeTimeErrorDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm ) {}

// constructor
LoKi::Particles::LifeTimeErrorDV::LifeTimeErrorDV( const IDVAlgorithm* algorithm, const std::string& fit )
    : LoKi::AuxFunBase( std::tie( algorithm, fit ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::LifeTimeDV( algorithm, fit ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::LifeTimeErrorDV* LoKi::Particles::LifeTimeErrorDV::clone() const {
  return new LoKi::Particles::LifeTimeErrorDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::LifeTimeErrorDV::result_type
LoKi::Particles::LifeTimeErrorDV::operator()( LoKi::Particles::LifeTimeErrorDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidTime" ).ignore();
    return LoKi::Constants::InvalidTime;
  }
  // check the tool
  if ( 0 == tool() ) { setTool( LoKi::GetTools::lifetimeFitter( *this, fitter() ) ); }
  // check the fitter
  Assert( 0 != tool(), "No Valid ILifetimeFitter is availabe" );
  // get the vertex from desktop
  const LHCb::VertexBase* vx = bestVertex( p, desktop()->geometry() );
  if ( 0 == vx ) {
    Error( "LHCb::VertexBase* points to NULL, return InvalidTime" ).ignore();
    return LoKi::Constants::InvalidTime;
  }
  setVertex( vx );
  // use the functor
  return lifeTimeError( p, desktop()->geometry() );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::LifeTimeErrorDV::fillStream( std::ostream& s ) const {
  return s << "BPVLTERR('" << fitter() << "')";
}

// the default constructor
LoKi::Particles::VertexZDistanceWithTheBestPV::VertexZDistanceWithTheBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) ), LoKi::AuxDesktopBase( algorithm ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexZDistanceWithTheBestPV* LoKi::Particles::VertexZDistanceWithTheBestPV::clone() const {
  return new LoKi::Particles::VertexZDistanceWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexZDistanceWithTheBestPV::result_type LoKi::Particles::VertexZDistanceWithTheBestPV::operator()(
    LoKi::Particles::VertexZDistanceWithTheBestPV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  const LHCb::VertexBase* ev = p->endVertex();
  if ( 0 == ev ) {
    Error( "LHCb::Particle::endVertex points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  const LHCb::VertexBase* bpv = bestVertex( p, desktop()->geometry() );
  if ( 0 == bpv ) {
    Error( "Related points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  return ev->position().Z() - bpv->position().Z(); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexZDistanceWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVVDZ";
}

// the default constructor
LoKi::Particles::VertexRhoDistanceWithTheBestPV::VertexRhoDistanceWithTheBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) ), LoKi::AuxDesktopBase( algorithm ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::VertexRhoDistanceWithTheBestPV* LoKi::Particles::VertexRhoDistanceWithTheBestPV::clone() const {
  return new LoKi::Particles::VertexRhoDistanceWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::VertexRhoDistanceWithTheBestPV::result_type
LoKi::Particles::VertexRhoDistanceWithTheBestPV::operator()(
    LoKi::Particles::VertexRhoDistanceWithTheBestPV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  const LHCb::VertexBase* ev = p->endVertex();
  if ( 0 == ev ) {
    Error( "LHCb::Particle::endVertex points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  // get the best vertex from desktop and use it
  const LHCb::VertexBase* bpv = bestVertex( p, desktop()->geometry() );
  if ( 0 == bpv ) {
    Error( "Related points to NULL, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  return ( ev->position() - bpv->position() ).Rho(); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::VertexRhoDistanceWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVVDRHO";
}

// constructor from the source
LoKi::Particles::MinVertexDistanceWithSource::MinVertexDistanceWithSource(
    const LoKi::Particles::MinVertexDistanceWithSource::_Source& s )
    : LoKi::AuxFunBase( std::tie( s ) ), m_fun( s ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MinVertexDistanceWithSource* LoKi::Particles::MinVertexDistanceWithSource::clone() const {
  return new LoKi::Particles::MinVertexDistanceWithSource( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinVertexDistanceWithSource::result_type LoKi::Particles::MinVertexDistanceWithSource::operator()(
    LoKi::Particles::MinVertexDistanceWithSource::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return 'HugeDistance'" ).ignore();
    return LoKi::Constants::HugeDistance; // RETURN
  }
  const LHCb::VertexBase* v = p->endVertex();
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'HugeDistance'" ).ignore();
    return LoKi::Constants::HugeDistance; // RETURN
  }
  // use the functor!
  return m_fun.minvd( v ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinVertexDistanceWithSource::fillStream( std::ostream& s ) const {
  return s << "MINVDSOURCE(" << m_fun.source() << ")";
}

// constructor from the source
LoKi::Particles::MinVertexChi2DistanceWithSource::MinVertexChi2DistanceWithSource(
    const LoKi::Particles::MinVertexChi2DistanceWithSource::_Source& s )
    : LoKi::AuxFunBase( std::tie( s ) ), m_fun( s ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MinVertexChi2DistanceWithSource* LoKi::Particles::MinVertexChi2DistanceWithSource::clone() const {
  return new LoKi::Particles::MinVertexChi2DistanceWithSource( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinVertexChi2DistanceWithSource::result_type
LoKi::Particles::MinVertexChi2DistanceWithSource::operator()(
    LoKi::Particles::MinVertexChi2DistanceWithSource::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return 'HugeChi2'" ).ignore();
    return LoKi::Constants::HugeChi2; // RETURN
  }
  const LHCb::VertexBase* v = p->endVertex();
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'HugeChi2'" ).ignore();
    return LoKi::Constants::HugeChi2; // RETURN
  }
  // use the functor!
  return m_fun.minvdchi2( v ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinVertexChi2DistanceWithSource::fillStream( std::ostream& s ) const {
  return s << "MINVDCHI2SOURCE(" << m_fun.source() << ")";
}

// the default constructor
LoKi::Particles::MinVertexDistanceDV::MinVertexDistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) ), m_fun( algorithm, s_PRIMARY ) {}

// the constructor from the vertex filter
LoKi::Particles::MinVertexDistanceDV::MinVertexDistanceDV( const IDVAlgorithm*           algorithm,
                                                           const LoKi::PhysTypes::VCuts& cut )
    : LoKi::AuxFunBase( std::tie( algorithm, cut ) ), m_fun( algorithm, cut ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MinVertexDistanceDV* LoKi::Particles::MinVertexDistanceDV::clone() const {
  return new LoKi::Particles::MinVertexDistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinVertexDistanceDV::result_type
LoKi::Particles::MinVertexDistanceDV::operator()( LoKi::Particles::MinVertexDistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return 'HugeDistance'" ).ignore();
    return LoKi::Constants::HugeDistance; // RETURN
  }
  const LHCb::VertexBase* v = p->endVertex();
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'HugeDistance'" ).ignore();
    return LoKi::Constants::HugeDistance; // RETURN
  }
  // use the functor!
  return m_fun.minvd( v ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinVertexDistanceDV::fillStream( std::ostream& s ) const {
  return s << "MINVDDV(" << m_fun.cut() << ")";
}

// the default constructor
LoKi::Particles::MinVertexChi2DistanceDV::MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) ), m_fun( algorithm, s_PRIMARY ) {}

// the constructor from the vertex filter
LoKi::Particles::MinVertexChi2DistanceDV::MinVertexChi2DistanceDV( const IDVAlgorithm*           algorithm,
                                                                   const LoKi::PhysTypes::VCuts& cut )
    : LoKi::AuxFunBase( std::tie( algorithm, cut ) ), m_fun( algorithm, cut ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MinVertexChi2DistanceDV* LoKi::Particles::MinVertexChi2DistanceDV::clone() const {
  return new LoKi::Particles::MinVertexChi2DistanceDV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MinVertexChi2DistanceDV::result_type
LoKi::Particles::MinVertexChi2DistanceDV::operator()( LoKi::Particles::MinVertexChi2DistanceDV::argument p ) const {
  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL, return 'HugeChi2'" ).ignore();
    return LoKi::Constants::HugeChi2; // RETURN
  }
  const LHCb::VertexBase* v = p->endVertex();
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* points to NULL, return 'HugeChi2'" ).ignore();
    return LoKi::Constants::HugeChi2; // RETURN
  }
  // use the functor!
  return m_fun.minvdchi2( v ); // RETURN
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MinVertexChi2DistanceDV::fillStream( std::ostream& s ) const {
  return s << "MINVDCHI2DV(" << m_fun.cut() << ")";
}

// the default constructor: create the object in invalid state
LoKi::Particles::TrgPointingScoreWithBestPV::TrgPointingScoreWithBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::TrgPointingScore( s_VERTEX ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::TrgPointingScoreWithBestPV* LoKi::Particles::TrgPointingScoreWithBestPV::clone() const {
  return new TrgPointingScoreWithBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::TrgPointingScoreWithBestPV::result_type LoKi::Particles::TrgPointingScoreWithBestPV::operator()(
    LoKi::Particles::TrgPointingScoreWithBestPV::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, retuen -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return pointing( p );
}

// the default constructor: create the object in invalid state
LoKi::Particles::PseudoRapidityWithTheBestPV::PseudoRapidityWithTheBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PseudoRapidityFromVertex( s_VERTEX ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::PseudoRapidityWithTheBestPV* LoKi::Particles::PseudoRapidityWithTheBestPV::clone() const {
  return new PseudoRapidityWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::PseudoRapidityWithTheBestPV::result_type LoKi::Particles::PseudoRapidityWithTheBestPV::operator()(
    LoKi::Particles::PseudoRapidityWithTheBestPV::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return eta( p );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::PseudoRapidityWithTheBestPV::fillStream( std::ostream& s ) const {
  return s << "BPVETA";
}

// the default constructor: create the object in invalid state
LoKi::Particles::PhiWithTheBestPV::PhiWithTheBestPV( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PseudoRapidityWithTheBestPV( algorithm ) {}

// MANDATORY: the clone method ("virtual constructor")
LoKi::Particles::PhiWithTheBestPV* LoKi::Particles::PhiWithTheBestPV::clone() const {
  return new PhiWithTheBestPV( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::PhiWithTheBestPV::result_type
LoKi::Particles::PhiWithTheBestPV::operator()( LoKi::Particles::PhiWithTheBestPV::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL, return -1000" ).ignore();
    return -1000; // RETURN
  }
  // get the best vertex from desktop and use it
  setVertex( bestVertex( p, desktop()->geometry() ) );
  return phi( p );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::PhiWithTheBestPV::fillStream( std::ostream& s ) const { return s << "BPVPHI"; }
