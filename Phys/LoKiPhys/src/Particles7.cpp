/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/ToStream.h"
// ============================================================================
// LoKiCore
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles7.h"
// ============================================================================
/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-21
 */
// ============================================================================
LoKi::Particles::VFunAsPFun::VFunAsPFun( const LoKi::Types::VFunc& vfun, const double bad )
    : LoKi::AuxFunBase( std::tie( vfun, bad ) ), m_vfun( vfun ), m_bad( bad ) {}
// ===========================================================================
LoKi::Particles::VFunAsPFun* LoKi::Particles::VFunAsPFun::clone() const { return new VFunAsPFun( *this ); }
// ===========================================================================
LoKi::Particles::VFunAsPFun::result_type
LoKi::Particles::VFunAsPFun::operator()( const LoKi::Particles::VFunAsPFun::argument p ) const {
  if ( p ) { return m_vfun( p->endVertex() ); } // RETURN
  Error( "Invalid Particle, return " + Gaudi::Utils::toString( m_bad ) ).ignore();
  return m_bad; // RETURN
}
// ===========================================================================
std::ostream& LoKi::Particles::VFunAsPFun::fillStream( std::ostream& s ) const {
  return s << "VFASPF(" << m_vfun << "," << m_bad << ")";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
