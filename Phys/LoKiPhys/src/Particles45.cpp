/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"

#include "Kernel/RelatedInfoNamed.h"

// local
#include "LoKi/ILoKiSvc.h"
#include "LoKi/Particles45.h"
//
LoKi::Particles::RelatedInfo::RelatedInfo( const std::string& location, const short index, const double bad )
    : LoKi::AuxFunBase( std::tie( location, index, bad ) ), m_location( location ), m_index( index ), m_bad( bad ) {}
//
LoKi::Particles::RelatedInfo::RelatedInfo( const std::string& location, const std::string& variable, const double bad )
    : LoKi::AuxFunBase( std::tie( location, variable, bad ) ), m_location( location ), m_bad( bad ) {
  const auto index = RelatedInfoNamed::indexByName( variable );
  if ( index == RelatedInfoNamed::UNKNOWN ) { Warning( "RelatedInfo variable " + variable + " unknown" ).ignore(); }
  m_index = index;
}
//
LoKi::Particles::RelatedInfo* LoKi::Particles::RelatedInfo::clone() const {
  return new LoKi::Particles::RelatedInfo( *this );
}
//
// MANDATORY: the only one essential method
LoKi::Particles::RelatedInfo::result_type
LoKi::Particles::RelatedInfo::operator()( LoKi::Particles::RelatedInfo::argument p ) const {
  if ( !p ) {
    Error( "Invalid particle, return ..." ).ignore();
    return -2000;
  }
  //
  if ( !m_table || !sameEvent() ) {
    SmartIF<IDataProviderSvc> ds( lokiSvc().getObject() );
    SmartDataPtr<IMAP>        data( ds, m_location );
    if ( !data ) {
      Warning( "No table at location " + m_location ).ignore();
      return -2000;
    }
    m_table = data;
    setEvent();
  }
  //
  const auto& r = m_table->relations( p );
  if ( r.empty() ) {
    Warning( "No entry for particle" ).ignore();
    return m_bad;
  }
  if ( 1 != r.size() ) {
    Warning( ">1 entry for particle" ).ignore();
    return m_bad;
  }
  const auto& m = r[0].to();
  //
  return m.info( m_index, m_bad );
}
/// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::RelatedInfo::fillStream( std::ostream& s ) const {
  return s << "RELINFO('" << m_location << "'," << m_index << "," << m_bad << ")";
}
