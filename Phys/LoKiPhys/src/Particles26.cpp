/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/IDistanceCalculator.h"

#include "LoKi/Child.h"
#include "LoKi/Constants.h"
#include "LoKi/GetTools.h"
#include "LoKi/ILoKiSvc.h"
#include "LoKi/Interface.h"
#include "LoKi/Particles26.h"

/**
 *  Implementation file for functions form file LoKi/Particles26
 *  @date 2009-04-24
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
namespace {

  /// the invalid particle:
  const LHCb::Particle* const s_PARTICLE = nullptr;

  /// the invalid vertex:
  const LHCb::VertexBase* const s_VERTEX = nullptr;

  /// the invalid tool
  const IDistanceCalculator* const s_TOOL = nullptr;

  /// the the valid tool name
  inline std::string toolName( const IAlgTool* tool, const std::string& nick ) {
    if ( 0 == tool || !nick.empty() ) { return nick; }
    const std::string&           name = tool->name();
    const bool                   pub  = ( 0 == name.find( "ToolSvc." ) );
    const std::string::size_type ldot = name.rfind( '.' );
    if ( std::string::npos != ldot ) {
      return !pub ? tool->type() + "/" + std::string( name, ldot + 1 )
                  : tool->type() + "/" + std::string( name, ldot + 1 ) + ":PUBLIC";
    }
    return tool->type();
  }

  /// the the valid tool name
  inline std::string toolName( const LoKi::Interface<IDistanceCalculator>& dc, const std::string& nick ) {
    const IAlgTool* tool = dc.getObject();
    return toolName( tool, nick );
  }

} // namespace

// constructor from two indices and the tool
LoKi::Particles::DOCA::DOCA( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2,
                             const IDistanceCalculator* dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm )
    , m_eval( algorithm, s_PARTICLE, dc, allow )
    , m_first( i1 )
    , m_second( i2 )
    , m_nick( "" ) {}

// constructor from two indices and the tool
LoKi::Particles::DOCA::DOCA( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2,
                             const LoKi::Interface<IDistanceCalculator>& dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm )
    , m_eval( algorithm, s_PARTICLE, dc, allow )
    , m_first( i1 )
    , m_second( i2 )
    , m_nick( "" ) {}

// constructor from two indices and the tool nickname
LoKi::Particles::DOCA::DOCA( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2, const std::string& nick,
                             const bool allow )
    : LoKi::AuxFunBase( std::tie( algorithm, i1, i2, nick, allow ) )
    , LoKi::AuxDesktopBase( algorithm )
    , m_eval( algorithm, s_PARTICLE, s_TOOL, allow )
    , m_first( i1 )
    , m_second( i2 )
    , m_nick( nick ) {}

// MANDATORY: the only one essential method
LoKi::Particles::DOCA::result_type LoKi::Particles::DOCA::operator()( LoKi::Particles::DOCA::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  /// get the first daughter
  const LHCb::Particle* first = LoKi::Child::child( p, firstIndex() );
  if ( 0 == first ) {
    Error( "Invalid first daughter particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  /// get the second daughter
  const LHCb::Particle* second = LoKi::Child::child( p, secondIndex() );
  if ( 0 == second ) {
    Error( "Invalid second daughter particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }

  // tool is valid?
  if ( !tool() ) { loadTool().ignore(); }

  // evaluate the result
  return doca( first, second, desktop()->geometry() );
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::DOCA::fillStream( std::ostream& s ) const {
  return s << "DOCA(" << firstIndex() << "," << secondIndex() << ",'" << toolName() << "',"
           << ( allow() ? "True" : "False" ) << ")";
}

// get toolname
std::string LoKi::Particles::DOCA::toolName() const { return ::toolName( tool(), nickname() ); }

StatusCode LoKi::Particles::DOCA::loadTool() const {
  // tool is valid?
  if ( !tool() ) {
    const IDistanceCalculator* dc = LoKi::GetTools::distanceCalculator( *this, nickname() );
    /// finally set the tool
    setTool( dc );
  }
  Assert( !( !tool() ), "Unable to locate tool!" );
  return StatusCode::SUCCESS;
}

// constructor from two indices and the tool
LoKi::Particles::DOCAChi2::DOCAChi2( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2,
                                     const IDistanceCalculator* dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCA( algorithm, i1, i2, dc, allow ) {}

// constructor from two indices and the tool
LoKi::Particles::DOCAChi2::DOCAChi2( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2,
                                     const LoKi::Interface<IDistanceCalculator>& dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCA( algorithm, i1, i2, dc, allow ) {}

// constructor from two indices and the tool nickname
LoKi::Particles::DOCAChi2::DOCAChi2( IDVAlgorithm const* algorithm, const size_t i1, const size_t i2,
                                     const std::string& nick, const bool allow )
    : LoKi::AuxFunBase( std::tie( algorithm, i1, i2, nick, allow ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::DOCA( algorithm, i1, i2, nick, allow ) {}

// MANDATORY: the only one essential method
LoKi::Particles::DOCAChi2::result_type
LoKi::Particles::DOCAChi2::operator()( LoKi::Particles::DOCAChi2::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid particle, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  /// get the first daughter
  const LHCb::Particle* first = LoKi::Child::child( p, firstIndex() );
  if ( 0 == first ) {
    Error( "Invalid first daughter particle, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  /// get the second daughter
  const LHCb::Particle* second = LoKi::Child::child( p, secondIndex() );
  if ( 0 == second ) {
    Error( "Invalid second daughter particle, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }

  // tool is valid?
  if ( !tool() ) { loadTool().ignore(); }

  // evaluate the result
  return chi2( first, second, desktop()->geometry() );
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::DOCAChi2::fillStream( std::ostream& s ) const {
  return s << "DOCACHI2(" << firstIndex() << "," << secondIndex() << ",'" << toolName() << "',"
           << ( allow() ? "True" : "False" ) << ")";
}

// constructor from two indices and the tool
LoKi::Particles::DOCAMax::DOCAMax( IDVAlgorithm const* algorithm, const IDistanceCalculator* dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCA( algorithm, 1, 1, dc, allow ) {}

// constructor from two indices and the tool
LoKi::Particles::DOCAMax::DOCAMax( IDVAlgorithm const* algorithm, const LoKi::Interface<IDistanceCalculator>& dc,
                                   const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCA( algorithm, 1, 1, dc, allow ) {}

// constructor from two indices and the tool nickname
LoKi::Particles::DOCAMax::DOCAMax( IDVAlgorithm const* algorithm, const std::string& nick, const bool allow )
    : LoKi::AuxFunBase( std::tie( algorithm, nick, allow ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::DOCA( algorithm, 1, 1, nick, allow ) {}

// MANDATORY: the only one essential method
LoKi::Particles::DOCAMax::result_type
LoKi::Particles::DOCAMax::operator()( LoKi::Particles::DOCAMax::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  const SmartRefVector<LHCb::Particle>& daugs = p->daughters();
  if ( daugs.empty() ) {
    Warning( "Empty list of daughetrs, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }

  // check the tool
  if ( !tool() ) { loadTool().ignore(); }

  // evaluate the result
  return docamax( daugs, desktop()->geometry() );
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::DOCAMax::fillStream( std::ostream& s ) const {
  return s << "DOCAMAX("
           << "'" << toolName() << "'," << ( allow() ? "True" : "False" ) << ")";
}

// constructor from two indices and the tool
LoKi::Particles::DOCAChi2Max::DOCAChi2Max( IDVAlgorithm const* algorithm, const IDistanceCalculator* dc,
                                           const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCAMax( algorithm, dc, allow ) {}

// constructor from two indices and the tool
LoKi::Particles::DOCAChi2Max::DOCAChi2Max( IDVAlgorithm const*                         algorithm,
                                           const LoKi::Interface<IDistanceCalculator>& dc, const bool allow )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::DOCAMax( algorithm, dc, allow ) {}

// constructor from two indices and the tool nickname
LoKi::Particles::DOCAChi2Max::DOCAChi2Max( IDVAlgorithm const* algorithm, const std::string& nick, const bool allow )
    : LoKi::AuxFunBase( std::tie( algorithm, nick, allow ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::DOCAMax( algorithm, nick ) {}

// MANDATORY: the only one essential method
LoKi::Particles::DOCAChi2Max::result_type
LoKi::Particles::DOCAChi2Max::operator()( LoKi::Particles::DOCAChi2Max::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid particle, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  const SmartRefVector<LHCb::Particle>& daugs = p->daughters();
  if ( daugs.empty() ) {
    Warning( "Empty list of daughetrs, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }

  // check the tool
  if ( !tool() ) { loadTool().ignore(); }

  // evaluate the result
  return docachi2max( daugs, desktop()->geometry() );
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::DOCAChi2Max::fillStream( std::ostream& s ) const {
  return s << "DOCACHI2MAX("
           << "'" << toolName() << "')";
}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIP::ChildIP( IDVAlgorithm const* algorithm, const size_t index, const IDistanceCalculator* tool )
    : LoKi::AuxFunBase( std::tie( algorithm, index ) )
    , LoKi::AuxDesktopBase( algorithm )
    , m_eval( algorithm, s_VERTEX, tool )
    , m_index( index )
    , m_nick() {}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIP::ChildIP( IDVAlgorithm const* algorithm, const size_t index,
                                   const LoKi::Interface<IDistanceCalculator>& tool )
    : LoKi::AuxDesktopBase( algorithm ), m_eval( algorithm, s_VERTEX, tool ), m_index( index ), m_nick() {}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIP::ChildIP( IDVAlgorithm const* algorithm, const size_t index, const std::string& nick )
    : LoKi::AuxFunBase( std::tie( algorithm, index, nick ) )
    , LoKi::AuxDesktopBase( algorithm )
    , m_eval( algorithm, s_VERTEX, s_TOOL )
    , m_index( index )
    , m_nick( nick ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::ChildIP* LoKi::Particles::ChildIP::clone() const { return new LoKi::Particles::ChildIP( *this ); }

// MANDATORY: the only one essential method
LoKi::Particles::ChildIP::result_type
LoKi::Particles::ChildIP::operator()( LoKi::Particles::ChildIP::argument p ) const {
  m_eval.setVertex( s_VERTEX );

  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL; returning InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }

  const LHCb::Vertex* vertex = p->endVertex();
  if ( 0 == vertex ) {
    Error( "LHCb::Particle* has a NULL endVertex; returning InvalidDistance " ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }

  /// get the daughter
  const LHCb::Particle* daughter = LoKi::Child::child( p, m_index );
  if ( 0 == daughter ) {
    Error( "Invalid daughter particle, return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }

  // tool is valid?
  if ( !tool() ) { loadTool().ignore(); }

  m_eval.setVertex( vertex );

  const double ip = m_eval.ip( daughter, desktop()->geometry() );

  m_eval.setVertex( s_VERTEX ); // re-set the vertex

  return ip;
}

// Load the tool
StatusCode LoKi::Particles::ChildIP::loadTool() const {
  // tool is valid?
  if ( !tool() ) {
    const IDistanceCalculator* dc = LoKi::GetTools::distanceCalculator( *this, m_nick );
    /// finally set the tool
    setTool( dc );
  }
  Assert( !( !tool() ), "Unable to locate tool!" );
  return StatusCode::SUCCESS;
}

// get toolname
std::string LoKi::Particles::ChildIP::toolName() const { return ::toolName( tool(), m_nick ); }

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::ChildIP::fillStream( std::ostream& s ) const {
  return s << " CHILDIP( " << m_index << ",'" << toolName() << "')";
}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIPChi2::ChildIPChi2( IDVAlgorithm const* algorithm, const size_t index,
                                           const IDistanceCalculator* tool )
    : LoKi::AuxFunBase( std::tie( algorithm, index ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ChildIP( algorithm, index, tool ) {}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIPChi2::ChildIPChi2( IDVAlgorithm const* algorithm, const size_t index,
                                           const LoKi::Interface<IDistanceCalculator>& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::ChildIP( algorithm, index, tool ) {}

// Constructor from the daughter index & tool
LoKi::Particles::ChildIPChi2::ChildIPChi2( IDVAlgorithm const* algorithm, const size_t index, const std::string& nick )
    : LoKi::AuxFunBase( std::tie( algorithm, index, nick ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ChildIP( algorithm, index, nick ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::ChildIPChi2* LoKi::Particles::ChildIPChi2::clone() const {
  return new LoKi::Particles::ChildIPChi2( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::ChildIPChi2::result_type
LoKi::Particles::ChildIPChi2::operator()( LoKi::Particles::ChildIPChi2::argument p ) const {
  m_eval.setVertex( s_VERTEX );

  if ( 0 == p ) {
    Error( "LHCb::Particle* points to NULL; returning InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }

  const LHCb::Vertex* vertex = p->endVertex();
  if ( 0 == vertex ) {
    Error( "LHCb::Particle* has a NULL endVertex; returning InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }

  /// get the daughter
  const LHCb::Particle* daughter = LoKi::Child::child( p, m_index );
  if ( 0 == daughter ) {
    Error( "Invalid daughter particle, return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }

  // tool is valid?
  if ( !tool() ) { loadTool().ignore(); }

  m_eval.setVertex( vertex );

  const double chi2 = m_eval.ipchi2( daughter, desktop()->geometry() );

  m_eval.setVertex( s_VERTEX ); // reset the vertex

  return chi2;
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::ChildIPChi2::fillStream( std::ostream& s ) const {
  return s << " CHILDIPCHI( " << m_index << ",'" << toolName() << "')";
}

// constructor from one index and the tool
LoKi::Particles::MTDOCA::MTDOCA( const IDVAlgorithm* algorithm, const size_t idaughter, const std::string& name )
    : LoKi::AuxFunBase( std::tie( algorithm, idaughter, name ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , m_eval( algorithm, s_PARTICLE, nullptr != algorithm ? algorithm->distanceCalculator( name ) : nullptr, false )
    , m_index( idaughter ) {}

// MANDATORY: the only one essential method
LoKi::Particles::MTDOCA::result_type LoKi::Particles::MTDOCA::operator()( LoKi::Particles::MTDOCA::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  /// get the daughter
  const LHCb::Particle* daughter = LoKi::Child::child( p, getIndex() );
  if ( 0 == daughter ) {
    Error( "Invalid daughter particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }

  // clone the mother and move it to the PV
  auto tempMother = moveMother( p, daughter, desktop()->geometry() );

  // evaluate the result
  return doca( tempMother.get(), daughter, desktop()->geometry() );
}

// Move the mother particle to the best PV of the daughter
std::unique_ptr<LHCb::Particle> LoKi::Particles::MTDOCA::moveMother( LHCb::Particle const* mother,
                                                                     LHCb::Particle const* daughter,
                                                                     IGeometryInfo const&  geometry ) const {
  // clone the mother and move it to the PV
  std::unique_ptr<LHCb::Particle> tempMother( mother->clone() );

  const LHCb::VertexBase* aPV = bestVertex( daughter, geometry );
  // Update the position errors
  tempMother->setReferencePoint( aPV->position() );
  tempMother->setPosCovMatrix( aPV->covMatrix() );

  // Cleanup and return
  // delete aPV ;
  return tempMother;
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::MTDOCA::fillStream( std::ostream& s ) const {
  return s << "MTDOCA(" << getIndex() << ")";
}

// constructor from one index and the tool
LoKi::Particles::MTDOCAChi2::MTDOCAChi2( const IDVAlgorithm* algorithm, const size_t idaughter,
                                         const std::string& nick )
    : LoKi::AuxFunBase( std::tie( algorithm, idaughter, nick ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::MTDOCA( algorithm, idaughter, nick ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MTDOCAChi2* LoKi::Particles::MTDOCAChi2::clone() const {
  return new LoKi::Particles::MTDOCAChi2( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MTDOCAChi2::result_type
LoKi::Particles::MTDOCAChi2::operator()( LoKi::Particles::MTDOCAChi2::argument p ) const {
  if ( !p ) {
    Error( "Invalid particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  /// get the daughter
  const LHCb::Particle* daughter = LoKi::Child::child( p, getIndex() );
  if ( !daughter ) {
    Error( "Invalid daughter particle, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }

  // clone the mother and move it to the PV
  auto tempMother = moveMother( p, daughter, desktop()->geometry() );

  // evaluate the result
  return chi2( tempMother.get(), daughter, desktop()->geometry() );
}

// OPTIONAL: nice printout
std::ostream& LoKi::Particles::MTDOCAChi2::fillStream( std::ostream& s ) const {
  return s << "MTDOCACHI2(" << getIndex() << "')";
}
