/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/GetPhysDesktop.h"

/**
 *  The Implementation file for class LoKi::AuxDesktopBase
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2008-01-16
 */

// do not allow late acquire of desktop
bool LoKi::AuxDesktopBase::s_allow_late_desktop_acquire = false;

// allow_late_desktop aquire ?
bool LoKi::AuxDesktopBase::allow_late_desktop_acquire() { return s_allow_late_desktop_acquire; }

// allow_late_desktop aquire ?
bool LoKi::AuxDesktopBase::allow_late_desktop_acquire( const bool allow ) {
  s_allow_late_desktop_acquire = allow;
  return allow_late_desktop_acquire();
}

// constructor from the desktop
LoKi::AuxDesktopBase::AuxDesktopBase( const IDVAlgorithm* desktop ) : LoKi::AuxFunBase(), m_desktop( desktop ) {
  if ( !desktop )
    throw GaudiException( "\n### No IDVAlgorithm.\n### You cannot use this function from outside of a "
                          "DVAlgorithm.\n### Are you in FunTuple or an Hlt selection?",
                          "LoKi::AuxDesktopBase::AuxDesktopBase", StatusCode::FAILURE );
}

// constructor from the desktop
LoKi::AuxDesktopBase::AuxDesktopBase( const LoKi::Interface<IDVAlgorithm>& desktop )
    : LoKi::AuxFunBase(), m_desktop( desktop ) {
  if ( !desktop )
    throw GaudiException( "\n### No IDVAlgorithm.\n### You cannot use this function from outside of a "
                          "DVAlgorithm.\n### Are you in FunTuple or an Hlt selection?",
                          "LoKi::AuxDesktopBase::AuxDesktopBase", StatusCode::FAILURE );
}

// copy constructor
LoKi::AuxDesktopBase::AuxDesktopBase( const LoKi::AuxDesktopBase& right )
    : LoKi::AuxFunBase( right )
    , m_desktop( right.m_desktop ) // ATTENTION - it is not a right copy!!!
{}

// destructor
LoKi::AuxDesktopBase::~AuxDesktopBase() {
  if ( m_desktop && !gaudi() ) {
    // Warning("IPhysDesktop:        manual reset") ;
    m_desktop.reset();
  }
}

// load the desktop
StatusCode LoKi::AuxDesktopBase::loadDesktop() const {
  if ( validDesktop() ) { return StatusCode::SUCCESS; }
  if ( !allow_late_desktop_acquire() ) { return Error( "loadDesktop: late acquire of desktop is disabled" ); }
  m_desktop = LoKi::getPhysDesktop( lokiSvc() );
  if ( !validDesktop() ) { Error( "loadDesktop(): unable to load IDVAlgorithm!" ).ignore(); }
  return StatusCode::SUCCESS;
}

// get "the best related vertex"
const LHCb::VertexBase* LoKi::AuxDesktopBase::bestVertex( const LHCb::Particle* p,
                                                          IGeometryInfo const&  geometry ) const {
  if ( nullptr == p ) {
    Warning( "relatedVertex: LHCb::Particle* points to NULL, return NULL" ).ignore();
    return nullptr;
  }
  Assert( validDesktop(), "Unable to retrieve IDVAlgorithm" );
  const LHCb::VertexBase* vertex = desktop()->bestVertex( p, geometry );
  if ( 0 == vertex ) { Warning( "bestVertex: IDVAlgorithm::bestVertex() returns NULL!" ).ignore(); }
  return vertex;
}

// get all primary vertices
LHCb::RecVertex::Range LoKi::AuxDesktopBase::primaryVertices() const {
  Assert( validDesktop(), "Unable to retrieve PhysDekstop!" );
  auto pvs = m_desktop->primaryVertices();
  if ( pvs.empty() ) { Warning( "primaryVertices: IDVAlgorithm::primaryVertices() empty " ).ignore(); }
  return pvs;
}
