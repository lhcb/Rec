/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include <algorithm>
#include <functional>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/ToStream.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/RecVertex.h"
#include "Event/Vertex.h"
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#include "Kernel/GetIDVAlgorithm.h"
#include "Kernel/IDVAlgorithm.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/PhysExtract.h"
#include "LoKi/PhysSources.h"
#include "LoKi/Services.h"
// ============================================================================
/** @file
 *  Implementation file for various sources
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyav@physics.syr.edu
 *  @date 2006-12-07
 */
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Particles::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path )
    : LoKi::AuxFunBase( std::tie( svc, path ) )
    , SourceTES::_Base( svc, path )
    , m_cut( LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Particles::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path,
                                       const LoKi::PhysTypes::Cuts& cuts )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts ) ), SourceTES::_Base( svc, path ), m_cut( cuts ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Particles::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, useRootInTES ) )
    , SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Particles::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path,
                                       const LoKi::PhysTypes::Cuts& cuts, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts, useRootInTES ) )
    , SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( cuts ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::SourceTES* LoKi::Particles::SourceTES::clone() const { return new SourceTES( *this ); }
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Particles::SourceTES::~SourceTES() {}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Particles::SourceTES::result_type LoKi::Particles::SourceTES::operator()() const {
  //
  LHCb::Particle::ConstVector output;
  StatusCode                  sc = get( output );
  if ( sc.isFailure() ) { return LHCb::Particle::ConstVector(); }
  return output;
}
// ======================================================================
// get the particles from TES
// ======================================================================
StatusCode LoKi::Particles::SourceTES::get( LHCb::Particle::ConstVector& output ) const {
  output.clear();
  //
  if ( algorithm() ) {
    // ========================================================================
    typedef LHCb::Particle::Range RANGE;
    const bool                    ok = algorithm()->exist<RANGE>( location(), useRootInTES() );
    if ( !ok ) { return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ); }
    const RANGE rng = algorithm()->get<RANGE>( location(), useRootInTES() );
    output.reserve( rng.size() );
    std::copy_if( rng.begin(), rng.end(), std::back_inserter( output ), std::cref( m_cut ) );
    return StatusCode::SUCCESS; //    RETURN
    // ========================================================================
  } else if ( datasvc() ) {
    // ========================================================================
    // try selection
    SmartDataPtr<LHCb::Particle::Selection> parts1( datasvc(), location() );
    if ( !( !parts1 ) ) {
      output.reserve( output.size() + parts1->size() );
      std::copy_if( parts1->begin(), parts1->end(), std::back_inserter( output ), std::cref( m_cut ) );
      return StatusCode::SUCCESS; //    RETURN
    }
    // ========================================================================
    // try container
    SmartDataPtr<LHCb::Particle::Container> parts2( datasvc(), location() );
    if ( !( !parts2 ) ) {
      output.reserve( output.size() + parts2->size() );
      std::copy_if( parts2->begin(), parts2->end(), std::back_inserter( output ), std::cref( m_cut ) );
      return StatusCode::SUCCESS; //    RETURN
    }
    // ========================================================================
    return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE );
    // ========================================================================
  }
  //
  return Error( "Invalid configuration", StatusCode::FAILURE );
}

// ======================================================================
// get the particles from TES
// ======================================================================
std::size_t LoKi::Particles::SourceTES::count() const {
  if ( algorithm() ) {
    // ========================================================================
    typedef LHCb::Particle::Range RANGE;
    const bool                    ok = algorithm()->exist<RANGE>( location(), useRootInTES() );
    if ( !ok ) {
      Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
      return 0;
    }
    const RANGE rng = algorithm()->get<RANGE>( location(), useRootInTES() );
    return LoKi::Algs::count_if( rng.begin(), rng.end(), m_cut );
    // ========================================================================
  } else if ( datasvc() ) {
    // ========================================================================
    // try selection
    SmartDataPtr<LHCb::Particle::Selection> parts1( datasvc(), location() );
    if ( !( !parts1 ) ) { return LoKi::Algs::count_if( parts1->begin(), parts1->end(), m_cut ); }
    // ========================================================================
    // try container
    SmartDataPtr<LHCb::Particle::Container> parts2( datasvc(), location() );
    if ( !( !parts2 ) ) { return LoKi::Algs::count_if( parts2->begin(), parts2->end(), m_cut ); }
    // ========================================================================
    Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
    // ========================================================================
  }
  //
  Error( "Invalid configuration", StatusCode::FAILURE ).ignore();
  return 0;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Particles::SourceTES::fillStream( std::ostream& o ) const {
  return o << "SOURCE(" << Gaudi::Utils::toString( path() ) << "," << m_cut << ")";
}
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location )
    : LoKi::AuxFunBase( std::tie( algorithm, location ) )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Source()
    , LoKi::TES::DataHandle<Range_>( algorithm, location )
    , m_cuts( LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location,
                                   const LoKi::PhysTypes::Cuts& cuts )
    : LoKi::AuxFunBase( std::tie( algorithm, location, cuts ) )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Source()
    , LoKi::TES::DataHandle<Range_>( algorithm, location )
    , m_cuts( cuts ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::TESData* LoKi::Particles::TESData::clone() const { return new TESData( *this ); }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Particles::TESData::result_type LoKi::Particles::TESData::operator()() const {
  const auto                            range = get();
  LoKi::Particles::TESData::result_type result;
  result.reserve( range.size() );
  std::copy_if( range.begin(), range.end(), std::back_inserter( result ), std::cref( m_cuts ) );
  return result;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Particles::TESData::fillStream( std::ostream& o ) const {
  return o << "TESDATA('" << location() << "'," << m_cuts << ")";
}

// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Particles::TESCounter::TESCounter( const LoKi::BasicFunctors<const LHCb::Particle*>::Source& s )
    : LoKi::AuxFunBase( std::tie( s ) ), LoKi::Functor<void, double>(), m_source( s ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::TESCounter* LoKi::Particles::TESCounter::clone() const {
  return new LoKi::Particles::TESCounter( *this );
}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
double LoKi::Particles::TESCounter::operator()() const { return m_source().size(); }
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Particles::TESCounter::fillStream( std::ostream& o ) const {
  return o << "NUMBER(" << m_source << ")";
}
// ============================================================================

// ============================================================================
// constructor from the desktop & cuts
// ============================================================================
LoKi::Particles::SourceDesktop::SourceDesktop( const IDVAlgorithm* desktop, const LoKi::PhysTypes::Cuts& cuts )
    : LoKi::AuxFunBase( std::tie( cuts ) )
    , LoKi::Particles::SourceDesktop::_Source()
    , m_desktop( desktop )
    , m_cut( cuts ) {}
// ============================================================================
// constructor from the desktop & cuts
// ============================================================================
LoKi::Particles::SourceDesktop::SourceDesktop( const LoKi::PhysTypes::Cuts& cuts, const IDVAlgorithm* desktop )
    : LoKi::AuxFunBase( std::tie( cuts ) )
    , LoKi::Particles::SourceDesktop::_Source()
    , m_desktop( desktop )
    , m_cut( cuts ) {}
// ============================================================================
// copy constructor
// ============================================================================
LoKi::Particles::SourceDesktop::SourceDesktop( const LoKi::Particles::SourceDesktop& right )
    : LoKi::AuxFunBase( right )
    , LoKi::Particles::SourceDesktop::_Source( right )
    , m_desktop( right.m_desktop )
    , m_cut( right.m_cut ) {}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Particles::SourceDesktop::~SourceDesktop() {
  if ( m_desktop && !gaudi() ) {
    // Warning("IPhysDesktop: manual reset") ;
    m_desktop.reset();
  }
}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Particles::SourceDesktop::result_type LoKi::Particles::SourceDesktop::operator()() const {
  if ( !m_desktop ) {
    const LoKi::Services& svcs = LoKi::Services::instance();
    const IDVAlgorithm*   alg  = Gaudi::Utils::getIDVAlgorithm( svcs.contextSvc() );
    if ( 0 != alg ) { m_desktop = alg; }
    //
    Assert( m_desktop, "Could not locate valid IDVAlgorithm" );
  }
  //
  LHCb::Particle::Range input = m_desktop->particles();
  //
  LHCb::Particle::ConstVector output;
  output.reserve( input.size() );
  // use cuts:
  std::copy_if( input.begin(), input.end(), std::back_inserter( output ), std::cref( m_cut.func() ) );
  //
  return output;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Particles::SourceDesktop::fillStream( std::ostream& o ) const {
  return o << "SOURCEDESKTOP( " << m_cut << ")";
}
// ============================================================================

// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Vertices::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path,
                                      const LoKi::PhysTypes::VCuts& cuts )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts ) ), LoKi::Vertices::SourceTES::_Base( svc, path ), m_cut( cuts ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Vertices::SourceTES::SourceTES( const IDataProviderSvc* svc, const std::string& path )
    : LoKi::AuxFunBase( std::tie( svc, path ) )
    , LoKi::Vertices::SourceTES::_Base( svc, path )
    , m_cut( LoKi::BasicFunctors<const LHCb::VertexBase*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Vertices::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path,
                                      const LoKi::PhysTypes::VCuts& cuts, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, cuts, useRootInTES ) )
    , LoKi::Vertices::SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( cuts ) {}
// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Vertices::SourceTES::SourceTES( const GaudiAlgorithm* svc, const std::string& path, const bool useRootInTES )
    : LoKi::AuxFunBase( std::tie( svc, path, useRootInTES ) )
    , LoKi::Vertices::SourceTES::_Base( svc, path, useRootInTES )
    , m_cut( LoKi::BasicFunctors<const LHCb::VertexBase*>::BooleanConstant( true ) ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::SourceTES* LoKi::Vertices::SourceTES::clone() const { return new SourceTES( *this ); }
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Vertices::SourceTES::~SourceTES() {}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Vertices::SourceTES::result_type LoKi::Vertices::SourceTES::operator()() const {
  LHCb::VertexBase::ConstVector output;
  StatusCode                    sc = get( output );
  if ( sc.isFailure() ) { return LHCb::VertexBase::ConstVector(); }
  return output;
}
// ============================================================================
// get the vertices from the certain  TES location
// ============================================================================
StatusCode LoKi::Vertices::SourceTES::get( LHCb::VertexBase::ConstVector& output ) const {
  // ========================================================================
  if ( algorithm() ) {
    {
      typedef LHCb::Vertex::Range RANGE1;
      const bool                  ok = algorithm()->exist<RANGE1>( location(), useRootInTES() );
      if ( !ok ) { return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ); }
      const RANGE1 rng = algorithm()->get<RANGE1>( location(), useRootInTES() );
      output.reserve( rng.size() );
      std::copy_if( rng.begin(), rng.end(), std::back_inserter( output ), std::cref( m_cut ) );
      return StatusCode::SUCCESS; //    RETURN
    }
    // ========================================================================
    {
      typedef LHCb::RecVertex::Range RANGE2;
      const bool                     ok = algorithm()->exist<RANGE2>( location(), useRootInTES() );
      if ( !ok ) { return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ); }
      const RANGE2 rng = algorithm()->get<RANGE2>( location(), useRootInTES() );
      output.reserve( rng.size() );
      std::copy_if( rng.begin(), rng.end(), std::back_inserter( output ), std::cref( m_cut ) );
      return StatusCode::SUCCESS; //    RETURN
    }
    // ========================================================================
    return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE );
  } else if ( datasvc() ) {
    // ========================================================================
    { // try selection
      SmartDataPtr<LHCb::RecVertex::Selection> parts1( datasvc(), location() );
      if ( !( !parts1 ) ) {
        output.reserve( output.size() + parts1->size() );
        std::copy_if( parts1->begin(), parts1->end(), std::back_inserter( output ), std::cref( m_cut ) );
        return StatusCode::SUCCESS; //    RETURN
      }
      SmartDataPtr<LHCb::RecVertex::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) {
        output.reserve( output.size() + parts2->size() );
        std::copy_if( parts2->begin(), parts2->end(), std::back_inserter( output ), std::cref( m_cut ) );
        return StatusCode::SUCCESS; //    RETURN
      }
    }
    { // try selection
      SmartDataPtr<LHCb::Vertex::Selection> parts1( datasvc(), location() );
      if ( !( !parts1 ) ) {
        output.reserve( output.size() + parts1->size() );
        std::copy_if( parts1->begin(), parts1->end(), std::back_inserter( output ), std::cref( m_cut ) );
        return StatusCode::SUCCESS; //    RETURN
      }
      SmartDataPtr<LHCb::Vertex::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) {
        output.reserve( output.size() + parts2->size() );
        std::copy_if( parts2->begin(), parts2->end(), std::back_inserter( output ), std::cref( m_cut ) );
        return StatusCode::SUCCESS; //    RETURN
      }
    }
    { // only container here
      SmartDataPtr<LHCb::VertexBase::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) {
        output.reserve( output.size() + parts2->size() );
        std::copy_if( parts2->begin(), parts2->end(), std::back_inserter( output ), std::cref( m_cut ) );
        return StatusCode::SUCCESS; //    RETURN
      }
    }
    // ========================================================================
    return Error( "No valid data at '" + location() + "'", StatusCode::FAILURE );
    // ========================================================================
  }
  //
  return Error( "Invalid configuration", StatusCode::FAILURE );
}
// ============================================================================
// get the vertices from the certain  TES location
// ============================================================================
std::size_t LoKi::Vertices::SourceTES::count() const {
  // ========================================================================
  if ( algorithm() ) {
    {
      typedef LHCb::Vertex::Range RANGE1;
      const bool                  ok = algorithm()->exist<RANGE1>( location(), useRootInTES() );
      if ( !ok ) {
        Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
        return 0;
      }
      const RANGE1 rng = algorithm()->get<RANGE1>( location(), useRootInTES() );
      return LoKi::Algs::count_if( rng.begin(), rng.end(), std::cref( m_cut ) );
    }
    // ========================================================================
    {
      typedef LHCb::RecVertex::Range RANGE2;
      const bool                     ok = algorithm()->exist<RANGE2>( location(), useRootInTES() );
      if ( !ok ) {
        Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
        return 0;
      }
      const RANGE2 rng = algorithm()->get<RANGE2>( location(), useRootInTES() );
      return LoKi::Algs::count_if( rng.begin(), rng.end(), std::cref( m_cut ) );
    }
    // ========================================================================
    Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
    return 0;
  } else if ( datasvc() ) {
    // ========================================================================
    { // try selection
      SmartDataPtr<LHCb::RecVertex::Selection> parts1( datasvc(), location() );
      if ( !( !parts1 ) ) { return LoKi::Algs::count_if( parts1->begin(), parts1->end(), std::cref( m_cut ) ); }
      SmartDataPtr<LHCb::RecVertex::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) { return LoKi::Algs::count_if( parts2->begin(), parts2->end(), std::cref( m_cut ) ); }
    }
    { // try selection
      SmartDataPtr<LHCb::Vertex::Selection> parts1( datasvc(), location() );
      if ( !( !parts1 ) ) { return LoKi::Algs::count_if( parts1->begin(), parts1->end(), std::cref( m_cut ) ); }
      SmartDataPtr<LHCb::Vertex::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) { return LoKi::Algs::count_if( parts2->begin(), parts2->end(), std::cref( m_cut ) ); }
    }
    { // only container here
      SmartDataPtr<LHCb::VertexBase::Container> parts2( datasvc(), location() );
      if ( !( !parts2 ) ) { return LoKi::Algs::count_if( parts2->begin(), parts2->end(), std::cref( m_cut ) ); }
    }
    // ========================================================================
    Error( "No valid data at '" + location() + "'", StatusCode::FAILURE ).ignore();
    return 0;
    // ========================================================================
  }
  //
  Error( "Invalid configuration", StatusCode::FAILURE ).ignore();
  return 0;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Vertices::SourceTES::fillStream( std::ostream& o ) const {
  return o << "VSOURCE(" << Gaudi::Utils::toString( path() ) << "," << m_cut << ")";
}
// ============================================================================

// ============================================================================
// constructor from the desktop & cuts
// ============================================================================
LoKi::Vertices::SourceDesktop::SourceDesktop( const IDVAlgorithm* desktop, const LoKi::PhysTypes::VCuts& cuts )
    : LoKi::AuxFunBase( std::tie( cuts ) )
    , LoKi::Vertices::SourceDesktop::_Source()
    , m_desktop( desktop )
    , m_cut( cuts ) {}
// ============================================================================
// constructor from the desktop & cuts
// ============================================================================
LoKi::Vertices::SourceDesktop::SourceDesktop( const LoKi::PhysTypes::VCuts& cuts, const IDVAlgorithm* desktop )
    : LoKi::AuxFunBase( std::tie( cuts ) )
    , LoKi::Vertices::SourceDesktop::_Source()
    , m_desktop( desktop )
    , m_cut( cuts ) {}
// ============================================================================
// copy constructor
// ============================================================================
LoKi::Vertices::SourceDesktop::SourceDesktop( const LoKi::Vertices::SourceDesktop& right )
    : LoKi::AuxFunBase( right )
    , LoKi::Vertices::SourceDesktop::_Source( right )
    , m_desktop( right.m_desktop )
    , m_cut( right.m_cut ) {}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Vertices::SourceDesktop::~SourceDesktop() {
  if ( m_desktop && !gaudi() ) {
    // Warning("IPhysDesktop: manual reset") ;
    m_desktop.reset();
  }
}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Vertices::SourceDesktop::result_type LoKi::Vertices::SourceDesktop::operator()() const {
  if ( !m_desktop ) {
    const LoKi::Services& svcs = LoKi::Services::instance();
    const IDVAlgorithm*   alg  = Gaudi::Utils::getIDVAlgorithm( svcs.contextSvc() );
    if ( alg ) { m_desktop = alg; }
    //
    Assert( m_desktop, "Could not locate valid IDVAlgorithm" );
  }
  //
  const auto input2 = m_desktop->primaryVertices();
  //
  if ( input2.empty() ) { Warning( "No input primary vertices from IDVAlgorithm" ).ignore(); }

  LHCb::VertexBase::ConstVector output;
  output.reserve( input2.size() );
  // use cuts:
  std::copy_if( input2.begin(), input2.end(), std::back_inserter( output ), std::cref( m_cut.func() ) );
  //
  if ( output.empty() ) { Warning( "No vertices are selected by '" + m_cut.printOut() + "'" ).ignore(); }
  //
  return output;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Vertices::SourceDesktop::fillStream( std::ostream& o ) const {
  return o << "VSOURCEDESKTOP( " << m_cut << ")";
}
// ============================================================================
// set the  desktop
// ============================================================================

// ============================================================================
// constructor
// ============================================================================
LoKi::Vertices::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location )
    : LoKi::AuxFunBase( std::tie( algorithm, location ) )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Source()
    , LoKi::TES::DataHandle<DataObject>( algorithm, location )
    , m_cuts( LoKi::BasicFunctors<const LHCb::VertexBase*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Vertices::TESData::TESData( const GaudiAlgorithm* algorithm, const std::string& location,
                                  const LoKi::PhysTypes::VCuts& cuts )
    : LoKi::AuxFunBase( std::tie( algorithm, location, cuts ) )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Source()
    , LoKi::TES::DataHandle<DataObject>( algorithm, location )
    , m_cuts( cuts ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::TESData* LoKi::Vertices::TESData::clone() const { return new TESData( *this ); }
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Vertices::TESData::result_type LoKi::Vertices::TESData::operator()() const {
  const DataObject*                    data = get();
  LoKi::Vertices::TESData::result_type result;
  //
  const LHCb::Vertex::Selection* sel1 = dynamic_cast<const LHCb::Vertex::Selection*>( data );
  if ( sel1 ) {
    result.reserve( sel1->size() );
    std::copy_if( sel1->begin(), sel1->end(), std::back_inserter( result ), std::cref( m_cuts ) );
    return result;
  }
  //
  const LHCb::RecVertex::Selection* sel2 = dynamic_cast<const LHCb::RecVertex::Selection*>( data );
  if ( sel2 ) {
    result.reserve( sel2->size() );
    std::copy_if( sel2->begin(), sel2->end(), std::back_inserter( result ), std::cref( m_cuts ) );
    return result;
  }
  //
  const LHCb::Vertex::Container* cnt1 = dynamic_cast<const LHCb::Vertex::Container*>( data );
  if ( cnt1 ) {
    result.reserve( cnt1->size() );
    std::copy_if( cnt1->begin(), cnt1->end(), std::back_inserter( result ), std::cref( m_cuts ) );
    return result;
  }
  //
  const LHCb::RecVertex::Container* cnt2 = dynamic_cast<const LHCb::RecVertex::Container*>( data );
  if ( cnt2 ) {
    result.reserve( cnt2->size() );
    std::copy_if( cnt2->begin(), cnt2->end(), std::back_inserter( result ), std::cref( m_cuts ) );
    return result;
  }
  Error( "No  valid data is found! '" + location() + "'" ).ignore();
  return result;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Vertices::TESData::fillStream( std::ostream& o ) const {
  return o << "VTESDATA('" << location() << "'," << m_cuts << ")";
}

// ============================================================================
// constructor from the service, TES location and cuts
// ============================================================================
LoKi::Vertices::TESCounter::TESCounter( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source& s )
    : LoKi::AuxFunBase( std::tie( s ) ), LoKi::Functor<void, double>(), m_source( s ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Vertices::TESCounter* LoKi::Vertices::TESCounter::clone() const {
  return new LoKi::Vertices::TESCounter( *this );
}
// ============================================================================
// MANDATORY: the only essential method:
// ============================================================================
LoKi::Vertices::TESCounter::result_type LoKi::Vertices::TESCounter::operator()( /* argument */ ) const {
  return m_source().size();
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Vertices::TESCounter::fillStream( std::ostream& o ) const {
  return o << "VNUMBER(" << m_source << ")";
}
// ============================================================================

// ============================================================================
// constructor from the optional cuts
// ============================================================================
LoKi::Particles::Flatten::Flatten()
    : LoKi::AuxFunBase( std::tie() )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Pipe()
    , m_cut( LoKi::BasicFunctors<const LHCb::Particle*>::BooleanConstant( true ) ) {}
// ============================================================================
// constructor from the optional cuts
// ============================================================================
LoKi::Particles::Flatten::Flatten( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( cut ) ), LoKi::BasicFunctors<const LHCb::Particle*>::Pipe(), m_cut( cut ) {}
// ============================================================================
// MANDATORY: virtual destructor
// ============================================================================
LoKi::Particles::Flatten::~Flatten() {}
// ============================================================================
// MANDATORY: clone method("virtual constructor")
// ============================================================================
LoKi::Particles::Flatten* LoKi::Particles::Flatten::clone() const { return new LoKi::Particles::Flatten( *this ); }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::Flatten::result_type
LoKi::Particles::Flatten::operator()( LoKi::Particles::Flatten::argument a ) const {
  //
  result_type flatten;
  if ( a.empty() ) { return flatten; } // RETURN
  //
  flatten.reserve( 6 * a.size() );
  //
  LoKi::Extract::getParticles( a.begin(), a.end(), std::back_inserter( flatten ), m_cut );
  //
  return flatten;
}
// ============================================================================
// OPTIONAL: the nice printout
// ============================================================================
std::ostream& LoKi::Particles::Flatten::fillStream( std::ostream& o ) const {
  return o << " FLATTEN(" << m_cut << ") ";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
