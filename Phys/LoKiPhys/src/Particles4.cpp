/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RecVertex.h"

#include "LoKi/Constants.h"
#include "LoKi/Kinematics.h"
#include "LoKi/Particles4.h"
#include "LoKi/PhysHelpers.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToStream.h"

#include <algorithm>
#include <vector>

/**
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-10
 */

namespace {
  const LHCb::VertexBase* const s_VERTEX = nullptr;
}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LHCb::VertexBase* vertex,
                                 const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Point3D& vertex,
                                 const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::VertexHolder& vertex,
                                 const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                                 const LHCb::VertexBase* vertex )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                                 const LoKi::Point3D& vertex )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpactParamTool& tool,
                                 const LoKi::Vertices::VertexHolder& vertex )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( vertex, tool ) {}

LoKi::Particles::ImpPar::ImpPar( IDVAlgorithm const* algo, const LoKi::Vertices::ImpParBase& tool )
    : LoKi::AuxDesktopBase( algo ), LoKi::Vertices::ImpParBase( tool ) {}

// the actual evaluator of impact parameter
double LoKi::Particles::ImpPar::ip( LoKi::Particles::ImpPar::argument p, IGeometryInfo const& geometry ) const {
  if ( !valid() ) {
    Error( " Invalid vertex! , return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  Assert( 0 != tool(), " Invalid IDistanceCalculator*" );
  StatusCode sc     = StatusCode::FAILURE;
  double     impact = LoKi::Constants::InvalidDistance;
  switch ( type() ) {
  case LoKi::Vertices::VertexHolder::_vertex:
    sc = tool()->distance( p, vertex(), impact, geometry );
    break; // BREAK
  case LoKi::Vertices::VertexHolder::_point:
    sc = tool()->distance( p, position(), impact, geometry );
    break; // BREAK
  default:
    Error( "Invalid switch! '" + Gaudi::Utils::toString( type() ) + "'" ).ignore();
    return LoKi::Constants::InvalidDistance;
    break; // BREAK
  }
  if ( sc.isFailure() ) {
    Warning( "Error from IDistanceCalculator; return 'InvalidDistance'", sc, 3 ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  return impact;
}

// the actual evaluator of impact parameter chi2
double LoKi::Particles::ImpPar::ipchi2( LoKi::Particles::ImpPar::argument p, IGeometryInfo const& geometry ) const {
  if ( !valid() ) {
    Error( " Invalid vertex! , return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  Assert( 0 != tool(), " Invalid IDistanceCalculator*" );
  StatusCode sc     = StatusCode::FAILURE;
  double     impact = LoKi::Constants::InvalidDistance;
  double     chi2   = LoKi::Constants::InvalidChi2;
  switch ( type() ) {
  case LoKi::Vertices::VertexHolder::_vertex:
    sc = tool()->distance( p, vertex(), impact, chi2, geometry );
    break; // BREAK
  case LoKi::Vertices::VertexHolder::_point:
    sc = tool()->distance( p, position(), impact, chi2, geometry );
    break; // BREAK
  default:
    Error( "Invalid switch! '" + Gaudi::Utils::toString( type() ) + "'" ).ignore();
    return LoKi::Constants::InvalidChi2;
    break; // BREAK
  }
  if ( sc.isFailure() ) {
    Warning( "IDistanceCalculator::distance failed; return 'InvalidChi2'", sc, 3 ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }
  return chi2;
}

/*  the actual evaluation of 'path-distance'
 *  @see IDistanceCalculator::pathDistance
 *  @param p        (INPUT) the particle
 *  @param distance (OUTPUT) the distance
 *  @param error    (OUTPUT) the error
 *  @param chi2     (OUTPUT) the chi2
 *  @return status code
 */
StatusCode LoKi::Particles::ImpPar::path( LoKi::Particles::ImpPar::argument p, double& distance, double& error,
                                          double& chi2, IGeometryInfo const& geometry ) const {
  distance = LoKi::Constants::InvalidDistance;
  error    = LoKi::Constants::InvalidDistance;
  chi2     = LoKi::Constants::InvalidChi2;
  if ( !valid() ) { return Error( "Invalid vertex!" ); }
  Assert( 0 != tool(), " Invalid IDistanceCalculator*" );
  StatusCode sc = StatusCode::FAILURE;
  switch ( type() ) {
  case LoKi::Vertices::VertexHolder::_vertex:
    sc = tool()->pathDistance( p, vertex(), distance, error, geometry, chi2 );
    break; // BREAK
  default:
    return Error( "Invalid switch! '" + Gaudi::Utils::toString( type() ) + "'" );
  }
  if ( sc.isFailure() ) { return Warning( "IDistanceCalculator::pathDistance failed", sc, 3 ); }
  return sc;
}

/*  the actual evaluation of 'projected-distance'
 *  @see IDistanceCalculator::projectedDistance
 *  @param p        (INPUT) the particle
 *  @return the distance
 */
double LoKi::Particles::ImpPar::projected( LoKi::Particles::ImpPar::argument p, IGeometryInfo const& geometry ) const {
  if ( !valid() ) {
    Error( " Invalid vertex! , return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  Assert( 0 != tool(), " Invalid IDistanceCalculator*" );
  StatusCode sc       = StatusCode::FAILURE;
  double     distance = LoKi::Constants::InvalidDistance;
  switch ( type() ) {
  case LoKi::Vertices::VertexHolder::_vertex:
    sc = tool()->projectedDistance( p, vertex(), geometry, distance );
    break; // BREAK
  default:
    Error( "Invalid switch! '" + Gaudi::Utils::toString( type() ) + "'" ).ignore();
    return LoKi::Constants::InvalidDistance;
    break; // BREAK
  }
  if ( sc.isFailure() ) {
    Warning( "Error from IDistanceCalculator; return 'InvalidDistance'", sc, 3 ).ignore();
    return LoKi::Constants::InvalidDistance; // RETURN
  }
  return distance;
}

/*  the actual evaluation of 'projected-distance'
 *  @see IDistanceCalculator::projectedDistance
 *  @param p        (INPUT) the particle
 *  @param distance (OUTPUT) the distance
 *  @param error    (OUTPUT) the error
 *  @return status code
 */
StatusCode LoKi::Particles::ImpPar::projected( LoKi::Particles::ImpPar::argument p, double& distance, double& error,
                                               IGeometryInfo const& geometry ) const {
  distance = LoKi::Constants::InvalidDistance;
  error    = LoKi::Constants::InvalidDistance;
  if ( !valid() ) { return Error( " Invalid vertex!" ); }
  Assert( 0 != tool(), " Invalid IDistanceCalculator*" );
  StatusCode sc = StatusCode::FAILURE;
  switch ( type() ) {
  case LoKi::Vertices::VertexHolder::_vertex:
    sc = tool()->projectedDistance( p, vertex(), distance, error, geometry );
    break; // BREAK
  default:
    return Error( "Invalid switch! '" + Gaudi::Utils::toString( type() ) + "'" );
  }
  if ( sc.isFailure() ) { return Warning( "IDistanceCalculator::distance failed", sc, 3 ); }
  return sc;
}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LHCb::VertexBase::ConstVector& vertices,
                                       const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LHCb::Vertex::ConstVector& vertices,
                                       const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::PhysTypes::VRange& vertices,
                                       const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LHCb::RecVertex::ConstVector& vertices,
                                       const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LHCb::RecVertex::Container* vertices,
                                       const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool ) {
  if ( vertices ) { addObjects( vertices->begin(), vertices->end() ); }
}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                       const LHCb::VertexBase::ConstVector& vertices )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                       const LHCb::Vertex::ConstVector& vertices )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                       const LoKi::PhysTypes::VRange& vertices )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                       const LHCb::RecVertex::ConstVector& vertices )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool )
    , LoKi::UniqueKeeper<LHCb::VertexBase>( vertices.begin(), vertices.end() ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpPar::MinImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                       const LHCb::RecVertex::Container* vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::ImpPar( algorithm, LoKi::Helpers::_First( vertices ), tool ) {
  if ( vertices ) { addObjects( vertices->begin(), vertices->end() ); }
}

template <class FUNCTOR>
struct PMFA3 {
  typedef typename FUNCTOR::argument    argument;
  typedef typename FUNCTOR::result_type result_type;
  typedef result_type ( FUNCTOR::*PMF )( argument, const IGeometryInfo& ) const;

  /// constructor
  PMFA3( const FUNCTOR* fun, PMF _pmf ) : m_fun( fun ), m_pmf( _pmf ) {}
  /// the only one method
  result_type operator()( argument a, const IGeometryInfo& g ) const { return ( m_fun->*m_pmf )( a, g ); }

  template <class VERTEX>
  void setVertex( VERTEX vertex ) const {
    m_fun->setVertex( vertex );
  }

  /// the functor
  const FUNCTOR* m_fun;
  /// member function
  PMF m_pmf;
};

LoKi::Particles::MinImpPar::result_type LoKi::Particles::MinImpPar::mip( LoKi::Particles::MinImpPar::argument p,
                                                                         IGeometryInfo const& geometry ) const {
  if ( 0 == p ) {
    Error( " Invalid argument! , return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  if ( empty() ) {
    Error( " Empty list of vertices, return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  result_type result = LoKi::Constants::InvalidDistance;
  if ( end() == LoKi::Helpers::_Min_vertex( begin(), end(),
                                            PMFA3<LoKi::Particles::ImpPar>( this, &LoKi::Particles::ImpPar::ip ),
                                            result, p, geometry ) ) {
    Error( "Invalid evaluation; return 'InvalidDistance'" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  setVertex( s_VERTEX );
  return result;
}

LoKi::Particles::MinImpPar::result_type LoKi::Particles::MinImpPar::mipchi2( LoKi::Particles::MinImpPar::argument p,
                                                                             IGeometryInfo const& geometry ) const {
  if ( 0 == p ) {
    Error( " Invalid argument! , return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  if ( empty() ) {
    Warning( " Empty list of vertices, return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  result_type result = LoKi::Constants::InvalidChi2;
  if ( end() == LoKi::Helpers::_Min_vertex( begin(), end(),
                                            PMFA3<LoKi::Particles::ImpPar>( this, &LoKi::Particles::ImpPar::ipchi2 ),
                                            result, p, geometry ) ) {
    Warning( "Invalid evaluation; return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  return result;
}

std::ostream& LoKi::Particles::MinImpPar::fillStream( std::ostream& stream ) const { return stream << "MIP"; }

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LHCb::VertexBase::ConstVector&   vertices,
                                               const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const* algorithm, const LHCb::Vertex::ConstVector& vertices,
                                               const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const* algorithm, const LoKi::PhysTypes::VRange& vertices,
                                               const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LHCb::RecVertex::ConstVector&    vertices,
                                               const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LHCb::RecVertex::Container*      vertices,
                                               const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LoKi::Vertices::ImpactParamTool& tool,
                                               const LHCb::VertexBase::ConstVector&   vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LoKi::Vertices::ImpactParamTool& tool,
                                               const LHCb::Vertex::ConstVector&       vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LoKi::Vertices::ImpactParamTool& tool,
                                               const LoKi::PhysTypes::VRange&         vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LoKi::Vertices::ImpactParamTool& tool,
                                               const LHCb::RecVertex::ConstVector&    vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

//  constructor from the vertices and the tool
LoKi::Particles::MinImpParChi2::MinImpParChi2( IDVAlgorithm const*                    algorithm,
                                               const LoKi::Vertices::ImpactParamTool& tool,
                                               const LHCb::RecVertex::Container*      vertices )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinImpPar( algorithm, vertices, tool ) {}

std::ostream& LoKi::Particles::MinImpParChi2::fillStream( std::ostream& stream ) const { return stream << "MIPCHI2"; }
