/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/Keeper.h"
#include "LoKi/Print.h"
#include "LoKi/UniqueKeeper.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles11.h"
#include "LoKi/PhysAlgs.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/lambda/bind.hpp"
#include "boost/lambda/lambda.hpp"
// ============================================================================
/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-22
 */
// ============================================================================
//  constructor from one particle
// ============================================================================
LoKi::Particles::IsAParticle::IsAParticle( const LHCb::Particle* p )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate(), LoKi::UniqueKeeper<LHCb::Particle>( p ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticle::IsAParticle( const LHCb::Particle::Container* p )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate(), LoKi::UniqueKeeper<LHCb::Particle>() {
  if ( 0 != p ) { addObjects( p->begin(), p->end() ); }
}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticle::IsAParticle( const LHCb::Particle::ConstVector& p )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::UniqueKeeper<LHCb::Particle>( p.begin(), p.end() ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticle::IsAParticle( const LoKi::PhysTypes::Range& p )
    : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate()
    , LoKi::UniqueKeeper<LHCb::Particle>( p.begin(), p.end() ) {}
// ============================================================================
//  MANDATORY: the only one essential method
// ============================================================================
bool LoKi::Particles::IsAParticle::inList( const LHCb::Particle* p ) const {
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return 'false'" ).ignore();
    return false;
  }
  //
  if ( empty() ) {
    Warning( "Empty list of particles is specified! return 'false'" ).ignore();
    return false;
  }
  // look for the particle
  return std::binary_search( begin(), end(), p );
}
// ============================================================================
bool LoKi::Particles::IsAParticle::inTree( const LHCb::Particle* p ) const {
  if ( 0 == p ) {
    Error( "Argument is invalid! return 'false'" ).ignore();
    return false;
  }
  if ( empty() ) {
    Warning( "Empty list of particles is specified! return 'false'" ).ignore();
    return false;
  }
  //
  using namespace boost::lambda;
  return LoKi::PhysAlgs::found( p, bind( &LoKi::Particles::IsAParticle::inList, this, _1 ) );
}
// ============================================================================
std::ostream& LoKi::Particles::IsAParticle::fillStream( std::ostream& stream ) const { return stream << "IS"; }

// ============================================================================
//  constructor from one particle
// ============================================================================
LoKi::Particles::IsAParticleInTree::IsAParticleInTree( const LHCb::Particle* p ) : LoKi::Particles::IsAParticle( p ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticleInTree::IsAParticleInTree( const LHCb::Particle::Container* p )
    : LoKi::Particles::IsAParticle( p ) {}
// ============================================================================
//  constructor from vector of particles
// ============================================================================
LoKi::Particles::IsAParticleInTree::IsAParticleInTree( const LHCb::Particle::ConstVector& p )
    : LoKi::Particles::IsAParticle( p ) {}
// ============================================================================
//  constructor from container of particle
// ============================================================================
LoKi::Particles::IsAParticleInTree::IsAParticleInTree( const LoKi::PhysTypes::Range& p )
    : LoKi::Particles::IsAParticle( p ) {}
// ============================================================================
//  MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::IsAParticleInTree::result_type
LoKi::Particles::IsAParticleInTree::operator()( LoKi::Particles::IsAParticleInTree::argument p ) const {
  return inTree( p );
}
// ============================================================================
std::ostream& LoKi::Particles::IsAParticleInTree::fillStream( std::ostream& stream ) const {
  return stream << "ISINTREE";
}

// ============================================================================
//  constructor from one particle
// ============================================================================
LoKi::Particles::IsAParticleFromTree::IsAParticleFromTree( const LHCb::Particle* p )
    : LoKi::Particles::IsAParticleInTree( p ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticleFromTree::IsAParticleFromTree( const LHCb::Particle::Container* p )
    : LoKi::Particles::IsAParticleInTree( p ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticleFromTree::IsAParticleFromTree( const LHCb::Particle::ConstVector& p )
    : LoKi::Particles::IsAParticleInTree( p ) {}
// ============================================================================
//  constructor from container of particles
// ============================================================================
LoKi::Particles::IsAParticleFromTree::IsAParticleFromTree( const LoKi::PhysTypes::Range& p )
    : LoKi::Particles::IsAParticleInTree( p ) {}
// ============================================================================
//  MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::IsAParticleFromTree::result_type
LoKi::Particles::IsAParticleFromTree::operator()( LoKi::Particles::IsAParticleFromTree::argument p ) const {
  if ( 0 == p ) {
    Error( "Argument is invalid! return 'false'" ).ignore();
    return false; // RETURN
  }
  if ( empty() ) {
    Warning( "Empty list of particles is specified! return 'false'" ).ignore();
    return false; // RETURN
  }

  auto matches = [p]( argument p_in_tree ) { return p_in_tree == p; };

  // find the particle by scanning of the decay trees of the other particles
  return std::find_if( begin(), end(),
                       [matches]( argument my_p ) { return LoKi::PhysAlgs::found( my_p, matches ); } ) != end();
}
// ============================================================================
std::ostream& LoKi::Particles::IsAParticleFromTree::fillStream( std::ostream& stream ) const {
  return stream << "FROM";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
