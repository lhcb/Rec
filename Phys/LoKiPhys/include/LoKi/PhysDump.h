/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// =======================================================================
#ifndef LOKI_PHYSDUMP_H
#  define LOKI_PHYSDUMP_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Dumper.h"
#  include "LoKi/Filters.h"
#  include "LoKi/PhysTypes.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 *  @date   2011-06-03
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Functors {
    // ========================================================================
    /** template specialization of ``dumper''
     *  @see LoKi::Fuctors::Dump_
     *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
     *  @date   2011-06-03
     */
    template <>
    Dump_<const LHCb::Particle*>::result_type
    Dump_<const LHCb::Particle*>::operator()( Dump_<const LHCb::Particle*>::argument a ) const;
    // ========================================================================
    /** template specialization of ``dumper''
     *  @see LoKi::Fuctors::Dump1_
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-01-28
     */
    template <>
    Dump1_<const LHCb::Particle*, bool>::result_type
    Dump1_<const LHCb::Particle*, bool>::operator()( Dump1_<const LHCb::Particle*, bool>::argument a ) const;
    // ========================================================================
    /** template specialization of ``dumper''
     *  @see LoKi::Fuctors::Dump1_
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   2012-01-28
     */
    template <>
    Dump1_<const LHCb::Particle*, double>::result_type
    Dump1_<const LHCb::Particle*, double>::operator()( Dump1_<const LHCb::Particle*, double>::argument a ) const;
    // ========================================================================
  } // namespace Functors
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PHYSDUMP_H
// ============================================================================
