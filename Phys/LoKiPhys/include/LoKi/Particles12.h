/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES12_H
#  define LOKI_PARTICLES12_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKiPhys
// ============================================================================
#  include "Event/Track.h"
#  include "LoKi/PhysTypes.h"
// ============================================================================
namespace LHCb {
  class ProtoParticle;
}
namespace LHCb {
  class MuonPID;
}
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-23
 *
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class ProtoHasInfo
     *  The trivial predicate whcii evaluated for true
     *  if the protoparticle "hasInfo".
     *
     *  @see LHCb::Particle
     *  @see LHCb::Particle::proto
     *  @see LHCb::ProtoParticle
     *  @see LHCb::ProtoParticle::hasInfo
     *
     *  @see LoKi::Cuts::PPHASINFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-23
     */
    class GAUDI_API ProtoHasInfo : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// constructor form the index in ProtoParticle::ExtraInfo
      ProtoHasInfo( int index );
      /// clone method (mandatory!)
      ProtoHasInfo* clone() const override { return new ProtoHasInfo( *this ); }
      /// the only one essential method
      result_type operator()( argument p ) const override;
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// index in ProtoParticle::ExtraInfo
      int m_info; // index in ProtoParticle::ExtraInfo
      // ======================================================================
    };
    // ========================================================================
    /** @class ProtoInfo
     *  Trivial function which evaluates LHCb::Particle::info
     *
     *  It relies on the method LHCb::Particle::info
     *
     *  @see LHCb::Particle
     *  @see LHCb::Particle::proto
     *  @see LHCb::ProtoParticle
     *  @see LHCb::ProtoParticle::info
     *  @see LoKi::Cuts::PPINFO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API ProtoInfo : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /** constructor from "info"
       *  @param key info index/mark/key
       *  @param def default valeu for info
       *  @param bad bad value to be retured for invalid particle
       */
      ProtoInfo( const int key, const double def, const double bad );
      /** constructor from "info"
       *  @param key info index/mark/key
       *  @param def default value for info
       */
      ProtoInfo( const int key, const double def );
      /// MANDATORY: clone method ("virtual constructor")
      ProtoInfo* clone() const override { return new ProtoInfo( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// index in ProtoParticle::ExtraInfo
      int m_key; // index in ProtoParticle::ExtraInfo
      /// value to be returned for invalid particle and protoparticle
      double m_def;
      /// default value for missing infomration
      double m_bad;
      // ======================================================================
    };
    // ========================================================================
    /** @class ProtoHasRichPID
     *  the trivial predicate wich tests the validity of
     *  ProtoParticle::richPID
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::ProtoParticle::richPID
     *  @see LHCb::RichPID
     *  @see LoKi::Cuts::PPHASRICH
     *  @see LoKi::Cuts::HASRICH
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API ProtoHasRichPID : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// Default Constructor
      ProtoHasRichPID() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      ProtoHasRichPID* clone() const override { return new ProtoHasRichPID( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class ProtoHasMuonPID
     *  the trivial predicate wich tests the validity of
     *  ProtoParticle::muonPID
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::ProtoParticle::muonPID
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::PPHASMUON
     *  @see LoKi::Cuts::HASMUON
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API ProtoHasMuonPID : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// Default Constructor
      ProtoHasMuonPID() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      ProtoHasMuonPID* clone() const override { return new ProtoHasMuonPID( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class ProtoHasCaloHypos
     *  the trivial predicate wich tests the validity of
     *  ProtoParticle::calo()
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::ProtoParticle::calo
     *  @see LHCb::CaloHypo
     *  @see LoKi::Cuts::PPHASCALO
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2006-02-15
     */
    class GAUDI_API ProtoHasCaloHypos : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// Default Constructor
      ProtoHasCaloHypos() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      ProtoHasCaloHypos* clone() const override { return new ProtoHasCaloHypos( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsMuon
     *  The trivial predicate whith returns LHCb::MuonPID::isMuon
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::ISMUON
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-02-25
     */
    class GAUDI_API IsMuon : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// Default Constructor
      IsMuon() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("constructor")
      IsMuon* clone() const override { return new IsMuon( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    protected:
      // ======================================================================
      /// "extract" muonPID form the particle
      const LHCb::MuonPID* muonPID( const LHCb::Particle* p ) const;
      // ======================================================================
    private:
      // ======================================================================
      /// helper object to decode muon-PID-status
      mutable LHCb::MuonPID m_pid; // helper object to decode muon-PID-status
      // ======================================================================
    };
    // ========================================================================
    /** @class IsMuonLoose
     *  The trivial predicate whith returns LHCb::MuonPID::isMuonLoose
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::ISMUONLOOSE
     *  @see LoKi::Cuts::ISLOOSEMUON
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-09-26
     */
    class GAUDI_API IsMuonLoose : public IsMuon {
    public:
      // ======================================================================
      /// Default Constructor
      IsMuonLoose() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      IsMuonLoose* clone() const override { return new IsMuonLoose( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class IsMuonTight
     *  The trivial predicate whith returns LHCb::MuonPID::isMuonTight
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::ISMUONTIGHT
     *  @see LoKi::Cuts::ISTIGHTMUON
     *
     *  @author Ricardo VAZQUEZ GOMEZ rvazquez@cern.ch
     *  @date 2015-07-16
     */
    class GAUDI_API IsMuonTight : public IsMuon {
    public:
      // ======================================================================
      /// Default Constructor
      IsMuonTight() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      IsMuonTight* clone() const override { return new IsMuonTight( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MuonChi2Correlated
     *  The trivial predicate whith returns LHCb::MuonPID::chi2Corr
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::MUONCHI2CORRELATED
     *
     *  @author Ricardo VAZQUEZ GOMEZ rvazquez@cern.ch
     *  @date 2018-04-06
     */
    class MuonChi2Correlated : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      MuonChi2Correlated() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      MuonChi2Correlated* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MuonMVA1
     *  The trivial predicate whith returns LHCb::MuonPID::MuonMVA1
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::MUONBDT_TMVA
     *
     *  @author Ricardo VAZQUEZ GOMEZ rvazquez@cern.ch
     *  @date 2018-04-06
     */
    class MuonMVA1 : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      MuonMVA1() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      MuonMVA1* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class MuonMVA2
     *  The trivial predicate whith returns LHCb::MuonPID::MuonMVA2
     *
     *  @see LHCb::Particle
     *  @see LHCb::ProtoParticle
     *  @see LHCb::MuonPID
     *  @see LoKi::Cuts::MUONBDT_CATBOOST
     *
     *  @author Ricardo VAZQUEZ GOMEZ rvazquez@cern.ch
     *  @date 2018-04-06
     */
    class MuonMVA2 : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      MuonMVA2() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      MuonMVA2* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class InMuonAcceptance
     *  Simple functor that checks if the particle is in muon acceptance
     *  @thanks Gaia Lanfranchi for kind help!
     *  @see LHcb::MuonPID::InAcceptance
     *  @see LoKi::Cuts::INMUON
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2010-01-08
     */
    class GAUDI_API InMuonAcceptance : public IsMuonLoose {
    public:
      // ======================================================================
      /// Default Constructor
      InMuonAcceptance() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      InMuonAcceptance* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackHasInfo
     *  The trivial predicate whcii evaluated for true
     *  if the tarck "hasInfo".
     *
     *  @see LHCb::Particle
     *
     *  @see LoKi::Cuts::THASINFO
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-02-11
     */
    class GAUDI_API TrackHasInfo : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// constructor from the index in Track::ExtraInfo
      TrackHasInfo( int index );
      /// clone method (mandatory!)
      TrackHasInfo* clone() const override { return new TrackHasInfo( *this ); }
      /// the only one essential method
      result_type operator()( argument p ) const override;
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// index in Track::ExtraInfo
      int m_info; // index in Track::ExtraInfo
      // ======================================================================
    };
    // ========================================================================
    /** @class TrackInfo
     *  Trivial function which evaluates LHCb::Track::info
     *
     *  It relies on the method LHCb::Track::info
     *
     *  @see LHCb::Particle
     *  @see LHCb::Particle::proto
     *  @see LHCb::Track
     *  @see LHCb::Track::info
     *  @see LoKi::Cuts::TINFO
     *  @see LoKi::Cuts::TRINFO
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-02-11
     */
    class GAUDI_API TrackInfo : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
      // ======================================================================
    public:
      // ======================================================================
      /** constructor from "info"
       *  @param key info index/mark/key
       *  @param def default valeu for info
       *  @param bad bad value to be retured for invalid particle
       */
      TrackInfo( const int key, const double def, const double bad );
      /** constructor from "info"
       *  @param key info index/mark/key
       *  @param def default value for info
       */
      TrackInfo( const int key, const double def );
      /// MANDATORY: clone method ("constructor")
      TrackInfo* clone() const override { return new TrackInfo( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // =======================================================================
      /// index in Track::ExtraInfo
      int m_key; // index in ProtoParticle::ExtraInfo
      /// value to be returned for invalid particle and protoparticle
      double m_def; // value to be returned for invalid objects
      /// default value for missing information
      double m_bad; // default value for missing information
      // ======================================================================
    };
    // ========================================================================
    /** @class NShared
     *  Get number of shared muon hits
     *  @see LoKi::Cuts::NSHAREDMU
     *  @see LHCb::MuonPID::nShared
     *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
     *  @date 2010-12-10
     */
    class NShared : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      NShared() : AuxFunBase{ std::tie() } {}
      /// MANDATORY: clone method ("virtual constructor")
      NShared* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the nice printtout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES12_H
// ============================================================================
