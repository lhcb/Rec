/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/ImpactParamTool.h"
#include "LoKi/Keeper.h"
#include "LoKi/PhysRangeTypes.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/UniqueKeeper.h"

/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-19
 */

namespace LoKi {

  namespace Particles {

    /** @class ClosestApproach
     *
     *  evaluator of the closest approach
     *  distance between 2 particles
     *
     *  The tool IDistanceCalculator is used
     *  for evaluation
     *
     *  @see LHCb::Particle
     *  @see IDistanceCalculator
     *  @see LoKi::Cuts::CLAPP
     *
     *  @date 2003-03-17
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    class GAUDI_API ClosestApproach : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                      virtual public LoKi::AuxDesktopBase,
                                      public LoKi::Vertices::ImpactParamTool {
    public:
      /// constructor from the particle and the tool
      ClosestApproach( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                       const LoKi::Vertices::ImpactParamTool& tool, const bool allow = false );
      /// constructor from the particle and the tool
      ClosestApproach( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                       const LHCb::Particle* particle, const bool allow = false );
      /// MANDATORY: clone method ("virtual constructor")
      ClosestApproach* clone() const override { return new ClosestApproach( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return distance( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

      /// the actual evaluation
      result_type distance( argument p, IGeometryInfo const& geometry ) const {
        return distance_( p, particle(), geometry );
      }
      /// the actual evaluation
      result_type distance_( LHCb::Particle const* p1, LHCb::Particle const* p2, IGeometryInfo const& geometry ) const;

      /// the actual evaluation of chi2
      result_type chi2_( LHCb::Particle const* p1, LHCb::Particle const* p2, IGeometryInfo const& geometry ) const;

      /// the actual evaluation
      result_type chi2( argument p, IGeometryInfo const& geometry ) const { return chi2_( p, particle(), geometry ); }

      /// allow transitions between categories?
      bool allow() const { return m_allow; } // allow transitions between categories?

      /// accessor to the particle
      const LHCb::Particle* particle() const { return m_particle; }
      /// set new particle
      void setParticle( const LHCb::Particle* value ) const { m_particle = value; }

    protected:
      /// the particle
      mutable const LHCb::Particle* m_particle;
      /// allo transitions ?
      bool m_allow;
    };

    /** @class ClosestApproachChi2
     *
     *  evaluator of the closest approach chi2
     *  distance between 2 particles
     *
     *  The tool IDistanceCalculator is used
     *  for evaluation
     *
     *  @see LHCb::Particle
     *  @see IDistanceCalculator
     *  @see LoKi::Cuts::CLAPPCHI2
     *
     *  @date 2003-03-17
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    struct GAUDI_API ClosestApproachChi2 : ClosestApproach {

      /// constructor from the particle and the tool
      ClosestApproachChi2( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                           const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle and the tool
      ClosestApproachChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                           const LHCb::Particle* particle );
      /// MANDATORY: clone method ("virtual constructor")
      ClosestApproachChi2* clone() const override { return new ClosestApproachChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return chi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class MinClosestApproach
     *
     *  evaluator of the minimal value of the
     *  closest approach distance between the
     *  particle and soem other particles
     *
     *  The tool IDistanceCalculator is used
     *  for evaluation
     *
     *  @see LHCb::Particle
     *  @see IDistanceCalculator
     *  @see LoKi::Particles::ClosestApproach
     *
     *  @date 2003-03-17
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    class GAUDI_API MinClosestApproach : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                         virtual public LoKi::AuxDesktopBase,
                                         public LoKi::UniqueKeeper<LHCb::Particle> {
    public:
      /// constructor from the particle(s) and the tool
      MinClosestApproach( IDVAlgorithm const* algorithm, const LHCb::Particle::ConstVector& particles,
                          const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle(s) and the tool
      MinClosestApproach( IDVAlgorithm const* algorithm, const LoKi::PhysTypes::Range& particles,
                          const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle(s) and the tool
      MinClosestApproach( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                          const LHCb::Particle::ConstVector& particles );
      /// constructor from the particle(s) and the tool
      MinClosestApproach( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                          const LoKi::PhysTypes::Range& particles );

      /** templated contructor
       *  from the sequence of "particles"
       *  @param first 'begin'-iterator for the sequence
       *  @param last  'end'-iterator for the sequence
       *  @param tool  helper tool needed for evaluation
       */
      template <class PARTICLE>
      MinClosestApproach( IDVAlgorithm const* algorithm, PARTICLE first, PARTICLE last,
                          const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::AuxDesktopBase( algorithm )
          , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
          , LoKi::UniqueKeeper<LHCb::Particle>( first, last )
          , m_fun( algorithm, (const LHCb::Particle*)0, tool ) {}
      /** templated contructor
       *  from the sequence of "particles"
       *  @param tool  helper tool needed for evaluation
       *  @param first 'begin'-iterator for the sequence
       *  @param last  'end'-iterator for the sequence
       */
      template <class PARTICLE>
      MinClosestApproach( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool, PARTICLE first,
                          PARTICLE last )
          : LoKi::AuxDesktopBase( algorithm )
          , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
          , LoKi::UniqueKeeper<LHCb::Particle>( first, last )
          , m_fun( algorithm, (const LHCb::Particle*)0, tool ) {}

      /// MANDATORY: clone method ("virtual constructor")
      MinClosestApproach* clone() const override { return new MinClosestApproach( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return distance( p, desktop()->geometry() ); }
      /// notify that we need the context algorithm
      static bool context_dvalgo() { return true; }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// the actual evaluation
      result_type distance( argument p, IGeometryInfo const& geometry ) const;
      /// the actual evaluation
      result_type chi2( argument p, IGeometryInfo const& geometry ) const;

    private:
      LoKi::Particles::ClosestApproach m_fun;
    };

    /** @class MinClosestApproachChi2
     *
     *  evaluator of the minimal value of the chi2 of the
     *  closest approach distance between the
     *  particle and soem other particles
     *
     *  The tool IDistanceCalculator is used
     *  for evaluation
     *
     *  @see LHCb::Particle
     *  @see IDistanceCalculator
     *  @see LoKi::Particles::ClosestApproachChi2
     *
     *  @date 2003-03-17
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     */
    struct GAUDI_API MinClosestApproachChi2 : MinClosestApproach {

      /// constructor from the particle(s) and the tool
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, const LHCb::Particle::ConstVector& particles,
                              const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle(s) and the tool
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, const LoKi::PhysTypes::Range& particles,
                              const LoKi::Vertices::ImpactParamTool& tool );
      /// constructor from the particle(s) and the tool
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                              const LHCb::Particle::ConstVector& particles );
      /// constructor from the particle(s) and the tool
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                              const LoKi::PhysTypes::Range& particles );
      /** templated contructor
       *  from the sequence of "particles"
       *  @param first 'begin'-iterator for the sequence
       *  @param last  'end'-iterator for the sequence
       *  @param tool helepr tool needed for evaluations
       */
      template <class PARTICLE>
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, PARTICLE first, PARTICLE last,
                              const LoKi::Vertices::ImpactParamTool& tool )
          : LoKi::Particles::MinClosestApproach( algorithm, first, last, tool ) {}
      /** templated contructor
       *  from the sequence of "particles"
       *  @param tool helepr tool needed for evaluations
       *  @param first 'begin'-iterator for the sequence
       *  @param last  'end'-iterator for the sequence
       */
      template <class PARTICLE>
      MinClosestApproachChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                              PARTICLE first, PARTICLE last )
          : LoKi::Particles::MinClosestApproach( algorithm, first, last, tool ) {}
      /// MANDATORY: clone method ("virtual constructor")
      MinClosestApproachChi2* clone() const override { return new MinClosestApproachChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return chi2( p, desktop()->geometry() ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

  } // namespace Particles

} //                                                      end of namespace LoKi
