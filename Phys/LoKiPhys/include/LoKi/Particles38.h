/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/ParticleProperties.h"
#include "LoKi/Particles0.h"
#include "LoKi/Services.h"
#include "LoKi/VertexHolder.h"

/** @file LoKi/Particles38.h
 *
 *  Collection of functions for 'corrected mass' variable by Mike Williams,
 *  see <a href="http://indico.cern.ch/conferenceDisplay.py?confId=104542">
 *  Mike's slides at Trigger meeting 28 September 2k+10 </a> and
 *  <a href="http://savannah.cern.ch/task/?17418">Savannah task #17418</a>
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @thanks Mike Williams
 *
 *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
 *  @date   2010-10-23
 */
namespace LoKi {

  namespace Particles {

    /** @class PtFlight
     *  Simple evaluator for transverse momentum relative to flight direction
     *  @see LoKi::Cuts:PTFLIGHT
     *  @see LoKi::Particles::TransverseMomentumRel
     *  @thanks Mike Williams
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */

    struct GAUDI_API PtFlight : LoKi::Particles::TransverseMomentumRel,
                                virtual LoKi::AuxDesktopBase,
                                LoKi::Vertices::VertexHolder {

      /** constructor from the primary vertex
       *  @param pv  the primary vertex
       */
      PtFlight( IDVAlgorithm const* algo, const LHCb::VertexBase* pv );
      /** constructor from the primary vertex
       *  @param point  the position of primary vertex
       */
      PtFlight( IDVAlgorithm const* algo, const LoKi::Point3D& point );
      /** constructor from the primary vertex
       *  @param x the x-position of primary vertex
       *  @param y the x-position of primary vertex
       *  @param z the x-position of primary vertex
       */
      PtFlight( IDVAlgorithm const* algo, const double x, const double y, const double z );
      /// MANDATORY: clone method ("virtual constructor")
      PtFlight* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

      /** evaluate the transverse momentum versus the flight direction
       *  @param p  (INPUT) the 4-momentum
       *  @param sv (INPUT) the decay  vertex of the particle
       *  @param pv (INPUT) the origin vertex of the particle
       *  @return the transverse momentum versus the fligth direction
       *  @see LoKi::TransverseMomentumRel::ptDir
       */
      double ptFlight( const LoKi::LorentzVector& p, const LoKi::Point3D& sv, const LoKi::Point3D& pv ) const {
        return ptDir( p, sv - pv );
      }

      /** evaluate the 'corrected' mass of the particle
       *  \f[  \vec{i} = \sqrt{ M^2 +
       *            \left|p_{T}^{\prime}\right|^2 } +
       *            \left|p_{T}^{\prime}\right|, \f]
       *  where \f$ \left|p_{T}^{\prime}\right|\f$ stands for the
       *  @param p  (INPUT) the 4-momentum
       *  @param sv (INPUT) the decay  vertex of the particle
       *  @param pv (INPUT) the origin vertex of the particle
       *  @return the corrected mass
       *  @see LoKi::PtFlight::ptDir
       *  @see LoKi::TransverseMomentumRel::ptDir
       *  @thanks Mike Williams
       */
      double mCorrFlight( const LoKi::LorentzVector& p, const LoKi::Point3D& sv, const LoKi::Point3D& pv ) const {
        return mCorrDir( p, sv - pv );
      }
    };

    /** @class MCorrected
     *  Simple evaluator for 'corrected' mass relative to flight direction
     *  \f[  \vec{i} = \sqrt{ M^2 +
     *            \left|p_{T}^{\prime}\right|^2 } +
     *            \left|p_{T}^{\prime}\right|, \f]
     *  where \f$ \left|p_{T}^{\prime}\right|\f$ stands for the
     *  transverse momentum relative to the fligth direction
     *
     *  For more information see
     *  <a href="http://indico.cern.ch/conferenceDisplay.py?confId=104542">
     *  Mike's slides at Trigger meeting 28 September 2k+10 </a>
     *
     *  @thanks Mike Williams
     *  @see LoKi::Cuts::CORRM
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     */

    struct GAUDI_API MCorrected : LoKi::Particles::PtFlight {

      /** constructor from the primary vertex
       *  @param pv  the primary vertex
       */
      MCorrected( IDVAlgorithm const* algo, const LHCb::VertexBase* pv );
      /** constructor from the primary vertex
       *  @param point  the position of primary vertex
       */
      MCorrected( IDVAlgorithm const* algo, const LoKi::Point3D& point );
      /** constructor from the primary vertex
       *  @param x the x-position of primary vertex
       *  @param y the x-position of primary vertex
       *  @param z the x-position of primary vertex
       */
      MCorrected( IDVAlgorithm const* algo, const double x, const double y, const double z );
      /// MANDATORY: clone method ("virtual constructor")
      MCorrected* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class BremMCorrected
     *  Simple evaluator for 'HOP' mass
     *
     *  For more information see
     *  <a href="https://cds.cern.ch/record/2102345/files/LHCb-INT-2015-037.pdf">
     *
     *  @author Pavol Stefko pavol.stefko@epfl.ch
     *  @author Guido Andreassi guido.andreassi@epfl.ch
     *  @author Violaine Bellee violaine.bellee@epfl.ch
     *  @date   2017-01-17
     */

    struct GAUDI_API BremMCorrected : LoKi::Particles::PtFlight {

      /** constructor from the primary vertex
       *  @param pv  the primary vertex
       */
      BremMCorrected( IDVAlgorithm const* algo, const LHCb::VertexBase* pv );
      /** constructor from the primary vertex
       *  @param point  the position of primary vertex
       */
      BremMCorrected( IDVAlgorithm const* algo, const LoKi::Point3D& point );
      /** constructor from the primary vertex
       *  @param x the x-position of primary vertex
       *  @param y the x-position of primary vertex
       *  @param z the x-position of primary vertex
       */
      BremMCorrected( IDVAlgorithm const* algo, const double x, const double y, const double z );
      // MANDATORY: clone method ("virtual constructor")
      BremMCorrected* clone() const override;
      // MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      // OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

    private:
      static double           m_mass;
      static LHCb::ParticleID m_pid;
      bool                    has_electron( const LHCb::Particle* parent ) const;
      bool                    has_only_electrons( const LHCb::Particle* parent ) const;
      bool                    classify( const LHCb::Particle* parent, LHCb::Particle::ConstVector& m_all_electrons,
                                        LHCb::Particle::ConstVector& m_rest_electrons, LHCb::Particle::ConstVector& m_electron_mothers,
                                        LHCb::Particle::ConstVector& m_others ) const;
    };

    /** @class PtFlightWithBestVertex
     *  Simple evaluator for transverse momentum relative to flight direction
     *  @see LoKi::Cuts::BPVPTFLIGHT
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */

    struct GAUDI_API PtFlightWithBestVertex : LoKi::Particles::PtFlight {

      /// constructor
      PtFlightWithBestVertex( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      PtFlightWithBestVertex* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class MCorrectedWithBestVertex
     *  Simple evaluator for corrected mass relative to flight direction
     *  \f[  \vec{i} = \sqrt{ M^2 +
     *            \left|p_{T}^{\prime}\right|^2 } +
     *            \left|p_{T}^{\prime}\right|, \f]
     *  where \f$ \left|p_{T}^{\prime}\right|\f$ stands for the
     *  transverse momentum relative to the fligth direction
     *
     *  For more information see
     *  <a href="http://indico.cern.ch/conferenceDisplay.py?confId=104542">
     *  Mike's slides at Trigger meeting 28 September 2k+10 </a>
     *
     *  @see LoKi::Cuts::BPVCORRM
     *  @see LoKi::Cuts::CORRM
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */

    struct GAUDI_API MCorrectedWithBestVertex : PtFlightWithBestVertex {

      /// constructor
      MCorrectedWithBestVertex( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      MCorrectedWithBestVertex* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class BremMCorrectedWithBestVertex
     *  Simple evaluator for 'HOP' mass
     *
     *  For more information see
     *  <a href="https://cds.cern.ch/record/2102345/files/LHCb-INT-2015-037.pdf">
     *
     *  @author Pavol Stefko pavol.stefko@epfl.ch
     *  @author Guido Andreassi guido.andreassi@epfl.ch
     *  @author Violaine Bellee violaine.bellee@epfl.ch
     *  @date   2017-01-17
     */

    struct GAUDI_API BremMCorrectedWithBestVertex : PtFlightWithBestVertex {

      /// constructor
      BremMCorrectedWithBestVertex( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      BremMCorrectedWithBestVertex* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

  } // namespace Particles

  namespace Cuts {

    /** @typedef PTFLIGHT
     *  Simple functor to evaluate the transverse momentum with respect to
     *  particle fligth direction
     *
     *  @code
     *
     *   // get the primary vertex:
     *   const LHCb::VertexBase* primary = ... ;
     *
     *   // create the functor
     *   const PTFLIGHT fun = PTFLIGHT ( primary ) ;
     *
     *   const LHCb::Particle* B = ... ;
     *
     *   // use functor:
     *   const double ptFlight = fun ( B ) ;
     *
     *  @endcode
     *
     *  @see LoKi::Particles::PtFlight
     *  @see LoKi::Particles::TransverseMomentumRel
     *  @see LoKi::Cuts::PTDIR
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */
    typedef LoKi::Particles::PtFlight PTFLIGHT;

    /** @typedef BPVPTFLIGHT
     *  Simple functor to evaluate the transverse momentum with respect to
     *  particle flight direction
     *  @see LoKi::Particles::PtFlightWithBestVertex
     *  @see LoKi::Particles::PtFlight
     *  @see LoKi::Particles::TransverseMomentumRel
     *  @see LoKi::Cuts::PTFLIGHT
     *  @see LoKi::Cuts::PTDIR
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */
    typedef LoKi::Particles::PtFlightWithBestVertex BPVPTFLIGHT;

    /** @typedef CORRM
     *  Simple functor to evaluate the 'corrected mass' with respect to
     *  particle fligth direction
     *
     *  @code
     *
     *   // get the primary vertex:
     *   const LHCb::VertexBase* primary = ... ;
     *
     *   // create the functor
     *   const CORRM fun =  CORRM ( primary ) ;
     *
     *   const LHCb::Particle* B = ... ;
     *
     *   // use functor:
     *   const double mCorr = fun ( B ) ;
     *
     *  @endcode
     *
     *  For more information see
     *  <a href="http://indico.cern.ch/conferenceDisplay.py?confId=104542">
     *  Mike's slides at Trigger meeting 28 September 2k+10 </a>
     *
     *  @see LoKi::Particles::MCorrected
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */
    typedef LoKi::Particles::MCorrected CORRM;

    /** @typedef BPVCORRM
     *  Simple functor to evaluate the corrected mass with respect to
     *  particle flight direction
     *
     *  For more information see
     *  <a href="http://indico.cern.ch/conferenceDisplay.py?confId=104542">
     *  Mike's slides at Trigger meeting 28 September 2k+10 </a>
     *
     *  @see LoKi::Particles::MCorrectedWithBestVertex
     *  @see LoKi::Particles::MCorrected
     *  @see LoKi::Particles::PtFlightWithBestVertex
     *  @see LoKi::Particles::PtFlight
     *  @see LoKi::Particles::TransverseMomentumRel
     *  @see LoKi::Cuts::CORRM
     *  @see LoKi::Cuts::PTFLIGHT
     *  @see LoKi::Cuts::BPVPTFLIGHT
     *  @see LoKi::Cuts::PTDIR
     *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
     *  @date   2010-10-23
     *  @thanks Mike Williams
     */
    typedef LoKi::Particles::MCorrectedWithBestVertex BPVCORRM;

    /** @typedef HOPM
     *  Simple functor to evaluate the HOP mass with respect to
     *  particle flight direction
     *
     *  For more information see
     *  <a href="https://cds.cern.ch/record/2102345/files/LHCb-INT-2015-037.pdf">
     *
     *  @see LoKi::Particles::MCorrectedWithBestVertex
     *  @see LoKi::Particles::MCorrected
     *  @see LoKi::Particles::PtFlightWithBestVertex
     *  @see LoKi::Particles::PtFlight
     *  @see LoKi::Cuts::CORRM
     *  @see LoKi::Cuts::PTFLIGHT
     *  @see LoKi::Cuts::BPVPTFLIGHT
     *  @author Pavol Stefko pavol.stefko@epfl.ch
     *  @author Guido Andreassi guido.andreassi@epfl.ch
     *  @author Violaine Bellee violaine.bellee@epfl.ch
     *  @date   2017-01-17
     *  @thanks Albert Puig Navarro
     */
    typedef LoKi::Particles::BremMCorrected HOPM;

    /** @typedef BPVCORRM
     *  Simple functor to evaluate the corrected mass with respect to
     *  particle flight direction
     *
     *  For more information see
     *  <a href="https://cds.cern.ch/record/2102345/files/LHCb-INT-2015-037.pdf">
     *
     *  @see LoKi::Particles::MCorrectedWithBestVertex
     *  @see LoKi::Particles::MCorrected
     *  @see LoKi::Particles::PtFlightWithBestVertex
     *  @see LoKi::Particles::PtFlight
     *  @see LoKi::Cuts::CORRM
     *  @see LoKi::Cuts::PTFLIGHT
     *  @see LoKi::Cuts::BPVPTFLIGHT
     *  @author Pavol Stefko pavol.stefko@epfl.ch
     *  @author Guido Andreassi guido.andreassi@epfl.ch
     *  @author Violaine Bellee violaine.bellee@epfl.ch
     *  @date   2017-01-17
     *  @thanks Albert Puig Navarro
     */
    typedef LoKi::Particles::BremMCorrectedWithBestVertex BPVHOPM;

  } // namespace Cuts

} //                                                      end of namespace LoKi
