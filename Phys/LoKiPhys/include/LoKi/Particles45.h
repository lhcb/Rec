/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LOKI_PARTICLES45_H
#define LOKI_PARTICLES45_H 1
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
#include "Event/RelatedInfoMap.h"
#include "Relations/IRelation.h"
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Interface.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/PhysTypes.h"
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#include "Kernel/IParticleValue.h"
// ============================================================================
/** @file
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Sebastian Neubert
 *  @date 2013-08-05
 */
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class RelatedInfo
     *  Functor for accessing related info.
     *  @see IParticleValue
     *  @see LoKi::Cuts::VALUE
     *  @author Sebastian Neubert
     *  @date 31/07/2017
     */
    class GAUDI_API RelatedInfo : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor from
      RelatedInfo( const std::string& location, const short index, const double bad = -1000 );

      // ======================================================================
      /// constructor from
      RelatedInfo( const std::string& location, const std::string& variable, const double bad = -1000 );
      /// MANDATORY: clone method ("virtual constructor")
      RelatedInfo* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      std::string m_location;
      short       m_index;
      double      m_bad{ -1000 };
      // ======================================================================
      typedef IRelation<LHCb::Particle, LHCb::RelatedInfoMap> IMAP;
      mutable const IMAP*                                     m_table = nullptr;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    typedef LoKi::Particles::RelatedInfo RELINFO;
    // ========================================================================
  } // namespace Cuts
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES45_H
