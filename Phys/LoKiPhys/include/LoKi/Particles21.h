/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES21_H
#  define LOKI_PARTICLES21_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/Particles0.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class DeltaNominalMass
     *  simple function which evaluates the delta mass with respect
     *  to the nominal mass
     *  @see LoKi::Cuts::DPDGM
     *  @see LoKi::Cuts::DPDGMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-09-23
     */
    class GAUDI_API DeltaNominalMass : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// Default Constructor
      DeltaNominalMass() = default;
      /// MANDATORY: clone method ("virtual constructor")
      DeltaNominalMass* clone() const override { return new DeltaNominalMass( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return delta( p ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "DPDGM"; }
      // ======================================================================
    public:
      // ======================================================================
      /// the evalautor of the delta mass
      double delta( argument p ) const;
      // ======================================================================
    };
    // ========================================================================
    /** @class AbsDeltaNominalMass
     *  simple function whcih evalaute the absolute value of
     *  delta mass with respect to the nominal mass
     *  @see LoKi::Cuts::ADPDGM
     *  @see LoKi::Cuts::ADPDGMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-09-23
     */
    class GAUDI_API AbsDeltaNominalMass : public DeltaNominalMass {
    public:
      // ======================================================================
      /// Default Constructor
      AbsDeltaNominalMass() = default;
      /// MANDATORY: clone method ("virtual constructor")
      AbsDeltaNominalMass* clone() const override { return new AbsDeltaNominalMass( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return ::fabs( delta( p ) ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "ADPDGM"; }
      // ======================================================================
    public:
      // ======================================================================
      /// the evalautor of the delta mass
      double adelta( argument p ) const;
      // ======================================================================
    };
    // ========================================================================
    /** @class DeltaNominalMassChi2
     *  simple function whcih evaluate the che2 of delta mass with resepct
     *  to the nominal mass
     *  @see LoKi::Cuts::CHI2PDGM
     *  @see LoKi::Cuts::CHI2PDGMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-09-23
     */
    class DeltaNominalMassChi2 : public AbsDeltaNominalMass {
    public:
      // ======================================================================
      /// Default Constructor
      DeltaNominalMassChi2() = default;
      /// MANDATORY: clone method ("virtual constructor")
      DeltaNominalMassChi2* clone() const override { return new DeltaNominalMassChi2( *this ); }
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override { return chi2( p ); }
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "CHI2PDGM"; }
      // ======================================================================
    public:
      // ======================================================================
      /// the evalautor of the delta mass
      double chi2( argument p ) const;
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES21_H
// ============================================================================
