/*****************************************************************************\
 * (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "SelTools/Utilities.h"
#include "Event/Track.h"

const LHCb::State* Sel::Utils::defaultStateForParticle( LHCb::Track const& track ) {
  const LHCb::State* state = nullptr;

  // Default: closest to the beam for track types with a Velo part.
  if ( !( track.checkType( LHCb::Track::Types::Downstream ) || track.checkType( LHCb::Track::Types::Ttrack ) ) ) {
    state = track.stateAt( LHCb::State::Location::ClosestToBeam );
  }
  // If not available: first measurement. Default for Downstream and T tracks
  if ( !state ) { state = track.stateAt( LHCb::State::Location::FirstMeasurement ); }

  return state;
};

void Sel::Utils::setPVForComposite( LHCb::Particle& newParticle, LHCb::Vertex const* newVertex,
                                    LHCb::PrimaryVertices const& pvs ) {
  // assign the PV, but only if a real vertex fit was run
  if ( newVertex && newVertex->nDoF() >= 0 ) {
    const auto pv = LHCb::bestPV( pvs, newVertex->position(), newParticle.momentum() );
    if ( pv ) newParticle.setPV( pv );
  } else {
    // Set the pv to the first valid pv of a daughter (not necessarily the best choice).
    // Check that the daughter has some vertex info, because merged pi0s and photons also may have
    // their pv set. (See NeutralParticleMaker.) We may want to move this logic into a separate function.
    for ( const auto& dau : newParticle.daughters() ) {
      if ( dau->pv() && ( dau->endVertex() || ( dau->proto() && dau->proto()->track() ) ) ) {
        newParticle.setPV( dau->pv() );
        break;
      }
    }
  }
}
