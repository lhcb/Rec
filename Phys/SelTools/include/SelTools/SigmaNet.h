/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Environment.h"
#include "GaudiKernel/GaudiException.h"
#include "Kernel/STLExtensions.h"
#include "MVAUtils.h"
#include <GaudiKernel/IFileAccess.h>

#include <Gaudi/Algorithm.h>
#include <any>
#include <nlohmann/json.hpp>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <boost/range/algorithm_ext/erase.hpp>

namespace Sel {

  template <typename T>
  T const& get_mva_config( MVA_config_dict dict, std::string_view k ) {
    auto i = dict.find( k );
    if ( i == dict.end() ) throw std::out_of_range{ std::string{ "Unknown key " }.append( k ) };
    return std::any_cast<T const&>( i->second );
  }

  inline void groupsort2( LHCb::span<float> x ) {
    for ( long unsigned int i = 0; i + 1 < x.size(); i += 2 ) {
      if ( x[i] > x[i + 1] ) std::swap( x[i], x[i + 1] );
    }
  }

  inline void multiply_and_add( LHCb::span<const float> w, LHCb::span<const float> b, LHCb::span<const float> x,
                                LHCb::span<float> y ) {
    assert( w.size() == y.size() * x.size() );
    assert( b.size() == y.size() );
    for ( long unsigned int i = 0; i < y.size(); ++i ) {
      y[i] = std::inner_product( x.begin(), x.end(), w.begin() + i * x.size(), b[i] );
    }
  }

  inline float sigmoid( float x ) { return 1 / ( 1 + expf( -1 * x ) ); }

  template <size_t N, typename input_type>
  float evaluate( LHCb::span<input_type const> values, LHCb::span<const float> clamp_los,
                  LHCb::span<const float> clamp_his, LHCb::span<const float> weights, LHCb::span<const float> biases,
                  LHCb::span<const long unsigned int> layer_sizes, LHCb::span<const float> monotone_constraints,
                  float lambda ) {

    assert( values.size() == layer_sizes.front() );

    std::array<float, 2 * N> storage{};
    auto                     input_for = [&]( long unsigned int layer ) {
      assert( layer > 0 && layer <= layer_sizes.size() );
      assert( layer_sizes[layer - 1] <= N );
      auto ret = LHCb::span{ storage };
      return ret.subspan( N * ( ( layer - 1 ) % 2 ), layer_sizes[layer - 1] );
    };
    auto output_for = [&]( int layer ) { return input_for( layer + 1 ); };

    std::copy( values.begin(), values.end(), input_for( 1 ).begin() );

    if ( clamp_los.size() > 0 ) /* there are clamping values available, so use them */ {
      auto vals = input_for( 1 );
      // do the clamping of the variables before sending it through an evaluation
      for ( uint32_t i = 0; i < vals.size(); ++i ) { vals[i] = std::clamp( vals[i], clamp_los[i], clamp_his[i] ); }
    }

    int n_layers = layer_sizes.size() - 1;
    for ( int layer = 1; layer < n_layers + 1; ++layer ) {

      auto input  = input_for( layer );
      auto output = output_for( layer );

      // output = b + W * input
      multiply_and_add( weights.first( input.size() * output.size() ), biases.first( output.size() ), input, output );
      // point to next layers weights and biases
      weights = weights.subspan( output.size() * input.size() );
      biases  = biases.subspan( output.size() );

      // activation (if not last layer)
      if ( layer != n_layers ) groupsort2( output );
    }

    auto response = output_for( n_layers ).front() +
                    lambda * std::inner_product( values.begin(), values.end(), monotone_constraints.begin(), 0.0f );
    return sigmoid( response );
  }

  class SigmaNet {
  public:
    using input_type = float;

    /////////////////////////////////// SigmaNet //////////////////////////////////////////////////////////
    // Implementation of a monotonic Lipschitz neural net
    ///////////////////////////////////////////////////////////////////////////////////////////////////////
    SigmaNet( MVA_config_dict config ) : m_config( std::move( config ) ) {}

    auto operator()( LHCb::span<input_type const> values ) const {
      if ( values.size() != m_input_size ) { throw exception( "'InputSize' does not agree with provided Input!!!" ); }
      return evaluate<m_size>( values, m_clamp_los, m_clamp_his, m_weights, m_biases, m_layer_sizes,
                               m_monotone_constraints, m_lambda );
    }

    /** Return the list of expected inputs. */
    std::vector<std::string> const& variables() const { return m_variables; }

    void bind( Gaudi::Algorithm* alg ) {
      m_filesvc.emplace( "ParamFileSvc", alg->name() );
      m_filesvc->retrieve().ignore();
      readConfig( *alg );      // Retrieve the configuration
      readWeightsFile( *alg ); // Read the .pkl File with the weights
      if ( alg->msgLevel( MSG::VERBOSE ) ) {
        alg->verbose() << "Variables found in " << m_weightfile << ": ";
        for ( auto& var : m_variables ) { alg->verbose() << "'" << var << "' "; }
        alg->verbose() << endmsg;
      }
    }

  private:
    MVA_config_dict                           m_config;
    std::vector<std::string>                  m_variables;  // Input variables needed by the classifier
    std::string                               m_name;       // Name of the method to book
    std::string                               m_weightfile; // File containing the trained weights
    std::optional<ServiceHandle<IFileAccess>> m_filesvc;

    unsigned int                   m_input_size;               // Number of input functors
    int                            m_n_layers;                 // Number of layers (input layer not counted)
    std::vector<long unsigned int> m_layer_sizes;              // Width of each layer including input layer
                                                               // [input_size,...,output_size]
    std::vector<float>                 m_monotone_constraints; // List of monotone constraints (Possible values -1,0,1)
    float                              m_lambda;               // Upper bound on the weight norm
    std::vector<float>                 m_weights;              // Network weights
    std::vector<float>                 m_biases;               // Network bias
    std::vector<float>                 m_clamp_los;            // in case of clamping, clamp to these values (lower)
    std::vector<float>                 m_clamp_his;            // in case of clamping, clamp to these values (upper)
    static constexpr long unsigned int m_size =
        128; // maximum width of the network. 128 is enough as the network can alternatively be made deeper.

  private:
    // Helper functions

    // Gaudi exception handler
    GaudiException exception( std::string s ) const {
      return GaudiException{ std::move( s ), "Sel::SigmaNet", StatusCode::FAILURE };
    }

    // Retrieve the configuration
    void readConfig( Gaudi::Algorithm const& alg ) {

      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "Start reading config..." << endmsg; }

      constexpr auto mandatory_keys =
          std::array{ "Name", "File", "Monotone_Constraints", "InputSize", "NLayers", "Lambda" };
      for ( auto const& key : mandatory_keys ) {
        if ( m_config.find( key ) == m_config.end() ) throw exception( "'" + std::string{ key } + " not provided" );
      }

      if ( alg.msgLevel( MSG::VERBOSE ) ) {
        alg.verbose() << "Finished checking config input. Everythings alright..." << endmsg;
      }

      //////////////////////////////////// Read Input size
      m_input_size = std::stoi( get_mva_config<std::string>( m_config, "InputSize" ) );
      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "input_size: " << m_input_size << endmsg; }

      //////////////////////////////////// Read Name
      m_name = get_mva_config<std::string>( m_config, "Name" );
      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "m_name: " << m_name << endmsg; }

      //////////////////////////////////// Read monotone constraints
      std::string monotone_constraints_string = get_mva_config<std::string>( m_config, "Monotone_Constraints" );
      if ( alg.msgLevel( MSG::VERBOSE ) ) {
        alg.verbose() << "monotone constraints: " << monotone_constraints_string << endmsg;
      }
      std::string              delimiter = ",";
      std::vector<std::string> temp_str_monotone_constraints;
      boost::remove_erase_if( monotone_constraints_string, boost::is_any_of( "[] " ) );
      boost::split( temp_str_monotone_constraints, monotone_constraints_string, boost::is_any_of( delimiter ) );
      for ( const auto& str : temp_str_monotone_constraints ) {
        int int_mon_constraint = std::stoi( str );
        if ( int_mon_constraint != 1 && int_mon_constraint != 0 && int_mon_constraint != -1 ) {
          throw exception( "Monotone constraints can only be 1,0,-1." );
        }
        m_monotone_constraints.push_back( int_mon_constraint );
      }

      ///////////////////////////////////////////////// Read lambda
      m_lambda = std::stof( get_mva_config<std::string>( m_config, "Lambda" ) );
      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "labmda: " << m_lambda << endmsg; }

      //////////////////////////////////////////////// Read number of layers
      m_n_layers = std::stoi( get_mva_config<std::string>( m_config, "NLayers" ) );
      assert( m_n_layers > 0 );
      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "nlayers: " << m_n_layers << endmsg; }

      ///////////////////////////////////////////////// Read input vars
      std::string vars_string = get_mva_config<std::string>( m_config, "Variables" );
      boost::remove_erase_if( vars_string, boost::is_any_of( "[] " ) );
      m_variables.clear();
      boost::split( m_variables, vars_string, boost::is_any_of( delimiter ) );

      ///////////////////////////////////////////////// Read weight file
      m_weightfile = get_mva_config<std::string>( m_config, "File" );
      if ( alg.msgLevel( MSG::VERBOSE ) ) { alg.verbose() << "weightfile: " << m_weightfile << endmsg; }
    }

    void readWeightsFile( Gaudi::Algorithm const& alg ) {
      // Check that the weightfile exists
      auto fin = m_filesvc.value()->read( m_weightfile );
      if ( !fin ) { throw exception( "Couldn't open file: " + m_weightfile + " using ParamFileSvc" ); }

      unsigned int num_layer              = 0;
      unsigned int total_number_of_layers = 1;
      unsigned int weights_counter        = 0;
      unsigned int bias_counter           = 0;

      auto weight_sizes = std::vector<int>( m_n_layers, 0 );
      auto bias_sizes   = std::vector<int>( m_n_layers, 0 );

      m_layer_sizes = std::vector<long unsigned int>( m_n_layers + 1, 0 );

      auto jf = nlohmann::json::parse( *fin );
      for ( const auto& [key, value] : jf.items() ) {
        if ( key.find( "variables_constraints" ) != std::string::npos ) {
          for ( const auto& wl : value ) {
            m_clamp_los.push_back( wl[0] );
            m_clamp_his.push_back( wl[1] );
          }
        } else if ( key.find( "sigmanet.sigma" ) != std::string::npos ) {
          if ( value.at( 0 ) != m_lambda ) { throw exception( "Specified lambda does not match your model" ); }
        } else if ( key.find( "weight" ) != std::string::npos ) {
          for ( const auto& wl : value ) {
            std::copy( wl.begin(), wl.end(), std::back_inserter( m_weights ) );
            weights_counter += wl.size();
            weight_sizes[num_layer] += wl.size();
          }
          num_layer += 1;
          if ( num_layer + 1 > total_number_of_layers ) { total_number_of_layers += 1; }
        } else if ( key.find( "bias" ) != std::string::npos ) {
          std::copy( value.begin(), value.end(), std::back_inserter( m_biases ) );
          bias_counter += value.size();
          bias_sizes[num_layer] += value.size();
        } else if ( key.find( "gamma" ) != std::string::npos ) {
          continue;
        } else {
          throw exception( "Error when reading json file" );
        }
      }

      long unsigned int max_layer_size = *std::max_element( m_layer_sizes.begin(), m_layer_sizes.end() );
      if ( max_layer_size > m_size ) {
        throw exception( "Maximum layer size is larger than the maximum allowed size" );
      }

      std::transform( weight_sizes.begin(), weight_sizes.end(), bias_sizes.begin(), m_layer_sizes.begin(),
                      []( auto ws, auto bs ) { return ws / bs; } );
      m_layer_sizes.at( weight_sizes.size() ) = 1;

      if ( alg.msgLevel( MSG::VERBOSE ) ) {
        alg.verbose() << "Neural network parameters found:" << endmsg;
        alg.verbose() << "Lambda = " << m_lambda << endmsg;
        alg.verbose() << "NLayers = " << m_n_layers << endmsg;
        alg.verbose() << "LayerSizes = [";
        for ( int i = 0; i < m_n_layers + 1; i++ ) { alg.verbose() << m_layer_sizes.at( i ) << ","; }
        alg.verbose() << "]" << endmsg;
        alg.verbose() << "Monotone_Constraints = [";
        for ( unsigned int i = 0; i < m_input_size; i++ ) { alg.verbose() << m_monotone_constraints.at( i ) << ","; }
        alg.verbose() << "]" << endmsg;
      }

      unsigned int total_parameters = 0;
      for ( int layer_idx = 1; layer_idx < m_n_layers + 1; ++layer_idx ) {
        long unsigned int n_inputs  = m_layer_sizes[layer_idx - 1];
        long unsigned int n_outputs = m_layer_sizes[layer_idx];
        total_parameters += n_outputs * n_inputs;
        total_parameters += n_outputs;
      }

      if ( total_parameters != ( weights_counter + bias_counter ) ) {
        throw exception( "Mismatch between weight_file( " + std::to_string( weights_counter + bias_counter ) +
                         " Parameters ) and specified architecture ( " + std::to_string( total_parameters ) +
                         " Parameters ) !!!" );
      }
    }
  };

} // namespace Sel
