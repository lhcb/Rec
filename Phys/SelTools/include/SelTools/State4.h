/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "LHCbMath/MatVec.h"

namespace Sel {
  /** @class  StateVector4
   *  @brief  Class representing an {x, y, tx, ty} state vector at given z.
   *  @tparam float_v Type representing a [SIMD vector of] float values.
   *
   *  Similar to LHCb::StateVector, but missing q/p (not needed for vertexing)
   *  and templated to allow explicit vectorisation over multiple states.
   */
  template <typename float_v>
  struct StateVector4 {
  public:
    StateVector4() = default;

    StateVector4( float_v x, float_v y, float_v z, float_v tx, float_v ty ) : m_x{ x, y }, m_t{ tx, ty }, m_z{ z } {}

    template <typename state_t>
    StateVector4( state_t const& s ) : m_x{ s.x(), s.y() }, m_t{ s.tx(), s.ty() }, m_z{ s.z() } {}

    template <typename XYZPoint, typename XYZVector>
    StateVector4( XYZPoint const& point, XYZVector const& direction )
        : m_x{ point.X(), point.Y() }
        , m_t{ direction.X() / direction.Z(), direction.Y() / direction.Z() }
        , m_z{ point.Z() } {}
    auto  x() const { return m_x( 0 ); }
    auto  y() const { return m_x( 1 ); }
    auto  z() const { return m_z; }
    auto  tx() const { return m_t( 0 ); }
    auto  ty() const { return m_t( 1 ); }
    auto& slopes() const { return m_t; }
    auto& x() { return m_x( 0 ); }
    auto& y() { return m_x( 1 ); }
    auto& z() { return m_z; }
    auto& tx() { return m_t( 0 ); }
    auto& ty() { return m_t( 1 ); }

  private:
    LHCb::LinAlg::Vec<float_v, 2> m_x, m_t;
    float_v                       m_z;
  };

  template <typename float_v>
  struct StateVector5 : public StateVector4<float_v> {
    StateVector5() = default;

    StateVector5( float_v x, float_v y, float_v z, float_v tx, float_v ty, float_v qop )
        : StateVector4<float_v>( x, y, z, tx, ty ), m_qop{ qop } {}

    template <typename state_t>
    StateVector5( state_t const& s ) : StateVector4<float_v>( s ), m_qop{ s.qOverP() } {}

    auto  qOverP() const { return m_qop; }
    auto& qOverP() { return m_qop; }

  private:
    float_v m_qop;
  };

  /** @class  State4
   *  @brief  Class representing an {x, y, tx, ty} state vector and covariance.
   *  @tparam float_v Type representing a [SIMD vector of] float values.
   *
   *  Similar to LHCb::State, but missing q/p + status flags, and templated to
   *  allow explicit vectorisation over states.
   *
   *  Used for Composite children (as opposed to Resonance) during in the
   *  ParticleVertexFitter.
   */
  template <typename float_v>
  struct State4 : public StateVector4<float_v> {
    auto&       covTT() { return m_covTT; }
    const auto& covTT() const { return m_covTT; }
    auto&       covXT() { return m_covXT; }
    const auto& covXT() const { return m_covXT; }
    auto&       covXX() { return m_covXX; }
    const auto& covXX() const { return m_covXX; }
    /** Return the 4x4 covariance matrix.
     *  Order: {x, y, tx, ty}
     */
    [[nodiscard]] auto posSlopeCovariance() const {
      LHCb::LinAlg::MatSym<float_v, 4> out;
      out = out.template place_at<0, 0>( m_covXX );
      out = out.template place_at<0, 2>( m_covXT );
      return out.template place_at<2, 2>( m_covTT );
    }

    // Copied from LHCb::State
    void linearTransportTo( float_v const& z ) {
      auto const dz  = z - this->z();
      auto const dz2 = dz * dz;
      this->x() += dz * this->tx();
      this->y() += dz * this->ty();
      m_covXX( 0, 0 ) += dz2 * m_covTT( 0, 0 ) + 2 * dz * m_covXT( 0, 0 );
      m_covXX( 1, 1 ) += dz2 * m_covTT( 1, 1 ) + 2 * dz * m_covXT( 1, 1 );
      m_covXX( 1, 0 ) += dz2 * m_covTT( 1, 0 ) + dz * ( m_covXT( 0, 1 ) + m_covXT( 1, 0 ) );
      // would this make covXT always symmetric as well?
      m_covXT( 0, 0 ) += dz * m_covTT( 0, 0 );
      m_covXT( 0, 1 ) += dz * m_covTT( 0, 1 );
      m_covXT( 1, 0 ) += dz * m_covTT( 1, 0 );
      m_covXT( 1, 1 ) += dz * m_covTT( 1, 1 );
      // m_covXT      += dz*m_covTT ;
      this->z() = z;
    }

  private:
    LHCb::LinAlg::MatSym<float_v, 2> m_covXX;
    LHCb::LinAlg::MatSym<float_v, 2> m_covTT;
    LHCb::LinAlg::Mat<float_v, 2, 2> m_covXT;
  };

  template <typename float_v>
  struct State5 : public StateVector5<float_v> {
    State5() = default;

    template <typename state_t>
    State5( state_t const& s ) : StateVector5<float_v>( s ) {
      auto const& cov = s.covariance();
      LHCb::Utils::unwind<0, 5>(
          [&]( auto i ) { LHCb::Utils::unwind<i, 5>( [&]( auto j ) { m_cov( i, j ) = cov( i, j ); } ); } );
    }

    void linearTransportTo( float_v const& z ) {
      auto const dz  = z - this->z();
      auto const dz2 = dz * dz;
      this->x() += dz * this->tx();
      this->y() += dz * this->ty();
      m_cov( 0, 0 ) += dz2 * m_cov( 2, 2 ) + 2 * dz * m_cov( 2, 0 );
      m_cov( 1, 0 ) += dz2 * m_cov( 3, 2 ) + dz * ( m_cov( 3, 0 ) + m_cov( 2, 1 ) );
      m_cov( 2, 0 ) += dz * m_cov( 2, 2 );
      m_cov( 3, 0 ) += dz * m_cov( 3, 2 );
      m_cov( 4, 0 ) += dz * m_cov( 4, 2 );
      m_cov( 1, 1 ) += dz2 * m_cov( 3, 3 ) + 2 * dz * m_cov( 3, 1 );
      m_cov( 2, 1 ) += dz * m_cov( 3, 2 );
      m_cov( 3, 1 ) += dz * m_cov( 3, 3 );
      m_cov( 4, 1 ) += dz * m_cov( 4, 3 );
      this->z() = z;
    }

    LHCb::LinAlg::MatSym<float_v, 2> covXX() const {
      return m_cov.template sub<LHCb::LinAlg::MatSym<float_v, 2>, 0, 0>();
    }
    LHCb::LinAlg::Mat<float_v, 2> covXT() const { return m_cov.template sub<LHCb::LinAlg::Mat<float_v, 2>, 2, 0>(); }

    auto&       cov() { return m_cov; }
    auto const& cov() const { return m_cov; }

  private:
    LHCb::LinAlg::MatSym<float_v, 5> m_cov{};
  };
} // namespace Sel
