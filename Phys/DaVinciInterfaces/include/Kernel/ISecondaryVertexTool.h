/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "Event/Vertex.h"

#include "GaudiKernel/IAlgTool.h"

#include <vector>

#include "DetDesc/IGeometryInfo.h"
namespace LHCb {
  class RecVertex;
}

/**
 *  v1.2
 *  @author Marco Musy (Milano)
 *  @date   2004-12-14
 */
class GAUDI_API ISecondaryVertexTool : virtual public IAlgTool {

public:
  DeclareInterfaceID( ISecondaryVertexTool, 3, 0 );

public:
  virtual std::vector<LHCb::Vertex> buildVertex( LHCb::RecVertex const&, LHCb::Particle::ConstVector const&,
                                                 IGeometryInfo const& ) = 0;
};
