/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/IGeometryInfo.h"
#include "Event/Particle.h"
#include "GaudiKernel/IAlgTool.h"
#include <string>

/**
 * Interface for bremsstrahlung correction to electron particle
 */
struct GAUDI_API ISelectiveBremAdder : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ISelectiveBremAdder, 4, 0 );

  // Add Brem
  virtual bool addBrem( LHCb::Particle& particle, bool force = false ) const = 0;
  // Add Brem on particle pair (removing overlap)
  virtual bool addBrem2Pair( LHCb::Particle& p1, LHCb::Particle& p2, bool force = false ) const = 0;
};
