/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <Gaudi/Accumulators.h>

namespace {
  bool isSameB( const LHCb::Particle* bCand, const LHCb::Particle* taggedB ) {
    return ( bCand->momentum() == taggedB->momentum() && bCand->referencePoint() == taggedB->referencePoint() );
  }
} // namespace

class FlavourTagsMerger : public LHCb::Algorithm::MergingTransformer<LHCb::FlavourTags(
                              const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& )> {
public:
  /// Standard constructor
  FlavourTagsMerger( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator, { "FlavourTagsList", { "" } }, { "OutputFlavourTags", "" } ) {}

  LHCb::FlavourTags operator()(
      const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& flavourTagsIterable ) const override;

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{ this, "#BCandidates" };
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{ this, "#FlavourTags" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FlavourTagsMerger )

LHCb::FlavourTags FlavourTagsMerger::operator()(
    const Gaudi::Functional::details::vector_of_const_<LHCb::FlavourTags>& flavourTagsIterable ) const {
  m_FTCount += flavourTagsIterable.size();

  LHCb::FlavourTags flavourTags;

  std::vector<const LHCb::Particle*> uniqueB;

  for ( const auto& inFlavourTags : flavourTagsIterable ) {
    for ( const auto* flavourTag : inFlavourTags ) {
      const auto* taggedB = flavourTag->taggedB();

      if ( !any_of( uniqueB.begin(), uniqueB.end(),
                    [taggedB]( const LHCb::Particle* bCand ) { return isSameB( bCand, taggedB ); } ) ) {
        uniqueB.push_back( taggedB );
      }
    }
  }

  m_BCount += uniqueB.size();

  for ( const auto* bCand : uniqueB ) {
    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );

    for ( const auto& inFlavourTags : flavourTagsIterable ) {
      for ( const auto* inFlavourTag : inFlavourTags ) {
        const auto* taggedB = inFlavourTag->taggedB();
        if ( isSameB( bCand, taggedB ) ) {
          for ( const auto& tagger : inFlavourTag->taggers() ) { flavourTag->addTagger( tagger ); }
        }
      }
    }

    flavourTags.insert( flavourTag.release() );
  }

  return flavourTags;
}
