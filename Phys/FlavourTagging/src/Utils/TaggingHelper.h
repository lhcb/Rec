/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Core/FloatComparison.h"
#include "Event/Particle.h"
#include "TrackKernel/PrimaryVertexUtils.h"
#include <cmath>

namespace TaggingHelper {

  enum struct CloneStatus {
    DifferentParticles = 0, //< different particles
    ConvertedGamma,         //< looks like converted gamma
    MattTxTyQp,             //< same slope in Velo (criteria due to M. Needham)
    MattTxTy,               //< same slope in Velo (criteria due to M. Needham)
    CloneTrackParams,       //< same particle based on match in p,eta,phi
    CloneHitContent         //< same particle based on track hit content
  };

  namespace details {
    inline double dPhi( const double x1, const double y1, const double x2, const double y2 ) {
      // if either of the vectors has zero length, return NaN (slightly different from what std::atan2 does already....)
      if ( LHCb::essentiallyZero( x1 * x1 + y1 * y1 ) || LHCb::essentiallyZero( x2 * x2 + y2 * y2 ) )
        return std::numeric_limits<double>::quiet_NaN();
      return std::atan2( x1 * y2 - x2 * y1, x1 * x2 + y1 * y2 );
    }
  } // namespace details

  inline double dPhi( const Gaudi::LorentzVector& lhs, const Gaudi::LorentzVector& rhs ) {
    return details::dPhi( lhs.x(), lhs.y(), rhs.x(), rhs.y() );
  }
  inline double dPhi( const Gaudi::LorentzVector& lhs, const Gaudi::XYZVector& rhs ) {
    return details::dPhi( lhs.x(), lhs.y(), rhs.x(), rhs.y() );
  }

  // This works for RecVertex etc: it requires either to match pointer or key
  template <typename PVContainer>
  auto pileUpVertices( const PVContainer& allPVs, const LHCb::VertexBase* bestPV ) {
    boost::container::small_vector<const LHCb::VertexBase*, 16> puVts;
    puVts.reserve( allPVs.size() - 1 );
    for ( const auto& pv : allPVs )
      if ( !( pv == bestPV || ( bestPV && pv->key() == bestPV->key() ) ) ) puVts.emplace_back( pv );
    return puVts;
  }

  template <typename PVContainer>
  auto pileUpVertices( const PVContainer& allPVs, const LHCb::Particle& bCand ) {
    return pileUpVertices( allPVs, LHCb::bestPV( allPVs, bCand ) );
  }

  // Specialization for extended PV container
  inline auto pileUpVertices( const LHCb::Event::PV::PrimaryVertexContainer& allPVs, int bestPVIndex ) {
    boost::container::small_vector<const LHCb::VertexBase*, 16> puVts;
    puVts.reserve( allPVs.size() - 1 );
    for ( auto const& pv : allPVs ) {
      if ( pv.index() != bestPVIndex ) puVts.push_back( &pv );
    }
    return puVts;
  }

  inline auto pileUpVertices( const LHCb::Event::PV::PrimaryVertexContainer& allPVs, const LHCb::VertexBase* bestPV ) {
    return bestPV ? pileUpVertices( allPVs, bestPV->index() )
                  : boost::container::small_vector<const LHCb::VertexBase*, 16>{};
  }

  inline auto pileUpVertices( const LHCb::Event::PV::PrimaryVertexContainer& allPVs, const LHCb::Particle& bCand ) {
    return pileUpVertices( allPVs, LHCb::bestPVIndex( allPVs, bCand ) );
  }

  inline bool areHitFractionClones( const LHCb::Particle& p1, const LHCb::Particle& p2,
                                    const double maxSharedHitFraction ) {
    if ( !p1.proto() || !p2.proto() ) return false;
    const LHCb::Track* t1 = p1.proto()->track();
    const LHCb::Track* t2 = p2.proto()->track();
    if ( !t1 || !t2 ) return false;
    return t1->nCommonLhcbIDs( *t2 ) > maxSharedHitFraction * std::min( t1->nLHCbIDs(), t2->nLHCbIDs() );
  }

  inline bool areMomentumParameterClones( const LHCb::Particle& p1, const LHCb::Particle& p2 ) {
    const auto& mom1 = p1.momentum();
    const auto& mom2 = p2.momentum();

    const double deta = mom1.eta() - mom2.eta();
    const double dphi = dPhi( mom1, mom2 );

    const Gaudi::SymMatrix4x4& C1   = p1.momCovMatrix();
    const Gaudi::SymMatrix4x4& C2   = p2.momCovMatrix();
    double                     err1 = std::sqrt( ( mom1.x() * mom1.x() * C1( 0, 0 ) + mom1.y() * mom1.y() * C1( 1, 1 ) +
                               mom1.z() * mom1.z() * C1( 2, 2 ) ) +
                                                 2. * ( mom1.x() * mom1.y() * C1( 0, 1 ) + mom1.x() * mom1.z() * C1( 0, 2 ) +
                                    mom1.y() * mom1.z() * C1( 1, 2 ) ) ) /
                  mom1.P();
    double err2 = std::sqrt( ( mom2.x() * mom2.x() * C2( 0, 0 ) + mom2.y() * mom2.y() * C2( 1, 1 ) +
                               mom2.z() * mom2.z() * C2( 2, 2 ) ) +
                             2. * ( mom2.x() * mom2.y() * C2( 0, 1 ) + mom2.x() * mom2.z() * C2( 0, 2 ) +
                                    mom2.y() * mom2.z() * C2( 1, 2 ) ) ) /
                  mom2.P();

    // check if the particles are clones based on their momenta and angles
    if ( p1.charge() && p2.charge() ) {
      const double dqp = double( p1.charge() ) / mom1.P() - double( p2.charge() ) / mom2.P();
      const double dqperr =
          std::sqrt( ( err1 / mom1.P2() ) * ( err1 / mom1.P2() ) + ( err2 / mom2.P2() ) * ( err2 / mom2.P2() ) );
      if ( std::abs( dqp / dqperr ) < 3. && std::abs( deta ) < 0.1 && std::abs( dphi ) < 0.1 ) return true;
    } else {
      const double dp    = mom1.P() - mom2.P();
      const double dperr = std::sqrt( err1 * err1 + err2 * err2 );
      if ( std::abs( dp / dperr ) < 3. && std::abs( deta ) < 0.1 && std::abs( dphi ) < 0.1 ) return true;
    }
    return false;
  }

  inline bool areSlopeClones( const LHCb::Particle& p1, const LHCb::Particle& p2 ) {
    const auto& mom1 = p1.momentum();
    const auto& mom2 = p2.momentum();

    const double dtx = mom1.x() / mom1.z() - mom2.x() / mom2.z();
    const double dty = mom1.y() / mom1.z() - mom2.y() / mom2.z();

    return std::abs( dtx ) < 0.0004 && std::abs( dty ) < 0.0002;
  }

  inline bool areSlopeChargeClones( const LHCb::Particle& p1, const LHCb::Particle& p2 ) {
    const auto& mom1 = p1.momentum();
    const auto& mom2 = p2.momentum();

    const double dtx = mom1.x() / mom1.z() - mom2.x() / mom2.z();
    const double dty = mom1.y() / mom1.z() - mom2.y() / mom2.z();

    if ( p1.charge() && p2.charge() ) {
      const double dqp = double( p1.charge() ) / mom1.P() - double( p2.charge() ) / mom2.P();

      if ( std::abs( dtx ) < 0.005 && std::abs( dty ) < 0.005 && std::abs( dqp ) < 1e-6 ) return true;
    }
    return false;
  }

  inline bool areConvertedGamma( const LHCb::Particle& p1, const LHCb::Particle& p2 ) {
    if ( p1.charge() * p2.charge() < 0 ) {
      const double         mmu = 105.6583715, me = 0.510998928;
      Gaudi::LorentzVector mom1 = p1.momentum();
      Gaudi::LorentzVector mom2 = p2.momentum();
      mom1.SetE( std::sqrt( me * me + mom1.P2() ) );
      mom2.SetE( std::sqrt( me * me + mom2.P2() ) );
      // use 0.25 * mmu as cutoff for low mass - halfway between 0
      // and half a muon mass seems fair when you keep mass resolution
      // and numerical precision in mind...
      const bool isLowMassSq = ( std::abs( ( mom1 + mom2 ).M2() ) < .0625 * mmu * mmu );
      mom1.SetE( std::sqrt( mmu * mmu + mom1.P2() ) );
      mom2.SetE( std::sqrt( mmu * mmu + mom2.P2() ) );
      if ( isLowMassSq || ( std::abs( ( mom1 + mom2 ).M2() ) < .0625 * mmu * mmu ) ) {
        // the system has a small invariant mass - typically, the two
        // tracks will also have a small angle (after all, it's a
        // gamma on a fixed target... ;), so cut on the cosine of that
        // angle
        const double p1p2 = mom1.x() * mom2.x() + mom1.y() * mom2.y() + mom1.z() * mom2.z();
        if ( p1p2 > 0.999999 * mom1.P() * mom2.P() ) return true;
      }
    }
    return false;
  }

  inline CloneStatus cloneCategory( const LHCb::Particle& p1, const LHCb::Particle& p2, double maxSharedHitFraction ) {
    if ( areHitFractionClones( p1, p2, maxSharedHitFraction ) ) return CloneStatus::CloneHitContent;
    if ( areMomentumParameterClones( p1, p2 ) ) return CloneStatus::CloneTrackParams;
    if ( areSlopeClones( p1, p2 ) ) return CloneStatus::MattTxTy;
    if ( areSlopeChargeClones( p1, p2 ) ) return CloneStatus::MattTxTyQp;
    if ( areConvertedGamma( p1, p2 ) ) return CloneStatus::ConvertedGamma;
    return CloneStatus::DifferentParticles;
  }

} // namespace TaggingHelper
