/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "../Classification/SSKaon/SSKaon_BDTeta_dev.h"
#include "../Classification/SSKaon/SSKaon_BDTseltracks_dev.h"
#include "DetDesc/DetectorElement.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/Tagger.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class Run2SSKaonTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                                             const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  Run2SSKaonTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "BCandidates", "" }, KeyValue{ "TaggingKaons", "" }, KeyValue{ "PrimaryVertices", "" },
                       KeyValue{ "StandardGeometry", LHCb::standard_geometry_top } },
                     KeyValue{ "OutputFlavourTags", "" } ) {}

  LHCb::FlavourTags operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingKaons,
                                const LHCb::PrimaryVertices& primaryVertices, const DetectorElement& ) const override;

private:
  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingKaons,
                                              const LHCb::PrimaryVertices& primaryVertices,
                                              const IGeometryInfo&         geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::Run2_SSKaon; }

  // cut values for sskaon selection
  Gaudi::Property<double> m_maxIpSigTagBestPV{ this, "MaxIpSigTagBestPV", 14.0,
                                               "Tagging particle requirement: Maximum IP significance wrt to best PV" };

  Gaudi::Property<double> m_minTagSelBDTValue{
      this, "MinTagSelBDTValue", 0.72,
      "Tagging particle requirement: Minimum BDT output of the tagging-candidate selector BDT " };

  // values for calibration purposes
  Gaudi::Property<double> m_averageEta{ this, "AverageEta", 0.3949 };
  Gaudi::Property<double> m_minPosDecision{ this, "MinPositiveTaggingDecision", 0.5,
                                            "Minimum value of the Eta for a tagging decision of +1" };
  Gaudi::Property<double> m_maxNegDecision{ this, "MaxNegativeTaggingDecision", 0.5,
                                            "Maximum value of the Eta for a tagging decision of -1" };

  mutable Gaudi::Accumulators::SummingCounter<>  m_BCount{ this, "#BCandidates" };
  mutable Gaudi::Accumulators::SummingCounter<>  m_kaonCount{ this, "#taggingKaons" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_FTCount{ this, "#goodFlavourTags" };

  ToolHandle<TaggingHelperTool> m_taggingHelperTool{ this, "TaggingHelper", "TaggingHelperTool" };
  // ToolHandle<IVertexFit>              m_vertexFitTool{this, "VertexFitter", "LoKi::VertexFitter"};

  std::vector<std::string> m_tagSelBdtVariables = {
      "log(Tr_P)",          "log(Tr_PT)",          "log(B_PT)",     "log(abs(deltaPhi))",
      "log(abs(deltaEta))", "log(pTrel)",          "log(Tr_BPVIP)", "log(sqrt(Tr_BPVIPCHI2))",
      "log(Tr_TRCHI2DOF)",  "log(nTracks_presel)", "nPV",           "Tr_PROBNNk",
      "Tr_PROBNNpi",        "Tr_PROBNNp" };
  BDTseltracksReaderCompileWrapper m_tagSelBDTReader = BDTseltracksReaderCompileWrapper( m_tagSelBdtVariables );

  std::vector<std::string>   m_BdtEtaVariables = { "log(B_PT)",
                                                   "nTracks_presel_BDTGcut",
                                                   "nPV",
                                                   "(Tr_Charge*Tr_BDTG_selBestTracks_14vars)",
                                                   "(Tr_Charge_2nd*Tr_BDTG_selBestTracks_14vars_2nd)",
                                                   "(Tr_Charge_3rd*Tr_BDTG_selBestTracks_14vars_3rd)" };
  BDTetaReaderCompileWrapper m_etaBdtReader    = BDTetaReaderCompileWrapper( m_BdtEtaVariables );

  struct TagCandidate {
    const LHCb::Particle* particle;
    double                refittedPVIP;
    double                ipSig;
    double                selBDTVal;

    static bool sortTagCandidates( const TagCandidate& t1, const TagCandidate& t2 ) {
      return t1.selBDTVal > t2.selBDTVal;
    }
  };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Run2SSKaonTagger )

LHCb::FlavourTags Run2SSKaonTagger::operator()( const LHCb::Particle::Range& bCandidates,
                                                const LHCb::Particle::Range& taggingKaons,
                                                const LHCb::PrimaryVertices& primaryVertices,
                                                const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();
  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_kaonCount += taggingKaons.size();
  // always() << bCandidates.size() << "  " << taggingKaons.size() << "  " << primaryVertices.size() << endmsg;

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingKaons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      auto flavourTag = std::make_unique<LHCb::FlavourTag>();
      flavourTag->addTagger( LHCb::Tagger{}.setType( taggerType() ) ).setTaggedB( bCand );
      flavourTags.insert( flavourTag.release() );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagResult = performTagging( *bCand, taggingKaons, primaryVertices, geometry );
    m_FTCount += tagResult.has_value();

    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );
    // if no appropriate tagging candidate found, fill with "empty" FlavourTag object
    flavourTag->addTagger( tagResult ? *tagResult : LHCb::Tagger{}.setType( taggerType() ) );
    flavourTags.insert( flavourTag.release() );
  }

  return flavourTags;
}

std::optional<LHCb::Tagger> Run2SSKaonTagger::performTagging( const LHCb::Particle&        bCand,
                                                              const LHCb::Particle::Range& taggingKaons,
                                                              const LHCb::PrimaryVertices& primaryVertices,
                                                              const IGeometryInfo&         geometry ) const {
  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const auto* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( !bestPV ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand, geometry);
  if(! refittedPV)
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, bestPV );

  std::vector<TagCandidate> preSelTagCandidates;
  for ( const auto* tagCand : taggingKaons ) {

    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // IP significance cut wrt bestPV
    auto refittedPVIP = m_taggingHelperTool->calcIPWithErr( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP ) continue;
    const double ipSig = refittedPVIP->first / refittedPVIP->second;
    if ( ipSig > m_maxIpSigTagBestPV ) continue;

    preSelTagCandidates.push_back( TagCandidate( { tagCand, refittedPVIP->first, ipSig, -999. } ) );
  }

  // always() << preSelTagCandidates.size() << " kaons survived presel" << endmsg;

  std::vector<TagCandidate> selTagCandidates;
  for ( auto& tagCand : preSelTagCandidates ) {
    Gaudi::LorentzVector tag_mom = tagCand.particle->momentum();
    // inputs for the BDT
    double deltaPhi       = TaggingHelper::dPhi( b_mom, tag_mom ); // ??? check if bigger than pi
    deltaPhi              = fabs( deltaPhi );
    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );

    const double btag_rel =
        ( b_mom.Px() * tag_mom.Px() + b_mom.Py() * tag_mom.Py() + b_mom.Pz() * tag_mom.Pz() ) / b_mom.P();
    const double btag_relPt = sqrt( tag_mom.P() * tag_mom.P() - btag_rel * btag_rel );

    const LHCb::ProtoParticle* tagProto     = tagCand.particle->proto();
    auto const                 gpid         = tagProto->globalChargedPID();
    const double               tagPIDk      = gpid ? gpid->ProbNNk() : -1000.0;
    const double               tagPIDpi     = gpid ? gpid->ProbNNpi() : -1000.0;
    const double               tagPIDp      = gpid ? gpid->ProbNNp() : -1000.0;
    const LHCb::Track*         tagTrack     = tagProto->track();
    const double               tagTrackChi2 = tagTrack->chi2PerDoF();

    auto tagCand_selBDTVal =
        m_tagSelBDTReader.GetMvaValue( { log( tag_mom.P() ), log( tag_mom.Pt() ), log( b_mom.Pt() ), log( deltaPhi ),
                                         log( deltaEta ), log( btag_relPt ), log( tagCand.refittedPVIP ),
                                         log( tagCand.ipSig ), log( tagTrackChi2 ), log( preSelTagCandidates.size() ),
                                         static_cast<double>( primaryVertices.size() ), tagPIDk, tagPIDpi, tagPIDp } );

    if ( tagCand_selBDTVal < m_minTagSelBDTValue ) continue;

    tagCand.selBDTVal = tagCand_selBDTVal;

    selTagCandidates.push_back( tagCand );
  }

  // always() << selTagCandidates.size() << " kaons survive BDT selection" << endmsg;
  if ( selTagCandidates.empty() ) return std::nullopt;

  std::stable_sort( selTagCandidates.begin(), selTagCandidates.end(), TagCandidate::sortTagCandidates );

  int goodTagCandidates = selTagCandidates.size();

  while ( selTagCandidates.size() < 3 ) {
    selTagCandidates.push_back( TagCandidate( { new LHCb::Particle(), 0, 0, 0 } ) );
  }

  double etaBdtVal = m_etaBdtReader.GetMvaValue(
      { log( b_mom.Pt() ), static_cast<double>( goodTagCandidates ), static_cast<double>( primaryVertices.size() ),
        selTagCandidates[0].particle->charge() * selTagCandidates[0].selBDTVal,
        selTagCandidates[1].particle->charge() * selTagCandidates[1].selBDTVal,
        selTagCandidates[2].particle->charge() * selTagCandidates[2].selBDTVal } );
  double calibEtaBdtVal = ( etaBdtVal / 0.71 + 1. ) * 0.5;

  double etaCCBdtVal = m_etaBdtReader.GetMvaValue(
      { log( b_mom.Pt() ), static_cast<double>( goodTagCandidates ), static_cast<double>( primaryVertices.size() ),
        -1. * selTagCandidates[0].particle->charge() * selTagCandidates[0].selBDTVal,
        -1. * selTagCandidates[1].particle->charge() * selTagCandidates[1].selBDTVal,
        -1. * selTagCandidates[2].particle->charge() * selTagCandidates[2].selBDTVal } );
  double calibEtaCCBdtVal = ( etaCCBdtVal / 0.71 + 1. ) * 0.5;
  double eta              = ( calibEtaBdtVal + ( 1. - calibEtaCCBdtVal ) ) * 0.5;

  auto tagdecision = ( eta >= m_minPosDecision  ? LHCb::Tagger::TagResult::bbar
                       : eta < m_maxNegDecision ? LHCb::Tagger::TagResult::b
                                                : LHCb::Tagger::TagResult::none );

  if ( eta >= 0.5 ) eta = 1. - eta;

  constexpr auto pol      = std::array{ 0.025841, -0.21766 };
  double         calibEta = ( pol[0] + m_averageEta ) + ( 1 + pol[1] ) * ( eta - m_averageEta );

  auto tagger =
      LHCb::Tagger{}.setDecision( tagdecision ).setOmega( calibEta ).setType( taggerType() ).setMvaValue( calibEta );

  for ( int i = 0; i < 1; i++ ) tagger.addToTaggerParts( selTagCandidates[i].particle );
  // TODO: Fix persistency to not segfault when adding more than 1 tagging particle
  // for ( int i = 0; i < 3; i++ ) tagger.addToTaggerParts( selTagCandidates[i].particle );

  return tagger;
}
