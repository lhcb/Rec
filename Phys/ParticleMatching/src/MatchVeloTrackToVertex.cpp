/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "Event/Particle.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Kernel/IParticle2State.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackKernel/PrimaryVertexUtils.h"

namespace {
  using output_t        = std::tuple<LHCb::Particles>;
  using filter_output_t = std::tuple<bool, LHCb::Particles>;
  LHCb::State compositeParticleToState( const LHCb::Particle& particle ) {
    // See also ParticleVertexFitter, which has the vectorised form of this
    LHCb::State  state;
    const auto&  p4 = particle.momentum();
    const auto&  px = p4.Px();
    const auto&  py = p4.Py();
    const auto&  pz = p4.Pz();
    const double tx = px / pz;
    const double ty = py / pz;

    double p = p4.P();
    state.setTx( tx );
    state.setTy( ty );
    const int Q = particle.charge();
    state.setQOverP( Q / p );
    const Gaudi::XYZPoint& pos = particle.endVertex()->position();
    state.setX( pos.x() );
    state.setY( pos.y() );
    state.setZ( pos.z() );

    ROOT::Math::SMatrix<double, 2, 3> Jxpos;
    Jxpos( 0, 0 ) = Jxpos( 1, 1 ) = 1;
    Jxpos( 0, 2 )                 = -tx;
    Jxpos( 1, 2 )                 = -ty;
    ROOT::Math::SMatrix<double, 3, 3> Jtxmom;
    Jtxmom( 0, 0 )      = 1 / pz;   // dtx/dpx
    Jtxmom( 1, 1 )      = 1 / pz;   // dty/dpy
    Jtxmom( 0, 2 )      = -tx / pz; // dtx/dpz
    Jtxmom( 1, 2 )      = -ty / pz; // dty/dpz
    const double dqopdp = -Q / ( p * p );
    Jtxmom( 2, 0 )      = dqopdp * px / p; // dqop/dpx
    Jtxmom( 2, 1 )      = dqopdp * py / p; // dqop/dpy
    Jtxmom( 2, 2 )      = dqopdp * pz / p; // dqop/dpz

    const auto&            momposcov = particle.posMomCovMatrix();
    const Gaudi::Matrix2x3 xtxcov =
        Jxpos * ROOT::Math::Transpose( momposcov.Sub<Gaudi::Matrix3x3>( 0, 0 ) ) * ROOT::Math::Transpose( Jtxmom );
    const Gaudi::SymMatrix2x2 xcov = ROOT::Math::Similarity( Jxpos, particle.posCovMatrix() );
    const Gaudi::SymMatrix3x3 txcov =
        ROOT::Math::Similarity( Jtxmom, particle.momCovMatrix().Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
    auto& cov = state.covariance();
    for ( int i = 0; i < 2; ++i )
      for ( int j = 0; j <= i; ++j ) cov( i, j ) = xcov( i, j );
    for ( int i = 0; i < 2; ++i )
      for ( int j = 0; j < 3; ++j ) cov( i, j + 2 ) = xtxcov( i, j );
    for ( int i = 0; i < 3; ++i )
      for ( int j = 0; j <= i; ++j ) cov( i + 2, j + 2 ) = txcov( i, j );
    return state;
  }
} // namespace

// Algorithm that matches a particle's vertex to a Velo track.
// See also https://inspirehep.net/literature/2768765
class MatchVeloTrackToVertex
    : public LHCb::Algorithm::MultiTransformerFilter<output_t( const LHCb::Particle::Range&,
                                                               const LHCb::Event::v1::Tracks&,
                                                               const LHCb::PrimaryVertices&, const DetectorElement& ),
                                                     LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  MatchVeloTrackToVertex( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformerFilter{ name,
                                pSvcLocator,
                                { KeyValue{ "SeedParticles", "" }, KeyValue{ "VeloTracks", "Rec/Track/Velo" },
                                  KeyValue{ "PrimaryVertices", "" },
                                  KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } },
                                { KeyValue{ "OutputParticles", "" } } } {}

  filter_output_t operator()( const LHCb::Particle::Range&, const LHCb::Event::v1::Tracks&,
                              const LHCb::PrimaryVertices&, const DetectorElement& ) const override;

private:
  Gaudi::Property<float> m_match_ip_cut{ this, "VeloMatchIP", 5.f * Gaudi::Units::mm,
                                         "Loose match IP cut (used before and after state extrapolation)" };
  Gaudi::Property<float> m_match_chi2_cut{
      this, "VeloMatchChi2", 64., "Match chi^2 cut as described in https://inspirehep.net/literature/2768765" };
  Gaudi::Property<float> m_eta_tolerance{ this, "EtaTolerance", 5.f, "max Delta Eta between VELO track and seed" };
  Gaudi::Property<float> m_offline{ this, "OfflineAlg", false, "Persist cloned object with Velo track as proto?" };

  mutable Gaudi::Accumulators::StatCounter<>     m_nVeloIn{ this, "Input VELO tracks" };
  mutable Gaudi::Accumulators::StatCounter<>     m_nCombIn{ this, "Input vertices" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_matched{ this, "Matched" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_end_velo_state_warning{
      this, "Provided VELO track does not have an end velo state" };
  ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "TrackExtrapolator", "TrackMasterExtrapolator" };
  ToolHandle<ITrackFitter>       m_velo_track_refitter{ this, "TrackFitter", "TrackMasterFitter/Fitter" };
  ToolHandle<IParticle2State>    m_particle_to_state{ this, "ParticleToState", "Particle2State" };
};
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MatchVeloTrackToVertex )

filter_output_t MatchVeloTrackToVertex::operator()( const LHCb::Particle::Range&   seeds,
                                                    const LHCb::Event::v1::Tracks& velo_tracks,
                                                    const LHCb::PrimaryVertices&   pvs,
                                                    const DetectorElement&         lhcb ) const {
  LHCb::Particles filtered_seeds;
  m_nVeloIn += velo_tracks.size();
  m_nCombIn += seeds.size();

  for ( const auto seed : seeds ) {
    bool                    matched = false;
    LHCb::Event::v1::Track* best_velo_track{ nullptr };
    double                  best_chi2 = 9e9, best_ip = -1.;
    LHCb::State             seed_as_state = compositeParticleToState( *seed );
    debug() << "Seed state at endvtx" << endmsg;
    if ( msgLevel( MSG::DEBUG ) ) seed_as_state.fillStream( std::cout );
    // loop velo tracks
    for ( const auto& velo_track : velo_tracks ) {
      // skip backward tracks
      if ( velo_track->isVeloBackward() ) continue;
      // check the rough IP of the VELO w.r.t. the seed's decay vertex
      const Gaudi::XYZPoint& seed_vertex_pos    = seed->endVertex()->position();
      const auto             closest_velo_state = velo_track->closestState( seed_vertex_pos.z() );
      const auto             ip                 = Gaudi::Math::impactParameter(
          seed_vertex_pos, Gaudi::Math::Line{ closest_velo_state.position(), closest_velo_state.slopes() } );
      if ( ip > m_match_ip_cut || abs( closest_velo_state.slopes().eta() - seed->momentum().eta() ) > m_eta_tolerance )
        continue;

      // use Xi properties for particle transport by default
      auto track_pid = LHCb::Tr::PID( 3312 );
      try {
        track_pid = LHCb::Tr::PID( seed->particleID().abspid() );
      } catch ( const std::runtime_error& e ) {
        warning()
            << "PID " << seed->particleID().abspid()
            << " is not known in TrackDefaultParticles. Using Xi properties instead. This can lead to wrong results!"
            << endmsg;
      }

      // transport seed to velo state position to get a proper momentum estimate
      if ( m_extrapolator->propagate( seed_as_state, closest_velo_state.position().z(), *lhcb.geometry(), track_pid ) ==
           StatusCode::FAILURE ) {
        seed_as_state = compositeParticleToState( *seed );
        continue;
      }
      // fit velo track with momentum estimate and PID from seed state to get estimate of the covariance matrix
      for ( auto state : velo_track->states() ) {
        state->setQOverP( seed_as_state.qOverP() );
        state->setErrQOverP2( seed_as_state.errQOverP2() );
      }
      if ( msgLevel( MSG::DEBUG ) ) velo_track->fillStream( std::cout );
      if ( m_velo_track_refitter->operator()( *velo_track, *lhcb.geometry(), track_pid ) == StatusCode::FAILURE ) {
        seed_as_state = compositeParticleToState( *seed );
        continue;
      }
      // Go to LastMeasurement state of the Velo track (best constrained and likely closest to the seed decay vertex) to
      // do the IP and Match chi2 calculations
      debug() << "Seed state at end velo" << endmsg;
      if ( msgLevel( MSG::DEBUG ) ) seed_as_state.fillStream( std::cout );
      const auto velo_lastm_fitted = velo_track->stateAt( LHCb::State::Location::LastMeasurement );
      debug() << "Velo state after fitting (LastMeasurement)" << endmsg;
      if ( msgLevel( MSG::DEBUG ) ) velo_lastm_fitted->fillStream( std::cout );
      if ( m_extrapolator->propagate( seed_as_state, velo_lastm_fitted->z(), *lhcb.geometry(), track_pid ) ==
           StatusCode::FAILURE ) {
        seed_as_state = compositeParticleToState( *seed );
        continue;
      }
      // get a chi^2 for deciding which velo track fits best
      const auto       cov_seed = seed_as_state.covariance();
      const auto       cov_velo = velo_lastm_fitted->covariance();
      Gaudi::Matrix5x5 tmp_cov  = cov_seed + cov_velo;
      Gaudi::Matrix4x4 cov      = tmp_cov.Sub<Gaudi::Matrix4x4>( 0, 0 );
      bool             isc      = cov.InvertFast();
      if ( !isc ) {
        seed_as_state = compositeParticleToState( *seed );
        continue;
      }
      Gaudi::Vector4 dv{ seed_as_state.x() - velo_lastm_fitted->x(), seed_as_state.y() - velo_lastm_fitted->y(),
                         seed_as_state.tx() - velo_lastm_fitted->tx(), seed_as_state.ty() - velo_lastm_fitted->ty() };
      const auto     match_chi2 = ROOT::Math::Similarity( dv, cov );
      // choose the best match and filter on IP and Match chi^2
      if ( best_ip = Gaudi::Math::impactParameter(
               seed_as_state.position(),
               Gaudi::Math::Line{ velo_lastm_fitted->position(), velo_lastm_fitted->slopes() } );
           match_chi2 < best_chi2 && match_chi2 > 0. && match_chi2 < m_match_chi2_cut && best_ip < m_match_ip_cut ) {
        best_chi2       = match_chi2;
        best_velo_track = velo_track;
        for ( auto state : best_velo_track->states() ) {
          state->setQOverP( seed_as_state.qOverP() );
          state->setErrQOverP2( seed_as_state.errQOverP2() );
        }
      }
      seed_as_state = compositeParticleToState( *seed );
    }
    if ( best_velo_track ) {
      if ( m_offline ) {
        // make copy of seed and start changing its properties
        auto seed_to_copy = seed->clone();
        // update covariance matrices, position, momentum from CTB state of the best velo track
        if ( m_particle_to_state->state2Particle( best_velo_track->closestToBeamState(), *seed_to_copy ) ==
             StatusCode::FAILURE )
          warning() << "Could not set properties of new Particle." << endmsg;
        auto* tmp_proto = new LHCb::ProtoParticle();
        tmp_proto->setTrack( best_velo_track );
        seed_to_copy->setProto( tmp_proto );
        const auto& pv = LHCb::bestPV( pvs, seed_to_copy->referencePoint(), seed_to_copy->momentum() );
        if ( pv ) seed_to_copy->setPV( pv );
        seed_to_copy->addInfo( LHCb::Particle::additionalInfo::VeloMatchIP, best_ip );
        seed_to_copy->addInfo( LHCb::Particle::additionalInfo::VeloMatchChi2, best_chi2 );
        filtered_seeds.insert( seed_to_copy );
      } else
        filtered_seeds.insert( seed->clone() );
      matched = true;
    }
    m_matched += matched;
  }
  return { filtered_seeds.size() > 0, std::move( filtered_seeds ) };
}
