/***************************************************************************** \
 * (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/RelationWeighted1D.h"

namespace {
  unsigned int get_overlap_in_lhcb_ids( const std::vector<LHCb::LHCbID>& vec_ids_a,
                                        const std::vector<LHCb::LHCbID>& vec_ids_b ) {
    return std::count_if( vec_ids_a.begin(), vec_ids_a.end(), [&]( const auto& lhcbID ) {
      return ( std::find( vec_ids_b.begin(), vec_ids_b.end(), lhcbID ) != vec_ids_b.end() );
    } );
  }

  std::vector<LHCb::LHCbID> get_velo_ids( const LHCb::Track& track ) {
    std::vector<LHCb::LHCbID> outputVector;
    const auto&               lhcbIdsOnTrack = track.lhcbIDs();

    std::copy_if( lhcbIdsOnTrack.begin(), lhcbIdsOnTrack.end(), std::back_inserter( outputVector ),
                  []( const auto& id ) { return id.isVP(); } );

    return outputVector;
  }

  std::vector<LHCb::LHCbID> get_ft_ids( const LHCb::Track& track ) {
    std::vector<LHCb::LHCbID> outputVector;
    const auto&               lhcbIdsOnTrack = track.lhcbIDs();

    std::copy_if( lhcbIdsOnTrack.begin(), lhcbIdsOnTrack.end(), std::back_inserter( outputVector ),
                  []( const auto& id ) { return id.isFT(); } );

    return outputVector;
  }

  std::vector<LHCb::LHCbID> get_ut_ids( const LHCb::Track& track ) {
    std::vector<LHCb::LHCbID> outputVector;
    const auto&               lhcbIdsOnTrack = track.lhcbIDs();

    std::copy_if( lhcbIdsOnTrack.begin(), lhcbIdsOnTrack.end(), std::back_inserter( outputVector ),
                  []( const auto& id ) { return id.isUT(); } );

    return outputVector;
  }

  std::vector<LHCb::LHCbID> get_requested_lhcbids( const LHCb::Track& track, bool includeVP, bool includeUT,
                                                   bool includeFT ) {
    std::vector<LHCb::LHCbID> lhcb_ids;

    if ( includeVP ) {
      auto velo_ids = get_velo_ids( track );
      lhcb_ids.insert( lhcb_ids.end(), velo_ids.begin(), velo_ids.end() );
    }

    if ( includeUT ) {
      auto ut_ids = get_ut_ids( track );
      lhcb_ids.insert( lhcb_ids.end(), ut_ids.begin(), ut_ids.end() );
    }

    if ( includeFT ) {
      auto ft_ids = get_ft_ids( track );
      lhcb_ids.insert( lhcb_ids.end(), ft_ids.begin(), ft_ids.end() );
    }

    return lhcb_ids;
  }

  std::optional<const LHCb::Track*> getTrack( const LHCb::Particle* particle ) {
    if ( !particle ) return std::nullopt;

    if ( !particle->isBasicParticle() ) return std::nullopt;

    if ( !particle->proto() || !( particle->proto()->track() ) ) return std::nullopt;

    return particle->proto()->track();
  }

  std::optional<const LHCb::Track*> getTrack( const LHCb::Track* track ) { return track; }
} // namespace

/**
 * Creates a relation table with the fraction of overlap
 * in LHCbIDs in the upgrade tracking detectors between one
 * set of particles or tracks, and another.
 *
 * Standard use-case is the matching of VELO / Upstream tracks
 * to long tracks, where the matching table is created with
 * the particles. Default behaviour is only matching
 * based on VELO IDs.
 *
 * @author Laurent Dufour
 * @author Guillaume Max Pietrzyk
 *
 **/
template <typename MatchFromType, typename MatchToType>
class LHCbIDOverlapRelationTable
    : public LHCb::Algorithm::Transformer<LHCb::RelationWeighted1D<MatchFromType, MatchToType, float>(
          const typename MatchFromType::Range&, const typename MatchToType::Range& )> {
public:
  using WeightedRelationTable = LHCb::RelationWeighted1D<MatchFromType, MatchToType, float>;
  using base_class_t = LHCb::Algorithm::Transformer<WeightedRelationTable( const typename MatchFromType::Range&,
                                                                           const typename MatchToType::Range& )>;

  LHCbIDOverlapRelationTable( const std::string& name, ISvcLocator* pSvc )
      : base_class_t(
            name, pSvc,
            { typename base_class_t::KeyValue{ "MatchFrom", "" }, typename base_class_t::KeyValue{ "MatchTo", "" } },
            typename base_class_t::KeyValue{ "OutputRelations", "" } ) {}

  WeightedRelationTable operator()( const typename MatchFromType::Range& match_from_particles,
                                    const typename MatchToType::Range&   match_to_objects ) const override {
    WeightedRelationTable outputRelTable;

    for ( const auto* matchFrom : match_from_particles ) {
      std::optional<const LHCb::Track*> ref_probe_track = getTrack( matchFrom );
      if ( !ref_probe_track.has_value() ) {
        ++m_msg_match_from_no_track;
        continue;
      }

      std::vector<LHCb::LHCbID> ref_probe_ids =
          get_requested_lhcbids( **ref_probe_track, m_includeVP.value(), m_includeUT.value(), m_includeFT.value() );

      for ( const auto* matchTo : match_to_objects ) {
        std::optional<const LHCb::Track*> long_track = getTrack( matchTo );
        if ( !long_track.has_value() ) {
          ++m_msg_match_to_no_track;
          continue;
        }

        if ( m_onlyConsiderOtherTracks.value() && *ref_probe_track == *long_track ) continue;

        const auto long_ids =
            get_requested_lhcbids( **long_track, m_includeVP.value(), m_includeUT.value(), m_includeFT.value() );

        // Get the number of common VELO ids between the probe track and the long track.
        const auto nMatch_with_ref_probe = get_overlap_in_lhcb_ids( ref_probe_ids, long_ids );
        float      frac_nMatch_velo      = (float)( nMatch_with_ref_probe ) / (float)( ref_probe_ids.size() );

        if ( frac_nMatch_velo > m_min_match_fraction ) {
          m_match += 1;
          outputRelTable.relate( matchFrom, matchTo, frac_nMatch_velo ).ignore(); // The fraction of common
                                                                                  // LHCb IDs between the
                                                                                  // probe and the long
                                                                                  // particles is the weight.
        } else
          m_zeromatch += 1;
      }
    }

    m_inCount += match_to_objects.size();
    m_refCount += match_from_particles.size();

    // Sort the relation table, such that best matches appear first
    outputRelTable.i_sort();

    return outputRelTable;
  }

private:
  Gaudi::Property<bool> m_includeVP{ this, "IncludeVP", true };
  Gaudi::Property<bool> m_includeFT{ this, "IncludeFT", false };
  Gaudi::Property<bool> m_includeUT{ this, "IncludeUT", false };

  Gaudi::Property<bool> m_onlyConsiderOtherTracks{ this, "OnlyLookAtOtherTracks", false };

  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{ this, "Match To Particles/Tracks" };
  mutable Gaudi::Accumulators::SummingCounter<> m_refCount{ this, "Match From Particles/Tracks" };
  mutable Gaudi::Accumulators::SummingCounter<> m_match{ this, "Found matches" };
  mutable Gaudi::Accumulators::SummingCounter<> m_zeromatch{ this, "Non-matches considered" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_from_no_track{
      this, "Match from object has no track - possibly not a basic object, or NULL" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_msg_match_to_no_track{
      this, "Match to object has no track - possibly not a basic object, or NULL" };
  Gaudi::Property<float> m_min_match_fraction{ this, "MinMatchFraction", 0.0f };
};

using LHCbIDOverlapRelationTableParticleToParticle = LHCbIDOverlapRelationTable<LHCb::Particle, LHCb::Particle>;
using LHCbIDOverlapRelationTableParticleToTrack    = LHCbIDOverlapRelationTable<LHCb::Particle, LHCb::Track>;
using LHCbIDOverlapRelationTableTrackToTrack       = LHCbIDOverlapRelationTable<LHCb::Track, LHCb::Track>;
using LHCbIDOverlapRelationTableTrackToParticle    = LHCbIDOverlapRelationTable<LHCb::Track, LHCb::Particle>;

DECLARE_COMPONENT_WITH_ID( LHCbIDOverlapRelationTableParticleToParticle, "LHCbIDOverlapRelationTable" )
DECLARE_COMPONENT_WITH_ID( LHCbIDOverlapRelationTableParticleToTrack, "LHCbIDOverlapRelationTableParticleToTrack" )
DECLARE_COMPONENT_WITH_ID( LHCbIDOverlapRelationTableTrackToTrack, "LHCbIDOverlapRelationTableTrackToTrack" )
DECLARE_COMPONENT_WITH_ID( LHCbIDOverlapRelationTableTrackToParticle, "LHCbIDOverlapRelationTableTrackToParticle" )
