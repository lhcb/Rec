/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Consumer.h"
#include "Relations/RelationWeighted1D.h"
#include <Gaudi/Accumulators/Histogram.h>

namespace {
  using WeightedRelationTable = LHCb::RelationWeighted1D<LHCb::Particle, LHCb::Particle, float>;
  using Consumer = LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range&, const WeightedRelationTable& )>;

} // namespace

/**
 * Algorithm to create plots for Online Efficiency measurement with partially reconstructed KS.q
 *
 **/
class KSVelo2LongEfficiencyMonitor : public Consumer {
public:
  KSVelo2LongEfficiencyMonitor( const std::string& name, ISvcLocator* pSvc )
      : Consumer( name, pSvc, { KeyValue{ "Particles", "" }, KeyValue{ "Table", "" } } ) {}

  StatusCode initialize() override {
    auto sc = Consumer::initialize();
    if ( sc.isFailure() ) return sc;
    using Axis1D = Gaudi::Accumulators::Axis<double>;
    m_efficiency_vs_p.emplace( this, "EfficiencyVsP", m_histodef_eff_p.value().title(),
                               Axis1D{ static_cast<unsigned int>( m_histodef_eff_p.value().bins() ),
                                       m_histodef_eff_p.value().lowEdge(), m_histodef_eff_p.value().highEdge() } );
    m_efficiency_vs_pt.emplace( this, "EfficiencyVsPt", m_histodef_eff_pt.value().title(),
                                Axis1D{ static_cast<unsigned int>( m_histodef_eff_pt.value().bins() ),
                                        m_histodef_eff_pt.value().lowEdge(), m_histodef_eff_pt.value().highEdge() } );
    m_efficiency_vs_eta.emplace( this, "EfficiencyVsEta", m_histodef_eff_eta.value().title(),
                                 Axis1D{ static_cast<unsigned int>( m_histodef_eff_eta.value().bins() ),
                                         m_histodef_eff_eta.value().lowEdge(),
                                         m_histodef_eff_eta.value().highEdge() } );

    m_efficiency_vs_p_den.emplace( this, "EfficiencyVsP_den", m_histodef_eff_p.value().title() + "_den",
                                   Axis1D{ static_cast<unsigned int>( m_histodef_eff_p.value().bins() ),
                                           m_histodef_eff_p.value().lowEdge(), m_histodef_eff_p.value().highEdge() } );
    m_efficiency_vs_pt_den.emplace( this, "EfficiencyVsPt_den", m_histodef_eff_pt.value().title() + "_den",
                                    Axis1D{ static_cast<unsigned int>( m_histodef_eff_pt.value().bins() ),
                                            m_histodef_eff_pt.value().lowEdge(),
                                            m_histodef_eff_pt.value().highEdge() } );
    m_efficiency_vs_eta_den.emplace( this, "EfficiencyVsEta_den", m_histodef_eff_eta.value().title() + "_den",
                                     Axis1D{ static_cast<unsigned int>( m_histodef_eff_eta.value().bins() ),
                                             m_histodef_eff_eta.value().lowEdge(),
                                             m_histodef_eff_eta.value().highEdge() } );

    m_efficiency_vs_p_num.emplace( this, "EfficiencyVsP_num", m_histodef_eff_p.value().title() + "_num",
                                   Axis1D{ static_cast<unsigned int>( m_histodef_eff_p.value().bins() ),
                                           m_histodef_eff_p.value().lowEdge(), m_histodef_eff_p.value().highEdge() } );
    m_efficiency_vs_pt_num.emplace( this, "EfficiencyVsPt_num", m_histodef_eff_pt.value().title() + "_num",
                                    Axis1D{ static_cast<unsigned int>( m_histodef_eff_pt.value().bins() ),
                                            m_histodef_eff_pt.value().lowEdge(),
                                            m_histodef_eff_pt.value().highEdge() } );
    m_efficiency_vs_eta_num.emplace( this, "EfficiencyVsEta_num", m_histodef_eff_eta.value().title() + "_num",
                                     Axis1D{ static_cast<unsigned int>( m_histodef_eff_eta.value().bins() ),
                                             m_histodef_eff_eta.value().lowEdge(),
                                             m_histodef_eff_eta.value().highEdge() } );

    m_efficiency_vs_eta_p.emplace( this, "EfficiencyVsEtaP", m_histodef_eff_eta.value().title(),
                                   Axis1D{ static_cast<unsigned int>( m_histodef_eff_eta.value().bins() / 2 ),
                                           m_histodef_eff_eta.value().lowEdge(),
                                           m_histodef_eff_eta.value().highEdge() },
                                   Axis1D{ static_cast<unsigned int>( m_histodef_eff_p.value().bins() / 2 ),
                                           m_histodef_eff_p.value().lowEdge(), m_histodef_eff_p.value().highEdge() } );
    m_efficiency_vs_eta_pt.emplace(
        this, "EfficiencyVsEtaPt", m_histodef_eff_eta.value().title(),
        Axis1D{ static_cast<unsigned int>( m_histodef_eff_eta.value().bins() / 2 ),
                m_histodef_eff_eta.value().lowEdge(), m_histodef_eff_eta.value().highEdge() },
        Axis1D{ static_cast<unsigned int>( m_histodef_eff_pt.value().bins() / 2 ), m_histodef_eff_pt.value().lowEdge(),
                m_histodef_eff_pt.value().highEdge() } );
    m_mass_unmatched_vs_momentum.emplace( this, "MassVsMomentum", "MassVsMomentum",
                                          Axis1D{ static_cast<unsigned int>( m_histodef_eff_p.value().bins() ),
                                                  m_histodef_eff_p.value().lowEdge(),
                                                  m_histodef_eff_p.value().highEdge() },
                                          Axis1D{ 200, 400., 600. } );
    return sc;
  }
  void operator()( const LHCb::Particle::Range& particles,
                   const WeightedRelationTable& matching_table ) const override {
    for ( auto const* particle : particles ) {
      ++m_mass_unmatched[particle->momentum().M()];

      LHCb::Particle const* tag   = nullptr;
      LHCb::Particle const* probe = nullptr;
      for ( auto const* product : particle->daughtersVector() ) {
        if ( product->proto()->track()->checkType( LHCb::Track::Types::Velo ) ) {
          probe = product;
        } else {
          tag = product;
        }
      }

      auto probe_momentum = particle->momentum() - tag->momentum();
      ++( *m_mass_unmatched_vs_momentum )[{ probe_momentum.P() / 1000., particle->momentum().M() }];
      double mass_window_size = m_signal_mass_range.value().second - m_signal_mass_range.value().first;

      bool is_signal = particle->momentum().M() > m_signal_mass_range.value().first &&
                       particle->momentum().M() < m_signal_mass_range.value().second;
      bool is_background =
          !is_signal && ( particle->momentum().M() > m_signal_mass_range.value().first - mass_window_size ||
                          particle->momentum().M() < m_signal_mass_range.value().second + mass_window_size );

      if ( !is_signal && !is_background ) continue;
      float weight = is_signal ? 1.0 : -1.0;

      m_probe_p[probe_momentum.P()] += weight;
      m_probe_pt[probe_momentum.Pt()] += weight;
      m_probe_eta[probe_momentum.Eta()] += weight;
      ( *m_efficiency_vs_p_den )[probe_momentum.P() / 1000.] += weight;
      ( *m_efficiency_vs_pt_den )[probe_momentum.Pt() / 1000.] += weight;
      ( *m_efficiency_vs_eta_den )[probe_momentum.Eta()] += weight;
      float       matched  = 0.;
      auto const& matches  = matching_table.relations( probe );
      int         n_fthits = 0;
      for ( auto const& match : matches ) {
        m_velo_match_weight[match.weight()] += weight;
        if ( match.weight() < m_min_match_fraction ) continue;
        ++m_mass_matched_no_filter[particle->momentum().M()];

        if ( match.to()->charge() * tag->charge() > 0 ) continue;

        auto rel_p_diff = probe_momentum.P() / match.to()->momentum().P() - 1.;
        m_probe_long_prel_diff[rel_p_diff] += weight;
        if ( std::abs( rel_p_diff ) > m_max_rel_momentum_diff.value() ) continue;

        auto const& ks_momentum_long = match.to()->momentum() + tag->momentum();
        ++m_mass_long[ks_momentum_long.M()];
        if ( ks_momentum_long.M() < m_long_mass_range.value().first ||
             ks_momentum_long.M() > m_long_mass_range.value().second )
          continue;

        ++m_mass_matched[particle->momentum().M()];
        matched  = 1.0;
        n_fthits = match.to()->proto()->track()->nFTHits();
        break;
      }

      m_efficiency += weight * matched;
      // no background subtraction here
      if ( is_signal ) {
        ( *m_efficiency_vs_p )[probe_momentum.P() / 1000.] += matched;
        ( *m_efficiency_vs_pt )[probe_momentum.Pt() / 1000.] += matched;
        ( *m_efficiency_vs_eta )[probe_momentum.Eta()] += matched;
        ( *m_efficiency_vs_eta_p )[{ probe_momentum.Eta(), probe_momentum.P() / 1000. }] += matched;
        ( *m_efficiency_vs_eta_pt )[{ probe_momentum.Eta(), probe_momentum.Pt() / 1000. }] += matched;
      }
      if ( matched > 0.0 ) {
        ( *m_efficiency_vs_p_num )[probe_momentum.P() / 1000.] += weight;
        ( *m_efficiency_vs_pt_num )[probe_momentum.Pt() / 1000.] += weight;
        ( *m_efficiency_vs_eta_num )[probe_momentum.Eta()] += weight;
        m_fthits[n_fthits] += weight;
      }
    }
    return;
  }

private:
  mutable Gaudi::Accumulators::SummingCounter<> m_efficiency{ this, "integrated efficiency" };

  mutable Gaudi::Accumulators::Histogram<1> m_velo_match_weight{
      this, "velo_match_weight", "velo_match_weight", { 110, 0, 1.1 } };
  mutable Gaudi::Accumulators::Histogram<1> m_probe_p{ this, "probe_p", "probe_p", { 100, 0, 100000 } };
  mutable Gaudi::Accumulators::Histogram<1> m_probe_pt{ this, "probe_pt", "probe_pt", { 100, 0, 5000 } };
  mutable Gaudi::Accumulators::Histogram<1> m_probe_eta{ this, "probe_eta", "probe_eta", { 90, 1, 5.5 } };
  mutable Gaudi::Accumulators::Histogram<1> m_probe_long_prel_diff{
      this, "probe_long_prel_diff", "probe_long_preg_diff", { 100, -0.5, 0.5 } };
  mutable Gaudi::Accumulators::Histogram<1> m_mass_unmatched{
      this, "mass_unmatched", "mass_unmatched", { 200, 400, 600 } };
  mutable Gaudi::Accumulators::Histogram<1> m_mass_matched{ this, "mass_matched", "mass_matched", { 200, 400, 600 } };
  mutable Gaudi::Accumulators::Histogram<1> m_mass_matched_no_filter{
      this, "mass_matched_no_filter", "mass_matched_no_filter", { 200, 400, 600 } };
  mutable Gaudi::Accumulators::Histogram<1> m_mass_long{ this, "mass_long", "mass_long", { 200, 400, 600 } };

  mutable Gaudi::Accumulators::Histogram<1> m_fthits{ this, "n_fthits", "n_fthits", { 10, 5.5, 15.5 } };

  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_mass_unmatched_vs_momentum;

  Gaudi::Property<Gaudi::Histo1DDef> m_histodef_eff_p{
      this, "HistogramDefEffP", { "Efficiency versus p [GeV]", 2., 50., 24 }, "Histogram definition" };
  Gaudi::Property<Gaudi::Histo1DDef> m_histodef_eff_pt{
      this, "HistogramDefEffPt", { "Efficiency versus pt [GeV]", 0.25, 5., 19 }, "Histogram definition" };
  Gaudi::Property<Gaudi::Histo1DDef> m_histodef_eff_eta{
      this, "HistogramDefEffEta", { "Efficiency versus eta", 1.5, 5., 14 }, "Histogram definition" };

  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_efficiency_vs_p;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_efficiency_vs_pt;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_efficiency_vs_eta;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_p_den;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_pt_den;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_eta_den;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_p_num;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_pt_num;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_efficiency_vs_eta_num;

  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<2>> m_efficiency_vs_eta_p;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<2>> m_efficiency_vs_eta_pt;

  Gaudi::Property<float> m_min_match_fraction{ this, "MinMatchFraction", 0.7f };
  // Range when a candidate is accepted as the momentum using two long tracks is in the Ks signal mass range.
  Gaudi::Property<float> m_max_rel_momentum_diff{ this, "MaxRelativePDiff", 0.2f };
  // Range when a candidate is accepted as the momentum using two long tracks is in the Ks signal mass range.
  Gaudi::Property<std::pair<double, double>> m_long_mass_range{ this, "LongMassRange", { 470., 520. } };
  // Range which defines a candidate is counted as signal bases on the mass from PV constraint.
  Gaudi::Property<std::pair<double, double>> m_signal_mass_range{ this, "SignalMassRange", { 480., 515. } };
};

DECLARE_COMPONENT( KSVelo2LongEfficiencyMonitor )
