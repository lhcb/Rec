/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "JsonConverters.h"

namespace {

  using Track  = LHCb::Event::v1::Track;
  using Tracks = LHCb::Event::v1::Tracks;
  using State  = LHCb::State;
  using json   = nlohmann::json;

  const std::map<Rich::ParticleIDType, std::string> color_map{
      { Rich::ParticleIDType::Unknown, "0xff0000" },  { Rich::ParticleIDType::Electron, "0xff00ff" },
      { Rich::ParticleIDType::Muon, "0x0000ff" },     { Rich::ParticleIDType::Pion, "0x777777" },
      { Rich::ParticleIDType::Kaon, "0x007700" },     { Rich::ParticleIDType::Proton, "0x00ffff" },
      { Rich::ParticleIDType::Deuteron, "0x777700" }, { Rich::ParticleIDType::BelowThreshold, "0xff0000" } };

} // namespace

namespace nlohmann {

  void to_json( json& j, State const* stateOfTrack ) {
    j = { stateOfTrack->x(), stateOfTrack->y(), stateOfTrack->z() };
  }

  void to_json( json& j, Track const* track ) {
    j = { { "chi2", track->chi2() }, { "dof", track->nDoF() }, { "pos", track->states() } };
  }

  void to_json( json& j, Tracks const& tracks ) {
    j = json::array();
    for ( auto const& t : tracks ) { j.emplace_back( t ); }
  }

  void to_json( json& j, LHCb::Pr::Velo::Tracks const& tracks ) {
    j = {};
    for ( const auto& t : tracks.scalar() ) {
      auto                  pos0 = t.StatePos( State::Location::ClosestToBeam );
      auto                  dir0 = t.StateDir( State::Location::ClosestToBeam );
      ROOT::Math::XYZVector p0{ float( pos0.x() ), float( pos0.y() ), float( pos0.z() ) };
      ROOT::Math::XYZVector d0{ float( dir0.x() ), float( dir0.y() ), float( dir0.z() ) };
      auto                  p1 = ( p0 + d0 ).Unit() * 300;
      json                  p  = { { "pos",
                                     { { float( pos0.x() ), float( pos0.y() ), float( pos0.z() ) },
                                       { float( p1.x() ), float( p1.y() ), float( p1.z() ) } } } };
      j.emplace_back( p );
    }
  }

  void to_json( json& j, LHCb::ProtoParticle const* proto ) {

    Rich::ParticleIDType pid = Rich::ParticleIDType::Unknown;
    if ( proto->muonPID() && proto->muonPID()->IsMuon() ) {
      pid = Rich::ParticleIDType::Muon;
    } else {
      if ( proto->richPID() ) { pid = proto->richPID()->bestParticleID(); }
    }

    j = { { "chi2", proto->track()->chi2() },  { "dof", proto->track()->nDoF() }, { "charge", proto->charge() },
          { "pos", proto->track()->states() }, { "id", Rich::text( pid ) },       { "color", color_map.at( pid ) } };
  }

  void to_json( json& j, LHCb::ProtoParticles const& protos ) {
    j = json::array();
    for ( auto const& p : protos ) { j.emplace_back( p ); }
  }

  void to_json( json& j, LHCb::RecVertex const* v ) {
    const auto pos = v->position();
    j              = { { "x", pos.x() }, { "y", pos.y() }, { "z", pos.z() } };
  }

  void to_json( json& j, LHCb::RecVertices const& vertices ) {
    j = json::array();
    for ( auto const& v : vertices ) { j.emplace_back( v ); }
  }

} // namespace nlohmann
