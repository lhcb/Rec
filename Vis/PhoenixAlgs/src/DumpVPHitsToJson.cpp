/*******************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "LHCbAlgs/Consumer.h"

#include "Event/ODIN.h"
#include "Event/PrHits.h"

#include "Phoenix/Store.h"

#include <string>

namespace {
  using Hits = LHCb::Pr::VP::Hits;
  using json = nlohmann::json;
} // namespace

namespace nlohmann {

  /// converters from VPHit to json
  void to_json( json& j, Hits const& hits ) {
    /**
     * Get the x, y, z positions of all Hits of type:
     * LHCb::Pr::VP::Hits
     */

    j = json::array();
    for ( auto const hit : hits.scalar() ) {
      float x         = hit.template get<LHCb::Pr::VP::VPHitsTag::pos>().x().cast();
      float y         = hit.template get<LHCb::Pr::VP::VPHitsTag::pos>().y().cast();
      float z         = hit.template get<LHCb::Pr::VP::VPHitsTag::pos>().z().cast();
      int   channelID = hit.template get<LHCb::Pr::VP::VPHitsTag::ChannelId>().cast();

      j += { { "channelID", channelID }, { "pos", { x, y, z } } };
    }
  }

} // namespace nlohmann

/**
 * Get the VP Hits run3 Event data from the Event Model and store them in the Phoenix/Store.h object,
 * which will be used from the Phoenix sink to dump them into a final .json file.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 *
 * @author Andreas PAPPAS
 * @date 03-08-2021
 */
namespace LHCb::Phoenix {
  class DumpVPHitEvent final : public Algorithm::Consumer<void( Hits const&, ODIN const& )> {

  public:
    DumpVPHitEvent( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( Hits const&, ODIN const& ) const override;

  private:
    mutable Store m_storeVP{ this, "VPHits_store", "Phoenix:VP" };
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpVPHitEvent::DumpVPHitEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "VPHitsLocation", "Raw/VP/Hits" }, KeyValue{ "ODIN", LHCb::ODINLocation::Default } } ) {}

void LHCb::Phoenix::DumpVPHitEvent::operator()( Hits const& hits, ODIN const& odin ) const {
  m_storeVP.storeEventData( { { "gps time", odin.gpsTime() },
                              { "run number", odin.runNumber() },
                              { "event number", odin.eventNumber() },
                              { "bunch crossing type", odin.bunchCrossingType() },
                              { "Content", { { "Hits", { { "VPHits", hits } } } } } } );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpVPHitEvent )
