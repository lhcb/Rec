/*******************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "JsonConverters.h"

#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"
#include <Event/ODIN.h>
#include <LHCbAlgs/Consumer.h>

#include <Phoenix/Store.h>

#include <string>

/**
 * Get the Long Tracks run3 Event data from the Event Model and store them in the Phoenix/Store.h object,
 * which will be used from the Phoenix sink to dump them into a final .json file.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 *
 * @author Andreas PAPPAS
 * @date 03-09-2021
 */
namespace LHCb::Phoenix {

  using namespace Rich::Future;

  class DumpRichEvent final
      : public Algorithm::Consumer<void( DAQ::DecodedData const&, Rich::Utils::RichSmartIDs const&, ODIN const& ),
                                   LHCb::Algorithm::Traits::usesConditions<Rich::Utils::RichSmartIDs>> {
  public:
    DumpRichEvent( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&]() { Rich::Utils::RichSmartIDs::addConditionDerivation( this ); } );
    }

    void operator()( DAQ::DecodedData const&, Rich::Utils::RichSmartIDs const&, ODIN const& ) const override;

  private:
    mutable Store m_storeTr{ this, "Tracks_store", "Phoenix:Tracks" };
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpRichEvent::DumpRichEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { { "DecodedDataLocation", DAQ::DecodedDataLocation::Default },
                  { "RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey },
                  { "ODIN", LHCb::ODINLocation::Default } } ) {}

void LHCb::Phoenix::DumpRichEvent::operator()( const DAQ::DecodedData& data, const Rich::Utils::RichSmartIDs& smartIDs,
                                               ODIN const& odin ) const {
  json hits = json::array();
  for ( const auto& rich : data ) {
    for ( const auto& panel : rich ) {
      for ( const auto& module : panel ) {
        for ( const auto& PD : module ) {
          const auto pdID = PD.pdID();
          if ( pdID.isValid() ) {
            for ( const auto& id : PD.smartIDs() ) {
              const auto gPos = smartIDs.globalPosition( id );
              hits += { { "pdID", (unsigned int)id },
                        { "type", "Box" },
                        { "pos", { gPos.x(), gPos.y(), gPos.z(), 15, 15, 15 } } };
            }
          }
        }
      }
    }
  }
  m_storeTr.storeEventData( { { "gps time", odin.gpsTime() },
                              { "run number", odin.runNumber() },
                              { "event number", odin.eventNumber() },
                              { "bunch crossing type", odin.bunchCrossingType() },
                              { "Content", { { "Hits", { { "RichHits", hits } } } } } } );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpRichEvent )
