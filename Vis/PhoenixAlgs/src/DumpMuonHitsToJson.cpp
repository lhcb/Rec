/*******************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "LHCbAlgs/Consumer.h"

#include "Event/ODIN.h"
#include "Event/PrHits.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "Phoenix/Store.h"

#include <string>

namespace {
  using json = nlohmann::json;

  /// converter from MuonHitContainer to json
  json buildHits( MuonHitContainer const& allHits, DeMuonDetector const& muonDet ) {
    json j = json::array();
    for ( unsigned int station = 0; station < 4; station++ ) {
      auto hitRange = allHits.hits( station );
      for ( auto const& hit : hitRange ) {
        auto tileID = hit.tile();
        auto pos    = muonDet.position( tileID );
        if ( pos ) {
          j += { { "tileID", (unsigned int)tileID },
                 { "type", "Box" },
                 { "pos", { pos->x(), pos->y(), pos->z(), 2 * pos->dX(), 2 * pos->dY(), 2 * pos->dZ() } } };
        }
      }
    }
    return j;
  }

} // namespace

/**
 * Get the Muon Hits run3 Event data from the Event Model and store them in the Phoenix/Store.h object,
 * which will be used from the Phoenix sink to dump them into a final .json file.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 */
namespace LHCb::Phoenix {
  class DumpMuonHitEvent final
      : public Algorithm::Consumer<void( MuonHitContainer const&, ODIN const&, DeMuonDetector const& ),
                                   LHCb::Algorithm::Traits::usesConditions<DeMuonDetector>> {

  public:
    DumpMuonHitEvent( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( MuonHitContainer const&, ODIN const&, DeMuonDetector const& ) const override;

  private:
    mutable Store m_storeMuon{ this, "MuonHits_store", "Phoenix:Muon" };
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpMuonHitEvent::DumpMuonHitEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "HitContainer", MuonHitContainerLocation::Default },
                  KeyValue{ "ODIN", LHCb::ODINLocation::Default },
                  KeyValue{ "MuonDetectorPath", DeMuonLocation::Default } } ) {}

void LHCb::Phoenix::DumpMuonHitEvent::operator()( MuonHitContainer const& muonHits, ODIN const& odin,
                                                  DeMuonDetector const& muonDet ) const {
  auto jsonHits = buildHits( muonHits, muonDet );
  m_storeMuon.storeEventData( { { "gps time", odin.gpsTime() },
                                { "run number", odin.runNumber() },
                                { "event number", odin.eventNumber() },
                                { "bunch crossing type", odin.bunchCrossingType() },
                                { "Content", { { "Hits", { { "MuonHits", jsonHits } } } } } } );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpMuonHitEvent )
