/*******************************************************************************\
 * (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

// INCLUDES

// STL
#include <algorithm>
#include <map>
#include <mutex>
#include <optional>
#include <string>

#include <DetDesc/DetectorElement.h>
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Event/ODIN.h>
#include <Event/PrVeloTracks.h>
#include <Event/ProtoParticle.h>
#include <Event/RecVertex.h>
#include <Event/Track_v1.h>
#include <GaudiKernel/ToolHandle.h>
#include <LHCbAlgs/Consumer.h>
#include <Phoenix/Store.h>
#include <TrackInterfaces/ITrackExtrapolator.h>

#include "JsonConverters.h"

/** @class DumpProtoParticlesEvent DumpProtoParticlesToJson.cpp
 *
 * Dump protoparticles to the JSON sink
 *
 */
namespace LHCb::Phoenix {

  class DumpProtoParticlesEvent final
      : public LHCb::Algorithm::Consumer<void( ProtoParticle::Range const&, ProtoParticle::Range const&, ODIN const&,
                                               const LHCb::Event::v1::Tracks&, const LHCb::Pr::Velo::Tracks&,
                                               const LHCb::RecVertices&, DetectorElement const& ),
                                         LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {

  public:
    /// Standard constructor
    DumpProtoParticlesEvent( const std::string& name, ISvcLocator* pSvcLocator );

    void operator()( ProtoParticle::Range const&, ProtoParticle::Range const&, ODIN const&,
                     const LHCb::Event::v1::Tracks&, const LHCb::Pr::Velo::Tracks&, const LHCb::RecVertices&,
                     DetectorElement const& ) const override;

  private:
    // Needed to extend the tracks in the detector
    ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "TrackMasterExtrapolator", "TrackMasterExtrapolator" };

    /// the entity which the Tracks Event Data will be stored
    /// thread safe
    mutable Store m_storeTr{ this, "Tracks_store", "Phoenix:Tracks" };
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpProtoParticlesEvent::DumpProtoParticlesEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "ChargedProtoParticles", ProtoParticleLocation::Charged },
                  KeyValue{ "NeutralProtoParticles", ProtoParticleLocation::Neutrals },
                  KeyValue{ "ODIN", LHCb::ODINLocation::Default }, KeyValue{ "VeloTracks", "Rec/Track/Velo" },
                  KeyValue{ "BackwardTracks", "" }, KeyValue{ "PrimaryVertices", LHCb::RecVertexLocation::Primary },
                  KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } } ) {}

void LHCb::Phoenix::DumpProtoParticlesEvent::operator()( ProtoParticle::Range const& charged,
                                                         ProtoParticle::Range const&, ODIN const& odin,
                                                         const LHCb::Event::v1::Tracks& veloTracks,
                                                         const LHCb::Pr::Velo::Tracks&  backwardTracks,
                                                         const LHCb::RecVertices&       primaryVertices,
                                                         DetectorElement const&         geometry ) const {

  // Cloning a local copy of all protoparticles, to be able to edit the tracks states
  std::vector<std::unique_ptr<ProtoParticle>> updated_charged;
  updated_charged.reserve( charged.size() );
  for ( const auto* proto : charged ) { updated_charged.push_back( std::unique_ptr<ProtoParticle>{ proto->clone() } ); }

  // To keep track of the allocated tracks
  std::deque<LHCb::Event::v1::Track> all_tracks;

  // Iterating on protoparticles to add the states we need
  for ( auto& proto : updated_charged ) {
    const LHCb::Event::v1::Track* track = proto->track();
    // It would be better to allocate the Tracks on the stack as per the commented lines below ,
    // but the states get corrupted in that case, to be investigated
    auto&                   tmp_track = all_tracks.emplace_back( *track );
    LHCb::Event::v1::Track* new_track = &tmp_track;

    bool ismuon = false;
    if ( proto->muonPID() && proto->muonPID()->IsMuon() ) { ismuon = true; }

    // Little utility to deal with states
    auto extrapolate_state = [&]( LHCb::Event::v1::Track* new_track, double z, LHCb::Tr::PID pid ) {
      auto s = new_track->closestState( z );
      if ( std::abs( s.z() - z ) < 500 ) {
        // we bail out if there is a state close by already, as this messes up the spline in Phoenix
        return;
      }
      auto new_state = LHCb::State{ s };
      m_extrapolator->propagate( new_state, z, geometry, pid ).ignore();
      new_track->addToStates( new_state );
    };

    // Making sure we have a state after the Detector for muons
    // using the TrackExtrapolator
    double zEnd = 20000;
    if ( ismuon ) {
      auto   states     = track->states();
      double lastStateZ = states.back()->z();
      if ( lastStateZ < zEnd ) { extrapolate_state( new_track, zEnd, LHCb::Tr::PID::Muon() ); }
    }

    // still need to add states e.g. at z=3.5m and z=7.5m
    LHCb::Tr::PID pid = LHCb::Tr::PID::Pion();
    if ( ismuon ) { pid = LHCb::Tr::PID::Muon(); }
    if ( track->hasVelo() || track->hasUT() )
      extrapolate_state( new_track, 3500, pid ); // Before magnet, only for tracks crossing the magnet
    extrapolate_state( new_track, 7500, pid );   // After magnet
    proto->setTrack( new_track );
  }

  // Adding a to_json in JsonConverters.cpp that takes a std::unique_ptr of ProtoParticle*
  // is not accepted by nlohmann::json, whereas passing the pointer works.
  // Investigations failed sor decided to iterate here on the vector of unique_pts and serialize the
  // ProtoParticle*
  nlohmann::json json_updated_charged_long = {};
  nlohmann::json json_updated_charged_seed = {};
  for ( auto const& p : updated_charged ) {
    if ( p->track()->hasVelo() && p->track()->hasT() ) json_updated_charged_long.push_back( p.get() );
    if ( !p->track()->hasVelo() && !p->track()->hasUT() && p->track()->hasT() )
      json_updated_charged_seed.push_back( p.get() );
  }

  // Now filtering the velo tracks already used as part of the long tracks
  std::vector<LHCb::Event::v1::Track const*> filteredVeloTracks;
  filteredVeloTracks.reserve( veloTracks.size() );
  for ( auto const* vt : veloTracks ) {
    bool islong = false;
    for ( auto const* p : charged ) {
      if ( p->track()->containsLhcbIDs( vt->lhcbIDs() ) ) {
        islong = true;
        break;
      }
      if ( !islong ) { filteredVeloTracks.push_back( vt ); }
    }
  }

  m_storeTr.storeEventData( { { "gps time", odin.gpsTime() },
                              { "run number", odin.runNumber() },
                              { "event number", odin.eventNumber() },
                              { "bunch crossing type", odin.bunchCrossingType() },
                              { "Content",
                                { { "Tracks",
                                    { { "LongTracks", json_updated_charged_long },
                                      { "VeloTracks", filteredVeloTracks },
                                      { "SeedTracks", json_updated_charged_seed },
                                      { "BackwardTracks", backwardTracks } } },
                                  { "Vertices", { { "primary", primaryVertices } } } } } } );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpProtoParticlesEvent )
