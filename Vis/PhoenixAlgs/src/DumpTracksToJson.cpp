/*******************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "JsonConverters.h"

#include <Event/ODIN.h>
#include <Event/PrVeloTracks.h>
#include <Event/Track_v1.h>
#include <LHCbAlgs/Consumer.h>
#include <TrackInterfaces/ITrackExtrapolator.h>

#include <Phoenix/Store.h>

#include <string>

/**
 * Get the Long Tracks run3 Event data from the Event Model and store them in the Phoenix/Store.h object,
 * which will be used from the Phoenix sink to dump them into a final .json file.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 *
 * @author Andreas PAPPAS
 * @date 03-09-2021
 */
namespace LHCb::Phoenix {
  class DumpTracksEvent final : public Algorithm::Consumer<void( Tracks const&, ODIN const& )> {

  public:
    DumpTracksEvent( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( Tracks const&, ODIN const& ) const override;

  private:
    mutable Store m_storeTr{ this, "Tracks_store", "Phoenix:Tracks" };
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpTracksEvent::DumpTracksEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "InputTracksName", Event::v1::TrackLocation::Velo },
                  KeyValue{ "ODIN", LHCb::ODINLocation::Default } } ) {}

void LHCb::Phoenix::DumpTracksEvent::operator()( Tracks const& tracks, ODIN const& odin ) const {
  // first extrapolate tracks until calo or Muon
  for ( auto const* track : tracks ) { std::cout << "Track type " << track->type() << std::endl; }
  // And now simply dump all tracks
  m_storeTr.storeEventData( { { "gps time", odin.gpsTime() },
                              { "run number", odin.runNumber() },
                              { "event number", odin.eventNumber() },
                              { "bunch crossing type", odin.bunchCrossingType() },
                              { "Content", { { "Tracks", { { "LongTracks", tracks } } } } } } );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpTracksEvent )
