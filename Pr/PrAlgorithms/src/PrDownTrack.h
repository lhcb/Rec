/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/Point3DTypes.h"

#include "Event/PrHits.h"
#include "Event/State.h"
#include "Event/StateParameters.h"

#include "boost/container/small_vector.hpp"

#include <cassert>

namespace Downstream {
  struct Hit {
  private:
    inline auto getScalarHit() const {
      const auto myHits = hits->scalar();
      assert( (std::size_t)hit < myHits.size() );
      return myHits[hit];
    }

  public:
    const LHCb::Pr::UT::Hits* hits = nullptr;
    int                       hit{ 0 };
    float                     x{ 0 }, z{ 0 };
    float                     projection{ 0 };

    using F = SIMDWrapper::scalar::types::float_v;
    using I = SIMDWrapper::scalar::types::int_v;

    Hit( const LHCb::Pr::UT::Hits* _hits, const int _hit, float _x, float _z, float _proj )
        : hits( _hits ), hit( _hit ), x( _x ), z( _z ), projection( _proj ) {}

    [[nodiscard]] auto lhcbID() const {
      const auto mH     = getScalarHit();
      const auto chanID = mH.get<LHCb::Pr::UT::UTHitsTag::channelID>().cast();
      return bit_cast<int>( LHCb::LHCbID( LHCb::Detector::UT::ChannelID( chanID ) ).lhcbID() );
    }
    [[nodiscard]] int planeCode() const {
      const auto mH     = getScalarHit();
      auto       lhcbid = mH.get<LHCb::Pr::UT::UTHitsTag::channelID>().cast();
      return ( lhcbid & static_cast<unsigned int>( UTInfo::MasksBits::HalfLayerMask ) ) >>
             static_cast<int>( UTInfo::MasksBits::HalfLayerBits );
    }
    [[nodiscard]] auto weight() const {
      const auto mH = getScalarHit();
      return mH.get<LHCb::Pr::UT::UTHitsTag::weight>().cast();
    }

    [[nodiscard]] auto sin() const {
      const auto mH = getScalarHit();
      return -mH.get<LHCb::Pr::UT::UTHitsTag::dxDy>().cast() * mH.get<LHCb::Pr::UT::UTHitsTag::cos>().cast();
    }

    [[nodiscard]] auto zAtYEq0() const {
      const auto mH = getScalarHit();
      return mH.get<LHCb::Pr::UT::UTHitsTag::zAtYEq0>().cast();
    }
    [[nodiscard]] bool isYCompatible( const float y, const float tol ) const {
      const auto mH = getScalarHit();
      const auto yMin =
          std::min( mH.get<LHCb::Pr::UT::UTHitsTag::yBegin>().cast(), mH.get<LHCb::Pr::UT::UTHitsTag::yEnd>().cast() );
      const auto yMax =
          std::max( mH.get<LHCb::Pr::UT::UTHitsTag::yBegin>().cast(), mH.get<LHCb::Pr::UT::UTHitsTag::yEnd>().cast() );
      return ( ( ( yMin - tol ) <= y ) && ( y <= ( yMax + tol ) ) );
    }
    [[nodiscard]] auto xAt( const float y ) const {
      const auto mH = getScalarHit();
      return mH.get<LHCb::Pr::UT::UTHitsTag::xAtYEq0>().cast() + y * mH.get<LHCb::Pr::UT::UTHitsTag::dxDy>().cast();
    }
  };

  using Hits = std::vector<Hit, LHCb::Allocators::EventLocal<Hit>>;

  inline constexpr auto IncreaseByProj = []( const Hit& lhs, const Hit& rhs ) {
    return ( lhs.projection < rhs.projection ? true : //
                 rhs.projection < lhs.projection ? false
                                                 : //
                 lhs.lhcbID() < rhs.lhcbID() );
  };
} // namespace Downstream

/** @class PrDownTrack PrDownTrack.h
 *  Track helper for Downstream track search
 *  Adapted from Pat/PatKShort package
 *  Further adapted for use with PrLongLivedTracking
 *
 *  @author Olivier Callot
 *  @date   2007-10-18
 *
 *  @author Adam Davis
 *  @date   2016-04-10
 *
 *  @author Christoph Hasse (new framework)
 *  @date   2017-03-01
 */

class PrDownTrack final {
public:
  using Hits = boost::container::small_vector<Downstream::Hit, 12, LHCb::Allocators::EventLocal<Downstream::Hit>>;
  // using Hits = boost::container::static_vector<Downstream::Hit, 20>;
  // Until we can put a bound on the number of hits, use a small_vector

  PrDownTrack( Gaudi::TrackVector stateVector, double stateZ, double zUT, LHCb::span<const double, 7> magnetParams,
               LHCb::span<const double> yParams, LHCb::span<const double, 3> momPar, double magnetScale )
      : m_stateVector( stateVector ), m_stateZ( stateZ ), m_zUT( zUT ) {
    const auto tx2  = stateTx() * stateTx();
    const auto ty2  = stateTy() * stateTy();
    m_momentumParam = ( momPar[0] + momPar[1] * tx2 + momPar[2] * ty2 ) * magnetScale;

    // -- See PrFitKsParams to see how these coefficients are derived.
    const double zMagnet = magnetParams[0] + magnetParams[1] * ty2 + magnetParams[2] * tx2 +
                           magnetParams[3] * std::abs( stateQoP() ) + /// this is where the old one stopped.
                           magnetParams[4] * std::abs( stateX() ) + magnetParams[5] * std::abs( stateY() ) +
                           magnetParams[6] * std::abs( stateTy() );

    const double dz      = zMagnet - stateZ;
    const double xMagnet = stateX() + dz * stateTx();
    m_slopeX             = xMagnet / zMagnet;
    const double dSlope  = std::abs( m_slopeX - stateTx() );
    const double dSlope2 = dSlope * dSlope;

    const double by = stateY() / ( stateZ + ( yParams[0] * fabs( stateTy() ) * zMagnet + yParams[1] ) * dSlope2 );
    m_slopeY        = by * ( 1. + yParams[0] * fabs( by ) * dSlope2 );

    const double yMagnet = stateY() + dz * by - yParams[1] * by * dSlope2;

    // -- These resolutions are semi-empirical and are obtained by fitting residuals
    // -- with MCHits and reconstructed tracks
    // -- See Tracking &Alignment meeting, 19.2.2015, for the idea
    double errXMag = dSlope2 * 15.0 + dSlope * 15.0 + 3.0;
    double errYMag = dSlope2 * 80.0 + dSlope * 10.0 + 4.0;

    // -- Assume better resolution for SciFi than for OT
    // -- obviously this should be properly tuned...
    errXMag /= 2.0;
    errYMag /= 1.5;

    // errXMag = 0.5  + 5.3*dSlope + 6.7*dSlope2;
    // errYMag = 0.37 + 0.7*dSlope - 4.0*dSlope2 + 11*dSlope2*dSlope;

    m_weightXMag = 1.0 / ( errXMag * errXMag );
    m_weightYMag = 1.0 / ( errYMag * errYMag );

    m_magnet = Gaudi::XYZPoint( xMagnet, yMagnet, zMagnet );

    //=== Save for reference
    m_displX = 0.;
    m_displY = 0.;

    //=== Initialize all other data members
    m_chi2 = 0.;
  }

  /// getters
  double      stateX() const noexcept { return m_stateVector[0]; }
  double      stateY() const noexcept { return m_stateVector[1]; }
  double      stateZ() const noexcept { return m_stateZ; }
  double      stateTx() const noexcept { return m_stateVector[2]; }
  double      stateTy() const noexcept { return m_stateVector[3]; }
  double      stateQoP() const noexcept { return m_stateVector[4]; }
  Hits&       hits() noexcept { return m_hits; }
  const Hits& hits() const noexcept { return m_hits; }
  double      xMagnet() const noexcept { return m_magnet.x(); }
  double      yMagnet() const noexcept { return m_magnet.y(); }
  double      zMagnet() const noexcept { return m_magnet.z(); }
  double      slopeX() const noexcept { return m_slopeX; }
  double      slopeY() const noexcept { return m_slopeY; }
  double      weightXMag() const noexcept { return m_weightXMag; }
  double      weightYMag() const noexcept { return m_weightYMag; }
  double      chi2() const noexcept { return m_chi2; }
  double      zUT() const noexcept { return m_zUT; }

  /// setters
  void setSlopeX( double slopeX ) noexcept { m_slopeX = slopeX; }
  void setChi2( double chi2 ) noexcept { m_chi2 = chi2; }

  // functions
  double xAtZ( double z ) const noexcept {
    const double curvature = 1.6e-5 * ( stateTx() - m_slopeX );
    return xMagnet() + ( z - zMagnet() ) * m_slopeX + curvature * ( z - m_zUT ) * ( z - m_zUT );
  }

  double yAtZ( double z ) const noexcept { return yMagnet() + m_displY + ( z - zMagnet() ) * slopeY(); }

  void updateX( double dx, double dsl ) noexcept {
    m_displX += dx;
    m_magnet = Gaudi::XYZPoint( m_magnet.x() + dx, m_magnet.y(), m_magnet.z() );
    m_slopeX += dsl;
  }
  void updateY( double dy ) noexcept { m_displY += dy; }

  double dxMagnet() const noexcept { return -m_displX; }

  double initialChi2() const noexcept {
    return m_displX * m_displX * m_weightXMag + m_displY * m_displY * m_weightYMag;
  }

  double momentum() const noexcept { return m_momentumParam / ( stateTx() - m_slopeX ); }

  double pt() const noexcept {
    const double tx2      = slopeX() * slopeX();
    const double ty2      = slopeY() * slopeY();
    const double sinTrack = sqrt( 1. - 1. / ( 1. + tx2 + ty2 ) );
    return sinTrack * std::abs( momentum() );
  }

  double distance( const Downstream::Hit& hit ) const noexcept { return hit.xAt( yAtZ( hit.z ) ) - xAtZ( hit.z ); }

  bool isYCompatible( const float tol ) const noexcept {
    return std ::all_of( m_hits.begin(), m_hits.end(),
                         [&]( const auto& hit ) { return hit.isYCompatible( yAtZ( hit.z ), tol ); } );
  }

  void sortFinalHits() noexcept {
    std::sort( m_hits.begin(), m_hits.end(), []( const Downstream::Hit& lhs, const Downstream::Hit& rhs ) {
      return std::make_tuple( lhs.z, lhs.lhcbID() ) < std::make_tuple( rhs.z, rhs.lhcbID() );
    } );
  }

private:
  Gaudi::TrackVector m_stateVector;
  double             m_stateZ{ 0 };
  Gaudi::XYZPoint    m_magnet;

  double m_momentumParam{ 0 };
  double m_zUT{ 0 };
  double m_slopeX{ 0 };
  double m_slopeY{ 0 };
  double m_displX{ 0 };
  double m_displY{ 0 };
  double m_weightXMag{ 0 };
  double m_weightYMag{ 0 };
  double m_chi2{ 0 };

  Hits m_hits; /// working list of hits on this track
};

// collection of downstream tracks... From PatDownTrack
using PrDownTracks = std::vector<PrDownTrack>;
