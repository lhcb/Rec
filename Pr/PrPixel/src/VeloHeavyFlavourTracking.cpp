/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IConditionDerivationMgr.h"
#include "Event/PrHits.h"
#include "Event/Track.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/MatVec.h"
#include "Relations/Relation2D.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"
#include "VPDet/DeVP.h"
#include "VPDet/VPDetPaths.h"

namespace LHCb::Pr::Velo {

  namespace {

    // in/outputs
    using VeloHits  = LHCb::Pr::VP::Hits;
    using PVs       = LHCb::PrimaryVertices;
    using Track     = LHCb::Event::v1::Track;
    using Tracks    = Track::Container;
    using Relations = LHCb::Relation2D<LHCb::Particle, Track>;
    using Output    = std::tuple<Tracks, Relations>;

    // miscellaneous
    using Position  = LHCb::LinAlg::Vec<float, 3>;
    using SensorID  = LHCb::Detector::VPChannelID::SensorID;
    using ChannelID = LHCb::Detector::VPChannelID;

    // to store relevant sensor information
    struct SensorInfo {
      float    z;
      SensorID id;
      // sorting
      friend bool operator<( SensorInfo const& lhs, SensorInfo const& rhs ) {
        return std::pair{ lhs.z, lhs.id } < std::pair{ rhs.z, rhs.id };
      }
    };
    using SensorInfos = std::vector<SensorInfo>;

    class Sensors {
    private:
      SensorInfos m_sensors;

    public:
      // constructors
      Sensors() = default;
      // conditions related constructor
      Sensors( DeVP const& velo ) {
        m_sensors = SensorInfos{};
        m_sensors.reserve( velo.numberSensors() );
        auto getSensorZs = [&]( auto const& sensor ) {
          m_sensors.push_back( { (float)sensor.z(), sensor.sensorNumber() } );
        };
        velo.runOnAllSensors( getSensorZs );
        std::sort( m_sensors.begin(), m_sensors.end() );
      };
      // main access
      SensorInfos const& operator()() const { return m_sensors; }
    };

    // to store relevant hit information
    struct HitInfo {
      float     d;
      Position  position;
      ChannelID id;
      // sorting in z first, then distance to search window
      friend bool operator<( HitInfo const& lhs, HitInfo const& rhs ) {
        return std::pair{ lhs.position.z(), lhs.d } < std::pair{ rhs.position.z(), rhs.d };
      }
      // equal just by unique channel id
      friend bool operator==( HitInfo const& lhs, HitInfo const& rhs ) { return lhs.id == rhs.id; }
    };
    using HitInfos = std::vector<HitInfo>;

    // functions
    template <typename Position3, typename Vector3>
    SensorInfos getCrossedSensors( DeVP const& velo, SensorInfos const& sensors, Position3 const& start,
                                   Position3 const& end, Vector3 const& slopes, float max_distance_squared ) {
      SensorInfos crossedSensors;
      auto        pos = start;
      for ( auto sensor : sensors ) {
        if ( sensor.z < pos.Z() ) continue;
        if ( sensor.z > end.Z() ) break;
        // if in range look if search window intersects with sensor
        pos = pos + ( sensor.z - pos.Z() ) * slopes;
        auto dist =
            velo.sensor( sensor.id ).closestDistanceToSensor( ROOT::Math::XYZPoint( pos.X(), pos.Y(), pos.Z() ) );
        if ( dist.perp2() < max_distance_squared ) crossedSensors.push_back( { pos.Z(), sensor.id } );
      }
      return crossedSensors;
    }

    template <typename Position3, typename Vector3>
    HitInfos getHitsForCrossedSensors( SensorInfos const& sensors, VeloHits const& hits, Position3 const& start,
                                       Vector3 const& slopes, float max_distance ) {
      HitInfos poshits;
      if ( !sensors.size() ) return poshits;
      poshits.reserve( 2 * sensors.size() );
      // starting point search
      auto pos = start;
      // check sensors
      for ( auto const sensor : sensors ) {
        // go to sensor location
        pos = pos + ( sensor.z - pos.z() ) * slopes;
        // look if this sensor has hits and if they are in the search window
        for ( auto hit : hits.scalar() ) {
          auto id = ChannelID( hit.get<LHCb::Pr::VP::VPHitsTag::ChannelId>().cast() );
          if ( id.sensor() == sensor.id ) {
            auto hitpos = hit.get<LHCb::Pr::VP::VPHitsTag::pos>().cast<float>();
            auto dist   = ( hitpos - pos ).rho();
            if ( dist < max_distance ) poshits.push_back( { dist, hitpos, id } );
          }
        }
      }
      // sort and remove duplicates (shouldn't be possible in this logic, but leave out of paranoia)
      std::sort( poshits.begin(), poshits.end() );
      poshits.erase( std::unique( poshits.begin(), poshits.end() ), poshits.end() );
      return poshits;
    }

    HitInfos getUniqueHitsPerZ( HitInfos const& all_hits ) {
      HitInfos hits;
      float    z = -std::numeric_limits<float>::max();
      // assumes sorted in z then distance to PV-SV line
      for ( auto const hit : all_hits ) {
        if ( hit.position.z() > z ) {
          hits.push_back( hit );
          z = hit.position.z();
        }
      }
      return hits;
    }

  } // namespace

  /**
   * @class HeavyFlavourTrackFinder
   * Search for hits of charged b/c hadrons based on primary and secondary vertices
   *
   * @author Maarten van Veghel (Nikhef, RUG)
   */

  class HeavyFlavourTrackFinder
      : public Algorithm::MultiTransformer<Output( LHCb::Particle::Range const&, PVs const&, VeloHits const&,
                                                   DeVP const&, Sensors const& ),
                                           LHCb::Algorithm::Traits::usesConditions<DeVP, Sensors>> {
  public:
    // standard constructor
    HeavyFlavourTrackFinder( const std::string& name, ISvcLocator* pSvcLocator );

    // initialize
    StatusCode initialize() override;

    // main function/operator
    Output operator()( LHCb::Particle::Range const&, PVs const&, VeloHits const&, DeVP const&,
                       Sensors const& ) const override;

  private:
    // properties
    Gaudi::Property<float> m_max_distance{ this, "MaxDistanceFromPVSV", 0.5 * Gaudi::Units::mm,
                                           "maximum hit distance from PV-SV line (search window)" };
    Gaudi::Property<bool>  m_unique_hits{ this, "UniqueHitsPerZ", true,
                                         "Make sure only the closest hit to PV-SV line per z value is used" };
    Gaudi::Property<bool>  m_revert_charge{ this, "RevertCharge", false, "Revert charge with respect to composite" };

    // statistics monitoring
    mutable Gaudi::Accumulators::StatCounter<> m_nInSensor{ this, "Nb of sensors crossed" };
    mutable Gaudi::Accumulators::StatCounter<> m_nHitCandidates{ this, "Nb hit candidates" };
  }; // namespace LHCb::Pr::Velo

  DECLARE_COMPONENT_WITH_ID( HeavyFlavourTrackFinder, "VeloHeavyFlavourTrackFinder" )

  // ============================= IMPLEMENTATION ===================================

  // constructor
  HeavyFlavourTrackFinder::HeavyFlavourTrackFinder( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer( name, pSvcLocator,
                          { KeyValue( "Composites", "" ), KeyValue( "PVs", "" ), KeyValue( "Hits", "" ),
                            KeyValue( "DeVP", LHCb::Det::VP::det_path ),
                            KeyValue( "Sensors", { "AlgorithmSpecific-" + name + "-sensorinfos" } ) },
                          { KeyValue{ "OutputTracks", "" }, KeyValue{ "OutputRelations", "" } } ) {}

  // initialization
  StatusCode LHCb::Pr::Velo::HeavyFlavourTrackFinder::initialize() {
    auto sc = MultiTransformer::initialize().andThen( [&] {
      addConditionDerivation<Sensors( DeVP const& )>( { LHCb::Det::VP::det_path }, inputLocation<Sensors>() );
    } );
    return sc;
  }

  // main execution
  Output LHCb::Pr::Velo::HeavyFlavourTrackFinder::operator()( LHCb::Particle::Range const& composites, PVs const& pvs,
                                                              VeloHits const& velohits, DeVP const& velo,
                                                              Sensors const& sensors ) const {
    // output
    auto result               = Output{};
    auto& [tracks, relations] = result;
    tracks.reserve( composites.size() );
    relations.reserve( composites.size() );

    for ( LHCb::Particle const* composite : composites ) {
      // primary vertex
      using Sel::Utils::endVertexPos;
      auto const& pv_obj = LHCb::bestPV( pvs, *composite );
      auto const& pv     = endVertexPos( pv_obj ).cast<float>();

      // secondary vertex
      auto const& end    = endVertexPos( *composite ).cast<float>();
      auto const  fd     = end - pv;
      auto const  slopes = fd / fd.Z();

      // crossed sensors in PV-SV cylinder (with m_max_distance as radius)
      auto crossedSensors = getCrossedSensors( velo, sensors(), pv, end, slopes, std::pow( m_max_distance, 2 ) );

      // build track (output; also save if no sensors crossed, as search window info is still persisted this way)
      using namespace LHCb::Event::Enum::Track;
      auto track = new Track( History::PrVeloHeavyFlavour, Type::Velo, PatRecStatus::PatRecIDs );
      track->addInfo( AdditionalInfo::nPRVelo3DExpect, crossedSensors.size() );

      // look for hits in relevant sensors (duplicates removed and sorted in z and distance to search window)
      auto all_found_hits = getHitsForCrossedSensors( crossedSensors, velohits, pv, slopes, m_max_distance.value() );
      auto unique_hits    = getUniqueHitsPerZ( all_found_hits );

      auto found_hits = m_unique_hits ? unique_hits : all_found_hits;
      auto nhits      = found_hits.size();

      // store sorted hits
      std::vector<LHCbID> found_ids;
      found_ids.reserve( nhits );
      for ( auto const found_hit : found_hits ) { found_ids.push_back( LHCbID( found_hit.id ) ); }
      track->setLhcbIDs( found_ids );

      // determine mother particle direction based on closest hit
      // in first crossed sensor or using SV-PV if not available
      auto direction    = nhits > 0 ? found_hits.front().position - pv : fd;
      auto charge       = composite->charge() != 0 ? ( m_revert_charge ? -1 : 1 ) * composite->charge() : 1;
      auto qoverp       = charge / composite->p();
      auto origin_state = LHCb::State();
      origin_state.setState( pv.x(), pv.y(), pv.z(), direction.x() / direction.z(), direction.y() / direction.z(),
                             qoverp );
      origin_state.setLocation( LHCb::State::Location::ClosestToBeam );
      track->addToStates( origin_state );

      // in case of found hit(s), add appropriate states
      if ( nhits > 0 ) {
        auto fh = found_hits.front();
        auto fm_state =
            LHCb::State( origin_state.stateVector(), fh.position.z(), LHCb::State::Location::FirstMeasurement );
        fm_state.setX( fh.position.x() );
        fm_state.setY( fh.position.y() );
        track->addToStates( fm_state );

        // in case more unique hits, add last measurement state
        if ( unique_hits.size() > 1 ) {
          auto lh           = unique_hits.back();
          auto lh_direction = lh.position - pv;
          auto last_state   = LHCb::State();
          last_state.setState( lh.position.x(), lh.position.y(), lh.position.z(), lh_direction.x() / lh_direction.z(),
                               lh_direction.y() / lh_direction.z(), qoverp );
          last_state.setLocation( LHCb::State::Location::LastMeasurement );
          track->addToStates( last_state );
        }
      }

      // save track
      tracks.insert( track );
      relations.i_push( composite, track );

      // statistics
      m_nInSensor += crossedSensors.size();
      m_nHitCandidates += nhits;
    }

    // mandatory sorting of relation table
    relations.i_sort();

    return result;
  }

} // namespace LHCb::Pr::Velo
