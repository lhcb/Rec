/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "LHCbDet/InteractionRegion.h"

#include "Event/PrHits.h"
#include "Event/PrLongTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/Track_v1.h"
#include "Event/Track_v3.h"

#include "VeloKalmanHelpers.h"

/**
 * Velo only Kalman fit
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */
namespace LHCb::Pr::Velo {
  using VP::Hits;

  class Kalman : public Algorithm::Transformer<Event::v3::Tracks( const EventContext&, UniqueIDGenerator const&,
                                                                  const Hits&, const Tracks&, const Long::Tracks&,
                                                                  const Conditions::InteractionRegion& ),
                                               Algorithm::Traits::usesConditions<Conditions::InteractionRegion>> {
    using TracksVP  = Tracks;
    using TracksFT  = Long::Tracks;
    using TracksFit = Event::v3::Tracks;
    using simd      = SIMDWrapper::best::types;
    using I         = simd::int_v;
    using F         = simd::float_v;

  public:
    Kalman( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       { KeyValue{ "UniqueIDGenerator", UniqueIDGeneratorLocation::Default },
                         KeyValue{ "HitsLocation", "Raw/VP/Hits" }, KeyValue{ "TracksVPLocation", "Rec/Track/VP" },
                         KeyValue{ "TracksFTLocation", "Rec/Track/FT" },
                         KeyValue{ "InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion" } },
                       KeyValue{ "OutputTracksLocation", "Rec/Track/Fit" } ) {}

    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&]() {
        // This is only needed to have a fallback in case the IR condition does not exist. In that case, the information
        // is taken from DeVP and thus the Velo motion system which is not exactly the same.
        Conditions::InteractionRegion::addConditionDerivation( this, inputLocation<Conditions::InteractionRegion>() );
      } );
    }

    TracksFit operator()( const EventContext& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen, const Hits& hits,
                          const TracksVP& tracksVP, const TracksFT& tracksFT,
                          const LHCb::Conditions::InteractionRegion& region ) const override {
      // Forward tracks and its fit are zipable as there is a one to one correspondence.
      TracksFit out{ Event::Enum::Track::Type::Long,
                     Event::Enum::Track::FitHistory::VeloKalman,
                     false,
                     unique_id_gen,
                     tracksFT.zipIdentifier(),
                     LHCb::getMemResource( evtCtx ) };
      out.reserve( tracksFT.size() );
      m_nbTracksCounter += tracksFT.size();

      for ( auto const& track : tracksFT.simd() ) {
        auto       loop_mask = track.loop_mask();
        auto const idxVP     = track.trackVP();
        auto const qop       = track.qOverP();

        auto [stateInfo, chi2, nDof] = fitBackwardWithMomentum(
            loop_mask, tracksVP, idxVP, qop, hits, Event::Enum::State::Location::ClosestToBeam, region.avgPosition );

        auto outTrack = out.emplace_back( tracksFT.size() );

        outTrack.field<LHCb::Event::v3::Tag::trackVP>().set( track.trackVP() );
        outTrack.field<LHCb::Event::v3::Tag::trackUT>().set( track.trackUT() );
        outTrack.field<LHCb::Event::v3::Tag::trackSeed>().set( track.indices() );
        outTrack.field<LHCb::Event::v3::Tag::Chi2>().set( chi2 );
        outTrack.field<LHCb::Event::v3::Tag::nDoF>().set( nDof );
        outTrack.field<LHCb::Event::v3::Tag::history>().set( tracksFT.history() );
        outTrack.field<LHCb::Event::v3::Tag::UniqueID>().set(
            decltype( outTrack.field<LHCb::Event::v3::Tag::UniqueID>().get() ){
                unique_id_gen.generate<int>().value() } );

        // Copy LHCbIds
        auto nVPHits   = track.nVPHits();
        auto VPHitsOut = outTrack.field<LHCb::Event::v3::Tag::VPHits>();
        VPHitsOut.resize( nVPHits );
        for ( int i = 0; i < nVPHits.hmax( loop_mask ); i++ ) {
          VPHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.vp_lhcbID( i ) );
        }

        auto nUTHits   = track.nUTHits();
        auto UTHitsOut = outTrack.field<LHCb::Event::v3::Tag::UTHits>();
        UTHitsOut.resize( nUTHits );
        for ( int i = 0; i < nUTHits.hmax( loop_mask ); i++ ) {
          UTHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.ut_lhcbID( i ) );
        }

        auto nFTHits   = track.nFTHits();
        auto FTHitsOut = outTrack.field<LHCb::Event::v3::Tag::FTHits>();
        FTHitsOut.resize( nFTHits );
        for ( int i = 0; i < nFTHits.hmax( loop_mask ); i++ ) {
          FTHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.ft_lhcbID( i ) );
        }

        // Closest to beam
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setPosition( stateInfo.pos().x(), stateInfo.pos().y(), stateInfo.pos().z() );
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setDirection( stateInfo.dir().x(), stateInfo.dir().y() );
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setQOverP( qop );
        outTrack
            .field<LHCb::Event::v3::Tag::StateCovs>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .set( stateInfo.covX().x(), 0, stateInfo.covX().y(), 0, 0, stateInfo.covY().x(), 0, stateInfo.covY().y(), 0,
                  stateInfo.covX().z(), 0, 0, stateInfo.covY().z(), 0, 0.1 * qop * qop );
      }

      return out;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{ this, "Nb of Produced Tracks" };
  };

  class KalmanTrackV1 : public Algorithm::Transformer<Event::v1::Tracks( const LHCb::Track::Range&, const DeVP&,
                                                                         const Conditions::InteractionRegion& ),
                                                      DetDesc::usesConditions<DeVP, Conditions::InteractionRegion>> {
  public:
    KalmanTrackV1( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       { KeyValue{ "TracksLocation", "Rec/Track/VP" }, KeyValue{ "DEVP", Det::VP::det_path },
                         KeyValue{ "InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion" } },
                       KeyValue{ "OutputTracksLocation", "Rec/Track/Fit" } ) {}

    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&]() {
        // This is only needed to have a fallback in case the IR condition does not exist. In that case, the information
        // is taken from DeVP and thus the Velo motion system which is not exactly the same.
        Conditions::InteractionRegion::addConditionDerivation( this, inputLocation<Conditions::InteractionRegion>() );
      } );
    }

    Event::v1::Tracks operator()( const LHCb::Track::Range& input_tracks, const DeVP& deVP,
                                  const LHCb::Conditions::InteractionRegion& region ) const override {
      auto out_tracks = LHCb::Event::v1::Tracks{};
      out_tracks.reserve( input_tracks.size() );

      std::vector<LHCb::LinAlg::Vec<float, 3>> hits;
      hits.reserve( 50 );

      for ( auto const* track : input_tracks ) {
        auto new_track = new Event::v1::Track( *track );

        hits.clear();
        for ( const LHCb::VPMicroCluster& cluster : new_track->vpClusters() ) {
          auto        chanId  = cluster.channelID();
          auto        fx      = static_cast<float>( cluster.xfraction() ) / 255.f;
          auto        fy      = static_cast<float>( cluster.yfraction() ) / 255.f;
          auto        ltg     = deVP.ltg( chanId.sensor() );
          float       cx      = static_cast<float>( chanId.scol() );
          float       cy      = static_cast<float>( chanId.row() );
          const float local_x = deVP.local_x( cx ) + fx * deVP.x_pitch( cx );
          const float local_y = ( 0.5f + cy + fy ) * deVP.pixel_size();
          const float gx      = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
          const float gy      = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
          const float gz      = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );
          hits.emplace_back( gx, gy, gz );
        }

        auto* state = new_track->stateAt( LHCb::State::Location::ClosestToBeam );
        auto  tx    = state->tx();
        auto  ty    = state->ty();

        if ( m_resetFirstState.value() ) {
          // Seed with a simple fit, similar to what is done in VeloClusterTracking
          const auto& p1 = hits[hits.size() - 3];
          const auto& p2 = hits[hits.size() - 1];
          auto        d  = p1 - p2;
          tx             = d.x() / d.z();
          ty             = d.y() / d.z();
        }

        LHCb::LinAlg::Vec<float, 3> dir{ tx, ty, 1.f };
        if ( new_track->isVeloBackward() ) {
          auto s = fitBackwardV1( hits, dir );
          s.transportTo( s.zBeam( region.avgPosition.x(), region.avgPosition.y() ) );
          state->setState( s.x, s.y, s.z, s.tx, s.ty, state->qOverP() );
        } else {
          auto s = fitForwardV1( hits, dir );
          s.transportTo( s.zBeam( region.avgPosition.x(), region.avgPosition.y() ) );
          state->setState( s.x, s.y, s.z, s.tx, s.ty, state->qOverP() );
        }

        out_tracks.insert( new_track );
      }

      m_nbTracksCounter += out_tracks.size();
      return out_tracks;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{ this, "Nb of Produced Tracks" };
    Gaudi::Property<bool>                         m_resetFirstState{ this, "ResetFirstState", true };
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::Kalman, "VeloKalman" )
DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::KalmanTrackV1, "VeloKalmanTrackV1" )
