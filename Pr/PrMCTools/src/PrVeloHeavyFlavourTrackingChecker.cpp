/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/RawEvent.h"
#include "Event/Track.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include "Functors/Common.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "Relations/Relation2D.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

/** @class PrVeloHeavyFlavourTrackingChecker PrVeloHeavyFlavourTrackingChecker.cpp
 *
 * For efficiency checker of VeloHeavyFlavourTrackFinder on MC
 *
 */

namespace {
  // in/outputs
  using Composite    = LHCb::Particle;
  using Composites   = LHCb::Particle::Range;
  using PVs          = LHCb::PrimaryVertices;
  using Track        = LHCb::Event::v1::Track;
  using P2TRelations = LHCb::Relation2D<LHCb::Particle, Track>;
  using MCParticles  = LHCb::MCParticles;
  using MCHits       = LHCb::MCHits;
  using Links        = LHCb::LinksByKey;

  // helper classes
  struct DecayMCParticles {
    LHCb::MCParticle const*                B        = NULL;
    LHCb::MCParticle const*                tau      = NULL;
    std::array<LHCb::MCParticle const*, 3> pions    = { NULL, NULL, NULL };
    std::array<Composite const*, 3>        recpions = { NULL, NULL, NULL };
  };

  struct WeightedHit {
    unsigned int id;
    float        weight;
    // reverse sorting (high to low)
    friend bool operator<( WeightedHit& lhs, WeightedHit& rhs ) { return lhs.weight > rhs.weight; }
  };

  // mcparticle filter
  std::optional<DecayMCParticles> has_b2taunu_tau23pions( LHCb::MCParticle const* mcp ) {
    if ( mcp->particleID().hasBottom() ) {
      auto vtxs = mcp->endVertices();
      if ( vtxs.size() ) {
        auto sv   = vtxs.at( vtxs.size() - 1 );
        auto daus = sv->products();
        if ( daus.size() >= 2 ) {
          bool   has_taup  = false;
          bool   has_ntau  = false;
          size_t n_photons = 0;
          for ( LHCb::MCParticle const* dau : daus ) {
            auto dID = dau->particleID().abspid();
            if ( dID == 15 ) has_taup = true;
            if ( dID == 16 ) has_ntau = true;
            if ( dID == 22 ) n_photons += 1;
          }
          if ( has_taup && has_ntau && ( n_photons == ( daus.size() - 2 ) ) ) {
            auto parts = DecayMCParticles();
            parts.B    = mcp;
            for ( LHCb::MCParticle const* dau : daus ) {
              if ( dau->particleID().abspid() == 15 ) parts.tau = dau;
            }
            auto tau_vtxs = parts.tau->endVertices();
            if ( tau_vtxs.size() ) {
              auto tv     = tau_vtxs.at( tau_vtxs.size() - 1 );
              int  i_pion = 0;
              for ( LHCb::MCParticle const* tdau : tv->products() ) {
                if ( tdau->particleID().abspid() == 211 ) {
                  parts.pions[i_pion] = tdau;
                  i_pion += 1;
                }
              }
            }
            return parts;
          } // getting all relevant MCParticles
        }
      }
    }
    return std::nullopt;
  }

  // check if MCParticle has match to track
  template <typename LinkMap>
  bool hasTrack( LHCb::MCParticle const* mcp, LinkMap const& map, float min_weight = 0.7 ) {
    bool has_track = false;
    map.applyToAllLinks( [&has_track, &mcp, &min_weight]( unsigned int, unsigned int mcPartKey, float weight ) {
      if ( ( (unsigned int)mcp->key() == mcPartKey ) && weight > min_weight ) has_track = true;
    } );
    return has_track;
  }

  // match MCParticle daughters (pions from tau) to Tracks (general), 'reconstructed' variable
  template <typename LinkMap>
  bool matchToTracks( DecayMCParticles const& mcps, LinkMap const& map, MCParticles const& mcparts,
                      float min_weight = 0.7 ) {
    size_t nmatches = 0;
    for ( LHCb::MCParticle const* pion : mcps.pions ) {
      map.applyToAllLinks(
          [&pion, &mcparts, &min_weight, &nmatches]( unsigned int, unsigned int mcPartKey, float weight ) {
            if ( ( pion == static_cast<LHCb::MCParticle const*>( mcparts.containedObject( mcPartKey ) ) ) &&
                 ( weight > min_weight ) )
              nmatches += 1;
          } );
    }
    return nmatches == mcps.pions.size();
  }

  // match MCParticle to Composites
  template <typename LinkMap>
  std::optional<Composite const*> matchToComposites( DecayMCParticles& mcps, Composites const& composites,
                                                     LinkMap const& map, MCParticles const& mcparts,
                                                     float min_weight = 0.7 ) {
    for ( Composite const* comp : composites ) {
      size_t nmatches = 0;
      for ( Composite const* dau : comp->daughtersVector() ) {
        if ( !dau->proto() ) continue;
        for ( int i = 0; i < (int)mcps.pions.size(); i++ ) {
          LHCb::MCParticle const* pion = mcps.pions[i];
          map.applyToLinks( dau->proto()->track()->key(), [&i, &dau, &mcps, &nmatches, &mcparts, &pion, &min_weight](
                                                              unsigned int, unsigned int mcPartKey, float weight ) {
            if ( ( pion == static_cast<LHCb::MCParticle const*>( mcparts.containedObject( mcPartKey ) ) ) &&
                 ( weight >= min_weight ) ) {
              mcps.recpions[i] = dau;
              nmatches += 1;
            }
          } );
        }
      }
      // assuming there is only one matching composite
      if ( nmatches == mcps.pions.size() ) return comp;
    }
    return std::nullopt;
  }

  // match MCParticles to MCHits
  auto getMCHits( DecayMCParticles const& mcps, MCHits const& mchits ) {
    std::vector<LHCb::MCHit const*> hits;
    // which ones come from B or tau?
    for ( LHCb::MCHit const* mchit : mchits ) {
      auto hitmcp = mchit->mcParticle();
      if ( hitmcp == mcps.B || hitmcp == mcps.tau ) hits.push_back( mchit );
    }
    // order them in z
    auto ordering = []( LHCb::MCHit const* i1, LHCb::MCHit const* i2 ) { return i1->entry().z() < i2->entry().z(); };
    if ( hits.size() > 1 ) std::sort( hits.begin(), hits.end(), ordering );
    return hits;
  }

  // composite observables
  float getMcorr( Composite const* composite, Gaudi::XYZVector const& direction ) {
    auto mom   = composite->momentum().Vect();
    auto mass2 = composite->momentum().mass2();
    auto pt    = ( mom - direction * mom.Dot( direction ) ).r();
    return pt + std::sqrt( mass2 + pt * pt );
  }

} // namespace

class PrVeloHeavyFlavourTrackingChecker
    : public Gaudi::Functional::Consumer<void( MCParticles const&, PVs const&, MCHits const&, Links const&,
                                               Links const& ),
                                         LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg>> {
public:
  using base_type =
      Gaudi::Functional::Consumer<void( MCParticles const&, PVs const&, MCHits const&, Links const&, Links const& ),
                                  LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg>>;
  using KeyValue = base_type::KeyValue;

  // standard constructor
  PrVeloHeavyFlavourTrackingChecker( std::string const& name, ISvcLocator* pSvc )
      : base_type{ name,
                   pSvc,
                   { KeyValue{ "MCParticles", LHCb::MCParticleLocation::Default }, KeyValue{ "PVs", "" },
                     KeyValue{ "MCHits", "/Event/MC/VP/Hits" }, KeyValue{ "VPLinks", "" },
                     KeyValue{ "TrackLinks", "" } } } {}

  // main execution
  void operator()( MCParticles const&, PVs const&, MCHits const&, Links const&, Links const& ) const override;

private:
  // data that might not be there
  DataObjectReadHandle<Composites>   m_composites{ this, "Composites", "" };
  DataObjectReadHandle<P2TRelations> m_relations{ this, "Composite2HeavyFlavourTrackRelations", "" };

  // monitoring
  mutable Gaudi::Accumulators::BinomialCounter<> m_hftrack_global_eff{ this,
                                                                       "Eff: has HF track | sel'd FS & HF reco'ible" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_hftrack_pure_eff{ this,
                                                                     "HF track: MC-matched | sel'd FS & HF reco'ible" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_reconstructible_hf_track{ this, "HF track: reco'ible" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_reconstructed_hf_track{ this, "HF track: reco'd | reco'ible" };

  mutable Gaudi::Accumulators::StatCounter<int>  m_hftrack_nhits{ this, "HF track: #hits | sel'd FS & HF track" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_hftrack_purity{ this, "HF track: hit purity | sel'd FS & HF track" };

  mutable Gaudi::Accumulators::BinomialCounter<> m_vphit_for_mchit_eff{ this, "Eff: VP hit for VP MCHit" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_hit_on_track_for_vphit_with_mchit_eff{
      this, "Eff: true hit on track | sel'd FS & HF track" };

  mutable Gaudi::Accumulators::BinomialCounter<> m_reconstructed_eff{ this, "Final state (FS): reco'd" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_final_state_eff{ this, "Final state (FS): sel'd | reco'd" };
};

DECLARE_COMPONENT_WITH_ID( PrVeloHeavyFlavourTrackingChecker, "PrVeloHeavyFlavourTrackingChecker" )

//// implementation /////

// main execution
void PrVeloHeavyFlavourTrackingChecker::operator()( MCParticles const& mcparts, PVs const& pvs, MCHits const& mchits,
                                                    Links const& vplinks, Links const& tracklinks ) const {
  // setup ntuple
  auto tuple = this->nTuple( "MCParticleTuple" );

  // table linking a LHCb::MCHit* to std::vector<LHCb::Detector::VPChannelID>> and vice versa
  std::map<LHCb::MCHit const*, std::vector<WeightedHit>> channelIDForMCHit;
  vplinks.applyToAllLinks(
      [&channelIDForMCHit, &mchits]( unsigned int channelID, unsigned int mcHitKey, float weight ) {
        channelIDForMCHit[mchits[mcHitKey]].emplace_back( WeightedHit{ channelID, weight } );
      } );

  // fill for all tracks
  for ( auto const mcp : mcparts ) {
    if ( !mcp ) continue;
    // check if it is the right MCParticle
    auto check_mcps = has_b2taunu_tau23pions( mcp );
    if ( !check_mcps.has_value() ) continue;
    auto mcps = check_mcps.value();
    // get MCHits for the candidate
    auto bhits       = getMCHits( mcps, mchits );
    auto first_mchit = bhits.size() ? bhits.front()->midPoint() : Gaudi::XYZPoint( 0., 0., 0. );
    // look at long tracks and see if pions match to all (as in 'reconstructed')
    auto reconstructed = matchToTracks( mcps, tracklinks, mcparts, 0.7 );
    // link MCParticle to daughters (pions) of composite
    auto composites = m_composites.get();
    auto composite  = matchToComposites( mcps, composites, tracklinks, mcparts, 0.7 );
    auto has_comp   = composite.has_value();
    // get heavy flavour track
    auto const*  relations = m_relations.getIfExists();
    Track const* track     = nullptr;
    if ( has_comp && relations ) {
      auto trackrange = relations->relations( composite.value() );
      if ( trackrange.size() ) track = trackrange.front();
    }
    auto has_track = track ? true : false;
    int  n_hits    = has_track ? track->nHits() : 0;
    auto has_hits  = n_hits > 0;

    //// counters

    // efficiencies of final state reconstruction and selection
    m_reconstructed_eff += reconstructed;
    if ( reconstructed ) m_final_state_eff += has_comp;

    // check MCHits associated to B+/tau+ if they are reconstructible
    // and if so look if they appear on the reconstructed track in case
    // the final state is reconstructed, selected and matched and has associated HF track
    bool has_reconstructible_hf_track = false;
    bool has_reconstructed_hf_track   = false;
    for ( auto const bhit : bhits ) {
      auto ids_for_mchit       = channelIDForMCHit.find( bhit );
      auto has_vphit_for_mchit = ids_for_mchit != channelIDForMCHit.end();
      m_vphit_for_mchit_eff += has_vphit_for_mchit;
      if ( !has_vphit_for_mchit ) continue;
      has_reconstructible_hf_track = true;
      auto ids                     = ids_for_mchit->second;
      int  n_matches               = 0;
      if ( has_hits ) {
        auto track_lhcbids = track->lhcbIDs();
        n_matches          = std::count_if( ids.begin(), ids.end(), [&track_lhcbids]( auto whit ) {
          return std::count_if( track_lhcbids.begin(), track_lhcbids.end(),
                                         [&whit]( auto lhcbid ) { return lhcbid.vpID() == whit.id; } ) > 0;
        } );
      }
      if ( reconstructed && has_comp && has_hits ) {
        has_reconstructed_hf_track |= n_matches > 0;
        m_hit_on_track_for_vphit_with_mchit_eff += n_matches > 0;
      }
    }

    float track_purity = -1.f;
    if ( reconstructed && has_comp && has_hits ) {
      int n_matched_hits = 0;
      for ( auto const lhcbid : track->lhcbIDs() ) {
        unsigned int channelid = lhcbid.vpID();
        int          n_match_per_hit =
            bhits.empty() ? 0
                                   : std::count_if( bhits.begin(), bhits.end(), [&channelIDForMCHit, &channelid]( auto mchit ) {
                              auto map_iter = channelIDForMCHit.find( mchit );
                              int  n_match_per_hit_per_mchit =
                                  ( map_iter == channelIDForMCHit.end() )
                                                ? 0
                                                : std::count_if( map_iter->second.begin(), map_iter->second.end(),
                                                                 [&channelid]( auto whit ) { return whit.id == channelid; } );
                              return n_match_per_hit_per_mchit > 0;
                            } );
        if ( n_match_per_hit > 0 ) n_matched_hits++;
        m_hftrack_purity += n_match_per_hit > 0;
      }
      track_purity = n_matched_hits / (float)n_hits;
      m_hftrack_nhits += n_hits;
    }

    m_reconstructible_hf_track += has_reconstructible_hf_track;
    if ( has_reconstructible_hf_track ) {
      m_reconstructed_hf_track += has_reconstructed_hf_track;
      if ( reconstructed && has_comp ) {
        m_hftrack_global_eff += has_hits;
        m_hftrack_pure_eff += has_hits && track_purity >= 0.7;
      }
    }

    //// fill ntuple with info
    std::string base = "MCP_";
    // mc truth
    auto sc = tuple->column( base + "TRUEID", mcp->particleID().pid() );
    sc &= tuple->column( base + "P", mcp->momentum().P() );
    sc &= tuple->column( base + "PT", mcp->momentum().Pt() );
    sc &= tuple->column( base + "eta", mcp->momentum().Eta() );
    sc &= tuple->column( base + "phi", mcp->momentum().Phi() );
    sc &= tuple->column( base + "tx", mcp->momentum().Px() / mcp->momentum().Pz() );
    sc &= tuple->column( base + "ty", mcp->momentum().Py() / mcp->momentum().Pz() );
    auto ovtx = mcps.B->originVertex()->position();
    sc &= tuple->column( base + "OriginVertex_x", ovtx.x() );
    sc &= tuple->column( base + "OriginVertex_y", ovtx.y() );
    sc &= tuple->column( base + "OriginVertex_z", ovtx.z() );
    auto bvtx = mcps.B->endVertices().size() ? mcps.B->endVertices().at( mcps.B->endVertices().size() - 1 )->position()
                                             : Gaudi::XYZPoint( 0., 0., -100 * Gaudi::Units::m );
    sc &= tuple->column( base + "EndVertex_x", bvtx.x() );
    sc &= tuple->column( base + "EndVertex_y", bvtx.y() );
    sc &= tuple->column( base + "EndVertex_z", bvtx.z() );
    auto tvtx = mcps.tau->endVertices().size()
                    ? mcps.tau->endVertices().at( mcps.tau->endVertices().size() - 1 )->position()
                    : Gaudi::XYZPoint( 0., 0., -110 * Gaudi::Units::m );
    sc &= tuple->column( base + "Tau_EndVertex_x", tvtx.x() );
    sc &= tuple->column( base + "Tau_EndVertex_y", tvtx.y() );
    sc &= tuple->column( base + "Tau_EndVertex_z", tvtx.z() );
    auto pionsmom = mcps.pions[0]->momentum() + mcps.pions[1]->momentum() + mcps.pions[2]->momentum();
    sc &= tuple->column( base + "Pions_P", pionsmom.P() );
    sc &= tuple->column( base + "Pions_PT", pionsmom.Pt() );
    sc &= tuple->column( base + "Pions_Mass", pionsmom.mass() );
    for ( int i = 0; i < (int)mcps.pions.size(); i++ ) {
      std::string pbase = "Pion_" + std::to_string( i ) + "_";
      auto        pion  = mcps.pions[i];
      sc &= tuple->column( base + pbase + "TRUEID", pion->particleID().pid() );
      sc &= tuple->column( base + pbase + "P", pion->momentum().P() );
      sc &= tuple->column( base + pbase + "PT", pion->momentum().Pt() );
      sc &= tuple->column( base + pbase + "has_Track", hasTrack( pion, tracklinks ) );
    }
    // mc hits
    sc &= tuple->column( base + "has_VP_MCHits", (bool)bhits.size() );
    sc &= tuple->column( base + "VP_first_MCHit_x", first_mchit.x() );
    sc &= tuple->column( base + "VP_first_MCHit_y", first_mchit.y() );
    sc &= tuple->column( base + "VP_first_MCHit_z", first_mchit.z() );
    // associated VP hits
    std::vector<WeightedHit> hits_for_mchit =
        bhits.size() ? channelIDForMCHit[bhits.front()] : std::vector<WeightedHit>();
    if ( hits_for_mchit.size() > 1 ) std::sort( hits_for_mchit.begin(), hits_for_mchit.end() );
    sc &= tuple->column( base + "VP_first_MCHit_has_VPHit", hits_for_mchit.size() > 0 );
    sc &=
        tuple->column( base + "VP_first_MCHit_VPHit_ChannelID", hits_for_mchit.size() ? hits_for_mchit.front().id : 0 );
    // composite info
    sc &= tuple->column( base + "reconstructed", reconstructed );
    sc &= tuple->column( base + "has_Composite", has_comp );
    std::string compbase = base + "Composite_";
    auto        endvtx   = has_comp ? composite.value()->endVertex()->position() : Gaudi::XYZPoint( 0., 0., 0. );
    float       vtxchi2  = has_comp ? composite.value()->endVertex()->chi2() : -1.f;
    sc &= tuple->column( compbase + "EndVertex_x", endvtx.x() );
    sc &= tuple->column( compbase + "EndVertex_y", endvtx.y() );
    sc &= tuple->column( compbase + "EndVertex_z", endvtx.z() );
    sc &= tuple->column( compbase + "EndVertex_chi2", vtxchi2 );
    auto compmom =
        has_comp ? composite.value()->momentum() : Gaudi::LorentzVector( 0., 0., 0., -1 * Gaudi::Units::GeV );
    sc &= tuple->column( compbase + "P", compmom.R() );
    sc &= tuple->column( compbase + "PT", compmom.Pt() );
    sc &= tuple->column( compbase + "eta", compmom.Eta() );
    sc &= tuple->column( compbase + "phi", compmom.Phi() );
    sc &= tuple->column( compbase + "Mass", compmom.mass() );
    // composite daughters info
    using Functors::Common::ImpactParameter;
    using Functors::Common::Position;
    using Functors::Common::ToLinAlg;
    using Sel::Utils::impactParameterChi2;
    for ( int i = 0; i < (int)mcps.pions.size(); i++ ) {
      std::string pbase  = "Pion_" + std::to_string( i ) + "_";
      auto        pion   = mcps.recpions[i];
      float       ip     = -1.f;
      float       ipchi2 = -1.f;
      if ( pion && has_track ) {
        auto pv = LHCb::bestPV( pvs, *pion );
        ip      = ImpactParameter( ToLinAlg( Position( pv ) ), pion ).cast();
        ipchi2  = impactParameterChi2( pion, pv ).cast();
      }
      sc &= tuple->column( compbase + pbase + "match", (bool)pion );
      sc &= tuple->column( compbase + pbase + "P", pion ? pion->momentum().R() : 0. );
      sc &= tuple->column( compbase + pbase + "PT", pion ? pion->momentum().Pt() : 0. );
      sc &= tuple->column( compbase + pbase + "IP", ip );
      sc &= tuple->column( compbase + pbase + "IPChi2", ipchi2 );
      sc &= tuple->column(
          compbase + pbase + "PIDK",
          pion ? ( pion->proto()->globalChargedPID() ? pion->proto()->globalChargedPID()->CombDLLk() : 0. ) : 0. );
    }
    // track info
    std::string hfbase = base + "HeavyFlavourTracking_";
    sc &= tuple->column( hfbase + "Track_nSensors",
                         has_track ? track->info( LHCb::Event::Enum::Track::AdditionalInfo::nPRVelo3DExpect, -1. )
                                   : -2. );
    sc &= tuple->column( hfbase + "Track_nHits", has_track ? track->nHits() : -1 );
    // track mc matching info
    bool fh_from_mcps = false;
    int  fh_trueid    = 0;
    if ( has_hits ) {
      float              max_weight = 0.f;
      LHCb::MCHit const* mchit      = nullptr;
      unsigned int       hid        = track->lhcbIDs().front().vpID();
      vplinks.applyToLinks( hid, [&max_weight, &mchits, &mchit]( unsigned int, unsigned int mcHitKey, float weight ) {
        if ( weight > max_weight ) {
          max_weight = weight;
          mchit      = static_cast<LHCb::MCHit const*>( mchits.containedObject( mcHitKey ) );
        }
      } );
      if ( mchit ) {
        fh_trueid    = mchit->mcParticle()->particleID().pid();
        fh_from_mcps = ( mchit->mcParticle() == mcps.B ) || ( mchit->mcParticle() == mcps.tau );
      }
    }
    decltype( track->position() ) fh_pos( 0., 0., 0. );
    LHCb::State const* fh_state = track ? track->stateAt( LHCb::State::Location::FirstMeasurement ) : nullptr;
    if ( fh_state ) fh_pos = fh_state->position();
    sc &= tuple->column( hfbase + "Track_first_hit_x", fh_pos.x() );
    sc &= tuple->column( hfbase + "Track_first_hit_y", fh_pos.y() );
    sc &= tuple->column( hfbase + "Track_first_hit_z", fh_pos.z() );
    sc &= tuple->column( hfbase + "Track_first_hit_LHCbID",
                         has_hits ? (unsigned int)track->lhcbIDs().front().vpID() : 0 );
    sc &= tuple->column( hfbase + "Track_first_hit_from_MCPs", fh_from_mcps );
    sc &= tuple->column( hfbase + "Track_first_hit_MC_TRUEID", fh_trueid );
    // more advanced variables for analysis
    auto  truedir    = mcps.B->momentum().Vect().Unit();
    auto  mcorr_true = has_comp ? getMcorr( composite.value(), truedir ) : 0.f;
    float mcorr_fh   = 0.f;
    float mcorr_ps   = 0.f;
    auto  slopes     = Gaudi::XYZVector( 0., 0., 1. );
    auto  pvpos      = Gaudi::XYZPoint( 0., 0., -10 * Gaudi::Units::m );
    if ( has_track ) {
      auto state = track->firstState();
      slopes.SetXYZ( state.tx(), state.ty(), 1. );
      auto fhdir = slopes.Unit();
      pvpos.SetXYZ( state.x(), state.y(), state.z() );
      auto pvsv = ( composite.value()->endVertex()->position() - pvpos ).Unit();
      mcorr_fh  = getMcorr( composite.value(), fhdir );
      mcorr_ps  = getMcorr( composite.value(), pvsv );
    }
    sc &= tuple->column( hfbase + "tx", slopes.x() );
    sc &= tuple->column( hfbase + "ty", slopes.y() );
    sc &= tuple->column( hfbase + "true_dir_mcorr", mcorr_true );
    sc &= tuple->column( hfbase + "first_hit_mcorr", mcorr_fh );
    sc &= tuple->column( hfbase + "pvsv_mcorr", mcorr_ps );
    sc &= tuple->column( hfbase + "PV_x", pvpos.x() );
    sc &= tuple->column( hfbase + "PV_y", pvpos.y() );
    sc &= tuple->column( hfbase + "PV_z", pvpos.z() );
    // write result
    sc.andThen( [&] { return tuple->write(); } ).ignore();
  }
}
