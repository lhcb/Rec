/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// #include "Event/FTLiteCluster.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "Event/TrackTags.h"
#include "Event/VPCluster.h"
#include "LHCbAlgs/Transformer.h"
#include "MCInterfaces/IIdealStateCreator.h"

#include "PrKernel/PrChecker.h"

using Track = LHCb::Event::v2::Track;

/** @class PrCheatedVPTracking
 *
 *  Ideal pattern recognition for VP tracks, in v2. Uses the links created by the
 *  PrLHCbID2MCParticle algorithm to construct tracks consisting only out of LHCbIDs
 *  associated to the MCParticle.
 *
 *  Hits in the VP are added, and a possibility exists to add hits in the UT
 *  as well, given a certain threshold (e.g. at least 4 hits).

 *  @author Laurent Dufour
 */
class PrCheatedVPTracking
    : public LHCb::Algorithm::Transformer<std::vector<Track>( const LHCb::MCParticles&, const LHCb::MCVertices&,
                                                              const LHCb::MCProperty&, const LHCb::LinksByKey& )> {
public:
  /// Standard constructor
  PrCheatedVPTracking( const std::string& name, ISvcLocator* pSvcLocator );

  /// make cheated tracks by getting the clusters matched to an MCParticle
  std::vector<Track> operator()( const LHCb::MCParticles&, const LHCb::MCVertices&, const LHCb::MCProperty&,
                                 const LHCb::LinksByKey& ) const override;

private:
  // Minimum number of VP clusters to be found for the track to be created
  Gaudi::Property<size_t>        m_minVPHits        = { this, "MinVPHits", 3 };
  Gaudi::Property<bool>          m_add_ideal_states = { this, "AddIdealStates", false };
  ToolHandle<IIdealStateCreator> m_ideal_state_creator{ this, "IdealStateCreator", "IdealStateCreator" };
};

DECLARE_COMPONENT( PrCheatedVPTracking )

PrCheatedVPTracking::PrCheatedVPTracking( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {
                       KeyValue{ "MCParticleLocation", LHCb::MCParticleLocation::Default },
                       KeyValue{ "MCVerticesLocation", LHCb::MCVertexLocation::Default },
                       KeyValue{ "MCPropertyLocation", LHCb::MCPropertyLocation::TrackInfo },
                       KeyValue{ "LHCbIdLinkLocation", "Link/Pr/LHCbID" },
                   },
                   KeyValue{ "OutputName", "Rec/Track/CheatedVP" } ) {}

std::vector<Track> PrCheatedVPTracking::operator()( const LHCb::MCParticles& mcParts, const LHCb::MCVertices&,
                                                    const LHCb::MCProperty&  mcProps,
                                                    const LHCb::LinksByKey&  links ) const {
  std::vector<Track> result;
  MCTrackInfo        trackInfo( mcProps );

  for ( const LHCb::MCParticle* mcPart : mcParts ) {
    if ( LHCb::Pr::Checker::reconstructibleType( mcPart, LHCb::Pr::Checker::RecAs::isNotVelo, trackInfo ).value() )
      continue;

    Track newTrack;
    newTrack.setType( Track::Type::Velo );
    newTrack.setHistory( Track::History::TrackIdealPR );
    newTrack.setPatRecStatus( Track::PatRecStatus::PatRecIDs );
    size_t nVPHits = 0;

    // add the VP hits & UT hits
    links.applyToAllLinks(
        [&mcParts, &mcPart, &newTrack, &nVPHits]( unsigned int srcKey, unsigned int mcPartKey, float ) {
          const LHCb::MCParticle* linkedMCPart =
              static_cast<const LHCb::MCParticle*>( mcParts.containedObject( mcPartKey ) );
          LHCb::LHCbID theId( srcKey );

          if ( mcPart == linkedMCPart ) {
            if ( theId.isVP() ) {
              newTrack.addToLhcbIDs( theId );
              ++nVPHits;
            }
          }
        } );

    if ( nVPHits < m_minVPHits.value() ) continue;

    const double qOverP = ( mcPart->particleID().threeCharge() / 3 ) / mcPart->p();
    const double x      = mcPart->originVertex()->position().X();
    const double y      = mcPart->originVertex()->position().Y();
    const double z      = mcPart->originVertex()->position().Z();
    const double tx     = mcPart->momentum().X() / mcPart->momentum().Z();
    const double ty     = mcPart->momentum().Y() / mcPart->momentum().Z();

    LHCb::State stateClosestToBeam;
    stateClosestToBeam.setLocation( LHCb::State::Location::ClosestToBeam );
    // stateClosestToBeam.setLocation( LHCb::State::Location::FirstMeasurement);
    stateClosestToBeam.setState( x, y, z, tx, ty, qOverP );
    newTrack.addToStates( stateClosestToBeam );

    if ( m_add_ideal_states ) {
      std::vector<LHCb::State> newstates{};
      newstates.reserve( 30 );
      m_ideal_state_creator->getMCHitStates( *mcPart, newstates ).ignore();
      newTrack.addToStates( newstates, LHCb::Tag::Unordered_tag{} );
    }
    result.emplace_back( newTrack );
  }

  return result;
}
