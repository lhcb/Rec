/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/PrHits.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/TrackEnums.h"
#include "Event/ZipUtils.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDAQ/UTInfo.h"

#include "PrKernel/PrChecker.h"

#include <cstddef>

namespace LHCb::Pr {

  namespace {
    using Event::Enum::Track::Type;
    constexpr auto nUTLayers = static_cast<std::size_t>( UTInfo::DetectorNumbers::TotalLayers );

    struct Hit {
      int    index{};
      LHCbID lhcbid{};
    };
  } // namespace

  /**
   * @brief This class adds the MC-linked UT hits to a reconstructed Velo track to produce a cheated
   * Upstream track. Hence, PrChecker efficiencies for these Upstream tracks are not 100% as they depend
   * on the Velo tracking and a Velo track must have been matched to a MC particle.
   */
  class PrCheatedUpstreamTracking
      : public Algorithm::Transformer<Upstream::Tracks( const Velo::Tracks&, const UT::Hits&, const MCParticles&,
                                                        const MCProperty&, const LinksByKey&, const LinksByKey& )> {
  public:
    PrCheatedUpstreamTracking( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       { KeyValue{ "InputTracksName", "" }, KeyValue{ "UTHits", "" }, KeyValue{ "MCParticles", "" },
                         KeyValue{ "MCProperty", "" }, KeyValue{ "LHCbIDLinks", "" },
                         KeyValue{ "VeloTrackLinks", "" } },
                       KeyValue{ "OutputName", "" } ){};

    Upstream::Tracks operator()( const Velo::Tracks& veloTracks, const UT::Hits& hits, const MCParticles& particles,
                                 const MCProperty& props, const LinksByKey& hitLinks,
                                 const LinksByKey& veloLinks ) const override {
      auto cheatedTracks =
          Upstream::Tracks{ &veloTracks, Zipping::generateZipIdentifier(), veloTracks.get_allocator().resource() };
      cheatedTracks.reserve( veloTracks.size() );
      const auto mcinfo    = MCTrackInfo{ props };
      auto       trackHits = std::vector<Hit>{};
      trackHits.reserve( TracksInfo::MaxUTHits );
      for ( const auto* mcp : particles ) {
        if ( !mcinfo.fullInfo( mcp ) || !Checker::reconstructibleType( mcp, Type::Upstream, mcinfo ).value() ) continue;

        trackHits.clear();
        for ( auto hit : hits.scalar() ) {
          hitLinks.applyToLinks( hits.lhcbid( hit.offset() ).lhcbID(), [&]( auto id, auto mcKey, auto /*weight*/ ) {
            if ( particles( mcKey ) == mcp ) trackHits.emplace_back( Hit{ hit.offset(), LHCbID{ id } } );
          } );
        }

        auto veloTrackIndex = -1;
        veloLinks.applyToAllLinks( [&]( auto veloIndex, auto mcKey, auto /*weight*/ ) {
          if ( particles( mcKey ) == mcp ) veloTrackIndex = veloIndex;
        } );
        if ( veloTrackIndex < 0 ) continue;

        const auto veloTrack = veloTracks.scalar()[veloTrackIndex];
        auto       outTrack  = cheatedTracks.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        outTrack.field<Upstream::Tag::trackVP>().set( veloTrackIndex );
        outTrack.field<Upstream::Tag::State>().setQOverP( static_cast<float>( mcp->particleID().threeCharge() / 3 ) /
                                                          mcp->p() );
        // TODO: this is filled with the EndVelo state only because it's convenient. Should be the state MidUT.
        outTrack.field<Upstream::Tag::State>().setPosition(
            veloTrack.StatePos( Event::Enum::State::Location::EndVelo ) );
        outTrack.field<Upstream::Tag::State>().setDirection(
            veloTrack.StateDir( Event::Enum::State::Location::EndVelo ) );

        const auto nVeloHits = veloTrack.nHits().cast();
        outTrack.field<Upstream::Tag::VPHits>().resize( nVeloHits );
        for ( auto idx = 0; idx < nVeloHits; ++idx ) {
          outTrack.field<Upstream::Tag::VPHits>()[idx].field<Upstream::Tag::Index>().set( veloTrack.vp_index( idx ) );
          outTrack.field<Upstream::Tag::VPHits>()[idx].field<Upstream::Tag::LHCbID>().set( veloTrack.vp_lhcbID( idx ) );
        }

        // The Kalman filter expects a limited amount of tracks, i.e. max 8: one for each layer plus overlap
        // the simulation in rare cases links more than 8 hits to a MC particle so need to restrict it here
        auto outHits = outTrack.field<Upstream::Tag::UTHits>();
        outHits.resize( Upstream::Tracks::MaxUTHits );
        auto nUTHits = std::array<std::size_t, nUTLayers>{};
        auto n       = 0;
        for ( auto hit : trackHits ) {
          const auto layer = hits.id( hit.index ).layer();
          if ( nUTHits[layer] >= Upstream::Tracks::MaxUTHits / nUTLayers ) continue;
          outHits[n].field<Upstream::Tag::Index>().set( hit.index );
          outHits[n].field<Upstream::Tag::LHCbID>().set(
              LHCb::Event::lhcbid_v<SIMDWrapper::scalar::types>( hit.lhcbid ) );
          ++nUTHits[layer];
          ++n;
        }
        outHits.resize( n );
      }
      return cheatedTracks;
    }
  };

  DECLARE_COMPONENT_WITH_ID( PrCheatedUpstreamTracking, "PrCheatedUpstreamTracking" )

} // namespace LHCb::Pr
