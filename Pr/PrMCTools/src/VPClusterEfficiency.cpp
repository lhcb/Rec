/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/RawBank.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include "LHCbAlgs/Consumer.h"
#include "VPDet/DeVP.h"

#include "Gaudi/Accumulators/HistogramArray.h"
#include "Gaudi/Accumulators/StaticHistogram.h"

#include <cmath>

/**
 * Checks the VPCluster efficiency for simulation
 */
class VPClusterEfficiency
    : public LHCb::Algorithm::Consumer<void( const LHCb::RawBank::View&, const std::vector<LHCb::VPFullCluster>&,
                                             const LHCb::MCHits&, const LHCb::MCParticles&, const LHCb::LinksByKey&,
                                             const LHCb::MCProperty&, const DeVP& ),
                                       LHCb::Algorithm::Traits::usesConditions<DeVP>> {
public:
  VPClusterEfficiency( const std::string& name, ISvcLocator* pSvcLocator );

  /// Consumer operator: takes VP clusters & MCHits to make efficiency plots
  void operator()( const LHCb::RawBank::View&, const std::vector<LHCb::VPFullCluster>&, const LHCb::MCHits&,
                   const LHCb::MCParticles&, const LHCb::LinksByKey&, const LHCb::MCProperty&,
                   const DeVP& ) const override;

private:
  mutable Gaudi::Accumulators::SigmaCounter<>    m_num_clusters{ this, "# clusters per event" };
  mutable Gaudi::Accumulators::SigmaCounter<>    m_num_pix_clu{ this, "# pixels per cluster" };
  mutable Gaudi::Accumulators::SigmaCounter<>    m_num_pix_hit{ this, "# pixels per MCHit" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_efficiency{ this, "Cluster Efficiency" };
  mutable Gaudi::Accumulators::SigmaCounter<>    m_residual_x{ this, "Residuals x [mm]" };
  mutable Gaudi::Accumulators::SigmaCounter<>    m_residual_y{ this, "Residuals y [mm]" };
  mutable Gaudi::Accumulators::SigmaCounter<>    m_purity{ this, "Purity" };

  // MCHit related histograms
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 52> m_mcHitXYPerModule{
      this,
      []( int n ) { return fmt::format( "All MCHit x,y VP module {}", n ); },
      []( int n ) { return fmt::format( "All MCHit x,y VP module {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_mcHitXY{
      this, "All MCHit x,y VP all modules", "All MCHit x,y VP all modules", { 240, -60, 60 }, { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_mcHitDistTravel{ this,
                                                                     "Distance travelled by MCHit in VP sensor (mm)",
                                                                     "Distance travelled by MCHit in VP sensor (mm)",
                                                                     { 100, 0, 1 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_mcHitEnergyDeposit{ this,
                                                                        "Energy depotisted by MCHit in VP sensor (MeV)",
                                                                        "Energy depotisted by MCHit in VP sensor (MeV)",
                                                                        { 100, 0, 1 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticProfileHistogram<2>, 52> m_mcHitEffXYPerModule{
      this,
      []( int n ) { return fmt::format( "Efficiency MCHit -> Pixel x,y VP module {}", n ); },
      []( int n ) { return fmt::format( "Efficiency MCHit -> Pixel x,y VP module {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticProfileHistogram<2> m_mcHitEffXY{ this,
                                                                       "Efficiency MCHit -> Pixel x,y VP all modules",
                                                                       "Efficiency MCHit -> Pixel x,y VP all modules",
                                                                       { 240, -60, 60 },
                                                                       { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticProfileHistogram<1> m_mcHitEffvDist{
      this,
      "Efficiency (MCHit->Pixel) v distance travelled by MCHit in VP sensor (mm)",
      "Efficiency (MCHit->Pixel) v distance travelled by MCHit in VP sensor (mm)",
      { 100, 0, 1 } };
  mutable Gaudi::Accumulators::StaticProfileHistogram<1> m_mcHitEffvEnergy{
      this,
      "Efficiency (MCHit->Pixel) v energy depotisted by MCHit in VP sensor (MeV)",
      "Efficiency (MCHit->Pixel) v energy depotisted by MCHit in VP sensor (MeV)",
      { 100, 0, 1 } };

  // Cluster related histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nHitsPerPixel{
      this, "Number of MCHits per VP pixel", "Number of MCHits per VP pixel", { 10, -0.5, 9.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nMCParticlesPerPixel{
      this, "Number of MCParticles per VP pixel", "Number of MCParticles per VP pixel", { 10, -0.5, 9.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_nMCParticlesPerPixelModule{
      this,
      "Number of MCParticles per VP Pixel v module",
      "Number of MCParticles per VP Pixel v module",
      { 10, -0.5, 9.5 },
      { 52, -0.5, 51.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nParticlesPerCluster{
      this, "Number of MCParticles per VP Cluster", "Number of MCParticles per VP Cluster", { 10, -0.5, 9.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_nParticlesPerClusterModule{
      this,
      "Number of MCParticles per VP Cluster v module",
      "Number of MCParticles per VP Cluster v module",
      { 10, -0.5, 9.5 },
      { 52, -0.5, 51.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_spillPerCluster{ this,
                                                                     "Fraction pixels spill or noise per cluster",
                                                                     "Fraction pixels spill or noise per cluster",
                                                                     { 26, 0., 1.04 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_spillPerClusterSize{
      this,
      "Fraction pixels spill or noise per cluster v cluster size",
      "Fraction pixels spill or noise per cluster v cluster size",
      { 26, 0., 1.04 },
      { 50, 0.5, 50.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                                          m_goodCluterPos{ this,
                                                                   "Good (>70% true) Clusters pos all modules",
                                                                   "Good (>70% true) Clusters pos all modules",
                                                                                                            { 240, -60, 60 },
                                                                                                            { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 52> m_goodCluterPosPerModule{
      this,
      []( int n ) { return fmt::format( "Good (>70% true) Clusters pos module {}", n ); },
      []( int n ) { return fmt::format( "Good (>70% true) Clusters pos module {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                                          m_badCluterPos{ this,
                                                                  "Bad (<70% true) Clusters pos all modules",
                                                                  "Bad (<70% true) Clusters pos all modules",
                                                                                                           { 240, -60, 60 },
                                                                                                           { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 52> m_badCluterPosPerModule{
      this,
      []( int n ) { return fmt::format( "Bad (<70% true) Clusters pos module {}", n ); },
      []( int n ) { return fmt::format( "Bad (<70% true) Clusters pos module {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_pixelXY{
      this, "Pixel xy all sensors", "Pixel xy all sensors", { 240, -60, 60 }, { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 52> m_pixelXYPerModule{
      this,
      []( int n ) { return fmt::format( "Pixel xy sensor {}", n ); },
      []( int n ) { return fmt::format( "Pixel xy sensor {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<1>                                          m_nPixelNotUsed{ this,
                                                                   "Number of pixels not in clusters per event",
                                                                   "Number of pixels not in clusters per event",
                                                                                                            { 100, 0., 10000. } };
  mutable Gaudi::Accumulators::StaticHistogram<1>                                          m_fracPixelNotInEvent{ this,
                                                                         "Fraction of pixels not in clusters per event",
                                                                         "Fraction of pixels not in clusters per event",
                                                                                                                  { 100, 0., 1. } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                                          m_pixelNotinCluster{ this,
                                                                       "Pixel not in cluster xy all sensors",
                                                                       "Pixel not in cluster xy all sensors",
                                                                                                                { 240, -60, 60 },
                                                                                                                { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 52> m_pixelNotinClusterPerModule{
      this,
      []( int n ) { return fmt::format( "Pixel not in cluster xy sensor {}", n ); },
      []( int n ) { return fmt::format( "Pixel not in cluster xy sensor {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };

  // missed MCHits histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsEta{
      this, "# MCHits not found - eta", "# MCHits not found - eta", { 70, -5.5, 5.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsPhi{
      this, "# MCHits not found - phi", "# MCHits not found - phi", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsPhikk{
      this, "# MCHits not found - phi - phikk", "# MCHits not found - phi - phikk", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsPhigee{
      this, "# MCHits not found - phi - gammaee", "# MCHits not found - phi - gammaee", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsR{
      this, "# MCHits not found - r", "# MCHits not found - r", { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsR7{
      this, "# MCHits not found - r<7mm", "# MCHits not found - r<7mm", { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsP{
      this, "# MCHits not found - p", "# MCHits not found - p", { 120, 0, 100 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsPt{
      this, "# MCHits not found - pt", "# MCHits not found - pt", { 120, 0, 10 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsZ{
      this, "# MCHits not found - z", "# MCHits not found - z", { 90, -300, 800 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedHitsModule{
      this, "# MCHits not found - module", "# MCHits not found - module", { 52, -0.5, 51.5 } };

  // recontructed MCHits histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsEta{
      this, "# MCHits reconstructible - eta", "# MCHits reconstructible - eta", { 70, -5.5, 5.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsPhi{
      this, "# MCHits reconstructible - phi", "# MCHits reconstructible - phi", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsPhikk{
      this, "# MCHits reconstructible - phi - phikk", "# MCHits reconstructible - phi - phikk", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsPhigee{ this,
                                                                   "# MCHits reconstructible - phi - gammaee",
                                                                   "# MCHits reconstructible - phi - gammaee",
                                                                   { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsR{
      this, "# MCHits reconstructible - r", "# MCHits reconstructible - r", { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsR7{
      this, "# MCHits reconstructible - r<7mm", "# MCHits reconstructible - r<7mm", { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsP{
      this, "# MCHits reconstructible - p", "# MCHits reconstructible - p", { 120, 0, 100 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsPt{
      this, "# MCHits reconstructible - pt", "# MCHits reconstructible - pt", { 120, 0, 10 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsZ{
      this, "# MCHits reconstructible - z", "# MCHits reconstructible - z", { 90, -300, 800 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recHitsModule{
      this, "# MCHits reconstructible - module", "# MCHits reconstructible - module", { 52, -0.5, 51.5 } };

  // missed MCHits from Velo reconstructible tracks histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsEta{
      this, "# MCHits not found Velo reco - eta", "# MCHits not found Velo reco - eta", { 70, -5.5, 5.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsPhi{
      this, "# MCHits not found Velo reco - phi", "# MCHits not found Velo reco - phi", { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsPhikk{ this,
                                                                         "# MCHits not found Velo reco - phi - phikk",
                                                                         "# MCHits not found Velo reco - phi - phikk",
                                                                         { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsPhigee{
      this,
      "# MCHits not found Velo reco - phi - gammaee",
      "# MCHits not found Velo reco - phi - gammaee",
      { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsR{
      this, "# MCHits not found Velo reco - r", "# MCHits not found Velo reco - r", { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsR7{
      this, "# MCHits not found Velo reco - r<7mm", "# MCHits not found Velo reco - r<7mm", { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsP{
      this, "# MCHits not found Velo reco - p", "# MCHits not found Velo reco - p", { 120, 0, 100 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsPt{
      this, "# MCHits not found Velo reco - pt", "# MCHits not found Velo reco - pt", { 120, 0, 10 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsZ{
      this, "# MCHits not found Velo reco - z", "# MCHits not found Velo reco - z", { 90, -300, 800 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_missedVeloHitsModule{
      this, "# MCHits not found Velo reco - module", "# MCHits not found Velo reco - module", { 52, -0.5, 51.5 } };

  // recontructed MCHits from Velo reconstructible tracks histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsEta{
      this, "# MCHits reconstructible Velo reco - eta", "# MCHits reconstructible Velo reco - eta", { 70, -5.5, 5.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsPhi{ this,
                                                                    "# MCHits reconstructible Velo reco - phi",
                                                                    "# MCHits reconstructible Velo reco - phi",
                                                                    { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsPhikk{
      this,
      "# MCHits reconstructible Velo reco - phi - phikk",
      "# MCHits reconstructible Velo reco - phi - phikk",
      { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsPhigee{
      this,
      "# MCHits reconstructible Velo reco - phi - gammaee",
      "# MCHits reconstructible Velo reco - phi - gammaee",
      { 21, -M_PI, M_PI } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsR{
      this, "# MCHits reconstructible Velo reco - r", "# MCHits reconstructible Velo reco - r", { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsR7{ this,
                                                                   "# MCHits reconstructible Velo reco - r<7mm",
                                                                   "# MCHits reconstructible Velo reco - r<7mm",
                                                                   { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsP{
      this, "# MCHits reconstructible Velo reco - p", "# MCHits reconstructible Velo reco - p", { 120, 0, 100 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsPt{
      this, "# MCHits reconstructible Velo reco - pt", "# MCHits reconstructible Velo reco - pt", { 120, 0, 10 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsZ{
      this, "# MCHits reconstructible Velo reco - z", "# MCHits reconstructible Velo reco - z", { 90, -300, 800 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recVeloHitsModule{ this,
                                                                       "# MCHits reconstructible Velo reco - module",
                                                                       "# MCHits reconstructible Velo reco - module",
                                                                       { 52, -0.5, 51.5 } };

  // recontructed MCHits from Muon histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_muonR7{
      this, "# muons r<7mm", "# muons r<7mm", { 11, -0.5, 10.5 } };
  mutable Gaudi::Accumulators::StaticProfileHistogram<1> m_muonEff{
      this, "Efficiency vs # #mu (at least 2 #mu)", "Efficiency vs # #mu (at least 2 #mu)", { 11, -0.5, 10.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_muonSemsorsMu{
      this, "Sensors w at least 2 #mu", "Sensors w at least 2 #mu", { 208, -0.5, 207.5 } };

  // cluster residual histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resX{
      this, "Residuals along x [mm]", "Residuals along x [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resY{
      this, "Residuals along y [mm]", "Residuals along y [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_resXPerModule{
      this,
      []( int n ) { return fmt::format( "Residuals along x [mm] - module{}", n ); },
      []( int n ) { return fmt::format( "Residuals along x [mm] - module{}", n ); },
      { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_resYPerModule{
      this,
      []( int n ) { return fmt::format( "Residuals along y [mm] - module{}", n ); },
      []( int n ) { return fmt::format( "Residuals along y [mm] - module{}", n ); },
      { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resXVelo{
      this, "Residuals along x - VELO reco [mm]", "Residuals along x - VELO reco [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resYVelo{
      this, "Residuals along y - VELO reco [mm]", "Residuals along y - VELO reco [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resXLong{
      this, "Residuals along x - long [mm]", "Residuals along x - long [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resYLong{
      this, "Residuals along y - long [mm]", "Residuals along y - long [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resXLongPixel{
      this, "Residuals along x - long pixels [mm]", "Residuals along x - long pixels [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resYLongPixel{
      this, "Residuals along y - long pixels [mm]", "Residuals along y - long pixels [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resXEdge{
      this, "Residuals along x - sensor edges [mm]", "Residuals along x - sensor edges [mm]", { 200, -0.2, 0.2 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_resYEdge{
      this, "Residuals along y - sensor edges [mm]", "Residuals along y - sensor edges [mm]", { 200, -0.2, 0.2 } };

  // matrix edge histograms
  mutable Gaudi::Accumulators::StaticHistogram<2> m_noRecClosestCluter2D{ this,
                                                                          "Non reconstructed MCHit - closest cluster",
                                                                          "Non reconstructed MCHit - closest cluster",
                                                                          { 36, -0.99, 0.99 },
                                                                          { 36, -0.99, 0.99 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_noRecClosestCluter1D{
      this,
      "Non reconstructed MCHit - closest cluster - 1D",
      "Non reconstructed MCHit - closest cluster - 1D",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_noRecClosestRecHit{
      this,
      "Non reconstructed MCHit - closest reconstructible MCHit",
      "Non reconstructed MCHit - closest reconstructible MCHit",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recClosestRecHit{
      this,
      "Reconstructible MCHit - closest reconstructible MCHit",
      "Reconstructible MCHit - closest reconstructible MCHit",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recClosestRecHitExt{
      this,
      "Reconstructible MCHit - closest reconstructible MCHit - extended",
      "Reconstructible MCHit - closest reconstructible MCHit - extended",
      { 3830, 0., 52.8 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_noRecClosestRecHitphikk{
      this,
      "Non reconstructed MCHit - closest reconstructible MCHit - phikk",
      "Non reconstructed MCHit - closest reconstructible MCHit - phikk",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recClosestRecHitphikk{
      this,
      "Reconstructible MCHit - closest reconstructible MCHit - phikk",
      "Reconstructible MCHit - closest reconstructible MCHit - phikk",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_noRecClosestRecHitgee{
      this,
      "Non reconstructed MCHit - closest reconstructible MCHit - gammaee",
      "Non reconstructed MCHit - closest reconstructible MCHit - gammaee",
      { 48, 0., 0.66 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_recClosestRecHitgee{
      this,
      "Reconstructible MCHit - closest reconstructible MCHit - gammaee",
      "Reconstructible MCHit - closest reconstructible MCHit - gammaee",
      { 48, 0., 0.66 } };

  // distribution number of clusters histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nClusters{ this, "# clusters", "# clusters", { 10000, 0, 10000 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nUnmatchedClusters{
      this, "# unmatched clusters", "# unmatched clusters", { 100, 0, 10000 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nClustersR{
      this, "# clusters distribution - r", "# clusters distribution - r", { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_nClustersRPerModule{
      this,
      []( int n ) { return fmt::format( "# cluster distribution - r - module{}", n ); },
      []( int n ) { return fmt::format( "# cluster distribution - r - module{}", n ); },
      { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nClustersModule{
      this, "# clusters distribution - module", "# clusters distribution - module", { 52, -0.5, 51.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nPixelPerCluster{
      this, "# pixel per cluster distribution", "# pixel per cluster distribution", { 51, -0.5, 50.5 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_nPixelPerClusterPerModule{
      this,
      []( int n ) { return fmt::format( "# pixel per cluster distribution - module{}", n ); },
      []( int n ) { return fmt::format( "# pixel per cluster distribution - module{}", n ); },
      { 51, -0.5, 50.5 } };

  // purity histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_purityHisto{
      this, "Purity distribution", "Purity distribution", { 110, 0, 1.1 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nPixels{
      this, "# pixel distribution - seen", "# pixel distribution - seen", { 20, 0, 100 } };
  mutable Gaudi::Accumulators::StaticProfileHistogram<1> m_purityPixels{
      this, "Purity vs # pixels", "Purity vs # pixels", { 20, 0, 100 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_nPixelsInCluster{
      this, "Number of pixels in cluster", "Number of pixels in cluster", { 33, 0, 32 } };

  // pixel stat histograms
  mutable Gaudi::Accumulators::StaticHistogram<1> m_cluterTohit{
      this, "# associated cluster to hit", "# associated cluster to hit", { 11, -0.5, 10.5 } };
  mutable Gaudi::Accumulators::StaticHistogram<1> m_pixels{
      this, "# pixel distribution", "# pixel distribution", { 10, 0, 100 } };

  // efficiency vs occupancy histograms
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 208> m_recMCHitPerSensor{
      this,
      []( int n ) { return fmt::format( "Reconstructible MCHit x,y VP sensor {}", n ); },
      []( int n ) { return fmt::format( "Reconstructible MCHit x,y VP sensor {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                                           m_recMCHit{ this,
                                                              "Reconstructible MCHit x,y VP all sensors",
                                                              "Reconstructible MCHit x,y VP all sensors",
                                                                                                        { 240, -60, 60 },
                                                                                                        { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 208> m_recMCHitVeloPerSensor{
      this,
      []( int n ) { return fmt::format( "Reconstructible MCHit x,y VP sensor - VELO {}", n ); },
      []( int n ) { return fmt::format( "Reconstructible MCHit x,y VP sensor - VELO {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2>                                           m_recMCHitVelo{ this,
                                                                  "Reconstructible MCHit x,y VP all sensors - VELO",
                                                                  "Reconstructible MCHit x,y VP all sensors - VELO",
                                                                                                            { 240, -60, 60 },
                                                                                                            { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 208> m_missedMCHitPerSensor{
      this,
      []( int n ) { return fmt::format( "Missed MCHit x,y VP sensor {}", n ); },
      []( int n ) { return fmt::format( "Missed MCHit x,y VP sensor {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_missedMCHit{
      this, "Missed MCHit x,y VP all sensors", "Missed MCHit x,y VP all sensors", { 240, -60, 60 }, { 240, -60, 60 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<2>, 208> m_missedMCHitVeloPerSensor{
      this,
      []( int n ) { return fmt::format( "Missed MCHit x,y VP sensor - VELO {}", n ); },
      []( int n ) { return fmt::format( "Missed MCHit x,y VP sensor - VELO {}", n ); },
      { 240, -60, 60 },
      { 240, -60, 60 } };
  mutable Gaudi::Accumulators::StaticHistogram<2> m_missedMCHitVelo{ this,
                                                                     "Missed MCHit x,y VP all sensors - VELO",
                                                                     "Missed MCHit x,y VP all sensors - VELO",
                                                                     { 240, -60, 60 },
                                                                     { 240, -60, 60 } };

  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_recHitsRPerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits reconstructible - r - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits reconstructible - r - module{}", n ); },
      { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_recHitsR7PerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits reconstructible - r<7mm - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits reconstructible - r<7mm - module{}", n ); },
      { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_recHitsVeloRPerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits reconstructible Velo reco - r - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits reconstructible Velo reco - r - module{}", n ); },
      { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_recHitsVeloR7PerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits reconstructible Velo reco - r<7mm - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits reconstructible Velo reco - r<7mm - module{}", n ); },
      { 14, 5.1, 7 } };

  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_missedHitsRPerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits not found - r - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits not found - r - module{}", n ); },
      { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_missedHitsR7PerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits not found - r<7mm - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits not found - r<7mm - module{}", n ); },
      { 14, 5.1, 7 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_missedHitsVeloRPerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits not found Velo reco - r - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits not found Velo reco - r - module{}", n ); },
      { 50, 5.1, 50 } };
  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::StaticHistogram<1>, 52> m_missedHitsVeloR7PerModule{
      this,
      []( int n ) { return fmt::format( "# MCHits not found Velo reco - r<7mm - module{}", n ); },
      []( int n ) { return fmt::format( "# MCHits not found Velo reco - r<7mm - module{}", n ); },
      { 14, 5.1, 7 } };
};

DECLARE_COMPONENT( VPClusterEfficiency )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPClusterEfficiency::VPClusterEfficiency( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{ name,
                pSvcLocator,
                { KeyValue{ "RawBanks", {} }, KeyValue{ "VPClusterLocation", LHCb::VPFullClusterLocation::Default },
                  KeyValue{ "MCHitLocation", LHCb::MCHitLocation::VP },
                  KeyValue{ "MCParticleLocation", LHCb::MCParticleLocation::Default },
                  KeyValue{ "VPDigit2MCHitLinksLocation", LHCb::VPDigitLocation::Default + "2MCHits" },
                  KeyValue{ "MCProperty", LHCb::MCPropertyLocation::TrackInfo },
                  KeyValue{ "DeVP", LHCb::Det::VP::det_path } } } {}

//=============================================================================
// Main execution
//=============================================================================
void VPClusterEfficiency::operator()( const LHCb::RawBank::View&              rawBanks,
                                      const std::vector<LHCb::VPFullCluster>& clusters, const LHCb::MCHits& mcHits,
                                      const LHCb::MCParticles&, const LHCb::LinksByKey&                     links,
                                      const LHCb::MCProperty& mcprop, const DeVP& det ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Found " << clusters.size() << " VP clusters" << endmsg;
    debug() << "Found " << mcHits.size() << " mcHits" << endmsg;
  }
  if ( rawBanks.empty() ) { throw GaudiException( "Missing VP banks", "No VP superpixel banks", StatusCode::FAILURE ); }
  if ( rawBanks[0]->type() != LHCb::RawBank::VP ) {
    throw GaudiException( "Wrong RawBank::Type", "No VP superpixel banks", StatusCode::FAILURE );
  }

  // convert Superpixels into digits
  std::vector<LHCb::Detector::VPChannelID> digits;
  digits.reserve( clusters.size() * 2 ); // guess as to max pixels to clusters ratio
  // offsets from SP address of component pixels
  /*
   * row,y
   *
   *  ^  37
   *  |  26
   *  |  15
   *  |  04
   *  +---> col,x
   */
  constexpr uint32_t CHIP_COLUMNS = 256; // hard code number of columns per chip
  // Loop over VP RawBanks
  for ( const auto& bank : rawBanks ) {
    const auto beforeSize = digits.size();
    const auto sensor     = LHCb::Detector::VPChannelID::SensorID( bank->sourceID() ); // sensor = source ID
    auto       data       = bank->range<uint32_t>();

    assert( data.size() == data.front() + 1 );
    for ( const uint32_t sp_word :
          data.subspan( 1 ) ) {     // note: the subspan(1) will skip the first entry, which is `nsp`
      uint8_t sp = sp_word & 0xFFU; // mask out pattern [bit 0-7]
      if ( 0 == sp ) continue;      // protect against empty super pixels.

      // bit 8-13   Super Pixel Row (0-63), bit 14-22  Super Pixel Column (0-383)
      const uint32_t sp_addr = ( sp_word & 0x007FFF00U ) >> 8; // get bits and left shift align
      const uint32_t sp_row  = sp_addr & 0x3FU;                // pick out row
      const uint32_t sp_col  = ( sp_addr >> 6 );               // left shift to align column val
      for ( uint32_t off = 0; off < 8; ++off ) {
        const uint8_t mask = 0x1U << off;
        if ( sp & mask ) {                               // test each of the 8 bits in turn
          const uint32_t cx = sp_col * 2 + ( off >> 2 ); // 0->3 no offset, 4->7 offset = 1
          const auto     cy =
              LHCb::Detector::VPChannelID::RowID{ sp_row * 4 + ( off & 0b11U ) }; // 0->3 add off, 4->7 add off-4
          const auto chip = LHCb::Detector::VPChannelID::ChipID{ cx / CHIP_COLUMNS };
          const auto ccol = LHCb::Detector::VPChannelID::ColumnID{ cx % CHIP_COLUMNS };
          digits.emplace_back( sensor, chip, ccol, cy );
        }
      }
    }
    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "Found " << digits.size() - beforeSize << " pixels in sensor " << to_unsigned( sensor ) << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Found " << digits.size() << " VP Digits from SuperPixel banks" << endmsg;
  }

  // Table linking a LHCb::Detector::VPChannelID* to std::vector<LHCb::MCHit>
  std::map<const unsigned int, std::vector<LHCb::MCHit const*>> MCHitForchannelId;
  links.applyToAllLinks( [&MCHitForchannelId, &mcHits]( unsigned int channelId, unsigned int mcHitKey, float ) {
    MCHitForchannelId[channelId].emplace_back( mcHits[mcHitKey] );
  } );

  // Table linking a LHCb::MCHit* to std::vector<LHCb::Detector::VPChannelID>> --orig
  std::map<const LHCb::MCHit*, std::vector<unsigned int>> channelIdForMCHit;
  links.applyToAllLinks( [&channelIdForMCHit, &mcHits]( unsigned int channelId, unsigned int mcHitKey, float ) {
    channelIdForMCHit[mcHits[mcHitKey]].emplace_back( channelId );
  } );

  // split MCHits into modules
  std::array<std::vector<LHCb::MCHit const*>, 52> hitsInModules;
  for ( unsigned int i = 0; i < 52; ++i ) hitsInModules[i].reserve( mcHits.size() / 52 ); // make some space
  for ( auto& mcH : mcHits ) {
    unsigned int module = mcH->sensDetID() / 4; // 4 sensors per module, numbered consecutively
    hitsInModules[module].push_back( mcH );
  }

  if ( msgLevel( MSG::VERBOSE ) ) {
    for ( unsigned int i = 0; i < 52; ++i ) {
      verbose() << "Found " << hitsInModules[i].size() << " MCHits in module " << i << endmsg;
    }
  }

  unsigned int modNum = 0;
  for ( const auto& hitsInModule : hitsInModules ) {
    for ( const auto& mcH : hitsInModule ) {
      // all MCHits
      ++m_mcHitXYPerModule[modNum][{ mcH->midPoint().x(), mcH->midPoint().y() }];
      ++m_mcHitXY[{ mcH->midPoint().x(), mcH->midPoint().y() }];
      // distance particle traveled in sensor & energy
      auto dist = mcH->pathLength();
      ++m_mcHitDistTravel[dist];
      // energy
      auto energy = mcH->energy();
      ++m_mcHitEnergyDeposit[energy];
      // were MCHits found
      double efficiency = ( channelIdForMCHit.find( mcH ) != channelIdForMCHit.end() );
      m_mcHitEffXYPerModule[modNum][{ mcH->midPoint().x(), mcH->midPoint().y() }] += efficiency;
      m_mcHitEffXY[{ mcH->midPoint().x(), mcH->midPoint().y() }] += efficiency;
      m_mcHitEffvDist[dist] += efficiency;
      m_mcHitEffvEnergy[energy] += efficiency;
    }
    modNum++;
  }

  // what pixels are in the event -- copy them all now, then delete as they are found in clusters
  auto pixUsed = std::set<LHCb::Detector::VPChannelID>( digits.begin(), digits.end() );

  // plot info on clusters
  for ( auto& clus : clusters ) {
    unsigned int           mainPix  = 0;
    unsigned int           otherPix = 0;
    std::set<unsigned int> mcKeysClus;
    for ( auto& channelID : clus.pixels() ) {

      unsigned int numMC = ( MCHitForchannelId.find( channelID.channelID() )->second.size() );
      ++m_nHitsPerPixel[numMC];
      // count MCParticles contributing to Cluster
      std::set<unsigned int> mcKeysPix;
      for ( auto& mcHit : ( *MCHitForchannelId.find( channelID.channelID() ) ).second ) {
        auto particle = mcHit->mcParticle();
        // Check if the hit originates from a delta ray.
        while ( particle && particle->originVertex() &&
                particle->originVertex()->type() == LHCb::MCVertex::MCVertexType::DeltaRay ) {
          particle = particle->mother();
        }
        mcKeysPix.insert( particle->key() );
      }
      unsigned int numMCP = mcKeysPix.size();
      ++m_nMCParticlesPerPixel[numMCP];
      ++m_nMCParticlesPerPixelModule[{ numMCP, channelID.module() }];
      // check if pixel is from MCHit
      if ( numMC != 0 ) {
        ++mainPix;
      } else {
        ++otherPix;
      }
      pixUsed.erase( channelID );                             // leave only "unused" pixels
      for ( auto& key : mcKeysPix ) mcKeysClus.insert( key ); // add to list of cluster MCParticles
    }
    double numMCParticles = (double)mcKeysClus.size();
    ++m_nParticlesPerCluster[numMCParticles];
    ++m_nParticlesPerClusterModule[{ numMCParticles, clus.channelID().module() }];
    double fracOther = ( (double)otherPix ) / ( (double)( otherPix + mainPix ) );
    ++m_spillPerCluster[fracOther];
    ++m_spillPerClusterSize[{ fracOther, clus.pixels().size() }];
    // plot positions of "good" clusters i.e. > 70% from MCHits
    unsigned int module = clus.channelID().module();
    if ( fracOther < 0.3 ) {
      ++m_goodCluterPos[{ clus.x(), clus.y() }];
      ++m_goodCluterPosPerModule[module][{ clus.x(), clus.y() }];
    } else {
      ++m_badCluterPos[{ clus.x(), clus.y() }];
      ++m_badCluterPosPerModule[module][{ clus.x(), clus.y() }];
    }
  }

  // all pixels
  for ( auto& channelID : digits ) {
    const DeVPSensor& sensor      = det.sensor( channelID.sensor() );
    Gaudi::XYZPoint   pointGlobal = sensor.channelToGlobalPoint( channelID );
    ++m_pixelXY[{ pointGlobal.x(), pointGlobal.y() }];
    ++m_pixelXYPerModule[channelID.module()][{ pointGlobal.x(), pointGlobal.y() }];
  }

  // lost pixels
  double nPixNotUsed = (double)( pixUsed.size() );
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Found " << nPixNotUsed << " VP Digits unused in clusters" << endmsg; }

  ++m_nPixelNotUsed[nPixNotUsed];
  if ( digits.size() > 0 ) { ++m_fracPixelNotInEvent[nPixNotUsed / (double)digits.size()]; }
  for ( auto& channelID : pixUsed ) {
    const DeVPSensor& sensor      = det.sensor( channelID.sensor() );
    Gaudi::XYZPoint   pointGlobal = sensor.channelToGlobalPoint( channelID );
    ++m_pixelNotinCluster[{ pointGlobal.x(), pointGlobal.y() }];
    ++m_pixelNotinClusterPerModule[channelID.module()][{ pointGlobal.x(), pointGlobal.y() }];
  }

  // cluster efficiency
  const auto trackInfo = MCTrackInfo{ mcprop };

  std::vector<LHCb::MCHit*> hitsReconstructible;
  std::vector<LHCb::MCHit*> hitsMissed;
  std::vector<LHCb::MCHit*> hitsReconstructibleVeloReco;
  std::vector<LHCb::MCHit*> hitsMissedVeloReco;

  std::array<std::vector<LHCb::MCHit*>, 208> hitsMissedInSensors;
  for ( unsigned int i = 0; i < 208; ++i ) hitsMissedInSensors[i].reserve( mcHits.size() / 208 ); // make some space
  std::array<std::vector<LHCb::MCHit*>, 208> hitsReconstructibleInSensors;
  for ( unsigned int i = 0; i < 208; ++i )
    hitsReconstructibleInSensors[i].reserve( mcHits.size() / 208 ); // make some space

  std::array<std::vector<LHCb::MCHit*>, 208> hitsMissedInSensorsMu;
  for ( unsigned int i = 0; i < 208; ++i ) hitsMissedInSensorsMu[i].reserve( mcHits.size() / 208 ); // make some space
  std::array<std::vector<LHCb::MCHit*>, 208> hitsReconstructibleInSensorsMu;
  for ( unsigned int i = 0; i < 208; ++i )
    hitsReconstructibleInSensorsMu[i].reserve( mcHits.size() / 208 ); // make some space

  for ( auto& mcH : mcHits ) {
    if ( channelIdForMCHit.find( mcH ) != channelIdForMCHit.end() ) { // check that hit has at least a digit associated
      hitsReconstructible.push_back( mcH );
      hitsMissed.push_back( mcH );
      hitsMissedInSensors[mcH->sensDetID()].push_back( mcH );
      hitsReconstructibleInSensors[mcH->sensDetID()].push_back( mcH );
      if ( trackInfo.hasVelo( mcH->mcParticle() ) ) { // make residuals for VELO reconstructible tracks
        hitsReconstructibleVeloReco.push_back( mcH );
        hitsMissedVeloReco.push_back( mcH );
      }
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 13 &&
           sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() ) < 7 ) {
        hitsReconstructibleInSensorsMu[mcH->sensDetID()].push_back( mcH );
        hitsMissedInSensorsMu[mcH->sensDetID()].push_back( mcH );
      }
    }
  }

  int unmatched_clusters = 0;
  for ( auto& clus : clusters ) {
    bool matched = false;
    for ( auto& channelID : clus.pixels() ) {
      for ( auto& mcHit : MCHitForchannelId.find( channelID.channelID() )->second ) {
        matched = true;
        hitsMissed.erase( std::remove( hitsMissed.begin(), hitsMissed.end(), mcHit ), hitsMissed.end() );
        hitsMissedInSensors[mcHit->sensDetID()].erase( std::remove( hitsMissedInSensors[mcHit->sensDetID()].begin(),
                                                                    hitsMissedInSensors[mcHit->sensDetID()].end(),
                                                                    mcHit ),
                                                       hitsMissedInSensors[mcHit->sensDetID()].end() );
        if ( trackInfo.hasVelo( mcHit->mcParticle() ) ) {
          hitsMissedVeloReco.erase( std::remove( hitsMissedVeloReco.begin(), hitsMissedVeloReco.end(), mcHit ),
                                    hitsMissedVeloReco.end() );
        }
        hitsMissedInSensorsMu[mcHit->sensDetID()].erase( std::remove( hitsMissedInSensorsMu[mcHit->sensDetID()].begin(),
                                                                      hitsMissedInSensorsMu[mcHit->sensDetID()].end(),
                                                                      mcHit ),
                                                         hitsMissedInSensorsMu[mcHit->sensDetID()].end() );
      }
    }
    if ( !matched ) ++unmatched_clusters;
  }

  // plots for missed MCHits
  for ( auto& mcH : hitsMissed ) {
    ++m_missedHitsEta[mcH->mcParticle()->momentum().Eta()];
    ++m_missedHitsPhi[mcH->mcParticle()->momentum().phi()];
    if ( nullptr != mcH->mcParticle()->originVertex()->mother() ) {
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 321 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 333 ) {
        ++m_missedHitsPhikk[mcH->mcParticle()->momentum().phi()];
      }
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 11 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 22 ) {
        ++m_missedHitsPhigee[mcH->mcParticle()->momentum().phi()];
      }
    }
    auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
    ++m_missedHitsR[r];
    ++m_missedHitsR7[r];
    ++m_missedHitsP[mcH->mcParticle()->p() / 1000.0];
    ++m_missedHitsPt[mcH->mcParticle()->pt() / 1000.0];
    ++m_missedHitsZ[mcH->midPoint().z()];
    ++m_missedHitsModule[mcH->sensDetID() / 4];
  }

  // plots for reconstructed MCHits
  for ( auto& mcH : hitsReconstructible ) {
    ++m_recHitsEta[mcH->mcParticle()->momentum().Eta()];
    ++m_recHitsPhi[mcH->mcParticle()->momentum().phi()];
    if ( nullptr != mcH->mcParticle()->originVertex()->mother() ) {
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 321 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 333 ) {
        ++m_recHitsPhikk[mcH->mcParticle()->momentum().phi()];
      }
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 11 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 22 ) {
        ++m_recHitsPhigee[mcH->mcParticle()->momentum().phi()];
      }
    }
    auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
    ++m_recHitsR[r];
    ++m_recHitsR7[r];
    ++m_recHitsP[mcH->mcParticle()->p() / 1000.0];
    ++m_recHitsPt[mcH->mcParticle()->pt() / 1000.0];
    ++m_recHitsZ[mcH->midPoint().z()];
    ++m_recHitsModule[mcH->sensDetID() / 4];
  }

  // plots for missed MCHits from Velo reconstructible track
  for ( auto& mcH : hitsMissedVeloReco ) {
    ++m_missedVeloHitsEta[mcH->mcParticle()->momentum().Eta()];
    ++m_missedVeloHitsPhi[mcH->mcParticle()->momentum().phi()];
    if ( nullptr != mcH->mcParticle()->originVertex()->mother() ) {
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 321 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 333 ) {
        ++m_missedVeloHitsPhikk[mcH->mcParticle()->momentum().phi()];
      }
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 11 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 22 ) {
        ++m_missedVeloHitsPhigee[mcH->mcParticle()->momentum().phi()];
      }
    }
    auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
    ++m_missedVeloHitsR[r];
    ++m_missedVeloHitsR7[r];
    ++m_missedVeloHitsP[mcH->mcParticle()->p() / 1000.0];
    ++m_missedVeloHitsPt[mcH->mcParticle()->pt() / 1000.0];
    ++m_missedVeloHitsZ[mcH->midPoint().z()];
    ++m_missedVeloHitsModule[mcH->sensDetID() / 4];
  }

  // plots for reconstructed MCHits from Velo reconstructible track
  for ( auto& mcH : hitsReconstructibleVeloReco ) {
    ++m_recVeloHitsEta[mcH->mcParticle()->momentum().Eta()];
    ++m_recVeloHitsPhi[mcH->mcParticle()->momentum().phi()];
    if ( nullptr != mcH->mcParticle()->originVertex()->mother() ) {
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 321 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 333 ) {
        ++m_recVeloHitsPhikk[mcH->mcParticle()->momentum().phi()];
      }
      if ( abs( mcH->mcParticle()->particleID().pid() ) == 11 &&
           abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == 22 ) {
        ++m_recVeloHitsPhigee[mcH->mcParticle()->momentum().phi()];
      }
    }
    auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
    ++m_recVeloHitsR[r];
    ++m_recVeloHitsR7[r];
    ++m_recVeloHitsP[mcH->mcParticle()->p() / 1000.0];
    ++m_recVeloHitsPt[mcH->mcParticle()->pt() / 1000.0];
    ++m_recVeloHitsZ[mcH->midPoint().z()];
    ++m_recVeloHitsModule[mcH->sensDetID() / 4];
  }

  // plots for reconstructed MCHits from muons
  for ( auto& hitsInSensor : hitsReconstructibleInSensorsMu ) {
    ++m_muonR7[hitsInSensor.size()];
    if ( hitsInSensor.size() > 1 ) {
      auto   sensor = hitsInSensor[0]->sensDetID();
      double efficiency;
      if ( hitsInSensor.size() - hitsMissedInSensorsMu[sensor].size() > 1 ) {
        efficiency = 1.0;
      } else {
        efficiency = 0.0;
      }
      m_muonEff[hitsInSensor.size()] += efficiency;
      ++m_muonSemsorsMu[sensor];
    }
  }

  // cluster residual plots
  for ( auto& clus : clusters ) {
    m_num_pix_clu += clus.pixels().size();
    std::set<LHCb::MCHit const*> associated_hits;
    auto                         module = clus.channelID().module();
    for ( auto& channelID : clus.pixels() ) {
      for ( auto& mcHit : ( *MCHitForchannelId.find( channelID.channelID() ) ).second ) {
        if ( channelIdForMCHit.find( mcHit ) != channelIdForMCHit.end() ) { associated_hits.insert( mcHit ); }
      }
    }
    for ( auto& mcHit : associated_hits ) {
      double x_dist = clus.x() - mcHit->midPoint().x();
      double y_dist = clus.y() - mcHit->midPoint().y();

      if ( abs( x_dist ) < 0.1 ) { m_residual_x += x_dist; }
      if ( abs( y_dist ) < 0.1 ) { m_residual_y += y_dist; }

      ++m_resX[x_dist];
      ++m_resY[y_dist];
      ++m_resXPerModule[module][x_dist];
      ++m_resYPerModule[module][y_dist];

      if ( trackInfo.hasVelo( mcHit->mcParticle() ) ) { // make residuals for VELO reconstructible tracks
        ++m_resXVelo[x_dist];
        ++m_resYVelo[y_dist];
      }

      if ( trackInfo.hasT( mcHit->mcParticle() ) && trackInfo.hasVelo( mcHit->mcParticle() ) ) { // make residuals for
        ++m_resXLong[x_dist];
        ++m_resYLong[y_dist];
      }

      if ( clus.channelID().col() == LHCb::Detector::VPChannelID::ColumnID{ 255 } ||
           clus.channelID().col() == LHCb::Detector::VPChannelID::ColumnID{ 256 } ||
           clus.channelID().col() == LHCb::Detector::VPChannelID::ColumnID{ 511 } ||
           clus.channelID().col() == LHCb::Detector::VPChannelID::ColumnID{ 512 } ) { // make residuals for long pixels
        ++m_resXLongPixel[x_dist];
        ++m_resYLongPixel[y_dist];
      }

      if ( clus.channelID().col() < LHCb::Detector::VPChannelID::ColumnID{ 3 } ||
           clus.channelID().col() > LHCb::Detector::VPChannelID::ColumnID{ 764 } ||
           clus.channelID().row() < LHCb::Detector::VPChannelID::RowID{ 3 } ||
           clus.channelID().row() > LHCb::Detector::VPChannelID::RowID{ 252 } ) { // make residuals for sensor edges
        ++m_resXEdge[x_dist];
        ++m_resYEdge[y_dist];
      }
    }
  }

  // matrix edge check
  for ( const auto& hitsMissedInSensor : hitsMissedInSensors ) {
    for ( const auto& mcH : hitsMissedInSensor ) {
      double x_dist     = 9999;
      double y_dist     = 9999;
      double dist       = 9999;
      double distmcHmcH = 9999;
      bool   gotdist    = false;
      for ( auto& clus : clusters ) {
        if ( clus.channelID().sensor() == LHCb::Detector::VPChannelID::SensorID( mcH->sensDetID() ) ) { // Velo sensor
                                                                                                        // are non
                                                                                                        // negative
          if ( sqrt( ( mcH->midPoint().x() - clus.x() ) * ( mcH->midPoint().x() - clus.x() ) +
                     ( mcH->midPoint().y() - clus.y() ) * ( mcH->midPoint().y() - clus.y() ) ) < dist ) {
            dist    = sqrt( ( mcH->midPoint().x() - clus.x() ) * ( mcH->midPoint().x() - clus.x() ) +
                            ( mcH->midPoint().y() - clus.y() ) * ( mcH->midPoint().y() - clus.y() ) );
            x_dist  = ( mcH->midPoint().x() - clus.x() );
            y_dist  = ( mcH->midPoint().y() - clus.y() );
            gotdist = true;
          }
        }
      }
      if ( gotdist ) {
        ++m_noRecClosestCluter2D[{ x_dist, y_dist }];
        ++m_noRecClosestCluter1D[dist];
      }

      gotdist = false;
      for ( auto& mcHReco : hitsReconstructibleInSensors[mcH->sensDetID()] ) {

        if ( ( mcHReco != mcH ) && ( mcHReco->mcParticle() != mcH->mcParticle() ) ) {
          if ( sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                         ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                     ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                         ( mcH->midPoint().y() - mcHReco->midPoint().y() ) ) < distmcHmcH ) {
            distmcHmcH = sqrt(
                ( mcH->midPoint().x() - mcHReco->midPoint().x() ) * ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                ( mcH->midPoint().y() - mcHReco->midPoint().y() ) * ( mcH->midPoint().y() - mcHReco->midPoint().y() ) );
            gotdist = true;
          }
        }
      }

      if ( gotdist && distmcHmcH > 0 ) { ++m_noRecClosestRecHit[distmcHmcH]; }
    }
  }

  for ( const auto& hitsReconstructibleInSensor : hitsReconstructibleInSensors ) {
    for ( const auto& mcH : hitsReconstructibleInSensor ) {
      double distmcHmcH = 9999;
      bool   gotdist    = false;
      for ( auto& mcHReco : hitsReconstructibleInSensors[mcH->sensDetID()] ) {
        if ( ( mcHReco != mcH ) && ( mcHReco->mcParticle() != mcH->mcParticle() ) ) {
          if ( sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                         ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                     ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                         ( mcH->midPoint().y() - mcHReco->midPoint().y() ) ) < distmcHmcH ) {
            distmcHmcH = sqrt(
                ( mcH->midPoint().x() - mcHReco->midPoint().x() ) * ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                ( mcH->midPoint().y() - mcHReco->midPoint().y() ) * ( mcH->midPoint().y() - mcHReco->midPoint().y() ) );
            gotdist = true;
          }
        }
      }
      if ( gotdist && distmcHmcH > 0 ) {
        ++m_recClosestRecHit[distmcHmcH];
        ++m_recClosestRecHitExt[distmcHmcH];
      }
    }
  }

  // matrix edge check - phi->kk
  constexpr auto KPlus    = 321;
  constexpr auto phi_1020 = 333;
  for ( const auto& hitsMissedInSensor : hitsMissedInSensors ) {
    for ( const auto& mcHMiss : hitsMissedInSensor ) {
      double dist    = 9999;
      bool   gotdist = false;
      if ( nullptr != mcHMiss->mcParticle()->originVertex()->mother() ) {
        if ( abs( mcHMiss->mcParticle()->particleID().pid() ) == KPlus &&
             abs( mcHMiss->mcParticle()->originVertex()->mother()->particleID().pid() ) == phi_1020 ) {
          for ( auto& mcH : hitsReconstructibleInSensors[mcHMiss->sensDetID()] ) {
            if ( mcH != mcHMiss && nullptr != mcH->mcParticle()->originVertex()->mother() ) {
              if ( abs( mcH->mcParticle()->particleID().pid() ) == KPlus &&
                   abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == phi_1020 ) {
                if ( sqrt( ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) *
                               ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) +
                           ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) *
                               ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) ) < dist ) {
                  dist    = sqrt( ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) *
                                      ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) +
                                  ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) *
                                      ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) );
                  gotdist = true;
                }
              }
            }
          }
          if ( gotdist && dist > 0 ) { ++m_noRecClosestRecHitphikk[dist]; }
        }
      }
    }
  }

  for ( const auto& hitsReconstructibleInSensor : hitsReconstructibleInSensors ) {
    for ( const auto& mcHReco : hitsReconstructibleInSensor ) {
      double distmcHmcH = 9999;
      bool   gotdist    = false;
      if ( nullptr != mcHReco->mcParticle()->originVertex()->mother() ) {
        if ( abs( mcHReco->mcParticle()->particleID().pid() ) == KPlus &&
             abs( mcHReco->mcParticle()->originVertex()->mother()->particleID().pid() ) == phi_1020 ) {
          for ( auto& mcH : hitsReconstructibleInSensors[mcHReco->sensDetID()] ) {
            if ( mcHReco != mcH && nullptr != mcH->mcParticle()->originVertex()->mother() ) {
              if ( abs( mcH->mcParticle()->particleID().pid() ) == KPlus &&
                   abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == phi_1020 ) {
                if ( sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                               ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                           ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                               ( mcH->midPoint().y() - mcHReco->midPoint().y() ) ) < distmcHmcH ) {
                  distmcHmcH = sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                                         ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                                     ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                                         ( mcH->midPoint().y() - mcHReco->midPoint().y() ) );
                  gotdist    = true;
                }
              }
            }
          }
          if ( gotdist && distmcHmcH > 0 ) { ++m_recClosestRecHitphikk[distmcHmcH]; }
        }
      }
    }
  }

  // matrix edge check - gamma->ee
  constexpr auto posit = 11;
  constexpr auto gamma = 22;
  for ( const auto& hitsMissedInSensor : hitsMissedInSensors ) {
    for ( const auto& mcHMiss : hitsMissedInSensor ) {
      double dist    = 9999;
      bool   gotdist = false;
      if ( nullptr != mcHMiss->mcParticle()->originVertex()->mother() ) {
        if ( abs( mcHMiss->mcParticle()->particleID().pid() ) == posit &&
             abs( mcHMiss->mcParticle()->originVertex()->mother()->particleID().pid() ) == gamma ) {
          for ( auto& mcH : hitsReconstructibleInSensors[mcHMiss->sensDetID()] ) {
            if ( mcH != mcHMiss && nullptr != mcH->mcParticle()->originVertex()->mother() ) {
              if ( abs( mcH->mcParticle()->particleID().pid() ) == posit &&
                   abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == gamma ) {
                if ( sqrt( ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) *
                               ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) +
                           ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) *
                               ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) ) < dist ) {
                  dist    = sqrt( ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) *
                                      ( mcH->midPoint().x() - mcHMiss->midPoint().x() ) +
                                  ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) *
                                      ( mcH->midPoint().y() - mcHMiss->midPoint().y() ) );
                  gotdist = true;
                }
              }
            }
          }
          if ( gotdist && dist > 0 ) { ++m_noRecClosestRecHitgee[dist]; }
        }
      }
    }
  }

  for ( const auto& hitsReconstructibleInSensor : hitsReconstructibleInSensors ) {
    for ( const auto& mcHReco : hitsReconstructibleInSensor ) {
      double distmcHmcH = 9999;
      bool   gotdist    = false;
      if ( nullptr != mcHReco->mcParticle()->originVertex()->mother() ) {
        if ( abs( mcHReco->mcParticle()->particleID().pid() ) == posit &&
             abs( mcHReco->mcParticle()->originVertex()->mother()->particleID().pid() ) == gamma ) {
          for ( auto& mcH : hitsReconstructibleInSensors[mcHReco->sensDetID()] ) {
            if ( mcHReco != mcH && nullptr != mcH->mcParticle()->originVertex()->mother() ) {
              if ( abs( mcH->mcParticle()->particleID().pid() ) == posit &&
                   abs( mcH->mcParticle()->originVertex()->mother()->particleID().pid() ) == gamma ) {
                if ( sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                               ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                           ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                               ( mcH->midPoint().y() - mcHReco->midPoint().y() ) ) < distmcHmcH ) {
                  distmcHmcH = sqrt( ( mcH->midPoint().x() - mcHReco->midPoint().x() ) *
                                         ( mcH->midPoint().x() - mcHReco->midPoint().x() ) +
                                     ( mcH->midPoint().y() - mcHReco->midPoint().y() ) *
                                         ( mcH->midPoint().y() - mcHReco->midPoint().y() ) );
                  gotdist    = true;
                }
              }
            }
          }
          if ( gotdist && distmcHmcH > 0 ) { ++m_recClosestRecHitgee[distmcHmcH]; }
        }
      }
    }
  }

  // plot distribution number of clusters
  ++m_num_clusters += clusters.size();
  ++m_nClusters[clusters.size()];
  ++m_nUnmatchedClusters[unmatched_clusters];
  for ( auto& clus : clusters ) {
    auto module = clus.channelID().module();
    auto r      = sqrt( clus.x() * clus.x() + clus.y() * clus.y() );
    ++m_nClustersR[r];
    ++m_nClustersRPerModule[module][r];
    ++m_nClustersModule[module];
    ++m_nPixelPerCluster[clus.pixels().size()];
    ++m_nPixelPerClusterPerModule[module][clus.pixels().size()];
  }

  // purity plots
  for ( auto& clus : clusters ) {
    std::set<LHCb::MCHit const*> associated_hits;
    std::vector<unsigned int>    ids_clu;
    for ( auto& channelID : clus.pixels() ) {
      ids_clu.push_back( channelID.channelID() );
      for ( auto& mcHit : MCHitForchannelId.find( channelID.channelID() )->second ) { associated_hits.insert( mcHit ); }
    }
    for ( auto& mcHit : associated_hits ) {
      auto const&               pixs = channelIdForMCHit.find( mcHit )->second;
      std::vector<unsigned int> ids_hit( pixs.begin(), pixs.end() );
      std::sort( ids_clu.begin(), ids_clu.end() );
      std::sort( ids_hit.begin(), ids_hit.end() );
      std::vector<int> common;
      set_intersection( ids_clu.begin(), ids_clu.end(), ids_hit.begin(), ids_hit.end(), back_inserter( common ) );
      auto pur = ( (double)common.size() ) / ids_hit.size();
      ++m_purityHisto[pur];
      m_purity += pur;
      ++m_nPixels[ids_hit.size()];
      m_purityPixels[ids_hit.size()] += pur;
    }
    ++m_nPixelsInCluster[ids_clu.size()];
  }

  // pixel stat plots
  for ( auto& mcHit : hitsReconstructible ) {
    std::set<int> associated_clusters;
    for ( auto& clus : clusters ) {
      if ( LHCb::Detector::VPChannelID::SensorID( mcHit->sensDetID() ) == clus.channelID().sensor() ) {
        for ( auto& channelID : clus.pixels() ) {
          for ( auto& mcHit_ass : ( *MCHitForchannelId.find( channelID.channelID() ) ).second ) {
            if ( mcHit_ass == mcHit ) { associated_clusters.insert( clus.channelID().channelID() ); }
          }
        }
      }
    }
    ++m_cluterTohit[associated_clusters.size()];
    ++m_pixels[channelIdForMCHit.find( mcHit )->second.size()];
    m_efficiency += !associated_clusters.empty();
    m_num_pix_hit += ( channelIdForMCHit.find( mcHit )->second.size() );
  }

  // efficiency vs occupancy
  for ( const auto& hitsReconstructibleInSensor : hitsReconstructibleInSensors ) {
    for ( const auto& mcH : hitsReconstructibleInSensor ) {
      // reconstructible MCHits
      const auto sensor = mcH->sensDetID();
      ++m_recMCHitPerSensor[sensor][{ mcH->midPoint().x(), mcH->midPoint().y() }];
      ++m_recMCHit[{ mcH->midPoint().x(), mcH->midPoint().y() }];
      if ( trackInfo.hasVelo( mcH->mcParticle() ) ) {
        ++m_recMCHitVeloPerSensor[sensor][{ mcH->midPoint().x(), mcH->midPoint().y() }];
        ++m_recMCHitVelo[{ mcH->midPoint().x(), mcH->midPoint().y() }];
      }
    }
  }

  for ( const auto& hitsMissedInSensor : hitsMissedInSensors ) {
    int sensor = -1;
    if ( hitsMissedInSensor.size() != 0 ) sensor = hitsMissedInSensor[0]->sensDetID();
    for ( auto& mcH : hitsMissedInSensor ) {
      // missed MCHits
      ++m_missedMCHitPerSensor[sensor][{ mcH->midPoint().x(), mcH->midPoint().y() }];
      ++m_missedMCHit[{ mcH->midPoint().x(), mcH->midPoint().y() }];
      if ( trackInfo.hasVelo( mcH->mcParticle() ) ) {
        ++m_missedMCHitVeloPerSensor[sensor][{ mcH->midPoint().x(), mcH->midPoint().y() }];
        ++m_missedMCHitVelo[{ mcH->midPoint().x(), mcH->midPoint().y() }];
      }
    }
  }

  for ( const auto& hitsReconstructibleInSensor : hitsReconstructibleInSensors ) {
    int module = -1;
    if ( hitsReconstructibleInSensor.size() != 0 ) module = hitsReconstructibleInSensor[0]->sensDetID() / 4;
    for ( const auto& mcH : hitsReconstructibleInSensor ) {
      // reconstructible MCHits
      auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
      ++m_recHitsRPerModule[module][r];
      ++m_recHitsR7PerModule[module][r];
      if ( trackInfo.hasVelo( mcH->mcParticle() ) ) {
        ++m_recHitsVeloRPerModule[module][r];
        ++m_recHitsVeloR7PerModule[module][r];
      }
    }
  }

  for ( const auto& hitsMissedInSensor : hitsMissedInSensors ) {
    int module = -1;
    if ( hitsMissedInSensor.size() != 0 ) module = hitsMissedInSensor[0]->sensDetID() / 4;
    for ( auto& mcH : hitsMissedInSensor ) {
      // missed MCHits
      auto r = sqrt( mcH->midPoint().x() * mcH->midPoint().x() + mcH->midPoint().y() * mcH->midPoint().y() );
      ++m_missedHitsRPerModule[module][r];
      ++m_missedHitsR7PerModule[module][r];
      if ( trackInfo.hasVelo( mcH->mcParticle() ) ) {
        ++m_missedHitsVeloRPerModule[module][r];
        ++m_missedHitsVeloR7PerModule[module][r];
      }
    }
  }
}
