/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/ToolHandle.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/StateParameters.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Event/TrackEnums.h"
#include "Event/TrackTypes.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbMath/StateVertexUtils.h"
#include "MCInterfaces/IIdealStateCreator.h"

#include "PrKernel/IPrDebugTrackingTool.h"
#include "PrKernel/PrChecker.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "fmt/format.h"

#include <algorithm>
#include <any>
#include <limits>
#include <map>
#include <mutex>
#include <string>
#include <utility>
#include <vector>

/**
 * This class can be used to produce an NTuple containing information about tracks not found by
 * the reconstruction (which reconstruction specifically depends on the config)
 */

namespace LHCb::Pr {
  namespace {
    using Checker::RecAs;
    using Event::Enum::Track::Type;
    constexpr auto nan = std::numeric_limits<float>::quiet_NaN();
    struct MCParticleData : std::map<std::string, IPrDebugTrackingTool::StorageVariant> {};

    struct TrMCPair {
      std::vector<State>       ideal_hit_states{};
      std::vector<StateVector> ideal_states{};
      int                      mc_idx{ -1 };
      int                      velo_idx{ -1 };
      int                      tr_idx{ -1 };
      float                    velo_match_frac{ 0.f };
      float                    match_frac{ 0.f };
      bool                     lost_in_track_fit{ false };
      bool                     lost{ true };
    };

    auto get_mc_hits_positions( const MCParticle* mcp, const MCHits& hits ) {
      auto tmp = std::vector<MCHit*>{};
      std::copy_if( hits.begin(), hits.end(), std::back_inserter( tmp ),
                    [&mcp]( auto hit ) { return mcp == hit->mcParticle(); } );
      auto hit_positions = std::vector<Gaudi::XYZPoint>{};
      std::transform( tmp.begin(), tmp.end(), std::back_inserter( hit_positions ),
                      []( auto hit ) { return hit->midPoint(); } );
      return hit_positions;
    }

    template <typename Func>
    auto get_position_component( span<const Gaudi::XYZPoint> positions, Func&& component ) {
      auto tmp = std::vector<double>{};
      std::transform( positions.begin(), positions.end(), std::back_inserter( tmp ), std::forward<Func>( component ) );
      return tmp;
    }

  } // namespace
  class PrDebugTrackingLosses
      : public Algorithm::Consumer<void( const Track::Range&, const Track::Range&, const MCParticles&, const MCHits&,
                                         const MCHits&, const MCHits&, const LinksByKey&, const LinksByKey&,
                                         const LinksByKey&, const LinksByKey&, const MCProperty&,
                                         const DetectorElement& ),
                                   Algorithm::Traits::usesConditions<DetectorElement>> {
  public:
    PrDebugTrackingLosses( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { KeyValue{ "StudyTracks", "" }, KeyValue{ "VeloTracks", "" }, KeyValue{ "MCParticles", "" },
                      KeyValue{ "MCVPHits", "" }, KeyValue{ "MCUTHits", "" }, KeyValue{ "MCFTHits", "" },
                      KeyValue{ "VeloTrackLinks", "" }, KeyValue{ "TrackLinks", "" }, KeyValue{ "LooseTrackLinks", "" },
                      KeyValue{ "FittedTrackLinks", "" }, KeyValue{ "TrackInfo", LHCb::MCPropertyLocation::TrackInfo },
                      KeyValue{ "StandardGeometryTop", standard_geometry_top } } ){};

    void operator()( const Track::Range&, const Track::Range&, const MCParticles&, const MCHits&, const MCHits&,
                     const MCHits&, const LinksByKey&, const LinksByKey&, const LinksByKey&, const LinksByKey&,
                     const MCProperty&, const DetectorElement& ) const override;

    double estimate_radationlength_fraction( const MCParticle&, const IGeometryInfo&, std::any& ) const;

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        if ( Checker::getReconstructibleType( m_tracktype ) == Checker::RecAs::Unknown ) {
          throw GaudiException(
              "Reconstructibility in PrChecker is not defined for TrackType=" + toString( m_tracktype ) + ".", name(),
              StatusCode::FAILURE );
        }
        return StatusCode::SUCCESS;
      } );
    }

  private:
    Gaudi::Property<Type>               m_tracktype{ this, "TrackType", Type::Long,
                                       "Defines the reconstructibility type considered when checking the MCParticles. "
                                                     "Usually matches the type of the TrackLinks." };
    Gaudi::Property<RecAs>              m_recas{ this, "RecAs", RecAs::Unknown,
                                    "Allows to further restrict the reconstructibility requirement set by TrackType." };
    Gaudi::Property<std::vector<float>> m_ideal_hit_states_z{
        this,
        "IdealStatesZ",
        { StateParameters::ZEndT, StateParameters::ZEndVelo },
        "The z positions at which ideal states are created for each MCParticle." };
    Gaudi::Property<std::pair<double, double>> m_radLengthBetweenZ{
        this, "RadLengthBetweenZ", { StateParameters::ZEndVelo, StateParameters::ZEndUT } };
    Gaudi::Property<double> m_betweenZStepSize{ this, "BetweenZStepSize", 2. * Gaudi::Units::cm };
    // the debug tool provides a convenient way to store information in an NTuple
    ToolHandle<IPrDebugTrackingTool> m_debug_tool{ this, "DebugTool", "PrDebugTrackingTool" };
    ToolHandle<IIdealStateCreator>   m_ideal_state_creator{ this, "IdealStateCreator", "IdealStateCreator" };
    ToolHandle<ITrackExtrapolator>   m_extrap{ this, "Extrapolator", "TrackRungeKuttaExtrapolator" };

    ServiceHandle<ITransportSvc> m_trSvc{ this, "TransportService", "TransportSvc", "transport service" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_mcp{
        this, "Matched non-reconstructible MC particle to a track or it was already removed due to multiple matches." };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_z_pos_above_limit{
        this, "Track's origin vertex z position is larger than upper limit configured by RadLengthBetweenZ." };

    // this algorithm only runs on MC events, so we can use a simple counter as multi-threading is not an issue. For
    // thread-safety we will still lock though.
    mutable std::size_t m_event_counter{};
    mutable std::mutex  m_lock;
  };

  DECLARE_COMPONENT_WITH_ID( PrDebugTrackingLosses, "PrDebugTrackingLosses" )

  void PrDebugTrackingLosses::operator()( const Track::Range& study_tracks, const Track::Range& velo_tracks,
                                          const MCParticles& mc_particles, const MCHits& mc_vphits,
                                          const MCHits& mc_uthits, const MCHits& mc_fthits,
                                          const LinksByKey& velo2mc_link, const LinksByKey& tr2mc_link,
                                          const LinksByKey& loose_tr2mc_link, const LinksByKey& fitted_tr2mc_link,
                                          const MCProperty& mcproperties, const DetectorElement& lhcb ) const {

    const auto track_info        = MCTrackInfo{ mcproperties };
    auto       mc_particles_info = std::vector<TrMCPair>{};
    mc_particles_info.reserve( mc_particles.size() );
    // first store all type-reconstructible MC particles in a vector
    for ( const auto* mcp : mc_particles ) {
      if ( !track_info.fullInfo( mcp ) ) continue;
      const auto trackTypeRecoble = Checker::reconstructibleType( mcp, m_tracktype, track_info ).value_or( false );
      const auto recAsRecoble     = Checker::reconstructibleType( mcp, m_recas, track_info ).value_or( true );
      if ( trackTypeRecoble && recAsRecoble ) {
        // by default all MCPs are assumed to be lost=true, only later tracks are considered
        auto& mcp_info = mc_particles_info.emplace_back();
        // at this point we know that the particle is reconstructible as the type we want. But we do not know if
        // we will find its (Long) track. To have valuable information in any case, get "ideal" MC information
        // about the states at the end of the velo and at the end of the scifi tracker, which are commonly used.
        // It can happen that the extrapolation of a state fails (Check for WARNINGs printed during running).
        // In that case, the StateVector is filled with NaNs.
        mcp_info.ideal_states.reserve( m_ideal_hit_states_z.size() );
        std::transform( m_ideal_hit_states_z.begin(), m_ideal_hit_states_z.end(),
                        std::back_inserter( mcp_info.ideal_states ), [&]( auto z ) {
                          StateVector state{};
                          m_ideal_state_creator->createStateVector( mcp, z, state, *lhcb.geometry() )
                              .orElse( [&state] {
                                state = { { nan, nan, nan, nan, nan }, nan }; // BATMAN!
                              } )
                              .ignore();
                          return state;
                        } );
        mcp_info.ideal_hit_states.reserve( 30 );
        // additionally, get ideal states at the positions of all MCHits of the particle (currently not stored)
        m_ideal_state_creator->getMCHitStates( *mcp, mcp_info.ideal_hit_states ).ignore();
        // finally remember the index of the MCP in the container
        mcp_info.mc_idx = static_cast<int>( mcp->index() );
      }
    }

    // flag all MC particles from the vector that are matched to a track. Also, flag it
    // in the vector if it was found but does not survive the track fit, i.e. is not found in the fitted track links.
    // In this case, however, we also store the track index to be able to understand why a found track did not survive
    // the track fit.
    tr2mc_link.applyToAllLinks( [&]( auto tr_idx, auto mc_idx, auto match_frac ) {
      auto mcp = std::find_if( mc_particles_info.begin(), mc_particles_info.end(),
                               [mc_idx]( auto pair ) { return static_cast<unsigned>( pair.mc_idx ) == mc_idx; } );
      if ( mcp == mc_particles_info.end() ) {
        // we could not find the MCP (matched to a track) in the container of tracktype-reconstructible particles
        if ( msgLevel( MSG::DEBUG ) ) ++m_no_mcp;
        return;
      }
      // arriving here means we have a track matched to the MCP, i.e. it's not lost in the PR!
      // but it might still be lost in the track fit and the following checks if it survives the
      // track fit
      auto lost_in_track_fit = true;
      fitted_tr2mc_link.applyToAllLinks( [&]( auto /*tr_idx*/, auto mc_idx, auto /*match_frac*/ ) {
        if ( static_cast<int>( mc_idx ) == mcp->mc_idx ) {
          lost_in_track_fit = false;
          return;
        }
      } );

      if ( lost_in_track_fit ) {
        // if it was lost in the track fit, store the index of the best track found in the PR with its
        // fraction of matching hits and flag for lost in fit
        if ( match_frac > mcp->match_frac ) {
          mcp->match_frac        = match_frac;
          mcp->tr_idx            = tr_idx;
          mcp->lost_in_track_fit = true;
          mcp->lost              = false;
        }
      } else {
        // this means we found a track from the given category matched to the MCP, i.e. not lost
        mcp->tr_idx     = tr_idx;
        mcp->match_frac = match_frac;
        mcp->lost       = false;
      }
    } );

    // the velo tracking is very efficient for most track types, so let's keep track of a potential velo seed
    velo2mc_link.applyToAllLinks( [&]( auto velo_idx, auto mc_idx, auto match_frac ) {
      auto mcp = std::find_if( mc_particles_info.begin(), mc_particles_info.end(),
                               [mc_idx]( auto pair ) { return static_cast<unsigned>( pair.mc_idx ) == mc_idx; } );
      if ( mcp == mc_particles_info.end() ) return;
      if ( match_frac > mcp->velo_match_frac ) {
        mcp->velo_idx        = velo_idx;
        mcp->velo_match_frac = match_frac;
      }
    } );
    // the default matching fraction is 70% but it might be interesting to look at corresponding tracks which were
    // almost matched, e.g. with a matching fraction of 50%
    loose_tr2mc_link.applyToAllLinks( [&]( auto tr_idx, auto mc_idx, auto match_frac ) {
      auto mcp = std::find_if( mc_particles_info.begin(), mc_particles_info.end(),
                               [mc_idx]( auto pair ) { return static_cast<unsigned>( pair.mc_idx ) == mc_idx; } );
      if ( mcp == mc_particles_info.end() ) return;
      if ( match_frac > mcp->match_frac ) {
        mcp->match_frac = match_frac;
        mcp->tr_idx     = tr_idx;
      }
    } );

    auto accelCache = m_trSvc->createCache();
    auto mcp_data   = MCParticleData{};
    for ( const auto& info : mc_particles_info ) {
      const auto* mcp             = mc_particles( info.mc_idx );
      const auto* destructive_end = mcp->goodEndVertex();
      auto        end_vtxs_types  = std::vector<int>{};
      auto        end_vtxs_x      = std::vector<double>{};
      auto        end_vtxs_y      = std::vector<double>{};
      auto        end_vtxs_z      = std::vector<double>{};
      auto        brem_vtxs_x     = std::vector<double>{};
      auto        brem_vtxs_y     = std::vector<double>{};
      auto        brem_vtxs_z     = std::vector<double>{};
      auto        brem_photons_px = std::vector<double>{};
      auto        brem_photons_py = std::vector<double>{};
      auto        brem_photons_pz = std::vector<double>{};
      auto        brem_photons_pe = std::vector<double>{};
      for ( auto vtx : mcp->endVertices() ) {
        end_vtxs_types.push_back( static_cast<int>( vtx->type() ) );
        end_vtxs_x.push_back( vtx->position().X() );
        end_vtxs_y.push_back( vtx->position().Y() );
        end_vtxs_z.push_back( vtx->position().Z() );
        if ( vtx->type() == MCVertex::Bremsstrahlung ) {
          brem_vtxs_x.push_back( vtx->position().X() );
          brem_vtxs_y.push_back( vtx->position().Y() );
          brem_vtxs_z.push_back( vtx->position().Z() );
          for ( auto p : vtx->products() ) {
            // get brem photons, occasionally there's also -99000000 in the vertex, what is it?
            if ( p->particleID().abspid() == 22 ) {
              brem_photons_px.push_back( p->momentum().X() );
              brem_photons_py.push_back( p->momentum().Y() );
              brem_photons_pz.push_back( p->momentum().Z() );
              brem_photons_pe.push_back( p->momentum().E() );
              break;
            }
          }
        }
      }
      mcp_data.clear();
      mcp_data["p"]                = mcp->p();
      mcp_data["pt"]               = mcp->pt();
      mcp_data["px"]               = mcp->momentum().Px();
      mcp_data["py"]               = mcp->momentum().Py();
      mcp_data["pz"]               = mcp->momentum().Pz();
      mcp_data["energy"]           = mcp->momentum().E();
      mcp_data["tx"]               = mcp->momentum().X() / mcp->momentum().Z();
      mcp_data["ty"]               = mcp->momentum().Y() / mcp->momentum().Z();
      mcp_data["eta"]              = mcp->momentum().Eta();
      mcp_data["phi"]              = mcp->momentum().Phi();
      mcp_data["originvtx_x"]      = mcp->originVertex()->position().x();
      mcp_data["originvtx_y"]      = mcp->originVertex()->position().y();
      mcp_data["originvtx_z"]      = mcp->originVertex()->position().z();
      mcp_data["originvtx_type"]   = static_cast<int>( mcp->originVertex()->type() );
      mcp_data["endvtx_x"]         = destructive_end ? destructive_end->position().x() : nan;
      mcp_data["endvtx_y"]         = destructive_end ? destructive_end->position().y() : nan;
      mcp_data["endvtx_z"]         = destructive_end ? destructive_end->position().z() : nan;
      mcp_data["endvtx_type"]      = static_cast<int>( destructive_end ? destructive_end->type() : MCVertex::Unknown );
      mcp_data["all_endvtx_types"] = end_vtxs_types;
      mcp_data["all_endvtx_x"]     = end_vtxs_x;
      mcp_data["all_endvtx_y"]     = end_vtxs_y;
      mcp_data["all_endvtx_z"]     = end_vtxs_z;
      mcp_data["brem_vtx_x"]       = brem_vtxs_x;
      mcp_data["brem_vtx_y"]       = brem_vtxs_y;
      mcp_data["brem_vtx_z"]       = brem_vtxs_z;
      mcp_data["brem_photons_px"]  = brem_photons_px;
      mcp_data["brem_photons_py"]  = brem_photons_py;
      mcp_data["brem_photons_pz"]  = brem_photons_pz;
      mcp_data["brem_photons_pe"]  = brem_photons_pe;
      mcp_data["fromSignal"]       = mcp->fromSignal();
      mcp_data["mother_id"]        = mcp->mother() ? mcp->mother()->particleID().pid() : -1;
      mcp_data["mother_key"]       = mcp->mother() ? mcp->mother()->key() : -1;
      mcp_data["isElectron"]       = Checker::particleType( mcp, Checker::RecAs::isElectron ).value();
      mcp_data["isMuon"]           = Checker::particleType( mcp, Checker::RecAs::isMuon ).value();
      mcp_data["isPion"]           = Checker::particleType( mcp, Checker::RecAs::isPion ).value();
      mcp_data["isKaon"]           = Checker::particleType( mcp, Checker::RecAs::isKaon ).value();
      mcp_data["isProton"]         = Checker::particleType( mcp, Checker::RecAs::isProton ).value();
      mcp_data["fromB"]            = Checker::originType( mcp, Checker::RecAs::fromB ).value_or( false );
      mcp_data["fromD"]            = Checker::originType( mcp, Checker::RecAs::fromD ).value_or( false );
      mcp_data["fromStrange"]      = Checker::originType( mcp, Checker::RecAs::strange ).value_or( false );
      mcp_data["fromPairProd"]     = Checker::originType( mcp, Checker::RecAs::PairProd ).value_or( false );
      mcp_data["fromHadInt"]       = Checker::originType( mcp, Checker::RecAs::fromHI ).value_or( false );
      mcp_data["fromDecay"]        = Checker::originType( mcp, Checker::RecAs::isDecay ).value_or( false );
      mcp_data["fromPV"]           = Checker::originType( mcp, Checker::RecAs::fromPV ).value_or( false );
      mcp_data["pid"]              = mcp->particleID().pid();
      mcp_data["event_count"]      = static_cast<int>( m_event_counter );

      auto pos_x                  = std::vector<double>{};
      auto pos_y                  = std::vector<double>{};
      auto pos_z                  = std::vector<double>{};
      auto velo_hit_positions     = get_mc_hits_positions( mcp, mc_vphits );
      auto ut_hit_positions       = get_mc_hits_positions( mcp, mc_uthits );
      auto scifi_hit_positions    = get_mc_hits_positions( mcp, mc_fthits );
      auto velo_pos_x             = get_position_component( velo_hit_positions, ROOT::Math::X<Gaudi::XYZPoint> );
      auto velo_pos_y             = get_position_component( velo_hit_positions, ROOT::Math::Y<Gaudi::XYZPoint> );
      auto velo_pos_z             = get_position_component( velo_hit_positions, ROOT::Math::Z<Gaudi::XYZPoint> );
      auto ut_pos_x               = get_position_component( ut_hit_positions, ROOT::Math::X<Gaudi::XYZPoint> );
      auto ut_pos_y               = get_position_component( ut_hit_positions, ROOT::Math::Y<Gaudi::XYZPoint> );
      auto ut_pos_z               = get_position_component( ut_hit_positions, ROOT::Math::Z<Gaudi::XYZPoint> );
      auto scifi_pos_x            = get_position_component( scifi_hit_positions, ROOT::Math::X<Gaudi::XYZPoint> );
      auto scifi_pos_y            = get_position_component( scifi_hit_positions, ROOT::Math::Y<Gaudi::XYZPoint> );
      auto scifi_pos_z            = get_position_component( scifi_hit_positions, ROOT::Math::Z<Gaudi::XYZPoint> );
      mcp_data["velo_hit_pos_x"]  = velo_pos_x;
      mcp_data["velo_hit_pos_y"]  = velo_pos_y;
      mcp_data["velo_hit_pos_z"]  = velo_pos_z;
      mcp_data["ut_hit_pos_x"]    = ut_pos_x;
      mcp_data["ut_hit_pos_y"]    = ut_pos_y;
      mcp_data["ut_hit_pos_z"]    = ut_pos_z;
      mcp_data["scifi_hit_pos_x"] = scifi_pos_x;
      mcp_data["scifi_hit_pos_y"] = scifi_pos_y;
      mcp_data["scifi_hit_pos_z"] = scifi_pos_z;

      const auto* tr = info.tr_idx >= 0 ? study_tracks[info.tr_idx] : nullptr;
      const auto* velo_tr =
          info.velo_idx >= 0 && !velo_tracks[info.velo_idx]->isVeloBackward() ? velo_tracks[info.velo_idx] : nullptr;
      mcp_data["lost_in_track_fit"] = info.lost_in_track_fit;
      mcp_data["lost"]              = info.lost;
      mcp_data["match_fraction"]    = info.match_frac > 0.f ? info.match_frac : nan;
      mcp_data["track_p"]           = tr ? tr->p() : nan;
      mcp_data["track_pt"]          = tr ? tr->pt() : nan;
      // together with the event counter the indices can be checked to find particles that share a Velo track
      mcp_data["mcp_idx"]        = info.mc_idx;
      mcp_data["velo_track_idx"] = info.velo_idx;
      mcp_data["velo_track_x"]   = velo_tr ? velo_tr->stateAt( State::Location::EndVelo )->x() : nan;
      mcp_data["velo_track_y"]   = velo_tr ? velo_tr->stateAt( State::Location::EndVelo )->y() : nan;
      mcp_data["velo_track_z"]   = velo_tr ? velo_tr->stateAt( State::Location::EndVelo )->z() : nan;
      mcp_data["velo_track_tx"]  = velo_tr ? velo_tr->stateAt( State::Location::EndVelo )->tx() : nan;
      mcp_data["velo_track_ty"]  = velo_tr ? velo_tr->stateAt( State::Location::EndVelo )->ty() : nan;

      for ( auto [i, z] : range::enumerate( m_ideal_hit_states_z ) ) {
        const auto key         = fmt::format( "ideal_state_{:.0f}_", z );
        const auto state       = info.ideal_states.at( i );
        mcp_data[key + "_x"]   = state.x();
        mcp_data[key + "_y"]   = state.y();
        mcp_data[key + "_z"]   = state.z();
        mcp_data[key + "_tx"]  = state.tx();
        mcp_data[key + "_ty"]  = state.ty();
        mcp_data[key + "_qop"] = state.qOverP();
      }

      mcp_data["rad_length_frac"] = estimate_radationlength_fraction( *mcp, *lhcb.geometry(), accelCache );

      std::vector<IPrDebugTrackingTool::VariableDef> data( mcp_data.begin(), mcp_data.end() );
      m_debug_tool->storeData( data );
    }
    std::scoped_lock lock{ m_lock };
    ++m_event_counter;
  }

  double PrDebugTrackingLosses::estimate_radationlength_fraction( const MCParticle& mcp, const IGeometryInfo& geo,
                                                                  std::any& cache ) const {
    const auto [z, zMax] = std::minmax( m_radLengthBetweenZ.value().first, m_radLengthBetweenZ.value().second );
    const auto P         = mcp.momentum();
    const auto vtxPos    = mcp.originVertex()->position();
    const auto qop       = ( mcp.particleID().threeCharge() / 3 ) / mcp.p();
    // start from the origin vertex of the MCParticle
    auto state = StateVector{ { vtxPos.x(), vtxPos.y(), P.x() / P.z(), P.y() / P.z(), qop }, vtxPos.z() };
    if ( state.z() > zMax ) {
      ++m_z_pos_above_limit;
      return std::numeric_limits<double>::quiet_NaN();
    }
    // propagate the MCParticle to the configured lower z positon of the range we want to study the rad length for
    if ( z > state.z() && m_extrap->propagate( state.parameters(), state.z(), z, geo )
                              .andThen( [&state, z = z] { state.setZ( z ); } )
                              .isFailure() ) {
      return std::numeric_limits<double>::quiet_NaN();
    }
    // from the lowest z value, we go in configured steps until zMax is too close. At every step,
    // the step distance in units of the radiation length of the encountered material is estimated
    // by the transport service. The radiation length fraction is summed up to eventually obtain
    // the total traversed material in fraction of the radiation length.
    auto totalRadLengthFrac{ 0. };
    for ( auto nextZ = state.z() + m_betweenZStepSize; nextZ < zMax; nextZ += m_betweenZStepSize ) {
      const auto p1 = Gaudi::XYZPoint{ state.x(), state.y(), state.z() };
      if ( m_extrap->propagate( state.parameters(), state.z(), nextZ, geo )
               .andThen( [&state, nextZ = nextZ] { state.setZ( nextZ ); } )
               .isFailure() ) {
        return std::numeric_limits<double>::quiet_NaN();
      }
      const auto p2 = Gaudi::XYZPoint{ state.x(), state.y(), state.z() };
      // distance in units of the radiation length between p1 and p2 can simply be added up
      try {
        totalRadLengthFrac += m_trSvc->distanceInRadUnits( p1, p2, cache, geo );
      } catch ( const TransportSvcException& ) { return std::numeric_limits<double>::quiet_NaN(); }
    }

    // at the end we still want to go the remaing way to zMax (if the distance is not too small)
    assert( std::abs( state.z() - zMax ) <= m_betweenZStepSize );
    if ( std::abs( state.z() - zMax ) > TrackParameters::propagationTolerance ) {
      const auto p1 = Gaudi::XYZPoint{ state.x(), state.y(), state.z() };
      m_extrap->propagate( state.parameters(), state.z(), zMax, geo )
          .andThen( [&state, zMax = zMax] { state.setZ( zMax ); } )
          .ignore();
      const auto p2 = Gaudi::XYZPoint{ state.x(), state.y(), state.z() };
      try {
        totalRadLengthFrac += m_trSvc->distanceInRadUnits( p1, p2, cache, geo );
      } catch ( const TransportSvcException& ) { return std::numeric_limits<double>::quiet_NaN(); }
    }
    return totalRadLengthFrac;
  }

} // namespace LHCb::Pr
