/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/GaudiException.h"

#include "Event/MCParticle.h"
#include "Event/MCTrackInfo.h"
#include "Event/TrackEnums.h"
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/meta_enum.h"

#include <algorithm>
#include <array>
#include <optional>
#include <string_view>

namespace LHCb::Pr::Checker {

  meta_enum_class( RecAs, int, Unknown, isLong, isNotLong, isDown, isNotDown, isUp, isNotUp, isVelo, isNotVelo, isUT,
                   isNotUT, isSeed, isNotSeed, strange, fromB, fromD, fromKsFromB, isElectron, isNotElectron,
                   BOrDMother, PairProd, isDecay, fromHI, fromPV, hasVeloOverlap, hasVeloCrossingSide,
                   muonHitsInAllStations, muonHitsInAtLeastTwoStations, isMuon, isPion, fromSignal, isKaon, isProton );

  inline RecAs getReconstructibleType( Event::Enum::Track::Type trackType ) {
    using Event::Enum::Track::Type;
    switch ( trackType ) {
    case Type::Velo:
    case Type::VeloBackward:
      return Checker::RecAs::isVelo;
    case Type::Long:
      return Checker::RecAs::isLong;
    case Type::Upstream:
      return Checker::RecAs::isUp;
    case Type::Downstream:
      return Checker::RecAs::isDown;
    case Type::Ttrack:
      return Checker::RecAs::isSeed;
    case Type::UT:
      return Checker::RecAs::isUT;
    case Type::Muon:
    case Type::SeedMuon:
    case Type::VeloMuon:
    case Type::MuonUT:
    case Type::LongMuon:
    case Type::Last:
    case Type::Unknown:
      return Checker::RecAs::Unknown;
    }
    __builtin_unreachable();
  }

  inline std::optional<bool> reconstructibleType( const MCParticle* mcp, RecAs kind, const MCTrackInfo& mcInfo ) {
    switch ( kind ) {
    case RecAs::isLong:
      return mcInfo.hasVeloAndT( mcp );
    case RecAs::isNotLong:
      return !mcInfo.hasVeloAndT( mcp );
    case RecAs::isDown:
      return mcInfo.hasT( mcp ) && mcInfo.hasUT( mcp );
    case RecAs::isNotDown:
      return !( mcInfo.hasT( mcp ) && mcInfo.hasUT( mcp ) );
    case RecAs::isUp:
      return mcInfo.hasVelo( mcp ) && mcInfo.hasUT( mcp );
    case RecAs::isNotUp:
      return !( mcInfo.hasVelo( mcp ) && mcInfo.hasUT( mcp ) );
    case RecAs::isVelo:
      return mcInfo.hasVelo( mcp );
    case RecAs::isNotVelo:
      return !mcInfo.hasVelo( mcp );
    case RecAs::isSeed:
      return mcInfo.hasT( mcp );
    case RecAs::isNotSeed:
      return !mcInfo.hasT( mcp );
    case RecAs::isUT:
      return mcInfo.hasUT( mcp );
    case RecAs::isNotUT:
      return !mcInfo.hasUT( mcp );
    default:
      return std::nullopt;
    }
  }

  inline std::optional<bool> reconstructibleType( const MCParticle* mcp, Event::Enum::Track::Type type,
                                                  const MCTrackInfo& mcInfo ) {
    return reconstructibleType( mcp, getReconstructibleType( type ), mcInfo );
  }

  inline std::optional<bool> particleType( const MCParticle* mcp, RecAs kind ) {
    switch ( kind ) {
    case RecAs::isElectron:
      return std::abs( mcp->particleID().pid() ) == 11;
    case RecAs::isNotElectron:
      return std::abs( mcp->particleID().pid() ) != 11;
    case RecAs::isMuon:
      return std::abs( mcp->particleID().pid() ) == 13;
    case RecAs::isPion:
      return std::abs( mcp->particleID().pid() ) == 211;
    case RecAs::isKaon:
      return std::abs( mcp->particleID().pid() ) == 321;
    case RecAs::isProton:
      return std::abs( mcp->particleID().pid() ) == 2212;
    case RecAs::fromSignal:
      return mcp->fromSignal();
    default:
      return std::nullopt;
    }
  }

  inline std::optional<bool> originType( const MCParticle* mcp, RecAs kind ) {
    if ( !mcp->originVertex() ) return false;
    const auto* mother = mcp->originVertex()->mother();
    if ( mother ) {
      if ( mother->originVertex() ) {
        double rOrigin = mother->originVertex()->position().rho();
        if ( std::abs( rOrigin ) < 5. ) {
          int pid = std::abs( mother->particleID().pid() );
          // -- MCParticle is coming from a strang particle
          if ( kind == RecAs::strange ) {
            return ( 130 == pid ||  // K0L
                     310 == pid ||  // K0S
                     321 == pid ||  // K+
                     3122 == pid || // Lambda
                     3222 == pid || // Sigma+
                     3212 == pid || // Sigma0
                     3112 == pid || // Sigma-
                     3322 == pid || // Xsi0
                     3312 == pid || // Xsi-
                     3334 == pid    // Omega-
            );
          }
          // -- It's a Kshort from a b Hadron
          if ( kind == RecAs::fromKsFromB ) {
            auto gmom = mother->originVertex()->mother();
            return gmom && 310 == pid && 2 == mcp->originVertex()->products().size() &&
                   gmom->particleID().hasBottom() && ( gmom->particleID().isMeson() || gmom->particleID().isBaryon() );
          }
        }
      }
    }
    // -- It's a daughter of a B or D hadron
    bool motherB = false;
    bool motherD = false;
    for ( ; mother; mother = mother->originVertex()->mother() ) {
      if ( mother->particleID().isMeson() || mother->particleID().isBaryon() ) {
        if ( mother->particleID().hasBottom() ) motherB = true;
        if ( mother->particleID().hasCharm() ) motherD = true;
      }
    }
    switch ( kind ) {
    case RecAs::fromD:
      return motherD;
    case RecAs::fromB:
      return motherB;
    case RecAs::BOrDMother:
      return motherD || motherB;
    default:;
    }
    // -- It's from a decay, from gamma->ee pair production or from Hadronic Interaction.
    // -- isDecay includes both DecayVertex and OscillatedAndDecay
    auto t = mcp->originVertex()->type();
    switch ( kind ) {
    case RecAs::isDecay:
      return t == LHCb::MCVertex::MCVertexType::DecayVertex || t == LHCb::MCVertex::MCVertexType::OscillatedAndDecay;
    case RecAs::PairProd:
      return t == LHCb::MCVertex::MCVertexType::PairProduction;
    case RecAs::fromHI:
      return t == LHCb::MCVertex::MCVertexType::HadronicInteraction;
    case RecAs::fromPV:
      return t == LHCb::MCVertex::MCVertexType::ppCollision;
    default:
      return std::nullopt;
    }
  }
} // namespace LHCb::Pr::Checker
