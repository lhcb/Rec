/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "FTDAQ/FTInfo.h"
#include "FTDet/DeFTDetector.h"

#include "GaudiAlg/GaudiAlgorithm.h"

#include "Core/FloatComparison.h"

#include <array>
#include <cstdint>

namespace LHCb::Detector::FT::Cache {
  struct MatsCache {
    static constexpr std::string_view Location = "AlgorithmSpecific-FTMatsCache";
    /**
     * partial SoA cache for mats, reserve enough (here 4096 which is more than enough)
     * space for all mats ( all mats should be less than 4 mats per module * 8 modules * 4 quarters * 16 layers)
     * This could be 'contracted' to a smaller, contiguous container, but at the price of complexity.
     */
    std::array<float, maxNumberMats>                  dxdy{};
    std::array<float, maxNumberMats>                  dzdy{};
    std::array<float, maxNumberMats>                  globaldy{};
    std::array<ROOT::Math::XYZPointF, maxNumberMats>  mirrorPoint{};
    std::array<ROOT::Math::XYZVectorF, maxNumberMats> ddx{};
    std::array<std::vector<double>, maxNumberMats>    matContractionParameterVector{};
    std::uint16_t                                     nInvalid = 0;
    float                                             uBegin{};
    float                                             halfChannelPitch{};
    float                                             dieGap{};
    float                                             sipmPitch{};

    std::tuple<float, float, float, float> calculateXYZFromChannel( const LHCb::Detector::FTChannelID& id,
                                                                    int clusFracBit ) const;
    float calculateCalibratedX( const LHCb::Detector::FTChannelID& id, float x0_orig ) const;

    MatsCache( const DeFT& ftDet, const Gaudi::Algorithm* parent = nullptr,
               const bool applyMatContractionCalibration = true ) {
      const auto first_mat = ftDet.firstMat();
      // This parameters are constant accross all mats:
#ifdef USE_DD4HEP
      this->dieGap           = first_mat.dieGap();
      this->sipmPitch        = first_mat.sipmPitch();
      this->uBegin           = first_mat.uBegin();
      this->halfChannelPitch = first_mat.halfChannelPitch();
#else
      this->dieGap           = first_mat->dieGap();
      this->sipmPitch        = first_mat->sipmPitch();
      this->uBegin           = first_mat->uBegin();
      this->halfChannelPitch = first_mat->halfChannelPitch();
#endif
      auto func = [this, applyMatContractionCalibration]( const DeFTMat& mat ) {
        assert( LHCb::essentiallyEqual( this->dieGap, mat.dieGap() ) && "Unexpected difference in dieGap" );
        assert( LHCb::essentiallyEqual( this->sipmPitch, mat.sipmPitch() ) && "Unexpected difference in sipmPitch" );
        assert( LHCb::essentiallyEqual( this->uBegin, mat.uBegin() ) && "Unexpected difference in uBegin" );
        assert( LHCb::essentiallyEqual( this->halfChannelPitch, mat.halfChannelPitch() ) &&
                "Unexpected difference in halfChannelPitch" );
        //---LoH: This is the only 'index' in SciFi software that is, in fact, an ID.
        //        It is kept as-is for simplicity. But this is neither contiguous
        //        nor well-ordered in terms of x.
        const auto matId                           = mat.elementID();
        const auto index                           = matId.globalMatID();
        this->mirrorPoint[index]                   = mat.mirrorPoint();
        this->ddx[index]                           = mat.ddx();
        this->dxdy[index]                          = mat.dxdy();
        this->dzdy[index]                          = mat.dzdy();
        this->globaldy[index]                      = mat.globaldy();
        this->matContractionParameterVector[index] = mat.getmatContractionParameterVector();

        if ( applyMatContractionCalibration ) { // compute all the calibrated channel positions and ensure always x_i-1
                                                // < x_i
          // check the readout direction
          const bool normalReadoutDir = matId.normalReadoutDir();
          float      prevX            = ( normalReadoutDir ? -1 : 1 ) * 10e3;

          bool alwaysIncreasing = true;

          // loop over SiPMs
          for ( size_t sipm = 0; sipm < FTConstants::nSiPM; sipm++ ) {
            // loop over channels
            for ( size_t chan = 1; chan < FTConstants::nChannels; chan++ ) {
              const auto id = LHCb::Detector::FTChannelID( mat.stationID(), mat.layerID(), mat.quarterID(),
                                                           mat.moduleID(), mat.matID(), sipm, chan );

              const auto [x0_orig, yMin, yMax, z0] = calculateXYZFromChannel( id, 0 );
              auto x0                              = x0_orig;
              if ( !this->matContractionParameterVector[index].empty() ) { // is applyToAllMats SIMD? Is this safe?
                x0 = calculateCalibratedX( id, x0_orig );
              }

              const bool increasingNormal  = ( x0 > prevX ) && normalReadoutDir;
              const bool decreasingReverse = ( x0 < prevX ) && !normalReadoutDir;
              alwaysIncreasing             = alwaysIncreasing && ( increasingNormal || decreasingReverse );
              prevX                        = x0;
            }
          }
          this->nInvalid += !alwaysIncreasing;

          std::transform( this->matContractionParameterVector[index].begin(),
                          this->matContractionParameterVector[index].end(),
                          this->matContractionParameterVector[index].begin(), [alwaysIncreasing]( double value ) {
                            return alwaysIncreasing * value; // if the mat is not always increasing then
                                                             // all the values are overwritten with a 0 for that mat
                          } );
        }
      };
      ftDet.applyToAllMats( func );
      if ( applyMatContractionCalibration && parent && nInvalid ) {
        parent->warning() << "Applying mat-end calibration would break x-ordering in " << nInvalid
                          << " mats. In order to prevent this, the mat-end calibration is now turned off in these mats."
                          << endmsg;
      }
    };
  };

  std::tuple<float, float, float, float> MatsCache::calculateXYZFromChannel( const LHCb::Detector::FTChannelID& id,
                                                                             int clusFracBit ) const {
    const auto hfChPitch    = ( 2 * id.channel() + 1 + clusFracBit ) * this->halfChannelPitch;
    const auto dieGap       = id.die() * this->dieGap;
    const auto sipmPitch    = id.sipm() * this->sipmPitch;
    const auto uFromChannel = this->uBegin + hfChPitch + dieGap + sipmPitch;
    const auto index        = id.globalMatID();
    const auto endPoint     = this->mirrorPoint[index] + this->ddx[index] * uFromChannel;

    const auto dxdy = this->dxdy[index];
    const auto dzdy = this->dzdy[index];
    auto       yMin = endPoint.y();
    const auto x0   = endPoint.x() - dxdy * yMin;
    const auto z0   = endPoint.z() - dzdy * yMin;
    auto       yMax = yMin + this->globaldy[index];
    if ( id.isBottom() ) std::swap( yMin, yMax );

    return { x0, yMin, yMax, z0 };
  }

  float MatsCache::calculateCalibratedX( const LHCb::Detector::FTChannelID& id, float x0_orig ) const {
    const auto  index                         = id.globalMatID();
    const auto& matContractionParameterVector = this->matContractionParameterVector[index];
    const auto  matContraction =
        matContractionParameterVector.at( ( id.sipm() * FTConstants::nChannels ) + id.channel() );
    const auto calibratedDdx = this->ddx[index] * matContraction;
    const auto x0Calibration = calibratedDdx.x() - this->dxdy[index] * calibratedDdx.y();
    return x0_orig + x0Calibration;
  }

} // namespace LHCb::Detector::FT::Cache
