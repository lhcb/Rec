/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "FTDAQ/FTInfo.h"
#include "FTDet/DeFTDetector.h"
#include <array>
#include <fmt/ostream.h>
#include <limits>
#include <string_view>

namespace LHCb::Detector::FT::Cache {

  struct Plane {
    float z{};
    float dxdy{};
    float dzdy{};
  };

  /**
   * @brief This object caches geometry information of the SciFi tracker as needed by the
   * pattern recognition algorithms.
   * @details The GeometryCache stores two kind of information: z, dxdy, dzdy of SciFi layers
   * which comes directly from the geometry and is not subject to alignment, and the same
   * quantities for SciFi quarters which are derived as the average of the respective module
   * quantities and thus profit from alignment.
   */
  struct GeometryCache {
    static constexpr std::string_view Location = "AlgorithmSpecific-FTGeometryCache";

    GeometryCache(){};
    GeometryCache( const DeFT& ftDet ) {
#ifndef USE_DD4HEP
      auto iLayer{ 0 };
#endif
      ftDet.applyToAllLayers( [&]( const DeFTLayer& layer ) {
#ifdef USE_DD4HEP
        const auto index = layer.layerIdx();
#else
        const auto index = iLayer++;
#endif
        m_layers.at( index ) = { layer.globalZ(), layer.dxdy(), layer.dzdy() };
      } );
#ifndef USE_DD4HEP
      auto iQuarter{ 0 };
#endif
      ftDet.applyToAllQuarters( [&]( const DeFTQuarter& quarter ) {
#ifdef USE_DD4HEP
        const auto index = quarter.quarterIdx();
#else
        const auto index = iQuarter++;
#endif
        m_quarters.at( index ) = { quarter.meanModuleZ(), quarter.meanModuleDxdy(), quarter.meanModuleDzdy() };
      } );
    };

    /**
     * @brief Get z position of A or C side of a zone (i.e. a quarter).
     *
     * @param iZone Index of the zone.
     * @param side A or C side of the detector.
     * @return This is the z position of all quarter modules averaged, i.e. changes with alignment.
     */
    auto z( int iZone, LHCb::Detector::FTChannelID::Side side ) const { return m_quarters[quarter( iZone, side )].z; }
    /**
     * @brief Get z position of a layer.
     *
     * @param iLayer Index of the layer.
     * @return This is the z position of the layer as defined in the geometry.
     */
    auto z( int iLayer ) const { return m_layers[iLayer].z; }
    /**
     * @brief Get dzdy slope of A or C side of a zone (i.e. a quarter).
     *
     * @param iZone Index of the zone.
     * @param side A or C side of the detector.
     * @return This is the dzdy slope of all quarter modules averaged, i.e. changes with alignment.
     */
    auto dzdy( int iZone, LHCb::Detector::FTChannelID::Side side ) const {
      return m_quarters[quarter( iZone, side )].dzdy;
    }
    /**
     * @brief Get dzdy slope of a layer.
     *
     * @param iLayer Index of the layer.
     * @return This is the dzdy slope of the layer as defined in the geometry.
     */
    auto dzdy( int iLayer ) const { return m_layers[iLayer].dzdy; }
    /**
     * @brief Get dxdy slope of A or C side of a zone (i.e. a quarter).
     *
     * @param iZone Index of the zone.
     * @param side A or C side of the detector.
     * @return This is the dxdy slope of all quarter modules averaged, i.e. changes with alignment.
     */
    auto dxdy( int iZone, LHCb::Detector::FTChannelID::Side side ) const {
      return m_quarters[quarter( iZone, side )].dxdy;
    }
    /**
     * @brief Get dxdy slope of a layer.
     *
     * @param iLayer Index of the layer.
     * @return This is the dxdy slope of the layer as defined in the geometry.
     */
    auto                 dxdy( int iLayer ) const { return m_layers[iLayer].dxdy; }
    friend std::ostream& operator<<( std::ostream& os, const GeometryCache& c );

  private:
    int quarter( int iZone, LHCb::Detector::FTChannelID::Side side ) const { return 2 * iZone + to_unsigned( side ); }
    std::array<Plane, nLayersTotal>   m_layers{ { { std::numeric_limits<float>::signaling_NaN() } } };
    std::array<Plane, nQuartersTotal> m_quarters{ { { std::numeric_limits<float>::signaling_NaN() } } };
  };

  inline std::ostream& operator<<( std::ostream& os, const GeometryCache& c ) {
    fmt::print( os, "LHCb::Detector::FT::Cache::GeometryCache:\n" );
    for ( auto layer{ 0u }; layer < nLayersTotal; ++layer ) {
      fmt::print( os, "\n=================Layer {:2}==================\n", layer );
      fmt::print( os, "(z = {:6.1f}, dxdy = {:7.5f}, dzdy = {:7.5f})\n", c.m_layers[layer].z, c.m_layers[layer].dxdy,
                  c.m_layers[layer].dzdy );
      fmt::print( os, "\n--------------------------------------------\n" );
      const auto q = nQuarters * layer;
      fmt::print( os,
                  "z     = {:11.5f}  |  z     = {:11.5f}\ndxdy  = {:11.5f}  |  dxdy  = {:11.5f}\ndzdy  = {:11.5f}  | "
                  " dzdy  = "
                  "{:11.5f}",
                  c.m_quarters[q + 3].z, c.m_quarters[q + 2].z, c.m_quarters[q + 3].dxdy, c.m_quarters[q + 2].dxdy,
                  c.m_quarters[q + 3].dzdy, c.m_quarters[q + 2].dzdy );
      fmt::print( os, "\n--------------------------------------------\n" );
      fmt::print(
          os,
          "z     = {:11.5f}  |  z     = {:11.5f}\ndxdy  = {:11.5f}  |  dxdy  = {:11.5f}\ndzdy  = {:11.5f}  |  dzdy  "
          "= {:11.5f}",
          c.m_quarters[q + 1].z, c.m_quarters[q + 0].z, c.m_quarters[q + 1].dxdy, c.m_quarters[q + 0].dxdy,
          c.m_quarters[q + 1].dzdy, c.m_quarters[q + 0].dzdy );
      fmt::print( os, "\n--------------------------------------------\n" );
      fmt::print( os, "\n============================================\n" );
    }
    return os;
  }
} // namespace LHCb::Detector::FT::Cache
