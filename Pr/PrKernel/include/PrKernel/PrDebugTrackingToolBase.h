/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "PrKernel/IPrDebugTrackingTool.h"

#include "GaudiAlg/GaudiTupleTool.h"

#include <string>
#include <string_view>
#include <type_traits>
#include <variant>
#include <vector>

struct PrDebugTrackingToolBase : public extends<GaudiTupleTool, IPrDebugTrackingTool> {

  // inherit standard constructors
  using extends::extends;

  // helper to detect std::vector
  template <typename T>
  struct is_vector : std::false_type {};
  template <typename... Ts>
  struct is_vector<std::vector<Ts...>> : std::true_type {};
  /**
   * @brief This is the default implementation for storing data in the tool interface.
   *
   * @param vars_and_values
   * @param tuple_name
   *
   * @note Supports tupling of int, float, std::vector<int>, std::vector<float>. The vectors
   * have an arbitrary size limit of 1024 entries.
   */

  virtual void storeData( LHCb::span<const VariableDef> vars_and_values, std::string_view tuple_name ) const override {

    Tuple tuple = nTuple( std::string{ tuple_name } );
    for ( auto [name, value] : vars_and_values ) {
      std::visit(
          [name = name, &tuple]( const auto& v ) {
            using Type = std::decay_t<decltype( v )>;
            if constexpr ( is_vector<Type>::value ) {
              return tuple->farray( name, v, std::string{ name } + "_length", 1024 );
            } else {
              return tuple->column( name, v );
            }
          },
          value )
          .ignore();
    }
    tuple->write().ignore();
  }
};
