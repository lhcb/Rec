/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FTDAQ/FTDAQHelper.h"
#include "UTDAQ/UTDAQHelper.h"

#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/FilterPredicate.h"

namespace LHCb::Pr {
  class PrGECFilter : public Algorithm::FilterPredicate<bool( const RawBank::View&, const RawBank::View& )> {

  public:
    PrGECFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : FilterPredicate(
              name, pSvcLocator,
              { KeyValue{ "FTRawBanks", "DAQ/RawBanks/FTCluster" }, KeyValue{ "UTRawBanks", "DAQ/RawBanks/UT" } } ) {}

    /// Algorithm execution
    bool operator()( const RawBank::View& ftBanks, const RawBank::View& utBanks ) const override {
      ++m_eventsProcessedCounter;

      // do not work for nothing !
      if ( m_nFTUTClusters <= 0 ) { return true; }

      if ( m_skipUT ) {
        if ( FTDAQ::nbFTClusters( ftBanks ) < m_nFTUTClusters ) { return true; }
        ++m_eventsRejectedCounter;
        return false;
      }

      // check UT clusters
      auto nbUTClusters = UTDAQ::nbUTClusters( utBanks, m_nFTUTClusters );
      if ( !nbUTClusters ) {
        ++m_eventsRejectedCounter;
        return false;
      }

      // check FT + UT clusters
      if ( FTDAQ::nbFTClusters( ftBanks ) + nbUTClusters.value() < m_nFTUTClusters ) { return true; }
      ++m_eventsRejectedCounter;
      return false;
    }

  private:
    Gaudi::Property<unsigned int> m_nFTUTClusters{ this, "NumberFTUTClusters", 0 };
    Gaudi::Property<bool>         m_skipUT{ this, "SkipUT", false, "Ignore UT raw banks" };

    mutable Gaudi::Accumulators::Counter<> m_eventsProcessedCounter{ this, "Nb Events Processed" };
    mutable Gaudi::Accumulators::Counter<> m_eventsRejectedCounter{ this, "Nb events removed" };
  };

  DECLARE_COMPONENT_WITH_ID( PrGECFilter, "PrGECFilter" )

} // namespace LHCb::Pr
