/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Gaudi/Accumulators.h"

#include "CaloFutureUtils/TrackUtils.h"
#include "Event/SOAZip.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Event/ZipUtils.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/SIMDWrapper.h"

#include "Functors/Filter.h"
#include "Functors/with_functors.h"

#include <limits>
#include <tuple>
#include <type_traits>
#include <utility>

namespace LHCb::Pr {

  namespace {
    using OutTracks = Event::v3::Tracks;
    template <typename T, template <typename...> typename BasicType>
    struct is_specialisation : std::false_type {};

    template <template <typename...> typename BasicType, typename... Args>
    struct is_specialisation<BasicType<Args...>, BasicType> : std::true_type {};

    template <typename T>
    struct always_false : std::false_type {};

    template <typename T>
    struct FilterCut {
      constexpr static auto PropertyName = "Cut";
      using Signature                    = Functors::filtered_t<T>( T const& );
    };

    template <typename Relation>
    using ViewType = typename Relation::template view_t<const Relation>;

    template <typename Relation>
    using ZipType = Event::zip_t<OutTracks, ViewType<Relation>>;

  } // namespace

  template <typename Relation>
  struct FilterTrackRelation
      : public with_functors<Algorithm::Transformer<OutTracks( const Relation& )>, FilterCut<ZipType<Relation>>> {

    using base_class_t =
        with_functors<Algorithm::Transformer<OutTracks( const Relation& )>, FilterCut<ZipType<Relation>>>;

    FilterTrackRelation( const std::string& name, ISvcLocator* pSvcLocator )
        : base_class_t( name, pSvcLocator, { typename base_class_t::KeyValue{ "Relation", "" } },
                        typename base_class_t::KeyValue{ "Output", "" } ) {}

    OutTracks operator()( const Relation& relation ) const override {

      const auto& pred = this->template getFunctor<FilterCut<ZipType<Relation>>>();

      const auto relView = [&] {
        if constexpr ( is_specialisation<Relation, Event::RelationTable1D>::value ) {
          return relation.buildView();
        } else if constexpr ( is_specialisation<Relation, Event::RelationTable2D>::value ) {
          return relation.buildFromView();
        } else {
          static_assert( always_false<Relation>::value, "Only RelationTable 1D and 2D supported." );
        }
      }();

      const auto zipView = Event::make_zip( *relation.from(), relView );

      auto filteredTracks = std::get<OutTracks>( pred( zipView ) );
      m_cutEff += { filteredTracks.size(), zipView.size() };
      return filteredTracks;
    };

  private:
    mutable Gaudi::Accumulators::BinomialCounter<> m_cutEff{ this, "Cut selection efficiency" };
  };

  using PrFilterTracks2CaloClusters   = FilterTrackRelation<Calo::TrackUtils::Tracks2Clusters>;
  using PrFilterTracks2ElectronMatch  = FilterTrackRelation<Calo::TrackUtils::Tracks2Electrons>;
  using PrFilterTracks2ElectronShower = FilterTrackRelation<Calo::TrackUtils::TracksElectronShower>;

  DECLARE_COMPONENT_WITH_ID( PrFilterTracks2CaloClusters, "PrFilterTracks2CaloClusters" )
  DECLARE_COMPONENT_WITH_ID( PrFilterTracks2ElectronMatch, "PrFilterTracks2ElectronMatch" )
  DECLARE_COMPONENT_WITH_ID( PrFilterTracks2ElectronShower, "PrFilterTracks2ElectronShower" )

} // namespace LHCb::Pr
