/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PrFilters/PrFilter.h"
#include "Event/Particle_v2.h"
#include "Event/PrVeloTracks.h"
#include "Event/Track_v3.h"
#include "PrKernel/PrSelection.h"
#include "TrackKernel/PrimaryVertexUtils.h"

namespace LHCb::Pr {

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Pr::Velo::Tracks>, "PrFilter__PrVeloTracks" )

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::v3::Tracks>, "PrFilter__PrFittedForwardTracks" )

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::v3::TracksWithPVs>, "PrFilter__PrFittedForwardTracksWithPVs" )

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::v3::TracksWithMuonID>, "PrFilter__PrFittedForwardTracksWithMuonID" )

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::Composites>, "PrFilter__Composites" )

  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::Event::PV::PrimaryVertexContainer>, "PrFilter__ExtendedPVs" )
  DECLARE_COMPONENT_WITH_ID( Filter<LHCb::PrimaryVertices>, "PrFilter__PV" )
} // namespace LHCb::Pr
