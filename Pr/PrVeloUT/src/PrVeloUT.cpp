/***************************************************************************** \
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "PrUTMagnetTool.h"

#include <algorithm>
#include <array>
#include <limits>
#include <vector>

#include "Gaudi/Accumulators.h"
#include "GaudiKernel/EventContext.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Magnet/DeMagnet.h"
#include "UTDet/DeUTDetector.h"

#include "Event/PrHits.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/SOACollection.h"
#include "Event/UTSectorHelper.h"
#include "Event/UTTrackUtils.h"
#include "Event/ZipUtils.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/SIMDWrapper.h"

#include "PrKernel/PrMutUTHits.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTInfo.h"

#include "vdt/sqrt.h"

namespace LHCb::Pr::VeloUT {
  namespace {
    using simd      = SIMDWrapper::best::types;
    using scalar    = SIMDWrapper::scalar::types;
    using TracksTag = Upstream::Tag;

    namespace HitTag = UT::Mut::HitTag;
    namespace TU     = LHCb::UT::TrackUtils;

    constexpr auto EndVelo       = Event::Enum::State::Location::EndVelo;
    constexpr auto nanMomentum   = std::numeric_limits<float>::quiet_NaN();
    constexpr auto totalUTLayers = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    // -- Used for the calculation of the size of the search windows
    constexpr std::array<float, totalUTLayers> normFact{ 0.95f, 1.0f, 1.36f, 1.41f };
    using LooseBounds   = std::array<TU::BoundariesLoose, totalUTLayers>;
    using NominalBounds = std::array<TU::BoundariesNominal, totalUTLayers>;

    struct ProtoTrackTag {
      struct InitialWeight : Event::float_field {};
      struct XMidField : Event::float_field {};
      struct InvKinkVeloDist : Event::float_field {};
      // -- this is for the hits
      // -- this does _not_ include overlap hits, so only 4 per track
      struct X : Event::floats_field<4> {};
      struct Z : Event::floats_field<4> {};
      struct Weight : Event::floats_field<4> {};
      struct Sin : Event::floats_field<4> {};
      struct ID : Event::ints_field<4> {};
      struct HitIndex : Event::ints_field<4> {};
      // fit output
      struct QOverP : Event::float_field {};
      struct Chi2 : Event::float_field {};
      struct XOffset : Event::float_field {};
      struct XSlope : Event::float_field {};
      struct YSlope : Event::float_field {};
      // -- and this the original state (in the Velo)
      struct VeloState : Event::pos_dir_field {};
      // -- and this an index to find the hit containers
      struct Ancestor : Event::int_field {};
      struct HitIndexContainer : Event::int_field {};

      template <typename T>
      using prototrack_t =
          Event::SOACollection<T, InitialWeight, XMidField, InvKinkVeloDist, X, Z, Weight, Sin, ID, HitIndex, QOverP,
                               Chi2, XOffset, XSlope, YSlope, VeloState, Ancestor, HitIndexContainer>;
    };

    struct ProtoTracks : ProtoTrackTag::prototrack_t<ProtoTracks> {
      using base_t = typename ProtoTrackTag::prototrack_t<ProtoTracks>;
      using base_t::allocator_type;
      using base_t::base_t;

      template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
      struct ProtoTrackProxy : Event::Proxy<simd, behaviour, ContainerType> {
        using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
        using base_t::base_t;
        auto isInvalid() const { return this->template field<ProtoTrackTag::HitIndex>( 0 ).get() == -1; }
      };

      template <SIMDWrapper::InstructionSet simd, ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = ProtoTrackProxy<simd, behaviour, ContainerType>;
    };

    struct VeloState {
      float x, y, z, tx, ty;
    };

    struct VeloUTGeomCache {
      VeloUTGeomCache( const DeUTDetector& det, const UTMagnetTool::Cache& magtoolcache ) : common{ det } {
        // m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
        // Cached once in VeloUTTool at initialization. No need to update with small UT movement.
        zMidUT = magtoolcache.zCenterUT;
        // zMidField and distToMomentum is properly recalculated in PrUTMagnetTool when B field changes
        distToMomentum = 1.0f / magtoolcache.dist2mom;
      }
      UTDAQ::GeomCache common;
      float            zMidUT{ 0.f };
      float            distToMomentum{ 0.f };
    };

    simd::mask_v CholeskyDecomposition3( const std::array<simd::float_v, 6>& mat, std::array<simd::float_v, 3>& rhs ) {
      // -- copied from Root::Math::CholeskyDecomp
      // -- first decompose
      std::array<simd::float_v, 6> dst;
      simd::mask_v                 mask = !( mat[0] > simd::float_v{ 0.0f } );
      dst[0]                            = max( 1e-6f, dst[0] ); // that's only needed if you care about FPE
      dst[0]                            = 1.0f / sqrt( mat[0] );
      dst[1]                            = mat[1] * dst[0];
      dst[2]                            = mat[2] - dst[1] * dst[1];
      mask                              = mask || ( dst[2] < simd::float_v{ 0.0f } );
      dst[2]                            = max( 1e-6f, dst[2] ); // that's only needed if you care about FPE
      dst[2]                            = 1.0f / sqrt( dst[2] );
      dst[3]                            = mat[3] * dst[0];
      dst[4]                            = ( mat[4] - dst[1] * dst[3] ) * dst[2];
      dst[5]                            = mat[5] - ( dst[3] * dst[3] + dst[4] * dst[4] );
      mask                              = mask || ( dst[5] < simd::float_v{ 0.0f } );
      dst[5]                            = max( 1e-6f, dst[5] ); // that's only needed if you care about FPE
      dst[5]                            = 1.0f / sqrt( dst[5] );

      // -- then solve
      // -- solve Ly = rhs
      const simd::float_v y0 = rhs[0] * dst[0];
      const simd::float_v y1 = ( rhs[1] - dst[1] * y0 ) * dst[2];
      const simd::float_v y2 = ( rhs[2] - ( dst[3] * y0 + dst[4] * y1 ) ) * dst[5];
      // solve L^Tx = y, and put x into rhs
      rhs[2] = (y2)*dst[5];
      rhs[1] = ( y1 - ( dst[4] * rhs[2] ) ) * dst[2];
      rhs[0] = ( y0 - ( dst[3] * rhs[2] + dst[1] * rhs[1] ) ) * dst[0];

      return mask;
    }

    // perform a fit using trackhelper's best hits with y correction, improve qop estimate
    template <typename Proxy>
    simd::float_v fastfitterSIMD( std::array<simd::float_v, 4>& improvedParams, Proxy pTrack, const float zMidUT,
                                  const simd::float_v qpxz2p, simd::mask_v& goodFitMask ) {
      // -- parameters that describe the z position of the kink point as a function of ty in a 4th order polynomial
      // (even
      // terms only)
      constexpr auto magFieldParams = std::array{ 2010.0f, -2240.0f, -71330.f };
      const auto     ty             = pTrack.template field<ProtoTrackTag::VeloState>().ty();
      const auto     tx             = pTrack.template field<ProtoTrackTag::VeloState>().tx();
      const auto     z              = pTrack.template field<ProtoTrackTag::VeloState>().z();
      const auto     y              = pTrack.template field<ProtoTrackTag::VeloState>().y();
      const auto     x              = pTrack.template field<ProtoTrackTag::VeloState>().x();
      const auto     ty2            = ty * ty;
      const auto     zKink          = magFieldParams[0] - ty2 * magFieldParams[1] - ty2 * ty2 * magFieldParams[2];
      const auto     xMidField      = x + tx * ( zKink - z );
      const auto     zDiff          = 0.001f * ( zKink - zMidUT );

      // -- This is to avoid division by zero...
      const auto qop     = pTrack.template field<ProtoTrackTag::QOverP>().get();
      const auto pHelper = max( abs( qop * qpxz2p ), 1e-9f );
      const auto invP    = pHelper * 1.f / sqrt( 1.0f + ty2 );

      // these resolution are semi-empirical, could be tuned and might not be correct for low momentum.
      // this is the resolution due to multiple scattering between Velo and UT
      const auto error1 = 0.14f + 10000.0f * invP;
      // this is the resolution due to the finite Velo resolution
      const auto error2 = 0.12f + 3000.0f * invP;

      const auto error  = error1 * error1 + error2 * error2;
      const auto weight = 1.0f / error;

      std::array<simd::float_v, 6> mat = { weight, weight * zDiff, weight * zDiff * zDiff, 0.0f, 0.0f, 0.0f };
      std::array<simd::float_v, 3> rhs = { weight * xMidField, weight * xMidField * zDiff, 0.0f };

      for ( int i = 0; i < totalUTLayers; ++i ) {
        // -- there are 3-hit candidates, but we'll
        // -- just treat them like 4-hit candidates
        // -- with 0 weight for the last hit
        const auto ui = pTrack.template field<ProtoTrackTag::X>( i ).get();
        const auto dz = 0.001f * ( pTrack.template field<ProtoTrackTag::Z>( i ).get() - zMidUT );
        const auto w  = pTrack.template field<ProtoTrackTag::Weight>( i ).get();
        const auto ta = pTrack.template field<ProtoTrackTag::Sin>( i ).get();
        mat[0] += w;
        mat[1] += w * dz;
        mat[2] += w * dz * dz;
        mat[3] += w * ta;
        mat[4] += w * dz * ta;
        mat[5] += w * ta * ta;
        rhs[0] += w * ui;
        rhs[1] += w * ui * dz;
        rhs[2] += w * ui * ta;
      }
      // TODO: this is overkill for dimension 3, vanilla matrix inversion should be fine
      goodFitMask = !CholeskyDecomposition3( mat, rhs );

      const auto xUTFit      = rhs[0];
      const auto xSlopeUTFit = 0.001f * rhs[1];
      const auto offsetY     = rhs[2];

      const auto distX = xMidField - xUTFit - xSlopeUTFit * ( zKink - zMidUT );
      // -- This takes into account that the distance between a point and track is smaller than the distance on the
      // x-axis
      const auto distCorrectionX2 = 1.0f / ( 1 + xSlopeUTFit * xSlopeUTFit );
      auto       chi2             = weight * ( distX * distX * distCorrectionX2 + offsetY * offsetY / ( 1.0f + ty2 ) );
      for ( int i = 0; i < totalUTLayers; ++i ) {
        const auto dz   = pTrack.template field<ProtoTrackTag::Z>( i ).get() - zMidUT;
        const auto w    = pTrack.template field<ProtoTrackTag::Weight>( i ).get();
        const auto x    = pTrack.template field<ProtoTrackTag::X>( i ).get();
        const auto sin  = pTrack.template field<ProtoTrackTag::Sin>( i ).get();
        const auto dist = x - xUTFit - xSlopeUTFit * dz - offsetY * sin;
        chi2 += w * dist * dist * distCorrectionX2;
      }

      // new VELO slope x
      const auto xb = 0.5f * ( ( xUTFit + xSlopeUTFit * ( zKink - zMidUT ) ) + xMidField ); // the 0.5 is empirical
      const auto xSlopeVeloFit = ( xb - x ) / ( zKink - z );

      improvedParams = { xUTFit, xSlopeUTFit, y + ty * ( zMidUT - z ) + offsetY, chi2 };

      // calculate q/p
      const auto sinInX  = xSlopeVeloFit / sqrt( 1.0f + xSlopeVeloFit * xSlopeVeloFit + ty2 );
      const auto sinOutX = xSlopeUTFit / sqrt( 1.0f + xSlopeUTFit * xSlopeUTFit + ty2 );
      return ( sinInX - sinOutX );
    }

    // -- Evaluate the linear discriminant
    // -- Coefficients derived with LD method for p, pT and chi2 with TMVA
    template <int nHits>
    simd::float_v evaluateLinearDiscriminantSIMD( std::array<simd::float_v, 3> inputValues ) {

      constexpr auto coeffs =
          ( nHits == 3 ? std::array{ 0.162880166064f, -0.107081172665f, 0.134153123662f, -0.137764853657f }
                       : std::array{ 0.235010729187f, -0.0938323617311f, 0.110823681145f, -0.170467109599f } );

      assert( coeffs.size() == inputValues.size() + 1 );

      return simd::float_v{ coeffs[0] } + coeffs[1] * log<simd::float_v>( inputValues[0] ) +
             coeffs[2] * log<simd::float_v>( inputValues[1] ) + coeffs[3] * log<simd::float_v>( inputValues[2] );
    }

    // --------------------------------------------------------------------
    // -- Helper function to calculate the planeCode: 0 - 1 - 2 - 3
    // --------------------------------------------------------------------
    int planeCode( unsigned int id ) {
      return ( (unsigned int)id & static_cast<unsigned int>( UTInfo::MasksBits::HalfLayerMask ) ) >>
             static_cast<int>( UTInfo::MasksBits::HalfLayerBits );
    }

    // --------------------------------------------------------------------
    // -- Helper function to find duplicates in hits in the output
    // --------------------------------------------------------------------
    [[maybe_unused]] bool findDuplicates( const Upstream::Tracks& outputTracks ) {
      for ( auto const& track : outputTracks.scalar() ) {
        std::vector<LHCbID> IDs;
        IDs.reserve( track.nUTHits().cast() );
        for ( int h = 0; h < track.nUTHits().cast(); h++ ) { IDs.emplace_back( track.ut_lhcbID( h ).LHCbID() ); }

        std::sort( IDs.begin(), IDs.end() );
        if ( std::adjacent_find( IDs.begin(), IDs.end() ) != IDs.end() ) return false;
      }
      return true;
    }
    // --------------------------------------------------------------------

    // -- TODO: These things are all hardcopied from the PrTableForFunction
    // -- and PrUTMagnetTool
    // -- If the granularity or whatever changes, this will give wrong results
    simd::int_v masterIndexSIMD( const simd::int_v index1, const simd::int_v index2, const simd::int_v index3 ) {
      return ( index3 * 11 + index2 ) * 31 + index1;
    }

    // ===========================================================================================
    // -- 2 helper functions for fit
    // -- Pseudo chi2 fit, templated for 3 or 4 hits
    // ===========================================================================================
    void addHit( span<float, 3> mat, span<float, 2> rhs, const UT::Mut::Hits& hits, int index, float zMidUT ) {
      const auto& hit = hits.scalar()[index];
      const float ui  = hit.x().cast();
      const float ci  = hit.cos().cast();
      const float dz  = 0.001f * ( hit.z().cast() - zMidUT );
      const float wi  = hit.weight().cast();
      mat[0] += wi * ci;
      mat[1] += wi * ci * dz;
      mat[2] += wi * ci * dz * dz;
      rhs[0] += wi * ui;
      rhs[1] += wi * ui * dz;
    }

    template <std::size_t N, typename Proxy>
    [[gnu::always_inline]] inline void simpleFit( const std::array<int, N>& indices, const UT::Mut::Hits& hits,
                                                  Proxy pTrack, float zMidUT, float zKink, float invSigmaVeloSlope ) {
      static_assert( N == 3 || N == 4 );

      // -- Scale the z-component, to not run into numerical problems
      // -- with floats
      const auto wb              = pTrack.template field<ProtoTrackTag::InitialWeight>().get().cast();
      const auto xMidField       = pTrack.template field<ProtoTrackTag::XMidField>().get().cast();
      const auto invKinkVeloDist = pTrack.template field<ProtoTrackTag::InvKinkVeloDist>().get().cast();
      const auto stateX          = pTrack.template field<ProtoTrackTag::VeloState>().x().cast();
      const auto stateTx         = pTrack.template field<ProtoTrackTag::VeloState>().tx().cast();

      const float zDiff = 0.001f * ( zKink - zMidUT );
      auto        mat   = std::array{ wb, wb * zDiff, wb * zDiff * zDiff };
      auto        rhs   = std::array{ wb * xMidField, wb * xMidField * zDiff };

      auto const muthit = hits.scalar();
      for ( auto index : indices ) { addHit( mat, rhs, hits, index, zMidUT ); }

      // TODO: this is overkill for dimension 2, vanilla matrix inversion should be fine
      ROOT::Math::CholeskyDecomp<float, 2> decomp( mat.data() );
      if ( !decomp ) return;

      decomp.Solve( rhs );

      const float xSlopeTTFit = 0.001f * rhs[1];
      const float xTTFit      = rhs[0];

      // new VELO slope x
      const float xb            = xTTFit + xSlopeTTFit * ( zKink - zMidUT );
      const float xSlopeVeloFit = ( xb - stateX ) * invKinkVeloDist;
      const float chi2VeloSlope = ( stateTx - xSlopeVeloFit ) * invSigmaVeloSlope;

      const float chi2TT = std::accumulate( indices.begin(), indices.end(), chi2VeloSlope * chi2VeloSlope,
                                            [&]( float chi2, const int index ) {
                                              const float du =
                                                  ( xTTFit + xSlopeTTFit * ( muthit[index].z().cast() - zMidUT ) ) -
                                                  muthit[index].x().cast();
                                              return chi2 + muthit[index].weight().cast() * ( du * du );
                                            } ) /
                           ( N + 1 - 2 );

      if ( chi2TT < pTrack.template field<ProtoTrackTag::Chi2>().get().cast() ) {

        // calculate q/p
        const float sinInX  = xSlopeVeloFit * vdt::fast_isqrtf( 1.0f + xSlopeVeloFit * xSlopeVeloFit );
        const float sinOutX = xSlopeTTFit * vdt::fast_isqrtf( 1.0f + xSlopeTTFit * xSlopeTTFit );
        const float qp      = sinInX - sinOutX;
        pTrack.template field<ProtoTrackTag::Chi2>().set( chi2TT );
        pTrack.template field<ProtoTrackTag::QOverP>().set( qp );
        pTrack.template field<ProtoTrackTag::XOffset>().set( xTTFit );
        pTrack.template field<ProtoTrackTag::XSlope>().set( xSlopeTTFit );
        for ( std::size_t i = 0; i < N; i++ ) { pTrack.template field<ProtoTrackTag::HitIndex>( i ).set( indices[i] ); }
        if constexpr ( N == 3 ) { pTrack.template field<ProtoTrackTag::HitIndex>( N ).set( -1 ); }
      }
    }
  } // namespace

  class PrVeloUT
      : public Algorithm::Transformer<Upstream::Tracks( const EventContext&, const Velo::Tracks&, const UT::Hits&,
                                                        const VeloUTGeomCache&, const DeMagnet& ),
                                      DetDesc::usesConditions<VeloUTGeomCache, DeMagnet>> {

  public:
    PrVeloUT( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       { KeyValue{ "InputTracksName", "Rec/Track/Velo" }, KeyValue{ "UTHits", UTInfo::HitLocation },
                         KeyValue{ "GeometryInfo", "AlgorithmSpecific-" + name + "-UTGeometryInfo" },
                         KeyValue{ "Magnet", Det::Magnet::det_path } },
                       KeyValue{ "OutputTracksName", "Rec/Track/UT" } ) {}

    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        addConditionDerivation( { DeUTDetLocation::location(), m_PrUTMagnetTool->cacheLocation() },
                                inputLocation<VeloUTGeomCache>(),
                                []( const DeUTDetector& det, const UTMagnetTool::Cache& magtoolcache ) {
                                  return VeloUTGeomCache( det, magtoolcache );
                                } );
      } );
    }

    Upstream::Tracks operator()( const EventContext&, const Velo::Tracks&, const UT::Hits&, const VeloUTGeomCache&,
                                 const DeMagnet& ) const override final;

  private:
    Gaudi::Property<float> m_minMomentum{ this, "MinMomentum", 1500.f * Gaudi::Units::MeV };
    Gaudi::Property<float> m_minPT{ this, "MinPT", 300.f * Gaudi::Units::MeV };
    Gaudi::Property<float> m_minMomentumFinal{ this, "MinMomentumFinal", 2500.f * Gaudi::Units::MeV };
    Gaudi::Property<float> m_minPTFinal{ this, "MinPTFinal", 425.f * Gaudi::Units::MeV };
    Gaudi::Property<float> m_maxPseudoChi2{ this, "MaxPseudoChi2", 1280. };
    Gaudi::Property<float> m_yTol{ this, "YTolerance", 0.5 * Gaudi::Units::mm }; // 0.8
    Gaudi::Property<float> m_yTolSlope{ this, "YTolSlope", 0.08 };               // 0.2
    Gaudi::Property<float> m_hitTol{ this, "HitTol", 0.8 * Gaudi::Units::mm };   // 0.8
    Gaudi::Property<float> m_deltaTx{ this, "DeltaTx", 0.018 };                  // 0.02
    Gaudi::Property<float> m_maxXSlope{ this, "MaxXSlope", 0.350 };
    Gaudi::Property<float> m_maxYSlope{ this, "MaxYSlope", 0.300 };
    Gaudi::Property<float> m_centralHoleSize{ this, "CentralHoleSize", 33. * Gaudi::Units::mm };
    Gaudi::Property<float> m_intraLayerDist{ this, "IntraLayerDist", 15.0 * Gaudi::Units::mm };
    Gaudi::Property<float> m_overlapTol{ this, "OverlapTol", 0.5 * Gaudi::Units::mm };
    Gaudi::Property<float> m_passHoleSize{ this, "PassHoleSize", 40. * Gaudi::Units::mm };
    Gaudi::Property<float> m_LD3Hits{ this, "LD3HitsMin", -0.5 };
    Gaudi::Property<float> m_LD4Hits{ this, "LD4HitsMin", -0.5 };
    Gaudi::Property<int>   m_minLayers{ this, "MinLayers", 3 };

    Gaudi::Property<bool> m_printVariables{ this, "PrintVariables", false };
    Gaudi::Property<bool> m_passTracks{ this, "PassTracks", false };
    Gaudi::Property<bool> m_finalFit{ this, "FinalFit", true };
    Gaudi::Property<bool> m_fiducialCuts{ this, "FiducialCuts", true };
    Gaudi::Property<bool> m_filterMode{ this, "FilterMode", false };
    Gaudi::Property<bool> m_looseSectorSearch{ this, "LooseSectorSearch", false };

    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_seedsCounter{ this, "#seeds" };
    mutable Gaudi::Accumulators::SummingCounter<unsigned int> m_tracksCounter{ this, "#tracks" };

    LHCb::UT::TrackUtils::MiniStates getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks,
                                                float zMidUT ) const;

    template <typename Boundaries, typename BoundariesTypes>
    bool getHitsScalar( VeloState, int, float, int, const UT::Hits&, span<const Boundaries, totalUTLayers>,
                        UT::Mut::Hits& ) const;

    inline void findHits( const UT::Hits& hh, simd::float_v yProto, simd::float_v ty, simd::float_v tx,
                          simd::float_v xOnTrackProto, simd::float_v tolProto, simd::float_v xTolNormFact,
                          UT::Mut::Hits& mutHits, simd::float_v yTol, int firstIndex, int lastIndex ) const;

    template <bool forward, typename Proxy>
    bool formClusters( const UT::Mut::Hits& hitsInLayers, Proxy pTracks, float zMidUT ) const;

    template <typename BdlTable, typename ProtoTracks>
    void prepareOutputTrackSIMD( const ProtoTracks& protoTracks, span<UT::Mut::Hits> hitsInLayers,
                                 Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks,
                                 const BdlTable& bdlTable, const DeMagnet& magnet, float zMidUT ) const;

    /// Multipurpose tool for Bdl and deflection
    ToolHandle<UTMagnetTool> m_PrUTMagnetTool{ this, "PrUTMagnetTool", "PrUTMagnetTool" };

    constexpr static float c_zKink{ 1780.0 };
    constexpr static float c_sigmaVeloSlope{ 0.10 * Gaudi::Units::mrad };
    constexpr static float c_invSigmaVeloSlope{ 10.0 / Gaudi::Units::mrad };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( PrVeloUT, "PrVeloUT" )

  Upstream::Tracks PrVeloUT::operator()( const EventContext& evtCtx, const Velo::Tracks& inputTracks,
                                         const UT::Hits& hh, const VeloUTGeomCache& velogeom,
                                         const DeMagnet& magnet ) const {

    Upstream::Tracks outputTracks{ &inputTracks, Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) };
    outputTracks.reserve( inputTracks.size() );
    m_seedsCounter += inputTracks.size();

    const auto& geometry = velogeom.common;
    const auto& bdlTable = m_PrUTMagnetTool->BdlTable();

    LHCb::UT::TrackUtils::MiniStates filteredStates = getStates( inputTracks, outputTracks, velogeom.zMidUT );

    const auto invMinPt       = 1.0f / m_minPT;
    const auto invMinMomentum = 1.0f / m_minMomentum;

    auto extrapolateToLayer = [this, invMinPt = invMinPt, invMinMomentum = invMinMomentum, &geometry,
                               &velogeom]( auto layerIndex, auto x, auto y, auto z, auto tx, auto ty, auto /*qop*/ ) {
      // -- this 0.002 seems a little odd...
      const auto theta     = max( 0.002f, sqrt( tx * tx + ty * ty ) );
      const auto invMinMom = min( invMinPt * theta, invMinMomentum );

      const auto xTol =
          abs( velogeom.distToMomentum * invMinMom * normFact[layerIndex] ) - abs( tx ) * m_intraLayerDist.value();
      const auto yTol = m_yTol.value() + m_yTolSlope.value() * xTol;

      const auto zGeo{ geometry.layers[layerIndex].z };
      const auto dxDy{ geometry.layers[layerIndex].dxDy };

      const auto yAtZ   = y + ty * ( zGeo - z );
      const auto xLayer = x + tx * ( zGeo - z );
      const auto yLayer = yAtZ + yTol * dxDy;
      return std::make_tuple( xLayer, yLayer, xTol, yTol );
    };

    std::variant<LooseBounds, NominalBounds> compBoundsArray;
    if ( !m_looseSectorSearch ) {
      compBoundsArray =
          LHCb::UTDAQ::findAllSectorsExtrap<TU::BoundariesNominal, TU::BoundariesNominalTag::types,
                                            TU::maxNumRowsBoundariesNominal, TU::maxNumColsBoundariesNominal>(
              filteredStates, geometry, extrapolateToLayer, m_minLayers );
    } else {
      compBoundsArray = LHCb::UTDAQ::findAllSectorsExtrap<TU::BoundariesLoose, TU::BoundariesLooseTag::types,
                                                          TU::maxNumRowsBoundariesLoose, TU::maxNumColsBoundariesLoose>(
          filteredStates, geometry, extrapolateToLayer, m_minLayers );
    }

    // TODO: these hit collections can be fully integrated into the ProtoTracks collection because currently
    // we have exactly one of them per ProtoTrack. This can be done by replacing the static floats_field<4> by
    // its vector variant and then storing all hits there (reducing them later to only 3 or 4). A good reason to
    // do it is performance because the allocations below are relatively expensive and wasteful memory-wise.
    // See issue https://gitlab.cern.ch/lhcb/Rec/-/issues/567
    std::vector<UT::Mut::Hits> hitsInLayers{};
    hitsInLayers.reserve( filteredStates.size() );
    for ( std::size_t i = 0; i < filteredStates.size(); ++i ) {
      hitsInLayers.emplace_back( Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) ).reserve( 32 );
    }

    auto pTracks = ProtoTracks{ Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx ) };
    pTracks.reserve( inputTracks.size() );

    for ( auto fState : filteredStates.scalar() ) {
      const auto x               = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().x().cast();
      const auto y               = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().y().cast();
      const auto z               = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().z().cast();
      const auto tx              = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().tx().cast();
      const auto ty              = fState.get<LHCb::UT::TrackUtils::MiniStateTag::State>().ty().cast();
      const auto hitContainerIdx = pTracks.size();
      hitsInLayers[hitContainerIdx].clear();
      hitsInLayers[hitContainerIdx].layerIndices.fill( -1 );
      if ( !m_looseSectorSearch ? getHitsScalar<TU::BoundariesNominal, TU::BoundariesNominalTag::types>(
                                      { x, y, z, tx, ty }, fState.offset(), velogeom.zMidUT, m_minLayers, hh,
                                      std::get<NominalBounds>( compBoundsArray ), hitsInLayers[hitContainerIdx] )
                                : getHitsScalar<TU::BoundariesLoose, TU::BoundariesLooseTag::types>(
                                      { x, y, z, tx, ty }, fState.offset(), velogeom.zMidUT, m_minLayers, hh,
                                      std::get<LooseBounds>( compBoundsArray ), hitsInLayers[hitContainerIdx] ) ) {
        const auto ancestorIndex = fState.get<LHCb::UT::TrackUtils::MiniStateTag::index>();
        const auto zDiff         = c_zKink - z;
        const auto a             = c_sigmaVeloSlope * zDiff;
        auto       pTrack        = pTracks.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        pTrack.field<ProtoTrackTag::XMidField>().set( x + tx * zDiff );
        pTrack.field<ProtoTrackTag::InitialWeight>().set( 1.f / ( a * a ) );
        pTrack.field<ProtoTrackTag::InvKinkVeloDist>().set( 1.f / zDiff );
        pTrack.field<ProtoTrackTag::VeloState>().setPosition( x, y, z );
        pTrack.field<ProtoTrackTag::VeloState>().setDirection( tx, ty );
        pTrack.field<ProtoTrackTag::Ancestor>().set( ancestorIndex );
        pTrack.field<ProtoTrackTag::Chi2>().set( m_maxPseudoChi2 );
        pTrack.field<ProtoTrackTag::HitIndexContainer>().set( hitContainerIdx );
        for ( auto i{ 0 }; i < totalUTLayers; ++i ) { pTrack.field<ProtoTrackTag::HitIndex>( i ).set( -1 ); }
      }
    }

    for ( auto pTrack : pTracks.scalar() ) {
      const auto hitsOffset = pTrack.offset();
      if ( !formClusters<true>( hitsInLayers[hitsOffset], pTrack, velogeom.zMidUT ) ) {
        formClusters<false>( hitsInLayers[hitsOffset], pTrack, velogeom.zMidUT );
      }
      if ( !m_filterMode && pTrack.isInvalid() ) continue;
      // -- this runs over all 4 layers, even if no hit was found
      // -- but it fills a weight of 0
      // -- Note: These are not "physical" layers, as the hits are ordered such that only
      // -- the last one can be not filled.
      auto hitsInL = hitsInLayers[hitsOffset].scalar();
      auto nValid{ 0 };
      if ( hitsInL.size() != 0 ) {
        for ( auto i = 0; i < totalUTLayers; ++i ) {
          auto       hitI   = pTrack.field<ProtoTrackTag::HitIndex>( i ).get().cast();
          const auto valid  = ( hitI != -1 );
          const auto weight = valid ? hitsInL[hitI].weight().cast() : 0.0f;
          nValid += valid;
          hitI          = std::max( 0, hitI );
          const auto id = LHCbID{ LHCb::Detector::UT::ChannelID( hitsInL[hitI].channelID().cast() ) };
          pTrack.field<ProtoTrackTag::Weight>( i ).set( weight );
          pTrack.field<ProtoTrackTag::X>( i ).set( hitsInL[hitI].x() );
          pTrack.field<ProtoTrackTag::Z>( i ).set( hitsInL[hitI].z() );
          pTrack.field<ProtoTrackTag::Sin>( i ).set( hitsInL[hitI].sin() );
          pTrack.field<ProtoTrackTag::HitIndex>( i ).set( hitsInL[hitI].index() );
          pTrack.field<ProtoTrackTag::ID>( i ).set( id.lhcbID() );
        }
      }
      // this is useful in filterMode
      if ( !nValid ) { pTrack.field<ProtoTrackTag::QOverP>().set( nanMomentum ); }
    }

    prepareOutputTrackSIMD( pTracks, hitsInLayers, outputTracks, inputTracks, bdlTable, magnet, velogeom.zMidUT );

    // -- The algorithm should not store duplicated hits...
    assert( findDuplicates( outputTracks ) && "Hit duplicates found" );

    m_tracksCounter += outputTracks.size();
    return outputTracks;
  }

  //=============================================================================
  // Get the state, do some cuts
  //=============================================================================
  LHCb::UT::TrackUtils::MiniStates PrVeloUT::getStates( const Velo::Tracks& inputTracks, Upstream::Tracks& outputTracks,
                                                        float zMidUT ) const {

    LHCb::UT::TrackUtils::MiniStates filteredStates{ Zipping::generateZipIdentifier(),
                                                     { inputTracks.get_allocator().resource() } };
    filteredStates.reserve( inputTracks.size() );

    const auto centralHoleR2 = simd::float_v{ m_centralHoleSize * m_centralHoleSize };

    for ( auto const& velotrack : inputTracks.simd() ) {
      auto const loopMask = velotrack.loop_mask();
      auto const trackVP  = velotrack.indices();
      auto       pos      = velotrack.StatePos( EndVelo );
      auto       dir      = velotrack.StateDir( EndVelo );

      simd::float_v xMidUT = pos.x() + dir.x() * ( zMidUT - pos.z() );
      simd::float_v yMidUT = pos.y() + dir.y() * ( zMidUT - pos.z() );

      simd::mask_v centralHoleMask = xMidUT * xMidUT + yMidUT * yMidUT < centralHoleR2;
      simd::mask_v slopesMask =
          ( ( abs( dir.x() ) > m_maxXSlope.value() ) || ( abs( dir.y() ) > m_maxYSlope.value() ) );
      simd::mask_v passHoleMask = abs( xMidUT ) < m_passHoleSize.value() && abs( yMidUT ) < m_passHoleSize.value();
      simd::mask_v mask         = centralHoleMask || slopesMask;
      simd::mask_v csMask       = loopMask && !mask && ( !simd::mask_v{ m_passTracks.value() } || !passHoleMask );

      auto fState = filteredStates.compress_back<SIMDWrapper::InstructionSet::Best>( csMask );
      fState.field<LHCb::UT::TrackUtils::MiniStateTag::State>().setPosition( pos.x(), pos.y(), pos.z() );
      fState.field<LHCb::UT::TrackUtils::MiniStateTag::State>().setDirection( dir.x(), dir.y() );
      fState.field<LHCb::UT::TrackUtils::MiniStateTag::State>().setQOverP( 0.f );
      fState.field<LHCb::UT::TrackUtils::MiniStateTag::index>().set( trackVP );

      if ( m_passTracks ) {
        auto outMask = loopMask && passHoleMask; // not sure if correct...

        auto const track = outputTracks.compress_back( outMask );
        track.template field<TracksTag::trackVP>().set( velotrack.indices() );
        track.template field<TracksTag::State>().setQOverP( 0.f );
        track.template field<TracksTag::State>().setPosition( pos );
        track.template field<TracksTag::State>().setDirection( dir );
        track.template field<TracksTag::UTHits>().resize( 0 );
        track.template field<TracksTag::VPHits>().resize( velotrack.nHits() );
        for ( int idx = 0; idx < velotrack.nHits().hmax( outMask ); ++idx ) {
          track.template field<TracksTag::VPHits>()[idx].template field<TracksTag::Index>().set(
              velotrack.vp_index( idx ) );
          track.template field<TracksTag::VPHits>()[idx].template field<TracksTag::LHCbID>().set(
              velotrack.vp_lhcbID( idx ) );
        }
      }
    }
    return filteredStates;
  }

  //=============================================================================
  // Find the hits
  //=============================================================================
  template <typename Boundaries, typename BoundariesTypes>
  [[gnu::flatten]] bool
  PrVeloUT::getHitsScalar( VeloState veloState, int stateOffset, float zMidUT, int minLayers, const UT::Hits& hh,
                           span<const Boundaries, totalUTLayers> compBoundsArray, UT::Mut::Hits& hitsInLayers ) const {

    const simd::float_v tolProto{ m_yTol.value() };

    const auto xState  = veloState.x;
    const auto yState  = veloState.y;
    const auto zState  = veloState.z;
    const auto txState = veloState.tx;
    const auto tyState = veloState.ty;

    // in filter mode tracks close to the hole in the centre of the UT may have no hits
    if ( m_filterMode ) {
      const auto xMidUT = xState + txState * ( zMidUT - zState );
      const auto yMidUT = yState + tyState * ( zMidUT - zState );
      const auto rMidUT = xMidUT * xMidUT + yMidUT * yMidUT;
      minLayers         = rMidUT < m_passHoleSize * m_passHoleSize ? 0 : minLayers;
    }

    std::size_t nSize   = 0;
    int         nLayers = 0;

    // -- the protos could be precomputed
    const simd::float_v yProto{ yState - tyState * zState };
    const simd::float_v xOnTrackProto{ xState - txState * zState };
    const simd::float_v ty{ tyState };
    const simd::float_v tx{ txState };
    // -- the second condition is to ensure at least 3 layers with hits
    for ( int layerIndex = 0; layerIndex < totalUTLayers && layerIndex - nLayers <= totalUTLayers - minLayers;
          ++layerIndex ) {

      const auto          compBoundsArr = compBoundsArray[layerIndex].scalar();
      const auto          xTolS = compBoundsArr[stateOffset].template get<typename BoundariesTypes::xTol>().cast();
      const auto          nPos  = compBoundsArr[stateOffset].template get<typename BoundariesTypes::nPos>().cast();
      const simd::float_v yTol  = m_yTol.value() + m_yTolSlope.value() * xTolS;
      const simd::float_v xTol  = xTolS + abs( tx ) * m_intraLayerDist.value();

      assert( nPos < ( m_looseSectorSearch ? TU::maxNumSectorsBoundariesLoose : TU::maxNumSectorsBoundariesNominal ) &&
              "nPos out of bound" );

      for ( int j = 0; j < nPos; j++ ) {

        const int sectA = compBoundsArr[stateOffset].template get<typename BoundariesTypes::sects>( j ).cast();
        const int sectB =
            ( j == nPos - 1 )
                ? sectA
                : compBoundsArr[stateOffset].template get<typename BoundariesTypes::sects>( j + 1 ).cast();

        assert( sectA != LHCb::UTDAQ::paddingSectorNumber && "sectA points to padding element" );
        assert( sectB != LHCb::UTDAQ::paddingSectorNumber && "sectB points to padding element" );
        assert( ( sectA > -1 ) && ( sectA < static_cast<int>( UTInfo::MaxNumberOfSectors ) ) &&
                "sector number out of bound" );
        assert( ( sectB > -1 ) && ( sectB < static_cast<int>( UTInfo::MaxNumberOfSectors ) ) &&
                "sector number out of bound" );

        // -- Sector is allowed to be a duplicate if it is the last element (as it has no consequence then)
        assert( ( ( sectA != sectB ) || ( j == nPos - 1 ) ) && "duplicated sectors" );

        // -- The idea is to merge adjacent ranges of indices, such that collecting hits is more efficient
        // -- let's try to make it branchless
        const auto temp       = hh.indices( sectA );
        const auto temp2      = hh.indices( sectB );
        const auto firstIndex = temp.first;
        // -- We put the lastIndex to the end of the next container if they join up
        // -- Note that this is _not_ fulfilled if the sector has elements and is a duplicate,
        // -- but this only happens if it is the padding element, in which case we are already at the last
        // -- element of the loop. If it happens in any other case, the asssert fires.
        const auto shift     = ( temp2.first == temp.second );
        const auto lastIndex = shift ? temp2.second : temp.second;
        j += shift;
        findHits( hh, yProto, ty, tx, xOnTrackProto, tolProto, xTol, hitsInLayers, yTol, firstIndex, lastIndex );
      }

      nLayers += ( nSize != hitsInLayers.size() );

      hitsInLayers.layerIndices[layerIndex] = nSize;
      nSize                                 = hitsInLayers.size();
    }

    // -- only use these hits, if we have at least 3 layers
    return nLayers >= minLayers;
  }
  // ==============================================================================
  // -- Method that finds the hits in a given layer within a certain range
  // ==============================================================================
  [[gnu::always_inline]] inline void PrVeloUT::findHits( const UT::Hits& hh, simd::float_v yProto, simd::float_v ty,
                                                         simd::float_v tx, simd::float_v xOnTrackProto,
                                                         simd::float_v tolProto, simd::float_v xTolNormFact,
                                                         UT::Mut::Hits& mutHits, simd::float_v yTol, int firstIndex,
                                                         int lastIndex ) const {

    const auto myHs = hh.simd();
    for ( int i = firstIndex; i < lastIndex; i += simd::size ) {

      const auto mH = myHs[i];

      // -- Calculate distance between straight line extrapolation from Velo and hit position
      const simd::float_v yy = yProto + ty * mH.get<LHCb::Pr::UT::UTHitsTag::zAtYEq0>();
      const simd::float_v xx =
          mH.get<LHCb::Pr::UT::UTHitsTag::xAtYEq0>() + yy * mH.get<LHCb::Pr::UT::UTHitsTag::dxDy>();
      const simd::float_v xOnTrack = xOnTrackProto + tx * mH.get<LHCb::Pr::UT::UTHitsTag::zAtYEq0>();
      const simd::float_v absdx    = abs( xx - xOnTrack );

      if ( none( absdx < xTolNormFact ) ) continue;
      auto loopMask = simd::loop_mask( i, lastIndex );

      // is there anything like minmax?
      const simd::float_v yMin =
          min( mH.get<LHCb::Pr::UT::UTHitsTag::yBegin>(), mH.get<LHCb::Pr::UT::UTHitsTag::yEnd>() );
      const simd::float_v yMax =
          max( mH.get<LHCb::Pr::UT::UTHitsTag::yBegin>(), mH.get<LHCb::Pr::UT::UTHitsTag::yEnd>() );

      const simd::float_v tol  = yTol + absdx * tolProto;
      auto                mask = ( yMin - tol < yy && yy < yMax + tol ) && ( absdx < xTolNormFact ) && loopMask;

      if ( none( mask ) ) continue;
      auto muthit = mutHits.compress_back<SIMDWrapper::InstructionSet::Best>( mask );
      muthit.field<HitTag::xs>().set( xx );
      muthit.field<HitTag::zs>().set( mH.get<LHCb::Pr::UT::UTHitsTag::zAtYEq0>() );
      muthit.field<HitTag::coss>().set( mH.get<LHCb::Pr::UT::UTHitsTag::cos>() );
      muthit.field<HitTag::sins>().set( mH.get<LHCb::Pr::UT::UTHitsTag::cos>() * -1.0f *
                                        mH.get<LHCb::Pr::UT::UTHitsTag::dxDy>() );
      muthit.field<HitTag::weights>().set( mH.get<LHCb::Pr::UT::UTHitsTag::weight>() );
      muthit.field<HitTag::channelIDs>().set( mH.get<LHCb::Pr::UT::UTHitsTag::channelID>() );
      muthit.field<HitTag::indexs>().set( simd::indices( i ) ); // fill the index in the original hit container
    }
  }
  //=========================================================================
  // Form clusters
  //=========================================================================
  template <bool forward, typename Proxy>
  bool PrVeloUT::formClusters( const UT::Mut::Hits& hitsInLayers, Proxy pTrack, float zMidUT ) const {

    const int begin0 = forward ? hitsInLayers.layerIndices[0] : hitsInLayers.layerIndices[3];
    const int end0   = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.size();

    const int begin1 = forward ? hitsInLayers.layerIndices[1] : hitsInLayers.layerIndices[2];
    const int end1   = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[3];

    const int begin2 = forward ? hitsInLayers.layerIndices[2] : hitsInLayers.layerIndices[1];
    const int end2   = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[2];

    const int begin3            = forward ? hitsInLayers.layerIndices[3] : hitsInLayers.layerIndices[0];
    const int end3              = forward ? hitsInLayers.size() : hitsInLayers.layerIndices[1];
    bool      fourLayerSolution = false;

    const auto stateTx = pTrack.template get<ProtoTrackTag::VeloState>().tx().cast();
    const auto hitsInL = hitsInLayers.scalar();

    // -- this is scalar for the moment
    for ( int i0 = begin0; i0 < end0; ++i0 ) {

      const float xhitLayer0 = hitsInL[i0].x().cast();
      const float zhitLayer0 = hitsInL[i0].z().cast();

      // Loop over Second Layer
      for ( int i2 = begin2; i2 < end2; ++i2 ) {

        const float xhitLayer2 = hitsInL[i2].x().cast();
        const float zhitLayer2 = hitsInL[i2].z().cast();

        const float tx = ( xhitLayer2 - xhitLayer0 ) / ( zhitLayer2 - zhitLayer0 );

        if ( std::abs( tx - stateTx ) > m_deltaTx ) continue;

        int   bestHit1Index = -1;
        float hitTol        = m_hitTol;

        for ( int i1 = begin1; i1 < end1; ++i1 ) {

          const float xhitLayer1 = hitsInL[i1].x().cast();
          const float zhitLayer1 = hitsInL[i1].z().cast();

          const float xextrapLayer1 = xhitLayer0 + tx * ( zhitLayer1 - zhitLayer0 );
          if ( std::abs( xhitLayer1 - xextrapLayer1 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer1 - xextrapLayer1 );
            bestHit1Index = i1;
          }
        }

        if ( fourLayerSolution && bestHit1Index == -1 ) continue;

        int bestHit3Index = -1;
        hitTol            = m_hitTol;
        for ( int i3 = begin3; i3 < end3; ++i3 ) {

          const float xhitLayer3 = hitsInL[i3].x().cast();
          const float zhitLayer3 = hitsInL[i3].z().cast();

          const float xextrapLayer3 = xhitLayer2 + tx * ( zhitLayer3 - zhitLayer2 );

          if ( std::abs( xhitLayer3 - xextrapLayer3 ) < hitTol ) {
            hitTol        = std::abs( xhitLayer3 - xextrapLayer3 );
            bestHit3Index = i3;
          }
        }
        // -- All hits found
        if ( bestHit1Index != -1 && bestHit3Index != -1 ) {
          simpleFit( std::array{ i0, bestHit1Index, i2, bestHit3Index }, hitsInLayers, pTrack, zMidUT, c_zKink,
                     c_invSigmaVeloSlope );

          if ( !fourLayerSolution && !pTrack.isInvalid() ) { fourLayerSolution = true; }
          continue;
        }

        // -- Nothing found in layer 3
        if ( !fourLayerSolution && bestHit1Index != -1 ) {
          simpleFit( std::array{ i0, bestHit1Index, i2 }, hitsInLayers, pTrack, zMidUT, c_zKink, c_invSigmaVeloSlope );
          continue;
        }
        // -- Noting found in layer 1
        if ( !fourLayerSolution && bestHit3Index != -1 ) {
          simpleFit( std::array{ i0, bestHit3Index, i2 }, hitsInLayers, pTrack, zMidUT, c_zKink, c_invSigmaVeloSlope );
          continue;
        }
      }
    }
    return fourLayerSolution;
  }
  //=========================================================================
  // Create the Velo-UT tracks
  //=========================================================================
  template <typename BdlTable, typename ProtoTracks>
  void PrVeloUT::prepareOutputTrackSIMD( const ProtoTracks& protoTracks, span<UT::Mut::Hits> hitsInLayers,
                                         Upstream::Tracks& outputTracks, const Velo::Tracks& inputTracks,
                                         const BdlTable& bdlTable, const DeMagnet& magnet, float zMidUT ) const {

    const auto velozipped = inputTracks.simd();
    for ( auto [ipTrack, pTrack] : range::enumerate( protoTracks.simd() ) ) {
      //== Handle states. copy Velo one, add TT.
      const auto ty      = pTrack.template field<ProtoTrackTag::VeloState>().ty();
      const auto tx      = pTrack.template field<ProtoTrackTag::VeloState>().tx();
      const auto z       = pTrack.template field<ProtoTrackTag::VeloState>().z();
      const auto y       = pTrack.template field<ProtoTrackTag::VeloState>().y();
      const auto x       = pTrack.template field<ProtoTrackTag::VeloState>().x();
      const auto zOrigin = select( ty > 0.001f, z - y / ty, z - x / tx );

      // -- These are calculations, copied and simplified from PrTableForFunction
      // -- FIXME: these rely on the internal details of PrTableForFunction!!!
      //           and should at least be put back in there, and used from here
      //           to make sure everything _stays_ consistent...
      auto var    = std::array{ ty, zOrigin, z };
      auto index1 = min( max( simd::int_v{ ( var[0] + 0.3f ) / 0.6f * 30 }, 0 ), 30 );
      auto index2 = min( max( simd::int_v{ ( var[1] + 250 ) / 500 * 10 }, 0 ), 10 );
      auto index3 = min( max( simd::int_v{ var[2] / 800 * 10 }, 0 ), 10 );
      auto bdl    = gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 ) );

      // -- TODO: check if we can go outside this table...
      const auto bdls = std::array{ gather( bdlTable.table().data(), masterIndexSIMD( index1 + 1, index2, index3 ) ),
                                    gather( bdlTable.table().data(), masterIndexSIMD( index1, index2 + 1, index3 ) ),
                                    gather( bdlTable.table().data(), masterIndexSIMD( index1, index2, index3 + 1 ) ) };

      constexpr auto minValsBdl = std::array{ -0.3f, -250.0f, 0.0f };
      constexpr auto maxValsBdl = std::array{ 0.3f, 250.0f, 800.0f };
      constexpr auto deltaBdl   = std::array{ 0.02f, 50.0f, 80.0f };
      const auto     boundaries =
          std::array{ -0.3f + simd::float_v{ index1 } * deltaBdl[0], -250.0f + simd::float_v{ index2 } * deltaBdl[1],
                      0.0f + simd::float_v{ index3 } * deltaBdl[2] };

      // -- This is an interpolation, to get a bit more precision
      simd::float_v addBdlVal{ 0.0f };
      for ( int i = 0; i < 3; ++i ) {
        // -- this should make sure that values outside the range add nothing to the sum
        var[i]               = select( minValsBdl[i] > var[i], boundaries[i], var[i] );
        var[i]               = select( maxValsBdl[i] < var[i], boundaries[i], var[i] );
        const auto dTab_dVar = ( bdls[i] - bdl ) / deltaBdl[i];
        const auto dVar      = ( var[i] - boundaries[i] );
        addBdlVal += dTab_dVar * dVar;
      }
      bdl += addBdlVal;
      // ----

      // -- order is: x, tx, y, chi2
      auto finalParams = std::array{ pTrack.template field<ProtoTrackTag::XOffset>().get(),
                                     pTrack.template field<ProtoTrackTag::XSlope>().get(), y + ty * ( zMidUT - z ),
                                     pTrack.template field<ProtoTrackTag::Chi2>().get() };

      const auto qpxz2p  = -1.0f / bdl * 3.3356f / Gaudi::Units::GeV;
      auto       fitMask = simd::mask_true();
      auto       qp      = m_finalFit
                               ? fastfitterSIMD( finalParams, pTrack, zMidUT, qpxz2p, fitMask )
                               : pTrack.template field<ProtoTrackTag::QOverP>().get() / sqrt( 1.0f + ty * ty ); // is this correct?

      qp             = select( fitMask, qp, pTrack.template field<ProtoTrackTag::QOverP>().get() );
      const auto qop = select( abs( bdl ) < 1.e-8f, simd::float_v{ 1000.0f }, qp * qpxz2p );

      // -- Don't make tracks that have grossly too low momentum
      // -- Beware of the momentum resolution!
      const auto p  = abs( 1.0f / qop );
      const auto pt = p * sqrt( tx * tx + ty * ty );

      const auto xUT  = finalParams[0];
      const auto txUT = finalParams[1];
      const auto yUT  = finalParams[2];
      // -- apply some fiducial cuts
      // -- they are optimised for high pT tracks (> 500 MeV)
      auto fiducialMask = simd::mask_false();

      if ( m_fiducialCuts ) {
        const float magSign = magnet.signedRelativeCurrent();

        fiducialMask = ( magSign * qop < 0.0f && xUT > -48.0f && xUT < 0.0f && abs( yUT ) < 33.0f );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && xUT < 48.0f && xUT > 0.0f && abs( yUT ) < 33.0f );

        fiducialMask = fiducialMask || ( magSign * qop < 0.0f && txUT > 0.09f + 0.0003f * pt );
        fiducialMask = fiducialMask || ( magSign * qop > 0.0f && txUT < -0.09f - 0.0003f * pt );
      }
      // -- evaluate the linear discriminant and reject ghosts
      // -- the values only make sense if the final fit is performed
      auto mvaMask = simd::mask_true();
      if ( const auto fourHitTrack = pTrack.template field<ProtoTrackTag::Weight>( 3 ).get() > 0.0001f; m_finalFit ) {
        const auto fourHitDisc  = evaluateLinearDiscriminantSIMD<4>( { p, pt, finalParams[3] } );
        const auto threeHitDisc = evaluateLinearDiscriminantSIMD<3>( { p, pt, finalParams[3] } );
        const auto fourHitMask  = fourHitDisc > m_LD4Hits.value();
        const auto threeHitMask = threeHitDisc > m_LD3Hits.value();
        // -- only have 3 or 4 hit tracks
        mvaMask = ( fourHitTrack && fourHitMask ) || ( !fourHitTrack && threeHitMask );
      }

      const auto pPTMask        = p > m_minMomentumFinal.value() && pt > m_minPTFinal.value();
      const auto loopMask       = pTrack.loop_mask();
      const auto validTrackMask = pPTMask && !fiducialMask && mvaMask && loopMask && !pTrack.isInvalid();

      const auto finalMask     = m_filterMode ? loopMask : validTrackMask;
      const auto finalQoP      = select( validTrackMask, qop, nanMomentum );
      const auto ancestor      = pTrack.template field<ProtoTrackTag::Ancestor>().get();
      const auto velo_ancestor = velozipped.gather( ancestor, finalMask );
      const auto currentsize   = outputTracks.size();

      auto oTrack = outputTracks.compress_back( finalMask );
      oTrack.template field<TracksTag::trackVP>().set( ancestor );
      oTrack.template field<TracksTag::State>().setQOverP( finalQoP );
      oTrack.template field<TracksTag::State>().setPosition( xUT, yUT, zMidUT );
      oTrack.template field<TracksTag::State>().setDirection( txUT, ty );
      oTrack.template field<TracksTag::VPHits>().resize( velo_ancestor.nHits() );
      for ( int idx = 0; idx < velo_ancestor.nHits().hmax( finalMask ); ++idx ) {
        oTrack.template field<TracksTag::VPHits>()[idx].template field<TracksTag::Index>().set(
            velo_ancestor.vp_index( idx ) );
        oTrack.template field<TracksTag::VPHits>()[idx].template field<TracksTag::LHCbID>().set(
            velo_ancestor.vp_lhcbID( idx ) );
      }

      if ( m_filterMode ) {
        oTrack.template field<TracksTag::UTHits>().resize( 0 );
        continue;
      }

      const auto txArray = SIMDWrapper::to_array( txUT );
      const auto xArray  = SIMDWrapper::to_array( xUT );

      std::array<int, simd::size> nUTHits{};
      // -- This is needed to find the planeCode of the layer with the missing hit
      std::array<int, simd::size> sumLayArray{};

      // -- from here on, go over each track individually to find and add the overlap hits
      // -- this is not particularly elegant...
      // -- As before, these are "pseudo layers", i.e. it is not guaranteed that if i > j, z[i] > z[j]
      const auto t       = ipTrack * simd::size;
      const auto pScalar = protoTracks.scalar();
      for ( int iLayer = 0; iLayer < totalUTLayers; ++iLayer ) {
        int trackIndex2 = 0;
        for ( unsigned int t2 = 0; t2 < simd::size; ++t2 ) {
          if ( !testbit( finalMask, t2 ) ) continue;
          const auto tscalar = t + t2;
          const bool goodHit =
              ( pScalar[tscalar].template field<ProtoTrackTag::Weight>( iLayer ).get().cast() > 0.0001f );
          const auto hitIdx = pScalar[tscalar].template field<ProtoTrackTag::HitIndex>( iLayer ).get();
          const auto id     = pScalar[tscalar].template field<ProtoTrackTag::ID>( iLayer ).get();
          // -- Only add the hit, if it is not in an empty layer (that sounds like a tautology,
          // -- but given that one always has 4 hits, even if only 3 make sense, it is needed)
          // -- Only the last pseudo-layer can be an empty layer
          if ( goodHit ) {
            auto hits = outputTracks.scalar()[currentsize + trackIndex2].field<TracksTag::UTHits>();
            hits.resize( nUTHits[t2] + 1 );
            hits[nUTHits[t2]].template field<TracksTag::Index>().set( hitIdx );
            hits[nUTHits[t2]].template field<TracksTag::LHCbID>().set( LHCb::Event::lhcbid_v<scalar>( id ) );
            nUTHits[t2] += 1;
          }
          // --
          // -----------------------------------------------------------------------------------
          // -- The idea of the following code is: In layers where we have found a hit, we search for
          // -- overlap hits.
          // -- In layers where no hit was found initially, we use the better parametrization of the final
          // -- track fit to pick up hits that were lost in the initial search
          // -----------------------------------------------------------------------------------
          const auto zhit = goodHit ? pScalar[tscalar].template field<ProtoTrackTag::Z>( iLayer ).get().cast() : zMidUT;
          const auto xhit =
              goodHit ? pScalar[tscalar].template field<ProtoTrackTag::X>( iLayer ).get().cast() : xArray[t2];
          const auto hitContIndex = pScalar[tscalar].template field<ProtoTrackTag::HitIndexContainer>().get().cast();

          // -- The total sum of all plane codes is: 0 + 1 + 2 + 3 = 6
          // -- We can therefore get the plane code of the last pseudo-layer
          // -- as: 6 - sumOfAllOtherPlaneCodes
          const auto pC = goodHit ? planeCode( id.cast() ) : 6 - sumLayArray[t2];
          sumLayArray[t2] += pC;

          assert( pC > -1 && pC < 4 && "plane code for overlap out of bound" );

          const float txUTS = txArray[t2];

          const int begin = hitsInLayers[hitContIndex].layerIndices[pC];
          const int end =
              ( pC == 3 ) ? hitsInLayers[hitContIndex].size() : hitsInLayers[hitContIndex].layerIndices[pC + 1];
          const auto hitsInL = hitsInLayers[hitContIndex].scalar();

          for ( int index2 = begin; index2 < end; ++index2 ) {
            const float zohit = hitsInL[index2].z().cast();
            if ( essentiallyEqual( zohit, zhit ) ) continue;

            const float xohit   = hitsInL[index2].x().cast();
            const float xextrap = xhit + txUTS * ( zohit - zhit );

            if ( xohit - xextrap < -m_overlapTol ) continue;
            if ( xohit - xextrap > m_overlapTol ) break;

            if ( nUTHits[t2] >= static_cast<int>( Upstream::Tracks::MaxUTHits ) ) continue;
            const auto utidx  = hitsInL[index2].index();
            const auto oid    = LHCbID{ LHCb::Detector::UT::ChannelID( hitsInL[index2].channelID().cast() ) };
            const auto lhcbid = bit_cast<int>( oid.lhcbID() );

            auto hits = outputTracks.scalar()[currentsize + trackIndex2].field<TracksTag::UTHits>();
            hits.resize( nUTHits[t2] + 1 );
            hits[nUTHits[t2]].template field<TracksTag::Index>().set( utidx );
            hits[nUTHits[t2]].template field<TracksTag::LHCbID>().set( LHCb::Event::lhcbid_v<scalar>( lhcbid ) );

            nUTHits[t2] += 1;
            // only one overlap hit
            break; // this should ensure there are never more than 8 hits on the track
          }
          trackIndex2++;
        }
      }
    } // loop on t
  }
} // namespace LHCb::Pr::VeloUT
