2023-09-04 Rec v35r17
===

This version uses Lbcom [v34r16](../../../../Lbcom/-/tags/v34r16),
LHCb [v54r16](../../../../LHCb/-/tags/v54r16),
Detector [v1r19](../../../../Detector/-/tags/v1r19),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r16](/../../tags/v35r16), with the following changes:

### Enhancements ~enhancement

- ~Monitoring | Mean residual alignment for FT, !3503 (@zexu)
