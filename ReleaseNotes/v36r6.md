2024-04-08 Rec v36r6
===

This version uses
Lbcom [v35r6](../../../../Lbcom/-/tags/v35r6),
LHCb [v55r6](../../../../LHCb/-/tags/v55r6),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r30](../../../../Detector/-/tags/v1r30) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Rec [v36r5](/../../tags/v36r5), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround



### Enhancements ~enhancement

- ~VP ~Monitoring | Update monitoring algorithms for 2024 data taking, !3846 (@mwaterla)
- Check for failure of invChol, !3798 (@gunther)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Functors | Renaming TAU to CTAU in DecayTreeFitter, !3815 (@avilla)


### Documentation ~Documentation


### Other

- ~Muon | Chamber efficiency monitor, !3765 (@rlitvino)
- Add KSVeloLongEfficiencyMonitor, !3796 (@sstahl)
- Add reflection veto to KS lines, !3784 (@ldufour)
