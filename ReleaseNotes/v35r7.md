2023-04-19 Rec v35r7
===

This version uses
Lbcom [v34r7](../../../../Lbcom/-/tags/v34r7),
LHCb [v54r7](../../../../LHCb/-/tags/v54r7),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12),
Detector [v1r11](../../../../Detector/-/tags/v1r11) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r6](/../../tags/v35r6), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Bug fix: fix typo introduced !3378, !3379 (@graven)


### Enhancements ~enhancement

- ~Composites | Added candidate multiplicity counter to ParticleCombiner, !3322 (@roneil)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Cleanup FunctorCore, !3378 (@graven)


### Documentation ~Documentation


### Other

- ~Tracking | More detailed parameterised scattering for no UT scenario, !3310 (@ausachov) [#379]
- ~FT | Updates to FTTrackMonitor (new plots, bugfixes), !3373 (@shollitt)
- ~RICH | Define public type for data storage in RichCKResolutionFitter::FitParams, !3362 (@jonrob)
- ~"MC checking" ~Tuples | Add MCPrimaries and more MC functors, !3279 (@jzhuo)
- Fix extrapolation to beamspot when calculating track ClosestToBeam states, !3383 (@samarian)
- Towards dropping GaudiAlgorithm, round 2, !3381 (@sponce)
- Towards dropping GaudiAlgorithm, !3369 (@sponce)
- Fix muon functor documentation, !3367 (@dcervenk)
- FT fix clusters per beam beam bunch crossing histogram, !3365 (@jheuel)
- Cleaned up forgotten .gitignore file when dir was dropped, !3366 (@sponce)
- Move {FTNZS,IMuonTrack} to use RawBank::View as input instead of RawEvent, !3364 (@graven)
- Remove MCMatchObjP2MCRelator, !3357 (@pherrero)
- Added functional flavour taggers, !3160 (@dtou)
