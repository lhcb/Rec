2023-01-27 Rec v35r4
===

This version uses
Lbcom [v34r4](../../../../Lbcom/-/tags/v34r4),
LHCb [v54r4](../../../../LHCb/-/tags/v54r4),
Detector [v1r8](../../../../Detector/-/tags/v1r8),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on the `master` branch.
Built relative to Rec [v35r3](/../../tags/v35r3), with the following changes:

### Fixes ~"bug fix" ~workaround

- ~Filters | Protect nullptr dereference and fix counters (follow !3188), !3256 (@rmatev)
- ~Functors | Fix(functors) Tree functors and tests in InstantiateFunctors, !3253 (@tfulghes)
- Fix an error in momentum calculation introduced in lhcb/Rec!2898, !3268 (@sesen)


### Enhancements ~enhancement

- Align the helper classes used in DaVinci, !3282 (@amathad)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Speed up python for functors and adapt to LHCb!3829, !3155 (@rmatev) [#261]
- ~RICH | RichFutureRecSys - Retire QMT tests on old data not supported by dd4hep, !3284 (@jonrob)
- Remove use of BOOST_FOREACH, !3262 (@graven)


### Other

- ~Calo ~PID | Hcal pid fix, !3252 (@mveghel)
- ~Calo ~"Event model" | Add track-cluster match info to neutral pp, !3255 (@mveghel)
- ~RICH | Misc rich reco updates, !3273 (@jonrob)
- ~Functors | Add new algorithm and a functor for storing Hlt1 TIS/TOS information, !3115 (@amathad)
- ~Functors ~Tuples | Add array input support for the thor functor grammar, !3283 (@jzhuo)
- ThOr functors: treat SmartRef as-if it was a pointer, !3289 (@graven)
- Make clang happy, !3286 (@graven) [DaVinci#92]
- Prefer namespace LHCb::Event over Sel::Utils for finding ADL-fallback accessors, !3285 (@graven)
- Remove unused code in MicroDst/..., !3280 (@graven)
- Add posibility for 5- and 6-body combinations (to be used in..., !3271 (@ipolyako) [PAPER-2016]
- Track Monitoring histograms, !3269 (@bpagare)
- Update References for: Rec!3273 based on lhcb-master-mr/6743, !3275 (@lhcbsoft)
- Prefer namespace LHCb::Event over Sel::Utils for finding ADL-fallback accessors, !3270 (@graven)
- Prefer LinksByKey::link over LinksByKey::addReference, !3267 (@graven)
- Remove LoKiTrack(s), !3264 (@pkoppenb)
- Follow changes in LHCb!3917, !3263 (@graven)
- Streamline FT monitoring code, !3260 (@graven)
- Fixed creation of Axis in TrackMonitor, !3261 (@sponce)
- Cleanup MicroDST code, !3259 (@graven)
- Add missing link library, !3257 (@rmatev)
- Follow Linked{From,To} changes in LHCb!3912, !3254 (@graven)
