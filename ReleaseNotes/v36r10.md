2024-06-13 Rec v36r10
===

This version uses
Lbcom [v35r9](../../../../Lbcom/-/tags/v35r9),
LHCb [v55r10](../../../../LHCb/-/tags/v55r10),
Detector [v1r33](../../../../Detector/-/tags/v1r33),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Rec [v36r9](/../../tags/v36r9), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Calo | Free temporary memory used by XGBoost classifier, !3952 (@rmatev)
- Fix indexing of muon stations in RecSummaryMaker (follow up !3922), !3965 (@rmatev)
- Bug fix: fix SSProton tagging decision, !3956 (@cprouve)


### Enhancements ~enhancement

- ~Monitoring | New histograms in BeamSpotMonitor, !3924 (@spradlin)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~Tracking | Ghost probability for with UT, !3923 (@mveghel)
- ~Muon | Changed search window size and fixed several histograms, !3961 (@rlitvino)
- ~Calo ~PID | Resolve "Wrong confidence level in neutral particle makers", !3860 (@mveghel) [#552]
- ~PID | ProbNN models for Upstream, Downstream using Long, !3926 (@sberneta)
- ~Monitoring | Enlarge windows for tracks/PV and nPVs, !3945 (@tmombach)
- Simplify CopyProtoParticle2MCRelations, !3964 (@graven)
- Update RecSummary with UT and Muon info, !3922 (@mveghel)
- Implement a seven-body combiner, !3902 (@mramospe)
- Simplify and optimise PrLongLivedTracking, !3811 (@ahennequ)
- Convert TtracksCatboostFilter to use Pr::Seeding::Tracks instead of v1 tracks, prepare T track calorimeter reconstruction, !3659 (@isanders) [Moore#657]
- Update References for: LHCb!4570, Rec!3923, Moore!3480, MooreOnline!451 based on lhcb-2024-patches-mr/586, !3953 (@lhcbsoft)
- Add UTError to PrStoreUTHit, and update default properties, !3853 (@hawu)
- New functors for estimation of helicity angle, !3770 (@vsvintoz) [lhcb-dpa/project#221]
