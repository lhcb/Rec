2017-07-11 RecSys v22r0
===
This version uses Gaudi v28r2, LHCb v43r0 and Lbcom v21r0 (and LCG_88 with ROOT 6.08.06)

This version is released on `master` branch. The previous release on `master` branch  was `Rec v21r3`. This release contains all the changes that were made in `Rec v21r4` and `Rec v21r5` (released on `2017-patches` branch, see corresponding release notes), as well as the additional changes described in this file.

## Change to compiler support
**As from this release, support for gcc49 is dropped**
This means that C++14 is now fully supported

## Thread safety
**[MR !593] Make TMVA NN in PrForward stateless**

## Code optimisations
**[MR !572] Rich MaPMT improvements following [LHCb!618]**
**[MR !598] Rich: adapt to streamlining of TrackSegment of LHCb!691, removing the segment type**


## Bug fixes and cleanups
**[MR !644] Fixed typo in CaloPi0Monitor leading to bad photon counts**

**[MR !637] Fixed logging of lack of vertices in VeloIPResolutionMonitor**
In case an empty set was in the TES, nothing was reported

**[MR !579] PrFitPolinomial: fixed compilation warning that could have lead to memory corruption**

**[MR !576] Fix old RICH pixel sorting**
Fixes an issue with the pixel sorting in the old RICH sequence, that affects the Upgrade productions only.

**[MR !625] Ignore some warnings coming from the Boost library**


## Code modernisations
**[MR !639] Modernize for Gaudi::Property in Muon**
**[MR !631] Modernize for Gaudi::Property in Hlt**
**[MR !627, !633] Modernize for Gaudi::Property in Calo**
**[MR !611] Modernize TrackExtrapolators**
**[MR !618] Replace StaticArray with boost::container::static_vector**
**[MR !651] Remove include of obsolete header files**

**[MR !595] Rich remove residual aerogel code from track segment maker**
**[MR !588] RichRecTrack tools remove unused pd panel pointer**


## Changes to tests
**[MR !585] Fix LumiAlgs tests for Gaudi/master in backwards compatible way**
**[MR !650] Increase VMEM test failure threshold to 1.4GB, to avoid false alarms**
