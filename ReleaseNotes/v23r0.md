

2018-03-07 Rec v23r0
===

This version uses Lbcom v22r0, LHCb v44r0, Gaudi v29r3 and LCG_93 with ROOT 6.12.06
<p>
This version is released on `2018-patches` branch.

Built relative to Rec v22r1, with the following changes:

### New features

- Keras Model, !906, !942 (@kgizdov) [LHCBPS-1769]
  - Expose Keras model headers and make it possible to inherit specific members (related to Phys!251)
  - Allow activation layer inheritance - improves interface

- Catboost integration into MuID, 824 (@nkazeev)


### Enhancements
- Update of TrackKernel to include new vertex fitting code, !893 (@wouter)

- Update dependencis of Tr/TrackInterfaces to include range/v3/utility/any.hpp, !857 (@philten) [LHCBPS-1764]
- Reset the status of the first node on each track iteration on TrackMasterFitter, !801 (@dcampora)

### Bug fixes
- Fix typo in PatLongLivedTracking parameters, !945 (@decianm)

- Remove extraneous PUBLIC_HEADERS uncovered by gaudi/Gaudi!614, !934, !936 (@rmatev)

- CaloTools: Add missing .value() to fix non-string -> string -> non-string conversion, !843 (@olupton)

### Code modernisations and cleanups
- Make PatBBDTSeedClassifier compatible with recent ranges-v3, !866 (@graven)
