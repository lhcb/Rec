2021-07-07 Rec v33r0
===

This version uses
Lbcom [v33r0](../../../../Lbcom/-/tags/v33r0),
LHCb [v53r0](../../../../LHCb/-/tags/v53r0),
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Rec [v32r1](/../../tags/v32r1), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Double max number of hits in a VP track, !2472 (@sponce)
- ~Tracking | StateDetailedBetheBlochEnergyCorrectionTool : actually use Material Cache, !2467 (@graven)
- ~Calo | CellularAutomaton: explicitly unquote string prior to parsing, !2466 (@graven)
- ~Calo | Fix bug in SelectiveBremMatchAlg, !2432 (@mveghel) [#197,[Rec#197]
- ~Core ~Conditions | Change interfaces to make geometry always explicit, !2411 (@sponce)
- ~Conditions | Fix deadlock (follow up !2411), !2483 (@sponce)


### Enhancements ~enhancement

- ~Decoding ~Tracking ~UT | Adapt the UT sector handling in PrAddUTHits, !2450 (@peilian)
- ~Tracking | Follow change from floats_field to states_field, !2474 (@decianm)
- ~Tracking | New parameters for scattering noise approximation, !2463 (@jkubat)
- ~Tracking | PrKalman Filter enable fitting of velo tracks, !2443 (@chasse)
- ~Tracking ~UT | Speed up and clean up PrAddUTHitsTool, !2482 (@gunther)
- ~Calo | Use fast, approximate functions for overlap/correction algo speed increase, !2406 (@mveghel)
- ~"Event model" | Adapt to LHCb!3067, !2445 (@chasse)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | MaterialLocator: remove backwards-compatible thread-unsafe caching, !2468 (@graven)
- ~Tracking ~"Event model" | Fixed compilation with DD4hep, !2459 (@sponce)
- ~Tracking ~"MC checking" | Fix warnings in x86_64_v2-centos7-clang11-opt build, !2479 (@peilian)
- ~FT | Follow changes in LHCb!3102, !2475 (@graven)
- ~Functors | ThOr functor clean-ups, !2456 (@apearce) [#204]
- ~"Event model" | Refactor: cleanup following removal of SOA{Container,Extension}, see LHCb!3078, !2452 (@chasse)
- ~"Event model" | Update reference files from lhcb-head-2 244, !2444 (@axu)
- ~Build | Prepare refs for new platforms, !2465 (@rmatev)
- ~Build | Drop removed properties from reference files, !2446 (@clemenci)
- ~Build | Rewrite CMake configuration in "modern CMake", !2416 (@clemenci)
- Adapt to drop of StatusCode check via StatusCodeSvc, !2453 (@clemenci)
- Adapt to changes in lhcb/LHCb!3069, !2447 (@graven)
