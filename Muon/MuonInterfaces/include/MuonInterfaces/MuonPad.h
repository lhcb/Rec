/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @class MuonPad MuonPad.h
 *
 *  Muon pad class for standalone muon track reconstruction
 *  @author Alessia
 *  @date   2022-09-29
 */

#include "Detector/Muon/TileID.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonLogPad.h"
#include <iostream>
#include <ostream>
#include <vector>

namespace MuonPadContainerLocation {
  inline const std::string Default = "Muon/MuonPads";
}

class MuonPad final {
public:
  using LogPadType = MuonLogPad::Type;
  /// standard constructor
  MuonPad() = default;
  /// constructor for uncrossed pad
  MuonPad( const CommonMuonHit* hit, bool truepad, LogPadType type )
      : m_tile{ hit->tile() }, m_truepad{ truepad }, m_muonhit{ hit }, m_type{ type } {
    // range of TDC: [0;15]  1  bin corresponds to 25/16 ns
    constexpr int lowerTDCvalue = -1;
    constexpr int upperTDCvalue = 16;
    // do some gimnastic to fill strips or pads
    if ( hit->uncrossed() ) {
      m_crossed     = false;
      m_subtiles[0] = ( hit->subtiles() )[0];
      m_time        = hit->time();
      if ( m_time > lowerTDCvalue && m_time < upperTDCvalue ) m_intime = true;
    } else {
      m_crossed     = true;
      m_subtiles[0] = ( hit->subtiles() )[0];
      m_subtiles[1] = ( hit->subtiles() )[1];

      assignTimes( hit->time(), hit->time() - hit->deltaTime() );
      int t2 = hit->time() - hit->deltaTime();
      if ( ( (int)hit->time() > lowerTDCvalue && (int)hit->time() < upperTDCvalue ) &&
           ( t2 > lowerTDCvalue && t2 < upperTDCvalue ) )
        m_intime = true;
    }
  }

  // public member functions
  /// returns the type of logical pad (uncrossed, crossed with 1 or 2 FE channels, or unpaired log. channel)
  LogPadType                   type() const { return m_type; }
  LHCb::Detector::Muon::TileID tile() const { return m_tile; }

  void assignTimes( float t1, float t2 ) {
    m_time  = 0.5 * ( t1 + t2 );
    m_dtime = 0.5 * ( t1 - t2 );
  }

  /// return the pad time (averaged for crossed pads), in TDC bits
  float time() const { return m_time; }
  float timeX() const {
    switch ( type() ) {
    case LogPadType::XONEFE:
    case LogPadType::XTWOFE:
      return m_time + m_dtime;
    default:
      return std::numeric_limits<float>::lowest();
    }
  }
  float timeY() const {
    switch ( type() ) {
    case LogPadType::XONEFE:
    case LogPadType::XTWOFE:
      return m_time - m_dtime;
    default:
      return std::numeric_limits<float>::lowest();
    }
  }

  /// return the half difference of the two hit times for crossed pads, in TDC bits
  float dtime() const { return m_dtime; }
  /// true if this is the crossing of two logical channels
  bool crossed() const { return m_crossed; }
  /// true if it's a real logical pad (if false, it's an unpaired logical channel)
  bool                         truepad() const { return m_truepad; }
  void                         settruepad() { m_truepad = true; }
  bool                         intime() const { return m_intime; }
  LHCb::Detector::Muon::TileID getHitTile( unsigned int i ) {
    if ( i == 0 ) return m_subtiles[0];
    if ( i == 1 ) return m_subtiles[1];
    return LHCb::Detector::Muon::TileID();
  }
  const CommonMuonHit* getHit() const { return m_muonhit; }

private:
  bool                                        m_crossed = false;
  LHCb::Detector::Muon::TileID                m_tile    = {};
  std::array<LHCb::Detector::Muon::TileID, 2> m_subtiles{ {} };
  float                                       m_time    = 0;
  float                                       m_dtime   = 0;
  bool                                        m_truepad = false;
  bool                                        m_intime  = false;
  const CommonMuonHit*                        m_muonhit{ nullptr };
  LogPadType                                  m_type = LogPadType::UNPAIRED;
};

using MuonPads          = std::vector<MuonPad, LHCb::Allocators::EventLocal<MuonPad>>;
using ConstMuonPads     = std::vector<const MuonPad*>;
using MuonPadRange      = const Gaudi::Range_<MuonPads>;
using ConstMuonPadRange = const Gaudi::Range_<ConstMuonPads>;
