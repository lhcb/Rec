/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Math/GenVector/VectorUtil.h"
#include "Math/Point3D.h"
#include "Math/Vector3D.h"
#include "MuonInterfaces/MuonCluster.h"
#include <iostream>
#include <list>

/**
 *  A Neuron is an object that describes the neurons used in the NNET
 *  pattern recognition in the Muon Detector. A Neuron is an oriented segment
 *  conventionally pointing outward the intteraction point. It is built out
 *  of pairs of MuonHits belonging to different Muon stations. It is
 *  characterised by a tail, a head, a set of neurons to which it is connected
 *  to and the corresponding weights.
 *  By convention, station and region of a neuron are those of the tail
 *
 *  @author Giovanni Passaleva
 *  @date   2007-08-21
 */
namespace LHCb::Muon {
  class Neuron final {
  public:
    Neuron( const MuonCluster* h, const MuonCluster* t );

    Neuron( const MuonCluster* h, const MuonCluster* t, int s, int r );

    Neuron( const MuonCluster* h, const MuonCluster* t, int hID, int tID, int s, int r );

    /// public member functions

    /// return neuron head
    const MuonCluster* head() const { return m_head; }
    /// returns the neuron tail
    const MuonCluster* tail() const { return m_tail; }

    /// return the neuorn station.
    int station() const { return m_station; }

    /// return neuron region
    int region() const { return m_region; }

    /// calculate the neuron length in terms of crossed stations
    int stationDifference();

    /// return neuron length
    double len() const;

    /// return neuron length along z
    // double deltaZ() const;
    // double deltaZ( const int st ) const;

    /// return neuron length in xz projection
    double lenXZ() const;

    /// return neuron length in yz projection
    double lenYZ() const;

    /// angle with another neuron in space
    double angle( const Neuron& n );

    /// angle with another neuron in XZ
    double angleXZ( const Neuron& n );

    /// angle with another neuron in YZ
    double angleYZ( const Neuron& n );

    /// check if this is th with n
    bool tailHead( const Neuron& n ) const;

    /// check if this is ht with n
    bool headTail( const Neuron& n ) const;

    /// check if this is tt with n
    bool tailTail( const Neuron& n ) const;

    /// check if this is hh with n
    bool headHead( const Neuron& n ) const;

    /// check if this is connected  to n
    bool connectedTo( const Neuron& n ) const;

    /// return the neuron projection in the xz plan
    ROOT::Math::XYZVector projXZ();

    /// return the neuron projection in the yz plan
    ROOT::Math::XYZVector projYZ();

    /// store the neuron weights
    void setWeight( Neuron* n, double ww );

    /// return the neuron weights
    const std::list<std::pair<Neuron*, double>>& getWeights() const;

    /// retain only the best HT and TH weights
    void cleanupWeights();

    /// kill double length neurons if there is a unit length one
    void killDoubleLength();
    void killDoubleLength( const float angcut );

    /// set a neuron status
    void setStatus( double st );

    /// return neuron status
    double status();

    /// Neuron ID, a progressive number for debugging purposes
    void setNeuronID( int id );

    /// retireve ID
    int neuronID();

    /// assign an ID to head and tail points
    void setHeadTailID( int hID, int tID );

    /// retireve head and tail IDs. First = head, Second = tail
    std::pair<int, int> headTailID();

  private:
    const MuonCluster*                    m_head = nullptr;
    const MuonCluster*                    m_tail = nullptr;
    ROOT::Math::XYZVector                 m_neuron;
    ROOT::Math::XYZVector                 m_neuronXZ;
    ROOT::Math::XYZVector                 m_neuronYZ;
    std::pair<const Neuron*, double>      m_weight;
    std::list<std::pair<Neuron*, double>> m_weight_list;
    double                                m_status{ 1.0 };
    int                                   m_station{ -1 };
    int                                   m_region{ -1 };
    int                                   m_headID{ -1 };
    int                                   m_tailID{ -1 };
    int                                   m_ID{ -1 };
    //
    void removeWeight( Neuron* pn );
  };
} // namespace LHCb::Muon
