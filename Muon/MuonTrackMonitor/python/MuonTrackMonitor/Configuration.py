###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import (
    GaudiSequencer,
    LHCbConfigurableUser,
    MeasurementProvider,
    MuonTrackAligMonitor,
    TrackKalmanFilter,
)
from Gaudi.Configuration import *
from TrackFitter.ConfiguredFitters import ConfiguredEventFitter


class MuonTrackMonitorConf(LHCbConfigurableUser):
    ## Properties -> "name": default value
    __slots__ = {
        "Histograms": "OfflineFull",
        "DataType": "2010",
        "OutputLevel": INFO,
        "Sequencer": GaudiSequencer("MoniMUONSeq"),
    }

    ## List of ConfigurableUser manipulated
    ##    __used_configurables__ = [ OtherConf ]

    ## List of DataTypes (years) for Run 2
    Run2DataTypes = ["2015", "2016", "2017", "2018"]

    def applyConf(self):
        MSRossiAndGreisen = False
        # For Run 2, we want the new multiple scattering
        if self.getProp("DataType") in self.Run2DataTypes:
            MSRossiAndGreisen = True

        muonTrackFit = ConfiguredEventFitter(
            "MuonTrackFitter",
            "Rec/Track/Muon",
            "Rec/Track/MuonFit",
            MSRossiAndGreisen=MSRossiAndGreisen,
        )
        muonTrackFit.Fitter.addTool(TrackKalmanFilter, "NodeFitter")
        muonTrackFit.Fitter.addTool(MeasurementProvider, name="MeasProvider")
        muonTrackFit.Fitter.MeasProvider.IgnoreMuon = False
        muonTrackFit.Fitter.MeasProvider.MuonProvider.clusterize = True  # =======
        # muonTrackFit.Fitter.MeasProvider.MuonProvider.OutputLevel    = DEBUG #=======
        muonTrackFit.Fitter.ErrorX = 1000
        muonTrackFit.Fitter.ErrorY = 1000
        muonTrackFit.Fitter.ErrorTx = 0.7
        muonTrackFit.Fitter.ErrorTy = 0.7
        muonTrackFit.Fitter.NumberFitIterations = 4
        muonTrackFit.Fitter.MaxNumberOutliers = 0  # 2
        muonTrackFit.OutputLevel = self.getProp("OutputLevel")

        muonMoniSeq = self.getProp("Sequencer")

        monalig = MuonTrackAligMonitor(
            "MuonTrackAligMonitor",
            HistoTopDir="Muon/",
            HistoLevel=self.getProp("Histograms"),
        )
        monalig.OutputLevel = self.getProp("OutputLevel")
        # monalig.IsLongTrackState = True
        monalig.LongToMuonMatch = True
        monalig.pCut = 6  # =========
        monalig.chi2nCut = 3
        monalig.chi2matchCut = 10
        monalig.IsCosmics = False

        # For Run 2, we want the new multiple scattering
        if self.getProp("DataType") in self.Run2DataTypes:
            from Configurables import (
                DetailedMaterialLocator,
                StateThickMSCorrectionTool,
                TrackMasterExtrapolator,
            )

            monalig.addTool(TrackMasterExtrapolator, name="Extrapolator")
            monalig.Extrapolator.addTool(
                DetailedMaterialLocator, name="MaterialLocator"
            )
            monalig.Extrapolator.MaterialLocator.addTool(
                StateThickMSCorrectionTool, "StateMSCorrectionTool"
            )
            monalig.Extrapolator.MaterialLocator.StateMSCorrectionTool.UseRossiAndGreisen = True

        muonMoniSeq.Members += [muonTrackFit, monalig]
