/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "DetDesc/DetectorElement.h"
#include "Detector/Muon/MuonConstants.h"
#include "Event/MuonPID.h"
#include "LHCbAlgs/Consumer.h"
#include "TrackInterfaces/ITrackChi2Calculator.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"
#include "LHCbAlgs/Consumer.h"

#include <fmt/format.h>

#include <boost/lexical_cast.hpp>

#include <vector>

namespace {
  using MuonZM1 = double;
}

namespace LHCb {
  class MuonTrackAligMonitor final
      : public Algorithm::Consumer<void( MuonPIDs const&, Tracks const&, DetectorElement const&, DeMuonDetector const&,
                                         MuonZM1 const& ),
                                   Algorithm::Traits::usesConditions<DetectorElement, DeMuonDetector, MuonZM1>> {

  public:
    MuonTrackAligMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    void       operator()( MuonPIDs const&, Tracks const&, DetectorElement const&, DeMuonDetector const&,
                     MuonZM1 const& ) const override;

  private:
    using OptHisto1D      = std::optional<Gaudi::Accumulators::Histogram<1>>;
    using OptProfHisto1D  = std::optional<Gaudi::Accumulators::ProfileHistogram<1>>;
    using OptHisto1DArray = std::optional<
        Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, Detector::Muon::nStations>>;
    using OptHisto2D = std::optional<Gaudi::Accumulators::Histogram<2>>;
    mutable OptHisto1D m_h_p, m_h_chi2;
    mutable OptHisto1D m_p_resxx, m_p_resxy, m_p_resxtx, m_p_resxty, m_p_restxx, m_p_restxy, m_p_restxtx, m_p_restxty,
        m_p_resyx, m_p_resyy, m_p_resytx, m_p_resyty, m_p_restyx, m_p_restyy, m_p_restytx, m_p_restyty, m_resxhsL,
        m_resyhsL, m_resxhsM, m_resyhsM;
    mutable OptHisto2D m_h_xy, m_h_txty;

    mutable OptHisto1DArray m_h_resxL_a, m_h_resyL_a, m_h_resxL_c, m_h_resyL_c, m_h_resxM_a, m_h_resyM_a, m_h_resxM_c,
        m_h_resyM_c;

    Gaudi::Property<bool>        m_LongToMuonMatch{ this, "LongToMuonMatch", true };
    Gaudi::Property<bool>        m_IsCosmics{ this, "IsCosmics", false };
    Gaudi::Property<double>      m_pCut{ this, "pCut", 0. };
    Gaudi::Property<double>      m_chi2nCut{ this, "chi2nCut", 3 };
    Gaudi::Property<double>      m_chi2matchCut{ this, "chi2matchCut", 10 };
    Gaudi::Property<std::string> m_histoLevel{ this, "HistoLevel", "OfflineFull" };

    bool m_notOnline{ true };
    bool m_expertMode{ false };

    ToolHandle<ITrackExtrapolator>   m_extrapolator{ this, "Extrapolator", "TrackMasterExtrapolator" };
    ToolHandle<ITrackChi2Calculator> m_chi2Calculator{ this, "Chi2Calculator", "TrackChi2Calculator" };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( MuonTrackAligMonitor, "MuonTrackAligMonitor" )

} // namespace LHCb

LHCb::MuonTrackAligMonitor::MuonTrackAligMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { { "Input", MuonPIDLocation::Default },
                  { "TrackInput", "/Event/Rec/Track/MuonFit" },
                  { "StandardGeometryTop", LHCb::standard_geometry_top },
                  { "MuonDetectorPath", DeMuonLocation::Default },
                  { "MuonZm1Location", name + "_MuonZM1" } } ) {}

StatusCode LHCb::MuonTrackAligMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    m_notOnline  = ( m_histoLevel.value() != "Online" );
    m_expertMode = ( m_histoLevel.value() == "Expert" );
    addConditionDerivation(
        { inputLocation<DeMuonDetector>() }, inputLocation<MuonZM1>(),
        [&]( DeMuonDetector const& muonDet ) -> MuonZM1 { return muonDet.getStationZ( 0 ) / Gaudi::Units::mm; } );
    const double ulow  = m_IsCosmics.value() ? -1.5 : -0.4;
    const double uhigh = -ulow;
    using Axis1D       = Gaudi::Accumulators::Axis<double>;
    if ( m_notOnline ) {
      m_h_p.emplace( this, "p", "p", Axis1D{ 100, 0, 100 } );
      if ( m_expertMode ) {
        m_h_xy.emplace( this, "x_vs_y", "x_vs_y", Axis1D{ 80, -4000, 4000 }, Axis1D{ 80, -4000, 4000 } );
        m_h_txty.emplace( this, "tx_vs_ty", "tx_vs_ty", Axis1D{ 100, ulow, uhigh }, Axis1D{ 100, ulow, uhigh } );
      }
    }
    if ( m_LongToMuonMatch.value() ) {
      if ( m_notOnline ) { m_h_chi2.emplace( this, "chi2_match", "chi2 match", Axis1D{ 53, -3, 50 } ); }
      // x coord
      m_p_resxx.emplace( this, "prof_resx_x", "profile res. x vs x", Axis1D{ 80, -4000, 4000 } );
      m_p_resxy.emplace( this, "prof_resx_y", "profile res. x vs y", Axis1D{ 80, -4000, 4000 } );
      if ( m_notOnline ) {
        m_p_resxtx.emplace( this, "prof_resx_tx", "profile res. x vs tx", Axis1D{ 60, ulow, uhigh } );
        m_p_resxty.emplace( this, "prof_resx_ty", "profile res. x vs ty", Axis1D{ 60, ulow, uhigh } );
        m_p_restxx.emplace( this, "prof_restx_x", "profile res. tx vs x", Axis1D{ 80, -4000, 4000 } );
        m_p_restxy.emplace( this, "prof_restx_y", "profile res. tx vs y", Axis1D{ 80, -4000, 4000 } );
        m_p_restxtx.emplace( this, "prof_restx_tx", "profile res. tx vs tx", Axis1D{ 60, ulow, uhigh } );
        m_p_restxty.emplace( this, "prof_restx_ty", "profile res. tx vs ty", Axis1D{ 60, ulow, uhigh } );
      }
      // y coord
      m_p_resyx.emplace( this, "prof_resy_x", "profile res. y vs x", Axis1D{ 80, -4000, 4000 } );
      m_p_resyy.emplace( this, "prof_resy_y", "profile res. y vs y", Axis1D{ 80, -4000, 4000 } );
      if ( m_notOnline ) {
        m_p_resytx.emplace( this, "prof_resy_tx", "profile res. y vs tx", Axis1D{ 60, ulow, uhigh } );
        m_p_resyty.emplace( this, "prof_resy_ty", "profile res. y vs ty", Axis1D{ 60, ulow, uhigh } );
        m_p_restyx.emplace( this, "prof_resty_x", "profile res. ty vs x", Axis1D{ 80, -4000, 4000 } );
        m_p_restyy.emplace( this, "prof_resty_y", "profile res. ty vs y", Axis1D{ 80, -4000, 4000 } );
        m_p_restytx.emplace( this, "prof_resty_tx", "profile res. ty vs tx", Axis1D{ 60, ulow, uhigh } );
        m_p_restyty.emplace( this, "prof_resty_ty", "profile res. ty vs ty", Axis1D{ 60, ulow, uhigh } );
      }
    }
    if ( m_notOnline ) {
      // using lambdas for the titles, as name wants i while title wants i+1
      auto getName  = []( auto& s ) { return [&s]( std::size_t i ) { return fmt::format( fmt::runtime( s ), i ); }; };
      auto getTitle = []( auto& s ) {
        return [&s]( std::size_t i ) { return fmt::format( fmt::runtime( s ), i + 1 ); };
      };
      m_h_resxL_a.emplace( this, getName( "residxL_aSide_station_${}" ),
                           getTitle( "X resid from Long tracks A side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resyL_a.emplace( this, getName( "residyL_aSide_station_${}" ),
                           getTitle( "Y resid from Long tracks A side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resxL_c.emplace( this, getName( "residxL_cSide_station_${}" ),
                           getTitle( "X resid from Long tracks C side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resyL_c.emplace( this, getName( "residyL_cSide_station_${}" ),
                           getTitle( "Y resid from Long tracks C side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resxM_a.emplace( this, getName( "residxM_aSide_station_${}" ),
                           getTitle( "X resid from Muon tracks A side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resyM_a.emplace( this, getName( "residyM_aSide_station_${}" ),
                           getTitle( "Y resid from Muon tracks A side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resxM_c.emplace( this, getName( "residxM_cSide_station_${}" ),
                           getTitle( "X resid from Muon tracks C side M{}" ), Axis1D{ 100, -500, 500 } );
      m_h_resyM_c.emplace( this, getName( "residyM_cSide_station_${}" ),
                           getTitle( "Y resid from Muon tracks C side M{}" ), Axis1D{ 100, -500, 500 } );
    }
    m_resxhsL.emplace( this, "residx_per_halfstationL", "average X res. per half station wtr LONG",
                       Axis1D{ 10, -0.5, 9.5 } );
    m_resyhsL.emplace( this, "residy_per_halfstationL", "average Y res. per half station wtr LONG",
                       Axis1D{ 10, -0.5, 9.5 } );
    m_resxhsM.emplace( this, "residx_per_halfstationM", "average X res. per half station wtr MUON",
                       Axis1D{ 10, -0.5, 9.5 } );
    m_resyhsM.emplace( this, "residy_per_halfstationM", "average Y res. per half station wtr MUON",
                       Axis1D{ 10, -0.5, 9.5 } );
    return StatusCode::SUCCESS;
  } );
}

void LHCb::MuonTrackAligMonitor::operator()( LHCb::MuonPIDs const& pMuids, LHCb::Tracks const& pMuTracks,
                                             DetectorElement const& lhcb, DeMuonDetector const& muonDet,
                                             MuonZM1 const& zM1 ) const {
  auto& geometry = *lhcb.geometry();

  for ( const auto& mupid : pMuids ) {
    // look for the muon track in the pMuTracks container, based on the one pointed
    // by the pMuids. The pointed track is not usable as it was not fitted, the fitted
    // version is in pMuTracks.
    auto muTrackNotFitted = mupid->muonTrack();
    if ( !muTrackNotFitted ) {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "no muon track associated to MuonPID object (ismuon=" << mupid->IsMuon() << ")... skipping"
                << endmsg;
      continue;
    }
    const auto muTrack   = pMuTracks.object( muTrackNotFitted->key() );
    const auto longTrack = mupid->idTrack();
    if ( !longTrack ) {
      warning() << "no track associated to MuonPID object.. skipping" << endmsg;
      continue;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "LongTrack p " << longTrack->p() / Gaudi::Units::GeV << endmsg;
      debug() << "LongTrack chi2 " << longTrack->chi2PerDoF() << " dof " << longTrack->nDoF() << endmsg;
      debug() << "MuonTrack chi2 " << muTrack->chi2PerDoF() << " dof " << muTrack->nDoF() << endmsg;
    }

    // Skip if poor quality
    if ( !( longTrack->p() / Gaudi::Units::GeV > m_pCut && longTrack->chi2PerDoF() < m_chi2nCut &&
            muTrack->chi2PerDoF() < m_chi2nCut && muTrack->nDoF() > 3 ) ) {
      continue;
    }
    State muState   = closestState( *muTrack, zM1 );
    State longState = closestState( *longTrack, muState.z() );

    StatusCode sc = m_extrapolator->propagate( longState, zM1, geometry, Tr::PID::Muon() );
    if ( sc.isFailure() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Extrapolating longState to z = " << zM1 << " failed " << endmsg;
      warning() << "Extrapolating a muon longState to z failed " << endmsg;
      continue;
    }

    sc = m_extrapolator->propagate( muState, zM1, geometry, Tr::PID::Muon() );
    if ( sc.isFailure() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Extrapolating muState to z = " << zM1 << " failed " << endmsg;
      warning() << "Extrapolating a muon muState to z failed " << endmsg;
      continue;
    }

    if ( msgLevel( MSG::DEBUG ) )
      debug() << " Extrapolation to z " << zM1 << " long = (" << longState.x() << "," << longState.y() << ")"
              << " muon = (" << muState.x() << "," << muState.y() << ")" << endmsg;

    double chi2;
    if ( m_notOnline ) {
      ++( *m_h_p )[longState.p() / Gaudi::Units::GeV];
      if ( m_expertMode ) {
        ++( *m_h_xy )[{ longState.x(), longState.y() }];
        ++( *m_h_txty )[{ longState.tx(), longState.ty() }];
      }
    }
    sc = m_chi2Calculator->calculateChi2( longState.stateVector(), longState.covariance(), muState.stateVector(),
                                          muState.covariance(), chi2 );
    if ( !sc.isSuccess() ) {
      warning() << "Could not invert matrices" << endmsg;
      // info() <<  "Could not invert matrices" << endmsg;
      continue;
    }

    if ( chi2 > m_chi2matchCut ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " matching chisquare not satisfactory " << chi2 << endmsg;
      continue;
    }

    double resx  = longState.x() - muState.x();
    double resy  = longState.y() - muState.y();
    double restx = longState.tx() - muState.tx();
    double resty = longState.ty() - muState.ty();
    double x     = muState.x();
    double y     = muState.y();
    double tx    = muState.tx();
    double ty    = muState.ty();

    if ( m_LongToMuonMatch.value() ) {
      ( *m_p_resxx )[x] += resx;
      ( *m_p_resxy )[y] += resx;

      if ( m_notOnline ) {
        ( *m_p_resxtx )[tx] += resx;
        ( *m_p_resxty )[ty] += resx;
        ( *m_p_restxx )[x] += restx;
        ( *m_p_restxy )[y] += restx;
        ( *m_p_restxtx )[tx] += restx;
        ( *m_p_restxty )[ty] += restx;
      }

      ( *m_p_resyx )[x] += resy;
      ( *m_p_resyy )[y] += resy;

      if ( m_notOnline ) {
        ( *m_p_resytx )[tx] += resy;
        ( *m_p_resyty )[ty] += resy;
        ( *m_p_restyx )[x] += resty;
        ( *m_p_restyy )[y] += resty;
        ( *m_p_restytx )[tx] += resty;
        ( *m_p_restyty )[ty] += resty;
        ++( *m_h_chi2 )[chi2];
      }
    }

    for ( const auto& tile0 : muTrack->lhcbIDs() ) {
      // Skip if it's not muon
      if ( tile0.isMuon() ) continue;

      const auto tile = tile0.muonID();
      auto       pos  = muonDet.position( tile );
      if ( !pos ) {
        warning() << "Could not get tile coordinates, skipping tile " << tile << endmsg;
        continue;
      }

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "*** tile position ***" << tile << endmsg;
        debug() << " x = " << pos->x() << " y = " << pos->y() << " z = " << pos->z() << endmsg;
        debug() << " region " << tile.region() << " station " << tile.station() << endmsg;
        debug() << "*********************" << tile << endmsg;
      }

      for ( int i = 0; i < 2; i++ ) {
        const auto fitState = i == 0 ? longState : muState;
        double     deltaZ   = pos->z() - fitState.z();
        double     rx       = pos->x() - ( fitState.x() + fitState.tx() * deltaZ );
        double     ry       = pos->y() - ( fitState.y() + fitState.ty() * deltaZ );
        if ( m_notOnline ) {
          if ( i == 0 ) {
            if ( pos->x() > 0 ) {
              ++( *m_h_resxL_a )[tile.station()][rx];
              ++( *m_h_resyL_a )[tile.station()][ry];
            } else {
              ++( *m_h_resxL_c )[tile.station()][rx];
              ++( *m_h_resyL_c )[tile.station()][ry];
            }
          } else {
            if ( pos->x() > 0 ) {
              ++( *m_h_resxM_a )[tile.station()][rx];
              ++( *m_h_resyM_a )[tile.station()][ry];
            } else {
              ++( *m_h_resxM_c )[tile.station()][rx];
              ++( *m_h_resyM_c )[tile.station()][ry];
            }
          }
        }
        auto index = ( x > 0 ? 0 : 5 ) + tile.station();
        if ( i == 0 ) {
          ( *m_resxhsL )[index] += rx;
          ( *m_resyhsL )[index] += ry;
        } else {
          ( *m_resxhsM )[index] += rx;
          ( *m_resyhsM )[index] += ry;
        }
      }
    }
  }
}
