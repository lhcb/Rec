/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <array>
#include <limits>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"

#include "MuonID/IMVATool.h" // Interface

#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "evaluator.h"
#include "model_calcer_wrapper.h"
#include "wrapped_calcer.h"

/** Implementation of MVATool
 *
 * 2017-05-29: Ricardo Vazquez Gomez
 */
namespace {
  constexpr auto INVSQRT3 = 0.5773502691896258;
  constexpr auto MSFACTOR = 5.552176750308537;

  template <class T>
  T squared( const T& a ) {
    return a * a;
  }
  // Features the MVA expects
  constexpr size_t n_features_per_station = 5;
  constexpr size_t used_stations          = 4;
  // 3 is for M2 extrapolation X, Y, and chi^2
  constexpr size_t nFeatures                = n_features_per_station * used_stations + 3;
  constexpr size_t dt_index                 = 0 * used_stations;
  constexpr size_t time_index               = 1 * used_stations;
  constexpr size_t crossed_index            = 2 * used_stations;
  constexpr size_t resX_index               = 3 * used_stations;
  constexpr size_t resY_index               = 4 * used_stations;
  constexpr size_t M2_extrapolation_X_index = n_features_per_station * used_stations;
  constexpr size_t M2_extrapolation_Y_index = n_features_per_station * used_stations + 1;
  constexpr size_t chi2_index               = n_features_per_station * used_stations + 2;
  // Values to use if there is no matched hit in a station
  constexpr float                                     defaultRes       = -10000.;
  constexpr std::array<float, n_features_per_station> default_features = { -10000., -10000., 0., defaultRes,
                                                                           defaultRes };
} // namespace

/** @class MVATool MVATool.h
 * A tool that provides an MVA for muon identification
 *
 * @author Ricardo Vazquez Gomez
 * @date 2017-05-29
 */

class MVATool final : public extends<GaudiTool, IMVATool> {
public:
  using extends::extends;

  StatusCode   initialize() override;
  MVAResponses calcBDT( const float trackP, const MuonTrackExtrapolation& extrapolation,
                        const MuonHitContainer& hitContainer, const float chi2corr,
                        std::array<float, 4> stZ ) const override;
  // TODO(kazeevn) this probably can be replaced with
  // Gaudi::Utils::getProperty
  // 1. Can someone please show me how to get the actual bool value?
  // 2. Will this be fast enough to call on every track?
  bool         getRunCatBoostBatch() const noexcept override { return m_runCatBoostBatch; }
  size_t       getNFeatures() const noexcept override { return nFeatures; }
  unsigned int nStations;
  // A way to have a nice continuous C array with data
  void                compute_features( const float trackP, const MuonTrackExtrapolation& extrapolation,
                                        const MuonHitContainer& hitContainer, const float chi2corr, LHCb::span<float> result,
                                        std::array<float, 4> stZ ) const override;
  std::vector<double> calcBDTBatch( LHCb::span<const float> features ) const override;

private:
  // There are two libaries that allow for running CatBoost models
  // standalone_evaluator and model_interface
  // As of LCG v94 the standalone_evaluator is slightly faster when running on
  // single predictions, but model_interface supports batches
  // Logic:
  // 1. If both RunCatBoostBatch and UseCatboostStandaloneEvaluator are true, fail
  // 2. If RunCatBoostBatch is true, run in batch mode
  // 3. If RunCatBoostBatch is false and UseCatboostStandaloneEvaluator is true,
  //    run standalone_evaluator
  // 4. If RunCatBoostBatch is false and UseCatboostStandaloneEvaluator is false,
  //    run model_interface
  Gaudi::Property<bool>        m_runCatBoostBatch{ this, "RunCatBoostBatch", false };
  Gaudi::Property<bool>        m_useStandAloneCatboost{ this, "UseCatboostStandaloneEvaluator", true };
  Gaudi::Property<std::string> m_CatBoostModelPath{
      this, "CatBoostModelPath", System::getEnv( "TMVAWEIGHTSROOT" ) + "/data/MuID/MuID-Run2-MC-570-v1.cb" };
  ModelCalcerWrapper m_CatBoostModel;
  // Catboost without the C++ wrapper to call predict on float**
  using CalcerHolderType = std::unique_ptr<ModelCalcerHandle, std::function<void( ModelCalcerHandle* )>>;
  CalcerHolderType                                       m_rawCatBoostModel;
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModel;
  std::vector<float>                                     m_stationZ;
};

/*
 * Initialises the tool.
 */
StatusCode MVATool::initialize() {
  auto sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return sc; }

#ifdef __x86_64__
  if ( m_runCatBoostBatch ) {
    if ( m_useStandAloneCatboost ) {
      error() << "UseCatboostStandaloneEvaluator can't be specfied "
              << "together with RunCatBoostBatch. Which of them do you want to run?" << endmsg;
      return StatusCode::FAILURE;
    }
    m_rawCatBoostModel = CalcerHolderType( ModelCalcerCreate(), ModelCalcerDelete );
    if ( !LoadFullModelFromFile( m_rawCatBoostModel.get(), m_CatBoostModelPath.value().c_str() ) ) {
      error() << "Failed to load CatBoost model. Likely cases are "
              << "a malformed or missing file. Model file: " << m_CatBoostModelPath << std::endl;
      error() << "Message from Catboost:" << std::endl;
      error() << GetErrorString();
      error() << endmsg;
      return StatusCode::FAILURE;
    }
  } else {
    try {
      if ( m_useStandAloneCatboost ) {
        m_StandAloneCatBoostModel = std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelPath );
      } else {
        m_CatBoostModel = ModelCalcerWrapper( m_CatBoostModelPath );
      }
    } catch ( const std::runtime_error& e ) {
      error() << "Failed to load CatBoost model. Likely cases are "
              << "a malformed or missing file. Model file: " << m_CatBoostModelPath << std::endl;
      error() << "Message from Catboost:" << std::endl;
      error() << e.what();
      error() << endmsg;
      return StatusCode::FAILURE;
    }
  }
#else
  if ( m_useStandAloneCatboost ) {
    if ( m_runCatBoostBatch ) {
      error() << "UseCatboostStandaloneEvaluator can't be specfied "
              << "together with RunCatBoostBatch. Which of them do you want to run?" << endmsg;
      return StatusCode::FAILURE;
    }
    m_StandAloneCatBoostModel = std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelPath );
  } else {
    error() << "This architecture only supports UseCatboostStandaloneEvaluator" << endmsg;
    return StatusCode::FAILURE;
  }
#endif
  return sc;
}

void MVATool::compute_features( const float trackP, const MuonTrackExtrapolation& extrapolation,
                                const MuonHitContainer& hitContainer, const float chi2corr, LHCb::span<float> result,
                                std::array<float, 4> m_stationZ ) const {
  // common factor for the multiple scattering error. Taken from formula.
  // commonFactor = 13.6/muTrack.p()/sqrt(6.);
  const float commonFactor = MSFACTOR / trackP;
  for ( size_t stationFromFirstUsed = 0; stationFromFirstUsed != used_stations; ++stationFromFirstUsed ) {
    const size_t st = stationFromFirstUsed; //+ firstUsedStation;
    if ( hitContainer.station( st ).hits().empty() ) {
      for ( size_t i = 0; i < n_features_per_station; ++i ) {
        result[i * used_stations + stationFromFirstUsed] = default_features[i];
      }
    } else {
      const auto matchedHit = std::min_element(
          hitContainer.station( st ).hits().begin(), hitContainer.station( st ).hits().end(),
          [st, extrapolation]( const CommonMuonHit& hit1, const CommonMuonHit& hit2 ) {
            auto dr = [&]( auto hit ) {
              return squared( hit.x() - extrapolation[st].first ) + squared( hit.y() - extrapolation[st].second );
            };
            return dr( hit1 ) < dr( hit2 );
          } );
      result[time_index + stationFromFirstUsed] = matchedHit->time();
      result[dt_index + stationFromFirstUsed]   = matchedHit->deltaTime();
      if ( matchedHit->uncrossed() ) {
        result[crossed_index + stationFromFirstUsed] = matchedHit->uncrossed();
      } else {
        result[crossed_index + stationFromFirstUsed] = 2.;
      }

      // compute the Error due to the RMS taking into account the whole traversed material. Common error for X and Y.
      const float travDist = sqrt( squared( m_stationZ[st] - m_stationZ[0] ) +
                                   squared( extrapolation[st].first - extrapolation[0].first ) +
                                   squared( extrapolation[st].second - extrapolation[0].second ) );
      // errMS = commonFactor*travDist*sqrt(travDist/17.58);
      const float errMS = commonFactor * travDist * sqrt( travDist ) * 0.23850119787527452;
      if ( !LHCb::essentiallyEqual( std::abs( extrapolation[st].first - matchedHit->x() ), 2000. ) ) {
        result[resX_index + stationFromFirstUsed] =
            ( extrapolation[st].first - matchedHit->x() ) /
            sqrt( squared( matchedHit->dx() * INVSQRT3 ) + errMS * errMS ); // multiply instead of divide
      } else {
        result[resX_index + stationFromFirstUsed] = defaultRes;
      }
      if ( !LHCb::essentiallyEqual( std::abs( extrapolation[st].second - matchedHit->y() ), 2000. ) ) {
        result[resY_index + stationFromFirstUsed] =
            ( extrapolation[st].second - matchedHit->y() ) /
            sqrt( squared( matchedHit->dy() * INVSQRT3 ) + errMS * errMS ); // mutiply instead of divide
      } else {
        result[resY_index + stationFromFirstUsed] = defaultRes;
      }
    }
  }

  // extrapolation always starts from M1
  result[M2_extrapolation_X_index] = extrapolation[1].first;
  result[M2_extrapolation_Y_index] = extrapolation[1].second;
  result[chi2_index]               = chi2corr;
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Input vars to the BDT: ";
    for ( auto feature = result.begin(); feature != result.end() - 1; ++feature ) { debug() << *feature << ", "; }
    debug() << result[nFeatures - 1] << endmsg;
  }
}

MVATool::MVAResponses MVATool::calcBDT( const float trackP, const MuonTrackExtrapolation& extrapolation,
                                        const MuonHitContainer& hitContainer, const float chi2corr,
                                        std::array<float, 4> stZ ) const {
  // Computes CatBoost values, CatBoost evaluator is choosen
  // according to RunCatBoostBatch and UseCatboostStandaloneEvaluator flags
  std::vector<float> inputVector( nFeatures );
  compute_features( trackP, extrapolation, hitContainer, chi2corr, inputVector, stZ );

  MVAResponses predictions;
#ifdef __x86_64__
  predictions.CatBoost =
      ( m_runCatBoostBatch ? calcBDTBatch( inputVector ).front()
        : m_useStandAloneCatboost
            ? m_StandAloneCatBoostModel->Apply( inputVector, NCatboostStandalone::EPredictionType::RawValue )
            : m_CatBoostModel.CalcFlat( inputVector ) );
#else
  predictions.CatBoost =
      m_StandAloneCatBoostModel->Apply( inputVector, NCatboostStandalone::EPredictionType::RawValue );
#endif

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "CatBoost = " << predictions.CatBoost << endmsg; }
  return predictions;
}

std::vector<double> MVATool::calcBDTBatch( LHCb::span<const float> features ) const {
  // features is a nice continuous vector where features for each track
  // lie consecutively
  const size_t              nTracks = features.size() / nFeatures;
  std::vector<const float*> featuresPtrs( nTracks );
  for ( size_t track_index = 0; track_index < nTracks; ++track_index ) {
    featuresPtrs[track_index] = features.data() + track_index * nFeatures;
  }
  std::vector<double> result( nTracks );
#ifdef __x86_64__
  CalcModelPredictionFlat( m_rawCatBoostModel.get(), nTracks, featuresPtrs.data(), nFeatures, result.data(),
                           result.size() );
#endif
  // Matches the printout of the calcBDT
  if ( msgLevel( MSG::DEBUG ) ) {
    for ( double score : result ) { debug() << "CatBoost = " << score << endmsg; }
  }
  return result;
}

DECLARE_COMPONENT( MVATool )
