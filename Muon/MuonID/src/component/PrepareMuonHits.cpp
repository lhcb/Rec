/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonCoord.h"
#include "Event/PrHits.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/CommonMuonStation.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Transformer.h"

#include "boost/container/static_vector.hpp"
#include "boost/numeric/conversion/cast.hpp"

#include <algorithm>
#include <vector>

/**
 *  Transformer supposed to take muon coord as input and
 *  muon hits as output
 *  Used to be CommonMuonHitManager
 *
 *  @author Roel Aaij
 *  @author Kevin Dungs
 *  @date   2015-01-03
 */
class PrepareMuonHits final
    : public LHCb::Algorithm::Transformer<MuonHitContainer( const LHCb::MuonCoords& coords, DeMuonDetector const& ),
                                          LHCb::Algorithm::Traits::usesConditions<DeMuonDetector>> {
public:
  PrepareMuonHits( const std::string& name, ISvcLocator* pSvcLocator );

  MuonHitContainer operator()( const LHCb::MuonCoords& coords, DeMuonDetector const& ) const override;

  StatusCode initialize() override;

private:
  mutable Gaudi::Accumulators::BinomialCounter<> m_badTile{ this, "Impossible MuonTileID" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PrepareMuonHits )

PrepareMuonHits::PrepareMuonHits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   { KeyValue{ "CoordLocation", LHCb::MuonCoordLocation::MuonCoords },
                     KeyValue{ "DeMuon", DeMuonLocation::Default } },
                   KeyValue{ "Output", MuonHitContainerLocation::Default } ) {}

StatusCode PrepareMuonHits::initialize() { return Transformer::initialize(); }

MuonHitContainer PrepareMuonHits::operator()( const LHCb::MuonCoords& muonCoords,
                                              DeMuonDetector const&   muonDet ) const {
  size_t                                  nStations = boost::numeric_cast<size_t>( muonDet.stations() );
  boost::container::static_vector<int, 5> nCoords( nStations, 0 );
  for ( const auto* coord : muonCoords ) { ++nCoords[coord->key().station()]; }
  boost::container::static_vector<CommonMuonHits, 5> hits( nStations );
  for ( unsigned int station = 0; station < nStations; ++station ) { hits[station].reserve( nCoords[station] ); }
  auto badTile = m_badTile.buffer();
  for ( const auto* coord : muonCoords ) {
    unsigned int station = coord->key().station();
    auto         pos     = muonDet.position( coord->key() );
    badTile += ( !pos.has_value() );
    if ( !pos ) continue;
    hits[station].emplace_back( coord->key(), pos->x(), pos->dX(), pos->y(), pos->dY(), pos->z(), pos->dZ(),
                                coord->uncrossed(), coord->digitTDC1(), coord->digitTDC1() - coord->digitTDC2() );
  }
  std::array<CommonMuonStation, 5> stations;
  for ( unsigned int station = 0; station < nStations; ++station ) {
    // create a MuonHitContainer to be returned and stored in the TES
    // For the upgrade nStations == 4, and M1 has been removed, so use
    // 1-5, for Run II use 0-5
    stations[station] = CommonMuonStation{ muonDet, station, std::move( hits[station] ) };
  }
  return MuonHitContainer{ std::move( stations ) };
}
