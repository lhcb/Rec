/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/Namespace.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonID/IMuonMatchTool.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiAlg/GaudiTool.h"

#include <array>
#include <stdexcept>

/**
 *  the tool chooses the combination of muon detector hits (1 per station) that best fits a track extrapolation
 *  based on a global chi^2 that takes properly into account correlations in the track extrapolation errors
 *
 *  input hits are given through a vector of TrackMuMatch, containing a CommonMuonHit and related track extrapolation
 * position optionally, a second list of "low-quality" hits (uncrossed or from large clusters) can be given if no
 * matches are found in the first list
 *
 *  use run() to perform the calculation, and then:
 *    getChisquare() to get the best chi2
 *    getListofCommonMuonHits() or getListofMuonTiles() to get the associated muon hits
 *
 *  @author Violetta Cogoni, Marianna Fontana, Rolf Odeman, ported here by Giacomo Graziani
 *  @date   2015-11-11
 */
class MuonChi2MatchTool final : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IMuonMatchTool> {
public:
  MuonChi2MatchTool( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override;

  StatusCode run( const LHCb::Track*, std::vector<TrackMuMatch>* bestMatches, float& chi2, unsigned& ndof,
                  std::vector<TrackMuMatch>* spareMatches = NULL ) const override;

private:
  static constexpr const int                  nSt = 4; // this algorithm uses stations M2 to M5
  typedef std::array<float, 15>               StationStationCovarianceType;
  typedef StationStationCovarianceType        ScatterCache;
  typedef std::array<std::array<float, 4>, 5> SigmaPadCache;
  typedef std::array<float, 5>                StationCache;

  ConditionAccessor<DeMuonDetector> m_mudet{ this, "DeMuonLocation", LHCb::Detector::Muon::Location::Default };
  int                               m_myFirstStation = -1; // recognisably a very invalid value...
  unsigned                          m_nStations      = -1; // recognisably a very invalid value...

  SigmaPadCache m_sigmapadX, m_sigmapadY;
  ScatterCache  m_scattercache;
  unsigned      m_maxcomb;

  // options
  Gaudi::Property<bool> m_OnlyCrossedHits{ this, "OnlyCrossedHits", false, "use only crossed hits" };

  Gaudi::Property<int> m_maxnhitinFOI{ this, "MaxHitsPerStation", 5,
                                       "consider only the closest MaxHitsPerStation muon hits per station" };

  Gaudi::Property<float> m_InputMaxSigmaMatch{
      this, "InputMaxSigmaMatch", 8.,
      "at input, only considers muon hits within this number of sigmas in x and y from track extrapolation" };

  class StationRegion {
  private:
    unsigned m_i = 40; // m_i takes the values 1 2 3 4 / 9 10 11 12 / 17 18 19 20 / 25 26 27 28
  public:
    StationRegion() noexcept {}
    StationRegion( unsigned sta, unsigned reg ) noexcept : m_i( ( sta & 7 ) | ( ( reg & 3 ) << 3 ) ) {}
    /// station runs from 1 to 4 (LHCb "old") or 0 to 3 (upgrade), M2-M5 in both cases
    unsigned sta() const noexcept { return m_i & 7; }
    /// region runs from 0 to 3 always, no exceptions
    unsigned reg() const noexcept { return ( m_i >> 3 ) & 3; }
  };

  class MuonCandidate {
  private:
    std::array<float, 5>                m_dx, m_dy;
    std::array<StationRegion, 5>        m_stareg;
    std::array<const CommonMuonHit*, 5> m_hits;
    unsigned                            m_n = 0;

  public:
    unsigned             size() const noexcept { return m_n; }
    unsigned             sta( unsigned idx ) const noexcept { return m_stareg[idx].sta(); };
    unsigned             reg( unsigned idx ) const noexcept { return m_stareg[idx].reg(); };
    float                dx( unsigned idx ) const noexcept { return m_dx[idx]; };
    float                dy( unsigned idx ) const noexcept { return m_dy[idx]; };
    const CommonMuonHit* hit( unsigned idx ) const noexcept { return m_hits[idx]; }

    MuonCandidate( unsigned n, const std::array<const CommonMuonHit*, 5>& hits,
                   const std::array<float, 5>& xtrackextrap, const std::array<float, 5>& ytrackextrap )
        : m_hits( hits ), m_n( n ) {
      for ( unsigned i = 0; i < n; ++i ) {
        m_stareg[i] = StationRegion( hits[i]->station(), hits[i]->region() );
        m_dx[i]     = hits[i]->x() - xtrackextrap[i];
        m_dy[i]     = hits[i]->y() - ytrackextrap[i];
      }
    }
  };
  typedef std::vector<MuonCandidate> MuonCandidates;

  class MuonCandCovariance {
  private:
    StationStationCovarianceType m_cov;
    unsigned                     m_n = 0;

    static constexpr unsigned index( unsigned i, unsigned j ) noexcept {
      return ( ( i > j ) ? i : j ) * ( ( ( i > j ) ? i : j ) + 1 ) / 2 + ( ( i > j ) ? j : i );
    }

  public:
    unsigned                            size() const noexcept { return m_n; }
    const StationStationCovarianceType& cov() const noexcept { return m_cov; }
    StationStationCovarianceType&       cov() noexcept { return m_cov; }
    float  operator()( unsigned i, unsigned j ) const noexcept { return m_cov[index( i, j )]; }
    float& operator()( unsigned i, unsigned j ) noexcept { return m_cov[index( i, j )]; }

    MuonCandCovariance( const MuonCandidate& cand, float p, const ScatterCache& scattercache,
                        const SigmaPadCache& sigmapad )
        : m_n( cand.size() ) {
      const auto invp2 = 1. / ( p * p );
      for ( unsigned i = 0; i < size(); ++i ) {
        for ( unsigned j = 0; j <= i; ++j ) {
          operator()( i, j ) = scattercache[index( cand.sta( i ), cand.sta( j ) )] * invp2;
        }
        const auto sigma = sigmapad[cand.sta( i )][cand.reg( i )];
        operator()( i, i ) += sigma * sigma;
      }
    }
  };

  /** @brief a little multi index class
   *
   * cycle through all possible combination of indices in more than one
   * dimension, with the first dimension index increasing fastest.
   *
   * @tparam MAXDIM 	maximum number of dimensions that the multiindex can handle
   *
   * Example:
   * @code
   * std::array<unsigned, 4> max{3, 4, 2, 1};
   * for (MultiIndex<4> i(4, max); i; ++i) {
   *     cout << "Index: (" << i[0] << ", " << i[1] << ", " <<
   *         i[2] << ", " << i[3] << ")" << endl;
   * }
   * @endcode
   */
  template <unsigned DIMMAX = 5>
  class MultiIndex {
  private:
    unsigned                     m_dim;     ///< actual number of dimensions
    std::array<unsigned, DIMMAX> m_max;     ///< max. index in each dim.
    std::array<unsigned, DIMMAX> m_current; ///< current index in each dim.
  public:
    /** @brief constructs a MultiIndex
     *
     * @param dim 	actual number of dimensions
     * @param max 	maximum index in each dimension
     */
    MultiIndex( unsigned dim, const std::array<unsigned, DIMMAX>& max ) : m_dim( dim ), m_max( max ) {
      if ( dim > DIMMAX ) throw std::length_error( "MultiIndex" );
      m_current.fill( 0 );
    }
    /// return number of dimensions
    unsigned size() const noexcept { return m_dim; }
    /// return true while we haven't reached the end
    operator bool() const noexcept { return m_current != m_max; }
    /// move to next index, return reference to new value
    MultiIndex<DIMMAX>& operator++() noexcept {
      for ( unsigned i = 0; i < m_dim; ++i ) {
        ++m_current[i];
        if ( m_current[i] < m_max[i] ) break;
        m_current[i] = 0;
        if ( i + 1 == m_dim ) m_current = m_max;
      }
      return *this;
    }
    /// move to next index, return old value
    MultiIndex<DIMMAX> operator++( int ) noexcept {
      auto retVal = *this;
      ++( *this );
      return retVal;
    }
    /// access to index in given dimension
    unsigned operator[]( unsigned idx ) const noexcept { return m_current[idx]; }
  };

  // private methods
  float                               calcChi2( const MuonCandidate& cand, float p, const SigmaPadCache& sigmapadX,
                                                const SigmaPadCache& sigmapadY ) const;
  MuonCandidates                      buildCandidates( std::vector<std::pair<float, float>>                             trkExtrap,
                                                       std::vector<std::vector<std::pair<const CommonMuonHit*, float>>> dhit,
                                                       std::vector<int> matchedStationIndex, unsigned ord ) const;
  std::array<std::array<float, 4>, 4> invert( MuonChi2MatchTool::MuonCandCovariance cov ) const;
};
