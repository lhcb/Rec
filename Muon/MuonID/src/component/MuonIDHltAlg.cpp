/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/MuonPIDs_v2.h"
#include "Event/PrHits.h"
#include "Event/PrLongTracks.h"
#include "Event/Track_v3.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/SerializeSTL.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/Utils.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonID/IMVATool.h"
#include "nlohmann/json.hpp"
#include "vdt/vdtMath.h"
#include <algorithm>
#include <array>
#include <boost/algorithm/string.hpp>
#include <boost/container/static_vector.hpp>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <utility>
#include <vector>

using namespace LHCb::Event::v3;
using namespace std::string_literals;
namespace {
  using uint4       = std::array<unsigned, 4>;
  using xy_t        = std::pair<float, float>;
  using StatusMasks = LHCb::Event::v2::Muon::StatusMasks;
  using scalar_t    = SIMDWrapper::scalar::types;

  constexpr size_t nMuonStations = 4;
  constexpr size_t nRegions      = 4;

  using MuonPIDs               = LHCb::Event::v2::Muon::PIDs;
  using TracksHlt1             = LHCb::Pr::Long::Tracks;
  using TracksHlt2             = LHCb::Event::v3::Tracks;
  using MuonTrackExtrapolation = std::array<std::pair<float, float>, nMuonStations>;
  using MuonTrackOccupancies   = std::array<unsigned, nMuonStations>;
  using SigmaPadCache          = std::array<std::array<float, nRegions>, nMuonStations>;
  using ScatterCache           = std::array<float, 10>; // try to 15
  using CovType                = std::array<float, 10>; // unneeded

  // compute min/max limits for float exp(x) exponent
  constexpr float          margin   = 1000.0f; // safety margin (for any subsequent calculations)
  static inline const auto min_expo = LHCb::Math::fast_log( std::numeric_limits<float>::min() * margin );
  static inline const auto max_expo = LHCb::Math::fast_log( std::numeric_limits<float>::max() / margin );

  struct MiniID {
    LHCb::Event::flags_v<scalar_t, StatusMasks> flags;
    float                                       chi2Corr = -10000.0f;
    float                                       LLMu     = LHCb::Math::fast_log( 0.00001f );
    float                                       LLBg     = LHCb::Math::fast_log( 1.0f );
    float                                       CatBoost = -999.f;
    std::vector<LHCb::LHCbID>                   tiles;
  };

  class Cache {

  public:
#ifdef USE_DD4HEP
    DeMuonDetector m_det;
#else
    const DeMuonDetector* m_det = nullptr;
#endif
    std::size_t                      m_stationsCount = 0;
    std::size_t                      m_regionsCount  = 0;
    MuonTrackExtrapolation           m_regionInner, m_regionOuter;
    SigmaPadCache                    m_sigmapadX, m_sigmapadY;
    ScatterCache                     m_scattercache;
    std::array<float, nMuonStations> m_stationZ{ {} };

  public:
    template <typename PARENT>
    Cache( DeMuonDetector const& det, const PARENT& p )
        : m_stationsCount( det.stations() ) //
        , m_regionsCount( det.stations() > 0 ? det.regions() / det.stations() : 0 ) {

      auto& msg = p.msgStream( MSG::DEBUG );

      // DetElems with dd4hep are transient handles with no persistent lifetime so you
      // cannot take the memory address ofthem and cache this, as with DetDesc.
      // Instead just copy the handle.
#ifdef USE_DD4HEP
      m_det = det;
#else
      m_det = &det;
#endif

      // Update the cached DeMuon geometry (should be done by the detector element..)
      for ( int s = 0; s != det.stations(); ++s ) {
        m_regionInner[s] = std::make_pair( det.getInnerX( s ), det.getInnerY( s ) );
        m_regionOuter[s] = std::make_pair( det.getOuterX( s ), det.getOuterY( s ) );
        m_stationZ[s]    = det.getStationZ( s );
        // fill the matrix of pad sizes
        for ( int r = 0; r != det.regions() / det.stations(); ++r ) {
          m_sigmapadX[s][r] = det.getPadSizeX( s, r ) / std::sqrt( 12.0 );
          m_sigmapadY[s][r] = det.getPadSizeY( s, r ) / std::sqrt( 12.0 );
        }
        msg << "Station " << s << endmsg;
        msg << " Inner(X,Y)= " << m_regionInner[s].first << ", " << m_regionInner[s].second << endmsg;
        msg << " Outer(X,Y)= " << m_regionOuter[s].first << ", " << m_regionOuter[s].second << endmsg;
        msg << "          Z= " << m_stationZ[s] << endmsg;
      }

      // fill the mcs
      constexpr auto mcs =
          std::array{ std::pair<float, float>{ 12800. * Gaudi::Units::mm, 25. },    // ECAL + SPD + PS was 28?
                      std::pair<float, float>{ 14300. * Gaudi::Units::mm, 53. },    // HCAL
                      std::pair<float, float>{ 15800. * Gaudi::Units::mm, 47.5 },   // M23 filter
                      std::pair<float, float>{ 17100. * Gaudi::Units::mm, 47.5 },   // M34 filter
                      std::pair<float, float>{ 18300. * Gaudi::Units::mm, 47.5 } }; // M45 filter

      for ( int i = 0; i < det.stations(); ++i ) {
        assert( std::size_t( i ) < m_stationZ.size() );
        for ( int j = 0; j <= i; ++j ) {
          const std::size_t k = i * ( i + 1 ) / 2 + j;
          assert( k < m_scattercache.size() );
          m_scattercache[k] = std::accumulate( mcs.begin(), mcs.begin() + j + 2, 0.0f,
                                               [i, j, z = m_stationZ]( float sum, const auto& c ) {
                                                 return sum + ( z[i] - c.first ) * ( z[j] - c.first ) *
                                                                  std::pow( 13.6 * Gaudi::Units::MeV, 2 ) * c.second;
                                               } );
        }
      }

      msg << *this << endmsg;
    }

  public:
    friend inline std::ostream& operator<<( std::ostream& s, const Cache& c ) {
      using GaudiUtils::operator<<;
      s << "Cache { stations=" << c.m_stationsCount << " regions=" << c.m_regionsCount;
      s << " extrapolation: inner=" << c.m_regionInner << " outer=" << c.m_regionOuter;
      s << " sigmaPadCache: X=" << c.m_sigmapadX << " Y=" << c.m_sigmapadY;
      s << " scatterCache=" << c.m_scattercache;
      s << " stationZ=" << c.m_stationZ;
      return s << " }";
    }
  };

} // namespace

namespace MuonTag = LHCb::Event::v2::Muon::Tag;

template <typename TrackType>
class MuonIDHltAlg final
    : public LHCb::Algorithm::Transformer<MuonPIDs( const TrackType&, const MuonHitContainer&, const Cache& ),
                                          LHCb::Algorithm::Traits::usesConditions<Cache>> {
  using base_class_t = LHCb::Algorithm::Transformer<MuonPIDs( const TrackType&, const MuonHitContainer&, const Cache& ),
                                                    LHCb::Algorithm::Traits::usesConditions<Cache>>;
  using base_class_t::addConditionDerivation;
  using base_class_t::debug;
  using base_class_t::inputLocation;
  using base_class_t::msgLevel;
  using base_class_t::warning;
  using typename base_class_t::KeyValue;

private: // definitions
  static constexpr float m_lowMomentumThres  = 6000.f;
  static constexpr float m_highMomentumThres = 10000.f;

  float                              m_foiFactor = 1.;
  float                              lastbrkJ, lastbrkP, min_lastBrk, min_brkSize, max_lastBrk;
  float                              m_preSelMomentum = 0.;
  std::array<std::vector<double>, 3> m_foiParamX, m_foiParamY;
  std::array<std::vector<float>, 5>  m_dllSig, m_dllBkg;
  std::vector<double>                m_momentumCuts;

private:
  // tools
  ToolHandle<IMVATool> m_MVATool{ this, "MVATool", "MVATool" };

  // properties

  mutable Gaudi::Accumulators::BinomialCounter<>  m_momCutCount{ this, "nMomentumCut" };
  mutable Gaudi::Accumulators::BinomialCounter<>  m_acceptanceCount{ this, "nInAcceptance" };
  mutable Gaudi::Accumulators::BinomialCounter<>  m_isMuonCount{ this, "nIsMuon" };
  mutable Gaudi::Accumulators::BinomialCounter<>  m_isTightCount{ this, "nIsMuonTight" };
  mutable Gaudi::Accumulators::StatCounter<>      m_MuLLCount{ this, "MuLL" };
  mutable Gaudi::Accumulators::StatCounter<>      m_BgLLCount{ this, "BgLL" };
  mutable Gaudi::Accumulators::StatCounter<float> m_muonMVAStat{ this, "muonMVAStat" };

  Gaudi::Property<bool>        m_useTTrack{ this, "useTTrack", false };
  Gaudi::Property<bool>        m_ComputeChi2{ this, "ComputeChi2", true };
  Gaudi::Property<bool>        m_runMVA{ this, "runMVA", true };
  Gaudi::Property<std::string> m_paramfiles_location{ this, "ParamFilesLocation", "paramfile://data/MuonIDGlobal.json",
                                                      "Location of json file for muonDLL" };

  ServiceHandle<IFileAccess> m_file_svc{ this, "FileSvc", "ParamFileSvc", "Service used to retrieve file contents" };

public:
  MuonIDHltAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class_t{ name,
                      pSvcLocator,
                      // Inputs
                      { KeyValue{ "InputTracks", "" }, // LHCb::TrackLocation::Default,
                        KeyValue{ "InputMuonHits", MuonHitContainerLocation::Default },
                        KeyValue{ "DetectorCache", "MuonIDHltAlg-" + name + "-ConditionsCache" } },
                      // Output
                      KeyValue{ "OutputMuonPID", "" } } {}

  StatusCode initialize() override {
    return base_class_t::initialize().andThen( [&] {
      using namespace std::string_literals;

      m_file_svc.retrieve().orThrow( "Could not obtain IFileAccess Service" );
      auto const buffer = m_file_svc->read( m_paramfiles_location );
      if ( !buffer ) {
        throw GaudiException( "Failed to read " + m_paramfiles_location, this->name(), StatusCode::FAILURE );
      }

      auto j = nlohmann::json::parse( *buffer );

      for ( auto el = j.begin(); el != j.end(); ++el ) {
        if ( el.key().find( "PreSelMomentum" ) != std::string::npos ) {
          for ( auto preselcut : el.value() ) m_preSelMomentum = preselcut;
        } else if ( el.key().find( "FOIfactor" ) != std::string::npos ) {
          for ( auto foi : el.value() ) m_foiFactor = foi;
        } else if ( el.key().find( "MomentumCuts" ) != std::string::npos ) {
          for ( auto mcut : el.value() ) m_momentumCuts.push_back( mcut );
        } else if ( el.key().find( "XFOIParameters1" ) != std::string::npos ) {
          for ( auto xfoi : el.value() ) m_foiParamX[0].push_back( xfoi );
        } else if ( el.key().find( "XFOIParameters2" ) != std::string::npos ) {
          for ( auto xfoi : el.value() ) m_foiParamX[1].push_back( xfoi );
        } else if ( el.key().find( "XFOIParameters3" ) != std::string::npos ) {
          for ( auto xfoi : el.value() ) m_foiParamX[2].push_back( xfoi );
        } else if ( el.key().find( "YFOIParameters1" ) != std::string::npos ) {
          for ( auto yfoi : el.value() ) m_foiParamY[0].push_back( yfoi );
        } else if ( el.key().find( "YFOIParameters2" ) != std::string::npos ) {
          for ( auto yfoi : el.value() ) m_foiParamY[1].push_back( yfoi );
        } else if ( el.key().find( "YFOIParameters3" ) != std::string::npos ) {
          for ( auto yfoi : el.value() ) m_foiParamY[2].push_back( yfoi );
        } else if ( el.key().find( "DLLsig1" ) != std::string::npos ) {
          for ( auto dlls : el.value() ) m_dllSig[0].push_back( dlls );
        } else if ( el.key().find( "DLLsig2" ) != std::string::npos ) {
          for ( auto dlls : el.value() ) m_dllSig[1].push_back( dlls );
        } else if ( el.key().find( "DLLsig3" ) != std::string::npos ) {
          for ( auto dlls : el.value() ) m_dllSig[2].push_back( dlls );
        } else if ( el.key().find( "DLLsig4" ) != std::string::npos ) {
          for ( auto dlls : el.value() ) m_dllSig[3].push_back( dlls );
        } else if ( el.key().find( "DLLsig5" ) != std::string::npos ) {
          for ( auto dlls : el.value() ) m_dllSig[4].push_back( dlls );
        } else if ( el.key().find( "DLLbkg1" ) != std::string::npos ) {
          for ( auto dllb : el.value() ) m_dllBkg[0].push_back( dllb );
        } else if ( el.key().find( "DLLbkg2" ) != std::string::npos ) {
          for ( auto dllb : el.value() ) m_dllBkg[1].push_back( dllb );
        } else if ( el.key().find( "DLLbkg3" ) != std::string::npos ) {
          for ( auto dllb : el.value() ) m_dllBkg[2].push_back( dllb );
        } else if ( el.key().find( "DLLbkg4" ) != std::string::npos ) {
          for ( auto dllb : el.value() ) m_dllBkg[3].push_back( dllb );
        } else if ( el.key().find( "DLLbkg5" ) != std::string::npos ) {
          for ( auto dllb : el.value() ) m_dllBkg[4].push_back( dllb );
        }
      }

      // compute constant parameters for muonDLL
      lastbrkJ    = m_dllSig[4].back();
      lastbrkP    = m_dllBkg[4].back();
      min_lastBrk = std::min( lastbrkP, lastbrkJ );
      min_brkSize = std::min( m_dllBkg[4].size(), m_dllSig[4].size() );
      max_lastBrk = std::max( lastbrkP, lastbrkJ );

      this->addConditionDerivation( { DeMuonLocation::Default }, this->template inputLocation<Cache>(),
                                    [p = this]( const DeMuonDetector& d ) {
                                      return Cache{ d, *p };
                                    } );

      // force debug output
      // return this->setProperty( "OutputLevel", MSG::DEBUG );
    } );
  }

  /** Iterates over all tracks in the current event and performs muon id on them.
   * Resulting PID objects are stored on the TES.
   */
  MuonPIDs operator()( const TrackType& input_tracks, const MuonHitContainer& hitContainer,
                       const Cache& cond ) const override {

    using StatusMasks = LHCb::Event::v2::Muon::StatusMasks;

    auto muonIDLambda = [&]( auto const& track ) {
      // TODO: Put to where the values are set after the
      // muonMap thing has been figured out...
      MiniID muPid;
      float  trackmom;
      if constexpr ( std::is_same_v<TrackType, TracksHlt1> ) {
        trackmom = LHCb::Utils::as_arithmetic( track.p() );
      } else {
        trackmom = track.p( track.defaultState() ).cast();
      }
      const bool preSel = ( trackmom > m_preSelMomentum );
      if ( !preSel ) return muPid;
      muPid.flags.set<StatusMasks::PreSelMomentum>( preSel );

      MuonTrackExtrapolation extrapolation;
      if constexpr ( std::is_same_v<TrackType, TracksHlt1> ) {
        extrapolation = extrapolateTrack( track.StatePosDir( SL::EndT ), cond );
      } else {
        extrapolation = extrapolateTrack( track.StatePosDir( SL::EndRich2 ), cond );
      }
      const bool inAcc = inAcceptance( extrapolation, cond );
      if ( !inAcc ) return muPid;
      muPid.flags.set<StatusMasks::InAcceptance>( inAcc );

      auto [occupancies, occupanciesTight] = countHits( trackmom, extrapolation, hitContainer, cond );
      const bool isM                       = isMuon( occupancies, trackmom );
      const bool isMTight                  = isMuon( occupanciesTight, trackmom );

      muPid.flags.set<StatusMasks::IsMuon>( isM );
      muPid.flags.set<StatusMasks::IsMuonTight>( isMTight );

      if ( isM ) {
        auto [chi2Corr, tiles]   = chi2CorrAndTileIDs( trackmom, extrapolation, hitContainer, occupancies, cond );
        muPid.chi2Corr           = chi2Corr;
        auto [probMu, probNonMu] = calcProbs( muPid.chi2Corr );
        muPid.LLMu               = LHCb::Math::fast_log( probMu );
        muPid.LLBg               = LHCb::Math::fast_log( probNonMu );
        muPid.tiles.reserve( tiles.size() );
        for ( auto id : tiles )
          if ( id.isMuon() ) muPid.tiles.emplace_back( id );
        // The batch case is handled outside of the per-track function
        if ( m_runMVA ) { //&& !m_MVATool->getRunCatBoostBatch()) {
          const float CatBoostPrediction =
              m_MVATool->calcBDT( trackmom, extrapolation, hitContainer, muPid.chi2Corr, cond.m_stationZ ).CatBoost;
          muPid.CatBoost = CatBoostPrediction;
        }
      }
      return muPid;
    };

    LHCb::Event::v2::Muon::PIDs muonPIDs{ input_tracks.zipIdentifier(), input_tracks.get_allocator() };
    muonPIDs.reserve( input_tracks.size() );

    for ( auto track : input_tracks.scalar() ) {
      auto muPid = muonIDLambda( track );
      auto pid   = muonPIDs.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
      if constexpr ( std::is_same_v<TrackType, TracksHlt2> ) {
        pid.field<MuonTag::Tiles>().resize( muPid.tiles.size() );
        for ( unsigned int i = 0; i < muPid.tiles.size(); i++ ) {
          const auto tileid = LHCb::Event::lhcbid_v<SIMDWrapper::scalar::types>( muPid.tiles[i] );
          pid.field<MuonTag::Tiles>()[i].template field<MuonTag::LHCbID>().set( tileid );
          pid.field<MuonTag::Tiles>()[i].template field<MuonTag::Index>().set( i );
        }
      }
      pid.field<MuonTag::Status>().set( muPid.flags );
      pid.field<MuonTag::Chi2Corr>().set( muPid.chi2Corr );
      pid.field<MuonTag::LLMu>().set( muPid.LLMu );
      pid.field<MuonTag::LLBg>().set( muPid.LLBg );
      pid.field<MuonTag::CatBoost>().set( muPid.CatBoost );

      // -- update all counters (easier here than in Lambda)
      m_momCutCount += (int)muPid.flags.has( StatusMasks::PreSelMomentum ).cast();
      m_acceptanceCount += (int)muPid.flags.has( StatusMasks::InAcceptance ).cast();
      m_isMuonCount += (int)muPid.flags.has( StatusMasks::IsMuon ).cast();
      m_isTightCount += (int)muPid.flags.has( StatusMasks::IsMuonTight ).cast();
      // we only count it when the value is not defaulted to background
      if ( muPid.flags.has( StatusMasks::IsMuon ).cast() ) {
        m_MuLLCount += muPid.LLMu;
        m_BgLLCount += muPid.LLBg;
        m_muonMVAStat += muPid.CatBoost;
      }
    }

    return muonPIDs;
  }

private: // methods
  /** Checks 'isMuon' given the occupancies corresponding to a track and the
   * track's momentum. The requirement differs in bins of p.
   */
  bool isMuon( const MuonTrackOccupancies& occupancies, float p ) const {
    if ( p < m_preSelMomentum ) return false;
    auto has = [&]( unsigned station ) { return occupancies[station] != 0; };
    if ( !has( 0 ) || !has( 1 ) ) return false;
    if ( p < m_momentumCuts[0] ) return true;
    if ( p < m_momentumCuts[1] ) return has( 2 ) || has( 3 );
    return has( 2 ) && has( 3 );
  }

  /** Helper function that returns the x, y coordinates of the FoI for a
   * given station and region and a track's momentum.
   */
  std::pair<float, float> foi( unsigned int station, unsigned int region, float p, const Cache& cond ) const {
    auto i = station * cond.m_regionsCount + region;
    if ( p < 1000000.f ) { // limit to 1 TeV momentum tracks
      const auto pGev                = p / static_cast<float>( Gaudi::Units::GeV );
      auto       ellipticalFoiWindow = [&]( const auto& fp ) {
        const float expo = -fp[2][i] * pGev;
        return fp[0][i] + fp[1][i] * vdt::fast_expf( std::clamp( expo, min_expo, max_expo ) );
      };
      return { ellipticalFoiWindow( m_foiParamX ), ellipticalFoiWindow( m_foiParamY ) };
    }
    return { m_foiParamX[0][i], m_foiParamY[0][i] };
  }

  /** Check whether track extrapolation is within detector acceptance.
   */
  bool inAcceptance( const MuonTrackExtrapolation& extrapolation, const Cache& cond ) const {
    auto abs_lt = []( const xy_t& p1, const xy_t& p2 ) {
      return std::abs( p1.first ) < p2.first && std::abs( p1.second ) < p2.second;
    };

    // Outer acceptance
    if ( !abs_lt( extrapolation[0], cond.m_regionOuter[0] ) ||
         !abs_lt( extrapolation[cond.m_stationsCount - 1], cond.m_regionOuter[cond.m_stationsCount - 1] ) ) {
      // Outside M2 - M5 region
      return false;
    }

    // Inner acceptance
    if ( abs_lt( extrapolation[0], cond.m_regionInner[0] ) ||
         abs_lt( extrapolation[cond.m_stationsCount - 1], cond.m_regionInner[cond.m_stationsCount - 1] ) ) {
      // Inside M2 - M5 chamber hole
      return false;
    }

    return true;
  }

  /** Project a given track into the muon stations.
   */
  template <typename State>
  MuonTrackExtrapolation extrapolateTrack( State const& state, const Cache& cond ) const {
    MuonTrackExtrapolation extrapolation;

    // Project the state into the muon stations
    // Linear extrapolation equivalent to TrackLinearExtrapolator
    for ( unsigned station = 0; station != cond.m_stationsCount; ++station ) {
      // x(z') = x(z) + (dx/dz * (z' - z))
      extrapolation[station] = { LHCb::Utils::as_arithmetic( state.x() ) +
                                     LHCb::Utils::as_arithmetic( state.tx() ) *
                                         ( cond.m_stationZ[station] - LHCb::Utils::as_arithmetic( state.z() ) ),
                                 LHCb::Utils::as_arithmetic( state.y() ) +
                                     LHCb::Utils::as_arithmetic( state.ty() ) *
                                         ( cond.m_stationZ[station] - LHCb::Utils::as_arithmetic( state.z() ) ) };
    }
    return extrapolation;
  }

  std::array<uint4, 2> countHits( const float p, const MuonTrackExtrapolation& extrapolation,
                                  const MuonHitContainer& hitContainer, const Cache& cond ) const {

    // decide the ordering to look for hits
    // The correct order to look should be momentum dependent
    // M3, M2                  3<p<6 GeV
    // (M4 or M5) and M3, M2   6<p<10 GeV
    // M5, M4, M3, M2          p>10 GeV
    const auto ordering{ p >= m_lowMomentumThres ? uint4{ 3, 2, 1, 0 } : uint4{ 1, 0, 3, 2 } };
    uint4      occupancies{ 0, 0, 0, 0 };
    uint4      occupancies_crossed{ 0, 0, 0, 0 };

    /* Define an inline callable that makes it easier to check whether a hit is
     * within a given window. */
    class IsInWindow {
      xy_t                center_;
      std::array<xy_t, 4> foi_;

    public:
      // Constructor takes parameters by value and then moves them into place.
      IsInWindow( xy_t center, xy_t foi0, xy_t foi1, xy_t foi2, xy_t foi3, double sf )
          : center_{ std::move( center ) }
          , foi_{ { std::move( foi0 ), std::move( foi1 ), std::move( foi2 ), std::move( foi3 ) } } {
        std::transform( begin( foi_ ), end( foi_ ), begin( foi_ ), [sf]( const xy_t& p ) -> xy_t {
          return { p.first * sf, p.second * sf };
        } );
      }
      bool operator()( const CommonMuonHit& hit ) const {
        auto region = hit.region();
        assert( region < 4 );
        return ( std::abs( hit.x() - center_.first ) < hit.dx() * foi_[region].first ) &&
               ( std::abs( hit.y() - center_.second ) < hit.dy() * foi_[region].second );
      }
    };

    auto makeIsInWindow = [&]( unsigned station ) {
      return IsInWindow{ extrapolation[station],     foi( station, 0, p, cond ), foi( station, 1, p, cond ),
                         foi( station, 2, p, cond ), foi( station, 3, p, cond ), m_foiFactor };
    };

    for ( unsigned station : ordering ) {
      auto predicate = makeIsInWindow( station );

      for ( const auto& hit : hitContainer.station( station ).hits() ) {
        if ( predicate( hit ) ) {
          occupancies[station]++;
          if ( !hit.uncrossed() ||
#ifdef USE_DD4HEP
               cond.m_det.mapInRegion( station, hit.region() ) == 1
#else
               cond.m_det->mapInRegion( station, hit.region() ) == 1
#endif
          ) {
            occupancies_crossed[station]++;
          }
        }
      }

      // evaluate the occupancies from less to more occupied to avoid
      // useless loops through stations
      if ( station < 2 && occupancies[station] == 0 ) break;
      if ( station == 2 && occupancies[station] == 0 && occupancies[station + 1] == 0 && p > m_lowMomentumThres &&
           p < m_highMomentumThres )
        break;
      if ( p > m_highMomentumThres && occupancies[station] == 0 ) break;
    }

    return { occupancies, occupancies_crossed };
  }

  /** Computes the correlated chi2
   */
  std::tuple<float, std::array<LHCb::LHCbID, nMuonStations>>
  chi2CorrAndTileIDs( const float p, const MuonTrackExtrapolation& extrapolation, const MuonHitContainer& hitContainer,
                      const MuonTrackOccupancies& occupancies, const Cache& cond ) const {

    std::array<LHCb::LHCbID, nMuonStations> tileIDs;

    const unsigned int order =
        std::count_if( occupancies.begin(), occupancies.end(), []( unsigned i ) { return i != 0; } );
    if ( order < 2 ) return std::make_tuple( -10000., tileIDs );

    struct MuonHit {
      unsigned sta, reg;
      double   deltax, deltay;
      MuonHit( unsigned s, unsigned r, double dx, double dy ) // remove with c++20
          : sta{ s }, reg{ r }, deltax{ dx }, deltay{ dy } {}
    };
    boost::container::static_vector<MuonHit, 4> cand;

    for ( unsigned i = 0; i < cond.m_stationsCount; i++ ) {

      if ( !occupancies[i] ) continue;
      if ( i > 1 && p < 6000 ) break;

      // take the hit closest to the extrapolation point
      assert( hitContainer.station( i ).hits().begin() != hitContainer.station( i ).hits().end() );
      const auto chit = *std::min_element(
          hitContainer.station( i ).hits().begin(), hitContainer.station( i ).hits().end(),
          [i, extrapolation]( const CommonMuonHit& hit1, const CommonMuonHit& hit2 ) {
            float dr1 = pow( hit1.x() - extrapolation[i].first, 2 ) + pow( hit1.y() - extrapolation[i].second, 2 );
            float dr2 = pow( hit2.x() - extrapolation[i].first, 2 ) + pow( hit2.y() - extrapolation[i].second, 2 );
            return dr1 < dr2;
          } );
      cand.emplace_back( i, chit.region(), chit.x() - extrapolation[i].first, chit.y() - extrapolation[i].second );
      if constexpr ( std::is_same_v<TrackType, TracksHlt2> ) tileIDs[i] = LHCb::LHCbID{ chit.tile() };
    }

    class MuonCandCovariance {
    private:
      CovType              m_cov = { 0 };
      unsigned             m_n   = 0;
      static constexpr int index( int i, int j ) noexcept {
        auto mx = std::max( i, j );
        auto mn = std::min( i, j );
        return mx * ( mx + 1 ) / 2 + mn;
      }

    public:
      unsigned       size() const noexcept { return m_n; }
      const CovType& cov() const noexcept { return m_cov; }
      CovType&       cov() noexcept { return m_cov; }
      float          operator()( unsigned i, unsigned j ) const noexcept { return m_cov[index( i, j )]; }
      float&         operator()( unsigned i, unsigned j ) noexcept { return m_cov[index( i, j )]; }
      MuonCandCovariance( LHCb::span<const MuonHit> cand, float p, const ScatterCache& scattercache,
                          const SigmaPadCache& sigmapad )
          : m_n( cand.size() ) {
        const auto invp2 = 1. / ( p * p );
        for ( unsigned i = 0; i < size(); ++i ) {
          for ( unsigned j = 0; j <= i; ++j ) {
            operator()( i, j ) = scattercache[index( cand[i].sta, cand[j].sta )] * invp2;
          }
          const auto sigma = sigmapad[cand[i].sta][cand[i].reg];
          operator()( i, i ) += sigma * sigma;
        }
      }
    };

    MuonCandCovariance covX( cand, p, cond.m_scattercache, cond.m_sigmapadX );
    MuonCandCovariance covY( cand, p, cond.m_scattercache, cond.m_sigmapadY );

    ROOT::Math::CholeskyDecompGenDim<float> decompX( covX.size(), &covX( 0, 0 ) );
    ROOT::Math::CholeskyDecompGenDim<float> decompY( covY.size(), &covY( 0, 0 ) );

    if ( !decompX || !decompY ) return std::make_tuple( -10000., tileIDs );
    decompX.Invert( &covX( 0, 0 ) );
    decompY.Invert( &covY( 0, 0 ) );

    float chi2corr = 0;
    for ( unsigned i = 0; i < cand.size(); ++i ) {
      for ( unsigned j = 0; j < i; ++j ) {
        chi2corr +=
            2 * ( cand[i].deltax * covX( i, j ) * cand[j].deltax + cand[i].deltay * covY( i, j ) * cand[j].deltay );
      }
      chi2corr += cand[i].deltax * covX( i, i ) * cand[i].deltax + cand[i].deltay * covY( i, i ) * cand[i].deltay;
    }

    return std::make_tuple( chi2corr / cand.size(), tileIDs );
  }

  /** Helper for DLL calculation
   */
  float poly3Back( const std::array<std::vector<float>, 5>& dll, float x ) const {
    assert( !dll[0].empty() && !dll[1].empty() && !dll[2].empty() && !dll[3].empty() && "dll vector is empty" );
    return dll[3].back() + dll[2].back() * x + dll[1].back() * x * x + dll[0].back() * x * x * x;
  }

  /** Helper for DLL calculation
   */
  float poly3At( const std::array<std::vector<float>, 5>& dll, float x, int pos ) const {
    assert( !dll[0].empty() && !dll[1].empty() && !dll[2].empty() && !dll[3].empty() && "dll vector is empty" );
    return dll[3].at( pos ) + dll[2].at( pos ) * x + dll[1].at( pos ) * x * x + dll[0].at( pos ) * x * x * x;
  }

  /** Compute the muonDLL
   */
  std::pair<float, float> calcProbs( const float chi2Corr ) const {

    int   base      = 0.;
    float probMu    = 0.00001;
    float probNonMu = 1.;
    float pivotP    = std::max( m_dllBkg[4].size(), m_dllSig[4].size() );

    if ( chi2Corr < min_lastBrk ) { pivotP = min_brkSize; }
    if ( chi2Corr < 0. || chi2Corr > 87. ) {
      // the value at 87 is the lastest not giving nan.
      // the default value at muonDLL -11.5 is chosen as it is the value at which the muonDLL saturates
      return { probMu, probNonMu };
    } else if ( chi2Corr > max_lastBrk ) {
      probMu    = 1 - poly3Back( m_dllSig, chi2Corr - lastbrkJ );
      probNonMu = poly3Back( m_dllBkg, chi2Corr - lastbrkP );
    } else {
      while ( pivotP >= 2. ) {
        float tmp = pivotP * 0.5;
        pivotP    = (int)floor( tmp );
        if ( chi2Corr >= m_dllBkg[4].at( base + pivotP ) ) {
          base += pivotP;
          if ( pivotP < tmp ) { pivotP++; }
        }
      }
      const float xd = chi2Corr - m_dllBkg[4].at( base );
      probNonMu      = poly3At( m_dllBkg, xd, base );

      if ( chi2Corr < lastbrkJ ) {
        const float xc = chi2Corr - m_dllSig[4].at( base );
        probMu         = 1 - poly3At( m_dllSig, xc, base );
      } else {
        const float xbc = chi2Corr - lastbrkJ;
        probMu          = 1 - poly3Back( m_dllSig, xbc );
      }
    }
    return { probMu, probNonMu };
  }
};

DECLARE_COMPONENT_WITH_ID( MuonIDHltAlg<TracksHlt1>, "MuonIDHlt1Alg" )
DECLARE_COMPONENT_WITH_ID( MuonIDHltAlg<TracksHlt2>, "MuonIDHlt2Alg" )
