/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCb
#include "Event/PrHits.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/MuonHitContainer.h"
#include <Detector/Muon/MuonConstants.h>

namespace LHCb {

  /**
   * Converter from MuonPIDs to MuonTracks so that alignment can work
   *
   * This has been developed as a work around after the replacement of MuonIDAlgLite by
   * MuonIDHlt2Alg, as the former one was creating a list of MuonTracks while the later
   * does not.
   */

  using CommonMuonHits = std::vector<CommonMuonHit, LHCb::Allocators::EventLocal<CommonMuonHit>>;

  class MuonHitContainerToCommonMuonHits : public Algorithm::Transformer<CommonMuonHits( const MuonHitContainer& )> {

  public:
    MuonHitContainerToCommonMuonHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{ "Input", "" }, KeyValue{ "Output", "" } ) {}

    CommonMuonHits operator()( const MuonHitContainer& hitContainer ) const override {
      CommonMuonHits out;
      out.reserve( hitContainer.hits( 0 ).size() * LHCb::Detector::Muon::nStations );
      for ( std::size_t s = 0; s < LHCb::Detector::Muon::nStations; ++s ) {
        auto const& hits = hitContainer.hits( s );
        out.insert( out.end(), hits.begin(), hits.end() );
      }
      return out;
    }
  };

  DECLARE_COMPONENT_WITH_ID( MuonHitContainerToCommonMuonHits, "MuonHitContainerToCommonMuonHits" )

} // namespace LHCb
