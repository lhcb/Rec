/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonPID.h"
#include "Event/ObjectContainersSharedMerger.h"
#include "LHCbAlgs/MergingTransformer.h"

#include <memory>
#include <numeric>

namespace LHCb {

  /**
   *  Merges MuonPID objects from multiple containers into one based on MergeRichPIDs
   *
   *  @author Ricardo Vazquez Gomez
   *  @date   2021-12-16
   */
  struct MergeMuonPIDsV1 final
      : Algorithm::MergingTransformer<LHCb::MuonPIDs( const Gaudi::Functional::vector_of_const_<LHCb::MuonPIDs>& )> {
    MergeMuonPIDsV1( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingTransformer( name, pSvcLocator, { "InputMuonPIDLocations", {} },
                              { "OutputMuonPIDLocation", LHCb::MuonPIDLocation::Default } ) {}
    LHCb::MuonPIDs operator()( const Gaudi::Functional::vector_of_const_<LHCb::MuonPIDs>& inPIDs ) const override;
  };

  LHCb::MuonPIDs
  MergeMuonPIDsV1::operator()( const Gaudi::Functional::vector_of_const_<LHCb::MuonPIDs>& inPIDs ) const {
    // the merged PID container
    LHCb::MuonPIDs outPIDs;
    // reserve total size
    outPIDs.reserve( std::accumulate( inPIDs.begin(), inPIDs.end(), 0u,
                                      []( const auto sum, const auto& pids ) { return sum + pids.size(); } ) );
    for ( const auto& pids : inPIDs ) {
      for ( const auto& pid : pids ) {
        auto newPID = std::make_unique<LHCb::MuonPID>( *pid );
        outPIDs.insert( newPID.get() );
        newPID.release();
      }
    }
    return outPIDs;
  }

  using MuonPIDContainersSharedMerger = LHCb::ObjectContainersSharedMerger<LHCb::MuonPID, LHCb::MuonPIDs>;

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( MergeMuonPIDsV1, "MergeMuonPIDsV1" )
  DECLARE_COMPONENT_WITH_ID( MuonPIDContainersSharedMerger, "MuonPIDContainersSharedMerger" )

} // namespace LHCb
