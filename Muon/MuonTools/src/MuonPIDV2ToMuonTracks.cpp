/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MuonPIDs_v2.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include <memory>

namespace LHCb {

  /**
   * Converter from MuonPIDs_v2 to MuonTracks so that muon hits can be added to the linked tracks
   *
   * This has been developed as a work around until MuonPIDs_v2 (all v2event model) can be used directly in HLT2
   * selection.
   */
  using MuonPIDs = LHCb::Event::v2::Muon::PIDs;
  class MuonPIDV2ToMuonTracks : public Algorithm::Transformer<Tracks( const MuonPIDs& )> {

  public:
    MuonPIDV2ToMuonTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, { KeyValue{ "InputMuonPIDs", "" } }, KeyValue{ "OutputMuonTracks", "" } ) {}

    Tracks operator()( const MuonPIDs& muonPIDs ) const override {
      Tracks out;
      out.reserve( muonPIDs.size() );
      m_nbMuonPIDsCounter += muonPIDs.size();

      for ( auto const& muonPID : muonPIDs.scalar() ) {
        auto const& muonids = muonPID.lhcbIDs();
        if ( muonids.empty() ) continue;
        auto outTrack = std::make_unique<LHCb::Event::v1::Track>();
        outTrack->addToLhcbIDs( muonids );
        out.insert( outTrack.release(), muonPID.indices().cast() );
      }
      return out;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbMuonPIDsCounter{ this, "Nb of input v2 MuonPIDs" };
  };

  DECLARE_COMPONENT_WITH_ID( MuonPIDV2ToMuonTracks, "MuonPIDV2ToMuonTracks" )

} // namespace LHCb
