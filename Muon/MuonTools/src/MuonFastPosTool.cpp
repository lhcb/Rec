/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/Muon/Namespace.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/IMuonFastPosTool.h"
#include "MuonDet/MuonTilePosition.h"

#include "DetDesc/GenericConditionAccessorHolder.h"

#include "GaudiAlg/GaudiTool.h"

#include <limits>

/**
 *  Convert an MuonTileID into an xyz position in the detector (with size)
 *  No abstract interface as I do not want to make more than one of these...
 *
 *  @author David Hutchcroft
 *  @date   07/03/2002
 */
namespace {
  std::optional<DeMuonDetector::TilePosition> adjust( LHCb::Muon::ComputeTilePosition::Result r ) {
    return DeMuonDetector::TilePosition{ r.p.X(), r.p.Y(), r.p.Z(),
                                         r.dX,    r.dY,    std::numeric_limits<double>::signaling_NaN() };
  }
} // namespace

namespace LHCb {

  class MuonFastPosTool : public extends<DetDesc::ConditionAccessorHolder<GaudiTool>, IMuonFastPosTool> {
  public:
    using extends::extends;

    StatusCode initialize() override {
      return GaudiTool::initialize().andThen( [&]() {
        addConditionDerivation(
            { Detector::Muon::Location::Default }, m_compute.key(),
            [&]( DeMuonDetector const& muonDet ) -> Muon::ComputeTilePosition { return { muonDet }; } );
      } );
    }

    /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
     * this ignores gaps: these can never be read out independently
     */
    std::optional<DeMuonDetector::TilePosition> calcTilePos( const Detector::Muon::TileID& tile ) const override {
      return adjust( m_compute.get().tilePosition( tile ) );
    }

    std::optional<DeMuonDetector::TilePosition> calcStripXPos( const Detector::Muon::TileID& tile ) const override {
      return adjust( m_compute.get().stripXPosition( tile ) );
    }

    std::optional<DeMuonDetector::TilePosition> calcStripYPos( const Detector::Muon::TileID& tile ) const override {
      return adjust( m_compute.get().stripYPosition( tile ) );
    }

  private:
    ConditionAccessor<Muon::ComputeTilePosition> m_compute{ this, name() + "_ComputeTilePosition" };
  };
  DECLARE_COMPONENT_WITH_ID( MuonFastPosTool, "MuonFastPosTool" )

} // namespace LHCb
