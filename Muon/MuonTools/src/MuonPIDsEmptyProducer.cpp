/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MuonPID.h"
#include "Event/Track.h"
#include "LHCbAlgs/Producer.h"

struct MuonPIDsEmptyProducer final : LHCb::Algorithm::Producer<LHCb::MuonPIDs()> {
  MuonPIDsEmptyProducer( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Producer<LHCb::MuonPIDs()>{ name, pSvcLocator, { "Output", "" } } {}

  LHCb::MuonPIDs operator()() const override { return {}; }

  DataObjectReadHandle<LHCb::Tracks> m_muontracks{ this, "InputMuonTracks", "" };
};

DECLARE_COMPONENT_WITH_ID( MuonPIDsEmptyProducer, "MuonPIDsEmptyProducer" )
