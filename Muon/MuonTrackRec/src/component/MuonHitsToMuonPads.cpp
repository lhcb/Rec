/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "Event/PrHits.h"
#include "Event/RawBank.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/ToolHandle.h"

#include "MuonInterfaces/MuonPad.h"
#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <optional>
#include <string>
#include <vector>

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */
using namespace Muon::DAQ;
namespace LHCb::Muon::MuonTrackRec {

  class HitsToPads final
      : public Algorithm::Transformer<MuonPads( const EventContext&, const MuonHitContainer&, const DeMuonDetector& ),
                                      Algorithm::Traits::usesConditions<DeMuonDetector>> {
  public:
    HitsToPads( const std::string& name, ISvcLocator* pSvcLocator );
    MuonPads operator()( const EventContext&, const MuonHitContainer&, const DeMuonDetector& ) const override;

  private:
  };

  DECLARE_COMPONENT_WITH_ID( HitsToPads, "MuonHitsToMuonPads" )
  //=============================================================================
  // Standard constructor
  //=============================================================================
  HitsToPads::HitsToPads( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "HitContainerLocation", MuonHitContainerLocation::Default },
                       KeyValue{ "MuonDetectorLocation", DeMuonLocation::Default } },
                     KeyValue{ "PadContainer", MuonPadContainerLocation::Default } ) {}

  MuonPads HitsToPads::operator()( const EventContext&, const MuonHitContainer& muonHits,
                                   const DeMuonDetector& det ) const {

    MuonPads pads{};

    int NStation = det.stations();
    if ( msgLevel( MSG::DEBUG ) ) debug() << "station # " << NStation << endmsg;
    if ( NStation > 4 ) throw GaudiException( "two many stations M1 will be ignored", name(), StatusCode::FAILURE );
    for ( int stat = 0; stat < NStation; stat++ ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << " station " << stat << endmsg;
      const auto mhits = muonHits.hits( stat );
      if ( mhits.size() == 0 ) { continue; }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "hits size " << mhits.size() << endmsg;
      for ( const auto& muonhit : mhits ) {
        MuonPad::LogPadType type = MuonPad::LogPadType::NOX;
        MuonPad             pad( &muonhit, true, type );

        bool truepad = false;

        if ( msgLevel( MSG::DEBUG ) )
          debug() << "hit  " << muonhit.station() << " " << muonhit.region() << " "
                  << det.getLogMapInRegion( muonhit.station(), muonhit.region() ) << " "
                  << det.readoutInRegion( muonhit.station(), muonhit.region() ) << endmsg;
        if ( det.getLogMapInRegion( muonhit.station(), muonhit.region() ) == 1 ) {
          truepad = true;
          type    = MuonPad::LogPadType::NOX;
        } else {
          if ( muonhit.uncrossed() ) {
            type = MuonPad::LogPadType::UNPAIRED;
          } else {
            truepad = true;
            if ( det.readoutInRegion( muonhit.station(), muonhit.region() ) == 1 ) type = MuonPad::LogPadType::XONEFE;
            if ( det.readoutInRegion( muonhit.station(), muonhit.region() ) == 2 ) type = MuonPad::LogPadType::XTWOFE;
          }
        }
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "hit  cross truepad type " << muonhit.uncrossed() << " " << truepad << " "
                  << static_cast<int>( type ) << endmsg;
        pads.emplace_back( &muonhit, truepad, type );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "processing station # " << stat << " ended" << endmsg;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << "pads size " << pads.size() << endmsg;
    return pads;
  }
} // namespace LHCb::Muon::MuonTrackRec
