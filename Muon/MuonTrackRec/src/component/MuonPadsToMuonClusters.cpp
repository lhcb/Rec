/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "Event/RawBank.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/ToolHandle.h"

#include "MuonInterfaces/MuonCluster.h"
#include "MuonInterfaces/MuonPad.h"
#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <optional>
#include <string>
#include <vector>

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */
using namespace Muon::DAQ;
namespace LHCb::Muon::MuonTrackRec {

  class PadsToClusters final
      : public Algorithm::Transformer<MuonClusters( const EventContext&, const MuonPads&, const DeMuonDetector& ),
                                      Algorithm::Traits::usesConditions<DeMuonDetector>> {
  public:
    PadsToClusters( const std::string& name, ISvcLocator* pSvcLocator );

    MuonClusters operator()( const EventContext&, const MuonPads&, const DeMuonDetector& ) const override;

  private:
    Gaudi::Property<unsigned int> m_maxPadsPerStation{ this, "MaxPadsPerStation", 1500 };
    int                           regX( const LHCb::Detector::Muon::TileID tile ) const {
      return ( ( tile.quarter() > 1 ? -1 : 1 ) * tile.nX() );
    }
    int regY( const LHCb::Detector::Muon::TileID tile ) const {
      return ( ( ( tile.quarter() > 0 && tile.quarter() < 3 ) ? -1 : 1 ) * tile.nY() );
    }
  };

  DECLARE_COMPONENT_WITH_ID( PadsToClusters, "MuonPadsToMuonClusters" )
  //=============================================================================
  // Standard constructor
  //=============================================================================
  PadsToClusters::PadsToClusters( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "PadContainerLocation", MuonPadContainerLocation::Default },
                       KeyValue{ "MuonDetectorLocation", DeMuonLocation::Default } },
                     KeyValue{ "ClusterContainer", MuonClusterContainerLocation::Default } ) {}

  MuonClusters PadsToClusters::operator()( const EventContext&, const MuonPads& muonPads,
                                           const DeMuonDetector& det ) const {

    int                            nhits = 0;
    std::map<const MuonPad*, bool> usedPad;
    bool                           searchNeighbours = true;

    std::vector<std::vector<const MuonPad*>> stationPads{ static_cast<size_t>( det.stations() ) };
    for ( auto& isP : stationPads ) isP.reserve( muonPads.size() );

    if ( msgLevel( MSG::DEBUG ) ) {
      for ( const auto& pad : muonPads ) {
        if ( !pad.truepad() ) continue;
        debug() << "LOGPAD Q" << ( pad.tile().quarter() + 1 ) << "M" << ( pad.tile().station() + 1 ) << "R"
                << ( pad.tile().region() + 1 ) << " nX=" << pad.tile().nX() << " nY=" << pad.tile().nY()
                << " time=" << pad.time() << " +/-" << pad.dtime() << endmsg;
      }
    }
    for ( const auto& pad : muonPads ) {
      if ( pad.truepad() ) stationPads[pad.tile().station()].push_back( &pad );
    }

    MuonClusters clusters{};

    for ( int station = 0; station < det.stations(); station++ ) {
      if ( stationPads[station].size() > m_maxPadsPerStation ) {
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "skipping station M" << station + 1 << " with too many pads:" << stationPads[station].size()
                  << endmsg;
        continue;
      }
      for ( auto ipad = stationPads[station].begin(); ipad != stationPads[station].end(); ipad++ ) {
        if ( !usedPad.count( *ipad ) ) {
          // cluster seed
          usedPad[*ipad] = true;
          auto i_pos     = det.position( ( *ipad )->tile() );
          if ( !i_pos.has_value() ) {
            if ( msgLevel( MSG::DEBUG ) ) debug() << "position not found" << endmsg;
            continue;
          }
          if ( i_pos ) clusters.emplace_back( MuonCluster( *ipad, i_pos.value() ) );
          MuonCluster& muon_cluster = clusters.back();
          // store a progressive hit number for debugging purposes
          muon_cluster.setHitID( ++nhits );
          // now search for adjacent pads
          searchNeighbours = true;
          while ( searchNeighbours ) {
            searchNeighbours = false;
            for ( auto jpad = std::next( ipad ); jpad != stationPads[station].end(); ++jpad ) {
              if ( usedPad.count( *jpad ) ) continue;
              bool takeit      = false;
              int  deltaRegion = abs( (int)( ( *ipad )->tile().region() - ( *jpad )->tile().region() ) );
              if ( deltaRegion > 1 ) continue;
              if ( deltaRegion == 0 ) { // same region: use logical position

                takeit =
                    std::any_of( muon_cluster.pads().begin(), muon_cluster.pads().end(), [&]( const MuonPad* clpad ) {
                      if ( clpad->tile().region() != ( *jpad )->tile().region() ) return false;
                      int deltaX = std::abs( regX( clpad->tile() ) - regX( ( *jpad )->tile() ) );
                      int deltaY = std::abs( regY( clpad->tile() ) - regY( ( *jpad )->tile() ) );
                      return ( ( deltaX == 0 && deltaY == 1 ) || ( deltaX == 1 && deltaY == 0 ) );
                    } );
              } else { // adjacent regions: use absolute position
                // auto pos = m_posTool->calcTilePos( ( *jpad )->tile() );
                auto j_pos = det.position( ( *jpad )->tile() );
                if ( j_pos.has_value() ) {
                  bool Xinside = ( j_pos->x() > muon_cluster.minX() && j_pos->x() < muon_cluster.maxX() );
                  bool Xadj    = ( ( j_pos->x() > muon_cluster.maxX() &&
                                  j_pos->x() - j_pos->dX() - muon_cluster.maxX() < j_pos->dX() / 2 ) ||
                                ( j_pos->x() < muon_cluster.minX() &&
                                  muon_cluster.minX() - j_pos->x() - j_pos->dX() < j_pos->dX() / 2 ) );
                  bool Yinside = ( j_pos->y() > muon_cluster.minY() && j_pos->y() < muon_cluster.maxY() );
                  bool Yadj    = ( ( j_pos->y() > muon_cluster.maxY() &&
                                  j_pos->y() - j_pos->dY() - muon_cluster.maxY() < j_pos->dY() / 2 ) ||
                                ( j_pos->y() < muon_cluster.minY() &&
                                  muon_cluster.minY() - j_pos->y() - j_pos->dY() < j_pos->dY() / 2 ) );
                  takeit       = ( ( Xinside || Xadj ) && ( Yinside || Yadj ) );
                } else {
                  takeit = false;
                }
              }
              if ( takeit ) { // it's a neighbour, add it to the cluster
                usedPad[*jpad] = true;
                auto j_pos     = det.position( ( *jpad )->tile() );
                if ( j_pos ) {
                  muon_cluster.addPad( *jpad, j_pos.value() );
                  searchNeighbours = true; // we exit the loop and restart from ipad+1 ith the larger cluster
                  break;
                }
              }
            }
          } // end of neighbour search

        } // end of unused pad request
      }   // end of loop on log. pads of a given station
    }     // end of loop on stations
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "\n OBTAINED CLUSTERS:" << endmsg;
      if ( det.stations() == 5 ) {
        for ( const auto& c : clusters ) {
          debug() << "Cluster #" << c.hitID() << " in M" << ( c.station() + 1 ) << " with " << c.npads()
                  << " pads   X=" << c.minX() << " - " << c.maxX() << "  Y=" << c.minY() << " - " << c.maxY()
                  << " first tile has Nx/Ny=" << c.tile().nX() << "/" << c.tile().nY() << endmsg;
        }
      } else {
        for ( const auto& c : clusters ) {
          debug() << "Cluster #" << c.hitID() << " in M" << ( c.station() + 2 ) << " with " << c.npads()
                  << " pads   X=" << c.minX() << " - " << c.maxX() << "  Y=" << c.minY() << " - " << c.maxY()
                  << " first tile has Nx/Ny=" << c.tile().nX() << "/" << c.tile().nY() << endmsg;
        }
      }
    }
    // end of clustering algorithm
    return clusters;
  }
} // namespace LHCb::Muon::MuonTrackRec
