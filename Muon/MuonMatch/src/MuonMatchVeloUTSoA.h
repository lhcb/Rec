/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PrHits.h"
#include "Event/PrUpstreamTracks.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/SIMDWrapper.h"
#include "Magnet/DeMagnet.h"
#include "MuonDet/DeMuonDetector.h"
#include "Utils.h"
#include <algorithm>
#include <numeric>
#include <string>
#include <tuple>
#include <utility>

using window_bounds = std::tuple<float, float, float, float>;
using UTTracks      = LHCb::Pr::Upstream::Tracks;

class MuonMatchVeloUTSoA : public LHCb::Algorithm::Transformer<UTTracks( const EventContext&, const UTTracks&,
                                                                         const MuonHitContainer&, const DeMagnet& ),
                                                               LHCb::Algorithm::Traits::usesConditions<DeMagnet>> {

public:
  MuonMatchVeloUTSoA( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "InputTracks", "Rec/Track/UT" },
                       KeyValue{ "InputMuonHits", MuonHitContainerLocation::Default },
                       KeyValue{ "Magnet", LHCb::Det::Magnet::det_path } },
                     KeyValue{ "OutputTracks", LHCb::TrackLocation::Match } ) {}

  StatusCode initialize() override;

  UTTracks operator()( const EventContext& evtCtx, const UTTracks& seeds, const MuonHitContainer& hit_cont,
                       const DeMagnet& ) const override;

private:
  Gaudi::Property<bool>  m_setQOverP{ this, "SetQOverP",
                                     false }; // update q/p with the information from the muon chambers
  Gaudi::Property<float> m_maxChi2DoF{ this, "MaxChi2DoF", 60 };
  Gaudi::Property<bool>  m_fitY{ this, "FitY", false };

  /// Counter for the number of input muon hits
  mutable Gaudi::Accumulators::AveragingCounter<> m_hitCount{ this, "#hits" };
  /// Counter for the number of input seeds
  mutable Gaudi::Accumulators::AveragingCounter<> m_seedCount{ this, "#seeds" };
  /// Counter for the matched tracks
  mutable Gaudi::Accumulators::AveragingCounter<> m_matchCount{ this, "#matched" };

  // Parameters for calcutating the
  static constexpr float m_kickOffset = 338.92 * Gaudi::Units::MeV;
  static constexpr float m_kickScale  = 1218.62 * Gaudi::Units::MeV;

  // Parameters for calculating the magnet focal plane
  static constexpr float m_za = +5.331 * Gaudi::Units::m;
  static constexpr float m_zb = -0.958 * Gaudi::Units::m;

  // Size of the search windows for each muon station
  static constexpr std::array<std::pair<float, float>, sizeof( MuonChamber )> m_window = {
      std::pair<float, float>{ 500 * Gaudi::Units::mm, 400 * Gaudi::Units::mm },
      std::pair<float, float>{ 600 * Gaudi::Units::mm, 500 * Gaudi::Units::mm },
      std::pair<float, float>{ 700 * Gaudi::Units::mm, 600 * Gaudi::Units::mm },
      std::pair<float, float>{ 800 * Gaudi::Units::mm, 700 * Gaudi::Units::mm } };

  // Momentum bounds defining the track types
  static constexpr std::array<float, 3> m_momentumBounds = {
      2.5 * Gaudi::Units::GeV, 6 * Gaudi::Units::GeV, 10 * Gaudi::Units::GeV }; // 3, 6 and 10 GeV/c for offline muons

  bool matchByTrackType( State& state, const MuonHitContainer& hit_cont, LHCb::Allocators::MemoryResource* memResource,
                         const DeMagnet& magnet ) const;

  template <MuonChamber... Last>
  bool match( State& state, const MuonHitContainer& hit_cont, MuonChamber first, MuonChamberSeq<Last...>&& last,
              LHCb::Allocators::MemoryResource* memResource, const DeMagnet& magnet ) const;

  std::optional<Hit> findHit( MuonChamber much, const State& state, const Hit& magnet_hit,
                              const MuonHitContainer& hit_cont, const float slope ) const;

  TrackType getTrackTypeFromMomentum( float p ) const;

  Hit getMagnetFocalPlane( const State& state ) const;

  window_bounds getFirstStationWindow( const State& state, const CommonMuonStation& station, const Hit& hit,
                                       const DeMagnet& magnet ) const;

  window_bounds getNextStationWindow( const State& state, const CommonMuonStation& station, const Hit& magnet_hit,
                                      const float slope ) const;

  float getChangeInXSlope( const State& state ) const;

  float extrapolateYInStraightLine( const State& state, float z ) const;

  std::tuple<float, float> fitHits( const Hits& hits, const Hit& magnetHit, const State& state ) const;

  template <class Container>
  std::tuple<float, float, unsigned int> fitLinear( const Container& points ) const;

  template <class Container>
  float chi2( const Container& points ) const;

  float getUpdatedMomentum( float dtx ) const;
};
