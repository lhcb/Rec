/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonMatchVeloUTSoA.h"
#include <ranges>

DECLARE_COMPONENT( MuonMatchVeloUTSoA )

StatusCode MuonMatchVeloUTSoA::initialize() {
  auto sc = Transformer::initialize();
  if ( sc.isFailure() ) return sc;
  return StatusCode::SUCCESS;
}

UTTracks MuonMatchVeloUTSoA::operator()( const EventContext& evtCtx, const UTTracks& seeds,
                                         const MuonHitContainer& hit_cont, const DeMagnet& magnet ) const {
  m_seedCount += seeds.size();

  size_t nhits = 0;
  for ( unsigned int s = M2; s < sizeof( MuonChamber ); s++ ) {
    const auto& station = hit_cont.station( s );
    nhits += station.hits().size();
  }
  m_hitCount += nhits;

  auto memResource = LHCb::getMemResource( evtCtx );

  UTTracks matched{ seeds.getVeloAncestors(), Zipping::generateZipIdentifier(), memResource };
  matched.reserve( seeds.size() );
  const auto velozipped = seeds.getVeloAncestors()->scalar();
  for ( auto const& seed : seeds.scalar() ) {
    auto const t = seed.offset();
    State      state( seed, velozipped[seed.trackVP().cast()] );
    auto       mask = this->matchByTrackType( state, hit_cont, memResource, magnet );

    matched.copy_back<SIMDWrapper::scalar::types>( seeds, t, mask );

    if ( m_setQOverP && mask )
      matched.scalar()[matched.size() - 1].field<LHCb::Pr::Upstream::Tag::State>().setQOverP( state.qop );
  }

  m_matchCount += matched.size();

  return matched;
}

bool MuonMatchVeloUTSoA::matchByTrackType( State& state, const MuonHitContainer& hit_cont,
                                           LHCb::Allocators::MemoryResource* memResource,
                                           const DeMagnet&                   magnet ) const {
  // Define the track type, if its momentum is too low, stop processing
  const TrackType tt = getTrackTypeFromMomentum( state.p );
  switch ( tt ) {
  case ( LowP ):
    return match( state, hit_cont, M3, MuonChamberSeq<M2>{}, memResource, magnet );
  case ( MediumP ):
    return match( state, hit_cont, M4, MuonChamberSeq<M3, M2>{}, memResource, magnet );
  case ( HighP ):
    return match( state, hit_cont, M5, MuonChamberSeq<M4, M3, M2>{}, memResource, magnet );
  default: // VeryLowP
    return false;
  };
}

template <MuonChamber... Last>
bool MuonMatchVeloUTSoA::match( State& state, const MuonHitContainer& hit_cont, MuonChamber first,
                                MuonChamberSeq<Last...>&&, LHCb::Allocators::MemoryResource* memResource,
                                const DeMagnet& magnet ) const {
  std::optional<Candidate> cand;

  // look for hit in first muon chamber within the search window
  const auto               magnet_hit    = getMagnetFocalPlane( state );
  const CommonMuonStation& first_station = hit_cont.station( first );
  const auto [xMin, xMax, yMin, yMax]    = getFirstStationWindow( state, first_station, magnet_hit, magnet );

  for ( const auto& mhit : hit_cont.hits( first ) ) {
    if ( mhit.x() > xMax || mhit.x() < xMin || mhit.y() > yMax || mhit.y() < yMin ) continue;

    // look for hits in other station windows using the first hit as a reference point
    const Hit  first_hit{ mhit };
    const auto xSlope = ( first_hit.x - magnet_hit.x ) / ( first_hit.z - magnet_hit.z );

    Hits hits{ memResource };
    hits.reserve( 4 );
    hits.emplace_back( first_hit );

    auto check = [&]( auto much ) {
      auto h = findHit( much, state, magnet_hit, hit_cont, xSlope );
      if ( h ) {
        hits.emplace_back( std::move( *h ) );
        return true;
      }
      return false;
    };

    const auto hitsInAllStations = ( check( Last ) && ... );
    if ( !hitsInAllStations ) continue;

    const auto [fit_slope, chi2_ndof] = fitHits( hits, magnet_hit, state );

    if ( chi2_ndof < m_maxChi2DoF ) {
      if ( !cand ) {
        cand.emplace( fit_slope, chi2_ndof );
        if ( !m_setQOverP ) break;
      } else if ( chi2_ndof < cand->chi2ndof )
        cand.emplace( fit_slope, chi2_ndof );
    }
  }

  if ( cand && m_setQOverP ) {
    const auto down = magnet.isDown() ? -1 : +1;
    const auto dtx  = cand->slope - state.tx;
    const auto q    = down * ( ( dtx < 0 ) - ( dtx > 0 ) );
    const auto p    = getUpdatedMomentum( dtx );
    state.qop       = ( q / p );
  }

  return cand ? true : false;
}

std::optional<Hit> MuonMatchVeloUTSoA::findHit( MuonChamber much, const State& state, const Hit& magnet_hit,
                                                const MuonHitContainer& hit_cont, const float slope ) const {

  const auto& station = hit_cont.station( much );

  const auto [xMin, xMax, yMin, yMax] = getNextStationWindow( state, station, magnet_hit, slope );

  // Look for the closest hit inside the search window
  std::optional<Hit> closest;
  float              min_dist2 = 0;
  for ( const auto& mhit : hit_cont.hits( much ) ) {
    if ( mhit.x() - mhit.dx() > xMax || mhit.x() + mhit.dx() < xMin || mhit.y() - mhit.dy() > yMax ||
         mhit.y() + mhit.dy() < yMin )
      continue;

    const Hit  hit{ mhit };
    const auto y_station = extrapolateYInStraightLine( state, hit.z );
    const auto resx      = magnet_hit.x + ( hit.z - magnet_hit.z ) * slope - hit.x;
    const auto resy      = y_station - hit.y;
    const auto dist2     = ( resx * resx + resy * resy ) / ( hit.sx2 + hit.sy2 );

    if ( !closest || dist2 < min_dist2 ) {
      closest.emplace( hit );
      min_dist2 = dist2;
    }
  }
  return closest;
}

TrackType MuonMatchVeloUTSoA::getTrackTypeFromMomentum( float p ) const {

  if ( p < m_momentumBounds[0] )
    return VeryLowP;
  else if ( p < m_momentumBounds[1] )
    return LowP;
  else if ( p < m_momentumBounds[2] )
    return MediumP;
  else
    return HighP;
}

Hit MuonMatchVeloUTSoA::getMagnetFocalPlane( const State& state ) const {
  const auto z = m_za + m_zb * state.tx * state.tx;
  return Hit{ state, z };
}

window_bounds MuonMatchVeloUTSoA::getFirstStationWindow( const State& state, const CommonMuonStation& station,
                                                         const Hit& magnet_hit, const DeMagnet& magnet ) const {

  const auto& [xw, yw] = m_window[station.station()];

  // Calculate search window in y
  const auto y_station = extrapolateYInStraightLine( state, station.z() );
  const auto yMin      = y_station - yw;
  const auto yMax      = y_station + yw;

  // Calculate search window in x
  const auto dz = ( station.z() - magnet_hit.z );

  const auto mag    = magnet.isDown() ? -1 : +1;
  const auto charge = state.qop < 0 ? -1 : +1;

  const auto tx  = state.tx;
  const auto dtx = getChangeInXSlope( state ); // change in slope caused by the magnetic field

  const auto slope = charge * mag < 0 ? tx + dtx : tx - dtx; // slope after the magnet

  const auto xpos = magnet_hit.x + dz * slope;
  const auto xMin = xpos - xw;
  const auto xMax = xpos + xw;

  return { xMin, xMax, yMin, yMax };
}

window_bounds MuonMatchVeloUTSoA::getNextStationWindow( const State& state, const CommonMuonStation& station,
                                                        const Hit& magnet_hit, const float slope ) const {

  const auto [xw, yw] = m_window[station.station()];

  const auto y_station = extrapolateYInStraightLine( state, station.z() );

  const auto yMin = y_station - yw;
  const auto yMax = y_station + yw;

  const auto x_station = ( station.z() - magnet_hit.z ) * slope + magnet_hit.x;

  const auto xMin = x_station - xw;
  const auto xMax = x_station + xw;

  return { xMin, xMax, yMin, yMax };
}

float MuonMatchVeloUTSoA::getChangeInXSlope( const State& state ) const {
  return m_kickScale / ( state.p - m_kickOffset );
}

float MuonMatchVeloUTSoA::extrapolateYInStraightLine( const State& state, float z ) const {
  return state.y + ( z - state.z ) * state.ty;
}

std::tuple<float, float> MuonMatchVeloUTSoA::fitHits( const Hits& hits, const Hit& magnet_hit,
                                                      const State& state ) const {

  float x_slope, x_chi2, x_ndof;
  std::tie( x_slope, x_chi2, x_ndof ) = fitLinear( hits | std::ranges::views::transform( [&magnet_hit]( const Hit& h ) {
                                                     return std::make_tuple( h.z - magnet_hit.z, h.x, h.sx2 );
                                                   } ) );

  float y_chi2, y_ndof;
  std::tie( std::ignore, y_chi2, y_ndof ) =
      m_fitY ? fitLinear( hits | std::ranges::views::transform( [&magnet_hit]( const Hit& h ) {
                            return std::make_tuple( h.z - magnet_hit.z, h.y, h.sy2 );
                          } ) )
             : std::make_tuple( (float)state.ty, // no fit in y, just the chi2 between the slope before the magnet
                                                 // with the matched hits
                                chi2( hits | std::ranges::views::transform( [&state, this]( const Hit& h ) {
                                        return std::make_tuple( h.y, extrapolateYInStraightLine( state, h.z ), h.sy2 );
                                      } ) ),
                                (unsigned int)hits.size() );

  const auto chi2_ndof = ( x_chi2 + y_chi2 ) / ( x_ndof + y_ndof );

  return { x_slope, chi2_ndof };
}

template <class Container>
std::tuple<float, float, unsigned int> MuonMatchVeloUTSoA::fitLinear( const Container& points ) const {
  auto S = 0., Sz = 0., Sc = 0.;
  for ( const auto [z, c, s2] : points ) {
    const auto is2 = 1. / s2;
    S += is2;
    Sz += z * is2;
    Sc += c * is2;
  }
  const auto alpha = Sz / S;
  auto       b = 0., Stt = 0.;
  for ( const auto [z, c, s2] : points ) {
    const auto t_i = ( z - alpha );
    const auto ts2 = t_i / s2;
    Stt += t_i * ts2;
    b += c * ts2;
  }
  b /= Stt;
  const auto a = ( Sc - Sz * b ) / S;

  const auto chisq = chi2( points | std::ranges::views::transform( [&a, &b]( const auto& point ) {
                             const auto [z, c, s2] = point;
                             return std::make_tuple( c, a + b * z, s2 );
                           } ) );
  return std::make_tuple( b, chisq, points.size() - 2 );
}

template <class Container>
float MuonMatchVeloUTSoA::chi2( const Container& points ) const {
  return std::accumulate( std::begin( points ), std::end( points ), 0., []( const auto prev, const auto& point ) {
    const auto [ob, th, s2] = point;
    const auto d            = ob - th;
    return prev + d * d / s2;
  } );
}

float MuonMatchVeloUTSoA::getUpdatedMomentum( float dtx ) const {
  return m_kickScale / std::fabs( dtx ) + m_kickOffset;
}
