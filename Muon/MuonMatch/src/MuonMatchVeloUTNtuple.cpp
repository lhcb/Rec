/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RecVertex.h"
#include "Kernel/STLExtensions.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "Associators/InputLinks.h"
#include "Associators/Location.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/IntLink.h"
#include "Event/MCHit.h"
#include "Event/MCMuonDigit.h"
#include "Event/MCParticle.h"
#include "Event/MuonDigit.h"
#include "Event/ODIN.h"
#include "Event/PrHits.h"
#include "Event/RecVertex_v2.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/SIMDWrapper.h"
#include "MCInterfaces/IMCReconstructible.h"
#include "Magnet/DeMagnet.h"
#include "MuonDAQ/CommonMuonHit.h"

#include <GaudiAlg/GaudiTupleAlg.h>
#include <GaudiKernel/StdArrayAsProperty.h>
#include <LHCbAlgs/Consumer.h>

#include <iostream>
#include <ranges>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "TMatrixD.h"
#include "TVectorD.h"

using Track    = LHCb::Event::v2::Track;
using Tracks   = std::vector<Track>;
using Vertex   = LHCb::Event::v2::RecVertex;
using Vertices = LHCb::Event::v2::RecVertices;

namespace {
  constexpr float za = +5.331 * Gaudi::Units::m;
  constexpr float zb = -0.958 * Gaudi::Units::m;
} // namespace

class MuonMatchVeloUTNtuple final
    : public LHCb::Algorithm::Consumer<
          void( LHCb::ODIN const& odin, Vertices const& pvs, Tracks const& uttracks, Tracks const& mmtracks,
                MuonHitContainer const& hit_handler, LHCb::MCParticles const& mctracks, LHCb::MCHits const& mcmuonhits,
                LHCb::LinksByKey const& mcparticleLinks, LHCb::LinksByKey const& mcdigits, DeMagnet const&,
                DeMuonDetector const& ),
          LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeMagnet, DeMuonDetector>> {
public:
  MuonMatchVeloUTNtuple( const std::string& name, ISvcLocator* pSvcLocator );
  virtual void operator()( LHCb::ODIN const&, Vertices const& pvs, Tracks const& uttracks, Tracks const& mmtracks,
                           MuonHitContainer const&, LHCb::MCParticles const&, LHCb::MCHits const&,
                           LHCb::LinksByKey const&, LHCb::LinksByKey const&, DeMagnet const&,
                           DeMuonDetector const& ) const override;

private:
  Gaudi::Property<bool> m_onlyMCMuons{ this, "onlyMCMuons", false };

  std::optional<LHCb::MCParticle*> isDecayInFlightToMuon( const LHCb::MCParticle* mcparticle ) const;
  float                            impactParameter( const Track& track, const Vertices& vertex ) const;
};

DECLARE_COMPONENT( MuonMatchVeloUTNtuple )

MuonMatchVeloUTNtuple::MuonMatchVeloUTNtuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "OdinLocation", LHCb::ODINLocation::Default },
                  KeyValue{ "PrimaryVertecies", LHCb::Event::v2::RecVertexLocation::Velo3D },
                  KeyValue{ "VeloUTTracks", LHCb::TrackLocation::VeloUT },
                  KeyValue{ "MuonMatchTracks", LHCb::TrackLocation::Match },
                  KeyValue{ "MuonHits", MuonHitContainerLocation::Default },
                  KeyValue{ "MCParticles", LHCb::MCParticleLocation::Default },
                  KeyValue{ "MCMuonHits", "/Event/MC/Muon/Hits" }, KeyValue{ "VeloUTMCParticlesLinks", "" },
                  KeyValue{ "MCMuonDigitLinks", "/Event/Link/Raw/Muon/Digits" },
                  KeyValue{ "Magnet", LHCb::Det::Magnet::det_path },
                  KeyValue{ "DeMuonLocation", DeMuonLocation::Default } } ) {}

void MuonMatchVeloUTNtuple::operator()( LHCb::ODIN const& odin, Vertices const& pvs, Tracks const& uttracks,
                                        Tracks const& mmtracks, MuonHitContainer const& muonhits,
                                        LHCb::MCParticles const& mcparticles, LHCb::MCHits const& mcmuonhits,
                                        LHCb::LinksByKey const& mcparticleLinks, LHCb::LinksByKey const& mcmuonLinks,
                                        DeMagnet const& magnet, DeMuonDetector const& muonDetector ) const {
  std::string tuplename;
  if ( m_onlyMCMuons )
    tuplename = "VeloUTMCMuonTuple";
  else
    tuplename = "VeloUTTuple";

  Tuple tuple = nTuple( tuplename );

  unsigned int index{ 0 };
  // loop over all VeloUT tracks
  for ( const auto& track : uttracks ) {

    // look for associated MCParticle
    unsigned int key = index;
    index++;
    LHCb::MCParticle const* mcparticle{ nullptr };
    float                   max_weight{ 0 };
    unsigned int            n_mcparticles{ 0 };
    mcparticleLinks.applyToLinks( key, [&n_mcparticles, &max_weight, &mcparticle,
                                        &mcparticles]( unsigned int, unsigned int mcPartKey, float weight ) {
      n_mcparticles++;
      if ( weight > max_weight ) {
        max_weight = weight;
        mcparticle = static_cast<LHCb::MCParticle const*>( mcparticles.containedObject( mcPartKey ) );
      }
    } );

    // look if the VeloUT track has an associated MC Particle and if that MCParticle is a muon
    bool isMCMatched = false;
    bool isMCMuon    = false;
    if ( mcparticle != nullptr ) {
      isMCMatched = true;
      if ( abs( mcparticle->particleID().pid() ) == 13 ) isMCMuon = true;
    }

    // look for associated MuonMatched track
    const Track* mmtrack{ nullptr };
    for ( const auto& mt : mmtracks ) {
      if ( mt.containsLhcbIDs( track ) ) {
        mmtrack = &mt;
        break;
      }
    }

    // if only MCMuons are supposed to into the tuple and no MCMuon mcparticle is associated, jump to the next VeloUT
    // track
    if ( m_onlyMCMuons && !isMCMuon ) continue;

    tuple->column( "RunNumber", odin.runNumber() ).ignore();
    tuple->column( "EvtNumber", odin.eventNumber() ).ignore();
    tuple->column( "TrackNumber", index ).ignore();
    int mag = magnet.isDown() ? -1 : +1;
    tuple->column( "MagnetPolarity", mag ).ignore();

    tuple->column( "VeloUT_P", (float)track.p() ).ignore();
    tuple->column( "VeloUT_PT", (float)track.pt() ).ignore();
    auto utmomentum = track.momentum();
    tuple->column( "VeloUT_PX", (float)utmomentum.x() ).ignore();
    tuple->column( "VeloUT_PY", (float)utmomentum.y() ).ignore();
    tuple->column( "VeloUT_PZ", (float)utmomentum.z() ).ignore();
    tuple->column( "VeloUT_TrackCharge", track.charge() ).ignore();
    tuple->column( "VeloUT_minIP", impactParameter( track, pvs ) ).ignore();

    const auto& firstState = track.position();
    tuple->column( "VeloUT_FirstState_X", firstState.x() ).ignore();
    tuple->column( "VeloUT_FirstState_Y", firstState.y() ).ignore();
    tuple->column( "VeloUT_FirstState_Z", firstState.z() ).ignore();

    const LHCb::State* utstate    = track.stateAt( LHCb::State::Location::EndVelo );
    auto               utposition = utstate->position();
    tuple->column( "VeloUT_EndVelo_X", utposition.x() ).ignore();
    tuple->column( "VeloUT_EndVelo_Y", utposition.y() ).ignore();
    tuple->column( "VeloUT_EndVelo_Z", utposition.z() ).ignore();
    tuple->column( "VeloUT_EndVelo_tX", utstate->tx() ).ignore();
    tuple->column( "VeloUT_EndVelo_tY", utstate->ty() ).ignore();
    tuple->column( "VeloUT_EndVelo_etX", std::sqrt( utstate->errTx2() ) ).ignore();
    tuple->column( "VeloUT_EndVelo_etY", std::sqrt( utstate->errTy2() ) ).ignore();

    if ( isMCMatched ) {
      tuple->column( "MC_Found", 1 ).ignore();
      auto mcMomentum = mcparticle->momentum();
      tuple->column( "MC_PID", mcparticle->particleID().pid() ).ignore();
      tuple->column( "MC_PT", (float)mcparticle->pt() ).ignore();
      tuple->column( "MC_P", (float)mcparticle->p() ).ignore();
      tuple->column( "MC_PX", (float)mcMomentum.Px() ).ignore();
      tuple->column( "MC_PY", (float)mcMomentum.Py() ).ignore();
      tuple->column( "MC_PZ", (float)mcMomentum.Pz() ).ignore();
      tuple->column( "MC_E", (float)mcMomentum.E() ).ignore();

      float tx = (float)mcMomentum.Px() / (float)mcMomentum.Pz();
      tuple->column( "MC_tX", tx ).ignore();
      float ty = (float)mcMomentum.Py() / (float)mcMomentum.Pz();
      tuple->column( "MC_tY", ty ).ignore();

      const auto& mother = mcparticle->mother();
      if ( mother )
        tuple->column( "MC_MOTHER_PID", mother->particleID().pid() ).ignore();
      else
        tuple->column( "MC_MOTHER_PID", -999 ).ignore();

      const LHCb::MCVertex* originVertex = mcparticle->originVertex();
      Gaudi::XYZPoint       pos          = originVertex->position();
      tuple->column( "MC_OriginVertex_X", (float)pos.x() ).ignore();
      tuple->column( "MC_OriginVertex_Y", (float)pos.y() ).ignore();
      tuple->column( "MC_OriginVertex_Z", (float)pos.z() ).ignore();

      auto muonDaughter = isDecayInFlightToMuon( mcparticle );
      if ( muonDaughter ) {
        tuple->column( "MC_hasMuonDaughter", 1 ).ignore();
        const auto& MOVPos = muonDaughter.value()->originVertex()->position();
        tuple->column( "MC_MuonDaughter_OriginVertex_X", (float)MOVPos.X() ).ignore();
        tuple->column( "MC_MuonDaughter_OriginVertex_Y", (float)MOVPos.Y() ).ignore();
        tuple->column( "MC_MuonDaughter_OriginVertex_Z", (float)MOVPos.Z() ).ignore();
      } else {
        tuple->column( "MC_hasMuonDaughter", 0 ).ignore();
        tuple->column( "MC_MuonDaughter_OriginVertex_X", -999.f ).ignore();
        tuple->column( "MC_MuonDaughter_OriginVertex_Y", -999.f ).ignore();
        tuple->column( "MC_MuonDaughter_OriginVertex_Z", -999.f ).ignore();
      }

      // find the associated muon reco hits
      for ( int stat = 0; stat < 4; stat++ ) // loop over 4 muons stations
      {
        std::vector<CommonMuonHit> matchedCrossedHits = {};
        const auto                 mhits              = muonhits.hits( stat );
        for ( const auto& muonhit : mhits ) {
          int subtilesMatched = 0;
          for ( auto const& subtile : muonhit.subtiles() ) {
            unsigned int key = static_cast<unsigned long>( subtile );
            mcmuonLinks.applyToLinks( key,
                                      [&mcparticle, &subtilesMatched]( unsigned int, unsigned int mcPartKey, float ) {
                                        if ( (int)mcPartKey == mcparticle->key() ) { subtilesMatched++; }
                                      } );
          }
          if ( subtilesMatched == 2 ) { matchedCrossedHits.push_back( muonhit ); }
        }

        std::vector<CommonMuonHit> matchedHits = {};
        if ( matchedCrossedHits.size() == 0 ) {
          const auto mhits = muonhits.hits( stat );
          for ( const auto& muonhit : mhits ) {
            for ( auto const& subtile : muonhit.subtiles() ) {
              unsigned int key = static_cast<unsigned long>( subtile );
              mcmuonLinks.applyToLinks(
                  key, [&matchedHits, &mcparticle, &muonhit]( unsigned int, unsigned int mcPartKey, float ) {
                    if ( (int)mcPartKey == mcparticle->key() ) { matchedHits.push_back( muonhit ); }
                  } );
            }
          }
        }

        if ( matchedCrossedHits.size() > 0 ) {
          std::string branchname = "M" + std::to_string( stat + 2 ) + "_Hit";
          tuple->column( branchname.c_str(), (unsigned long long)matchedCrossedHits.size() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_IsCrossed";
          tuple->column( branchname.c_str(), 1 ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_X";
          tuple->column( branchname.c_str(), (float)matchedCrossedHits[0].x() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Y";
          tuple->column( branchname.c_str(), (float)matchedCrossedHits[0].y() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Z";
          tuple->column( branchname.c_str(), (float)matchedCrossedHits[0].z() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dX";
          tuple->column( branchname.c_str(), (float)matchedCrossedHits[0].dx() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dY";
          tuple->column( branchname.c_str(), (float)matchedCrossedHits[0].dy() ).ignore();
        } else if ( matchedHits.size() > 0 ) {
          std::string branchname = "M" + std::to_string( stat + 2 ) + "_Hit";
          tuple->column( branchname.c_str(), (unsigned long long)matchedHits.size() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_IsCrossed";
          tuple->column( branchname.c_str(), 0 ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_X";
          tuple->column( branchname.c_str(), (float)matchedHits[0].x() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Y";
          tuple->column( branchname.c_str(), (float)matchedHits[0].y() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Z";
          tuple->column( branchname.c_str(), (float)matchedHits[0].z() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dX";
          tuple->column( branchname.c_str(), (float)matchedHits[0].dx() ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dY";
          tuple->column( branchname.c_str(), (float)matchedHits[0].dy() ).ignore();
        } else {
          std::string branchname = "M" + std::to_string( stat + 2 ) + "_Hit";
          tuple->column( branchname.c_str(), (unsigned long long)0 ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_IsCrossed";
          tuple->column( branchname.c_str(), 0 ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_X";
          tuple->column( branchname.c_str(), -999.f ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Y";
          tuple->column( branchname.c_str(), -999.f ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Z";
          tuple->column( branchname.c_str(), -999.f ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dX";
          tuple->column( branchname.c_str(), -999.f ).ignore();
          branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dY";
          tuple->column( branchname.c_str(), -999.f ).ignore();
        }
      }

      // add MC muon hits
      std::optional<LHCb::MCHit*> MCM2Hit;
      std::optional<LHCb::MCHit*> MCM3Hit;
      std::optional<LHCb::MCHit*> MCM4Hit;
      std::optional<LHCb::MCHit*> MCM5Hit;
      for ( const auto mcmhit : mcmuonhits ) {
        const LHCb::MCParticle* hitparticle = mcmhit->mcParticle();
        if ( hitparticle->key() == mcparticle->key() ) {
          auto midPoint = mcmhit->midPoint();
          if ( fabs( midPoint.Z() - 15260. ) < 160. )
            MCM2Hit.emplace( mcmhit );
          else if ( fabs( midPoint.Z() - 16470. ) < 160. )
            MCM3Hit.emplace( mcmhit );
          else if ( fabs( midPoint.Z() - 17660. ) < 160. )
            MCM4Hit.emplace( mcmhit );
          else if ( fabs( midPoint.Z() - 18870. ) < 160. )
            MCM5Hit.emplace( mcmhit );
        }
      }
      if ( MCM2Hit ) {
        tuple->column( "MC_M2_Hit", 1 ).ignore();
        tuple->column( "MC_M2_Hit_X", (float)MCM2Hit.value()->midPoint().X() ).ignore();
        tuple->column( "MC_M2_Hit_Y", (float)MCM2Hit.value()->midPoint().Y() ).ignore();
        tuple->column( "MC_M2_Hit_Z", (float)MCM2Hit.value()->midPoint().Z() ).ignore();
        tuple->column( "MC_M2_Hit_Time", (float)MCM2Hit.value()->time() ).ignore();
        const auto region = muonDetector.Hit2ChamberRegionNumber( MCM2Hit.value()->midPoint() );
        tuple->column( "MC_M2_Hit_Region", region ).ignore();
        tuple->column( "MC_M2_Hit_PadSizeX", muonDetector.getPadSizeX( 0, region ) ).ignore();
        tuple->column( "MC_M2_Hit_PadSizeY", muonDetector.getPadSizeY( 0, region ) ).ignore();
      } else {
        tuple->column( "MC_M2_Hit", 0 ).ignore();
        tuple->column( "MC_M2_Hit_X", -999.f ).ignore();
        tuple->column( "MC_M2_Hit_Y", -999.f ).ignore();
        tuple->column( "MC_M2_Hit_Z", -999.f ).ignore();
        tuple->column( "MC_M2_Hit_Time", -999.f ).ignore();
        tuple->column( "MC_M2_Hit_Region", -9 ).ignore();
        tuple->column( "MC_M2_Hit_PadSizeX", -999.f ).ignore();
        tuple->column( "MC_M2_Hit_PadSizeY", -999.f ).ignore();
      }
      if ( MCM3Hit ) {
        tuple->column( "MC_M3_Hit", 1 ).ignore();
        tuple->column( "MC_M3_Hit_X", (float)MCM3Hit.value()->midPoint().X() ).ignore();
        tuple->column( "MC_M3_Hit_Y", (float)MCM3Hit.value()->midPoint().Y() ).ignore();
        tuple->column( "MC_M3_Hit_Z", (float)MCM3Hit.value()->midPoint().Z() ).ignore();
        tuple->column( "MC_M3_Hit_Time", (float)MCM3Hit.value()->time() ).ignore();
        const auto region = muonDetector.Hit2ChamberRegionNumber( MCM3Hit.value()->midPoint() );
        tuple->column( "MC_M3_Hit_Region", region ).ignore();
        tuple->column( "MC_M3_Hit_PadSizeX", muonDetector.getPadSizeX( 1, region ) ).ignore();
        tuple->column( "MC_M3_Hit_PadSizeY", muonDetector.getPadSizeY( 1, region ) ).ignore();
      } else {
        tuple->column( "MC_M3_Hit", 0 ).ignore();
        tuple->column( "MC_M3_Hit_X", -999.f ).ignore();
        tuple->column( "MC_M3_Hit_Y", -999.f ).ignore();
        tuple->column( "MC_M3_Hit_Z", -999.f ).ignore();
        tuple->column( "MC_M3_Hit_Time", -999.f ).ignore();
        tuple->column( "MC_M3_Hit_Region", -9 ).ignore();
        tuple->column( "MC_M3_Hit_PadSizeX", -999.f ).ignore();
        tuple->column( "MC_M3_Hit_PadSizeY", -999.f ).ignore();
      }
      if ( MCM4Hit ) {
        tuple->column( "MC_M4_Hit", 1 ).ignore();
        tuple->column( "MC_M4_Hit_X", (float)MCM4Hit.value()->midPoint().X() ).ignore();
        tuple->column( "MC_M4_Hit_Y", (float)MCM4Hit.value()->midPoint().Y() ).ignore();
        tuple->column( "MC_M4_Hit_Z", (float)MCM4Hit.value()->midPoint().Z() ).ignore();
        tuple->column( "MC_M4_Hit_Time", (float)MCM4Hit.value()->time() ).ignore();
        const auto region = muonDetector.Hit2ChamberRegionNumber( MCM4Hit.value()->midPoint() );
        tuple->column( "MC_M4_Hit_Region", region ).ignore();
        tuple->column( "MC_M4_Hit_PadSizeX", muonDetector.getPadSizeX( 2, region ) ).ignore();
        tuple->column( "MC_M4_Hit_PadSizeY", muonDetector.getPadSizeY( 2, region ) ).ignore();
      } else {
        tuple->column( "MC_M4_Hit", 0 ).ignore();
        tuple->column( "MC_M4_Hit_X", -999.f ).ignore();
        tuple->column( "MC_M4_Hit_Y", -999.f ).ignore();
        tuple->column( "MC_M4_Hit_Z", -999.f ).ignore();
        tuple->column( "MC_M4_Hit_Time", -999.f ).ignore();
        tuple->column( "MC_M4_Hit_Region", -9 ).ignore();
        tuple->column( "MC_M4_Hit_PadSizeX", -999.f ).ignore();
        tuple->column( "MC_M4_Hit_PadSizeY", -999.f ).ignore();
      }
      if ( MCM5Hit ) {
        tuple->column( "MC_M5_Hit", 1 ).ignore();
        tuple->column( "MC_M5_Hit_X", (float)MCM5Hit.value()->midPoint().X() ).ignore();
        tuple->column( "MC_M5_Hit_Y", (float)MCM5Hit.value()->midPoint().Y() ).ignore();
        tuple->column( "MC_M5_Hit_Z", (float)MCM5Hit.value()->midPoint().Z() ).ignore();
        tuple->column( "MC_M5_Hit_Time", (float)MCM5Hit.value()->time() ).ignore();
        const auto region = muonDetector.Hit2ChamberRegionNumber( MCM5Hit.value()->midPoint() );
        tuple->column( "MC_M5_Hit_Region", region ).ignore();
        tuple->column( "MC_M5_Hit_PadSizeX", muonDetector.getPadSizeX( 3, region ) ).ignore();
        tuple->column( "MC_M5_Hit_PadSizeY", muonDetector.getPadSizeY( 3, region ) ).ignore();
      } else {
        tuple->column( "MC_M5_Hit", 0 ).ignore();
        tuple->column( "MC_M5_Hit_X", -999.f ).ignore();
        tuple->column( "MC_M5_Hit_Y", -999.f ).ignore();
        tuple->column( "MC_M5_Hit_Z", -999.f ).ignore();
        tuple->column( "MC_M5_Hit_Time", -999.f ).ignore();
        tuple->column( "MC_M5_Hit_Region", -9 ).ignore();
        tuple->column( "MC_M5_Hit_PadSizeX", -999.f ).ignore();
        tuple->column( "MC_M5_Hit_PadSizeY", -999.f ).ignore();
      }
    } else {
      tuple->column( "MC_Found", 0 ).ignore();
      tuple->column( "MC_PID", -999 ).ignore();
      tuple->column( "MC_PT", -999.f ).ignore();
      tuple->column( "MC_P", -999.f ).ignore();
      tuple->column( "MC_PX", -999.f ).ignore();
      tuple->column( "MC_PY", -999.f ).ignore();
      tuple->column( "MC_PZ", -999.f ).ignore();
      tuple->column( "MC_E", -999.f ).ignore();
      tuple->column( "MC_tX", -999.f ).ignore();
      tuple->column( "MC_tY", -999.f ).ignore();
      tuple->column( "MC_MOTHER_PID", -999 ).ignore();
      tuple->column( "MC_hasMuonDaughter", 0 ).ignore();
      tuple->column( "MC_MuonDaughter_OriginVertex_X", -999.f ).ignore();
      tuple->column( "MC_MuonDaughter_OriginVertex_Y", -999.f ).ignore();
      tuple->column( "MC_MuonDaughter_OriginVertex_Z", -999.f ).ignore();
      tuple->column( "MC_OriginVertex_X", -999.f ).ignore();
      tuple->column( "MC_OriginVertex_Y", -999.f ).ignore();
      tuple->column( "MC_OriginVertex_Z", -999.f ).ignore();

      for ( unsigned int stat = 0; stat < 4; stat++ ) {
        std::string branchname = "M" + std::to_string( stat + 2 ) + "_Hit";
        tuple->column( branchname.c_str(), (unsigned long long)0 ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_IsCrossed";
        tuple->column( branchname.c_str(), 0 ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_X";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Y";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_Z";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dX";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "M" + std::to_string( stat + 2 ) + "_Hit_dY";
        tuple->column( branchname.c_str(), -999.f ).ignore();

        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit";
        tuple->column( branchname.c_str(), 0 ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_X";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_Y";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_Z";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_Time";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_Region";
        tuple->column( branchname.c_str(), -9 ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_PadSizeX";
        tuple->column( branchname.c_str(), -999.f ).ignore();
        branchname = "MC_M" + std::to_string( stat + 2 ) + "_Hit_PadSizeY";
        tuple->column( branchname.c_str(), -999.f ).ignore();
      }
    }

    if ( mmtrack ) {
      tuple->column( "MuonMatched", 1 ).ignore();
      tuple->column( "Muon_TrackCharge", mmtrack->charge() ).ignore();
      tuple->column( "Muon_P", (float)mmtrack->p() ).ignore();
      tuple->column( "Muon_PT", (float)mmtrack->pt() ).ignore();
      auto mmomentum = mmtrack->momentum();
      tuple->column( "Muon_PX", (float)mmomentum.x() ).ignore();
      tuple->column( "Muon_PY", (float)mmomentum.y() ).ignore();
      tuple->column( "Muon_PZ", (float)mmomentum.z() ).ignore();
      float magnet_z = (float)( za + zb * utstate->tx() * utstate->tx() );
      float magnet_x = (float)( ( magnet_z - utstate->z() ) * utstate->tx() + utstate->x() );
      tuple->column( "MagnetFocal_X", magnet_x ).ignore();
      tuple->column( "MagnetFocal_Z", magnet_z ).ignore();
    } else {
      tuple->column( "MuonMatched", 0 ).ignore();
      tuple->column( "Muon_TrackCharge", -999 ).ignore();
      tuple->column( "Muon_P", -999.f ).ignore();
      tuple->column( "Muon_PT", -999.f ).ignore();
      tuple->column( "Muon_PX", -999.f ).ignore();
      tuple->column( "Muon_PY", -999.f ).ignore();
      tuple->column( "Muon_PZ", -999.f ).ignore();
      tuple->column( "MagnetFocal_X", -999.f ).ignore();
      tuple->column( "MagnetFocal_Z", -999.f ).ignore();
    }
    tuple->write().ignore();
    // index++;
  }
}

std::optional<LHCb::MCParticle*>
MuonMatchVeloUTNtuple::isDecayInFlightToMuon( const LHCb::MCParticle* mcparticle ) const {
  std::optional<LHCb::MCParticle*> muonFound;

  auto mcendverticies = mcparticle->endVertices();
  for ( const auto& endvert : mcendverticies ) {
    auto daughters = endvert->products();
    for ( auto& d : daughters ) {
      if ( abs( d->particleID().pid() ) == 13 ) {
        muonFound.emplace( d );
        return muonFound;
      } else {
        isDecayInFlightToMuon( d );
      }
    }
  }
  return muonFound;
}

float MuonMatchVeloUTNtuple::impactParameter( const Track& track, const Vertices& pvs ) const {
  if ( !pvs.size() ) return -999.;

  const auto ips2 = std::ranges::views::transform( pvs, [&track]( const Vertex& pv ) {
    const auto& pos   = pv.position();
    const auto& state = track.closestState( pos.Z() );
    const auto  line  = Gaudi::Math::Line{ state.position(), state.slopes() };
    const auto  ipvec = Gaudi::Math::closestPoint( pos, line ) - pos;
    return ipvec.Mag2();
  } );

  return std::sqrt( *std::min_element( ips2.begin(), ips2.end() ) );
}
