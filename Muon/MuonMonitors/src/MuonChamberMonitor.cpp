/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/DetectorElement.h"
#include "Event/State.h"
#include "Event/Track_v3.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "LHCbMath/FastMaths.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"
#include "vdt/vdtMath.h"
#include <Event/Particle.h>
#include <Event/PrHits.h>
#include <Event/ProtoParticle.h>
#include <GaudiKernel/IFileAccess.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiUtils/HistoLabels.h>
#include <LHCbAlgs/Consumer.h>
#include <MuonDAQ/CommonMuonHit.h>
#include <MuonDet/DeMuonDetector.h>
#include <MuonDet/MuonNamespace.h>
#include <cmath>
#include <iostream>
#include <string>

#include "LHCbMath/Utils.h"

/** @class MuonChamberMonitor MuonChamberMonitor.cpp
 *
 *
 *  @author Lorenzo Paolucci, Roman Litvinov
 *  @date   2023-09-04
 */

namespace {

  constexpr size_t nMuonStations = 4;
  constexpr size_t nRegions      = 4;

  constexpr auto c_regionNch    = std::array{ 12, 24, 48, 192 };
  constexpr auto c_regionNchRow = std::array{ 4, 4, 4, 8 };
  constexpr auto c_regionNchCol = std::array{ 4, 8, 16, 32 };

  using xy_t = std::pair<double, double>;

  using SigmaPadCache = std::array<std::array<double, nRegions>, nMuonStations>;
  using ArrayPairSt   = std::array<std::pair<double, double>, nMuonStations>;

  enum ArrayType { Quadrant, Station, Region, StationRegion };

  std::vector<std::string> HistNameWriter( ArrayType type ) {
    std::vector<std::string> labels;

    switch ( type ) {
    case ArrayType::Quadrant:
      for ( unsigned int q = 0; q < 4; q++ ) labels.push_back( fmt::format( "Q{}", q + 1 ) );
      break;
    case ArrayType::Station:
      for ( unsigned int s = 0; s < 4; s++ ) labels.push_back( fmt::format( "M{}", s + 2 ) );
      break;
    case ArrayType::Region:
      for ( unsigned int r = 0; r < 4; r++ ) labels.push_back( fmt::format( "R{}", r + 1 ) );
      break;
    case ArrayType::StationRegion:
      for ( unsigned int s = 0; s < 4; s++ ) {
        for ( unsigned int r = 0; r < 4; r++ ) { labels.push_back( fmt::format( "M{}R{}", s + 2, r + 1 ) ); }
      }
      break;
    }

    return labels;
  }

  unsigned int GetKeyHist( ArrayType type, unsigned int stat = 0, unsigned int reg = 0, unsigned int quad = 0 ) {
    switch ( type ) {
    case ArrayType::Quadrant:
      return quad;
    case ArrayType::Station:
      return stat;
    case ArrayType::Region:
      return reg;
    case ArrayType::StationRegion:
      return stat * 4 + reg;
    }
    return 0;
  }

  struct GeomCache {

    std::array<double, nMuonStations> m_stationOuterX; // Outer abs(x) edge of station
    std::array<double, nMuonStations> m_stationOuterY;
    std::array<double, nMuonStations> m_stationInnerX; // Inner abs(x) edge of station
    std::array<double, nMuonStations> m_stationInnerY;
    std::array<double, nMuonStations> m_stationZ{ {} };
    SigmaPadCache                     m_sigmapadX, m_sigmapadY;

    std::array<std::array<double, nRegions>, nMuonStations> m_regionOuterX; // Outer abs(x) edge of station and region
    std::array<std::array<double, nRegions>, nMuonStations> m_regionOuterY;

    std::pair<std::array<std::tuple<unsigned int, double, double>, 16>,
              std::array<std::tuple<unsigned int, double, double>, 16>>
        m_chambersInRegion;

    GeomCache( const DeMuonDetector& det ) {
      for ( unsigned int s = 0; s < nMuonStations; s++ ) {
        m_stationOuterX[s] = det.getOuterX( s );
        m_stationOuterY[s] = det.getOuterY( s );
        m_stationInnerX[s] = det.getInnerX( s );
        m_stationInnerY[s] = det.getInnerY( s );
        m_stationZ[s]      = det.getStationZ( s );

        for ( unsigned int r = 0; r < nRegions; r++ ) {
          for ( int r = 0; r != det.regions() / det.stations(); ++r ) {
            m_sigmapadX[s][r] = det.getPadSizeX( s, r ) / std::sqrt( 12.0 );
            m_sigmapadY[s][r] = det.getPadSizeY( s, r ) / std::sqrt( 12.0 );
          }
          m_regionOuterX[s][r] = m_stationOuterX[s] / pow( 2., 3 - r );
          m_regionOuterY[s][r] = m_stationOuterY[s] / pow( 2., 3 - r );
          m_chambersInRegion.first[GetKeyHist( ArrayType::StationRegion, s, r )] = {
              c_regionNchRow[r], -m_stationOuterX[s] / pow( 2., 3 - r ), m_stationOuterX[s] / pow( 2., 3 - r ) };
          m_chambersInRegion.second[GetKeyHist( ArrayType::StationRegion, s, r )] = {
              c_regionNchCol[r], -m_stationOuterY[s] / pow( 2., 3 - r ), m_stationOuterY[s] / pow( 2., 3 - r ) };
        }
      }
    }
  };

} // namespace

namespace LHCb {

  class MuonChamberMonitor final
      : public LHCb::Algorithm::Consumer<void( const DetectorElement& lhcb, const GeomCache& geometryinfo,
                                               const LHCb::Particle::Range& probes, const LHCb::Particle::Range& tags,
                                               const MuonHitContainer& muonhits ),
                                         LHCb::Algorithm::Traits::usesConditions<DetectorElement, GeomCache>> {

  public:
    MuonChamberMonitor( const std::string& name, ISvcLocator* pSvLocator );

    StatusCode initialize() override;
    void operator()( const DetectorElement& lhcb, const GeomCache& geometryinfo, const LHCb::Particle::Range& probes,
                     const LHCb::Particle::Range& tags, const MuonHitContainer& muonhits ) const override;

  private:
    void fillHistograms( const DetectorElement& lhcb, const GeomCache& geometryinfo,
                         const LHCb::Particle::Range& probes, const LHCb::Particle::Range& tags,
                         const MuonHitContainer& muonhits ) const;

    // The track extrapolator
    ToolHandle<ITrackExtrapolator> m_extrapolator = { this, "ReferenceExtrapolator", "TrackMasterExtrapolator" };

    std::tuple<CommonMuonHit, double> computeChi2( const GeomCache& geometryinfo, const xy_t& ext, const xy_t& extMS,
                                                   const CommonMuonHitRange& hitContainer ) const;

    int HitsInChi2( const GeomCache& geometryinfo, const xy_t& ext, const xy_t& extMS,
                    const CommonMuonHitRange& hitContainer, int sign ) const;

    int xy2Chamber( const GeomCache& geometryinfo, const double x, const double y, unsigned int station,
                    unsigned int region ) const;

    Gaudi::Property<double> m_chi2cut{ this, "chi2cut", 25.0 };
    double                  m_chi2bad = 9999.;

    std::once_flag m_alreadyInitialized;

    // Muon Candidate
    class MuonCandidate final {
    public:
      MuonCandidate() = default;

      const LHCb::ProtoParticle* m_proto = nullptr;
      const LHCb::Track*         m_track = nullptr;
      const LHCb::State*         m_state = nullptr;

      double                              m_probeP{ 0. };
      std::array<double, nMuonStations>   m_probeChi2St{ { 0., 0., 0., 0. } };
      ArrayPairSt                         m_trackExt{ { { 0., 0. }, { 0., 0. }, { 0., 0. }, { 0., 0. } } };
      ArrayPairSt                         m_trackMS{ { { 0., 0. }, { 0., 0. }, { 0., 0. }, { 0., 0. } } };
      ArrayPairSt                         m_chit_XY{ { { 0., 0. }, { 0., 0. }, { 0., 0. }, { 0., 0. } } };
      std::array<unsigned, nMuonStations> m_extrapReg{ { 0, 0, 0, 0 } };
      std::array<unsigned, nMuonStations> m_chitReg{ { 0, 0, 0, 0 } };
      std::array<unsigned, nMuonStations> m_idReg{ { 0, 0, 0, 0 } };
    };

    // Histograms
    struct Histograms {
      // Activity histograms
      mutable Gaudi::Accumulators::Histogram<1> m_tagsP;
      mutable Gaudi::Accumulators::Histogram<1> m_tagsPT;
      mutable Gaudi::Accumulators::Histogram<1> m_tagPsRapidity;
      mutable Gaudi::Accumulators::Histogram<1> m_ntags;

      mutable Gaudi::Accumulators::Histogram<1> m_probesP;
      mutable Gaudi::Accumulators::Histogram<1> m_probesPT;
      mutable Gaudi::Accumulators::Histogram<1> m_probePsRapidity;
      mutable Gaudi::Accumulators::Histogram<2> m_probeExtrap;

      mutable Gaudi::Accumulators::Histogram<1> m_denStation;
      mutable Gaudi::Accumulators::Histogram<1> m_numStation;
      mutable Gaudi::Accumulators::Histogram<1> m_denStationRegion;
      mutable Gaudi::Accumulators::Histogram<1> m_numStationRegion;

      mutable Gaudi::Accumulators::Histogram<2>                m_errXY2;
      mutable std::array<Gaudi::Accumulators::Histogram<1>, 4> m_Chi2Station;

      mutable Gaudi::Accumulators::Histogram<2> m_HitsInExtrap;
      mutable Gaudi::Accumulators::Histogram<2> m_HitsInBkg;
      mutable Gaudi::Accumulators::Histogram<2> m_HitsInExtrapSub;
      mutable Gaudi::Accumulators::Histogram<2> m_edgeEffect;

      mutable std::array<Gaudi::Accumulators::Histogram<1>, 16> m_denChamb;
      mutable std::array<Gaudi::Accumulators::Histogram<1>, 16> m_numChamb;
      mutable std::array<Gaudi::Accumulators::Histogram<2>, 16> m_denChamb2D;
      mutable std::array<Gaudi::Accumulators::Histogram<2>, 16> m_numChamb2D;

      // Builder for array of 1D histos with different ranges
      template <std::size_t... IDXs>
      static std::array<Gaudi::Accumulators::Histogram<1>, sizeof...( IDXs )>
      histoArrayBuilder( const MuonChamberMonitor* owner, ArrayType type, const std::string& name,
                         const std::string& title, std::tuple<unsigned, double, double> xbins,
                         std::index_sequence<IDXs...> ) {
        std::vector<std::string> labels = HistNameWriter( type );
        return { { { owner,
                     name + labels[IDXs],
                     title + labels[IDXs],
                     { std::get<0>( xbins ), std::get<1>( xbins ), std::get<2>( xbins ) } }... } };
      }

      template <std::size_t... IDXs>
      static std::array<Gaudi::Accumulators::Histogram<1>, sizeof...( IDXs )> histoArrayBuilder(
          const MuonChamberMonitor* owner, ArrayType type, const std::string& name, const std::string& title,
          std::array<std::tuple<unsigned, double, double>, sizeof...( IDXs )> xbins, std::index_sequence<IDXs...> ) {
        std::vector<std::string> labels = HistNameWriter( type );
        return { { { owner,
                     name + labels[IDXs],
                     title + labels[IDXs],
                     { std::get<0>( xbins[IDXs] ), std::get<1>( xbins[IDXs] ), std::get<2>( xbins[IDXs] ) } }... } };
      }

      // Builder for array of 2D histos with different ranges
      template <std::size_t... IDXs>
      static std::array<Gaudi::Accumulators::Histogram<2>, sizeof...( IDXs )> histoArrayBuilder(
          const MuonChamberMonitor* owner, ArrayType type, const std::string& name, const std::string& title,
          std::array<std::tuple<unsigned, double, double>, sizeof...( IDXs )> xbins,
          std::array<std::tuple<unsigned, double, double>, sizeof...( IDXs )> ybins, std::index_sequence<IDXs...> ) {
        std::vector<std::string> labels = HistNameWriter( type );
        return { { { owner,
                     name + labels[IDXs],
                     title + labels[IDXs],
                     { std::get<0>( xbins[IDXs] ), std::get<1>( xbins[IDXs] ), std::get<2>( xbins[IDXs] ) },
                     { std::get<0>( ybins[IDXs] ), std::get<1>( ybins[IDXs] ), std::get<2>( ybins[IDXs] ) } }... } };
      }

      // Builder for array of 2D histos with fixed ranges
      template <std::size_t... IDXs>
      static std::array<Gaudi::Accumulators::Histogram<2>, sizeof...( IDXs )>
      histoArrayBuilder( const MuonChamberMonitor* owner, ArrayType type, const std::string& name,
                         const std::string& title, std::tuple<unsigned, double, double> xbins,
                         std::tuple<unsigned, double, double> ybins, std::index_sequence<IDXs...> ) {
        std::vector<std::string> labels = HistNameWriter( type );
        return { { { owner,
                     name + labels[IDXs],
                     title + labels[IDXs],
                     { std::get<0>( xbins ), std::get<1>( xbins ), std::get<2>( xbins ) },
                     { std::get<0>( ybins ), std::get<1>( ybins ), std::get<2>( ybins ) } }... } };
      }

      // Builder for array of 2D histos with known labels and fixed range
      template <std::size_t... IDXs>
      static std::array<Gaudi::Accumulators::Histogram<2>, sizeof...( IDXs )>
      histoArrayBuilder( const MuonChamberMonitor* owner, const std::string& name, const std::string& title,
                         std::vector<std::string> labels, std::tuple<unsigned, double, double> xbins,
                         std::tuple<unsigned, double, double> ybins, std::index_sequence<IDXs...> ) {
        return { { { owner,
                     name + labels[IDXs],
                     title + labels[IDXs],
                     { std::get<0>( xbins ), std::get<1>( xbins ), std::get<2>( xbins ) },
                     { std::get<0>( ybins ), std::get<1>( ybins ), std::get<2>( ybins ) } }... } };
      }

      Histograms( const MuonChamberMonitor* owner, const GeomCache& geometryinfo )
          : m_tagsP{ owner, "tags_P", "Momentum of tag particles", { 1000, 0.0, 100000 } }
          , m_tagsPT{ owner, "tags_PT", "Trnsverse momentum of tag particles", { 100, 0.5, 10000 } }
          , m_tagPsRapidity{ owner, "tagPsRapidity", "Pseudo rapidity of tag particles", { 30, 0, 6.0 } }
          , m_ntags{ owner, "ntags", "N of tags in event", { 20, 0, 20.0 } }

          , m_probesP{ owner, "probes_P", "Momentum of probe particles", { 1000, 0.0, 100000 } }
          , m_probesPT{ owner, "probes_PT", "Trnsverse momentum of probe particles", { 100, 0.5, 10000 } }
          , m_probePsRapidity{ owner, "probePsRapidity", "Pseudo rapidity of probe particles", { 30, 0, 6.0 } }
          , m_probeExtrap{ owner,
                           "probeExtrap",
                           "Extrapolation plot",
                           { 500, -8000.0, 8000.0 },
                           { 500, -8000.0, 8000.0 } }

          , m_denStation{ owner,
                          "denStation",
                          "Denominator probes in 4 stations",
                          { 4, -0.5, 3.5, "", HistNameWriter( ArrayType::Station ) } }
          , m_numStation{ owner,
                          "numStation",
                          "Numerator probes in 4 stations",
                          { 4, -0.5, 3.5, "", HistNameWriter( ArrayType::Station ) } }
          , m_denStationRegion{ owner,
                                "denStationRegion",
                                "Denominator probes in 16 regions",
                                { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                                  HistNameWriter( ArrayType::StationRegion ) } }
          , m_numStationRegion{ owner,
                                "numStationRegion",
                                "Numerator probes in 16 regions",
                                { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                                  HistNameWriter( ArrayType::StationRegion ) } }
          , m_errXY2{ owner, "errXY2", "MS error XY2", { 300, 0.0, 30.0 }, { 300, 0.0, 30.0 } }
          , m_Chi2Station{ histoArrayBuilder( owner, ArrayType::Station, "chi2_", "Chi2 of closest hit in station_",
                                              { 500, 0., 1000. }, std::make_index_sequence<4>() ) }

          , m_HitsInExtrap{ owner,
                            "HitsInExtrap",
                            "All hits in chi2 area",
                            { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                              HistNameWriter( ArrayType::StationRegion ) },
                            { 20, 0.0, 20.0 } }
          , m_HitsInBkg{ owner,
                         "HitsInBkg",
                         "Background hits estimate",
                         { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                           HistNameWriter( ArrayType::StationRegion ) },
                         { 20, 0.0, 20.0 } }
          , m_HitsInExtrapSub{ owner,
                               "HitsInExtrapSub",
                               "Hits in chi2 area - bkg",
                               { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                                 HistNameWriter( ArrayType::StationRegion ) },
                               { 40, -20.0, 20.0 } }
          , m_edgeEffect{ owner,
                          "edgeEffect",
                          "Hit and extrapolation chamber matched",
                          { nRegions * nMuonStations, -0.5, nRegions * nMuonStations - 0.5, "",
                            HistNameWriter( ArrayType::StationRegion ) },
                          { 4, -1.0, 3.0 } }

          , m_denChamb{ histoArrayBuilder( owner, ArrayType::StationRegion, "denChamb_", "Denominator chambers_",
                                           { 200, 0.0, 200.0 }, std::make_index_sequence<16>() ) }
          , m_numChamb{ histoArrayBuilder( owner, ArrayType::StationRegion, "numChamb_", "Numerator chambers_",
                                           { 200, 0.0, 200.0 }, std::make_index_sequence<16>() ) }
          , m_denChamb2D{ histoArrayBuilder( owner, ArrayType::StationRegion, "denChamb2D_", "Denominator chambers 2D_",
                                             geometryinfo.m_chambersInRegion.first,
                                             geometryinfo.m_chambersInRegion.second, std::make_index_sequence<16>() ) }
          , m_numChamb2D{ histoArrayBuilder( owner, ArrayType::StationRegion, "numChamb2D_", "Numerator chambers 2D_",
                                             geometryinfo.m_chambersInRegion.first,
                                             geometryinfo.m_chambersInRegion.second,
                                             std::make_index_sequence<16>() ) } {}
    };
    std::unique_ptr<Histograms> m_histos;
  };

  DECLARE_COMPONENT_WITH_ID( MuonChamberMonitor, "MuonChamberMonitor " )

  MuonChamberMonitor::MuonChamberMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top },
                    KeyValue{ "GeomCache", name + "-GeomCache" }, KeyValue{ "Probes", ParticleLocation::User },
                    KeyValue{ "Tags", ParticleLocation::User },
                    KeyValue{ "MuonHits", MuonHitContainerLocation::Default } } ) {}

  StatusCode MuonChamberMonitor::initialize() {
    return Consumer::initialize().andThen( [&]() {
      // Add condition callback to initialize the histograms on the first event.
      addConditionDerivation( { DeMuonLocation::Default }, name() + "-GeomCache", [this]( const DeMuonDetector& det ) {
        info() << "Loading muon geometry." << endmsg;
        const GeomCache geometryinfo( det );
        info() << "Geometry loaded correctly." << endmsg;
        std::call_once( this->m_alreadyInitialized,
                        [this, &geometryinfo] { // I hope the 'this' here refers to the captured
                                                // 'this', and not the 'this' of the synthesized
                                                // object corresponding to the outer lambda....
                          m_histos = std::make_unique<Histograms>( this, geometryinfo );
                          info() << "Histograms booked." << endmsg;
                        } );
        return geometryinfo;
      } );
    } );
  }

  void MuonChamberMonitor::operator()( const DetectorElement& lhcb, const GeomCache& geometryinfo,
                                       const LHCb::Particle::Range& probes, const LHCb::Particle::Range& tags,
                                       const MuonHitContainer& muonhits ) const {
    fillHistograms( lhcb, geometryinfo, probes, tags, muonhits );
  }

  void MuonChamberMonitor::fillHistograms( const DetectorElement& lhcb, const GeomCache& geometryinfo,
                                           const LHCb::Particle::Range& probes, const LHCb::Particle::Range& tags,
                                           const MuonHitContainer& muonhits ) const {
    auto& geometry = *lhcb.geometry();

    unsigned int ntags = 0;
    for ( auto const* tag : tags ) {
      if ( !tag->proto() ) { continue; }
      ++m_histos->m_tagsP[tag->proto()->track()->p()];
      ++m_histos->m_tagsPT[tag->proto()->track()->pt()];
      ++m_histos->m_tagPsRapidity[tag->proto()->track()->pseudoRapidity()];
      ntags++;
    }
    ++m_histos->m_ntags[ntags];
    for ( auto const* probe : probes ) {
      if ( !probe->proto() || ntags == 0 ) { continue; }
      ++m_histos->m_probesP[probe->proto()->track()->p()];
      ++m_histos->m_probesPT[probe->proto()->track()->pt()];
      ++m_histos->m_probePsRapidity[probe->proto()->track()->pseudoRapidity()];

      // Initialise muon candidate
      MuonCandidate candMuon;
      candMuon.m_probeP = probe->proto()->track()->p();
      candMuon.m_proto  = probe->proto();
      candMuon.m_track  = probe->proto()->track();
      candMuon.m_state  = probe->proto()->track()->stateAt( LHCb::State::Location::EndRich2 );

      State trackState = closestState( *probe->proto()->track(), candMuon.m_state->z() );

      // Match region of extrapolation
      for ( unsigned int s = 0; s != nMuonStations; ++s ) {
        StatusCode sc = m_extrapolator->propagate( trackState, geometryinfo.m_stationZ[s], geometry );
        if ( !sc )
          warning() << fmt::format( "Something went wrong in track extrapolation in station M{}", s + 2 ) << endmsg;
        candMuon.m_trackExt[s] = { trackState.x(), trackState.y() };
        candMuon.m_trackMS[s]  = { trackState.errX2(), trackState.errY2() };
        int region             = 0;
        while ( region < 3 ) {
          if ( fabs( candMuon.m_trackExt[s].first ) < ( geometryinfo.m_regionOuterX[s][region] ) &&
               fabs( candMuon.m_trackExt[s].second ) < ( geometryinfo.m_regionOuterY[s][region] ) )
            break;
          region++;
        }
        candMuon.m_extrapReg[s] = region;
        candMuon.m_idReg[s]     = s * nMuonStations + region;
      }

      for ( unsigned int s = 0; s != nMuonStations; ++s ) {

        ++m_histos->m_probeExtrap[{ candMuon.m_trackExt[s].first, candMuon.m_trackExt[s].second }];

        auto [chit, chi2] =
            computeChi2( geometryinfo, candMuon.m_trackExt[s], candMuon.m_trackMS[s], muonhits.station( s ).hits() );

        candMuon.m_chitReg[s]     = chit.region();
        candMuon.m_chit_XY[s]     = { chit.x(), chit.y() };
        candMuon.m_probeChi2St[s] = chi2;
        ++m_histos->m_Chi2Station[s][candMuon.m_probeChi2St[s]];
      };

      for ( unsigned int s = 0; s != nMuonStations; ++s ) {

        bool otherStationsMatched = true;
        for ( unsigned ss = 0; ss != nMuonStations; ++ss ) {
          if ( ss == s ) continue; // Ignore the station we are looking at
          if ( candMuon.m_probeChi2St[ss] < m_chi2cut )
            continue;
          else {
            otherStationsMatched = false;
            break;
          }
        }

        if ( !otherStationsMatched ) continue;
        int ExtHits =
            HitsInChi2( geometryinfo, candMuon.m_trackExt[s], candMuon.m_trackMS[s], muonhits.station( s ).hits(), 1 );
        int BkgHits =
            HitsInChi2( geometryinfo, candMuon.m_trackExt[s], candMuon.m_trackMS[s], muonhits.station( s ).hits(), -1 );

        ++m_histos->m_HitsInExtrap[{ candMuon.m_idReg[s], ExtHits }];
        ++m_histos->m_HitsInBkg[{ candMuon.m_idReg[s], BkgHits }];
        ++m_histos->m_HitsInExtrapSub[{ candMuon.m_idReg[s], ExtHits - BkgHits }];

        // chamber with hit
        double hcx        = candMuon.m_chit_XY[s].first;
        double hcy        = candMuon.m_chit_XY[s].second;
        int    chamberHit = xy2Chamber( geometryinfo, hcx, hcy, s, candMuon.m_chitReg[s] );

        // chamber at extrapolation point
        double ecx        = candMuon.m_trackExt[s].first;
        double ecy        = candMuon.m_trackExt[s].second;
        int    chamberExt = xy2Chamber( geometryinfo, ecx, ecy, s, candMuon.m_extrapReg[s] );

        double errX2 = trackState.errX2();
        double errY2 = trackState.errY2();

        ++m_histos->m_errXY2[{ errX2, errY2 }];

        bool chamberMatched = true;
        if ( chamberHit == chamberExt && candMuon.m_extrapReg[s] == candMuon.m_chitReg[s] ) {
          ++m_histos->m_edgeEffect[{ candMuon.m_idReg[s], 1 }];
        } else {
          chamberMatched = false;
          ++m_histos->m_edgeEffect[{ candMuon.m_idReg[s], 0 }];
        }

        unsigned int chitRegId = s * nMuonStations + candMuon.m_chitReg[s];
        ++m_histos->m_denStation[s];
        ++m_histos->m_denStationRegion[chitRegId];
        if ( chamberMatched ) {
          ++m_histos->m_denChamb[chitRegId][chamberExt];
          ++m_histos->m_denChamb2D[chitRegId][{ hcx, hcy }];
        }

        if ( candMuon.m_probeChi2St[s] < m_chi2cut ) {
          ++m_histos->m_numStation[s];
          ++m_histos->m_numStationRegion[chitRegId];
          if ( chamberMatched ) {
            ++m_histos->m_numChamb[chitRegId][chamberExt];
            ++m_histos->m_numChamb2D[chitRegId][{ hcx, hcy }];
          }
        }
      }
    }
  }

  // Compute chi2, note: we're using all hits (no crossed/uncrossed distinction)
  std::tuple<CommonMuonHit, double> MuonChamberMonitor::computeChi2( const GeomCache& geometryinfo, const xy_t& ext,
                                                                     const xy_t&               extMS,
                                                                     const CommonMuonHitRange& hitContainer ) const {

    if ( hitContainer.size() == 0 ) return std::tuple( CommonMuonHit(), m_chi2bad ); // assign bad chi2

    // take the hit with smallest chi2
    assert( hitContainer.begin() != hitContainer.end() );
    const auto chit = *std::min_element(
        hitContainer.begin(), hitContainer.end(),
        [geometryinfo, ext, extMS]( const CommonMuonHit& hit1, const CommonMuonHit& hit2 ) {
          auto dr2 = [&]( CommonMuonHit const& h ) {
            return pow( ( h.x() - ext.first ), 2 ) /
                       ( pow( geometryinfo.m_sigmapadX[h.station()][h.region()], 2 ) + extMS.first ) +
                   pow( ( h.y() - ext.second ), 2 ) /
                       ( pow( geometryinfo.m_sigmapadY[h.station()][h.region()], 2 ) + extMS.second );
          };
          return dr2( hit1 ) < dr2( hit2 );
        } );

    double chi2 = pow( ( chit.x() - ext.first ), 2 ) /
                      ( pow( geometryinfo.m_sigmapadX[chit.station()][chit.region()], 2 ) + extMS.first ) +
                  pow( ( chit.y() - ext.second ), 2 ) /
                      ( pow( geometryinfo.m_sigmapadY[chit.station()][chit.region()], 2 ) + extMS.second );

    return std::tuple( chit, chi2 );
  }

  int MuonChamberMonitor::HitsInChi2( const GeomCache& geometryinfo, const xy_t& ext, const xy_t& extMS,
                                      const CommonMuonHitRange& hitContainer, int sign ) const {
    if ( hitContainer.size() == 0 ) return 0;
    int nhits = 0;
    for ( auto& hit : hitContainer ) {
      double chi2 = pow( ( hit.x() - sign * ext.first ), 2 ) /
                        ( pow( geometryinfo.m_sigmapadX[hit.station()][hit.region()], 2 ) + extMS.first ) +
                    pow( ( hit.y() - sign * ext.second ), 2 ) /
                        ( pow( geometryinfo.m_sigmapadY[hit.station()][hit.region()], 2 ) + extMS.second );
      if ( chi2 < m_chi2cut ) { nhits++; }
    }
    return nhits;
  }

  int MuonChamberMonitor::xy2Chamber( const GeomCache& geometryinfo, double x, double y, unsigned int station,
                                      unsigned int region ) const {

    double maxX = geometryinfo.m_stationOuterX[station] / pow( 2., 3 - region );
    double maxY = geometryinfo.m_stationOuterY[station] / pow( 2., 3 - region );

    int nrow = c_regionNch[region] * 4 / ( 3 * c_regionNchCol[region] );
    int row  = (int)( ( y + maxY ) / ( 2 * maxY ) * (double)nrow );
    if ( row > ( nrow - 1 ) ) row = nrow - 1;
    if ( row < 0 ) row = 0;

    int col = (int)( ( maxX - x ) / ( 2 * maxX ) * (double)c_regionNchCol[region] );
    if ( col > ( c_regionNchCol[region] - 1 ) ) col = c_regionNchCol[region] - 1;
    if ( col < 0 ) col = 0;

    int chid = ( nrow - 1 - row ) * c_regionNchCol[region] + col;

    // subtract missing chambers in beam pipe hole
    if ( row < nrow * 3 / 4 ) {
      int nrskip;
      if ( row < nrow * 1 / 4 ) {
        nrskip = nrow / 2;
      } else {
        nrskip = nrow * 3 / 4 - row;
        if ( col < c_regionNchCol[region] / 2 ) nrskip--;
      }
      chid -= ( nrskip * c_regionNchCol[region] / 2 );
    }

    return chid;
  }
} // namespace LHCb
