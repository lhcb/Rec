/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "Detector/Muon/Namespace.h"
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"
#include "Event/MuonCoord.h"
#include "Event/MuonDigit.h"
#include "Event/MuonPID.h"
#include "Event/RecHeader.h"
#include "Event/Track.h"
#include "LHCbAlgs/Consumer.h"
#include "MuonDet/DeMuonDetector.h"

#include "Detector/Muon/MuonConstants.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"

#include <cmath>
#include <iomanip>
#include <optional>
#include <string>
#include <vector>

namespace {
  struct TrackInfo {
    double              trp0{ -10000. };
    double              trpT{ -10000. };
    int                 trRegionM2{ -1 };
    std::vector<double> trackX{}; // position of track in x(mm) in each station
    std::vector<double> trackY{}; // position of track in y(mm) in each station
  };
  struct MuonPIDInfo {
    bool         trIsPreSel{ false };
    unsigned int trIsMuon{ 1000 };
    unsigned int trIsMuonLoose{ 1000 };
    unsigned int trNShared{ 1000 };
    double       trMuonLhd{ -1000. };
    double       trNMuonLhd{ -1000. };
  };
  struct MuonTrackInfo {
    double       trDist2{ -1000 };
    double       trquality{ -1000 };
    double       trCLquality{ -1000 };
    double       trChi2{ -1000 };
    double       trCLarrival{ -1000 };
    unsigned int trnhitsfoi[20]{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
  };
  double getRateError( double rate, double nPreSelScaling ) {
    return 100. * sqrt( rate * ( 1. - rate ) * nPreSelScaling );
  }
} // namespace

namespace LHCb {

  /**
   *  Monitor MuonPID multiplicities and spectrum
   *
   *  @author Erica Polycarpo Macedo
   *  @date   2010-03-20
   */
  class MuonPIDChecker : public Algorithm::Consumer<void( LHCb::Tracks const&, LHCb::MuonPIDs const&,
                                                          LHCb::Tracks const&, DeMuonDetector const& ),
                                                    Algorithm::Traits::usesConditions<DeMuonDetector>> {
  public:
    MuonPIDChecker( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { { "TrackLocation", LHCb::TrackLocation::Default },
                      { "MuonIDLocation", LHCb::MuonPIDLocation::Default },
                      { "MuonTrackLocation", LHCb::TrackLocation::Muon },
                      { "MuonDetectorLocation", Detector::Muon::Location::Default } } ){};

    StatusCode initialize() override;
    void       operator()( LHCb::Tracks const&, LHCb::MuonPIDs const&, LHCb::Tracks const&,
                     DeMuonDetector const& ) const override;
    StatusCode finalize() override;

  private:
    Gaudi::Property<int> m_HistosOutput{
        this, "HistosOutput", 3,
        "OutputLevel for Histograms; 0=None, 1=Online, 2=OfflineExpress, 3=OfflineFull, 4=Expert" };
    Gaudi::Property<int>  m_TrackType{ this, "TrackType", 0, "Look at Long,Downstream or Both types of tracks" };
    Gaudi::Property<bool> m_RunningMC{ this, "RunningMC", false, "Swap between real and MC data" };
    Gaudi::Property<std::vector<float>> m_monitCutValues{ this,
                                                          "MonitorCutValues",
                                                          // IsMuonLoose, IsMuon, MuProb, MuProb, DLL, DLL, NShared,
                                                          // NShared
                                                          { 1., 1., 0.9, 0.9, 1.4, 1.4, 1., 1. } };
    Gaudi::Property<float>              m_DLLlower{ this, "DLLlower", -1., "lower limit for DLL histo" };
    Gaudi::Property<float>              m_DLLupper{ this, "DLLupper", 6., "upper limit for DLL histo" };
    Gaudi::Property<unsigned int>       m_DLLnbins{ this, "DLLnbins", 35, "number of bins for DLL histo" };

    MuonPIDInfo   getMuonPIDInfo( const LHCb::Track&, const LHCb::MuonPIDs& ) const;
    MuonTrackInfo getMuonTrackInfo( LHCb::Track const&, LHCb::Tracks const&, MuonPIDInfo const& ) const;
    TrackInfo     getTrackInfo( DeMuonDetector const&, LHCb::Track const& ) const;
    void          fillPreSelPlots( int level, TrackInfo const&, MuonPIDInfo const& ) const;
    void          fillIMLPlots( int level, TrackInfo const&, MuonPIDInfo const&, MuonTrackInfo const& ) const;
    void          fillIMPlots( int level, TrackInfo const&, MuonPIDInfo const&, MuonTrackInfo const& ) const;
    void          fillHitMultPlots( int level, TrackInfo const&, MuonTrackInfo const& ) const;

    mutable Gaudi::Accumulators::AveragingCounter<> m_nbTracks{ this, "Number of Tracks analysed" };
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbPreSelTracks{ this, "Number of PreSelected Tracks" };
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbIsMuonLoose{ this, "Number of IsMuonLoose Candidates" };
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbIsMuon{ this, "Number of IsMuon Candidates" };
    mutable Gaudi::Accumulators::Counter<>          m_nbIsMuonLooseProb{ this, "Number of IsMuonLoose under MuonProb" };
    mutable Gaudi::Accumulators::Counter<>          m_nbIsMuonProb{ this, "Number of IsMuon under MuonProb" };
    mutable Gaudi::Accumulators::Counter<>          m_nbIsMuonLooseDLL{ this, "Number of IsMuonLoose over DLL" };
    mutable Gaudi::Accumulators::Counter<>          m_nbIsMuonDLL{ this, "Number of IsMuon over DLL" };
    mutable Gaudi::Accumulators::Counter<> m_nbIsMuonLooseNShared{ this, "Number of IsMuonLoose under NShared" };
    mutable Gaudi::Accumulators::Counter<> m_nbIsMuonNShared{ this, "Number of IsMuon under NShared" };

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_{ this, "" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_badMuonVsLoose{
        this, "getMuonPIDInfo:: Muon Track IsMuon < IsMuonLoose" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_tooManyPids{
        this, "getMuonPIDInfo:: nMuonPIDs associated to track >1" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noAncestor{
        this, "getMuonTrackInfo:: failed to get Muon Track ancestor" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_badPSFlag{
        this, "getMuonTrackInfo:: PS flag is different for Muon Track and MuonPID" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_badIMLFlag{
        this, "getMuonTrackInfo:: IML flag is different for Muon Track and MuonPID" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_badIMFlag{
        this, "getMuonTrackInfo:: IM flag is different for Muon Track and MuonPID" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_extrapolateFailed{
        this, "getTrackInfo:: Failed to extrapolate track" };

    using OptHisto1D     = std::optional<Gaudi::Accumulators::Histogram<1>>;
    using OptProfHisto1D = std::optional<Gaudi::Accumulators::ProfileHistogram<1>>;
    using OptHisto1DArray =
        std::optional<Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, Detector::Muon::nRegions>>;
    using OptProfHisto1DArray = std::optional<
        Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::ProfileHistogram<1>, Detector::Muon::nStations>>;
    using OptHisto2DArray = std::optional<
        Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, Detector::Muon::nStations>>;
    // Histograms for HistosOutput level >= 1
    mutable OptHisto1D m_hNIMLtracks;
    mutable OptHisto1D m_hNIMtracks;
    mutable OptHisto1D m_hNIMLtracksRatio;
    mutable OptHisto1D m_hIMLMomentum;
    mutable OptHisto1D m_hIMLPT;
    mutable OptHisto1D m_hIMLRegion;
    // Histograms for HistosOutput level >= 2
    mutable OptHisto1D      m_hNtracks;
    mutable OptHisto1D      m_hPSNtracks;
    mutable OptHisto1D      m_hNIMLPStracksRatio;
    mutable OptHisto1D      m_hNIMPStracksRatio;
    mutable OptHisto1D      m_hNIMtracksRatio;
    mutable OptHisto1D      m_hPSRegion;
    mutable OptHisto1D      m_hPSMomentum;
    mutable OptHisto1D      m_hPSPT;
    mutable OptHisto1D      m_hIML_PS;
    mutable OptHisto1D      m_hIM_PS;
    mutable OptHisto1D      m_hNShared_IML;
    mutable OptHisto1D      m_hDist2_IML;
    mutable OptHisto1D      m_hProbMu_IML;
    mutable OptHisto1D      m_hProbNMu_IML;
    mutable OptHisto1D      m_hMuDLL_IML;
    mutable OptHisto1D      m_hNIMLvsXM2;
    mutable OptHisto1D      m_hNIMLvsYM2;
    mutable OptHisto1D      m_hIMMomentum;
    mutable OptHisto1D      m_hIMPT;
    mutable OptHisto1D      m_hIMRegion;
    mutable OptHisto1D      m_hNShared_IM;
    mutable OptHisto1D      m_hDist2_IM;
    mutable OptHisto1D      m_hProbMu_IM;
    mutable OptHisto1D      m_hProbNMu_IM;
    mutable OptHisto1D      m_hMuDLL_IM;
    mutable OptHisto1D      m_hNIMvsXM2;
    mutable OptHisto1D      m_hNIMvsYM2;
    mutable OptHisto1DArray m_hDist2_IML_R;
    mutable OptHisto1DArray m_hProbMu_IML_R;
    mutable OptHisto1DArray m_hDist2_IM_R;
    mutable OptHisto1DArray m_hProbMu_IM_R;
    // Histograms for HistosOutput level >= 3
    mutable OptProfHisto1D      m_hEffvsP_IML;
    mutable OptProfHisto1D      m_hEffvsPT_IML;
    mutable OptProfHisto1D      m_hEffvsP_IM;
    mutable OptProfHisto1D      m_hEffvsPT_IM;
    mutable OptHisto1D          m_hIM_IML;
    mutable OptHisto1DArray     m_hDLL_IML_R;
    mutable OptHisto1DArray     m_hDLL_IM_R;
    mutable OptProfHisto1D      m_hAvTotNhitsFOIvsR;
    mutable OptProfHisto1D      m_hAvTotNhitsFOIvsX;
    mutable OptProfHisto1D      m_hAvTotNhitsFOIvsY;
    mutable OptProfHisto1DArray m_hAvNHhitsFOIvsR_M;
    // Histograms for HistosOutput level >= 4
    mutable OptHisto1D          m_hChi2_IML;
    mutable OptHisto1D          m_hQuality_IML;
    mutable OptHisto1D          m_hCLQuality_IML;
    mutable OptHisto1D          m_hCLArrival_IML;
    mutable OptProfHisto1D      m_hProbMuvsP_IML;
    mutable OptProfHisto1D      m_hNProbMuvsP_IML;
    mutable OptHisto1D          m_hChi2_IM;
    mutable OptHisto1D          m_hQuality_IM;
    mutable OptHisto1D          m_hCLQuality_IM;
    mutable OptHisto1D          m_hCLArrival_IM;
    mutable OptProfHisto1D      m_hProbMuvsP_IM;
    mutable OptProfHisto1D      m_hNProbMuvsP_IM;
    mutable OptHisto2DArray     m_hNHhitsFOIvsR_M;
    mutable OptHisto2DArray     m_hNhitsFOIvsX_M;
    mutable OptHisto2DArray     m_hNhitsFOIvsY_M;
    mutable OptProfHisto1DArray m_hAvNhitsFOIvsX_M;
    mutable OptProfHisto1DArray m_hAvNhitsFOIvsY_M;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( MuonPIDChecker, "MuonPIDChecker" )

} // namespace LHCb

StatusCode LHCb::MuonPIDChecker::initialize() {
  return Consumer::initialize().andThen( [&] {
    using Axis1D = Gaudi::Accumulators::Axis<double>;
    // Book histograms according to m_HistosOutput
    if ( m_HistosOutput > 0 ) {
      // fillMultiplicityPlots
      m_hNIMLtracks.emplace( this, "hNIMLtracks", "IsMuonLoose Track multiplicity", Axis1D{ 12, -0.5, 11.5 } );
      m_hNIMtracks.emplace( this, "hNIMtracks", "IsMuonLoose Track multiplicity", Axis1D{ 12, -0.5, 11.5 } );
      m_hNIMLtracksRatio.emplace( this, "hNIMLtracksRatio", "#IsMuonLoose/#Tracks", Axis1D{ 22, 0., 1.1 } );
      // fillIMLPlots
      m_hIMLMomentum.emplace( this, "hIMLMomentum", "IsMuonLoose Candidate Momentum (GeV/c^2)",
                              Axis1D{ 100, -25., 25. } );
      m_hIMLPT.emplace( this, "hIMLPT", "IsMuonLoose Candidate p_T (GeV/c^2)", Axis1D{ 100, -5., 5. } );
      m_hIMLRegion.emplace( this, "hIMLRegion", "MS Region for IML  tracks", Axis1D{ 4, 0.5, 4.5 } );
    }
    if ( m_HistosOutput > 1 ) {
      // fillMultiplicityPlots
      m_hNtracks.emplace( this, "hNtracks", "Track multiplicity", Axis1D{ 61, -0.5, 60.5 } );
      m_hPSNtracks.emplace( this, "hPSNtracks", "PreSelection Track multiplicity", Axis1D{ 51, -0.5, 50.5 } );
      m_hNIMLPStracksRatio.emplace( this, "hNIMLPStracksRatio", "#IsMuonLoose/#PSTracks", Axis1D{ 22, 0., 1.1 } );
      m_hNIMPStracksRatio.emplace( this, "hNIMPStracksRatio", "#IsMuon/#PSTracks", Axis1D{ 22, 0., 1.1 } );
      m_hNIMtracksRatio.emplace( this, "hNIMtracksRatio", "#IsMuon/#Tracks", Axis1D{ 22, 0., 1.1 } );
      // fillPreSelPlots
      m_hPSRegion.emplace( this, "hPSRegion", "MS Region for PS  tracks", Axis1D{ 4, 0.5, 4.5 } );
      m_hPSMomentum.emplace( this, "hPSMomentum", "PreSelected Track Momentum (GeV/c^2)", Axis1D{ 100, -25., 25. } );
      m_hPSPT.emplace( this, "hPSPT", "PreSelected Track p_T (GeV/c^2)", Axis1D{ 100, -5., 5. } );
      m_hIML_PS.emplace( this, "hIML_PS", " IsMuonLoose for PS Tracks ", Axis1D{ 2, -0.5, 1.5 } );
      m_hIM_PS.emplace( this, "hIM_PS", " IsMuon for PS Tracks ", Axis1D{ 2, -0.5, 1.5 } );
      // fillIMLPlots
      m_hNShared_IML.emplace( this, "hNShared_IML", " NShared for PS Tracks ", Axis1D{ 6, -0.5, 5.5 } );
      m_hDist2_IML.emplace( this, "hDist2_IML", "Muon Dist for IML candidates", Axis1D{ 100, 0., 600. } );
      m_hProbMu_IML.emplace( this, "hProbMu_IML", "Muon Probability for IML candidates", Axis1D{ 60, -0.1, 1.1 } );
      m_hProbNMu_IML.emplace( this, "hProbNMu_IML", "Non-Muon Probability for IML candidates",
                              Axis1D{ 60, -0.1, 1.1 } );
      m_hMuDLL_IML.emplace( this, "hMuDLL_IML", "Muon DLL for IML candidates",
                            Axis1D{ m_DLLnbins, m_DLLlower, m_DLLupper } );
      m_hNIMLvsXM2.emplace( this, "hNIMLvsXM2", "MS X position at M2 for IML  tracks", Axis1D{ 550, -5500, 5500 } );
      m_hNIMLvsYM2.emplace( this, "hNIMLvsYM2", "MS Y position at M2 for IML  tracks", Axis1D{ 550, -5500, 5500 } );
      // fillIMPlots
      m_hIMMomentum.emplace( this, "hIMMomentum", "IsMuon candidate Momentum (GeV/c^2)", Axis1D{ 100, -25., 25. } );
      m_hIMPT.emplace( this, "hIMPT", "IsMuon candidate p_T (GeV/c^2)", Axis1D{ 100, -5., 5. } );
      m_hIMRegion.emplace( this, "hIMRegion", "MS Region for IM tracks", Axis1D{ 4, 0.5, 4.5 } );
      m_hNShared_IM.emplace( this, "hNShared_IM", " NShared for PS Tracks ", Axis1D{ 6, -0.5, 5.5 } );
      m_hDist2_IM.emplace( this, "hDist2_IM", "Muon Dist for IM candidates", Axis1D{ 100, 0., 600. } );
      m_hProbMu_IM.emplace( this, "hProbMu_IM", "Muon Probability for IM candidates", Axis1D{ 60, -0.1, 1.1 } );
      m_hProbNMu_IM.emplace( this, "hProbNMu_IM", "Non-Muon Probability for IM candidates", Axis1D{ 60, -0.1, 1.1 } );
      m_hMuDLL_IM.emplace( this, "hMuDLL_IM", "Muon DLL for IM candidates",
                           Axis1D{ m_DLLnbins, m_DLLlower, m_DLLupper } );
      m_hNIMvsXM2.emplace( this, "hNIMvsXM2", "MS X position at M2 for IM  tracks", Axis1D{ 550, -5500, 5500 } );
      m_hNIMvsYM2.emplace( this, "hNIMvsYM2", "MS Y position at M2 for IM  tracks", Axis1D{ 550, -5500, 5500 } );
      m_hDist2_IML_R.emplace( this, "hDist2_IML_R{}", "Muon Dist for IML candidates at R{}", Axis1D{ 100, 0., 600. } );
      m_hProbMu_IML_R.emplace( this, "hProbMu_IML_R{}", "Muon Probability for IML candidates at R{}",
                               Axis1D{ 60, -0.1, 1.1 } );
      m_hDist2_IM_R.emplace( this, "hDist2_IM_R{}", "Muon Dist for IM candidates at R{}", Axis1D{ 100, 0., 600. } );
      m_hProbMu_IM_R.emplace( this, "hProbMu_IM_R{}", "Muon Probability for IM candidates at R{}",
                              Axis1D{ 60, -0.1, 1.1 } );
    }
    if ( m_HistosOutput > 2 ) {
      // fillPreSelPlots
      m_hEffvsP_IML.emplace( this, "hEffvsP_IML", "IML Efficiency vs P (GeV/c^2)", Axis1D{ 100, -25., 25. } );
      m_hEffvsPT_IML.emplace( this, "hEffvsPT_IML", "IML Efficiency vs PT (GeV/c^2)", Axis1D{ 100, -5., 5. } );
      m_hEffvsP_IM.emplace( this, "hEffvsP_IM", "IM Efficiency vs P (GeV/c^2)", Axis1D{ 100, -25., 25. } );
      m_hEffvsPT_IM.emplace( this, "hEffvsPT_IM", "IM Efficiency vs PT (GeV/c^2)", Axis1D{ 100, -5., 5. } );
      // fillIMLPlots
      m_hIM_IML.emplace( this, "hIM_IML", " IsMuon for IML Tracks ", Axis1D{ 2, -0.5, 1.5 } );
      m_hDLL_IML_R.emplace( this, "hDLL_IML_R{}", "Muon DLL for IML candidates at R{}",
                            Axis1D{ m_DLLnbins, m_DLLlower, m_DLLupper } );
      m_hDLL_IM_R.emplace( this, "hDLL_IM_R{}", "Muon DLL for IM candidates at R{}",
                           Axis1D{ m_DLLnbins, m_DLLlower, m_DLLupper } );
      // fillHitMultPlots
      m_hAvTotNhitsFOIvsR.emplace( this, "hAvTotNhitsFOIvsR", "Mean Number of hits in FOI vs Region (M2)",
                                   Axis1D{ 4, 0.5, 4.5 } );
      m_hAvTotNhitsFOIvsX.emplace( this, "hAvTotNhitsFOIvsX", "Mean Number of hits in FOI vs X (M2)",
                                   Axis1D{ 200, -5000, 5000 } );
      m_hAvTotNhitsFOIvsY.emplace( this, "hAvTotNhitsFOIvsY", "Mean Number of hits in FOI vs Y (M2)",
                                   Axis1D{ 200, -5000, 5000 } );
      m_hAvNHhitsFOIvsR_M.emplace( this, "hAvNHhitsFOIvsR_M{}", "Mean Number of hits in FOI for M{} vs MS Region",
                                   Axis1D{ 4, 0.5, 4.5 } );
    }
    if ( m_HistosOutput > 3 ) {
      // fillIMLPlots
      m_hChi2_IML.emplace( this, "hChi2_IML", "Chi2 per nDOF for IML Candidates", Axis1D{ 100, 0., 200. } );
      m_hQuality_IML.emplace( this, "hQuality_IML", "Track Quality for IML Candidates", Axis1D{ 100, 0., 200. } );
      m_hCLQuality_IML.emplace( this, "hCLQuality_IML", "Track CL Quality for IML Candidates",
                                Axis1D{ 60, -0.1, 1.1 } );
      m_hCLArrival_IML.emplace( this, "hCLArrival_IML", "Track CL Arrival for IML Candidates",
                                Axis1D{ 60, -0.1, 1.1 } );
      m_hProbMuvsP_IML.emplace( this, "hProbMuvsP_IML", "Mean Muon Prob vs p for IML tracks", Axis1D{ 100, 0., 100. } );
      m_hNProbMuvsP_IML.emplace( this, "hNProbMuvsP_IML", "Mean non-Muon Prob vs P for IML tracks ",
                                 Axis1D{ 100, 0., 100. } );
      // fillIMPlots
      m_hChi2_IM.emplace( this, "hChi2_IM", "Chi2 per nDOF for IM Candidates", Axis1D{ 100, 0., 200. } );
      m_hQuality_IM.emplace( this, "hQuality_IM", "Track Quality for IM Candidates", Axis1D{ 100, 0., 200. } );
      m_hCLQuality_IM.emplace( this, "hCLQuality_IM", "Track CL Quality for IM Candidates", Axis1D{ 60, -0.1, 1.1 } );
      m_hCLArrival_IM.emplace( this, "hCLArrival_IM", "Track CL Arrival for IM Candidates", Axis1D{ 60, -0.1, 1.1 } );
      m_hProbMuvsP_IM.emplace( this, "hProbMuvsP_IM", "Mean Muon Prob vs p for IM tracks", Axis1D{ 100, 0., 100. } );
      m_hNProbMuvsP_IM.emplace( this, "hNProbMuvsP_IM", "Mean non-Muon Prob vs P for IM tracks ",
                                Axis1D{ 100, 0., 100. } );
      // fillHitMultPlots
      m_hNHhitsFOIvsR_M.emplace( this, "hNHhitsFOIvsR_M{}", "Number of hits in FOI for M{} vs MS Region",
                                 Axis1D{ 4, 0.5, 4.5 }, Axis1D{ 11, -0.5, 10.5 } );
      m_hNhitsFOIvsX_M.emplace( this, "hNhitsFOIvsX_M{}", "Number of hits in FOI for M{} vs MS X Pos.",
                                Axis1D{ 11, -0.5, 10.5 }, Axis1D{ 200, -5000, 5000 } );
      m_hNhitsFOIvsY_M.emplace( this, "hNhitsFOIvsY_M{}", "Number of hits in FOI for M{} vs MS Y Pos.",
                                Axis1D{ 11, -0.5, 10.5 }, Axis1D{ 200, -5000, 5000 } );
      m_hAvNhitsFOIvsX_M.emplace( this, "hAvNhitsFOIvsX_M{}", "Mean Number of hits in FOI for M{} vs MS X Pos.",
                                  Axis1D{ 200, -5000, 5000 } );
      m_hAvNhitsFOIvsY_M.emplace( this, "hAvNhitsFOIvsY_M{}", "Mean Number of hits in FOI for M{} vs MS Y Pos.",
                                  Axis1D{ 200, -5000, 5000 } );
    }
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void LHCb::MuonPIDChecker::operator()( LHCb::Tracks const& trTracks, LHCb::MuonPIDs const& muids,
                                       LHCb::Tracks const& muTracks, DeMuonDetector const& muonDet ) const {
  // Event  counters
  unsigned int nTr{ 0 };
  unsigned int nTrPreSel{ 0 };
  unsigned int nTrIsMuonLoose{ 0 };
  unsigned int nTrIsMuon{ 0 };
  for ( const auto& track : trTracks ) {
    if ( !( track->checkFlag( LHCb::Track::Flags::Clone ) ) &&
         ( ( track->checkType( LHCb::Track::Types::Long ) && ( m_TrackType == 0 || m_TrackType == 2 ) ) ||
           ( track->checkType( LHCb::Track::Types::Downstream ) && ( m_TrackType == 1 || m_TrackType == 2 ) ) ) ) {
      nTr++;
      auto trackInfo     = getTrackInfo( muonDet, *track );
      auto muonPIDInfo   = getMuonPIDInfo( *track, muids );
      auto muonTrackInfo = getMuonTrackInfo( *track, muTracks, muonPIDInfo );
      if ( muonPIDInfo.trIsPreSel ) {
        nTrPreSel++;
        // Find region hit by the track
        trackInfo.trRegionM2 = muonDet.regNum( trackInfo.trackX[1], trackInfo.trackY[1], 1 ) + 1;
        fillPreSelPlots( m_HistosOutput, trackInfo, muonPIDInfo );
        fillHitMultPlots( m_HistosOutput, trackInfo, muonTrackInfo );
        if ( muonPIDInfo.trIsMuonLoose > 0 ) {
          nTrIsMuonLoose++;
          fillIMLPlots( m_HistosOutput, trackInfo, muonPIDInfo, muonTrackInfo );
          if ( muonPIDInfo.trIsMuon > 0 ) {
            nTrIsMuon++;
            fillIMPlots( m_HistosOutput, trackInfo, muonPIDInfo, muonTrackInfo );
          }
        }
      } // track is preselected
    }   // track type is satisfied
  }     // loop over tracks
  // fill multiplicity plots
  if ( m_HistosOutput > 0 ) {
    ++( *m_hNIMLtracks )[nTrIsMuonLoose];
    ++( *m_hNIMtracks )[nTrIsMuon];
    if ( nTr > 0 ) ++( *m_hNIMLtracksRatio )[(double)nTrIsMuonLoose / nTr];
  }
  if ( m_HistosOutput > 1 ) {
    ++( *m_hNtracks )[nTr];
    ++( *m_hPSNtracks )[nTrPreSel];
    if ( nTrPreSel > 0 ) {
      ++( *m_hNIMLPStracksRatio )[(double)nTrIsMuonLoose / nTrPreSel];
      ++( *m_hNIMPStracksRatio )[(double)nTrIsMuon / nTrPreSel];
    }
    if ( nTr > 0 ) ++( *m_hNIMtracksRatio )[(double)nTrIsMuon / nTr];
  }
  m_nbTracks += nTr;
  m_nbPreSelTracks += nTrPreSel;
  m_nbIsMuonLoose += nTrIsMuonLoose;
  m_nbIsMuon += nTrIsMuon;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode LHCb::MuonPIDChecker::finalize() {
  if ( 0 < m_nbPreSelTracks.sum() ) {
    double nPreSelScaling = 1. / (double)m_nbPreSelTracks.sum();
    info() << "----------------------------------------------------------------" << endmsg;
    if ( m_TrackType == 0 ) info() << "                  MuonID Rates for Long Tracks " << endmsg;
    if ( m_TrackType == 1 ) info() << "                MuonID Rates for Downstream Tracks " << endmsg;
    if ( m_TrackType == 2 ) info() << "             MuonID Rates for Long+Downstream Tracks " << endmsg;
    info() << "----------------------------------------------------------------" << endmsg;
    info() << "  Criterium                          Rate After IsMuonLoose (%) " << endmsg;
    info() << "----------------------------------------------------------------" << endmsg;
    double rate = (double)m_nbIsMuonLoose.sum() * nPreSelScaling;
    info() << fmt::format( "IsMuonLoose         = {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[0], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonLooseProb.nEntries() * nPreSelScaling;
    info() << fmt::format( "IsMuoLoose MuonProb < {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[2], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonLooseDLL.nEntries() * nPreSelScaling;
    info() << fmt::format( "IsMuoLoose DLL      > {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[4], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonLooseNShared.sum() * nPreSelScaling;
    info() << fmt::format( "IsMuonLoose NShared < {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[6], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;

    info() << "----------------------------------------------------------------" << endmsg;
    info() << "  Criterium                            Rate After IsMuon (%) " << endmsg;
    info() << "----------------------------------------------------------------" << endmsg;
    rate = (double)m_nbIsMuon.sum() * nPreSelScaling;
    info() << fmt::format( "IsMuon              = {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[1], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonProb.nEntries() * nPreSelScaling;
    info() << fmt::format( "IsMuon     MuonProb < {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[3], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonDLL.nEntries() * nPreSelScaling;
    info() << fmt::format( "IsMuon     DLL      > {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[5], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
    rate = (double)m_nbIsMuonNShared.nEntries() * nPreSelScaling;
    info() << fmt::format( "IsMuon     NShared  < {:3.1f}      :  {:7.3f} +-{:7.3f}", m_monitCutValues[7], 100 * rate,
                           getRateError( rate, nPreSelScaling ) )
           << endmsg;
  }
  return Consumer::finalize(); // must be called after all other actions
}

//=====================================================================
//  Fill MuonPID Info
//====================================================================
MuonPIDInfo LHCb::MuonPIDChecker::getMuonPIDInfo( const LHCb::Track& track, const LHCb::MuonPIDs& muids ) const {
  MuonPIDInfo res;
  int         nMuonPIDs = 0; // number of MuonPIDs associated to track
  // link between track and MuonPID
  for ( const auto& muid : muids ) {
    if ( muid->idTrack() == &track ) { // found Associated MuonPID
      nMuonPIDs++;
      // Preselection
      if ( muid->PreSelMomentum() && muid->InAcceptance() ) {
        res.trIsPreSel    = true;
        res.trIsMuon      = muid->IsMuon();
        res.trIsMuonLoose = muid->IsMuonLoose();
        if ( res.trIsMuonLoose < res.trIsMuon ) { // Sanity Check
          ++m_badMuonVsLoose;
        }
        res.trMuonLhd  = muid->MuonLLMu();
        res.trNMuonLhd = muid->MuonLLBg();
        res.trNShared  = muid->nShared();

      } // Pre-selection
    }   // Association to Track
  }     // loop over MuonPIDs
  if ( nMuonPIDs > 1 ) ++m_tooManyPids;
  return res;
}
//=====================================================================
//  Fill Muon Track Info
//====================================================================
MuonTrackInfo LHCb::MuonPIDChecker::getMuonTrackInfo( LHCb::Track const& track, LHCb::Tracks const& muTracks,
                                                      MuonPIDInfo const& mpi ) const {
  MuonTrackInfo    res;
  std::vector<int> assocHits( Detector::Muon::nRegions * Detector::Muon::nStations );
  for ( auto& muTrack : muTracks ) {
    // Get Track ancestors
    const LHCb::Track*                trParent  = NULL;
    const SmartRefVector<LHCb::Track> Trmothers = muTrack->ancestors();
    for ( SmartRefVector<LHCb::Track>::const_iterator imother = Trmothers.begin(); imother != Trmothers.end();
          imother++ ) {
      trParent = *imother;
    }
    if ( trParent == NULL ) {
      ++m_noAncestor;
      continue;
    }
    // if muon track ancestor is the current track get info
    if ( trParent == &track ) {
      // Sanity Checks
      unsigned int muTrPS = 0;
      if ( !essentiallyZero( muTrack->info( LHCb::Track::AdditionalInfo::MuonMomentumPreSel, 0 ) ) &&
           !essentiallyZero( muTrack->info( LHCb::Track::AdditionalInfo::MuonInAcceptance, 0 ) ) )
        muTrPS = 1;
      if ( muTrPS != mpi.trIsPreSel ) ++m_badPSFlag;
      unsigned int TrIsMuonLoose = (unsigned int)muTrack->info( LHCb::Track::AdditionalInfo::IsMuonLoose, 0 );
      if ( TrIsMuonLoose != mpi.trIsMuonLoose ) ++m_badIMLFlag;
      unsigned int TrIsMuon = (unsigned int)muTrack->info( LHCb::Track::AdditionalInfo::IsMuon, 0 );
      if ( TrIsMuon != mpi.trIsMuon ) ++m_badIMFlag;
      // Get Info
      res.trquality   = muTrack->info( LHCb::Track::AdditionalInfo::MuonChi2perDoF, 0 );
      res.trChi2      = muTrack->chi2PerDoF();
      res.trDist2     = muTrack->info( LHCb::Track::AdditionalInfo::MuonDist2, 0 );
      res.trCLquality = muTrack->info( LHCb::Track::AdditionalInfo::MuonCLQuality, 0 );
      res.trCLarrival = muTrack->info( LHCb::Track::AdditionalInfo::MuonCLArrival, 0 );
      // Look at coords
      for ( const auto& id : muTrack->lhcbIDs() ) {
        if ( !id.isMuon() ) continue;
        LHCb::Detector::Muon::TileID mutile   = id.muonID();
        int                          region   = mutile.region();
        int                          station  = mutile.station();
        int                          nStatReg = station * Detector::Muon::nRegions + region;
        res.trnhitsfoi[nStatReg]++;
      } // end of loop over lhcbIDs
    }
  }
  return res;
}
//=====================================================================
//  Fill Plots for PreSelected Tracks
//====================================================================
void LHCb::MuonPIDChecker::fillPreSelPlots( int level, TrackInfo const& ti, MuonPIDInfo const& mpi ) const {
  if ( level > 1 ) {
    ++( *m_hPSRegion )[ti.trRegionM2];
    ++( *m_hPSMomentum )[ti.trp0];
    ++( *m_hPSPT )[ti.trpT];
    ++( *m_hIML_PS )[mpi.trIsMuonLoose];
    ++( *m_hIM_PS )[mpi.trIsMuon];
  }
  if ( level > 2 ) {
    // Efficiencies
    ( *m_hEffvsP_IML )[ti.trp0] += mpi.trIsMuonLoose;
    ( *m_hEffvsPT_IML )[ti.trpT] += mpi.trIsMuonLoose;
    ( *m_hEffvsP_IM )[ti.trp0] += mpi.trIsMuon;
    ( *m_hEffvsPT_IM )[ti.trpT] += mpi.trIsMuon;
  }
  return;
}
//=====================================================================
//  Fill Plots for IsMuonLooseCandidates
//====================================================================
void LHCb::MuonPIDChecker::fillIMLPlots( int level, TrackInfo const& ti, MuonPIDInfo const& mpi,
                                         MuonTrackInfo const& mti ) const {
  if ( exp( mpi.trMuonLhd ) < m_monitCutValues[2] ) ++m_nbIsMuonLooseProb;
  if ( mpi.trMuonLhd - mpi.trNMuonLhd > m_monitCutValues[4] ) ++m_nbIsMuonLooseDLL;
  if ( mpi.trNShared < m_monitCutValues[6] ) ++m_nbIsMuonLooseNShared;
  if ( level > 0 ) {
    ++( *m_hIMLMomentum )[ti.trp0];
    ++( *m_hIMLPT )[ti.trpT];
    ++( *m_hIMLRegion )[ti.trRegionM2];
  }
  if ( level > 1 ) {
    ++( *m_hNShared_IML )[mpi.trNShared];
    ++( *m_hDist2_IML )[mti.trDist2];
    ++( *m_hProbMu_IML )[exp( mpi.trMuonLhd )];
    ++( *m_hProbNMu_IML )[exp( mpi.trNMuonLhd )];
    ++( *m_hMuDLL_IML )[mpi.trMuonLhd - mpi.trNMuonLhd];
    ++( *m_hNIMLvsXM2 )[ti.trackX[1]];
    ++( *m_hNIMLvsYM2 )[ti.trackY[1]];
    ++( *m_hDist2_IML_R )[ti.trRegionM2][mti.trDist2];
    ++( *m_hProbMu_IML_R )[ti.trRegionM2][exp( mpi.trMuonLhd )];
  }
  if ( level > 2 ) {
    ++( *m_hIM_IML )[mpi.trIsMuon];
    ++( *m_hDLL_IML_R )[ti.trRegionM2][mpi.trMuonLhd - mpi.trNMuonLhd];
  }
  if ( level > 3 ) {
    ++( *m_hChi2_IML )[mti.trChi2];
    ++( *m_hQuality_IML )[mti.trquality];
    ++( *m_hCLQuality_IML )[mti.trCLquality];
    ++( *m_hCLArrival_IML )[mti.trCLarrival];
    ( *m_hProbMuvsP_IML )[std::abs( ti.trp0 )] += exp( mpi.trMuonLhd );
    ( *m_hNProbMuvsP_IML )[std::abs( ti.trp0 )] += exp( mpi.trNMuonLhd );
  }
}
//=====================================================================
//  Fill Plots for IsMuon Candidates
//====================================================================
void LHCb::MuonPIDChecker::fillIMPlots( int level, TrackInfo const& ti, MuonPIDInfo const& mpi,
                                        MuonTrackInfo const& mti ) const {
  if ( exp( mpi.trMuonLhd ) < m_monitCutValues[3] ) ++m_nbIsMuonProb;
  if ( mpi.trMuonLhd - mpi.trNMuonLhd > m_monitCutValues[5] ) ++m_nbIsMuonDLL;
  if ( mpi.trNShared < m_monitCutValues[7] ) ++m_nbIsMuonNShared;
  if ( level > 1 ) {
    ++( *m_hIMMomentum )[ti.trp0];
    ++( *m_hIMPT )[ti.trpT];
    ++( *m_hIMRegion )[ti.trRegionM2];
    ++( *m_hNShared_IM )[mpi.trNShared];
    ++( *m_hDist2_IM )[mti.trDist2];
    ++( *m_hDist2_IM_R )[ti.trRegionM2][mti.trDist2];
    ++( *m_hProbMu_IM )[exp( mpi.trMuonLhd )];
    ++( *m_hProbMu_IM_R )[ti.trRegionM2][exp( mpi.trMuonLhd )];
    ++( *m_hProbNMu_IM )[exp( mpi.trNMuonLhd )];
    ++( *m_hMuDLL_IM )[mpi.trMuonLhd - mpi.trNMuonLhd];
    ++( *m_hNIMvsXM2 )[ti.trackX[1]];
    ++( *m_hNIMvsYM2 )[ti.trackY[1]];
  }
  if ( level > 2 ) { ++( *m_hDLL_IM_R )[ti.trRegionM2][mpi.trMuonLhd - mpi.trNMuonLhd]; }
  if ( level > 3 ) {
    ++( *m_hChi2_IM )[mti.trChi2];
    ++( *m_hQuality_IM )[mti.trquality];
    ++( *m_hCLQuality_IM )[mti.trCLquality];
    ++( *m_hCLArrival_IM )[mti.trCLarrival];
    ( *m_hProbMuvsP_IM )[std::abs( ti.trp0 )] += exp( mpi.trMuonLhd );
    ( *m_hNProbMuvsP_IM )[std::abs( ti.trp0 )] += exp( mpi.trNMuonLhd );
  }
  return;
}
//=====================================================================
//  Fill Plots for Hit Multiplicities
//====================================================================
void LHCb::MuonPIDChecker::fillHitMultPlots( int level, TrackInfo const& ti, MuonTrackInfo const& mti ) const {
  GaudiAlg::HistoID hname;
  std::string       htitle;
  if ( level > 2 ) {
    std::vector<unsigned int> nhitsfoiS( Detector::Muon::nStations );
    unsigned int              NhitsFOI = 0;
    for ( unsigned int i = 0; i < Detector::Muon::nStations; i++ ) {
      for ( unsigned int j = 0; j < Detector::Muon::nRegions; j++ ) {
        nhitsfoiS[i] += mti.trnhitsfoi[i * Detector::Muon::nRegions + j];
      }
      NhitsFOI += nhitsfoiS[i];
      ( *m_hAvNHhitsFOIvsR_M )[i + 1][ti.trRegionM2] += nhitsfoiS[i];
      if ( level > 3 ) {
        ++( *m_hNHhitsFOIvsR_M )[i + 1][{ ti.trRegionM2, nhitsfoiS[i] }];
        ++( *m_hNhitsFOIvsX_M )[i + 1][{ nhitsfoiS[i], ti.trackX[i] }];
        ++( *m_hNhitsFOIvsY_M )[i + 1][{ nhitsfoiS[i], ti.trackY[i] }];
        ( *m_hAvNhitsFOIvsX_M )[i + 1][ti.trackX[i]] += nhitsfoiS[i];
        ( *m_hAvNhitsFOIvsY_M )[i + 1][ti.trackY[i]] += nhitsfoiS[i];
      }
    }
    ( *m_hAvTotNhitsFOIvsR )[ti.trRegionM2] += NhitsFOI;
    ( *m_hAvTotNhitsFOIvsX )[ti.trackX[1]] += NhitsFOI;
    ( *m_hAvTotNhitsFOIvsY )[ti.trackY[1]] += NhitsFOI;
  }
}
//=====================================================================
//  Fill MuonPID Info
//====================================================================
TrackInfo LHCb::MuonPIDChecker::getTrackInfo( DeMuonDetector const& muonDet, LHCb::Track const& track ) const {
  TrackInfo res;
  // get state in zero position
  const LHCb::State& stateP0 = track.firstState();
  if ( std::abs( stateP0.qOverP() ) > 0.001 / Gaudi::Units::GeV ) {
    res.trp0 = ( 1. / stateP0.qOverP() ) / Gaudi::Units::GeV;
    res.trpT = ( stateP0.pt() / Gaudi::Units::GeV ) * (int)( res.trp0 / fabs( res.trp0 ) );
  } else if ( stateP0.qOverP() > 0. ) {
    res.trp0 = 1000. / Gaudi::Units::GeV;
    res.trpT = 1000. / Gaudi::Units::GeV;
  } else {
    res.trp0 = -1000. / Gaudi::Units::GeV;
    res.trpT = -1000. / Gaudi::Units::GeV;
  }
  // Extrapolate Tracks to get X,Y of extrapolation point in each station
  // get state closest to M1 for extrapolation
  const LHCb::State* state = &( track.closestState( 9450 ) );
  if ( !state ) {
    ++m_extrapolateFailed;
    return {};
  }
  // Project the state into the muon stations
  for ( unsigned int station = 0; station < Detector::Muon::nStations; station++ ) {
    // x(z') = x(z) + (dx/dz * (z' - z))
    auto dz = muonDet.getStationZ( station ) - state->z();
    res.trackX.push_back( state->x() + ( state->tx() * dz ) );
    res.trackY.push_back( state->y() + ( state->ty() * dz ) );
  }
  return res;
}
