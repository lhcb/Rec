/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/ODIN.h"
#include "FTDAQ/FTInfo.h"

namespace FTMonitoring {
  static constexpr uint16_t maxFillID = 3564;
}

template <typename OWNER, typename K, typename H, typename A>
void buildHistogram( OWNER owner, std::map<K, H>& h, K k, std::string name, std::string labels, A axis ) {
  h.emplace( std::piecewise_construct, std::forward_as_tuple( k ), std::forward_as_tuple( owner, name, labels, axis ) );
}

// TODO: move to LHCb
constexpr bool isIsolatedBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_08 );
}

constexpr bool isLeadingBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_09 );
}

constexpr bool isTrailingBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_10 );
}

constexpr bool isEmptyBeforeIsolatedBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_11 );
}

constexpr bool isEmptyAfterIsolatedBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_12 );
}

constexpr bool isEmptyBeforeLeadingBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_13 );
}

constexpr bool isEmptyAfterTrailingBX( std::uint16_t eventType ) {
  return eventType & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_14 );
}

template <typename T>
class Mutable {
  mutable T m_t;

public:
  template <typename... Args, typename = std::enable_if_t<std::is_constructible_v<T, Args...>>>
  Mutable( Args&&... args ) : m_t{ std::forward<Args>( args )... } {}

  template <typename Arg>
  decltype( auto ) operator[]( Arg&& arg ) const {
    return m_t[std::forward<Arg>( arg )];
  }
};
