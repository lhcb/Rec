/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <array>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

class FillingSchemeProvider {
public:
  // forward decl for source of filling scheme
  class FillingSchemeSourceBase;

private:
  /// type used for internal bookkeeping (bunch type ee/eb/be/bb, flags)
  using ExtendedBunchType = unsigned;
  /// internal table for filling scheme and flags
  using FillingSchemeVector = std::vector<ExtendedBunchType>;

public:
  /// model for filling scheme
  class FillingScheme {
  public:
    using BunchIDList = std::vector<unsigned>;
    /// bx types: ee (NoBeam), be (Beam1), eb (Beam2), bb (BeamCrossing)
    enum BXTypes { NoBeam = 0, Beam1 = 1, Beam2 = 2, BeamCrossing = 3 };
    /// return bx type for given bxid [1, ..., orbitLength()]

    /// an extent of consecutive bxids of the same type
    class TypedExtent {
    private:
      unsigned             m_first  = 0;
      unsigned             m_last   = 0;
      BXTypes              m_type   = NoBeam;
      const FillingScheme* m_parent = nullptr;

    public:
      constexpr TypedExtent() noexcept = default;
      constexpr TypedExtent( unsigned first, unsigned last, BXTypes bxtype, const FillingScheme& parent ) noexcept
          : m_first( first ), m_last( last ), m_type( bxtype ), m_parent( &parent ) {}
      constexpr TypedExtent( const TypedExtent& /* unused */ ) noexcept            = default;
      constexpr TypedExtent( TypedExtent&& /* unused */ ) noexcept                 = default;
      constexpr TypedExtent& operator=( const TypedExtent& /* unused */ ) noexcept = default;
      constexpr TypedExtent& operator=( TypedExtent&& /* unused */ ) noexcept      = default;

      constexpr unsigned             first() const noexcept { return m_first; }
      constexpr unsigned             last() const noexcept { return m_last; }
      constexpr BXTypes              bxtype() const noexcept { return m_type; }
      constexpr const FillingScheme& parent() const noexcept { return *m_parent; }
      constexpr unsigned             size() const noexcept {
        return ( m_first <= m_last ) ? ( m_last - m_first ) : ( m_last + ( m_parent->orbitLength() + 1 - m_first ) );
      }
      constexpr bool contains( unsigned bxid ) const noexcept {
        if ( m_first <= m_last ) {
          return m_first <= bxid && bxid < m_last;
        } else {
          // extent wraps around
          return m_last <= bxid || bxid < m_first;
        }
      }
      const TypedExtent& next() const noexcept {
        const auto& extns = m_parent->extents();
        std::size_t idx   = this - extns.data();
        if ( idx < extns.size() - 1 )
          ++idx;
        else
          idx = 0;
        return extns[idx];
      }
      const TypedExtent& prev() const noexcept {
        const auto& extns = m_parent->extents();
        std::size_t idx   = this - extns.data();
        if ( idx >= 1 )
          --idx;
        else
          idx = extns.size() - 1;
        return extns[idx];
      }
    };
    using TypedExtents = std::vector<TypedExtent>;

  private:
    friend class FillingSchemeProvider;
    enum Masks {
      MaskBXType     = 0x3,
      MaskLeading    = 0x4,
      MaskTailing    = 0x8,
      MaskLeadingBX1 = 0x10,
      MaskTailingBX1 = 0x20,
      MaskLeadingBX2 = 0x40,
      MaskTailingBX2 = 0x80,
    };
    enum Shifts {
      ShiftBXType     = 0,
      ShiftLeading    = 2,
      ShiftTailing    = 3,
      ShiftLeadingBX1 = 4,
      ShiftTailingBX1 = 5,
      ShiftLeadingBX2 = 6,
      ShiftTailingBX2 = 7,
    };
    constexpr static inline unsigned s_masksLeading[3] = {
        FillingScheme::Masks::MaskLeading, FillingScheme::Masks::MaskLeadingBX1, FillingScheme::Masks::MaskLeadingBX2 };
    constexpr static inline unsigned s_masksTailing[3] = {
        FillingScheme::Masks::MaskTailing, FillingScheme::Masks::MaskTailingBX1, FillingScheme::Masks::MaskTailingBX2 };

    FillingSchemeVector        m_table;
    BunchIDList                m_ee;
    BunchIDList                m_be;
    BunchIDList                m_eb;
    BunchIDList                m_bb;
    std::array<BunchIDList, 3> m_isolated;
    std::array<BunchIDList, 3> m_leading;
    std::array<BunchIDList, 3> m_tailing;
    TypedExtents               m_extents;

  public:
    FillingScheme( FillingSchemeVector&& fs );

    /// return orbit length in bunches
    unsigned orbitLength() const noexcept { return m_table.size(); }
    /// get list of empty-empty bunches
    const BunchIDList& getEE() const noexcept { return m_ee; }
    /// get list of beam1-empty bunches
    const BunchIDList& getBE() const noexcept { return m_be; }
    /// get list of empty-beam2 bunches
    const BunchIDList& getEB() const noexcept { return m_eb; }
    /// get list of beam-beam bunches
    const BunchIDList& getBB() const noexcept { return m_bb; }
    /// get list of isolated bunches (bxtype->bxtype->bb->bxtype->bxtype)
    const BunchIDList& getIsolated( BXTypes bxtype = NoBeam ) const noexcept {
      return m_isolated[static_cast<unsigned>( bxtype )];
    }
    /// get list of leading bunches (bxtype->bxtype->bb->bb)
    const BunchIDList& getLeading( BXTypes bxtype = NoBeam ) const noexcept {
      return m_leading[static_cast<unsigned>( bxtype )];
    }
    /// get list of tailing bunches (bb->bb->bxtype->bxtype)
    const BunchIDList& getTailing( BXTypes bxtype = NoBeam ) const noexcept {
      return m_tailing[static_cast<unsigned>( bxtype )];
    }
    /// return typed extents (contiguous ranges of bxid of same bx type)
    const TypedExtents& extents() const noexcept { return m_extents; }
    /// find extent containing bxid (may return nullptr if bxid invalid)
    const TypedExtent* findExtent( unsigned bxid ) const noexcept;

    BXTypes getBXType( unsigned bxid ) const noexcept {
      return static_cast<BXTypes>( ( m_table[bxid - 1] & Masks::MaskBXType ) >> Shifts::ShiftBXType );
    }

    /// return if leading bunch for given bxid [1, ..., orbitLength()]
    bool isLeading( unsigned bxid, BXTypes bxtype = NoBeam ) const noexcept {
      const auto mask = s_masksLeading[bxtype] | s_masksTailing[bxtype];
      return static_cast<bool>( s_masksLeading[bxtype] == ( m_table[bxid - 1] & mask ) );
    }

    /// return if tailing bunch for given bxid [1, ..., orbitLength()]
    bool isTailing( unsigned bxid, BXTypes bxtype = NoBeam ) const noexcept {
      const auto mask = s_masksLeading[bxtype] | s_masksTailing[bxtype];
      return static_cast<bool>( s_masksTailing[bxtype] == ( m_table[bxid - 1] & mask ) );
    }

    /// return if isolated bunch for given bxid [1, ..., orbitLength()]
    bool isIsolated( unsigned bxid, BXTypes bxtype = NoBeam ) const noexcept {
      const auto mask = s_masksLeading[bxtype] | s_masksTailing[bxtype];
      return static_cast<bool>( mask == ( m_table[bxid - 1] & mask ) );
    }

    bool isSpillover( unsigned bxid, unsigned n = 0, BXTypes bxtype = NoBeam ) const noexcept {
      int shiftedbxid = bxid - n - 1;
      if ( shiftedbxid <= 0 ) shiftedbxid += orbitLength();
      if ( !isTailing( shiftedbxid, bxtype ) ) return false;
      for ( int i = bxid; shiftedbxid != i; ) {
        if ( bxtype != getBXType( i ) ) return false;
        --i;
        if ( i <= 0 ) i += orbitLength();
      }
      return true;
    }

    bool isPrespill( unsigned bxid, unsigned n = 0, BXTypes bxtype = NoBeam ) const noexcept {
      unsigned shiftedbxid = bxid + n + 1;
      if ( shiftedbxid > orbitLength() ) shiftedbxid -= orbitLength();
      if ( !isLeading( shiftedbxid, bxtype ) ) return false;
      for ( unsigned i = bxid; shiftedbxid != i; ) {
        if ( bxtype != getBXType( i ) ) return false;
        ++i;
        if ( i > orbitLength() ) i -= orbitLength();
      }
      return true;
    }
  };

private:
  /// mutex to protect multithreaded access
  mutable std::mutex m_mtx;
  /// shared pointer to filling scheme
  mutable std::shared_ptr<FillingScheme> m_data{ nullptr };

  /// unique pointer to filling scheme source
  std::unique_ptr<FillingSchemeSourceBase> m_source{ nullptr };

  /// get filling scheme from data source
  FillingSchemeVector retrieveFillingScheme( /* pass through conditions */ ) const;
  /// analyse filling scheme before presenting it to clients
  static FillingSchemeVector analyseFillingScheme( FillingSchemeVector&& scheme );

public:
  // tags specifying from where filling scheme should be read
  struct FromFile {};       ///< tag: read from file
  struct FromDIM {};        ///< tag: read from DIM
  struct FromConditions {}; ///< tag: read from conditions

  /// construct with info from file
  FillingSchemeProvider( FromFile /* unused */, const std::string& filename );
  /// construct with info from DIM (ONLINE only!)
  FillingSchemeProvider( FromDIM /* unused */, const std::string& dns );
  /// construct with info from conditions (not implemented yet)
  FillingSchemeProvider( FromConditions /* unused */, ... /* still unspecified */ );
  /// destructor
  ~FillingSchemeProvider();

  /// obtain filling scheme
  std::shared_ptr<const FillingScheme> get( /* pass through conditions */ ) const;

  using BXTypes     = FillingScheme::BXTypes;
  using BunchIDList = FillingScheme::BunchIDList;
};

// vim: sw=4:tw=78:ft=cpp:et
