/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ODIN.h"
#include <Gaudi/Accumulators/Histogram.h>

class TAEHandler {

  using Histogram = Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float>;
  using Axis      = Gaudi::Accumulators::Axis<float>;

  bool         m_enableTAE;
  unsigned int m_maxTAEHalfWindow;

  mutable Histogram                                   m_TAE;
  mutable Histogram                                   m_TAEindex;
  mutable Histogram                                   m_TAEErrors;
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_tae_bunch_id_not_set;

  enum errors {
    NotTAE,
    Skipped,
    TAEDisabled,
    TAEBunchIdsNotSet,
    NUM_ERRORS,
  };

public:
  template <typename Owner>
  TAEHandler( Owner* owner, bool enableTAE, unsigned int maxTAEHalfWindow, std::string_view extraInfo )
      : m_enableTAE{ enableTAE }
      , m_maxTAEHalfWindow{ maxTAEHalfWindow }
      , m_TAE( owner, "TAEEvents", fmt::format( "TAE events{};TAE;Events", extraInfo ),
               Axis{ 2 * maxTAEHalfWindow + 1, -0.5f - maxTAEHalfWindow, maxTAEHalfWindow + 0.5f } )
      , m_TAEindex( owner, "TAEIndex", fmt::format( "TAE events{};TAE index;Events", extraInfo ),
                    Axis{ 2 * maxTAEHalfWindow + 1, -0.5f, 2 * maxTAEHalfWindow + 0.5f } )
      , m_TAEErrors{ owner, "TAEErrors", fmt::format( "TAE errors{};TAE errors;Events", extraInfo ),
                     Axis{ NUM_ERRORS, -0.5f, static_cast<float>( NUM_ERRORS ) - 0.5f } }
      , m_tae_bunch_id_not_set{ owner, "TAE bunch IDs are not set" } {}

  std::optional<int> fromOdin( const LHCb::ODIN& odin, LHCb::span<unsigned int> bunchIds ) const {
    if ( !m_enableTAE ) {
      ++m_TAEErrors[errors::TAEDisabled];
      return std::nullopt;
    }

    if ( !odin.isTAE() ) {
      ++m_TAEErrors[errors::NotTAE];
      return std::nullopt;
    }

    ++m_TAEindex[odin.timeAlignmentEventIndex()];

    if ( bunchIds.empty() ) {
      ++m_TAEErrors[errors::TAEBunchIdsNotSet];
      ++m_tae_bunch_id_not_set;
      return std::nullopt;
    }

    for ( const auto bunchId : bunchIds ) {
      int testIDX = static_cast<int>( odin.bunchId() ) - static_cast<int>( bunchId );
      if ( static_cast<unsigned int>( abs( testIDX ) ) <= m_maxTAEHalfWindow ) {
        ++m_TAE[testIDX];
        return testIDX;
      }
    }
    ++m_TAEErrors[errors::Skipped];
    return std::nullopt;
  }
};
