/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <algorithm>
#include <cassert>
#include <fstream>

#include "FillingSchemeProvider.h"

#ifdef USE_FT_FILLINGSCHEME_FROM_DIM
// for DIM
#  include <RTL/rtl.h>
#  include <dim/dic.h>
#endif // USE_FT_FILLINGSCHEME_FROM_DIM

/// abstract base for filling scheme sources
class FillingSchemeProvider::FillingSchemeSourceBase {
public:
  using FillingScheme                                                                   = std::vector<unsigned>;
  virtual ~FillingSchemeSourceBase()                                                    = default;
  virtual bool                           changed( /* pass through conditions */ ) const = 0;
  virtual std::shared_ptr<FillingScheme> data( /* pass through conditions */ ) const    = 0;
};

namespace {
#ifdef USE_FT_FILLINGSCHEME_FROM_DIM
  /// RAII style class to get the filling scheme from DIM
  class FillingSchemeFromDIM : public FillingSchemeProvider::FillingSchemeSourceBase {
  private:
    using FillingSchemeDIM = std::pair<std::string, std::string>;

    mutable std::mutex                     m_mutex{};
    mutable std::shared_ptr<FillingScheme> m_data{};
    mutable bool                           m_changed{ false };

    struct DP {
      int                   id{ 0 };
      time_t                last{ 0 };
      FillingSchemeFromDIM* algo{ nullptr };
      size_t*               size{ nullptr };
      std::string*          str{ nullptr };
    };

    DP   m_svcB1FS;
    DP   m_svcB2FS;
    long m_dns_ID{ 0L };

    std::string m_dns;

    FillingSchemeDIM m_raw{};

    /// DimInfo overload to process messages
    static void fs_str_handler( void* tag, void* address, int* size );

    void subscribe();
    void unsubscribe();

  public:
    FillingSchemeFromDIM( const std::string& dns ) : m_dns( dns ) { subscribe(); }
    virtual ~FillingSchemeFromDIM() override { unsubscribe(); }
    virtual bool changed() const override {
      auto _ = std::unique_lock{ m_mutex };
      return m_changed;
    }
    virtual std::shared_ptr<FillingScheme> data() const override;
  };

  std::shared_ptr<FillingSchemeFromDIM::FillingScheme> FillingSchemeFromDIM::data() const {
    auto _ = std::unique_lock{ m_mutex };
    if ( m_changed ) {
      const std::string& beam1 = m_raw.first;
      const std::string& beam2 = m_raw.second;
      // strip trailing zero characters ('\0', i.e. C-style string
      // termination)
      using size_type     = typename std::string::size_type;
      const size_type sz1 = beam1.find_last_not_of( "\0" );
      const size_type sz2 = beam2.find_last_not_of( "\0" );
      // validate strings from DIM (we actually got a string, and
      // the data has the right length)
      if ( sz1 != sz2 )
        throw std::runtime_error( "FillingSchemeProviderFromDIM::"
                                  "getRawFillingScheme: data "
                                  "from DIM unexpected: beams "
                                  "have different numbers of "
                                  "bunches" );
      std::shared_ptr<FillingScheme> fs = std::make_shared<FillingScheme>( sz1, 0u );
      // build raw filling scheme from DATA in DIM
      for ( size_type i = 0; sz1 != i; ++i ) {
        // validate strings from DIM: '0' and '1' are valid
        if ( ( '0' != beam1[i] && '1' != beam1[i] ) || ( '0' != beam2[i] && '1' != beam2[i] ) )
          throw std::runtime_error( "FillingSchemeProviderFromDIM::"
                                    "getRawFillingScheme: data "
                                    "from DIM is invalid" );
        ( *fs )[i] = unsigned( '1' == beam1[i] ) | ( unsigned( '1' == beam2[i] ) << 1 );
      }
      m_data    = fs;
      m_changed = false;
    }
    return m_data;
  }

  /// DimInfo overload to process messages
  void FillingSchemeFromDIM::fs_str_handler( void* tag, void* address, int* size ) {
    if ( tag && address && size && *size ) {
      int                       len = *size;
      FillingSchemeFromDIM::DP* dp  = *(FillingSchemeFromDIM::DP**)tag;
      auto                      _   = std::unique_lock{ dp->algo->m_mutex };
      const char*               val = (const char*)address;
      dp->last                      = ::time( 0 );
      dp->algo->m_changed           = true;
      dp->str->assign( val, val + len );
    }
  }

  /// subscribe to DIM to get notified of filling scheme
  void FillingSchemeFromDIM::subscribe() {
    if ( m_dns.empty() ) { m_dns = RTL::nodeNameShort(); }
    if ( 0 == m_dns_ID ) { m_dns_ID = ::dic_add_dns( m_dns.c_str(), ::dim_get_dns_port() ); }
    if ( 0 == m_svcB1FS.id ) {
      m_svcB1FS.last = 0;
      m_svcB1FS.algo = this;
      m_svcB1FS.str  = &m_raw.first;
      m_svcB1FS.id   = ::dic_info_service_dns( m_dns_ID, "LHC/B1FillingScheme", MONITORED, 0, 0, 0, fs_str_handler,
                                               (long)&m_svcB1FS, 0, 0 );
    }
    if ( 0 == m_svcB2FS.id ) {
      m_svcB2FS.last = 0;
      m_svcB2FS.algo = this;
      m_svcB2FS.str  = &m_raw.second;
      m_svcB2FS.id   = ::dic_info_service_dns( m_dns_ID, "LHC/B2FillingScheme", MONITORED, 0, 0, 0, fs_str_handler,
                                               (long)&m_svcB2FS, 0, 0 );
    }
  }

  /// unsubscribe from DIM, no longer get notified of filling scheme
  void FillingSchemeFromDIM::unsubscribe() {
    if ( 0 != m_svcB1FS.id ) {
      ::dic_release_service( m_svcB1FS.id );
      m_svcB1FS.id = 0;
    }
    if ( 0 == m_svcB2FS.id ) {
      ::dic_release_service( m_svcB1FS.id );
      m_svcB1FS.id = 0;
    }
  }
#else  // USE_FT_FILLINGSCHEME_FROM_DIM
  /// simple mockup class when DIM is unavailable
  class FillingSchemeFromDIM : public FillingSchemeProvider::FillingSchemeSourceBase {
  public:
    FillingSchemeFromDIM( const std::string& /* unused */ ) {
      // do not throw in the constructor, since it's needed for building
      // python configurables
    }
    virtual bool changed() const override {
      throw std::runtime_error( "FillingSchemeDIM::changed(): "
                                "Only supported in ONLINE context." );
    }
    virtual std::shared_ptr<FillingScheme> data() const override {
      throw std::runtime_error( "FillingSchemeDIM::data(): "
                                "Only supported in ONLINE context." );
    }
  };
#endif // USE_FT_FILLINGSCHEME_FROM_DIM

  class FillingSchemeFromFile : public FillingSchemeProvider::FillingSchemeSourceBase {
  private:
    static constexpr unsigned      NBunches = 3564;
    std::shared_ptr<FillingScheme> m_data;

  public:
    FillingSchemeFromFile( const std::string& filename );
    virtual bool                           changed() const override { return false; }
    virtual std::shared_ptr<FillingScheme> data() const override { return m_data; }
  };

  FillingSchemeFromFile::FillingSchemeFromFile( const std::string& filename )
      : m_data( std::make_shared<FillingScheme>( NBunches, 0u ) ) {
    // open file
    std::ifstream ifile( filename, std::ios::binary );
    auto          fs = m_data.get();
    // process lines
    unsigned nlines = 0;
    for ( std::string line; std::getline( ifile, line ); ++nlines ) {
      // read too many lines
      if ( nlines > NBunches )
        throw std::runtime_error( "FillingSchemeProvider::readFillingScheme: "
                                  "too many lines in file" );
      std::size_t         pos = 0;
      const unsigned long tmp = std::stol( line, &pos );
      // validate: bunch type between 0 and 3
      if ( tmp > 3 )
        throw std::runtime_error( "FillingSchemeProvider::readFillingScheme: "
                                  "invalid bunch type read from file" );
      ( *fs )[nlines] = tmp;
      // validate: no other crap on the line
      if ( pos < ( line.size() - 1 ) )
        throw std::runtime_error( "FillingSchemeProvider::readFillingScheme: "
                                  "trailing characters on line after "
                                  "bunch type while reading from file" );
    }
    // validate: need to have enough lines for filling scheme
    if ( NBunches != nlines )
      throw std::runtime_error( "FillingSchemeProvider::readFillingScheme: too "
                                "few lines in file" );
  }

  /// how to read from conditions
  class FillingSchemeFromConditions : public FillingSchemeProvider::FillingSchemeSourceBase {
  public:
    FillingSchemeFromConditions() {
      // do not throw in the constructor, since it's needed for building
      // python configurables
    }
    virtual bool changed() const override {
      throw std::runtime_error( "FillingSchemeConditions::changed(): "
                                "Not implemented yet." );
    }
    virtual std::shared_ptr<FillingScheme> data() const override {
      throw std::runtime_error( "FillingSchemeConditions::data(): "
                                "Not implemented yet." );
    }
  };
} // namespace

FillingSchemeProvider::FillingSchemeProvider( FillingSchemeProvider::FromFile /* unused */,
                                              const std::string& filename )
    : m_source( std::make_unique<FillingSchemeFromFile>( filename ) ) {}

FillingSchemeProvider::FillingSchemeProvider( FillingSchemeProvider::FromDIM /* unused */, const std::string& dns )
    : m_source( std::make_unique<FillingSchemeFromDIM>( dns ) ) {}

FillingSchemeProvider::FillingSchemeProvider( FillingSchemeProvider::FromConditions /* unused */,
                                              ... /* to be specified */ )
    : m_source( std::make_unique<FillingSchemeFromConditions>() ) {}

FillingSchemeProvider::~FillingSchemeProvider() {}

FillingSchemeProvider::FillingScheme::FillingScheme( FillingSchemeProvider::FillingSchemeVector&& fs )
    : m_table( std::move( fs ) ) {
  m_ee.reserve( std::count_if( m_table.begin(), m_table.end(),
                               []( unsigned code ) { return NoBeam == ( code & Masks::MaskBXType ); } ) );
  m_be.reserve( std::count_if( m_table.begin(), m_table.end(),
                               []( unsigned code ) { return Beam1 == ( code & Masks::MaskBXType ); } ) );
  m_eb.reserve( std::count_if( m_table.begin(), m_table.end(),
                               []( unsigned code ) { return Beam2 == ( code & Masks::MaskBXType ); } ) );
  m_bb.reserve( std::count_if( m_table.begin(), m_table.end(),
                               []( unsigned code ) { return BeamCrossing == ( code & Masks::MaskBXType ); } ) );
  for ( unsigned bxtype = 0; 3 != bxtype; ++bxtype ) {
    m_leading[bxtype].reserve( std::count_if( m_table.begin(), m_table.end(), [&bxtype]( unsigned code ) {
      return BeamCrossing == ( code & Masks::MaskBXType ) && ( code & s_masksLeading[bxtype] ) &&
             !( code & s_masksTailing[bxtype] );
    } ) );
    m_tailing[bxtype].reserve( std::count_if( m_table.begin(), m_table.end(), [&bxtype]( unsigned code ) {
      return BeamCrossing == ( code & Masks::MaskBXType ) && !( code & s_masksLeading[bxtype] ) &&
             ( code & s_masksTailing[bxtype] );
    } ) );
    m_isolated[bxtype].reserve( std::count_if( m_table.begin(), m_table.end(), [&bxtype]( unsigned code ) {
      return BeamCrossing == ( code & Masks::MaskBXType ) && ( code & s_masksLeading[bxtype] ) &&
             ( code & s_masksTailing[bxtype] );
    } ) );
  }
  // work out upper bound for how many extents there are
  {
    unsigned last = 4;
    m_extents.reserve( std::count_if( m_table.begin(), m_table.end(), [&last]( unsigned code ) {
      const unsigned bxtype = code & Masks::MaskBXType;
      const bool     retVal = last != bxtype;
      last                  = bxtype;
      return retVal;
    } ) );
  }

  using size_type = typename FillingSchemeVector::size_type;
  for ( size_type i = 0; m_table.size() != i; ++i ) {
    const unsigned info = m_table[i];
    switch ( info & Masks::MaskBXType ) {
    case NoBeam:
      m_ee.push_back( i + 1 );
      break;
    case Beam1:
      m_be.push_back( i + 1 );
      break;
    case Beam2:
      m_eb.push_back( i + 1 );
      break;
    case BeamCrossing:
      m_bb.push_back( i + 1 );
      for ( unsigned bxtype = 0; 3 != bxtype; ++bxtype ) {
        if ( info & s_masksLeading[bxtype] ) {
          if ( !( info & s_masksTailing[bxtype] ) ) {
            m_leading[bxtype].push_back( i + 1 );
          } else {
            m_isolated[bxtype].push_back( i + 1 );
          }
        } else if ( info & s_masksTailing[bxtype] ) {
          m_tailing[bxtype].push_back( i + 1 );
        }
      }
      break;
    }
  }

  // work out contiguous ranges of same bx type (extents)
  {
    unsigned   first      = 0;
    unsigned   prev       = m_table.back() & Masks::MaskBXType;
    const bool wraparound = prev == ( m_table.front() & Masks::MaskBXType );
    const auto it         = std::find_if( m_table.begin(), m_table.end(),
                                          [prev]( unsigned code ) { return ( code & Masks::MaskBXType ) != prev; } );
    for ( size_type i = it - m_table.begin(); m_table.size() != i; ++i ) {
      const unsigned info = m_table[i] & Masks::MaskBXType;
      if ( prev != info ) {
        if ( first || !wraparound ) {
          // new extent starts
          m_extents.emplace_back( first + 1, i + 1, static_cast<BXTypes>( m_table[first] & Masks::MaskBXType ), *this );
        }
        first = i;
      }
      prev = info;
    }
    if ( wraparound ) {
      if ( m_extents.empty() ) {
        // one single extent spanning all bunch IDs
        m_extents.emplace_back( 1, orbitLength() + 1, static_cast<BXTypes>( m_table.front() & Masks::MaskBXType ),
                                *this );
      } else {
        // last extent wraps around
        m_extents.emplace_back( first + 1, it - m_table.begin() + 1,
                                static_cast<BXTypes>( m_table.front() & Masks::MaskBXType ), *this );
      }
    }
  }
  // set to true if you are debugging the extent building code
  constexpr bool verify_extents = false;
  if ( verify_extents ) {
    for ( [[maybe_unused]] unsigned i = m_extents.size() - 1, j = 0; m_extents.size() != j; ++j ) {
      // check extents are contiguous, and change bxtype, and that we
      // can navigate to next/previous extent
      assert( m_extents[i].last() == m_extents[j].first() );
      assert( m_extents[i].bxtype() != m_extents[j].bxtype() );
      assert( &m_extents[i].next() == &m_extents[j] );
      assert( &m_extents[i] == &m_extents[j].prev() );
      i = j;
    }
    for ( unsigned i = 1; i <= orbitLength(); ++i ) {
      // check that we can find an extent for each valid bxid, that the
      // extent found actually contains that bxid, and that the bxtype
      // matches
      [[maybe_unused]] const TypedExtent* extn = findExtent( i );
      assert( extn );
      assert( extn->contains( i ) );
      assert( extn->bxtype() == getBXType( i ) );
    }
  }
}

FillingSchemeProvider::FillingSchemeVector FillingSchemeProvider::retrieveFillingScheme() const {
  return analyseFillingScheme( FillingSchemeVector( *( m_source->data() ) ) );
}

FillingSchemeProvider::FillingSchemeVector
FillingSchemeProvider::analyseFillingScheme( FillingSchemeProvider::FillingSchemeVector&& source ) {
  FillingSchemeVector retVal( std::move( source ) );
  // okay, done reading, figure out leading/trailing info
  unsigned typelast = retVal[retVal.size() - 1] & FillingScheme::Masks::MaskBXType;
  unsigned typecurr = retVal[0] & FillingScheme::Masks::MaskBXType;
  for ( unsigned i = 0; retVal.size() != i; ++i ) {
    const unsigned last     = ( 0 == i ) ? ( retVal.size() - 1 ) : ( i - 1 );
    const unsigned next     = ( ( retVal.size() - 1 ) == i ) ? 0 : ( i + 1 );
    const unsigned typenext = retVal[next] & FillingScheme::Masks::MaskBXType;
    if ( typecurr != typelast ) {
      for ( unsigned bxtype = 0; 3 != bxtype; ++bxtype ) {
        if ( 3 == typelast && bxtype == typecurr && bxtype == typenext ) {
          retVal[last] |= FillingScheme::s_masksTailing[bxtype];
        }
      }
    }
    if ( typecurr != typenext ) {
      for ( unsigned bxtype = 0; 3 != bxtype; ++bxtype ) {
        if ( bxtype == typelast && bxtype == typecurr && 3 == typenext ) {
          retVal[next] |= FillingScheme::s_masksLeading[bxtype];
        }
      }
    }
    typelast = typecurr;
    typecurr = typenext;
  }
  // all done!
  return retVal;
}

std::shared_ptr<const FillingSchemeProvider::FillingScheme> FillingSchemeProvider::get() const {
  auto _ = std::unique_lock{ m_mtx };
  if ( m_source->changed() || !m_data ) {
    // we don't have the filling scheme yet, so get it
    m_data = std::make_shared<FillingScheme>( retrieveFillingScheme() );
  }
  return m_data;
}

const FillingSchemeProvider::FillingScheme::TypedExtent*
FillingSchemeProvider::FillingScheme::findExtent( unsigned bxid ) const noexcept {
  if ( 0 < bxid && bxid <= orbitLength() ) {
    const auto it = std::lower_bound( m_extents.begin(), m_extents.end(), bxid,
                                      []( const TypedExtent& e, unsigned bxid ) { return e.first() < bxid; } );
    if ( m_extents.end() != it ) {
      // found something (in the non-wrap-around extents)
      if ( m_extents.begin() != it && ( it - 1 )->first() <= bxid && bxid < ( it - 1 )->last() ) {
        // either bxid is in the extent before it...
        return m_extents.data() + ( it - 1 - m_extents.begin() );
      } else if ( it->first() == bxid ) {
        // or hit first bxid of an extent...
        return m_extents.data() + ( it - m_extents.begin() );
      }
    }
    // check wrap-around extent (if needed)
    const auto& last = m_extents.back();
    if ( last.first() > last.last() && ( last.first() <= bxid || bxid < last.last() ) ) {
      return m_extents.data() + ( m_extents.size() - 1 );
    }
  }
  // nothing found
  return nullptr;
}

// vim: sw=4:tw=78:ft=cpp:et
