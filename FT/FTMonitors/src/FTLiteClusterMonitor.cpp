/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/FTLiteCluster.h"
#include "Event/ODIN.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/GaudiException.h>

#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"

/** @class FTLiteClusterMonitor FTLiteClusterMonitor.cpp
 *
 *
 *  @author Hendrik Jage
 *  Inspired by FTClusterMonitor from  Roel Aaij,
 *  adapted by Lex Greeven, Johannes Heuel
 *  @date   2023-06-26
 */
using FTLiteClusters  = LHCb::FTLiteCluster::FTLiteClusters;
using ODIN            = LHCb::ODIN;
namespace GA          = Gaudi::Accumulators;
namespace FTConstants = LHCb::Detector::FT;

class FTLiteClusterMonitor : public LHCb::Algorithm::Consumer<void( const FTLiteClusters&, const ODIN& )> {
public:
  FTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void operator()( const FTLiteClusters&, const ODIN& ) const override;

  using Histogram1D        = GA::Histogram<1, GA::atomicity::full, float>;
  using Histogram2D        = GA::Histogram<2, GA::atomicity::full, float>;
  using ProfileHistogram   = GA::ProfileHistogram<1, GA::atomicity::full, float>;
  using ProfileHistogram2D = GA::ProfileHistogram<2, GA::atomicity::full, float>;
  using Axis               = GA::Axis<float>;

  // Event level histograms
private:
  // FIXME: Create here directly, how to treat mutable?
  const Axis a_bxid = Axis{ 4096, // *TODO: Is there a constant instead? // FIXME: constexpr?
                            -0.5f, 4096 - 0.5f };

  mutable Histogram2D m_clustersPerEvent_bxid{ this, "ClustersPerEventPerBX",
                                               "Clusters Per Event per BX;BX ID;Clusters / BX", a_bxid,
                                               Axis{ 351, -50.0f, 35050.0f } };
  mutable Histogram1D m_clusters_bxid_c{ this, "ClustersPerBunchCrossingCSide",
                                         "Bunch crossing ID (C-side);BX ID;Clusters", a_bxid };
  mutable Histogram1D m_clusters_bxid_a{ this, "ClustersPerBunchCrossingASide",
                                         "Bunch crossing ID (A-side);BX ID;Clusters", a_bxid };
  mutable Histogram1D m_clusters_bxid{ this, "ClustersPerBunchCrossing", "Bunch crossing ID;BX ID;Clusters", a_bxid };
  mutable Histogram1D m_events_bxid{ this, "EventsPerBunchCrossing", "Bunch crossing ID; BX ID; Events", a_bxid };
  mutable Histogram1D m_clusters{ this, "nClusters", "Number of clusters;Clusters/event; Events",
                                  Axis{ 2700, -0.5f, 27000 - 0.5f } };

  enum struct ColType { BB, B1, B2, EE, ERROR };

  // - - - - - - - - - - -
  // QuarterSiPMHistograms FIXME: DetectorHistogram?
  // - - - - - - - - - - -
  struct QuarterSiPMHistograms {
    QuarterSiPMHistograms( FTLiteClusterMonitor* owner, std::string name_2D, std::string name_profile,
                           std::string name_slices );
    void fill( const LHCb::Detector::FTChannelID& channel_id, float variable = 0 );

    // Name/histogram path
    const std::string m_name;

    // Settings
    const bool m_plot_2D;
    const bool m_plot_profile;
    const bool m_plot_slices;

    // Plot options
    static constexpr int   m_v_gap    = 1;   // number empty bins on top/bottom
    static constexpr int   m_h_gap    = 12;  // number empty bins on left/right
    static constexpr int   m_l_gap    = 0;   // number empty bins between layers
    static constexpr float m_l_center = 0.5; // central position of 1st layer

    // Transformation factor
    static constexpr float m_sipm_to_module   = 1. / FTConstants::nSiPMsPerModule;
    static constexpr float m_quarter_to_layer = 1. / ( FTConstants::nHalfLayers + m_l_gap );

    // Axis
    const Axis a_sipm_per_module = Axis{ 2u * ( FTConstants::nMaxSiPMsPerQuarter + m_h_gap ),
                                         -( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module ),
                                         +( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module ) };
    const Axis a_quarter_per_layer =
        Axis{ FTConstants::nHalfLayers * FTConstants::nLayersTotal // #half layers
                  + m_l_gap * ( FTConstants::nLayersTotal - 1 )    // #gaps between layers
                  + 2 * m_v_gap,                                   // #empty bins on top+bottom
              -m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center,
              +m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center + FTConstants::nLayersTotal - 1 };

    // Histograms
    std::optional<Histogram2D>                              m_clusters;
    std::optional<ProfileHistogram2D>                       m_profile;
    std::map<std::tuple<size_t, size_t, bool>, Histogram1D> m_clusters_TLY;
    // FIXME: Split 1D and 2D, const axis/helper function?
  };

  // - - - - - - - - - - -
  // ChannelIdxHistograms
  // - - - - - - - - - - -
  struct ChannelIdxHistograms {
    ChannelIdxHistograms( FTLiteClusterMonitor* owner, bool TLQ, std::string folder = "" );
    void fill( const LHCb::Detector::FTChannelID& channel_id );

    // Axis
    const Axis a_quarter_channel_idx =
        Axis{ FTConstants::nMaxChannelsPerQuarter, -0.5, FTConstants::nMaxChannelsPerQuarter - 0.5f };
    const Axis a_quarter_channel_idx_sipm = Axis{ FTConstants::nMaxChannelsPerQuarter / FTConstants::nChannels, -0.5,
                                                  FTConstants::nMaxChannelsPerQuarter - 0.5f };

    // Plot TLQ histograms?
    const bool m_TLQ;

    // Histograms
    mutable QuarterSiPMHistograms m_quarter_sipm_hists;
    std::unique_ptr<Histogram1D>  m_channel;
    std::unique_ptr<Histogram1D>  m_SiPM;
    // FIXME: Global quarter index, naming when constructing
    // FIXME: Struct with histograms per quarter -> std::array of struct
    std::map<std::tuple<size_t, size_t, size_t>, Histogram1D> m_TLQ_channel;
    std::map<std::tuple<size_t, size_t, size_t>, Histogram1D> m_TLQ_SiPM;
  };

  // - - - - - - - - - - - - - -
  // ClustersPerEventHistograms
  // - - - - - - - - - - - - - -
  struct ClustersPerEventHistograms {
    ClustersPerEventHistograms( FTLiteClusterMonitor* owner, std::string folder = "" );
    void reset();
    void update( const LHCb::Detector::FTChannelID& channel_id );
    void fill();

    // Histograms
    std::unique_ptr<Histogram2D>      m_ClustersPerEventPerSiPM;
    std::unique_ptr<ProfileHistogram> m_ClustersPerEventPerSiPMProfile;

    // Arrays
    std::array<unsigned int, FTConstants::nSiPMsTotal> m_clustersPerSiPM{}; // FIXME: Move to operator?
  };

  // -------------------------------------------------------------

private:
  mutable GA::Counter<> m_nCalls{ this, "number of calls" };

  const std::set<ColType> m_colTypeHists = { ColType::BB, ColType::B1, ColType::B2, ColType::EE };

  // structs
  mutable ChannelIdxHistograms       m_channel_idx_hists;
  mutable ClustersPerEventHistograms m_clusters_per_event_hists;

  // per collision type
  mutable std::map<ColType, Histogram1D>                m_colType_clusters;
  mutable std::map<ColType, ChannelIdxHistograms>       m_colType_channel_idx_hists;
  mutable std::map<ColType, ClustersPerEventHistograms> m_colType_clusters_per_event_hists;
};
// -------------------------------------------------------------

// - - - - - - - - - - -
// ChannelIdxHistograms
// - - - - - - - - - - -
FTLiteClusterMonitor::ChannelIdxHistograms::ChannelIdxHistograms( FTLiteClusterMonitor* owner, bool TLQ,
                                                                  std::string folder )
    : m_TLQ( TLQ )
    , m_quarter_sipm_hists( owner, folder + "Clusters_QuarterVsSiPM", "", folder == "" ? "TLY_Clusters_SiPM/" : "" ) {
  m_channel = std::make_unique<Histogram1D>(
      owner, folder + "ClustersPerQuarterChannelIdx",
      "Clusters per quarter channel Idx;Quarter channel Idx;Clusters (per channel)", a_quarter_channel_idx );
  m_SiPM = std::make_unique<Histogram1D>( owner, folder + "ClustersPerQuarterChannelIdx_SiPM",
                                          "Clusters per quarter channel Idx;Quarter channel Idx;Clusters (per SiPM)",
                                          a_quarter_channel_idx_sipm );

  if ( m_TLQ ) {
    // Histograms per station, layer and quarter
    for ( size_t station = 1; station <= FTConstants::nStations; ++station ) {
      for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
        for ( size_t quarter = 0; quarter < FTConstants::nQuarters; ++quarter ) {
          std::string TLQ = fmt::format( "T{}L{}Q{}", station, layer, quarter );
          m_TLQ_channel.emplace( std::piecewise_construct, std::forward_as_tuple( station, layer, quarter ),
                                 std::forward_as_tuple( owner, folder + "TLQ_Clusters_Channel/" + TLQ,
                                                        "Clusters per quarter channel idx in " + TLQ +
                                                            ";Quarter channel idx;Clusters (per channel)",
                                                        a_quarter_channel_idx ) );

          m_TLQ_SiPM.emplace( std::piecewise_construct, std::forward_as_tuple( station, layer, quarter ),
                              std::forward_as_tuple( owner, folder + "TLQ_Clusters_SiPM/" + TLQ,
                                                     "Clusters per quarter channel idx in " + TLQ +
                                                         ";Quarter channel idx;Clusters (per SiPM)",
                                                     a_quarter_channel_idx_sipm ) );
        }
      }
    }
  }
}

void FTLiteClusterMonitor::ChannelIdxHistograms::fill( const LHCb::Detector::FTChannelID& channel_id ) {

  // ChannelIdx
  const auto quarter_channel_idx = channel_id.localChannelIdx_quarter();

  // fill histograms
  ++( *m_channel )[quarter_channel_idx];
  ++( *m_SiPM )[quarter_channel_idx];

  if ( m_TLQ ) {
    const auto station     = static_cast<unsigned int>( channel_id.station() );
    const auto layer_idx   = static_cast<unsigned int>( channel_id.layer() );
    const auto quarter_idx = static_cast<unsigned int>( channel_id.quarter() );

    ++m_TLQ_channel.at( { station, layer_idx, quarter_idx } )[quarter_channel_idx];
    ++m_TLQ_SiPM.at( { station, layer_idx, quarter_idx } )[quarter_channel_idx];
  }

  m_quarter_sipm_hists.fill( channel_id );
}

// - - - - - - - - - - -
// QuarterSiPMHistograms
// - - - - - - - - - - -
FTLiteClusterMonitor::QuarterSiPMHistograms::QuarterSiPMHistograms( FTLiteClusterMonitor* owner, std::string name_2D,
                                                                    std::string name_profile, std::string name_slices )
    // only histograms with a name are created
    : m_plot_2D( !name_2D.empty() ), m_plot_profile( !name_profile.empty() ), m_plot_slices( !name_slices.empty() ) {
  if ( m_plot_2D ) {
    m_clusters.emplace( owner, name_2D, "SiPMNumber vs Quarter ID;Module;Layer", a_sipm_per_module,
                        a_quarter_per_layer );
  }
  if ( m_plot_profile ) {
    m_profile.emplace( owner, name_profile, "SiPMNumber vs Quarter ID;Module;Layer", a_sipm_per_module,
                       a_quarter_per_layer );
  }

  if ( m_plot_slices ) {
    for ( size_t station = 1; station <= FTConstants::nStations; ++station ) {
      for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
        for ( bool y_up : { true, false } ) {
          std::string TLY_title = fmt::format( "T{}L{} (y {} 0)", station, layer, y_up ? ">" : "<" );
          std::string TLY_name  = fmt::format( "T{}L{}{}", station, layer, y_up ? "U" : "D" );
          m_clusters_TLY.emplace(
              std::piecewise_construct, std::forward_as_tuple( station, layer, y_up ),
              std::forward_as_tuple( owner, name_slices + TLY_name,
                                     "Clusters per module in " + TLY_title + ";Module;Clusters (per SiPM)",
                                     a_sipm_per_module ) );
        }
      }
    }
  }
}

void FTLiteClusterMonitor::QuarterSiPMHistograms::fill( const LHCb::Detector::FTChannelID& channel_id,
                                                        float                              variable ) {
  const auto quarter_sipm_idx = channel_id.localSiPMIdx_quarter();
  const auto global_layer_idx = channel_id.globalLayerIdx();
  const auto station          = static_cast<unsigned int>( channel_id.station() );
  const auto layer_idx        = static_cast<unsigned int>( channel_id.layer() );
  const auto quarter_idx      = static_cast<unsigned int>( channel_id.quarter() );

  // transform coordinates
  // * quarter_sipm_idx -> module
  double x = ( quarter_sipm_idx + 0.5f ) * m_sipm_to_module;

  if ( quarter_idx % 2 == 0 ) { // mirror C Side
    x *= -1;
  }

  // * quarter_idx -> staggered layer
  const double y = global_layer_idx + m_l_center + 0.5 * m_quarter_to_layer * ( quarter_idx < 2 ? -1 : 1 );

  // upper or lower half?
  const bool y_up = ( quarter_idx < 2 ? false : true );

  // fill histograms
  if ( m_plot_2D ) { ++( *m_clusters )[{ x, y }]; }
  if ( m_plot_profile ) { ( *m_profile )[{ x, y }] += variable; }
  if ( m_plot_slices ) { ++m_clusters_TLY.at( { station, layer_idx, y_up } )[x]; }
}

// ClustersPerEventHistograms
// - - - - - - - - - - - - - -
FTLiteClusterMonitor::ClustersPerEventHistograms::ClustersPerEventHistograms( FTLiteClusterMonitor* owner,
                                                                              std::string           folder ) {

  m_ClustersPerEventPerSiPM = std::make_unique<Histogram2D>(
      owner, folder + "ClustersPerEventPerSiPM", "Clusters Per Event per SiPM;SiPM;Clusters / Event",
      Axis{ FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f }, Axis{ 32, -0.5f, 32 - 0.5f } );

  m_ClustersPerEventPerSiPMProfile = std::make_unique<ProfileHistogram>(
      owner, folder + "ClustersPerEventPerSiPMProfile", "Clusters Per Event per SiPM;SiPM;#LTClusters / Event#GT",
      Axis{ FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f } );

  reset();
}

void FTLiteClusterMonitor::ClustersPerEventHistograms::reset() {
  // initialise all to zero, as also zeros should be filled
  std::fill( begin( m_clustersPerSiPM ), end( m_clustersPerSiPM ), 0 );
}

void FTLiteClusterMonitor::ClustersPerEventHistograms::update( const LHCb::Detector::FTChannelID& channel_id ) {

  // clusters per event per sipm
  m_clustersPerSiPM[channel_id.globalSipmIdx()]++;
}

void FTLiteClusterMonitor::ClustersPerEventHistograms::fill() {
  for ( size_t sipm_idx = 0; sipm_idx < FTConstants::nSiPMsTotal; sipm_idx++ ) {
    ( *m_ClustersPerEventPerSiPMProfile )[sipm_idx] += m_clustersPerSiPM[sipm_idx];
    ++( *m_ClustersPerEventPerSiPM )[{ sipm_idx, m_clustersPerSiPM[sipm_idx] }];
  }
  reset();
}

// -------------------------------------------------------------
// FTLiteClusterMonitor
// -------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTLiteClusterMonitor )

FTLiteClusterMonitor::FTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "ClusterLocation", LHCb::FTLiteClusterLocation::Default },
                  KeyValue{ "InputODIN", LHCb::ODINLocation::Default } } )
    , m_channel_idx_hists( this, true )
    , m_clusters_per_event_hists( this ) {}

// -------------------------------------------------------------
// Initialize
StatusCode FTLiteClusterMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    for ( ColType colType : m_colTypeHists ) {
      const std::string folder = ( colType == ColType::BB )   ? "BeamBeam/"
                                 : ( colType == ColType::B1 ) ? "BeamGas1/"
                                 : ( colType == ColType::B2 ) ? "BeamGas2/"
                                 : ( colType == ColType::EE ) ? "EmptyEmpty/"
                                                              : "BXERROR";
      m_colType_clusters.emplace( std::piecewise_construct, std::forward_as_tuple( colType ),
                                  std::forward_as_tuple( this, folder + "nClusters",
                                                         "Number of clusters;Clusters/event; Events",
                                                         Axis{ 2700, -0.5f, 27000 - 0.5f } ) );
      m_colType_channel_idx_hists.emplace( std::piecewise_construct, std::forward_as_tuple( colType ),
                                           std::forward_as_tuple( this, false, folder ) );
      m_colType_clusters_per_event_hists.emplace( std::piecewise_construct, std::forward_as_tuple( colType ),
                                                  std::forward_as_tuple( this, folder ) );
    }

    return StatusCode::SUCCESS;
  } );
}

// -------------------------------------------------------------
// Executed on each event
void FTLiteClusterMonitor::operator()( const FTLiteClusters& clusters, const ODIN& odin ) const {

  const auto n_clusters     = clusters.size();
  int        nClustersASide = 0;
  int        nClustersCSide = 0;

  // ODIN information
  // - - - - - - - - -
  const auto bunch_id = odin.bunchId();

  // * collision type
  const auto bx_type    = odin.bunchCrossingType();
  bool       isPhysics  = bx_type == LHCb::ODIN::BXTypes::BeamCrossing;
  bool       isBeam1Gas = bx_type == LHCb::ODIN::BXTypes::Beam1;
  bool       isBeam2Gas = bx_type == LHCb::ODIN::BXTypes::Beam2;
  bool       isEmpty    = bx_type == LHCb::ODIN::BXTypes::NoBeam;

  ColType colType = isPhysics    ? ColType::BB
                    : isBeam1Gas ? ColType::B1
                    : isBeam2Gas ? ColType::B2
                    : isEmpty    ? ColType::EE
                                 : ColType::ERROR;

  bool fill_colType = std::find( m_colTypeHists.begin(), m_colTypeHists.end(), colType ) != m_colTypeHists.end();

  // Fill cluster level histograms
  for ( const auto& cluster : clusters.range() ) {

    // Cluster position information
    const auto channel_id = cluster.channelID();

    // FIXME: LHCBSCIFI-201
    if ( channel_id.isRight() ) {
      // C Side // FIXME: All A/C Side histograms in std::array, size 2
      ++nClustersCSide;
    } else {
      // A Side
      ++nClustersASide;
    }
    m_channel_idx_hists.fill( channel_id );
    m_clusters_per_event_hists.update( channel_id );

    // Per collision type
    if ( fill_colType ) {
      m_colType_channel_idx_hists.at( colType ).fill( channel_id );
      m_colType_clusters_per_event_hists.at( colType ).update( channel_id );
    }
  }
  // Fill event level histograms
  ++m_clusters[n_clusters];

  // Per Bunch crossing
  ++m_events_bxid[bunch_id];
  ++m_clustersPerEvent_bxid[{ bunch_id, n_clusters }];
  m_clusters_bxid[bunch_id] += n_clusters;
  m_clusters_bxid_a[bunch_id] += nClustersASide;
  m_clusters_bxid_c[bunch_id] += nClustersCSide;

  m_clusters_per_event_hists.fill();

  // Per collision type
  if ( fill_colType ) {
    ++m_colType_clusters.at( colType )[n_clusters];
    m_colType_clusters_per_event_hists.at( colType ).fill();
  }

  ++m_nCalls;
}
