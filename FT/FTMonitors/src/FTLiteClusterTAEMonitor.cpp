/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/FTLiteCluster.h"
#include "Event/ODIN.h"
#include "Event/TAEUtils.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/MergingTransformer.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/GaudiException.h>

/** @class FTLiteClusterTAEMonitor FTLiteClusterTAEMonitor.cpp
 *
 *
 *  @author Hendrik Jage, Halime Sazak
 *  Inspired by FTClusterMonitor from  Roel Aaij,
 *  adapted by Lex Greeven, Johannes Heuel
 *  @date   2024-03-06
 */
using FTLiteClusters  = LHCb::FTLiteCluster::FTLiteClusters;
using ODIN            = LHCb::ODIN;
using ODINVector      = Gaudi::Functional::vector_of_const_<ODIN const*>;
using InputVector     = Gaudi::Functional::vector_of_const_<FTLiteClusters const*>;
using BXTypes         = LHCb::ODIN::BXTypes;
namespace FTConstants = LHCb::Detector::FT;
namespace GA          = Gaudi::Accumulators;

class FTLiteClusterTAEMonitor final
    : public LHCb::Algorithm::MergingConsumer<void( ODINVector const&, InputVector const& )> {
public:
  FTLiteClusterTAEMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  void operator()( ODINVector const&, InputVector const& ) const override;

private:
  LHCb::TAE::Handler m_taeHandler{ this };

  using Histogram1D        = GA::Histogram<1, GA::atomicity::full, float>;
  using Histogram2D        = GA::Histogram<2, GA::atomicity::full, float>;
  using ProfileHistogram   = GA::ProfileHistogram<1, GA::atomicity::full, float>;
  using ProfileHistogram2D = GA::ProfileHistogram<2, GA::atomicity::full, float>;
  using Axis               = GA::Axis<float>;

private:
  enum struct ColType { BB, B1, B2, EE, ERROR };

  enum struct TaeType {
    AllBX,
    IndexAllBX,
    IsolatedBX,
    FakeIsoBX,
    LeadingBX,
    TailingBX,
    OdinIsolatedBX,
    OdinFakeIsoBX,
    OdinCombinedBX,
    OdinIndexIsolatedBX,
    OdinIndexFakeIsoBX,
    OdinIndexCombinedBX,
  };

  // - - - - - - - - - - -
  // QuarterSiPMHistograms FIXME: DetectorHistogram?
  // - - - - - - - - - - -
  struct QuarterSiPMHistograms {
    QuarterSiPMHistograms( FTLiteClusterTAEMonitor* owner, std::string name_2D, std::string name_profile,
                           std::string name_slices );
    void fill( const LHCb::Detector::FTChannelID& channel_id, float variable = 0 );

    // Name/histogram path
    const std::string m_name;

    // Settings
    const bool m_plot_2D;
    const bool m_plot_profile;
    const bool m_plot_slices;

    // Plot options
    static constexpr int   m_v_gap    = 1;   // number empty bins on top/bottom
    static constexpr int   m_h_gap    = 12;  // number empty bins on left/right
    static constexpr int   m_l_gap    = 0;   // number empty bins between layers
    static constexpr float m_l_center = 0.5; // central position of 1st layer

    // Transformation factor
    static constexpr float m_sipm_to_module   = 1. / FTConstants::nSiPMsPerModule;
    static constexpr float m_quarter_to_layer = 1. / ( FTConstants::nHalfLayers + m_l_gap );

    // Axis
    const Axis a_sipm_per_module = Axis{ 2u * ( FTConstants::nMaxSiPMsPerQuarter + m_h_gap ),
                                         -( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module ),
                                         +( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module ) };
    const Axis a_quarter_per_layer =
        Axis{ FTConstants::nHalfLayers * FTConstants::nLayersTotal // #half layers
                  + m_l_gap * ( FTConstants::nLayersTotal - 1 )    // #gaps between layers
                  + 2 * m_v_gap,                                   // #empty bins on top+bottom
              -m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center,
              +m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center + FTConstants::nLayersTotal - 1 };

    // Histograms
    std::optional<Histogram2D>                              m_clusters;
    std::optional<ProfileHistogram2D>                       m_profile;
    std::map<std::tuple<size_t, size_t, bool>, Histogram1D> m_clusters_TLY;
    // FIXME: Split 1D and 2D, const axis/helper function?
  };
  using TaeTypes = std::map<TaeType, int>;

  // - - - - - - - - - - -
  // TAEHistograms
  // - - - - - - - - - - -

  struct TAEHistograms {

    TAEHistograms( FTLiteClusterTAEMonitor* owner );
    static std::string tae_name( const TaeType& type );
    TaeTypes           eval_odin( const ODIN& odin, const ColType& colType ) const;
    void               fill_cluster( const LHCb::Detector::FTChannelID& channel_id, const TaeTypes& taeType ) const;
    void               fill_event( const int nClusters, const TaeTypes& taeType ) const;

    // Axis
    const int  m_maxTAE;
    const Axis a_tae_corr;
    const Axis a_tae_idx;
    const Axis a_tae_iso;
    const Axis a_tae_fake;
    const Axis a_tae_comb;
    const Axis a_sipm;

    // TODO: Make this an option?
    const std::set<TaeType> m_cluster_hists = { TaeType::AllBX,
                                                TaeType::IndexAllBX,
                                                TaeType::IsolatedBX,
                                                TaeType::FakeIsoBX,
                                                TaeType::LeadingBX,
                                                TaeType::TailingBX,
                                                TaeType::OdinIsolatedBX,
                                                TaeType::OdinFakeIsoBX,
                                                TaeType::OdinIndexIsolatedBX,
                                                TaeType::OdinIndexFakeIsoBX };
    const std::set<TaeType> m_event_hists   = {
        TaeType::IndexAllBX,          TaeType::OdinIsolatedBX,     TaeType::OdinFakeIsoBX,      TaeType::OdinCombinedBX,
        TaeType::OdinIndexIsolatedBX, TaeType::OdinIndexFakeIsoBX, TaeType::OdinIndexCombinedBX };

    // Histograms
    mutable std::map<TaeType, Histogram1D>           m_clusters;
    mutable std::map<TaeType, Histogram1D>           m_clusters_a;
    mutable std::map<TaeType, Histogram1D>           m_clusters_c;
    mutable std::map<TaeType, Histogram2D>           m_sipm;
    mutable std::map<TaeType, ProfileHistogram>      m_sipm_profile;
    mutable std::map<TaeType, ProfileHistogram>      m_sipm_profile_a;
    mutable std::map<TaeType, ProfileHistogram>      m_sipm_profile_c;
    mutable std::map<TaeType, QuarterSiPMHistograms> m_quarter_sipm_hists;

    // * per event histograms
    mutable std::map<TaeType, Histogram2D> m_clustersPerEvent;
  };

  // -------------------------------------------------------------

private:
  // TAE global
  mutable GA::Counter<> m_nCalls{ this, "number of calls" };

  // FIXME: Options
  // Gaudi::Property<bool> m_drawHistsPerQuarter{this, "DrawHistsPerQuarter", true, "Enable histrograms per quarter"};
  const unsigned int   m_maxTAEHalfWindow = 7;
  Gaudi::Property<int> m_SuperTAEHalfWindow{ this, "SuperTAEHalfWindow", 3, "Define SuperTAE HalfWindow size" };

  const std::set<ColType> m_colTypeHists = { ColType::BB, ColType::B1, ColType::B2, ColType::EE };

  // structs
  TAEHistograms m_tae_hists;
};

// - - - - - - - - - - -
// TAEHistograms
// - - - - - - - - - - -
FTLiteClusterTAEMonitor::TAEHistograms::TAEHistograms( FTLiteClusterTAEMonitor* owner )
    : m_maxTAE( static_cast<int>( owner->m_maxTAEHalfWindow ) )
    , a_tae_corr( 2 * m_maxTAE + 1, -m_maxTAE - 0.5f, m_maxTAE + 0.5f )
    , a_tae_idx( 15, 0.5, 15.5 )
    , a_tae_iso( 3, -0.5, 2.5, "", { "Before", "Isolated", "After" } )
    , a_tae_fake( 4, -0.5, 3.5, "", { "Before", "Leading", "Trailing", "After" } )
    , a_tae_comb( 3, -0.5, 2.5, "", { "Before", "Collision", "After" } )
    , a_sipm( FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f ) {

  for ( auto tae_type : m_cluster_hists ) {

    // choose TAE axis based on the type
    const Axis& a_tae = ( tae_type == TaeType::IndexAllBX || tae_type == TaeType::OdinIndexIsolatedBX ||
                          tae_type == TaeType::OdinIndexFakeIsoBX || tae_type == TaeType::OdinIndexCombinedBX )
                            ? a_tae_idx
                        : ( tae_type == TaeType::OdinIsolatedBX ) ? a_tae_iso
                        : ( tae_type == TaeType::OdinFakeIsoBX )  ? a_tae_fake
                        : ( tae_type == TaeType::OdinCombinedBX ) ? a_tae_comb
                                                                  : a_tae_corr;

    // declare histograms
    m_clusters.emplace( std::piecewise_construct, std::forward_as_tuple( tae_type ),
                        std::forward_as_tuple( owner, fmt::format( "{}/Clusters", tae_name( tae_type ).c_str() ),
                                               "Number of Clusters per TAE;TAE;Clusters", a_tae ) );
    m_clusters_a.emplace( std::piecewise_construct, std::forward_as_tuple( tae_type ),
                          std::forward_as_tuple( owner, fmt::format( "{}/ClustersASide", tae_name( tae_type ).c_str() ),
                                                 "Number of Clusters per TAE (A Side);TAE;Clusters", a_tae ) );
    m_clusters_c.emplace( std::piecewise_construct, std::forward_as_tuple( tae_type ),
                          std::forward_as_tuple( owner, fmt::format( "{}/ClustersCSide", tae_name( tae_type ).c_str() ),
                                                 "Number of Clusters per TAE (C Side);TAE;Clusters", a_tae ) );
    m_sipm.emplace( std::piecewise_construct, std::forward_as_tuple( tae_type ),
                    std::forward_as_tuple( owner, fmt::format( "{}/SiPM", tae_name( tae_type ).c_str() ),
                                           "TAE per SIPM;Global SiPM index;TAE", a_sipm, a_tae ) );
    m_sipm_profile.emplace( std::piecewise_construct, std::forward_as_tuple( tae_type ),
                            std::forward_as_tuple( owner, fmt::format( "{}/SiPMProfile", tae_name( tae_type ).c_str() ),
                                                   "mean TAE per SIPM;Global SiPM index;mean TAE", a_sipm ) );
    m_sipm_profile_a.emplace(
        std::piecewise_construct, std::forward_as_tuple( tae_type ),
        std::forward_as_tuple( owner, fmt::format( "{}/SiPMProfileASide", tae_name( tae_type ).c_str() ),
                               "mean TAE per SIPM (A Side);Global SiPM index;mean TAE", a_sipm ) );
    m_sipm_profile_c.emplace(
        std::piecewise_construct, std::forward_as_tuple( tae_type ),
        std::forward_as_tuple( owner, fmt::format( "{}/SiPMProfileCSide", tae_name( tae_type ).c_str() ),
                               "mean TAE per SIPM (C Side);Global SiPM index;mean TAE", a_sipm ) );
    m_quarter_sipm_hists.emplace(
        std::piecewise_construct, std::forward_as_tuple( tae_type ),
        std::forward_as_tuple( owner, "", fmt::format( "{}/QuarterVsSiPMProfile", tae_name( tae_type ).c_str() ),
                               "" ) );
  }
  for ( auto tae_type : m_event_hists ) {

    // choose TAE axis based on the type
    const Axis& a_tae = ( tae_type == TaeType::IndexAllBX || tae_type == TaeType::OdinIndexIsolatedBX ||
                          tae_type == TaeType::OdinIndexFakeIsoBX || tae_type == TaeType::OdinIndexCombinedBX )
                            ? a_tae_idx
                        : ( tae_type == TaeType::OdinIsolatedBX ) ? a_tae_iso
                        : ( tae_type == TaeType::OdinFakeIsoBX )  ? a_tae_fake
                        : ( tae_type == TaeType::OdinCombinedBX ) ? a_tae_comb
                                                                  : a_tae_corr;

    // declare histograms
    m_clustersPerEvent.emplace(
        std::piecewise_construct, std::forward_as_tuple( tae_type ),
        std::forward_as_tuple( owner, fmt::format( "{}/ClustersPerEvent", tae_name( tae_type ).c_str() ),
                               "Clusters/Event per TAE;TAE;Clusters/Event", a_tae, Axis{ 351, -50.0f, 35050.0f } ) );
  }
}

std::string FTLiteClusterTAEMonitor::TAEHistograms::tae_name( const TaeType& type ) {
  switch ( type ) {
  case TaeType::AllBX: {
    return "TAE";
  }
  case TaeType::IndexAllBX: {
    return "idxTAE";
  }
  case TaeType::IsolatedBX: {
    return "isoTAE";
  }
  case TaeType::FakeIsoBX: {
    return "fakeTAE";
  }
  case TaeType::LeadingBX: {
    return "leadTAE";
  }
  case TaeType::TailingBX: {
    return "tailTAE";
  }
  case TaeType::OdinIsolatedBX: {
    return "isoOdinTAE";
  }
  case TaeType::OdinFakeIsoBX: {
    return "fakeOdinTAE";
  }
  case TaeType::OdinCombinedBX: {
    return "combOdinTAE";
  }
  case TaeType::OdinIndexIsolatedBX: {
    return "isoOdinIdxTAE";
  }
  case TaeType::OdinIndexFakeIsoBX: {
    return "fakeOdinIdxTAE";
  }
  case TaeType::OdinIndexCombinedBX: {
    return "combOdinIdxTAE";
  }
  default: {
    return "unknown";
  }
  }
}

FTLiteClusterTAEMonitor::TaeTypes FTLiteClusterTAEMonitor::TAEHistograms::eval_odin( const ODIN&    odin,
                                                                                     const ColType& colType ) const {
  TaeTypes tae_types;

  // FIXME: Sketch of logic

  // Odin event type
  // - - - - - - - -

  bool isEmpty   = ( colType == ColType::EE );
  bool isPhysics = ( colType == ColType::BB );

  const auto event_type        = odin.eventType();
  bool       isIsolatedBX      = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_08 );
  bool       isLeadingBX       = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_09 );
  bool       isTrailingBX      = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_10 );
  bool       isEmptyBeforeIso  = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_11 );
  bool       isEmptyAfterIso   = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_12 );
  bool       isEmptyBeforeLead = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_13 );
  bool       isEmptyAfterTrail = event_type & static_cast<std::uint16_t>( LHCb::ODIN::EventTypes::et_bit_14 );

  // Odin regions
  const bool isIsolatedReg = isEmptyBeforeIso || isIsolatedBX || isEmptyAfterIso;
  const bool isLeadingReg  = isEmptyBeforeLead || isLeadingBX;
  const bool isTrailingReg = isTrailingBX || isEmptyAfterTrail;
  const bool isFakeReg     = isLeadingReg || isTrailingReg;
  const bool isOdinReg     = isIsolatedReg || isFakeReg;
  const bool isBeforeReg   = isEmptyBeforeIso || isEmptyBeforeLead;
  const bool isAfterReg    = isEmptyAfterIso || isEmptyAfterTrail;
  const bool isColReg      = isIsolatedBX || isLeadingBX || isTrailingBX;

  // TAE indicies
  // - - - - - - -
  // FIXME: Comment, add what types and bins are (add "_bin")
  const int tae_idx = odin.timeAlignmentEventIndex();

  // * corrected tae index
  const int tae_corr = -tae_idx + m_maxTAE + 1;

  // * odin axis
  int tae_odin_iso = isEmptyBeforeIso ? 0 : isIsolatedBX ? 1 : isEmptyAfterIso ? 2 : -1; // FIXME: make consts for bins?
  int tae_odin_fake = isEmptyBeforeLead ? 0 : isLeadingBX ? 1 : isTrailingBX ? 2 : isEmptyAfterTrail ? 3 : -1;
  int tae_odin_comb = isBeforeReg ? 0 : isColReg ? 1 : isAfterReg ? 2 : -1;

  // determine matching BX types
  // all
  tae_types[TaeType::AllBX]      = tae_corr;
  tae_types[TaeType::IndexAllBX] = tae_idx;

  // iso tae
  if ( tae_corr == -1 ? isEmptyBeforeIso : tae_corr == 0 ? isIsolatedBX : tae_corr == 1 ? isEmptyAfterIso : isEmpty ) {
    tae_types[TaeType::IsolatedBX] = tae_corr;
  }

  // fake tae
  if ( tae_corr == -1  ? isEmptyBeforeLead
       : tae_corr == 0 ? isLeadingBX || isTrailingBX
       : tae_corr == 1 ? isEmptyAfterTrail
                       : isEmpty ) {
    tae_types[TaeType::FakeIsoBX] = tae_corr;
  }

  // lead tae
  if ( tae_corr == -1 ? isEmptyBeforeLead : tae_corr == 0 ? isLeadingBX : tae_corr > 0 ? isPhysics : isEmpty ) {
    tae_types[TaeType::LeadingBX] = tae_corr;
  }

  // trail tae
  if ( tae_corr < 0 ? isPhysics : tae_corr == 0 ? isTrailingBX : tae_corr == 1 ? isEmptyAfterTrail : isEmpty ) {
    tae_types[TaeType::TailingBX] = tae_corr;
  }

  // odin iso tae
  if ( isIsolatedReg ) {
    tae_types[TaeType::OdinIsolatedBX]      = tae_odin_iso;
    tae_types[TaeType::OdinIndexIsolatedBX] = tae_idx;
  }

  // odin fake tae
  if ( isFakeReg ) {
    tae_types[TaeType::OdinFakeIsoBX]      = tae_odin_fake;
    tae_types[TaeType::OdinIndexFakeIsoBX] = tae_idx;
  }

  // odin fake tae
  if ( isOdinReg ) {
    tae_types[TaeType::OdinCombinedBX]      = tae_odin_comb;
    tae_types[TaeType::OdinIndexCombinedBX] = tae_idx;
  }

  return tae_types;
}

void FTLiteClusterTAEMonitor::TAEHistograms::fill_cluster( const LHCb::Detector::FTChannelID& channel_id,
                                                           const TaeTypes&                    taeTypes ) const {

  // channel information
  const auto global_sipm_idx = channel_id.globalSipmIdx();

  // fill histograms
  for ( auto [type, tae] : taeTypes ) {

    // Only fill requested histograms
    if ( std::find( m_cluster_hists.begin(), m_cluster_hists.end(), type ) == m_cluster_hists.end() ) { continue; }

    ++m_clusters.at( type )[tae];
    ++m_sipm.at( type )[{ global_sipm_idx, tae }];
    m_sipm_profile.at( type )[global_sipm_idx] += tae;
    m_quarter_sipm_hists.at( type ).fill( channel_id, tae );

    // histograms per side
    if ( channel_id.isRight() ) {
      // C Side
      ++m_clusters_c.at( type )[tae];
      m_sipm_profile_c.at( type )[global_sipm_idx] += tae;
    } else {
      // A Side
      ++m_clusters_a.at( type )[tae];
      m_sipm_profile_a.at( type )[global_sipm_idx] += tae;
    }
  }
}

void FTLiteClusterTAEMonitor::TAEHistograms::fill_event( const int nClusters, const TaeTypes& taeTypes ) const {

  // fill histograms
  for ( auto [type, tae] : taeTypes ) {

    // Only fill requested histograms
    if ( std::find( m_event_hists.begin(), m_event_hists.end(), type ) == m_event_hists.end() ) { continue; }

    ++m_clustersPerEvent.at( type )[{ tae, nClusters }];
  }
}

// - - - - - - - - - - -
// QuarterSiPMHistograms
// - - - - - - - - - - -
FTLiteClusterTAEMonitor::QuarterSiPMHistograms::QuarterSiPMHistograms( FTLiteClusterTAEMonitor* owner,
                                                                       std::string name_2D, std::string name_profile,
                                                                       std::string name_slices )
    // only histograms with a name are created
    : m_plot_2D( !name_2D.empty() ), m_plot_profile( !name_profile.empty() ), m_plot_slices( !name_slices.empty() ) {
  if ( m_plot_2D ) {
    m_clusters.emplace( owner, name_2D, "SiPMNumber vs Quarter ID;Module;Layer", a_sipm_per_module,
                        a_quarter_per_layer );
  }
  if ( m_plot_profile ) {
    m_profile.emplace( owner, name_profile, "SiPMNumber vs Quarter ID;Module;Layer", a_sipm_per_module,
                       a_quarter_per_layer );
  }

  if ( m_plot_slices ) {
    for ( size_t station = 1; station <= FTConstants::nStations; ++station ) {
      for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
        for ( bool y_up : { true, false } ) {
          std::string TLY_title = fmt::format( "T{}L{} (y {} 0)", station, layer, y_up ? ">" : "<" );
          std::string TLY_name  = fmt::format( "T{}L{}{}", station, layer, y_up ? "U" : "D" );
          m_clusters_TLY.emplace(
              std::piecewise_construct, std::forward_as_tuple( station, layer, y_up ),
              std::forward_as_tuple( owner, name_slices + TLY_name,
                                     "Clusters per module in " + TLY_title + ";Module;Clusters (per SiPM)",
                                     a_sipm_per_module ) );
        }
      }
    }
  }
}

void FTLiteClusterTAEMonitor::QuarterSiPMHistograms::fill( const LHCb::Detector::FTChannelID& channel_id,
                                                           float                              variable ) {
  const auto quarter_sipm_idx = channel_id.localSiPMIdx_quarter();
  const auto global_layer_idx = channel_id.globalLayerIdx();
  const auto station          = static_cast<unsigned int>( channel_id.station() );
  const auto layer_idx        = static_cast<unsigned int>( channel_id.layer() );
  const auto quarter_idx      = static_cast<unsigned int>( channel_id.quarter() );

  // transform coordinates
  // * quarter_sipm_idx -> module
  double x = ( quarter_sipm_idx + 0.5f ) * m_sipm_to_module;

  if ( quarter_idx % 2 == 0 ) { // mirror C Side
    x *= -1;
  }

  // * quarter_idx -> staggered layer
  const double y = global_layer_idx + m_l_center + 0.5 * m_quarter_to_layer * ( quarter_idx < 2 ? -1 : 1 );

  // upper or lower half?
  const bool y_up = ( quarter_idx < 2 ? false : true );

  // fill histograms
  if ( m_plot_2D ) { ++( *m_clusters )[{ x, y }]; }
  if ( m_plot_profile ) { ( *m_profile )[{ x, y }] += variable; }
  if ( m_plot_slices ) { ++m_clusters_TLY.at( { station, layer_idx, y_up } )[x]; }
}

// -------------------------------------------------------------
// FTLiteClusterTAEMonitor
// -------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTLiteClusterTAEMonitor )

FTLiteClusterTAEMonitor::FTLiteClusterTAEMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : LHCb::Algorithm::MergingConsumer<void( ODINVector const&, InputVector const& )>(
          name, pSvcLocator, { KeyValues{ "ODINVector", {} }, KeyValues{ "InputVector", {} } } )
    , m_tae_hists( this ){};

// -------------------------------------------------------------
// Executed on each event
void FTLiteClusterTAEMonitor::operator()( ODINVector const& odinVector, InputVector const& inputVector ) const {

  auto taeEvents = m_taeHandler.arrangeTAE( odinVector, inputVector, m_SuperTAEHalfWindow );
  if ( taeEvents.empty() ) { return; }

  for ( const auto& element : taeEvents ) {
    // int                   offset   = element.first; //FIXME: Switch to offset in next version
    const LHCb::ODIN&     odin     = element.second.first;
    const FTLiteClusters& clusters = element.second.second;

    const auto n_clusters = clusters.size();

    // - - - - - - - - -
    // ODIN information
    // - - - - - - - - -
    // * collision type
    const auto bx_type    = odin.bunchCrossingType();
    bool       isPhysics  = bx_type == LHCb::ODIN::BXTypes::BeamCrossing;
    bool       isBeam1Gas = bx_type == LHCb::ODIN::BXTypes::Beam1;
    bool       isBeam2Gas = bx_type == LHCb::ODIN::BXTypes::Beam2;
    bool       isEmpty    = bx_type == LHCb::ODIN::BXTypes::NoBeam;
    ColType    colType    = isPhysics    ? ColType::BB
                            : isBeam1Gas ? ColType::B1
                            : isBeam2Gas ? ColType::B2
                            : isEmpty    ? ColType::EE
                                         : ColType::ERROR;

    const TaeTypes taeTypes = m_tae_hists.eval_odin( odin, colType );
    for ( const auto& cluster : clusters.range() ) {
      // Cluster position information
      const auto channel_id = cluster.channelID();

      m_tae_hists.fill_cluster( channel_id, taeTypes );
    }
    m_tae_hists.fill_event( n_clusters, taeTypes );
  }

  ++m_nCalls;
}
