###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def filling_scheme(fname):
    import tempfile

    from XRootD import client

    with client.File() as f:
        status, _ = f.open(str(fname))
        if not status.ok:
            raise RuntimeError(f"could not open {fname}: {status.message}")
        status, data = f.read()
        if not status.ok:
            raise RuntimeError(f"could not read {fname}: {status.message}")
        with tempfile.NamedTemporaryFile("w", delete=False) as f:
            f.write(data.decode("utf-8"))
            return f.name
