###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
    make_odin,
)
from PyConf.control_flow import CompositeNode


def filling_scheme(fname):
    import tempfile

    from XRootD import client

    with client.File() as f:
        status, _ = f.open(str(fname))
        if not status.ok:
            raise RuntimeError(f"could not open {fname}: {status.message}")
        status, data = f.read()
        if not status.ok:
            raise RuntimeError(f"could not read {fname}: {status.message}")
        with tempfile.NamedTemporaryFile("w", delete=False) as f:
            f.write(data.decode("utf-8"))
            return f.name


def configureInput(fileDBEntry, monitoringFileName, evt_max=1000):
    options = ApplicationOptions(_enabled=False)
    options.set_input_and_conds_from_testfiledb(fileDBEntry)
    options.monitoring_file = monitoringFileName
    options.evt_max = evt_max
    options.force_odin = True
    config = configure_input(options)
    from DDDB.CheckDD4Hep import UseDD4Hep

    if UseDD4Hep:
        dd4hepSvc = config[
            "LHCb::Det::LbDD4hep::DD4hepSvc/LHCb::Det::LbDD4hep::DD4hepSvc"
        ]
        dd4hepSvc.DetectorList = ["/world", "FT"]
    return options, config


def zs_cluster_monitoring():
    options, config = configureInput(
        "FTZSClusterMonitoring", "zs_cluster_monitoring.json"
    )

    from PyConf.Algorithms import FTClusterMonitor, FTRawBankDecoder

    odin = make_odin()
    clusters = FTRawBankDecoder(
        name="DecodeFT", RawBanks=default_raw_banks("FTCluster"), Odin=odin
    ).OutputLocation
    monitor = FTClusterMonitor(
        name="FTClusterMonitor",
        ClusterLocation=clusters,
        InputODIN=make_odin(),
        DrawHistsPerStation=True,
        DrawHistsPerQuarter=True,
        DrawHistsPerModule=True,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        enableTAE=True,
        Online=False,
    )

    nconfig = configure(
        options, CompositeNode("SciFiMonitorSequence", children=[monitor])
    )

    # amend JSONSink config
    jsonSink = nconfig["Gaudi::Monitoring::JSONSink/Gaudi__Monitoring__JSONSink"]
    jsonSink.NamesToSave = [
        ".*MainEvent/LiteClustersPerPseudoChannel_64",
        ".*nClustersPerTAE",
    ]

    config.update(nconfig)


def zs_lite_cluster_monitoring():
    options, config = configureInput(
        "FTZSLiteClusterMonitoring", "zs_lite_cluster_monitoring.json"
    )

    from PyConf.Algorithms import FTLiteClusterMonitor, FTRawBankDecoder

    odin = make_odin()
    clusters = FTRawBankDecoder(
        name="DecodeFT", RawBanks=default_raw_banks("FTCluster"), Odin=odin
    ).OutputLocation
    monitor = FTLiteClusterMonitor(
        name="FTLiteClusterMonitor", ClusterLocation=clusters, InputODIN=make_odin()
    )

    nconfig = configure(
        options, CompositeNode("SciFiMonitorSequence", children=[monitor])
    )

    # amend JSONSink config
    from os import environ

    debug = environ.get("DEBUG_SCIFI", None) is not None
    if debug:
        print("[INFO] Debug mode")
    jsonSink = nconfig["Gaudi::Monitoring::JSONSink/Gaudi__Monitoring__JSONSink"]
    if not debug:
        jsonSink.NamesToSave = [
            "^ClustersPerQuarterChannelIdx_SiPM$",
            "^TLQ_Clusters_SiPM/T3L3Q0$",
            "^BeamBeam/ClustersPerQuarterChannelIdx_SiPM$",
        ]
    jsonSink.ComponentsToSave = ["FTLiteClusterMonitor"]

    config.update(nconfig)


def nzs_cluster_monitoring():
    options, config = configureInput(
        "FTNZSClusterMonitoring", "nzs_cluster_monitoring.json", 100
    )

    from PyConf.Algorithms import FTClusterMonitor, FTNZSRawBankDecoder

    clusters = FTNZSRawBankDecoder(
        name="DecodeFT", RawBank=default_raw_banks("FTNZS")
    ).NZSClusterLocation
    monitor = FTClusterMonitor(
        name="FTClusterMonitor",
        ClusterLocation=clusters,
        InputODIN=make_odin(),
        DrawHistsPerStation=True,
        DrawHistsPerQuarter=True,
        DrawHistsPerModule=True,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        enableTAE=True,
        Online=False,
    )

    nconfig = configure(
        options, CompositeNode("SciFiMonitorSequence", children=[monitor])
    )

    # amend JSONSink config
    jsonSink = nconfig["Gaudi::Monitoring::JSONSink/Gaudi__Monitoring__JSONSink"]
    jsonSink.NamesToSave = [
        ".*MainEvent/LiteClustersPerPseudoChannel_64",
        ".*nClustersPerTAE",
    ]

    config.update(nconfig)


def nzs_cluster_monitoring_from_digits():
    options, config = configureInput(
        "FTNZSClusterMonitoring", "nzs_cluster_monitoring_from_digits.json", 100
    )

    from PyConf.Algorithms import (
        FTClusterMonitor,
        FTNZSClusterCreator,
        FTNZSRawBankDecoder,
    )

    digits = FTNZSRawBankDecoder(
        name="DecodeFT", RawBank=default_raw_banks("FTNZS")
    ).OutputLocation
    clusters = FTNZSClusterCreator(
        name="ClusterFT", InputLocation=digits
    ).OutputLocation
    monitor = FTClusterMonitor(
        name="FTClusterMonitor",
        ClusterLocation=clusters,
        InputODIN=make_odin(),
        DrawHistsPerStation=True,
        DrawHistsPerQuarter=True,
        DrawHistsPerModule=True,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        enableTAE=True,
        Online=False,
    )

    nconfig = configure(
        options, CompositeNode("SciFiMonitorSequence", children=[monitor])
    )

    # amend JSONSink config
    jsonSink = nconfig["Gaudi::Monitoring::JSONSink/Gaudi__Monitoring__JSONSink"]
    jsonSink.NamesToSave = [
        ".*MainEvent/LiteClustersPerPseudoChannel_64",
        ".*nClustersPerTAE",
    ]

    config.update(nconfig)


def nzs_digit_monitoring():
    options, config = configureInput(
        "FTNZSClusterMonitoring", "nzs_digit_monitoring.json", 300
    )

    from PyConf.Algorithms import FTDigitMonitor, FTNZSRawBankDecoder

    digits = FTNZSRawBankDecoder(
        name="DecodeFT", RawBank=default_raw_banks("FTNZS")
    ).OutputLocation
    monitor = FTDigitMonitor(
        name="FTDigitMonitor",
        DigitLocation=digits,
        InputODIN=make_odin(),
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        TAEBunchIds=[56],
        enableTAE=True,
        Online=False,
    )

    nconfig = configure(
        options, CompositeNode("SciFiMonitorSequence", children=[monitor])
    )

    # amend JSONSink config
    jsonSink = nconfig["Gaudi::Monitoring::JSONSink/Gaudi__Monitoring__JSONSink"]
    jsonSink.NamesToSave = ["TAEErrors", "TAEEvents", "TAEHalfWindows"]

    config.update(nconfig)
