/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "IMCCloner.h"
#include <Event/MCParticle.h>
#include <Event/MCVertex.h>
#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/KeyedObject.h>
#include <algorithm>

/** @namespace MicroDST Functors.hpp MicroDST/Functors.hpp
 *
 *
 *  Collection of useful functors satisfying the Cloner policy and
 *  dealing with the cloning and storing clones into TES locations.
 *
 *  @author Juan PALACIOS
 *  @date   2007-10-24
 */
namespace {
  /** Returns the full location of the given DataObject in the Data Store
   *
   *  @param pObj Data object
   *
   *  @return Location of given data object
   */
  std::string objectLocation( const DataObject* pObj ) {
    return ( !pObj ? "" : ( pObj->registry() ? pObj->registry()->identifier() : "" ) );
  }

  /// Returns the full location of the parent of a given object
  std::string objectLocation( const ContainedObject* obj ) { return ( obj ? objectLocation( obj->parent() ) : "" ); }

  //===========================================================================

} // namespace

//=============================================================================

//-----------------------------------------------------------------------------
// Implementation file for class : MCParticleCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------
/** @class MCParticleCloner MCParticleCloner.h
 *
 *  MicroDSTTool to clone an LHCb::MCParticle and place it in a TES location
 *  parallel to that of the parent. It clones and stores the origin vertex
 *  using simple copy, and clones the origin vertex's LHCb::MCParticle products.
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class MCParticleCloner final : public extends<GaudiTool, MicroDST::IMCCloner> {
  LHCb::MCParticle* clone( const LHCb::MCParticle* mcp ) const;
  LHCb::MCVertex*   clone( const LHCb::MCVertex* mcVertex ) const;
  void cloneDecayProducts( const SmartRefVector<LHCb::MCParticle>& products, LHCb::MCVertex* clonedVertex ) const;
  void cloneDecayVertices( const SmartRefVector<LHCb::MCVertex>& endVertices, LHCb::MCParticle* clonedParticle ) const;

  LHCb::MCVertex* operator()( const LHCb::MCVertex* vertex ) const {
    if ( !vertex ) return nullptr;
    LHCb::MCVertex* cln        = getStoredClone( vertex );
    const auto      nProd      = vertex->products().size();
    const auto      nCloneProd = ( cln ? cln->products().size() : 0 );
    return cln && ( nProd == nCloneProd ) ? cln : clone( vertex );
  }

  template <typename Type>
  Type* cloneKeyedContainerItem( const Type* item ) const;

  template <class T>
  T* getStoredClone( const T* original ) const {
    if ( !original || !original->parent() ) return nullptr;
    const auto cloneLocation = outputTESLocation( objectLocation( original->parent() ) );
    auto       clones        = getIfExists<typename T::Container>( cloneLocation );
    return clones ? clones->object( original->key() ) : nullptr;
  }

  std::string outputTESLocation( const std::string& inputLocation ) const {
    std::string tmp( inputLocation );
    const auto  loc = tmp.find( m_rootInTES );
    if ( loc != tmp.npos ) { tmp.replace( loc, m_rootInTES.length(), "" ); }
    return m_outputPrefix.value() + "/" + tmp;
  }

  std::string outputTESLocation( DataObject const* d ) const { return outputTESLocation( objectLocation( d ) ); }

  mutable Gaudi::Accumulators::BinomialCounter<> m_cloned_mcvertex{ this, "MCVertices cloned" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_cloned_mcparticle{ this, "MCParticles cloned" };
  Gaudi::Property<std::string>                   m_outputPrefix{ this, "OutputPrefix", "MicroDST" };
  std::string                                    m_rootInTES = "/Event/";

  template <typename Type>
  auto& counter_for_() const {
    if constexpr ( std::is_same_v<Type, LHCb::MCParticle> ) { return m_cloned_mcparticle; }
    if constexpr ( std::is_same_v<Type, LHCb::MCVertex> ) { return m_cloned_mcvertex; }
  }

public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override {
    return extends::initialize().andThen( [&] {
      if ( !rootInTES().empty() ) m_rootInTES = rootInTES();
      debug() << "Set rootInTES to " << m_rootInTES << endmsg;
    } );
  }

  LHCb::MCParticle* operator()( const LHCb::MCParticle* mcp ) const override {
    if ( !mcp ) return nullptr;
    LHCb::MCParticle* cln = getStoredClone( mcp );
    return cln ? cln : clone( mcp );
  }
};

//=============================================================================

LHCb::MCParticle* MCParticleCloner::clone( const LHCb::MCParticle* mcp ) const {
  if ( !mcp ) return nullptr;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "clone() called for " << *mcp << endmsg;

  // Clone the MCParticle
  LHCb::MCParticle* cln = cloneKeyedContainerItem( mcp );

  // Is there an originVertex that should be cloned?
  if ( const LHCb::MCVertex* originVertex = mcp->originVertex(); originVertex ) {

    // Has it already been cloned
    LHCb::MCVertex* originVertexClone = getStoredClone( originVertex );
    if ( !originVertexClone ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloning origin vertex " << *originVertex << endmsg;

      // make a clone
      originVertexClone = cloneKeyedContainerItem( originVertex );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloned vertex " << *originVertexClone << endmsg;

      // Clear the current list of products in the cloned vertex
      originVertexClone->clearProducts();

      // Clone the origin vertex mother
      const auto* mother      = mcp->mother();
      auto*       motherClone = ( mother ? ( *this )( mother ) : nullptr );
      if ( motherClone && msgLevel( MSG::DEBUG ) ) debug() << "Cloned mother " << *motherClone << endmsg;
      originVertexClone->setMother( motherClone );
    }

    // Add the cloned origin vertex to the cloned MCP
    cln->setOriginVertex( originVertexClone );

    // Add the cloned MCP to the cloned origin vertex, if not already there
    const bool found = std::any_of( originVertexClone->products().begin(), originVertexClone->products().end(),
                                    [&cln]( const LHCb::MCParticle* mcP ) { return mcP == cln; } );
    if ( !found ) { originVertexClone->addToProducts( cln ); }

  } else {
    cln->setOriginVertex( nullptr );
  }

  // Clone the end vertices
  cln->clearEndVertices();
  cloneDecayVertices( mcp->endVertices(), cln );

  return cln;
}

//=============================================================================

void MCParticleCloner::cloneDecayVertices( const SmartRefVector<LHCb::MCVertex>& endVertices,
                                           LHCb::MCParticle*                     clonedParticle ) const {
  for ( const auto& endVtx : endVertices ) {
    if ( endVtx->isDecay() && !( endVtx->products().empty() ) ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "Cloning Decay Vertex " << *endVtx << " with " << endVtx->products().size() << " products!"
                  << endmsg;
      clonedParticle->addToEndVertices( ( *this )( endVtx ) );
    }
  }
}

//=============================================================================

LHCb::MCVertex* MCParticleCloner::clone( const LHCb::MCVertex* vertex ) const {
  if ( !vertex ) return nullptr;
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Cloning MCVertex key=" << vertex->key() << " " << objectLocation( vertex ) << endmsg;
  LHCb::MCVertex* cln = cloneKeyedContainerItem( vertex );
  if ( cln ) {
    // Clone and set the mother for this vertex
    cln->setMother( ( *this )( vertex->mother() ) );
    // clone daugthers
    cloneDecayProducts( vertex->products(), cln );
  }
  return cln;
}

//=============================================================================

void MCParticleCloner::cloneDecayProducts( const SmartRefVector<LHCb::MCParticle>& products,
                                           LHCb::MCVertex*                         clonedVertex ) const {
  // Clear the current products
  clonedVertex->clearProducts();

  // loop over the products and clone
  for ( const auto& prod : products ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " -> Cloning MCParticle key=" << prod->key() << " " << objectLocation( prod ) << endmsg;

    LHCb::MCParticle* productClone = ( *this )( prod );
    if ( productClone ) {
      // set origin vertexfor the cloned product particle
      productClone->setOriginVertex( clonedVertex );
      // if not already present, add to vertex products list
      const bool found = std::any_of( clonedVertex->products().begin(), clonedVertex->products().end(),
                                      [&productClone]( const LHCb::MCParticle* mcP ) { return mcP == productClone; } );
      if ( !found ) clonedVertex->addToProducts( productClone );
    }
  }
}
//=============================================================================

template <class Type>
Type* MCParticleCloner::cloneKeyedContainerItem( const Type* item ) const {
  auto& counter = counter_for_<Type>();
  if ( !item ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Cannot clone a NULL pointer !" << endmsg;
    counter += false;
    return nullptr;
  }

  if ( !item->parent() ) {
    Warning( "Cannot clone an object with no parent!" ).ignore();
    counter += false;
    return nullptr;
  }

  auto clones = getOrCreate<typename Type::Container, typename Type::Container>( outputTESLocation( item->parent() ) );
  // Propagate DataObject version from original to clone container
  clones->setVersion( item->parent()->version() );

  // try and get clone
  auto clonedItem = clones->object( item->key() );
  if ( clonedItem ) {
    counter += false;
    return clonedItem;
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "cloneKeyedContainerItem: Cloning item key " << item->key() << " in "
            << item->parent()->registry()->identifier() << " to " << objectLocation( clones ) << endmsg;
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << *item << endmsg;
  }

  clonedItem = item->clone();
  if ( !clonedItem ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "cloneKeyedContainerItem: Cloning FAILED" << endmsg;
    counter += false;
    return nullptr;
  }

  clones->insert( clonedItem, item->key() );
  counter += true;
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Cloned item " << *clonedItem << endmsg;

  return clonedItem;
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCParticleCloner )

//=============================================================================
