/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Gaudi/Accumulators.h"
#include "IMCCloner.h"
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
/** @class CopySignalMCParticles CopySignalMCParticles.h
 *
 *  Clones all 'signal' MCParticles.
 *
 *  @author Chris Jones
 *  @date   2015-03-24
 */

class CopySignalMCParticles final : public Gaudi::Algorithm {
  /// MCParticle Cloner
  ToolHandle<MicroDST::IMCCloner> m_cloner{ this, "MCCloner", "MCCloner" };

  /// Location of MCParticles to clone
  DataObjectReadHandle<LHCb::MCParticles> m_mcPs{ this, "MCParticlesLocation", LHCb::MCParticleLocation::Default };

  mutable Gaudi::Accumulators::BinomialCounter<> m_isSignal{ this, "copy signal" };
  mutable Gaudi::Accumulators::BinomialCounter<> m_container_present{ this, "input container present" };

public:
  /// Standard constructor
  using Gaudi::Algorithm::Algorithm;

  StatusCode execute( const EventContext& ) const override {
    const auto* mcPs = m_mcPs.getIfExists();
    m_container_present += ( mcPs != nullptr );
    if ( mcPs ) {
      auto buf = m_isSignal.buffer();
      for ( const auto* mcP : *mcPs ) {
        bool signal = mcP->fromSignal();
        buf += signal;
        if ( signal ) ( *m_cloner )( mcP );
      }
    }
    return StatusCode::SUCCESS;
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CopySignalMCParticles )

//=============================================================================
