/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "IMCCloner.h"
#include "Kernel/Particle2MCParticle.h"
#include <LHCbAlgs/MergingTransformer.h>
#include <functional>

typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> PP2MCPTable;

//=============================================================================

/** @namespace MicroDST RelTableFunctors.h MicroDST/RelTableFunctors.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2009-04-17
 */

namespace MicroDST {
  /** @class RelationsClonerAlg RelationsClonerAlg.h MicroDST/RelationsClonerAlg.h
   *
   *  Algorithm to clone the 'To' side of a relations table.
   *
   *  @author Juan PALACIOS juan.palacios@nikhef.nl
   *  @date   2009-04-14
   */

  class CopyMCRelationsTargets
      : public LHCb::Algorithm::MergingTransformer<void( Gaudi::Functional::vector_of_const_<PP2MCPTable*> const& )> {

  public:
    //===========================================================================
    /// Standard constructor
    CopyMCRelationsTargets( std::string name, ISvcLocator* locator )
        : MergingTransformer{ name, locator, { "InputLocations", {} } } {}

    //===========================================================================

    void operator()( Gaudi::Functional::vector_of_const_<PP2MCPTable*> const& tables ) const override {
      for ( const auto* table : tables ) {
        m_container_absent += ( table == nullptr );
        if ( !table ) continue;
        for ( const auto& entry : table->relations() ) ( *m_cloner )( entry.to() );
        m_nEntries += table->relations().size();
      }
    }
    //=========================================================================

  private:
    //===========================================================================

    ToolHandle<IMCCloner> m_cloner{ this, "MCCloner", "MCCloner" };

    mutable Gaudi::Accumulators::BinomialCounter<> m_container_absent{ this, "Input container absent" };
    mutable Gaudi::Accumulators::StatCounter<>     m_nEntries{ this, "# of cloned entries" };
  };

  DECLARE_COMPONENT_WITH_ID( CopyMCRelationsTargets, "CopyMCRelationsTargets" )

} // namespace MicroDST

//=============================================================================
