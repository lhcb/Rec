/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/extends.h"

// Forward declarations
namespace LHCb {
  class MCParticle;
}

/** @class ICloneMCParticle MicroDST/ICloneMCParticle.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
namespace MicroDST {
  struct GAUDI_API IMCCloner : extend_interfaces<IAlgTool> {

    /// Interface ID
    DeclareInterfaceID( IMCCloner, 1, 0 );

    /// Clone operator
    virtual LHCb::MCParticle* operator()( const LHCb::MCParticle* source ) const = 0;
  };

} // namespace MicroDST
