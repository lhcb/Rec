###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT COMMAND lhcb_find_package)
  # Look for LHCb find_package wrapper
  find_file(LHCbFindPackage_FILE LHCbFindPackage.cmake)
  if(LHCbFindPackage_FILE)
      include(${LHCbFindPackage_FILE})
  else()
      # if not found, use the standard find_package
      macro(lhcb_find_package)
          find_package(${ARGV})
      endmacro()
  endif()
endif()

# -- Public dependencies
lhcb_find_package(Lbcom REQUIRED)

find_package(Boost CONFIG REQUIRED headers)
find_package(ROOT REQUIRED
    MathCore
    RIO
    Tree
)
find_package(BLAS REQUIRED)

find_data_package(TMVAWeights REQUIRED)

# -- Private dependencies
if(WITH_Rec_PRIVATE_DEPENDENCIES)
    find_package(AIDA REQUIRED)
    find_package(Boost CONFIG REQUIRED
        container
        filesystem
        iostreams
        log
        regex
    )
    find_package(CLHEP REQUIRED)
    find_package(Eigen3 REQUIRED)
    find_package(FastJet REQUIRED)
    find_package(fmt REQUIRED)
    find_package(GSL REQUIRED)
    find_package(nlohmann_json REQUIRED)
    find_package(Python REQUIRED Development Interpreter)
    find_package(ROOT REQUIRED
        Core
        GenVector
        Hist
        Matrix
        TMVA
    )
    find_package(Vc REQUIRED)
    find_package(VDT REQUIRED)
    find_package(XGBoost REQUIRED)
    find_package(yaml-cpp REQUIRED)

    if(BUILD_TESTING)
        find_package(BLAS REQUIRED)
        find_package(Boost CONFIG REQUIRED unit_test_framework)
    endif()
endif()
