/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/CaloAdc.h"
#include "Event/PlumeAdc.h"

#include "Event/RawEvent.h"

#include "Detector/Calo/CaloCellID.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "Kernel/PlumeChannelID.h"
#include "LHCbAlgs/Transformer.h"
#include "boost/container/small_vector.hpp"

#include "LHCbMath/LHCbMath.h"
#include <bitset>
#include <map>
#include <vector>

using LHCb::Detector::Plume::ChannelID;
using PlumeCoincidences = std::vector<std::pair<LHCb::PlumeAdc, LHCb::PlumeAdc>>;

/** @class PlumeCoincidenceFinder
 *
 * Algorithm to find the coincideneces in Plume
 *
 */

class PlumeCoincidenceFinder : public LHCb::Algorithm::Transformer<PlumeCoincidences( const LHCb::PlumeAdcs& )> {

public:
  /// Standard constructor
  PlumeCoincidenceFinder( const std::string&, ISvcLocator* );
  PlumeCoincidences operator()( const LHCb::PlumeAdcs& ) const override;

private:
  mutable Gaudi::Accumulators::StatCounter<> m_numCoincidances = { this, "Number of Coincidences in event" };

  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, int> m_CoincHist{
      this, "numCoinc", "Coincidence distribution", { 24, 0, 24 } };

  mutable Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, int> m_numPairs{
      this, "numPairs", "Number of pairs in event", { 10, 0, 10 } };
};

DECLARE_COMPONENT( PlumeCoincidenceFinder )

PlumeCoincidenceFinder::PlumeCoincidenceFinder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{ "InputADCs", LHCb::PlumeAdcLocation::Default },
                   KeyValue{ "OutputCoincidances", "MC/Plume/Coincidences" } ) {}

PlumeCoincidences PlumeCoincidenceFinder::operator()( const LHCb::PlumeAdcs& adcs ) const {
  PlumeCoincidences coincidences;
  int               numPairsInEvent = 0;

  for ( auto const& adc1 : adcs ) {
    if ( adc1->channelID().channelType() == ChannelID::ChannelType::LUMI ) continue;
    for ( auto const& adc2 : adcs ) {
      if ( adc2->channelID().channelType() == ChannelID::ChannelType::LUMI ) continue;
      if ( adc1->channelID().channelID() + 24 == adc2->channelID().channelID() ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Found coincidence: " << *adc1 << " " << *adc2 << endmsg;
        ++numPairsInEvent;
        ++m_numCoincidances;
        ++m_CoincHist[adc1->channelID().channelID()];
        coincidences.push_back( std::make_pair( LHCb::PlumeAdc( *adc1 ), LHCb::PlumeAdc( *adc2 ) ) );
        break;
      }
    }
  }

  ++m_numPairs[numPairsInEvent];
  return coincidences;
}
