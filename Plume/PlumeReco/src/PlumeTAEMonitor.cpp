/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Event
#include "Event/ODIN.h"
#include "Event/PlumeAdc.h"
#include "Event/TAEUtils.h"

// Gaudi
#include "Gaudi/Accumulators/Histogram.h"
#include "LHCbAlgs/MergingTransformer.h"

namespace {
  const auto axisADC = Gaudi::Accumulators::Axis<double>( ( 4096 + 256 ) / 16, -256 / 16, 4096 / 16 );
}

using LHCb::Detector::Plume::ChannelID;
using ODINVector  = Gaudi::Functional::vector_of_const_<LHCb::ODIN const*>;
using InputVector = Gaudi::Functional::vector_of_const_<LHCb::PlumeAdcs const*>;
;
using BXTypes = LHCb::ODIN::BXTypes;

class PlumeTAEMonitor final : public LHCb::Algorithm::MergingConsumer<void( ODINVector const&, InputVector const& )> {
public:
  PlumeTAEMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingConsumer<void( ODINVector const&, InputVector const& )>(
            name, pSvcLocator, { KeyValues{ "ODINVector", {} }, KeyValues{ "InputVector", {} } } ){};

  void operator()( ODINVector const&, InputVector const& ) const override;

private:
  LHCb::TAE::Handler m_taeHandler{ this };

  mutable Gaudi::Accumulators::Histogram<2> m_tae_adc{ this, "tae_adc", "ADC counts in lumi channels vs TAE offset",
                                                       LHCb::TAE::defaultAxis, axisADC };
};

DECLARE_COMPONENT( PlumeTAEMonitor )

void PlumeTAEMonitor::operator()( ODINVector const& odinVector, InputVector const& inputVector ) const {
  auto taeEvents = m_taeHandler.arrangeTAE( odinVector, inputVector );
  if ( taeEvents.empty() ) { // Something went wrong
    return;
  }

  for ( auto const& element : taeEvents ) {
    int                    offset = element.first; // 0 is central, negative for Prev, positive for Next
    LHCb::ODIN const&      odin   = element.second.first;
    LHCb::PlumeAdcs const& input  = element.second.second;

    for ( const auto& digit : input ) {
      const auto adc  = digit->adc();
      const auto type = digit->channelID().channelType();
      if ( offset == 0 || odin.bunchCrossingType() == LHCb::ODIN::BXTypes::NoBeam ) {
        // one histogram that mixes together all channels and all TAE types (INDIVs and "fake TAEs")
        if ( type == ChannelID::ChannelType::LUMI ) { ++m_tae_adc[{ offset, adc }]; }
      }
    }
  }
}
