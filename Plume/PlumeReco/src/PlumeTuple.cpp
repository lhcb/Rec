/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/PlumeAdc.h"
#include "Event/RecSummary.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiKernel/Memory.h"
#include "LHCbAlgs/Consumer.h"

#include <algorithm>
#include <atomic>

using Input = LHCb::PlumeAdcs;

//-----------------------------------------------------------------------------
// Implementation file for class : PlumeTuple
//
// 2021-11-12 : Vladyslav Orlov
//-----------------------------------------------------------------------------

using LHCb::Detector::Plume::ChannelID;

/** @class PlumeTuple PlumeTuple.h
 *
 * Fill a Tuple with adc counts from Plume detector channels
 *
 * @author Vladyslav Orlov
 * @date   2021-11-12
 *
 */
class PlumeTuple final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN&, const Input& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  PlumeTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                                        ///< Algorithm initialization
  void       operator()( const LHCb::ODIN&, const Input& ) const override; ///< Algorithm execution

private:
  Gaudi::Property<bool> m_read_all_channels{ this, "ReadAllChannels", false,
                                             "Read all channels, including those that are nominally not used" };

  mutable PublicToolHandle<ISequencerTimerTool> m_timerTool{ this, "TimerTool",
                                                             "SequencerTimerTool" }; ///< global timer tool
  int                                           m_timer      = 0;                    ///< timer index
  mutable std::atomic<unsigned long long>       m_evtCounter = { 0 };

  template <class TYPE>
  void fillTuple( Tuple& tuple, const std::string& var, const TYPE number ) const {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Filling " << var << " with " << number << endmsg;
    tuple->column( var, number ).ignore();
  }
};
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PlumeTuple )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
PlumeTuple::PlumeTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, { KeyValue{ "ODIN", "" }, KeyValue{ "Input", "" } } ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode PlumeTuple::initialize() {
  return Consumer::initialize().andThen( [&] { return m_timerTool.retrieve(); } ).andThen( [&] {
    m_timer = m_timerTool->addTimer( name() );
    m_timerTool->start( m_timer ); // start it now
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void PlumeTuple::operator()( const LHCb::ODIN& odin, const Input& adcs ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_evtCounter;
  Tuple tuple = nTuple( "Plume", "PlumeTuple" );

  for ( const auto& adc : adcs ) {
    const double t = m_timerTool->stop( m_timer ); // stop
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Time is " << t << endmsg;
    m_timerTool->start( m_timer ); // start again

    // Fill tuple
    fillTuple( tuple, "EventInSequence", m_evtCounter.load() );
    fillTuple( tuple, "RunNumber", odin.runNumber() );
    fillTuple( tuple, "EvtNumber", (int)odin.eventNumber() );
    fillTuple( tuple, "OrbitNumber", (int)odin.orbitNumber() );
    fillTuple( tuple, "gpsTime", (unsigned long long)odin.gpsTime() );
    fillTuple( tuple, "bcid", odin.bunchId() );
    fillTuple( tuple, "bxType", (unsigned short int)odin.bunchCrossingType() );
    fillTuple( tuple, "calibType", (unsigned short int)odin.calibrationType() );

    if ( !m_read_all_channels ) {
      const auto type = adc->channelID().channelType();
      if ( type != ChannelID::ChannelType::TIME_T ) {
        fillTuple( tuple, "adc_" + adc->channelID().toString(), adc->adc() );
        if ( type != ChannelID::ChannelType::TIME ) {
          fillTuple( tuple, "ot_" + adc->channelID().toString(), adc->overThreshold() );
        }
      } else {
        fillTuple( tuple, "time_" + adc->channelID().toString(), adc->adc() );
      }
    } else {
      if ( msgLevel( MSG::DEBUG ) ) debug() << adc->channelID().all() << " " << adc->adc() << endmsg;
      fillTuple( tuple, "adc_" + std::to_string( adc->channelID().all() ), adc->adc() );
      fillTuple( tuple, "ot_" + std::to_string( adc->channelID().all() ), adc->overThreshold() );
    }
  }

  tuple->write().ignore();
}
