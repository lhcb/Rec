###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Plume/PlumeReco
-------------------------
#]=======================================================================]

gaudi_add_module(PlumeReco
    SOURCES
        src/PlumeCoincidenceFinder.cpp
        src/PlumeRawToDigits.cpp
        src/PlumeDigitMonitor.cpp
        src/PlumeTAEMonitor.cpp
        src/PlumeTuple.cpp

    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DAQUtilsLib
        LHCb::DetDescLib
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::PrKernel
        Rec::TrackInterfacesLib
)

gaudi_add_tests(QMTest)
