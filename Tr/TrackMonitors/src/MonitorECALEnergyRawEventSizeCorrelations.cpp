/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/CaloDigits_v2.h"
#include "Event/PrHits.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/Consumer.h"
#include "RichFutureUtils/RichDecodedData.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Accumulators/HistogramArray.h>
#include <PrKernel/UTHitHandler.h>
#include <algorithm>

namespace {
  const std::array<std::string, 11> s_sdHistNames = { "ODINHLT", "VELO", "RICH1", "UT",   "SCIFI", "RICH2",
                                                      "PLUME",   "ECAL", "HCAL",  "MUON", "TDET" };

  int8_t histIndex( const std::string& x ) {
    auto it = std::find( s_sdHistNames.begin(), s_sdHistNames.end(), x );
    return ( it == s_sdHistNames.end() ) ? ( -1 ) : std::distance( s_sdHistNames.begin(), it );
  }

  auto create_SDHistIndices() {
    std::array<int8_t, 32> b;
    b.fill( -1 );
    b[0]  = histIndex( "ODINHLT" );
    b[2]  = histIndex( "VELO" );
    b[3]  = histIndex( "VELO" );
    b[4]  = histIndex( "RICH1" );
    b[5]  = histIndex( "UT" );
    b[6]  = histIndex( "UT" );
    b[7]  = histIndex( "SCIFI" );
    b[8]  = histIndex( "SCIFI" );
    b[9]  = histIndex( "RICH2" );
    b[10] = histIndex( "PLUME" );
    b[11] = histIndex( "ECAL" );
    b[12] = histIndex( "HCAL" );
    b[13] = histIndex( "MUON" );
    b[14] = histIndex( "MUON" );
    b[15] = histIndex( "TDET" );
    return b;
  }

  /// map from the upper 5 bits of the source ID to the SD histogram
  const std::array<int8_t, 32> s_sdHistIndices = create_SDHistIndices();
} // namespace

///////////////Energy-Raw event size correlations
class MonitorECALEnergyRawEventSizeCorrelations
    : public LHCb::Algorithm::Consumer<void( LHCb::RawEvent const&            rawEvent,
                                             LHCb::Event::Calo::Digits const& edigits )> {
public:
  MonitorECALEnergyRawEventSizeCorrelations( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, { KeyValue{ "RawEvent", "" }, KeyValue{ "ECALDigits", "" } } ){};

  void operator()( LHCb::RawEvent const& rawEvent, LHCb::Event::Calo::Digits const& edigits ) const override;

  mutable Gaudi::Accumulators::Histogram<2> m_ecaltot_size_correlation{
      this,
      "ECALTotalEventSizeCorrelation",
      "ECALTotalEventSizeCorrelation",
      { { 200, 0, 60000000, "ECAL energy" }, { 200, 0, 2e6, "Total raw data size (w/o MDF header)" } } }; // size range
                                                                                                          // is tuned
                                                                                                          // for PbPb

  mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, s_sdHistNames.size()>
      m_ecalot_sdeventsize_correlation{
          this,
          []( int i ) { return fmt::format( "ECAL{}EventSizeCorrelation", s_sdHistNames[i] ); },
          []( int i ) { return fmt::format( "ECAL{}EventSizeCorrelation", s_sdHistNames[i] ); },
          { 200, 0, 60000000, "ECAL energy" },
          { 200, 0, 1e6, "SD raw data size" } }; // size range is tuned for PbPb
};

DECLARE_COMPONENT( MonitorECALEnergyRawEventSizeCorrelations )

void MonitorECALEnergyRawEventSizeCorrelations::operator()( LHCb::RawEvent const&            rawEvent,
                                                            LHCb::Event::Calo::Digits const& edigits ) const {

  const auto ecaltot = std::accumulate( edigits.begin(), edigits.end(), 0.,
                                        []( auto init, const auto& digit ) { return init + digit.energy(); } );

  unsigned int                                   totalEventSize = 0;
  std::array<unsigned int, s_sdHistNames.size()> sdEventSize    = {};
  for ( auto type : LHCb::RawBank::types() ) {
    for ( const auto& bank : rawEvent.banks( type ) ) {
      totalEventSize += bank->totalSize();
      auto sd = s_sdHistIndices[bank->sourceID() >> 11];
      if ( sd >= 0 ) { sdEventSize[sd] += bank->totalSize(); }
    }
  }

  ++m_ecaltot_size_correlation[{ ecaltot, totalEventSize }];
  for ( unsigned short sd = 0; sd < sdEventSize.size(); ++sd ) {
    ++m_ecalot_sdeventsize_correlation[sd][{ ecaltot, sdEventSize[sd] }];
  }
}
