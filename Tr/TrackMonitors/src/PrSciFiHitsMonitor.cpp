/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/FT/FTConstants.h"
#include "Event/ODIN.h"
#include "Event/PrHits.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "Kernel/SynchronizedValue.h"
#include "LHCbAlgs/Consumer.h"
#include <array>
#include <string>

namespace LHCb::Pr::FT {
  using namespace LHCb::Detector::FT;

  class SciFiHitsMonitor : public Algorithm::Consumer<void( const Hits&, LHCb::ODIN const& )> {

  public:
    SciFiHitsMonitor( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, { KeyValue{ "SciFiHits", "" }, KeyValue{ "ODINLocation", "" } } ) {}

    // StatusCode initialize() override;

    void operator()( const Hits& hits, LHCb::ODIN const& odin ) const override {

      // -- store the first time one encounters per run nnumber, to define tZero per run
      // -- events are not necessarily ordered in time, but rarely should be unordered by seconds
      auto tDiff = m_offsets.with_lock( [&]( std::map<Gaudi::TimeSpan::ValueType, Gaudi::Time>& offsets ) {
        auto [i, _] = offsets.emplace( odin.runNumber(), odin.eventTime() );
        return odin.eventTime() - i->second;
      } );

      for ( auto zone{ 0u }; zone < nZonesTotal; ++zone ) {
        const auto [start, end] = hits.getZoneIndices( zone );
        auto xPosHistoBuffer    = m_hitsXPosHistos.at( zone / 2u ).buffer();
        for ( auto iHit = start; iHit < end; ++iHit ) { ++xPosHistoBuffer[hits.x( iHit )]; }
        // -- Fill the averge hits per time profile histos
        auto nHits    = std::max( 0, end - start );
        int  tDiffSec = static_cast<int>( tDiff.seconds() );
        m_hitsPerLayerPerTime[zone][tDiffSec] += nHits;
      }
    };

  private:
    using Histo = Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float>;
    mutable std::array<Histo, nLayersTotal> m_hitsXPosHistos{ {
        { this, "T1X1", "SciFi hits x positions T1X1", { 2000, -3000, 3000 } },
        { this, "T1U", "SciFi hits x positions T1U", { 2000, -3000, 3000 } },
        { this, "T1V", "SciFi hits x positions T1V", { 2000, -3000, 3000 } },
        { this, "T1X2", "SciFi hits x positions T1X2", { 2000, -3000, 3000 } },
        { this, "T2X1", "SciFi hits x positions T2X1", { 2000, -3000, 3000 } },
        { this, "T2U", "SciFi hits x positions T2U", { 2000, -3000, 3000 } },
        { this, "T2V", "SciFi hits x positions T2V", { 2000, -3000, 3000 } },
        { this, "T2X2", "SciFi hits x positions T2X2", { 2000, -3000, 3000 } },
        { this, "T3X1", "SciFi hits x positions T3X1", { 2000, -3200, 3200 } },
        { this, "T3U", "SciFi hits x positions T3U", { 2000, -3200, 3200 } },
        { this, "T3V", "SciFi hits x positions T3V", { 2000, -3200, 3200 } },
        { this, "T3X2", "SciFi hits x positions T3X2", { 2000, -3200, 3200 } },
    } };

    mutable LHCb::cxx::SynchronizedValue<std::map<Gaudi::TimeSpan::ValueType, Gaudi::Time>> m_offsets;

    using ProfileHisto = Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, float>;
    mutable std::array<ProfileHisto, 2 * nLayersTotal> m_hitsPerLayerPerTime{ {
        { this, "HitsInT1X1vsTimeBottom", "SciFi hits in T1X1 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT1X1vsTimeTop", "SciFi hits in T1X1 top vs time", { 500, 0, 5000 } },
        { this, "HitsInT1UvsTimeBottom", "SciFi hits in T1U bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT1UvsTimeTop", "SciFi hits in T1U top vs time", { 500, 0, 5000 } },
        { this, "HitsInT1VvsTimeBottom", "SciFi hits in T1V bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT1VvsTimeTop", "SciFi hits in T1V top vs time", { 500, 0, 5000 } },
        { this, "HitsInT1X2vsTimeBottom", "SciFi hits in T1X2 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT1X2vsTimeTop", "SciFi hits in T1X2 top vs time", { 500, 0, 5000 } },

        { this, "HitsInT2X1vsTimeBottom", "SciFi hits in T2X1 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT2X1vsTimeTop", "SciFi hits in T2X1 top vs time", { 500, 0, 5000 } },
        { this, "HitsInT2UvsTimeBottom", "SciFi hits in T2U bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT2UvsTimeTop", "SciFi hits in T2U top vs time", { 500, 0, 5000 } },
        { this, "HitsInT2VvsTimeBottom", "SciFi hits in T2V bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT2VvsTimeTop", "SciFi hits in T2V top vs time", { 500, 0, 5000 } },
        { this, "HitsInT2X2vsTimeBottom", "SciFi hits in T2X2 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT2X2vsTimeTop", "SciFi hits in T2X2 top vs time", { 500, 0, 5000 } },

        { this, "HitsInT3X1vsTimeBottom", "SciFi hits in T3X1 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT3X1vsTimeTop", "SciFi hits in T3X1 top vs time", { 500, 0, 5000 } },
        { this, "HitsInT3UvsTimeBottom", "SciFi hits in T3U bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT3UvsTimeTop", "SciFi hits in T3U top vs time", { 500, 0, 5000 } },
        { this, "HitsInT3VvsTimeBottom", "SciFi hits in T3V bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT3VvsTimeTop", "SciFi hits in T3V top vs time", { 500, 0, 5000 } },
        { this, "HitsInT3X2vsTimeBottom", "SciFi hits in T3X2 bottom vs time", { 500, 0, 5000 } },
        { this, "HitsInT3X2vsTimeTop", "SciFi hits in T3X2 top vs time", { 500, 0, 5000 } },

    } };
  };

  DECLARE_COMPONENT_WITH_ID( SciFiHitsMonitor, "PrSciFiHitsMonitor" )

} // namespace LHCb::Pr::FT
