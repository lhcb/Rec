/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/Track.h"
#include "Kernel/meta_enum.h"
#include "RichFutureUtils/RichDecodedData.h"
#include <Event/PrHits.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <LHCbAlgs/Consumer.h>
#include <PrKernel/UTHitHandler.h>

meta_enum_class( CollisionType, int, Unknown = 0, pp = 1, PbPb = 2 );

class MonitorDetectorCorrelations
    : public LHCb::Algorithm::Consumer<void(
          LHCb::Pr::VP::Hits const&, LHCb::Pr::FT::Hits const&, UT::HitHandler const&, MuonHitContainer const&,
          LHCb::Event::Calo::v2::Clusters const&, LHCb::Event::Calo::Digits const&, LHCb::Event::Calo::Digits const&,
          Rich::Future::DAQ::DecodedData const& )> {
public:
  MonitorDetectorCorrelations( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "VeloHits", "" }, KeyValue{ "SciFiHits", "" }, KeyValue{ "UTHits", "" },
                    KeyValue{ "MuonHits", "" }, KeyValue{ "ECALClusters", "" }, KeyValue{ "ECALDigits", "" },
                    KeyValue{ "HCALDigits", "" }, KeyValue{ "RichPixels", "" } } ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      using axis1D =
          Gaudi::Accumulators::Axis<decltype( m_velo_scifi_hits_correlation )::value_type::AxisArithmeticType>;
      m_velo_scifi_hits_correlation.emplace( this, "VeloScifiHitsCorrelation", "VeloScifiHitsCorrelation",
                                             axis1D{ 400, 0, m_velo_max, "Velo Hits" },
                                             axis1D{ 400, 0, m_scifi_max, "Scifi Hits" } );
      m_velo_ut_hits_correlation.emplace( this, "VeloUTHitsCorrelation", "VeloUTHitsCorrelation",
                                          axis1D{ 400, 0, m_velo_max, "Velo Hits" },
                                          axis1D{ 400, 0, m_ut_max, "UT Hits" } );
      m_velo_muon_hits_correlation.emplace( this, "VeloMuonHitsCorrelation", "VeloMuonHitsCorrelation",
                                            axis1D{ 400, 0, m_velo_max, "Velo Hits" },
                                            axis1D{ 400, 0, m_muon_max, "Muon Hits" } );
      m_scifi_ut_hits_correlation.emplace( this, "ScifiUTHitsCorrelation", "ScifiUTHitsCorrelation",
                                           axis1D{ 400, 0, m_scifi_max, "SciFi Hits" },
                                           axis1D{ 400, 0, m_ut_max, "UT Hits" } );
      m_scifi_muon_hits_correlation.emplace( this, "ScifiMuonHitsCorrelation", "ScifiMuonHitsCorrelation",
                                             axis1D{ 400, 0, m_scifi_max, "SciFi Hits" },
                                             axis1D{ 400, 0, m_muon_max, "Muon Hits" } );
      m_ECAL_energy_scifi_hits_correlation.emplace( this, "ECALScifiCorrelation", "ECALScifiCorrelation",
                                                    axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                    axis1D{ 400, 0, m_scifi_max, "SciFi Hits" } );
      m_ECAL_energy_velo_hits_correlation.emplace( this, "ECALVeloCorrelation", "ECALVeloCorrelation",
                                                   axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                   axis1D{ 400, 0, m_velo_max, "Velo Hits" } );
      m_ECAL_energy_ut_hits_correlation.emplace( this, "ECALUTCorrelation", "ECALUTCorrelation",
                                                 axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                 axis1D{ 400, 0, m_ut_max, "UT Hits" } );
      m_ECAL_energy_rich1_hits_correlation.emplace( this, "ECALRICH1Correlation", "ECALRICH1Correlation",
                                                    axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                    axis1D{ 400, 0, m_rich1_max, "RICH1 Hits" } );
      m_ECAL_energy_rich2_hits_correlation.emplace( this, "ECALRICH2Correlation", "ECALRICH2Correlation",
                                                    axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                    axis1D{ 400, 0, m_rich2_max, "RICH2 Hits" } );
      m_scifi_hits_HCAL_energy_correlation.emplace( this, "ScifiHCALCorrelation", "ScifiHCALCorrelation",
                                                    axis1D{ 400, 0, m_scifi_max, "SciFi Hits" },
                                                    axis1D{ 400, 0, m_hcalcounts_max, "Total HCAL ADC counts" } );
      m_ECAL_energy_HCAL_energy_correlation.emplace( this, "ECALHCALCorrelation", "ECALHCALCorrelation",
                                                     axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                     axis1D{ 400, 0, m_hcalcounts_max, "Total HCAL ADC counts" } );
      m_ECAL_energy_ECAL_clusters_correlation.emplace( this, "ECALECALCorrelation", "ECALECALCorrelation",
                                                       axis1D{ 400, 0, m_ecalenergy_max, "ECAL energy" },
                                                       axis1D{ 400, 0, 700, "ECAL Clusters" } );
      m_scifi_rich1_hits_correlation.emplace( this, "ScifiRICH1HitsCorrelation", "ScifiRICH1HitsCorrelation",
                                              axis1D{ 400, 0, m_scifi_max, "SciFi Hits" },
                                              axis1D{ 400, 0, m_rich1_max, "RICH1 Hits" } );
      m_velo_rich1_hits_correlation.emplace( this, "VeloRICH1HitsCorrelation", "VeloRICH1HitsCorrelation",
                                             axis1D{ 400, 0, m_velo_max, "Velo Hits" },
                                             axis1D{ 400, 0, m_rich1_max, "RICH1 Hits" } );
      m_scifi_rich2_hits_correlation.emplace( this, "ScifiRICH2HitsCorrelation", "ScifiRICH2HitsCorrelation",
                                              axis1D{ 400, 0, m_scifi_max, "SciFi Hits" },
                                              axis1D{ 400, 0, m_rich2_max, "RICH1 Hits" } );
    } );
  }
  void operator()( LHCb::Pr::VP::Hits const& velo_hits, LHCb::Pr::FT::Hits const& scifi_hits,
                   UT::HitHandler const& ut_hits, MuonHitContainer const& muon_hits,
                   LHCb::Event::Calo::v2::Clusters const& calo_clusters, LHCb::Event::Calo::Digits const& ecal_digits,
                   LHCb::Event::Calo::Digits const&      hcal_digits,
                   Rich::Future::DAQ::DecodedData const& rich_hits ) const override {

    auto n_muon_hits = muon_hits.hits( 0 ).size() + muon_hits.hits( 1 ).size() + muon_hits.hits( 2 ).size() +
                       muon_hits.hits( 3 ).size();

    uint nClusters = 0;
    for ( const auto& cluster : calo_clusters.scalar() ) {
      if ( cluster.energy() > m_MinEECAL ) { ++nClusters; }
    }

    uint hcal_adc_abovethreshold = 0;
    for ( const auto& digit : hcal_digits ) {
      if ( digit.adc() > m_MinADCHCAL ) { hcal_adc_abovethreshold += digit.adc(); }
    }

    const auto ecaltot = std::accumulate( ecal_digits.begin(), ecal_digits.end(), 0.,
                                          []( auto init, const auto& digit ) { return init + digit.energy(); } );

    ++m_velo_scifi_hits_correlation.value()[{ velo_hits.size(), scifi_hits.size() }];
    ++m_velo_ut_hits_correlation.value()[{ velo_hits.size(), ut_hits.nbHits() }];
    ++m_scifi_ut_hits_correlation.value()[{ scifi_hits.size(), ut_hits.nbHits() }];
    ++m_velo_muon_hits_correlation.value()[{ velo_hits.size(), n_muon_hits }];
    ++m_scifi_muon_hits_correlation.value()[{ scifi_hits.size(), n_muon_hits }];
    ++m_ECAL_energy_velo_hits_correlation.value()[{ ecaltot, velo_hits.size() }];
    ++m_ECAL_energy_ut_hits_correlation.value()[{ ecaltot, ut_hits.nbHits() }];
    ++m_ECAL_energy_scifi_hits_correlation.value()[{ ecaltot, scifi_hits.size() }];
    ++m_ECAL_energy_rich1_hits_correlation.value()[{ ecaltot, rich_hits.nTotalHits( Rich::Rich1 ) }];
    ++m_ECAL_energy_rich2_hits_correlation.value()[{ ecaltot, rich_hits.nTotalHits( Rich::Rich2 ) }];
    ++m_scifi_hits_HCAL_energy_correlation.value()[{ scifi_hits.size(), hcal_adc_abovethreshold }];
    ++m_ECAL_energy_HCAL_energy_correlation.value()[{ ecaltot, hcal_adc_abovethreshold }];
    ++m_ECAL_energy_ECAL_clusters_correlation.value()[{ ecaltot, nClusters }];
    ++m_velo_rich1_hits_correlation.value()[{ velo_hits.size(), rich_hits.nTotalHits( Rich::Rich1 ) }];
    ++m_scifi_rich1_hits_correlation.value()[{ scifi_hits.size(), rich_hits.nTotalHits( Rich::Rich1 ) }];
    ++m_scifi_rich2_hits_correlation.value()[{ scifi_hits.size(), rich_hits.nTotalHits( Rich::Rich2 ) }];
  }

private:
  float m_velo_max;
  float m_scifi_max;
  float m_ut_max;
  float m_muon_max;
  float m_rich1_max;
  float m_rich2_max;
  float m_ecalenergy_max;
  float m_hcalcounts_max;

  Gaudi::Property<CollisionType> m_collision_type{
      this, "CollisionType", CollisionType::pp,
      [this]( auto& ) {
        switch ( m_collision_type ) {
        case CollisionType::pp:
          m_velo_max       = 5000;
          m_scifi_max      = 15000;
          m_ut_max         = 3000;
          m_muon_max       = 1000;
          m_rich1_max      = 15000;
          m_rich2_max      = 8000;
          m_ecalenergy_max = 3500000;
          m_hcalcounts_max = 30000;
          return;
        case CollisionType::PbPb:
          m_velo_max       = 48000;
          m_ut_max         = 10000;
          m_scifi_max      = 32000;
          m_muon_max       = 18000;
          m_rich1_max      = 80000;
          m_rich2_max      = 65000;
          m_ecalenergy_max = 20000000;
          m_hcalcounts_max = 450000;
          return;
        case CollisionType::Unknown:
          throw GaudiException{ "Value for property CollisionType is not helpful", name(), StatusCode::FAILURE };
        }
        throw GaudiException{ "value for property CollisionType does not exist", name(),
                              StatusCode::FAILURE }; // this will actually never be reached, as the parsing of the
                                                     // `CollisionType` enum will (should!) fail before we ever get
                                                     // here...
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };
  Gaudi::Property<int>   m_MinADCHCAL{ this, "MinADCHCAL", 10 }; // Minimum ADC threshold to have some zero suppression
  Gaudi::Property<float> m_MinEECAL{ this, "MinEECAL", 100 * Gaudi::Units::MeV }; // Minimum energy threshold to have
                                                                                  // some zero suppression

  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_velo_scifi_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_velo_ut_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_velo_muon_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_scifi_ut_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_scifi_muon_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_scifi_hits_HCAL_energy_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_HCAL_energy_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_ECAL_clusters_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_velo_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_scifi_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_ut_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_rich1_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_ECAL_energy_rich2_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_scifi_rich1_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_velo_rich1_hits_correlation;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_scifi_rich2_hits_correlation;
};

class MonitorTrackECALCorrelations
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, LHCb::Event::Calo::Digits const& )> {
public:
  MonitorTrackECALCorrelations( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "TracksInContainer", LHCb::TrackLocation::Default }, KeyValue{ "ECALDigits", "" } } ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      // using axis1D =
      //     Gaudi::Accumulators::Axis<decltype( m_ECAL_energy_nvelotracks_correlation
      //     )::value_type::AxisArithmeticType>;
      using Axis1D = Gaudi::Accumulators::Axis<double>;

      m_ECAL_energy_nvelotracks_correlation.setTitle( "VELOTracksECALCorrelation" );
      m_ECAL_energy_nvelotracks_correlation.setAxis<0>( Axis1D( Gaudi::Histo1DDef( 0, m_ecalenergy_max, 100 ) ) );
      m_ECAL_energy_nvelotracks_correlation.setAxis<1>( Axis1D( Gaudi::Histo1DDef( -1, m_velotracks_max, 101 ) ) );

      m_ECAL_energy_longtracks_correlation.setTitle( "LongTracksECALCorrelation" );
      m_ECAL_energy_longtracks_correlation.setAxis<0>( Axis1D( Gaudi::Histo1DDef( 0, m_ecalenergy_max, 100 ) ) );
      m_ECAL_energy_longtracks_correlation.setAxis<1>( Axis1D( Gaudi::Histo1DDef( -1, m_longtracks_max, 101 ) ) );

      // m_ECAL_energy_nvelotracks_correlation.emplace( this, "VELOTracksECALCorrelation", "VeloTracksECALCorrelation",
      //                                                axis1D{ 100, 0, m_ecalenergy_max, "ECAL energy" },
      //                                                axis1D{ 101, -1, m_velotracks_max, "Velo Tracks" } );
      // m_ECAL_energy_longtracks_correlation.emplace( this, "LongTracksECALCorrelation", "LongTracksECALCorrelation",
      //                                               axis1D{ 100, 0, m_ecalenergy_max, "ECAL energy" },
      //                                               axis1D{ 101, -1, m_longtracks_max, "Long Tracks" } );
    } );
  }
  void operator()( LHCb::Track::Range const& tracks, LHCb::Event::Calo::Digits const& ecal_digits ) const override {
    std::array<unsigned int, int( LHCb::Event::Enum::Track::Type::Last )> multiplicityMap = {};
    for ( const LHCb::Track* track : tracks ) {
      // auto& histos = m_histogrammap[int( track->type() )];
      // if ( !histos ) continue;
      multiplicityMap[int( track->type() )] += 1;
    }
    const auto count_velo = multiplicityMap[1]; // VELO track LHCb::Event::Enum::Track::Type
    const auto count_long = multiplicityMap[3]; // Long track LHCb::Event::Enum::Track::Type
    const auto ecaltot    = std::accumulate( ecal_digits.begin(), ecal_digits.end(), 0.,
                                             []( auto init, const auto& digit ) { return init + digit.energy(); } );
    ++m_ECAL_energy_nvelotracks_correlation[{ ecaltot, count_velo }];
    ++m_ECAL_energy_longtracks_correlation[{ ecaltot, count_long }];
  }

private:
  float m_velotracks_max;
  float m_longtracks_max;
  float m_ecalenergy_max;

  Gaudi::Property<CollisionType> m_collision_type{
      this, "CollisionType", CollisionType::pp,
      [this]( auto& ) {
        switch ( m_collision_type ) {
        case CollisionType::pp:
          m_velotracks_max = 600;
          m_longtracks_max = 300;
          m_ecalenergy_max = 3500000;
          return;
        case CollisionType::PbPb:
          m_velotracks_max = 3000;
          m_longtracks_max = 1500;
          m_ecalenergy_max = 20000000;
          return;
        case CollisionType::Unknown:
          throw GaudiException{ "Value for property CollisionType is not helpful", name(), StatusCode::FAILURE };
        }
        throw GaudiException{ "value for property CollisionType does not exist", name(),
                              StatusCode::FAILURE }; // this will actually never be reached, as the parsing of the
                                                     // `CollisionType` enum will (should!) fail before we ever get
                                                     // here...
      },
      Gaudi::Details::Property::ImmediatelyInvokeHandler{ true } };

  mutable Gaudi::Accumulators::Histogram<2> m_ECAL_energy_nvelotracks_correlation{ this, "VELOTracksECALCorrelation" };
  mutable Gaudi::Accumulators::Histogram<2> m_ECAL_energy_longtracks_correlation{ this, "LongTracksECALCorrelation" };
};

DECLARE_COMPONENT( MonitorDetectorCorrelations )
DECLARE_COMPONENT( MonitorTrackECALCorrelations )
