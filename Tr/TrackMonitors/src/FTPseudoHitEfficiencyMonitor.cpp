/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/Traits.h"

#include "DetDesc/DetectorElement.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/PrHits.h"
#include "FTDet/DeFTDetector.h"
#include "FTDet/DeFTMat.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Event/PrHits.h"
#include "Event/Track.h"

#include "Gaudi/Accumulators/Histogram.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackInterpolator.h"

#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

/**
 * Algorithm based on the track-based efficiencies, in particular the
 * OTHitEfficiencyMonitor
 * conceptualised by Wouter Hulsbergen, modified by Francesco Dettori,
 * and others in the next generations.
 *
 * Written a SciFi-version by Laurent Dufour, Lukas Witola, Zehua Xu.
 *
 * Yingrui Hou extended the default scifi hit efficiency monitor to include an online version named
 *"FTPseudoHitEfficiency", featuring the hit efficiency hitmap figure for MONET.
 *
 * A lightweight version, in which only the 1D and 2D histograms are
 * created, can be used.
 *
 **/
class FTPseudoHitEfficiencyMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, const LHCb::Pr::FT::Hits&,
                                             const DetectorElement&, const DeFT& ),
                                       LHCb::Algorithm::Traits::usesConditions<DetectorElement, DeFT>> {
private:
  Gaudi::Property<std::vector<unsigned int>> m_layersUnderStudy{ this, "LayersUnderStudy" };
  Gaudi::Property<float>                     m_maxDoca{ this, "MaxDoca", 2.5 * Gaudi::Units::mm };
  Gaudi::Property<float>                     m_maxTrackCov{ this, "MaxTrackCov", 0.5 * Gaudi::Units::mm };
  Gaudi::Property<float>                     m_trackMaxChi2PerDoF{ this, "MaxTrackChi2PerDof", 3. };
  Gaudi::Property<float>                     m_trackMinP{ this, "MinTrackP", 2000 * Gaudi::Units::MeV };
  Gaudi::Property<float>                     m_trackMinPt{ this, "MinTrackPT", 400 * Gaudi::Units::MeV };

  ToolHandle<ITrackInterpolator> m_interpolator{ this, "Interpolator", "TrackInterpolator" };
  ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "Extrapolator", "TrackLinearExtrapolator" };

  // For plotting the 2D map, copied from FTLiteClusterMonitor
  // Plot options
  static constexpr int   m_v_gap    = 1;   // number empty bins on top/bottom
  static constexpr int   m_h_gap    = 12;  // number empty bins on left/right
  static constexpr int   m_l_gap    = 0;   // number empty bins between layers
  static constexpr float m_l_center = 0.5; // central position of 1st layer
  // Transformation factor
  static constexpr float m_sipm_to_module   = 1. / FTConstants::nSiPMsPerModule;
  static constexpr float m_quarter_to_layer = 1. / ( FTConstants::nHalfLayers + m_l_gap );
  // Axis
  static constexpr unsigned int m_2DMap_xbins = 2u * ( FTConstants::nMaxSiPMsPerQuarter + m_h_gap );
  static constexpr float        m_2DMap_xmin  = -( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module );
  static constexpr float        m_2DMap_xmax  = +( FTConstants::nModulesMax + m_h_gap * m_sipm_to_module );
  static constexpr unsigned int m_2DMap_ybins = ( FTConstants::nHalfLayers * FTConstants::nLayersTotal +
                                                  m_l_gap * ( FTConstants::nLayersTotal - 1 ) + 2 * m_v_gap );
  static constexpr float        m_2DMap_ymin  = -m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center;
  static constexpr float        m_2DMap_ymax =
      +m_quarter_to_layer * ( 1 + m_v_gap ) + m_l_center + FTConstants::nLayersTotal - 1;

  mutable Gaudi::Accumulators::ProfileHistogram<2, Gaudi::Accumulators::atomicity::full, float> m_efficiencyPerSiPM_map{
      this,
      "EfficiencyPerSiPM",
      "Efficiency Per SiPM; Module; Layer ID",
      { m_2DMap_xbins, m_2DMap_xmin, m_2DMap_xmax },
      { m_2DMap_ybins, m_2DMap_ymin, m_2DMap_ymax } };
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsSipm{
      this,
      "EfficiencyVsSipm",
      "Efficiency vs Sipm;GlobalSipmIdx;Efficiency",
      { FTConstants::nSiPMsTotal, 0, FTConstants::nSiPMsTotal } };

  struct LayerHistograms {
    LayerHistograms( FTPseudoHitEfficiencyMonitor* parent, const std::string& prefix )
        : efficiencyVsXY{ parent,
                          prefix + "EfficiencyVsXY",
                          "Efficiency vs XY; X [mm];Y [mm]",
                          { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm },
                          { 124, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm } }
        , efficiencyVsX{ parent,
                         prefix + "EfficiencyVsX",
                         "Efficiency vs X;X [mm];Efficiency",
                         { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm } }
        , efficiencyVsModule{ parent,
                              prefix + "EfficiencyVsModule",
                              "Efficiency vs Module;GlobalModuleIdx;Efficiency",
                              { FTConstants::nModulesTotal, 0, FTConstants::nModulesTotal } }
        , efficiencyVsMat{ parent,
                           prefix + "EfficiencyVsMat",
                           "Efficiency vs Mat;GloalMatIdx;Efficiency",
                           { FTConstants::nMatsTotal, 0, FTConstants::nMatsTotal } }
        , efficiencyVsSipm{ parent,
                            prefix + "EfficiencyVsSipm",
                            "Efficiency vs Sipm;GlobalSipmIdx;Efficiency",
                            { FTConstants::nSiPMsTotal, 0, FTConstants::nSiPMsTotal } }
        , efficiencyVsLocalChannel{ parent,
                                    prefix + "EfficiencyVsLocalChannel",
                                    "Efficiency vs Channel;LocalChannelIdx;Efficiency",
                                    { FTConstants::nChannels, 0, FTConstants::nChannels } }
        , doca{ parent,
                prefix + "Doca",
                "DOCA;DOCA [mm];Entries",
                { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } }
        , docaX{ parent,
                 prefix + "DocaX",
                 "DOCA X;DOCA X [mm];Entries",
                 { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } }
        , docaY{ parent,
                 prefix + "DocaY",
                 "DOCA Y;DOCA Y [mm];Entries",
                 { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } }
        , docaZ{ parent,
                 prefix + "DocaZ",
                 "DOCA Z;DOCA Z [mm];Entries",
                 { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } } {};
    mutable Gaudi::Accumulators::ProfileHistogram<2> efficiencyVsXY;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsX;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsModule;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsMat;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsSipm;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsLocalChannel;
    mutable Gaudi::Accumulators::Histogram<1>        doca;
    mutable Gaudi::Accumulators::Histogram<1>        docaX;
    mutable Gaudi::Accumulators::Histogram<1>        docaY;
    mutable Gaudi::Accumulators::Histogram<1>        docaZ;
  };

  std::map<unsigned int, LayerHistograms> m_layerhistograms;

  /* calculate the binX and binY by the channelID, copied from the FTLiteClusterMonitor */
  std::pair<double, double> getChannelXY( LHCb::Detector::FTChannelID channelID ) const {
    const auto quarter_sipm_idx = channelID.localSiPMIdx_quarter();
    const auto global_layer_idx = channelID.globalLayerIdx();
    const auto quarter_idx      = static_cast<unsigned int>( channelID.quarter() );
    double     x                = ( quarter_sipm_idx + 0.5f ) * m_sipm_to_module;
    if ( quarter_idx % 2 == 0 ) { // mirror C Side
      x *= -1;
    }
    const double y = global_layer_idx + m_l_center + 0.5 * m_quarter_to_layer * ( quarter_idx < 2 ? -1 : 1 );
    return std::make_pair( x, y );
  };

  std::vector<unsigned int> IsSciFiLayers( const DeFT& ftDet, const std::vector<unsigned int>& layers ) const {
    std::vector<unsigned int> SciFiLayers;
    for ( auto layer : layers ) {
      const auto& ftStation = ftDet.getStation( layer / 4 );
      if ( !ftStation ) {
        ++m_failed_to_find_station;
        continue;
      }
      const auto& ftLayer = ftStation->getLayer( layer % 4 );
      if ( !ftLayer ) {
        ++m_failed_to_find_layer;
        continue;
      }
      SciFiLayers.push_back( layer );
    }
    return SciFiLayers;
  };

  // Debug informations
  mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG> m_failed_to_interpolate{ this, "Failed to interpolate track." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG> m_conv_too_large{ this, "Covariance too large." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG> m_failed_to_extrapolate_layer{
      this, "Failed to extrapolate track to layer." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG> m_no_mat{ this, "Failed to find a corresponding SciFi mat." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG> m_failed_to_extrapolate_mat{
      this, "Failed to extrapolate track to mat." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_parallel{ this, "Track and hit are parallel." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_failed_to_find_layer{ this, "Failed to find SciFi layer." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_failed_to_find_station{ this, "Failed to find SciFi station." };
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_failed_to_find_input_layer{
      this, "Failed to find SciFi layer from the input layer list." };

public:
  FTPseudoHitEfficiencyMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "TrackLocation", "" }, KeyValue{ "PrFTHitsLocation", "" },
                    KeyValue{ "LHCbGeoLocation", LHCb::standard_geometry_top },
                    KeyValue{ "FTDetectorLocation", DeFTDetectorLocation::Default } } ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      for ( auto layer : m_layersUnderStudy ) {
        m_layerhistograms.emplace( std::piecewise_construct, std::forward_as_tuple( layer ),
                                   std::forward_as_tuple( this, fmt::format( "Layer{}", layer ) ) );
      }
    } );
  }

  void operator()( const LHCb::Track::Range& tracks, const LHCb::Pr::FT::Hits& hits, const DetectorElement& lhcb,
                   const DeFT& ftDet ) const override {

    // Get the station & layer under study
    auto SciFiLayers = IsSciFiLayers( ftDet, m_layersUnderStudy );
    if ( SciFiLayers.size() == 0 ) {
      ++m_failed_to_find_input_layer;
      return;
    }

    // Match the hits to the certain Mat
    std::unordered_map<unsigned int, std::vector<int>> tmp_TrackHit;
    for ( unsigned int index = 0; index < hits.size(); ++index ) {
      auto MatID_hit = hits.id( index ).globalMatID();
      tmp_TrackHit[MatID_hit].push_back( index );
    }

    for ( const auto& layer : SciFiLayers ) {
      const auto& ftStation = ftDet.getStation( layer / 4 );
      const auto& ftLayer   = ftStation->getLayer( layer % 4 );
      for ( const auto& track : tracks ) {
        // Basic selection of the tracks
        if ( m_trackMaxChi2PerDoF.value() > 0 && track->chi2PerDoF() > m_trackMaxChi2PerDoF.value() ) continue;
        if ( m_trackMinP.value() > 0 && track->p() < m_trackMinP.value() ) continue;
        if ( m_trackMinPt.value() > 0 && track->pt() < m_trackMinPt.value() ) continue;
        // Interpolate the track to the z-position of the layer under study
        LHCb::State state;
        if ( !m_interpolator->interpolate( *track, ftLayer->globalZ(), state, lhcb ).isSuccess() ) {
          ++m_failed_to_interpolate;
          continue;
        }
        // Only consider if state has reasonably small error
        if ( std::sqrt( state.covariance()( 0, 0 ) ) > m_maxTrackCov.value() ) {
          ++m_conv_too_large;
          continue;
        }
        // To get a slightly more precise answer, intersect with the plane of the layer.
        // Use a simple linear extrapolation here
        if ( !m_extrapolator->propagate( state, ftLayer->plane(), lhcb ).isSuccess() ) {
          ++m_failed_to_extrapolate_layer;
          continue;
        }
        // Find the mat that the track intersects with
        const auto& ftMat = ftLayer->findMatWithXY( state.position() );
        if ( !ftMat ) {
          ++m_no_mat;
          continue;
        }
        // Extrapolate the state to the mat
        if ( !m_extrapolator->propagate( state, ftMat->plane(), lhcb ).isSuccess() ) {
          ++m_failed_to_extrapolate_mat;
          continue;
        }
        auto trackLine   = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>( state.position(), state.slopes() );
        auto MatID_track = ftMat->elementID().globalMatID();
        auto [channelID, fraction] = ftMat->calculateChannelAndFrac( ftMat->toLocal( state.position() ).x() );
        auto  it                   = tmp_TrackHit.find( MatID_track );
        auto& layerhists           = m_layerhistograms.at( layer );
        float matchedHit           = 0;
        if ( it != tmp_TrackHit.end() ) {
          for ( const auto& hit_index : it->second ) {
            auto [y0, y1] = hits.yEnd( hit_index );
            // Get the trajectory of the SciFi hit
            auto hitLine = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>(
                Gaudi::XYZPoint( hits.x( hit_index, y0 ), y0, hits.z( hit_index, y0 ) ),
                Gaudi::XYZPoint( hits.x( hit_index, y1 ), y1, hits.z( hit_index, y1 ) ) );
            Gaudi::XYZPoint trackPoint;
            Gaudi::XYZPoint hitPoint;
            if ( !Gaudi::Math::closestPoints( trackLine, hitLine, trackPoint, hitPoint ) ) {
              ++m_parallel;
              continue;
            }
            auto docaVector = ( trackPoint - hitPoint );
            auto signedDoca = ( docaVector.x() / std::abs( docaVector.x() ) ) * docaVector.R();
            if ( std::abs( signedDoca ) <= m_maxDoca ) { matchedHit = 1; }

            // DOCA histograms
            ++layerhists.doca[signedDoca];
            ++layerhists.docaX[docaVector.x()];
            ++layerhists.docaY[docaVector.y()];
            ++layerhists.docaZ[docaVector.z()];
          }
        }
        // Efficiency histograms
        layerhists.efficiencyVsXY[{ state.x(), state.y() }] += matchedHit;
        layerhists.efficiencyVsX[state.x()] += matchedHit;
        auto [x, y] = getChannelXY( channelID );
        m_efficiencyPerSiPM_map[{ x, y }] += matchedHit;
        m_efficiencyVsSipm[channelID.globalSipmIdx()] += matchedHit;
        layerhists.efficiencyVsModule[channelID.globalModuleIdx()] += matchedHit;
        layerhists.efficiencyVsMat[channelID.globalMatIdx()] += matchedHit;
        layerhists.efficiencyVsSipm[channelID.globalSipmIdx()] += matchedHit;
        layerhists.efficiencyVsLocalChannel[channelID.localChannelIdx()] += matchedHit;
      }
    }
  }
};

DECLARE_COMPONENT( FTPseudoHitEfficiencyMonitor )
