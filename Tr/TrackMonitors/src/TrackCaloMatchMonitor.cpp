/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/DetectorElement.h"
#include "Event/CaloCluster.h"
#include "Event/CaloPosition.h"
#include "Event/KalmanFitResult.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/State.h"
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "LHCbAlgs/Consumer.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackKernel/TrackFunctors.h"

#include <Gaudi/Accumulators/Histogram.h>
#include <optional>

namespace {
  using GeometricZ = double;
} // namespace

namespace LHCb {

  class TrackCaloMatchMonitor
      : public Algorithm::Consumer<void( CaloClusters const&, Track::Range const&, DetectorElement const&,
                                         GeometricZ const& ),
                                   Algorithm::Traits::usesConditions<DetectorElement, GeometricZ>> {
  public:
    TrackCaloMatchMonitor( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    { { "CaloClustersLocation", CaloClusterLocation::Ecal },
                      { "TrackLocation", TrackLocation::Default },
                      { "StandardGeometryTop", standard_geometry_top },
                      { "GeometricZLocation", name + "GeometricZLocation" } } ) {}
    StatusCode initialize() override;
    void       operator()( CaloClusters const&, Track::Range const&, DetectorElement const&,
                     GeometricZ const& ) const override;

  private:
    Gaudi::Property<bool>          m_requireTHits{ this, "RequireTHits", true }; // this you need for cosmics (MIPs)
    Gaudi::Property<double>        m_clusterZCorrection{ this, "ClusterZCorrection", -60 * Gaudi::Units::mm };
    ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "Extrapolator", "TrackMasterExtrapolator" };

    struct RegionHistos {
      std::unique_ptr<Gaudi::Accumulators::Histogram<1>> histos[3];
      template <typename Parent>
      RegionHistos( Parent* p, const char* nameformat, const char* titleformat, double xmin, double xmax ) {
        const char regiontitles[3][256] = { "outer", "middle", "inner" };
        char       name[256];
        char       title[256];
        for ( int i = 0; i < 3; ++i ) {
          sprintf( name, nameformat, regiontitles[i] );
          sprintf( title, titleformat, regiontitles[i] );
          histos[i].reset( new Gaudi::Accumulators::Histogram<1>{ p, name, title, { 100, xmin, xmax } } );
          ;
        }
      }
      auto& operator[]( unsigned i ) { return histos[i]; }
    };

    mutable RegionHistos m_dxASideH1{ this, "dxASide_%s", "xEcal - xTrk (%s A-side)", -200, 200 };
    mutable RegionHistos m_dxCSideH1{ this, "dxCSide_%s", "xEcal - xTrk (%s C-side)", -200, 200 };
    mutable RegionHistos m_dyASideH1{ this, "dyASide_%s", "yEcal - yTrk (%s A-side)", -200, 200 };
    mutable RegionHistos m_dyCSideH1{ this, "dyCSide_%s", "yEcal - yTrk (%s C-side)", -200, 200 };
    mutable RegionHistos m_dyVeloASideH1{ this, "dyVeloASide_%s", "yEcal - yVeloTrk (%s A-side)", -200, 200 };
    mutable RegionHistos m_dyVeloCSideH1{ this, "dyVeloCSide_%s", "yEcal - yVeloTrk (%s C-side)", -200, 200 };
    mutable RegionHistos m_eOverPH1{ this, "eop_%s", "E/p (%s)", -2, 2 };
    mutable RegionHistos m_zH1{ this, "zmatch_%s", "ZMatch (%s)", 12e3, 14e3 };
    mutable RegionHistos m_deltazH1{ this, "dzmatch_%s", "zEcal - zMatch (%s)", -400, 400 };

    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dxVsXPr{
        this, "dxVsX", "xEcal - xTrk vs x", { 50, -3500, 3500 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dxVsYPr{
        this, "dxVsY", "xEcal - xTrk vs y", { 50, -3500, 3500 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVsXPr{
        this, "dyVsX", "yEcal - yTrk vs x", { 50, -3500, 3500 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVsYPr{
        this, "dyVsY", "yEcal - yTrk vs y", { 50, -3500, 3500 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dxVsTxPr{
        this, "dxVsTx", "xEcal - xTrk vs tx", { 50, -0.6, 0.6 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dxVsTyPr{
        this, "dxVsTy", "xEcal - xTrk vs ty", { 50, -0.3, 0.3 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVsTxPr{
        this, "dyVsTx", "yEcal - yTrk vs tx", { 50, -0.6, 0.6 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVsTyPr{
        this, "dyVsTy", "yEcal - yTrk vs ty", { 50, -0.3, 0.3 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVeloVsXPr{
        this, "dyVeloVsX", "yEcal - yTrk vs x", { 50, -3500, 3500 } };
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_dyVeloVsYPr{
        this, "dyVeloVsY", "yEcal - yTrk vs y", { 50, -3500, 3500 } };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( TrackCaloMatchMonitor, "TrackEcalMatchMonitor" )

} // namespace LHCb

StatusCode LHCb::TrackCaloMatchMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    addConditionDerivation( { DeCalorimeterLocation::Ecal }, inputLocation<GeometricZ>(),
                            [/*this*/]( DeCalorimeter const& caloDet ) {
                              const auto geoZ = caloDet.toGlobal( Gaudi::XYZPoint() ).z() + caloDet.zOffset();
                              // For now we lost the functionality to set the zrange properly
                              //  m_zH1[i] = book1D( histitle, geoZ - 400, geoZ + 400 );
                              return geoZ;
                            } );
    return StatusCode::SUCCESS;
  } );
}

namespace {

  struct MyCaloPosition {
    MyCaloPosition( const LHCb::Detector::Calo::CellID& _cell, const LHCb::CaloPosition& _pos )
        : cell( _cell ), pos( _pos ) {}
    LHCb::Detector::Calo::CellID cell;
    LHCb::CaloPosition           pos;
  };

  using namespace LHCb::Pr::Tracks::Fit;
  using namespace LHCb;

  template <typename FitResult>
  std::optional<LHCb::State> unbiasedEndVeloState( const FitResult& fitresult ) {
    std::optional<LHCb::State> velostate;
    const auto                 nodes_ = nodes( fitresult );
    // find the most-downstream node still within velo
    using NodePointerType = decltype( &( *nodes_.begin() ) );
    const auto node = std::accumulate( nodes_.begin(), nodes_.end(), static_cast<const NodePointerType>( nullptr ),
                                       []( const auto* n, const auto& i ) {
                                         if ( i.z() >= StateParameters::ZEndVelo ) return n;
                                         return ( !n || ( i.z() > n->z() ) ) ? &i : n;
                                       } );
    if ( node ) {
      const bool upstream = nodes_.front().z() > nodes_.back().z();
      velostate           = upstream ? filteredStateBackward( *node ) : filteredStateForward( *node );
    }
    return velostate;
  }

  auto unbiasedEndVeloState( const LHCb::Track& track ) {
    std::optional<LHCb::State> velostate;
    const auto*                fr = track.fitResult();
    if ( track.hasVelo() && fr ) {
      const auto* prkfr = dynamic_cast<const LHCb::PrKalmanFitResult*>( fr );
      if ( prkfr )
        velostate = unbiasedEndVeloState( *prkfr );
      else {
        const auto* tmffr = dynamic_cast<const LHCb::KalmanFitResult*>( fr );
        if ( tmffr ) velostate = unbiasedEndVeloState( *tmffr );
      }
    }
    return velostate;
  }

} // namespace

void LHCb::TrackCaloMatchMonitor::operator()( CaloClusters const& clusters, Track::Range const& trackcontainer,
                                              DetectorElement const& lhcb, GeometricZ const& geometricZ ) const {
  auto& geometry = *lhcb.geometry();
  // make a list of calo positions, depending on the subsystem use clusters or cells
  std::vector<MyCaloPosition> calopositions;

  calopositions.reserve( clusters.size() );
  for ( const CaloCluster* cluster : clusters ) { calopositions.emplace_back( cluster->seed(), cluster->position() ); }

  for ( MyCaloPosition& cluster : calopositions ) cluster.pos.setZ( geometricZ );

  for ( const Track* track : trackcontainer ) {
    if ( !m_requireTHits || track->hasT() ) {
      const State& closest = closestState( *track, geometricZ );
      StateVector  state   = { closest.stateVector(), closest.z() };
      m_extrapolator->propagate( state, geometricZ, geometry ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

      const auto                       fullvelostate = unbiasedEndVeloState( *track );
      std::optional<LHCb::StateVector> velostate;
      if ( fullvelostate ) {
        velostate = LHCb::StateVector( fullvelostate->stateVector(), fullvelostate->z() );
        velostate->setQOverP( state.qOverP() );
        auto sc = m_extrapolator->propagate( *velostate, geometricZ, geometry );
        if ( !sc.isSuccess() ) velostate.reset();
      }

      constexpr double maxxbyregion[3] = { 100., 60., 30. };
      constexpr double maxybyregion[3] = { 100., 60., 30. };

      for ( const MyCaloPosition& cluster : calopositions ) {
        // state = &(closestState(*track,pos.z())) ;
        const double dz     = cluster.pos.z() + m_clusterZCorrection.value() - state.z();
        const double xtrack = state.x() + state.tx() * dz;
        const double ytrack = state.y() + state.ty() * dz;
        const double dx     = cluster.pos.x() - xtrack;
        const double dy     = cluster.pos.y() - ytrack;

        if ( std::abs( dy ) < 200 && std::abs( dx ) < 200 ) {
          const bool goodymatch = std::abs( dz ) < maxybyregion[cluster.cell.area()];
          const bool goodxmatch = std::abs( dz ) < maxxbyregion[cluster.cell.area()];
          if ( xtrack > 0 ) {
            ( *m_dxASideH1[cluster.cell.area()] )[dx]++;
            ( *m_dyASideH1[cluster.cell.area()] )[dy]++;
          } else {
            ( *m_dxCSideH1[cluster.cell.area()] )[dx]++;
            ( *m_dyCSideH1[cluster.cell.area()] )[dy]++;
          }
          ( *m_eOverPH1[cluster.cell.area()] )[cluster.pos.e() * state.qOverP()]++;

          // compute the z-coordinate for which sqrt(dx^2+dy^2) is minimal
          const double tx  = state.tx();
          const double ty  = state.ty();
          const double ddz = ( tx * dx + ty * dy ) / ( tx * tx + ty * ty );
          ( *m_zH1[cluster.cell.area()] )[cluster.pos.z() + ddz]++;
          ( *m_deltazH1[cluster.cell.area()] )[ddz]++;
          // fill some profile histograms
          if ( std::abs( dy ) < 100 && std::abs( dx ) < 100 ) {
            if ( goodymatch ) {
              m_dxVsXPr[xtrack] += dx;
              m_dxVsYPr[ytrack] += dx;
              m_dxVsTxPr[state.tx()] += dx;
              m_dxVsTyPr[state.ty()] += dx;
            }
            if ( goodxmatch ) {
              m_dyVsXPr[xtrack] += dy;
              m_dyVsYPr[ytrack] += dy;
              m_dyVsTxPr[state.tx()] += dy;
              m_dyVsTyPr[state.ty()] += dy;
            }
          }

          if ( velostate && std::abs( dx ) < 100 ) {
            const auto ytrack = velostate->y() + velostate->ty() * dz;
            const auto dy     = cluster.pos.y() - ytrack;
            if ( cluster.pos.x() > 0 ) {
              ( *m_dyVeloASideH1[cluster.cell.area()] )[dy]++;
            } else {
              ( *m_dyVeloCSideH1[cluster.cell.area()] )[dy]++;
            }
            if ( goodxmatch ) {
              m_dyVeloVsXPr[xtrack] += dy;
              m_dyVeloVsYPr[ytrack] += dy;
            }
          }
        }
      }
    }
  }
}
