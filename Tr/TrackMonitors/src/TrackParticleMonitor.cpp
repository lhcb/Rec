/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "../../../Phys/JetAccessories/src/JetUtils.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/ChiSquare.h"
#include "Event/Particle.h"
#include "Event/ParticleUtils.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Consumer.h"
#include "Magnet/DeMagnet.h"
#include "TrackInterfaces/ITrackStateProvider.h"
#include "TrackKernel/TrackStateVertex.h"
#include "TrackKernel/TrackTraj.h"

namespace {

  std::pair<std::vector<LHCb::Track const*>, std::vector<double>>
  addTracks( const LHCb::Particle& p, std::vector<const LHCb::Track*>& tracks, std::vector<double>& masshypos ) {
    std::pair<std::vector<const LHCb::Track*>, std::vector<double>> r( tracks, masshypos );
    for ( const LHCb::Particle* c : LHCb::Utils::Particle::walk( p ) ) {
      if ( !c->proto() || !c->proto()->track() ) continue; // <<-- skip particles that do not correspond to a track
      tracks.push_back( c->proto()->track() );
      masshypos.push_back( c->momentum().M() );
    }
    return r;
  }

} // namespace

// TrackParticleMonitor class definition
class TrackParticleMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range&, const DetectorElement&, const DeMagnet& ),
                                       LHCb::Algorithm::Traits::usesConditions<DetectorElement, DeMagnet>> {

public:
  // Basic constructor
  TrackParticleMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  // Algorithm initialize
  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      m_histograms_track_vertex.emplace(
          std::piecewise_construct, std::forward_as_tuple( 0 ),
          std::forward_as_tuple( this, m_vertex_x_range, m_vertex_y_range, m_vertex_z_range ) );
      m_histograms_bias.emplace(
          std::piecewise_construct, std::forward_as_tuple( 0 ),
          std::forward_as_tuple( this, m_min_mass / Gaudi::Units::GeV, m_max_mass / Gaudi::Units::GeV,
                                 m_pdiff_range / Gaudi::Units::GeV, m_massbins, m_pdiff_bins, m_bins_angles ) );
      m_histograms_mass_vs_bias.emplace(
          std::piecewise_construct, std::forward_as_tuple( 0 ),
          std::forward_as_tuple( this, m_particle, m_pdiff_bins, m_pdiff_range / Gaudi::Units::GeV, m_bins_angles,
                                 m_minp / Gaudi::Units::GeV, m_maxp / Gaudi::Units::GeV, m_minpt / Gaudi::Units::GeV,
                                 m_maxpt / Gaudi::Units::GeV ) );
      if ( m_2Dhistos.value() ) {
        m_histograms_mass_vs_bias_2D.emplace(
            std::piecewise_construct, std::forward_as_tuple( 0 ),
            std::forward_as_tuple( this, m_pdiff_bins, m_pdiff_range / Gaudi::Units::GeV, m_bins_angles,
                                   m_minp / Gaudi::Units::GeV, m_maxp / Gaudi::Units::GeV, m_minpt / Gaudi::Units::GeV,
                                   m_maxpt / Gaudi::Units::GeV, m_min_mass / Gaudi::Units::GeV,
                                   m_max_mass / Gaudi::Units::GeV, m_massbins ) );
      }
    } );
  }

  // Algorithm execute
  void operator()( LHCb::Particle::Range const& particles, DetectorElement const& lhcb,
                   DeMagnet const& magnet ) const override;

private:
  // Tool to obtain track states
  ToolHandle<ITrackStateProvider> m_stateprovider{ "TrackStateProvider" };

  // Particle properties handle
  ServiceHandle<LHCb::IParticlePropertySvc> m_propertysvc{ this, "ParticlePropertySvc", "LHCb::ParticlePropertySvc" };

  // Histogram settings. Default values are the standard ones for D0 -> KPi candidates
  Gaudi::Property<unsigned int> m_pdiff_bins{ this, "PdiffBins", 20 };
  Gaudi::Property<unsigned int> m_bins_angles{ this, "AngularBins", 20 };
  Gaudi::Property<float>        m_min_mass{ this, "MinMass", 1.835 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_max_mass{ this, "MaxMass", 1.895 * Gaudi::Units::GeV };
  Gaudi::Property<unsigned int> m_massbins{ this, "BinsMass", 100 };
  Gaudi::Property<float>        m_minpt{ this, "MinPt", 0 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_maxpt{ this, "MaxPt", 30 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_minp{ this, "MinP", 0 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_maxp{ this, "MaxP", 200 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_pdiff_range{ this, "PdiffDelta", 100 * Gaudi::Units::GeV };
  Gaudi::Property<float>        m_vertex_x_range{ this, "VertexDeltaX", 2.5 * Gaudi::Units::mm };
  Gaudi::Property<float>        m_vertex_y_range{ this, "VertexDeltaY", 2 * Gaudi::Units::mm };
  Gaudi::Property<float>        m_vertex_z_range{ this, "VertexDeltaZ", 200 * Gaudi::Units::mm };
  Gaudi::Property<std::string>  m_particle{ this, "ParticleSpecies", "D0" };

  // Flag for 2D histograms
  Gaudi::Property<bool> m_2Dhistos{ this, "Include2DHistos", false };

  // Track and vertex histograms
  struct TrackVertexHistos {
    mutable Gaudi::Accumulators::Histogram<1> track_chi2PerDoF;
    mutable Gaudi::Accumulators::Histogram<1> track_pt;
    mutable Gaudi::Accumulators::Histogram<1> track_p;
    mutable Gaudi::Accumulators::Histogram<1> vertex_chi2;
    mutable Gaudi::Accumulators::Histogram<1> vertex_chi2prob;
    mutable Gaudi::Accumulators::Histogram<1> vertex_x_pos;
    mutable Gaudi::Accumulators::Histogram<1> vertex_y_pos;
    mutable Gaudi::Accumulators::Histogram<1> vertex_z_pos;
    mutable Gaudi::Accumulators::Histogram<1> vertex_x_error;
    mutable Gaudi::Accumulators::Histogram<1> vertex_y_error;
    mutable Gaudi::Accumulators::Histogram<1> vertex_z_error;

    TrackVertexHistos( const TrackParticleMonitor* owner, float const& x_range, float const& y_range,
                       float const& z_range )
        : track_chi2PerDoF{ owner, "trackChi2", "Track #chi^2 per dof", { 100, 0, 5, "#chi^2 / nDoF" } }
        , track_pt{ owner, "trackPt", "Track p_{T}", { 100, 0, 20, "p_{T} [GeV]" } }
        , track_p{ owner, "trackP", "Track momentum", { 100, 0, 100, "p [GeV]" } }
        , vertex_chi2{ owner, "chi2", "vertex chi2", { 100, 0, 5, "#chi^2" } }
        , vertex_chi2prob{ owner, "chi2prob", "vertex chi2prob", { 100, 0, 1, "#chi^2 probability" } }
        , vertex_x_pos{ owner, "vtxx", "x position of vertex", { 100, -x_range, x_range, "x [mm]" } }
        , vertex_y_pos{ owner, "vtxy", "y position of vertex", { 100, -y_range, y_range, "y [mm]" } }
        , vertex_z_pos{ owner, "vtxz", "z position of vertex", { 100, -z_range, z_range, "z [mm]" } }
        , vertex_x_error{ owner,
                          "vtxxerr",
                          "error in vertex_x coordinate",
                          { 50, 0, 0.2 * Gaudi::Units::mm, "x_err [mm]" } }
        , vertex_y_error{ owner,
                          "vtxyerr",
                          "error in vertex_y coordinate",
                          { 50, 0, 0.2 * Gaudi::Units::mm, "y_err [mm]" } }
        , vertex_z_error{
              owner, "vtxzerr", "error in vertex_z coordinate", { 50, 0, 2.5 * Gaudi::Units::mm, "z_err [mm]" } } {}
  };

  // 1D histos of bias variables
  struct BiasHistos {
    mutable Gaudi::Accumulators::Histogram<1> masspull;
    mutable Gaudi::Accumulators::Histogram<1> mass;
    mutable Gaudi::Accumulators::Histogram<1> mass_negy;
    mutable Gaudi::Accumulators::Histogram<1> mass_posy;
    mutable Gaudi::Accumulators::Histogram<1> mass_negx;
    mutable Gaudi::Accumulators::Histogram<1> mass_posx;
    mutable Gaudi::Accumulators::Histogram<1> mass_negy_negx;
    mutable Gaudi::Accumulators::Histogram<1> mass_negy_posx;
    mutable Gaudi::Accumulators::Histogram<1> mass_posy_negx;
    mutable Gaudi::Accumulators::Histogram<1> mass_posy_posx;
    mutable Gaudi::Accumulators::Histogram<1> pdiff;
    mutable Gaudi::Accumulators::Histogram<1> pAsym;
    mutable Gaudi::Accumulators::Histogram<1> eta;
    mutable Gaudi::Accumulators::Histogram<1> tx;
    mutable Gaudi::Accumulators::Histogram<1> ty;
    mutable Gaudi::Accumulators::Histogram<1> phi;
    mutable Gaudi::Accumulators::Histogram<1> matt;
    mutable Gaudi::Accumulators::Histogram<1> openingAngle;

    BiasHistos( const TrackParticleMonitor* owner, float const& min_mass, float const& max_mass,
                float const& pdiff_range, unsigned int const& mass_bins, unsigned int const& pdiff_bins,
                unsigned int const& bins_angles )
        : masspull{ owner, "masspull", "mass pull", { mass_bins, -5, 5, "pull" } }
        , mass{ owner, "mass", "mass", { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_negy{ owner, "massNegativeY", "mass for y<0", { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_posy{ owner, "massPositiveY", "mass for y>0", { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_negx{ owner, "massNegativeX", "mass for x<0", { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_posx{ owner, "massPositiveX", "mass for x>0", { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_negy_negx{ owner,
                          "massNegativeY_NegativeX",
                          "mass for y<0 and x<0",
                          { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_negy_posx{ owner,
                          "massNegativeY_PositiveX",
                          "mass for y<0 and x>0",
                          { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_posy_negx{ owner,
                          "massPositiveY_NegativeX",
                          "mass for y>0 and x<0",
                          { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , mass_posy_posx{ owner,
                          "massPositiveY_PositiveX",
                          "mass for y>0 and x>0",
                          { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } }
        , pdiff{ owner,
                 "momdiff",
                 "Momentum difference",
                 { pdiff_bins * 5, -pdiff_range, pdiff_range, "p_{+} - p_{-} [GeV/c]" } }
        , pAsym{ owner, "asym", "Momentum asymmetry", { pdiff_bins * 5, -1, 1, "p asymmetry" } }
        , eta{ owner, "eta", "Pseudorapidity", { 100, 2, 7, "eta" } }
        , tx{ owner, "tx", "tx slope", { 100, -0.2, 0.2, "tx" } }
        , ty{ owner, "ty", "ty slope", { 100, -0.2, 0.2, "ty" } }
        , phi{ owner, "phiangle", "Azimuthal angle of the decay plane", { bins_angles * 5, 0, M_PI, "phi [rad]" } }
        , matt{ owner,
                "phimatt",
                "Angle between the decay plane and the y axis",
                { bins_angles * 5, 0, M_PI, "phiMatt [rad]" } }
        , openingAngle{ owner, "openingangle", "Opening angle", { bins_angles * 5, 0, 0.3, "Opening angle [rad]" } } {}
  };

  // Profile histograms of mass vs bias variables
  struct MassvsBiasHistos {
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_p;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_pt;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_pdiff;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_pAsym;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_eta;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_tx;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_ty;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_phi;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_matt;
    mutable Gaudi::Accumulators::ProfileHistogram<1> mass_vs_openingAngle;

    MassvsBiasHistos( const TrackParticleMonitor* owner, std::string const& particle_name,
                      unsigned int const& pdiff_bins, float const& pdiff_range, unsigned int const& bins_angles,
                      float const& p_min, float const& p_max, float const& pt_min, float const& pt_max )
        : mass_vs_p{ owner,
                     "massVersusMom",
                     fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs p", particle_name ),
                     { 20, p_min, p_max, "p [GeV/c]" } }
        , mass_vs_pt{ owner,
                      "massVersusPt",
                      fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs pt", particle_name ),
                      { 20, pt_min, pt_max, "pt [GeV/c]" } }
        , mass_vs_pdiff{ owner,
                         "massVersusMomDif",
                         fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs daughter p difference", particle_name ),
                         { pdiff_bins, -pdiff_range, pdiff_range, "p_{+} - p_{-} [GeV/c]" } }
        , mass_vs_pAsym{ owner,
                         "massVersusMomAsym",
                         fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs daughter p asymmetry", particle_name ),
                         { pdiff_bins, -1, 1, "p asymmetry" } }
        , mass_vs_eta{ owner,
                       "massVersusEta",
                       fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs eta", particle_name ),
                       { 20, 2, 7, "eta" } }
        , mass_vs_tx{ owner,
                      "massVersusTx",
                      fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs tx", particle_name ),
                      { 40, -0.2, 0.2, "tx" } }
        , mass_vs_ty{ owner,
                      "massVersusTy",
                      fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs ty", particle_name ),
                      { 40, -0.2, 0.2, "ty" } }
        , mass_vs_phi{ owner,
                       "massVersusPhi",
                       fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs decay plane phi", particle_name ),
                       { bins_angles, -M_PI, M_PI, "phi [rad]" } }
        , mass_vs_matt{ owner,
                        "massVersusPhiMatt",
                        fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs decay plane phiMatt", particle_name ),
                        { bins_angles, 0, M_PI, "phiMatt [rad]" } }
        , mass_vs_openingAngle{ owner,
                                "massVersusOpenAngle",
                                fmt::format( "m({0}) - m({0})_{{PDG}} [MeV/c^{{2}}] vs opening angle", particle_name ),
                                { bins_angles, 0, 0.3, "Opening angle [rad]" } } {}
  };

  // 2D histograms of mass vs bias variables
  struct MassvsBias2DHistos {
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_p_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_pt_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_pdiff_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_pAsym_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_eta_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_tx_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_ty_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_phi_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_matt_2D;
    mutable Gaudi::Accumulators::Histogram<2> mass_vs_openingAngle_2D;

    MassvsBias2DHistos( const TrackParticleMonitor* owner, unsigned int const& pdiff_bins, float const& pdiff_range,
                        unsigned int const& bins_angles, float const& p_min, float const& p_max, float const& pt_min,
                        float const& pt_max, float const& min_mass, float const& max_mass,
                        unsigned int const& mass_bins )
        : mass_vs_p_2D{ owner,
                        "massVersusMom2D",
                        "mass vs p",
                        { { 20, p_min, p_max, "p [GeV/c]" }, { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_pt_2D{ owner,
                         "massVersusPt2D",
                         "mass vs pt",
                         { { 20, pt_min, pt_max, "pt [GeV/c]" }, { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_pdiff_2D{ owner,
                            "massVersusMomDif2D",
                            "mass vs momentum difference",
                            { { pdiff_bins, -pdiff_range, pdiff_range, "p_{+} - p_{-} [GeV/c]" },
                              { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_pAsym_2D{ owner,
                            "massVersusMomAsym2D",
                            "mass vs momentum asymetry",
                            { { pdiff_bins, -1, 1, "p asymmetry" },
                              { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_eta_2D{ owner,
                          "massVersusEta2D",
                          "mass vs eta",
                          { { 20, 2, 7, "eta" }, { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_tx_2D{ owner,
                         "massVersusTx2D",
                         "mass vs tx",
                         { { 40, -0.2, 0.2, "tx" }, { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_ty_2D{ owner,
                         "massVersusTy2D",
                         "mass vs ty",
                         { { 40, -0.2, 0.2, "ty" }, { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_phi_2D{ owner,
                          "massVersusPhi2D",
                          "mass vs decay plane phi",
                          { { bins_angles, -M_PI, M_PI, "phi [rad]" },
                            { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_matt_2D{ owner,
                           "massVersusPhiMatt2D",
                           "mass vs decay plane phiMatt",
                           { { bins_angles, 0, M_PI, "phiMatt [rad]" },
                             { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } }
        , mass_vs_openingAngle_2D{ owner,
                                   "massVersusOpenAngle2D",
                                   "mass vs decay plane opening angle",
                                   { { bins_angles, 0, 0.3, "Opening angle [rad]" },
                                     { mass_bins, min_mass, max_mass, "m [GeV/c^{2}]" } } } {}
  };

  // Maps storing histogram objects
  std::map<int, TrackVertexHistos>  m_histograms_track_vertex;
  std::map<int, BiasHistos>         m_histograms_bias;
  std::map<int, MassvsBiasHistos>   m_histograms_mass_vs_bias;
  std::map<int, MassvsBias2DHistos> m_histograms_mass_vs_bias_2D;
};

DECLARE_COMPONENT( TrackParticleMonitor )

TrackParticleMonitor::TrackParticleMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{ name,
                pSvcLocator,
                { KeyValue{ "InputLocation", "" }, KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top },
                  KeyValue{ "Magnet", LHCb::Det::Magnet::det_path } } } {}

// Algorithm
void TrackParticleMonitor::operator()( LHCb::Particle::Range const& particles, DetectorElement const& lhcb,
                                       DeMagnet const& magnet ) const {

  for ( const auto& particle : particles ) {
    std::vector<const LHCb::Track*> tracks;
    std::vector<double>             masshypos;
    addTracks( *particle, tracks, masshypos );

    // Create and fill vector of track states
    std::vector<const LHCb::State*> states;
    double                          z                   = particle->referencePoint().z();
    auto&                           track_vertex_histos = m_histograms_track_vertex.at( 0 );
    for ( const LHCb::Track* track : tracks ) {

      ++track_vertex_histos.track_chi2PerDoF[track->chi2PerDoF()];
      ++track_vertex_histos.track_pt[track->pt() / Gaudi::Units::GeV];
      ++track_vertex_histos.track_p[track->p() / Gaudi::Units::GeV];

      LHCb::State* state = new LHCb::State();
      m_stateprovider->stateFromTrajectory( *state, *track, z, *lhcb.geometry() ).ignore();
      states.push_back( state );
    }

    // Obtain masses from PIDs
    double                        pdgmass( 0 ), pdgwidth( 0 );
    const LHCb::ParticleProperty* prop = m_propertysvc->find( particle->particleID() );
    if ( prop ) {
      pdgmass  = prop->mass() / Gaudi::Units::GeV;
      pdgwidth = prop->width() / Gaudi::Units::GeV;
    }
    // Obtain vertex and perform vertex fit
    LHCb::TrackStateVertex vertex( states );
    vertex.fit();
    double                     chi2     = vertex.chi2();
    double                     chi2prob = LHCb::ChiSquare( chi2, vertex.nDoF() ).prob();
    const Gaudi::SymMatrix3x3& poscov   = vertex.covMatrix();

    // Assume at least two tracks per particle
    LHCb::State stateA = vertex.state( 0 );
    LHCb::State stateB = vertex.state( 1 );
    if ( ( magnet.isDown() && stateA.qOverP() > 0 ) || ( !magnet.isDown() && stateA.qOverP() < 0 ) ) {
      std::swap( stateA, stateB );
    }

    // DAUGHTERS
    Gaudi::XYZVector p3A  = stateA.momentum() / Gaudi::Units::GeV;
    double           pneg = p3A.R();
    Gaudi::XYZVector p3B  = stateB.momentum() / Gaudi::Units::GeV;
    double           ppos = p3B.R();
    double           pdif = ppos - pneg;
    double           asym = pdif / ( ppos + pneg );

    // MOTHER
    Gaudi::LorentzVector p4       = vertex.p4( masshypos ) / Gaudi::Units::GeV;
    double               mass     = p4.M();
    double               merr     = vertex.massErr( masshypos ) / Gaudi::Units::GeV;
    double               masspull = ( mass - pdgmass ) / std::sqrt( merr * merr + pdgwidth * pdgwidth );
    double               mom      = p4.P();
    double               pt       = p4.Pt();
    double               eta      = p4.Eta();
    double               tx       = p4.x() / p4.z();
    double               ty       = p4.y() / p4.z();

    // DECAY PLANE ANGLES

    // Unit vector perp. to the decay plane
    Gaudi::XYZVector norm = p3A.Cross( p3B ).Unit();

    // Azimuthal inclination of the decay plane
    double phiangle = std::atan2( norm.y(), norm.x() );

    // Angle between the normal vector and the y axis (0 or pi means that the decay happens in the xz plane)
    double phimatt = std::acos( norm.y() );

    // Opening angle
    double openangle = std::acos( p3A.Dot( p3B ) / ( ppos * pneg ) );

    // Vertex histograms
    ++track_vertex_histos.vertex_chi2[chi2];
    ++track_vertex_histos.vertex_chi2prob[chi2prob];
    ++track_vertex_histos.vertex_x_pos[vertex.position().x()];
    ++track_vertex_histos.vertex_y_pos[vertex.position().y()];
    ++track_vertex_histos.vertex_z_pos[vertex.position().z()];
    ++track_vertex_histos.vertex_x_error[std::sqrt( poscov( 0, 0 ) )];
    ++track_vertex_histos.vertex_y_error[std::sqrt( poscov( 1, 1 ) )];
    ++track_vertex_histos.vertex_z_error[std::sqrt( poscov( 2, 2 ) )];

    // 1D histograms of bias variables
    auto& bias = m_histograms_bias.at( 0 );

    if ( p3A.y() < 0 && p3B.y() < 0 ) {
      ++bias.mass_negy[mass];
      if ( p3A.x() < 0 && p3B.x() < 0 ) {
        ++bias.mass_negy_negx[mass];
      } else if ( p3A.x() > 0 && p3B.x() > 0 ) {
        ++bias.mass_negy_posx[mass];
      }
    } else if ( p3A.y() > 0 && p3B.y() > 0 ) {
      ++bias.mass_posy[mass];
      if ( p3A.x() < 0 && p3B.x() < 0 ) {
        ++bias.mass_posy_negx[mass];
      } else if ( p3A.x() > 0 && p3B.x() > 0 ) {
        ++bias.mass_posy_posx[mass];
      }
    }
    if ( p3A.x() < 0 && p3B.x() < 0 ) {
      ++bias.mass_negx[mass];
    } else if ( p3A.x() > 0 && p3B.x() > 0 ) {
      ++bias.mass_posx[mass];
    }

    ++bias.mass[mass];
    ++bias.masspull[masspull];
    ++bias.pdiff[pdif];
    ++bias.pAsym[asym];
    ++bias.eta[eta];
    ++bias.tx[tx];
    ++bias.ty[ty];
    ++bias.phi[phiangle];
    ++bias.matt[phimatt];
    ++bias.openingAngle[openangle];

    // Profile histograms of mass vs bias variables
    auto& mass_vs_bias = m_histograms_mass_vs_bias.at( 0 );
    mass_vs_bias.mass_vs_p[mom] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_pt[pt] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_pdiff[pdif] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_pAsym[asym] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_eta[eta] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_tx[tx] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_ty[ty] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_phi[phiangle] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_matt[phimatt] += ( mass - pdgmass ) * Gaudi::Units::GeV;
    mass_vs_bias.mass_vs_openingAngle[openangle] += ( mass - pdgmass ) * Gaudi::Units::GeV;

    // 2D histograms of mass vs bias variables  upon request
    if ( m_2Dhistos.value() ) {
      auto& mass_vs_bias_2D = m_histograms_mass_vs_bias_2D.at( 0 );
      ++mass_vs_bias_2D.mass_vs_p_2D[{ mom, mass }];
      ++mass_vs_bias_2D.mass_vs_pt_2D[{ pt, mass }];
      ++mass_vs_bias_2D.mass_vs_pdiff_2D[{ pdif, mass }];
      ++mass_vs_bias_2D.mass_vs_pAsym_2D[{ asym, mass }];
      ++mass_vs_bias_2D.mass_vs_eta_2D[{ eta, mass }];
      ++mass_vs_bias_2D.mass_vs_tx_2D[{ tx, mass }];
      ++mass_vs_bias_2D.mass_vs_ty_2D[{ ty, mass }];
      ++mass_vs_bias_2D.mass_vs_phi_2D[{ phiangle, mass }];
      ++mass_vs_bias_2D.mass_vs_matt_2D[{ phimatt, mass }];
      ++mass_vs_bias_2D.mass_vs_openingAngle_2D[{ openangle, mass }];
    }
    // Memory management
    for ( const LHCb::State* s : states ) delete s;
  }
}
