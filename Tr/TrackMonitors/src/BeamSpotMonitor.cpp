/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/RecVertex.h"

#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/IPublishSvc.h>
#include <GaudiKernel/SmartIF.h>
#include <Kernel/meta_enum.h>
#include <LHCbAlgs/Consumer.h>

#include <cmath>
#include <filesystem>
#include <fstream>
#include <mutex>
#include <optional>
#include <regex>
#include <sstream>
#include <string>
#include <yaml-cpp/yaml.h>

namespace {
  using PVView = LHCb::RecVertex::Range;

  /// Meta-enum for directing how to handle inconceivably large differences
  meta_enum_class( IncHandType, int, Unknown = 0, WarnUpdate, WarnSkip, ExceptionThrow );

  /// Unbiased sample covariance calculator for off-diagonals
  template <typename X, typename XX>
  auto calculate_spread_offdiag( X const& x, X const& y, XX const& xy ) {
    return ( xy.sum() - xy.nEntries() * x.mean() * y.mean() ) / ( xy.nEntries() - 1 );
  }

  /// Format position condition "<value>*mm"
  template <typename T>
  inline std::string format_position( T x ) {
    return std::to_string( x / Gaudi::Units::mm ) + "*mm";
  }

  /// Format spread condition "<value>*mm2"
  template <typename T>
  inline std::string format_spread( T x ) {
    return std::to_string( x / Gaudi::Units::mm2 ) + "*mm2";
  }

  /// Find largest version number in a directory
  /// Adapted from AlignOnlineYMLCopier::AlignOnlineYMLCopier
  template <typename T>
  int find_max_version_number( T dir ) {
    int        max_version_nr = -1;
    std::regex regex_version_file( "^v([0-9]+)$" );
    for ( const auto& entry : std::filesystem::directory_iterator( dir ) ) {
      auto        version_file_name = entry.path().stem().string();
      std::smatch matches;
      if ( std::regex_search( version_file_name, matches, regex_version_file ) && matches.size() > 1 &&
           matches[1].matched ) {
        int version_nr = std::stoi( matches[1] );
        if ( version_nr > max_version_nr ) max_version_nr = version_nr;
      }
    }
    return max_version_nr;
  }

  namespace IRCondKeys {
    /// Key names for production of YAML maps
    const std::string irkey = "InteractionRegion";
    const std::string pkey  = "position";
    const std::string skey  = "spread";
  }; // namespace IRCondKeys

} // namespace

class BeamSpotMonitor : public LHCb::Algorithm::Consumer<void( LHCb::ODIN const&, PVView const& )> {

public:
  SmartIF<IPublishSvc> m_publishSvc;

  /// Standard constructor
  BeamSpotMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm execute
  void operator()( LHCb::ODIN const&, PVView const& pvcontainer ) const override;

  /// Initialization and finalization
  StatusCode initialize() override;

  virtual StatusCode finalize() override {
    if ( m_onlineMode ) m_publishSvc->undeclarePubAll();
    return Consumer::finalize();
  }

  /// Internal representation of cached conditions
  struct IRConditionsCache {
    /// Cache variables
    unsigned              nPVs{ 0 };
    unsigned              runNumber{ 0 };
    std::array<double, 3> position{ 0.0, 0.0, 0.0 };
    std::array<double, 6> spread{ 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

    YAML::Node asYAML() const;
    void       emitYAML( YAML::Emitter& ) const;
    void       loadYAMLFile( const std::filesystem::path& );
  };

private:
  // Methods
  //---------------------------------------------------------------------------
  /// Initialize histograms that have configuration properties, like axis limts
  void init_configurable_histos() const;

  /// Non-thread-safe part of the event loop --- resetting and publishing
  void check_and_publish_reset( LHCb::ODIN const& ) const;

  /// Conditions for and implementation of copying accumulating ctrs to cache
  bool check_cache_counters( LHCb::ODIN const& ) const;
  void cache_counters() const;
  bool check_conceivable() const;

  /// Conditions for and implementation of publication of cached conditions
  bool check_publish() const;
  bool ymlWriter() const;

  /// Reset the accumulators
  bool check_reset_accumulators( const unsigned ) const;
  void reset_accumulators( const unsigned ) const;

  /// Checkers for conditions value drifts
  bool is_pos_delta_x_over_thresh( double thresh ) const {
    return std::abs( m_pvXPosCtr.mean() - m_cache.position[0] ) > thresh;
  }
  bool is_pos_delta_y_over_thresh( double thresh ) const {
    return std::abs( m_pvYPosCtr.mean() - m_cache.position[1] ) > thresh;
  }
  bool is_pos_delta_z_over_thresh( double thresh ) const {
    return std::abs( m_pvZPosCtr.mean() - m_cache.position[2] ) > thresh;
  }
  bool is_spread_delta_xx_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[0] - m_pvXPosCtr.unbiased_sample_variance() ) > thresh;
  }
  bool is_spread_delta_xy_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[1] - calculate_spread_offdiag( m_pvXPosCtr, m_pvYPosCtr, m_pvXYProdCtr ) ) > thresh;
  }
  bool is_spread_delta_yy_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[2] - m_pvYPosCtr.unbiased_sample_variance() ) > thresh;
  }
  bool is_spread_delta_zx_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[3] - calculate_spread_offdiag( m_pvZPosCtr, m_pvXPosCtr, m_pvZXProdCtr ) ) > thresh;
  }
  bool is_spread_delta_yz_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[4] - calculate_spread_offdiag( m_pvYPosCtr, m_pvZPosCtr, m_pvYZProdCtr ) ) > thresh;
  }
  bool is_spread_delta_zz_over_thresh( double thresh ) const {
    return std::abs( m_cache.spread[5] - m_pvZPosCtr.unbiased_sample_variance() ) > thresh;
  }
  bool is_pos_rho_delta_over_thresh( double thresh ) const {
    return std::pow( m_pvXPosCtr.mean() - m_cache.position[0], 2 ) +
               std::pow( m_pvYPosCtr.mean() - m_cache.position[1], 2 ) >
           std::pow( thresh, 2 );
  }

  /// Lookup map for checkers
  using TPRED = bool ( BeamSpotMonitor::* )( double ) const;
  mutable std::map<std::string, TPRED> m_thresh_func_map{ { "x", &BeamSpotMonitor::is_pos_delta_x_over_thresh },
                                                          { "y", &BeamSpotMonitor::is_pos_delta_y_over_thresh },
                                                          { "z", &BeamSpotMonitor::is_pos_delta_z_over_thresh },
                                                          { "xx", &BeamSpotMonitor::is_spread_delta_xx_over_thresh },
                                                          { "xy", &BeamSpotMonitor::is_spread_delta_xy_over_thresh },
                                                          { "yx", &BeamSpotMonitor::is_spread_delta_xy_over_thresh },
                                                          { "yy", &BeamSpotMonitor::is_spread_delta_yy_over_thresh },
                                                          { "zx", &BeamSpotMonitor::is_spread_delta_zx_over_thresh },
                                                          { "xz", &BeamSpotMonitor::is_spread_delta_zx_over_thresh },
                                                          { "yz", &BeamSpotMonitor::is_spread_delta_yz_over_thresh },
                                                          { "zy", &BeamSpotMonitor::is_spread_delta_yz_over_thresh },
                                                          { "zz", &BeamSpotMonitor::is_spread_delta_zz_over_thresh },
                                                          { "rho", &BeamSpotMonitor::is_pos_rho_delta_over_thresh } };

  /// Check all configured thresholds
  using ThreshMap = std::map<std::string, double>;
  bool are_any_deltas_over_thresh( const ThreshMap& thresh_map ) const {
    bool t = false;
    for ( const auto& [key, value] : thresh_map ) { t = t || ( this->*( m_thresh_func_map[key] ) )( value ); }
    return t;
  }

  // Properties
  //---------------------------------------------------------------------------

  // Output configuration properties
  //...........................................................................
  /// Report conditions to log (INFO)
  Gaudi::Property<bool> m_condToLog{ this, "LogConditions", false,
                                     "Write conditions to logfile with level INFO when updating" };

  /// Create direct run conditions
  Gaudi::Property<bool> m_writeDBFiles{ this, "MakeDBRunFile", false };

  /// LHCB DB to write to path
  Gaudi::Property<std::string> m_conditionsDbPath{ this, "conditionsDbPath",
                                                   "/group/online/hlt/conditions.run3/lhcb-conditions-database" };
  Gaudi::Property<std::string> m_conditionsPathInDb{ this, "conditionsPathInDb",
                                                     "Conditions/LHCb/Online/InteractionRegion.yml/.pool" };

  /// Use IPublishSvc Online
  Gaudi::Property<bool>        m_onlineMode{ this, "OnlineMode", false, "Running in Online" };
  Gaudi::Property<std::string> m_knownAsName{ this, "KnownAsToPublishSvc", "LHCb/Online/InteractionRegion",
                                              "Name used to publish online conditions" };
  Gaudi::Property<std::string> m_pubSvcName{ this, "PublishSvc", "LHCb::PublishSvc",
                                             "Publishing Svc implementing IPublishSvc" };

  // Accumulation configuration properties
  //...........................................................................
  Gaudi::Property<bool> m_allowVeloOpen{ this, "VeloOpenUpdates", false, "Allow updates if VELO is not closed" };
  Gaudi::Property<long unsigned> m_minPVsForCalib{ this, "MinPVsForCalib", 100ul,
                                                   "Minumum number of accumulated PVs for a calibration" };

  /// Thresholds triggering updated publication
  Gaudi::Property<ThreshMap> m_maxAbsDeltaMap{
      this,
      "MaxAbsDeltaMap",
      { { "x", 0.02 * Gaudi::Units::mm }, { "y", 0.02 * Gaudi::Units::mm } },
      "Map of {string: double} maximum allowed absolute differences from previous to produce new conditions.\nAllowed "
      "keys:  position components:  'x', 'y', and 'z'.\nspread matrix components:  'xx', 'xy', 'yy', 'xz', 'yz', and "
      "'zz'.\nAdditional thresholds: 'rho': the transverse difference in the position" };
  Gaudi::Property<ThreshMap> m_inconceivableAbsDeltaMap{
      this,
      "InconceivableAbsDeltaMap",
      {},
      "Map of {string: double} thresholds defining IR conditions deviations that are 'too large'.\nThe format and "
      "allowed keys are the same as those for MaxAbsDeltaMap.\nThe handling of thresholds is controlled by "
      "InconceivableHandling." };
  Gaudi::Property<IncHandType> m_inconceivableHandling{
      this, "InconceivableHandling", IncHandType::WarnUpdate,
      "How to handle updates that exceed any of the configured InconceivableAbsDeltaMap thresholds.  Allowed values "
      "are 'WarnUpdate', 'WarnSkip', and 'ExceptionThrow'" };

  /// Histogram limits
  Gaudi::Property<double> m_histPVXMin{ this, "HistPVXMin", -2.5 * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVXMax{ this, "HistPVXMax", 2.5 * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVYMin{ this, "HistPVYMin", -2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVYMax{ this, "HistPVYMax", 2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVZMin{ this, "HistPVZMin", -200. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVZMax{ this, "HistPVZMax", 200. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histPVZMin_wide{ this, "HistPVZMin_Wide", -1.5e3 * Gaudi::Units::mm,
                                             "Wide z window for PV plot" };
  Gaudi::Property<double> m_histPVZMax_wide{ this, "HistPVZMax_Wide", 1.5e3 * Gaudi::Units::mm,
                                             "Wide z window for PV plot" };

  Gaudi::Property<double> m_hist2DPVXMin{ this, "Hist2DPVXMin", 0.8 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DPVXMax{ this, "Hist2DPVXMax", 1.4 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DPVYMin{ this, "Hist2DPVYMin", -0.1 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DPVYMax{ this, "Hist2DPVYMax", 0.5 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DPVZMin{ this, "Hist2DPVZMin", -200. * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DPVZMax{ this, "Hist2DPVZMax", 200. * Gaudi::Units::mm };

  Gaudi::Property<double> m_histPVXYMin{ this, "HistPVXYMin", -0.1 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histPVXYMax{ this, "HistPVXYMax", 0.1 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histPVYZMin{ this, "HistPVYZMin", -20. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histPVYZMax{ this, "HistPVYZMax", 20. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histPVZXMin{ this, "HistPVZXMin", -10.0 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histPVZXMax{ this, "HistPVZXMax", 10.0 * Gaudi::Units::mm2 };

  // Limits for conditions histos
  Gaudi::Property<double> m_histIRCondPosXMin{ this, "HistIRCondPosXMin", -2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histIRCondPosXMax{ this, "HistIRCondPosXMax", 2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histIRCondPosYMin{ this, "HistIRCondPosYMin", -2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histIRCondPosYMax{ this, "HistIRCondPosYMax", 2. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histIRCondPosZMin{ this, "HistIRCondPosZMin", -200. * Gaudi::Units::mm };
  Gaudi::Property<double> m_histIRCondPosZMax{ this, "HistIRCondPosZMax", 200. * Gaudi::Units::mm };

  Gaudi::Property<double> m_hist2DCondDelPosXHW{ this, "Hist2DCondDelPosXHalfWidth", 0.1 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DCondDelPosYHW{ this, "Hist2DCondDelPosYHalfWidth", 0.1 * Gaudi::Units::mm };
  Gaudi::Property<double> m_hist2DCondDelPosZHW{ this, "Hist2DCondDelPosZHalfWidth", 100. * Gaudi::Units::mm };

  Gaudi::Property<double> m_histIRCondSpreadXXMin{ this, "HistIRCondSpreadXXMin", 0. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadXXMax{ this, "HistIRCondSpreadXXMax", 0.5 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadXYMin{ this, "HistIRCondSpreadXYMin", -0.2 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadXYMax{ this, "HistIRCondSpreadXYMax", 0.2 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadYYMin{ this, "HistIRCondSpreadYYMin", 0. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadYYMax{ this, "HistIRCondSpreadYYMax", 0.5 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadZXMin{ this, "HistIRCondSpreadZXMin", -5. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadZXMax{ this, "HistIRCondSpreadZXMax", 5. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadYZMin{ this, "HistIRCondSpreadYZMin", -5. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadYZMax{ this, "HistIRCondSpreadYZMax", 5. * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadZZMin{ this, "HistIRCondSpreadZZMin", 1.e3 * Gaudi::Units::mm2 };
  Gaudi::Property<double> m_histIRCondSpreadZZMax{ this, "HistIRCondSpreadZZMax", 3.e3 * Gaudi::Units::mm2 };

  // Accumulating statistics counters
  //---------------------------------------------------------------------------
  mutable unsigned                                m_accRunNumber{ 0 }; // Run number of last processed event
  mutable Gaudi::Accumulators::SigmaCounter<>     m_pvXPosCtr{ this, "PV x position" };
  mutable Gaudi::Accumulators::SigmaCounter<>     m_pvYPosCtr{ this, "PV y position" };
  mutable Gaudi::Accumulators::SigmaCounter<>     m_pvZPosCtr{ this, "PV z position" };
  mutable Gaudi::Accumulators::AveragingCounter<> m_pvXYProdCtr{ this, "PV x * y for covariance" };
  mutable Gaudi::Accumulators::AveragingCounter<> m_pvYZProdCtr{ this, "PV y * z for covariance" };
  mutable Gaudi::Accumulators::AveragingCounter<> m_pvZXProdCtr{ this, "PV z * x for covariance" };

  // Cached statistics counters and variables
  //---------------------------------------------------------------------------
  mutable IRConditionsCache m_cache;

  // PublishSvc message buffer
  mutable std::string m_pub_msg;

  // Histograms
  //---------------------------------------------------------------------------
  mutable Gaudi::Accumulators::Histogram<1> m_numPrimaryVertices{
      this, "NumPrimaryVertices", "Primary vertices per event;Number of PVs;Entries", { 11, -0.5, 10.5 } };
  mutable Gaudi::Accumulators::Histogram<1> m_pvNTracks{
      this, "NumTracks", "Number of tracks per primary vertex;Number of tracks;Entries", { 101, -0.5, 100.5 } };

  // Histograms for individual PVs
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosZWide;

  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVPosYvX;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVPosXvZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVPosYvZ;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosCovXY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosCovYZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histPVPosCovZX;

  // Histograms for individual PVs relative to conditions
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVDelPosYvX;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVDelPosXvZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histPVDelPosYvZ;

  // Histograms for conditions (published and unpublished)
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondPosX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondPosY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondPosZ;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadXX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadXY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadYY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadZX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadYZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondSpreadZZ;

  // Histograms for conditions differences relative to cached conditions
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histIRCondDelPosYvX;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histIRCondDelPosXvZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_histIRCondDelPosYvZ;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadXX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadXY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadYY;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadZX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadYZ;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_histIRCondDelSpreadZZ;

  // Warning counters
  //---------------------------------------------------------------------------
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_badPVCount{ this, "Too few PVs to produce IR conditions" };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_extremeCondCount{
      this, "Provisional IR Conditions update beyond conceivable thresholds" };

  // Other data members
  //---------------------------------------------------------------------------
  /// Lock for main event processing
  mutable std::mutex m_mutex;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( BeamSpotMonitor )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
BeamSpotMonitor::BeamSpotMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{ name,
                pSvcLocator,
                { KeyValue{ "ODINLocation", LHCb::ODINLocation::Default },
                  KeyValue{ "PVContainer", LHCb::RecVertexLocation::Primary } } } {}

//=============================================================================
/// Initialization
//=============================================================================
StatusCode BeamSpotMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    init_configurable_histos();

    // Check configured threshold
    for ( auto pthresh : m_maxAbsDeltaMap ) {
      if ( m_thresh_func_map.find( pthresh.first ) == m_thresh_func_map.end() )
        throw GaudiException( "Bad configruation:  invalid threshold key " + pthresh.first, name(),
                              StatusCode::FAILURE );
    }
    info() << "Configured thresholds: " << m_maxAbsDeltaMap << endmsg;

    for ( auto pthresh : m_inconceivableAbsDeltaMap ) {
      if ( m_thresh_func_map.find( pthresh.first ) == m_thresh_func_map.end() )
        throw GaudiException( "Bad configruation:  invalid threshold key " + pthresh.first, name(),
                              StatusCode::FAILURE );
    }
    // This block uses members_of<IncHandType>().  Failure to use
    //   members_of<IncHandType>(), which is defined by a macro expansion from
    //   meta_enum.h, _somewhere_ causes a clang compiler warning.
    if ( m_inconceivableHandling.value() == IncHandType::Unknown ) {
      info() << "Property " << m_inconceivableHandling.name() << " is set to unknown value "
             << m_inconceivableHandling.value() << ", will use default behavior." << endmsg;
      info() << "Defined options for " << m_inconceivableHandling.name() << " are ";
      for ( auto opt : members_of<IncHandType>() ) {
        if ( opt != IncHandType::Unknown ) info() << opt << ", ";
      }
      info() << "." << endmsg;
    }
    info() << "Inconceivable handling: " << m_inconceivableHandling.value()
           << ", thresholds: " << m_inconceivableAbsDeltaMap << endmsg;

    // Conditions writing path safety imitating RICH RefIndexCalib
    const auto curPath = std::filesystem::current_path();
    if ( m_writeDBFiles ) {
      try {
        if ( !std::filesystem::exists( m_conditionsDbPath.value() ) ) {
          std::filesystem::create_directories( m_conditionsDbPath.value() );
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Created " << m_conditionsDbPath.value() << endmsg;
        }
      } catch ( const std::filesystem::filesystem_error& expt ) {
        warning() << "Cannot write to '" << m_conditionsDbPath.value() << "' -> Resetting Db path to '"
                  << curPath.string() << "/conditions'" << endmsg;
        m_conditionsDbPath = curPath.string() + "/conditions";
      }

      const std::filesystem::path conditions_dir =
          std::filesystem::path( m_conditionsDbPath.value() ) / std::filesystem::path( m_conditionsPathInDb.value() );
      try {
        if ( !std::filesystem::exists( conditions_dir ) ) {
          std::filesystem::create_directories( conditions_dir );
          if ( msgLevel( MSG::DEBUG ) ) debug() << "Created " << conditions_dir.string() << endmsg;
        } else { // Try to initialize cache from most recent conditions
          const auto max_version_nr = find_max_version_number( conditions_dir );
          if ( max_version_nr >= 0 ) {
            const std::filesystem::path conditions_file = conditions_dir / ( "v" + std::to_string( max_version_nr ) );

            info() << "Initializing cache from conditions file " << conditions_file.string() << endmsg;
            m_cache.loadYAMLFile( conditions_file );

            YAML::Emitter condEmi;
            m_cache.emitYAML( condEmi );
            info() << "Cache initialized from file with contents:\n" << condEmi.c_str() << endmsg;
          }
        }
      } catch ( const std::filesystem::filesystem_error& expt ) {
        // TODO:  consider whether this is a recoverable condition.
        throw GaudiException( "Conditions directory does not exist and cannot be created", name(),
                              StatusCode::FAILURE );
      }

      // Set up LHCb::PublishSvc
      if ( m_onlineMode ) {
        m_publishSvc = this->service( m_pubSvcName );
        if ( !this->m_publishSvc.get() ) {
          throw GaudiException( "Cannot access incident service of type PublishSvc.", name(), StatusCode::FAILURE );
        }
        m_publishSvc->declarePubItem( m_knownAsName, m_pub_msg );
      }
    }
  } );
}

/// Initialize histograms that depende on properties
void BeamSpotMonitor::init_configurable_histos() const {
  using axis1D = Gaudi::Accumulators::Axis<decltype( m_histPVPosX )::value_type::AxisArithmeticType>;

  // Histograms for individual PVs
  m_histPVPosX.emplace( this, "PVPosX", "Primary vertex X position;PV X [mm];Entries",
                        axis1D{ 200, m_histPVXMin, m_histPVXMax } );

  m_histPVPosY.emplace( this, "PVPosY", "Primary vertex Y position;PV Y [mm];Entries",
                        axis1D{ 200, m_histPVYMin, m_histPVYMax } );
  m_histPVPosZ.emplace( this, "PVPosZ", "Primary vertex Z position;PV Z [mm];Entries",
                        axis1D{ 200, m_histPVZMin, m_histPVZMax } );
  m_histPVPosZWide.emplace( this, "PVPosZWide", "Primary vertex Z position (wide);PV Z [mm];Entries",
                            axis1D{ 200, m_histPVZMin_wide, m_histPVZMax_wide } );

  m_histPVPosYvX.emplace( this, "PVPosYvX", "Primary vertex position Y vs. X;PV X [mm];PV Y [mm]",
                          axis1D{ 100, m_hist2DPVXMin, m_hist2DPVXMax },
                          axis1D{ 100, m_hist2DPVYMin, m_hist2DPVYMax } );
  m_histPVPosXvZ.emplace( this, "PVPosXvZ", "Primary vertex position X vs. Z;PV Z [mm];PV X [mm]",
                          axis1D{ 100, m_hist2DPVZMin, m_hist2DPVZMax },
                          axis1D{ 100, m_hist2DPVXMin, m_hist2DPVXMax } );
  m_histPVPosYvZ.emplace( this, "PVPosYvZ", "Primary vertex position Y vs. Z;PV Z [mm];PV Y [mm]",
                          axis1D{ 100, m_hist2DPVZMin, m_hist2DPVZMax },
                          axis1D{ 100, m_hist2DPVYMin, m_hist2DPVYMax } );

  m_histPVPosCovXY.emplace( this, "PVCovXY",
                            "Interaction region covariance X-Y;PV (X - X_mean)*(Y - Y_mean) [mm2];Entries",
                            axis1D{ 200, m_histPVXYMin, m_histPVXYMax } );
  m_histPVPosCovYZ.emplace( this, "PVCovYZ",
                            "Interaction region covariance Y-Z;PV (Y - Y_mean)*(Z - Z_mean) [mm2];Entries",
                            axis1D{ 200, m_histPVYZMin, m_histPVYZMax } );
  m_histPVPosCovZX.emplace( this, "PVCovZX",
                            "Interaction region covariance Z-X;PV (Z - Z_mean)*(X - X_mean) [mm2];Entries",
                            axis1D{ 200, m_histPVZXMin, m_histPVZXMax } );

  // Histograms for individual PVs relative to conditions
  // Axis limits are not independent properties---based on limits for raw 2D PVs
  m_histPVDelPosYvX.emplace(
      this, "PVDelPosYvX", "Primary vertex position relative to conditions Y vs. X;PV X [mm];PV Y [mm]",
      axis1D{ 100, -( m_hist2DPVXMax - m_hist2DPVXMin ) / 2, ( m_hist2DPVXMax - m_hist2DPVXMin ) / 2 },
      axis1D{ 100, -( m_hist2DPVYMax - m_hist2DPVYMin ) / 2, ( m_hist2DPVYMax - m_hist2DPVYMin ) / 2 } );
  m_histPVDelPosXvZ.emplace(
      this, "PVDelPosXvZ", "Primary vertex position relative to conditions X vs. Z;PV Z [mm];PV X [mm]",
      axis1D{ 100, -( m_hist2DPVZMax - m_hist2DPVZMin ) / 2, ( m_hist2DPVZMax - m_hist2DPVZMin ) / 2 },
      axis1D{ 100, -( m_hist2DPVXMax - m_hist2DPVXMin ) / 2, ( m_hist2DPVXMax - m_hist2DPVXMin ) / 2 } );
  m_histPVDelPosYvZ.emplace(
      this, "PVDelPosYvZ", "Primary vertex position relative to conditions Y vs. Z;PV Z [mm];PV Y [mm]",
      axis1D{ 100, -( m_hist2DPVZMax - m_hist2DPVZMin ) / 2, ( m_hist2DPVZMax - m_hist2DPVZMin ) / 2 },
      axis1D{ 100, -( m_hist2DPVYMax - m_hist2DPVYMin ) / 2, ( m_hist2DPVYMax - m_hist2DPVYMin ) / 2 } );

  // Histograms for conditions (published and unpublished)
  m_histIRCondPosX.emplace( this, "CondPosX",
                            "Conditions IR Position X (published and unpublished);IR position[0] (x) [mm];Entries",
                            axis1D{ 200, m_histIRCondPosXMin, m_histIRCondPosXMax } );
  m_histIRCondPosY.emplace( this, "CondPosY",
                            "Conditions IR Position Y (published and unpublished);IR position[1] (y) [mm];Entries",
                            axis1D{ 200, m_histIRCondPosYMin, m_histIRCondPosYMax } );
  m_histIRCondPosZ.emplace( this, "CondPosZ",
                            "Conditions IR Position Z (published and unpublished);IR position[2] (z) [mm];Entries",
                            axis1D{ 200, m_histIRCondPosZMin, m_histIRCondPosZMax } );

  m_histIRCondSpreadXX.emplace( this, "CondSpreadXX",
                                "Conditions IR Spread XX (published and unpublished);IR spread[0] (xx) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadXXMin, m_histIRCondSpreadXXMax } );
  m_histIRCondSpreadXY.emplace( this, "CondSpreadXY",
                                "Conditions IR Spread XY (published and unpublished);IR spread[1] (xy) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadXYMin, m_histIRCondSpreadXYMax } );
  m_histIRCondSpreadYY.emplace( this, "CondSpreadYY",
                                "Conditions IR Spread YY (published and unpublished);IR spread[2] (yy) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadYYMin, m_histIRCondSpreadYYMax } );
  m_histIRCondSpreadZX.emplace( this, "CondSpreadZX",
                                "Conditions IR Spread ZX (published and unpublished);IR spread[3] (xz) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadZXMin, m_histIRCondSpreadZXMax } );
  m_histIRCondSpreadYZ.emplace( this, "CondSpreadYZ",
                                "Conditions IR Spread YZ (published and unpublished);IR spread[4] (yz) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadYZMin, m_histIRCondSpreadYZMax } );
  m_histIRCondSpreadZZ.emplace( this, "CondSpreadZZ",
                                "Conditions IR Spread ZZ (published and unpublished);IR spread[5] (zz) [mm2];Entries",
                                axis1D{ 200, m_histIRCondSpreadZZMin, m_histIRCondSpreadZZMax } );

  // Histograms for conditions differences relative to cached conditions
  m_histIRCondDelPosYvX.emplace( this, "CondDelPosYvX",
                                 "Conditions IR Position relative to published Y vs. X;PV X [mm];PV Y [mm]",
                                 axis1D{ 100, -m_hist2DCondDelPosXHW, m_hist2DCondDelPosXHW },
                                 axis1D{ 100, -m_hist2DCondDelPosYHW, m_hist2DCondDelPosYHW } );
  m_histIRCondDelPosXvZ.emplace( this, "CondDelPosXvZ",
                                 "Conditions IR Position relative to published X vs. Z;PV Z [mm];PV X [mm]",
                                 axis1D{ 100, -m_hist2DCondDelPosZHW, m_hist2DCondDelPosZHW },
                                 axis1D{ 100, -m_hist2DCondDelPosXHW, m_hist2DCondDelPosXHW } );
  m_histIRCondDelPosYvZ.emplace( this, "CondDelPosYvZ",
                                 "Conditions IR Position relative to published Y vs. Z;PV Z [mm];PV Y [mm]",
                                 axis1D{ 100, -m_hist2DCondDelPosZHW, m_hist2DCondDelPosZHW },
                                 axis1D{ 100, -m_hist2DCondDelPosYHW, m_hist2DCondDelPosYHW } );

  // Spread Delta histo axis limits are not independent properties atm
  // They are based on the limits for absolute conditions histos
  m_histIRCondDelSpreadXX.emplace(
      this, "CondDelSpreadXX",
      "Conditions IR Spread relative to published XX (published and unpublished);IR spread[0] (xx) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadXXMax - m_histIRCondSpreadXXMin ) / 2,
              ( m_histIRCondSpreadXXMax - m_histIRCondSpreadXXMin ) / 2 } );
  m_histIRCondDelSpreadXY.emplace(
      this, "CondDelSpreadXY",
      "Conditions IR Spread relative to published XY (published and unpublished);IR spread[1] (xy) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadXYMax - m_histIRCondSpreadXYMin ) / 2,
              ( m_histIRCondSpreadXYMax - m_histIRCondSpreadXYMin ) / 2 } );
  m_histIRCondDelSpreadYY.emplace(
      this, "CondDelSpreadYY",
      "Conditions IR Spread relative to published YY (published and unpublished);IR spread[2] (yy) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadYYMax - m_histIRCondSpreadYYMin ) / 2,
              ( m_histIRCondSpreadYYMax - m_histIRCondSpreadYYMin ) / 2 } );
  m_histIRCondDelSpreadZX.emplace(
      this, "CondDelSpreadZX",
      "Conditions IR Spread relative to published ZX (published and unpublished);IR spread[3] (xz) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadZXMax - m_histIRCondSpreadZXMin ) / 2,
              ( m_histIRCondSpreadZXMax - m_histIRCondSpreadZXMin ) / 2 } );
  m_histIRCondDelSpreadYZ.emplace(
      this, "CondDelSpreadYZ",
      "Conditions IR Spread relative to published YZ (published and unpublished);IR spread[4] (yz) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadYZMax - m_histIRCondSpreadYZMin ) / 2,
              ( m_histIRCondSpreadYZMax - m_histIRCondSpreadYZMin ) / 2 } );
  m_histIRCondDelSpreadZZ.emplace(
      this, "CondDelSpreadZZ",
      "Conditions IR Spread relative to published ZZ (published and unpublished);IR spread[5] (zz) [mm2];Entries",
      axis1D{ 100, -( m_histIRCondSpreadZZMax - m_histIRCondSpreadZZMin ) / 2,
              ( m_histIRCondSpreadZZMax - m_histIRCondSpreadZZMin ) / 2 } );
}

//=============================================================================
// Algorithm execution
//=============================================================================
void BeamSpotMonitor::operator()( LHCb::ODIN const& odin, PVView const& pvcontainer ) const {
  check_and_publish_reset( odin );

  // Skip this event if it is from a run prior to the one we are accumulating.
  if ( odin.runNumber() < m_accRunNumber ) return;

  // number of primary vertices
  ++m_numPrimaryVertices[pvcontainer.size()];

  for ( const auto& pv : pvcontainer ) {
    m_pvXPosCtr += pv->position().x();
    m_pvYPosCtr += pv->position().y();
    m_pvZPosCtr += pv->position().z();
    m_pvXYProdCtr += pv->position().x() * pv->position().y();
    m_pvYZProdCtr += pv->position().y() * pv->position().z();
    m_pvZXProdCtr += pv->position().z() * pv->position().x();

    ++m_pvNTracks[pv->tracks().size()];
    ++m_histPVPosX.value()[pv->position().x()];
    ++m_histPVPosY.value()[pv->position().y()];
    ++m_histPVPosZ.value()[pv->position().z()];
    ++m_histPVPosZWide.value()[pv->position().z()];

    ++m_histPVPosYvX.value()[{ pv->position().x(), pv->position().y() }];
    ++m_histPVPosXvZ.value()[{ pv->position().z(), pv->position().x() }];
    ++m_histPVPosYvZ.value()[{ pv->position().z(), pv->position().y() }];

    if ( m_cache.nPVs > 0 ) {
      ++m_histPVDelPosYvX
            .value()[{ pv->position().x() - m_cache.position[0], pv->position().y() - m_cache.position[1] }];
      ++m_histPVDelPosXvZ
            .value()[{ pv->position().z() - m_cache.position[2], pv->position().x() - m_cache.position[0] }];
      ++m_histPVDelPosYvZ
            .value()[{ pv->position().z() - m_cache.position[2], pv->position().y() - m_cache.position[1] }];
    }

    // Must be incremented after accumulators to avoid excursion on first PV.
    ++m_histPVPosCovXY
          .value()[( pv->position().x() - m_pvXPosCtr.mean() ) * ( pv->position().y() - m_pvYPosCtr.mean() )];
    ++m_histPVPosCovYZ
          .value()[( pv->position().y() - m_pvYPosCtr.mean() ) * ( pv->position().z() - m_pvZPosCtr.mean() )];
    ++m_histPVPosCovZX
          .value()[( pv->position().z() - m_pvZPosCtr.mean() ) * ( pv->position().x() - m_pvXPosCtr.mean() )];
  }
}

//=============================================================================
// Non-thread-safe resetting and publishing
//=============================================================================
void BeamSpotMonitor::check_and_publish_reset( LHCb::ODIN const& odin ) const {
  std::scoped_lock lock{ m_mutex };

  // Handle cacheing and publication before processing the event.
  const auto curRunNumber = odin.runNumber();
  if ( check_reset_accumulators( curRunNumber ) ) {
    if ( check_cache_counters( odin ) ) {
      // Copy values of accumulating counters to cache
      cache_counters();

      if ( check_publish() ) {
        if ( !ymlWriter() ) {
          throw GaudiException( "YAML writing of conditions failed - unable to write new conditions", name(),
                                StatusCode::FAILURE );
        }
      }
    }
    // Reset accumulators
    reset_accumulators( curRunNumber );
  }
}

//=============================================================================
// Check for cacheing the counters
// Update cache
// - If have accumulated enough PVs
// - AND if either
//    - The VELO is closed
//    - OR VELO Open updates are allowed
// - AND one or more of the following:
//   - The cached conditions were uninitialized
//   - OR There are no configured thresholds (update cache every m_minPVsForCalib), or
//   - OR Any of the configured thresholds are exceeded AND the update passes the 'inconceivable' check
//=============================================================================
bool BeamSpotMonitor::check_cache_counters( LHCb::ODIN const& odin ) const {
  return ( m_pvXPosCtr.nEntries() >= m_minPVsForCalib ) &&
         ( m_allowVeloOpen || !odin.eventTypeBit( LHCb::ODIN::EventTypes::VeloOpen ) ) &&
         ( m_cache.nPVs == 0 || m_maxAbsDeltaMap.empty() ||
           ( are_any_deltas_over_thresh( m_maxAbsDeltaMap ) &&
             ( m_inconceivableAbsDeltaMap.empty() || check_conceivable() ) ) );
}

//=============================================================================
// Check whether to publish the cached counters
//=============================================================================
bool BeamSpotMonitor::check_publish() const { return m_cache.nPVs >= m_minPVsForCalib; }

//=============================================================================
// Check whether to reset the accumulators
// Reset when
// - Accumulated at least the target number of PVs
// - Processing an event from a new, subsequent run.
//=============================================================================
bool BeamSpotMonitor::check_reset_accumulators( const unsigned curRunNumber ) const {
  return ( m_pvXPosCtr.nEntries() >= m_minPVsForCalib ) || ( m_accRunNumber < curRunNumber );
}

//=============================================================================
// Cache the counters
//=============================================================================
void BeamSpotMonitor::cache_counters() const {
  m_cache.nPVs      = m_pvXPosCtr.nEntries();
  m_cache.runNumber = m_accRunNumber;

  m_cache.position[0] = m_pvXPosCtr.mean();
  m_cache.position[1] = m_pvYPosCtr.mean();
  m_cache.position[2] = m_pvZPosCtr.mean();

  m_cache.spread[0] = m_pvXPosCtr.unbiased_sample_variance();
  m_cache.spread[1] = calculate_spread_offdiag( m_pvXPosCtr, m_pvYPosCtr, m_pvXYProdCtr );
  m_cache.spread[2] = m_pvYPosCtr.unbiased_sample_variance();
  m_cache.spread[3] = calculate_spread_offdiag( m_pvZPosCtr, m_pvXPosCtr, m_pvZXProdCtr );
  m_cache.spread[4] = calculate_spread_offdiag( m_pvYPosCtr, m_pvZPosCtr, m_pvYZProdCtr );
  m_cache.spread[5] = m_pvZPosCtr.unbiased_sample_variance();
}

//=============================================================================
// Reset the accumulators
//=============================================================================
void BeamSpotMonitor::reset_accumulators( const unsigned curRunNumber ) const {
  // Book histograms before resetting
  if ( m_pvXPosCtr.nEntries() >= m_minPVsForCalib ) {
    ++m_histIRCondPosX.value()[m_pvXPosCtr.mean()];
    ++m_histIRCondPosY.value()[m_pvYPosCtr.mean()];
    ++m_histIRCondPosZ.value()[m_pvZPosCtr.mean()];

    ++m_histIRCondSpreadXX.value()[m_pvXPosCtr.unbiased_sample_variance()];
    ++m_histIRCondSpreadXY.value()[calculate_spread_offdiag( m_pvXPosCtr, m_pvYPosCtr, m_pvXYProdCtr )];
    ++m_histIRCondSpreadYY.value()[m_pvYPosCtr.unbiased_sample_variance()];
    ++m_histIRCondSpreadZX.value()[calculate_spread_offdiag( m_pvZPosCtr, m_pvXPosCtr, m_pvZXProdCtr )];
    ++m_histIRCondSpreadYZ.value()[calculate_spread_offdiag( m_pvYPosCtr, m_pvZPosCtr, m_pvYZProdCtr )];
    ++m_histIRCondSpreadZZ.value()[m_pvZPosCtr.unbiased_sample_variance()];

    if ( m_cache.nPVs > 0 ) {
      ++m_histIRCondDelPosYvX
            .value()[{ m_pvXPosCtr.mean() - m_cache.position[0], m_pvYPosCtr.mean() - m_cache.position[1] }];
      ++m_histIRCondDelPosXvZ
            .value()[{ m_pvZPosCtr.mean() - m_cache.position[2], m_pvXPosCtr.mean() - m_cache.position[0] }];
      ++m_histIRCondDelPosYvZ
            .value()[{ m_pvZPosCtr.mean() - m_cache.position[2], m_pvYPosCtr.mean() - m_cache.position[1] }];

      ++m_histIRCondDelSpreadXX.value()[m_pvXPosCtr.unbiased_sample_variance() - m_cache.spread[0]];
      ++m_histIRCondDelSpreadXY
            .value()[calculate_spread_offdiag( m_pvXPosCtr, m_pvYPosCtr, m_pvXYProdCtr ) - m_cache.spread[1]];
      ++m_histIRCondDelSpreadYY.value()[m_pvYPosCtr.unbiased_sample_variance() - m_cache.spread[2]];
      ++m_histIRCondDelSpreadZX
            .value()[calculate_spread_offdiag( m_pvZPosCtr, m_pvXPosCtr, m_pvZXProdCtr ) - m_cache.spread[3]];
      ++m_histIRCondDelSpreadYZ
            .value()[calculate_spread_offdiag( m_pvYPosCtr, m_pvZPosCtr, m_pvYZProdCtr ) - m_cache.spread[4]];
      ++m_histIRCondDelSpreadZZ.value()[m_pvZPosCtr.unbiased_sample_variance() - m_cache.spread[5]];
    }
  }

  // Reset
  m_pvXPosCtr.reset();
  m_pvYPosCtr.reset();
  m_pvZPosCtr.reset();
  m_pvXYProdCtr.reset();
  m_pvYZProdCtr.reset();
  m_pvZXProdCtr.reset();
  m_accRunNumber = curRunNumber;
}

//=============================================================================
// Check inconceivable thresholds and handle results.
// Returns true of configuration indicates an update is ok.
//=============================================================================
bool BeamSpotMonitor::check_conceivable() const {
  if ( are_any_deltas_over_thresh( m_inconceivableAbsDeltaMap ) ) {
    ++m_extremeCondCount;
    switch ( m_inconceivableHandling ) {
    case IncHandType::ExceptionThrow:
      throw GaudiException( "Provisional IR Conditions update beyond conceivable thresholds", name(),
                            StatusCode::FAILURE );
      return false;
    case IncHandType::WarnSkip:
      return false;
    case IncHandType::WarnUpdate:
      return true;
    case IncHandType::Unknown:
      return true;
    default:
      return true;
    }
  } else {
    return true;
  }
}

//=============================================================================
// YML formatter
//
// Format of example condition
// lhcb-conditions-database/Conditions/LHCb/Online/InteractionRegion.yml/0/200
//
// From its comments, the ordering of elements should be
// InteractionRegion:
//     position: <avg_position_x_y_z: [float x3]>
//     spread: <spread_matrix_xx_xy_yy_xz_yz_zz: [float x6]>
//
// The example conditions are
// InteractionRegion:
//     position: [ "0.0*mm", "0.0*mm", "0.0*mm" ]
//     spread: [ "0.0064*mm2", "0.0*mm2", "0.0064*mm2", "0.0*mm2", "0.0*mm2", "2809.0*mm2" ]
//
// Note that the numeric values and their units are enclosed in double quotes.
//=============================================================================
void BeamSpotMonitor::IRConditionsCache::emitYAML( YAML::Emitter& yout ) const {
  using namespace IRCondKeys;

  yout.SetSeqFormat( YAML::Flow );

  yout << YAML::BeginDoc;
  yout << YAML::BeginMap;
  yout << YAML::Key << irkey;
  yout << YAML::Value << YAML::BeginMap;
  yout << YAML::Key << pkey;
  yout << YAML::Value << YAML::BeginSeq;
  for ( auto x : position ) yout << YAML::DoubleQuoted << format_position( x );
  yout << YAML::EndSeq;

  yout << YAML::Key << skey;
  yout << YAML::Value << YAML::BeginSeq;
  for ( auto x : spread ) yout << YAML::DoubleQuoted << format_spread( x );
  yout << YAML::EndSeq;

  yout << YAML::EndMap << YAML::EndMap;
}

//=============================================================================
// Experimental construction of YAML::Node
// Ultimately, the format of InteractionRegion.yml, with double quoted
//   string values but unquoted string keys, requires a custom emitter method
//   using the current yaml-cpp interface.
//
// This can be a more robuse way of construction the YAML if the formatting
// complications are resolved or determined to be irrelevant.
//=============================================================================
YAML::Node BeamSpotMonitor::IRConditionsCache::asYAML() const {
  using namespace IRCondKeys;

  YAML::Node ir;
  for ( auto x : position ) ir[irkey][pkey].push_back( format_position( x ) );

  for ( auto x : spread ) ir[irkey][skey].push_back( format_spread( x ) );

  ir[irkey][pkey].SetStyle( YAML::EmitterStyle::Flow );
  ir[irkey][skey].SetStyle( YAML::EmitterStyle::Flow );

  return ir;
}

//=============================================================================
// Initialize IRConditionsCache from a file.
//=============================================================================
void BeamSpotMonitor::IRConditionsCache::loadYAMLFile( const std::filesystem::path& condfile ) {
  using namespace IRCondKeys;

  YAML::Node conds = YAML::LoadFile( condfile );

  // Convert position
  // Get position as strings
  const std::vector<std::string> strpos = conds[irkey][pkey].as<std::vector<std::string>>();
  // Verify size
  assert( position.size() == strpos.size() );
  // Transform strings
  // TODO:  Use an expression evaluator that properly treats units.  Such a
  //     parser exists in DetDesc and in DD4hep....
  std::transform( strpos.begin(), strpos.end(), position.begin(), [&]( auto x ) {
    std::size_t idx = 0;
    return std::stod( x, &idx );
  } );

  // Get spread as strings
  const std::vector<std::string> strspr = conds[irkey][skey].as<std::vector<std::string>>();
  // Verify size
  assert( spread.size() == strspr.size() );
  // Transform strings
  // TODO:  Use an expression evaluator that properly treats units.  Such a
  //     parser exists in DetDesc and in DD4hep....
  std::transform( strspr.begin(), strspr.end(), spread.begin(), [&]( auto x ) {
    std::size_t idx = 0;
    return std::stod( x, &idx );
  } );

  nPVs      = 1;
  runNumber = 1;
}

bool BeamSpotMonitor::ymlWriter() const {
  if ( m_cache.nPVs < m_minPVsForCalib ) {
    ++m_badPVCount;
    return false;
  }

  YAML::Emitter ymlBuff;
  m_cache.emitYAML( ymlBuff );

  if ( m_condToLog ) info() << ymlBuff.c_str() << endmsg;

  if ( m_writeDBFiles ) {
    // Adapting file handling from RICH RefIndexCalib
    const std::filesystem::path conditions_dir =
        std::filesystem::path( m_conditionsDbPath.value() ) / std::filesystem::path( m_conditionsPathInDb.value() );

    const auto max_version_nr = find_max_version_number( conditions_dir );

    const std::filesystem::path conditions_file( "v" + std::to_string( max_version_nr + 1 ) );
    const std::filesystem::path full_conditions_path = conditions_dir / conditions_file;

    info() << "Writing new conditions file " << full_conditions_path.string() << endmsg;

    // Outfile.
    std::ofstream logging( full_conditions_path.string().c_str() );
    logging << ymlBuff.c_str() << std::endl;
    logging.close();

    if ( m_onlineMode ) { // Tell Online about new file
      m_pub_msg = std::to_string( m_cache.runNumber ) + " " + conditions_file.string();
      m_publishSvc->updateItem( m_knownAsName );
    }
  }

  return true;
}
