/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Consumer.h"

namespace {
  using PVs    = LHCb::RecVertices;
  using Tracks = LHCb::Track::Range;
} // namespace

class PVToTracksRelationsMonitor final
    : public LHCb::Algorithm::Consumer<void( PVs const&, Tracks const& ), LHCb::Algorithm::Traits::usesConditions<>> {
public:
  using base_type =
      LHCb::Algorithm::Consumer<void( PVs const&, Tracks const& ), LHCb::Algorithm::Traits::usesConditions<>>;
  using KeyValue = base_type::KeyValue;

  PVToTracksRelationsMonitor( std::string const& name, ISvcLocator* pSvc )
      : base_type{ name, pSvc, { KeyValue{ "PVs", "" }, KeyValue{ "Tracks", "" } } } {}

  void operator()( PVs const&, Tracks const& ) const override;

private:
  // properties
  Gaudi::Property<bool> m_error_mode{ this, "ErrorMode", false, "Make it fail if not all tracks are matched" };

  // monitoring
  mutable Gaudi::Accumulators::StatCounter<> m_npvs{ this, "Nb of PVs" };
  mutable Gaudi::Accumulators::StatCounter<> m_npvtracks{ this, "Nb of tracks associated to a PV" };
  mutable Gaudi::Accumulators::StatCounter<> m_ntracks{ this, "Nb of tracks (used in matching)" };

  mutable Gaudi::Accumulators::BinomialCounter<> m_pvtrack_to_track_match{ this, "PV track to track match" };
};

DECLARE_COMPONENT_WITH_ID( PVToTracksRelationsMonitor, "PVToTracksRelationsMonitor" )

void PVToTracksRelationsMonitor::operator()( PVs const& pvs, Tracks const& tracks ) const {
  m_npvs += pvs.size();
  m_ntracks += tracks.size();
  for ( auto const& pv : pvs ) {
    m_npvtracks += pv->tracks().size();
    for ( auto const& pv_track : pv->tracks() ) {
      auto matched_track = std::find_if( tracks.begin(), tracks.end(), pv_track );
      auto found_track   = matched_track != tracks.end();
      m_pvtrack_to_track_match += found_track;
      if ( m_error_mode && !found_track ) {
        throw GaudiException( "Track linked in PV was not found in (original) track container!",
                              "PVToTracksRelationsMonitor", StatusCode::FAILURE );
      }
    }
  }
  return;
}
