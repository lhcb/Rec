/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/PrimaryVertices.h"
#include "Event/RecVertex.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "Event/Track_v2.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/Tuples.h"
#include "GaudiKernel/HistoDef.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiUtils/HistoStats.h"
#include "LHCbAlgs/Consumer.h"
#include "MCInterfaces/IForcedBDecayTool.h"
#include "VPDet/DeVP.h"
#include <Event/MCTrackInfo.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Accumulators/RootHistogram.h>
#include <LHCbDet/InteractionRegion.h>
#include <Linker/LinkedTo.h>
using Vertices   = LHCb::Event::PV::PrimaryVertexContainer;
using VertexType = Vertices::value_type;

// For comparisons between reconstruction algorithms.

// Input:
// - Two sets of reco PVs

// Matching condition:
// - The z-distance between a pair of vertices from reco set 1 and reco set 2 is smaller than min(sqrt(sigma_1**2 +
// sigma_2**2), 0.5)

// Output:
// - PV_nTuple_evt: Sizes and difference of sizes of PVs subsets
// - PV_nTuple: Matched (only) vertices (e.g. pulls, dz, dx, etc.)
// - PV_nTuple_1: Stats of the first passed set of PVs
// - PV_nTuple_2: Stats of the second passed set of PVs

// Example of usage in Moore:

// def make_reconstruction():
//   <your reconstructions of tracks and PVs>
//   vertex_compare = [VertexCompare(inputVerticesName1=pv_container_1, inputVerticesName2=pv_container_2)]
//   return Reconstruction('vertex_compare', vertex_compare)

//-----------------------------------------------------------------------------
// Implementation file for class : VertexCompare
//-----------------------------------------------------------------------------

template <typename VERTEXTYPE>
struct RecPVInfo {
public:
  int                  ntracks     = { 0 }; // number of tracks in a vertex
  int                  nBackTracks = { 0 }; // number of backward tracks in a vertex
  Gaudi::SymMatrix3x3  covPV;
  Gaudi::XYZPoint      position;                  // position
  Gaudi::XYZPoint      positionSigma;             // position sigmas
  int                  indexanothervert = { -1 }; // index to another matched vertex in pair
  VERTEXTYPE const*    pRECPV{ nullptr };
  bool                 rec1, rec2;
  double               chi2   = { 0.0 };
  double               nDoF   = { 0.0 };
  int                  mother = { 0 };
  Gaudi::LorentzVector momentum;
};

bool compZ( const RecPVInfo<VertexType>& first, const RecPVInfo<VertexType>& second ) {
  return ( first.position.Z() > second.position.Z() );
}

bool trackcomp( const RecPVInfo<VertexType>& first, const RecPVInfo<VertexType>& second ) {
  if ( first.ntracks > second.ntracks ) return true;
  if ( first.ntracks < second.ntracks ) return false;
  return compZ( first, second );
}

struct VtxVariables {
  int    ntracks            = -9999;
  double x                  = -9999.;
  double y                  = -9999.;
  double z                  = -9999.;
  double dxr                = -9999.;
  double dyr                = -9999.;
  double r                  = -9999.;
  double errx               = -9999.;
  double erry               = -9999.;
  double errz               = -9999.;
  double err_r              = -9999.;
  double covxx              = -9999.;
  double covyy              = -9999.;
  double covzz              = -9999.;
  double covxy              = -9999.;
  double covxz              = -9999.;
  double covyz              = -9999.;
  double chi2               = -9999.;
  double nDoF               = -9999.;
  int    dsize              = -9999;
  int    match              = -9999;
  int    pv_rank            = -9999;
  bool   equal_sizes        = false;
  bool   single             = false;
  bool   opposite_container = false;
};

VtxVariables SetVtxVariables( const RecPVInfo<VertexType>& vrtf, const int& counter, const int& size1, const int& size2,
                              const double m_beamSpotX, const double m_beamSpotY ) {

  struct VtxVariables vtx;
  if ( vrtf.indexanothervert == -1 ) {
    vtx.match = 0;
  } else {
    vtx.match = 1;
  }

  Gaudi::SymMatrix3x3 covPV_part = vrtf.covPV;
  vtx.covxx                      = ( covPV_part( 0, 0 ) );
  vtx.covyy                      = ( covPV_part( 1, 1 ) );
  vtx.covzz                      = ( covPV_part( 2, 2 ) );
  vtx.covxy                      = ( covPV_part( 0, 1 ) );
  vtx.covxz                      = ( covPV_part( 0, 2 ) );
  vtx.covyz                      = ( covPV_part( 1, 2 ) );
  vtx.chi2                       = vrtf.chi2;
  vtx.nDoF                       = vrtf.nDoF;
  vtx.x                          = vrtf.position.x();
  vtx.y                          = vrtf.position.y();
  vtx.z                          = vrtf.position.z();
  vtx.errx                       = std::sqrt( vtx.covxx );
  vtx.erry                       = std::sqrt( vtx.covyy );
  vtx.errz                       = std::sqrt( vtx.covzz );
  vtx.dxr                        = vtx.x - m_beamSpotX;
  vtx.dyr                        = vtx.y - m_beamSpotY;
  vtx.r                          = std::sqrt( vtx.dxr * vtx.dxr + vtx.dyr * vtx.dyr );
  vtx.err_r =
      std::sqrt( ( ( vtx.dxr * vtx.errx ) * ( vtx.dxr * vtx.errx ) + ( vtx.dyr * vtx.erry ) * ( vtx.dyr * vtx.erry ) ) /
                 ( vtx.dxr * vtx.dxr + vtx.dyr * vtx.dyr ) );

  vtx.ntracks = vrtf.ntracks;

  if ( vtx.ntracks > 1 ) {
    vtx.pv_rank = counter + 1;
  } else {
    vtx.pv_rank = -99999;
  }
  vtx.dsize       = size1 - size2;
  vtx.equal_sizes = ( size1 == size2 );
  if ( ( size1 == 1 ) && ( size2 == 1 ) ) {
    vtx.single = true;
  } else {
    vtx.single = false;
  }
  if ( size2 == 0 ) {
    vtx.opposite_container = false;
  } else {
    vtx.opposite_container = true;
  }

  return vtx;
}

// ============================================================================
// get the error in kurtosis
// ============================================================================

template <typename StatAccumulator, typename FourthSumAccumulator>
double kurtosis( StatAccumulator const& stAcc, FourthSumAccumulator const& stAcc_fourth_sum ) {
  const auto n   = stAcc.nEntries();
  const auto mu4 = stAcc_fourth_sum.sum() / n; // Calculate a 4t moment using statAccumulater of squared variables
  const auto s4  = std::pow( stAcc.standard_deviation(), 4 );
  return ( std::fabs( s4 ) > 0 ? mu4 / s4 - 3.0 : 0.0 );
}

// ============================================================================
// get an error in the rms value
// ============================================================================

template <typename StatAccumulator, typename FourthSumAccumulator>
double rmsErr( StatAccumulator const& stAcc, FourthSumAccumulator const& stAcc_fourth_sum ) {
  const auto n = stAcc.nEntries();
  if ( 1 >= n ) { return 0.0; }
  auto result = 2.0 + kurtosis( stAcc, stAcc_fourth_sum );
  result += 2.0 / ( n - 1 );
  result /= 4.0 * n;
  return stAcc.standard_deviation() * std::sqrt( std::max( result, 0.0 ) );
}

class VertexCompare
    : public LHCb::Algorithm::Consumer<
          void( Vertices const&, Vertices const&, LHCb::Conditions::InteractionRegion const& ),
          LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, LHCb::Conditions::InteractionRegion>> {

public:
  /// Standard constructor
  VertexCompare( std::string const& name, ISvcLocator* pSvcLocator )
      : Consumer{ name,
                  pSvcLocator,
                  { KeyValue{ "inputVerticesName1", LHCb::Event::PV::DefaultLocation },
                    KeyValue{ "inputVerticesName2", LHCb::Event::PV::DefaultLocation },
                    KeyValue{ "InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion" } } } {}

  StatusCode initialize() override; ///< Algorithm initialization
  void       operator()( Vertices const& recoVtx1, Vertices const& recoVtx2,
                   LHCb::Conditions::InteractionRegion const& ) const override; ///< Algorithm execution
  StatusCode finalize() override;                                                     ///< Algorithm finalization

private:
  bool debugLevel() const { return msgLevel( MSG::DEBUG ) || msgLevel( MSG::VERBOSE ); }

  static constexpr auto ntrack_bins = std::array{ 3.5, 15.5, 30.5, 45.5, 58.5, 70.5 };

  Gaudi::Property<bool> m_produceHistogram{ this, "produceHistogram", true };
  Gaudi::Property<bool> m_produceNtuple{ this, "produceNtuple", true };
  Gaudi::Property<bool> m_monitoring{ this, "monitoring", false };
  Gaudi::Property<bool> m_requireSingle{ this, "requireSingle", false };

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_dx;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_dx_fourth_sum;

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_dy;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_dy_fourth_sum;

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_dz;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_dz_fourth_sum;

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_pullx;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_pullx_fourth_sum;

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_pully;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_pully_fourth_sum;

  mutable Gaudi::Accumulators::StatAccumulator<Gaudi::Accumulators::atomicity::full, double> m_stat_pullz;
  mutable Gaudi::Accumulators::SumAccumulator<Gaudi::Accumulators::atomicity::full, double>  m_stat_pullz_fourth_sum;

  struct monitoringHistos {
    mutable std::array<Gaudi::Accumulators::Histogram<1>, 5> m_histo_nTracksBins_dx;
    mutable std::array<Gaudi::Accumulators::Histogram<1>, 5> m_histo_nTracksBins_dy;
    mutable std::array<Gaudi::Accumulators::Histogram<1>, 5> m_histo_nTracksBins_dz;
    mutable Gaudi::Accumulators::Histogram<1>                m_histo_pullx_Monitoring;
    mutable Gaudi::Accumulators::Histogram<1>                m_histo_pully_Monitoring;
    mutable Gaudi::Accumulators::Histogram<1>                m_histo_pullz_Monitoring;

    template <std::size_t... IDXs>
    static std::array<Gaudi::Accumulators::Histogram<1>, sizeof...( IDXs )>
    histo1DArrayBuilder( const VertexCompare* owner, const std::string& name, Gaudi::Histo1DDef def,
                         std::index_sequence<IDXs...> ) {
      return { { { owner,
                   name + std::to_string( IDXs ),
                   def.title() + ", ntracks > " + std::to_string( ntrack_bins[IDXs] ) + " & " +
                       std::to_string( ntrack_bins[IDXs + 1] ) + " <ntracks",
                   { static_cast<unsigned int>( def.bins() ), def.lowEdge(), def.highEdge() } }... } };
    }
    monitoringHistos( const VertexCompare* owner )
        : m_histo_nTracksBins_dx{ histo1DArrayBuilder( owner, "dx_Monitoring_ntracks_bin",
                                                       Gaudi::Histo1DDef{ "dx, mm", -0.15, 0.15, 50 },
                                                       std::make_index_sequence<5>() ) }
        , m_histo_nTracksBins_dy{ histo1DArrayBuilder( owner, "dy_Monitoring_ntracks_bin",
                                                       Gaudi::Histo1DDef{ "dy, mm", -0.15, 0.15, 50 },
                                                       std::make_index_sequence<5>() ) }
        , m_histo_nTracksBins_dz{ histo1DArrayBuilder( owner, "dz_Monitoring_ntracks_bin",
                                                       Gaudi::Histo1DDef{ "dz, mm", -1.5, 1.5, 50 },
                                                       std::make_index_sequence<5>() ) }
        , m_histo_pullx_Monitoring{ owner, "pullx_Monitoring", "pull x", { 20, -5, 5 } }
        , m_histo_pully_Monitoring{ owner, "pully_Monitoring", "pull y", { 20, -5, 5 } }
        , m_histo_pullz_Monitoring{ owner, "pullz_Monitoring", "pull z", { 20, -5, 5 } } {}
  };
  std::unique_ptr<monitoringHistos> m_monitoringHistos;

  mutable std::optional<Gaudi::Accumulators::Histogram<1>>     m_nTracks1;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>     m_nTracks2;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_dx;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_dy;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_dz;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_pullx;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_pully;
  mutable std::optional<Gaudi::Accumulators::RootHistogram<1>> m_histo_pullz;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>     m_nTracks_dif;

  mutable Gaudi::Accumulators::Counter<> m_nVtx{ this, "Number of pairs of vertices in processed events" };
  mutable Gaudi::Accumulators::Counter<> m_nRec1{ this, "Number of vertices in input1" };
  mutable Gaudi::Accumulators::Counter<> m_nRec2{ this, "Number of vertices in input2" };
  mutable Gaudi::Accumulators::Counter<> m_nRec_geq10tr_1{
      this, "Number of vertices in input1 with more or exactly 10 tracks" };
  mutable Gaudi::Accumulators::Counter<> m_nRec_geq10tr_2{
      this, "Number of vertices in input2 with more or exactly 10 tracks" };
  mutable Gaudi::Accumulators::Counter<> m_nVtx_geq10tr_matched1{
      this, "Number of vertices with more or exactly 10 tracks in matched subset of input1" };
  mutable Gaudi::Accumulators::Counter<> m_nVtx_geq10tr_matched2{
      this, "Number of vertices with more or exactly 10 tracks in matched subset of input2" };
  mutable Gaudi::Accumulators::Counter<> m_nEvt{ this, "Event number" };
  void                                   printRat( std::string mes, int a, int b );
  void matchByDistance( std::vector<RecPVInfo<VertexType>>& vert1, std::vector<RecPVInfo<VertexType>>& vert2,
                        std::vector<int>& link ) const;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( VertexCompare, "VertexCompare" )

//=============================================================================
// Initialization
//=============================================================================
StatusCode VertexCompare::initialize() {

  return Consumer::initialize().andThen( [&]() {
    if ( m_monitoring.value() ) m_monitoringHistos = std::make_unique<monitoringHistos>( this );
    // define range of new histograms from properties
    using axis1D = Gaudi::Accumulators::Axis<Gaudi::Accumulators::Histogram<1>::AxisArithmeticType>;

    if ( m_produceHistogram.value() || m_monitoring.value() ) {
      m_nTracks1.emplace( this, "ntracks_set1", "Number of tracks in PV set 1", axis1D{ 50, 0., 150. } );
      m_nTracks2.emplace( this, "ntracks_set2", "Number of tracks in PV set 2", axis1D{ 50, 0., 150. } );
    }
    if ( m_produceHistogram.value() ) {
      m_histo_dx.emplace( this, "dx", "dx, mm", axis1D{ 50, -0.15, 0.15 } );
      m_histo_dy.emplace( this, "dy", "dy, mm", axis1D{ 50, -0.15, 0.15 } );
      m_histo_dz.emplace( this, "dz", "dz, mm", axis1D{ 50, -1.5, 1.5 } );
      m_histo_pullx.emplace( this, "pullx", "pull x", axis1D{ 20, -5, 5 } );
      m_histo_pully.emplace( this, "pully", "pull y", axis1D{ 20, -5, 5 } );
      m_histo_pullz.emplace( this, "pullz", "pull z", axis1D{ 20, -5, 5 } );
      m_nTracks_dif.emplace( this, "dtracks", "Difference in the number of tracks in two sets of PVs",
                             axis1D{ 50, 0., 150. } );
    }
    m_nVtx.reset();
    m_nEvt.reset();
    LHCb::Conditions::InteractionRegion::addConditionDerivation( this,
                                                                 inputLocation<LHCb::Conditions::InteractionRegion>() );
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void VertexCompare::operator()( Vertices const& recoVtx1, Vertices const& recoVtx2,
                                LHCb::Conditions::InteractionRegion const& region ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  const auto                         beamspot = region.avgPosition;
  std::vector<RecPVInfo<VertexType>> fullVrt1, fullVrt2;
  fullVrt1.reserve( recoVtx1.size() );
  fullVrt2.reserve( recoVtx2.size() );

  if ( m_requireSingle == true && !( recoVtx1.size() == 1 && recoVtx2.size() == 1 ) ) return;

  if ( debugLevel() ) debug() << "  Vtx Properities       x       y       z      chi2/ndof     ntracks" << endmsg;

  int                   size_diff = -9999;
  RecPVInfo<VertexType> recinfo1;
  RecPVInfo<VertexType> recinfo2;

  for ( auto const& pv : recoVtx1 ) {
    ++m_nRec1;
    recinfo1.pRECPV        = &pv;
    recinfo1.position      = pv.position();
    recinfo1.covPV         = pv.covMatrix();
    recinfo1.positionSigma = Gaudi::XYZPoint{ std::sqrt( recinfo1.covPV( 0, 0 ) ), std::sqrt( recinfo1.covPV( 1, 1 ) ),
                                              std::sqrt( recinfo1.covPV( 2, 2 ) ) };
    recinfo1.ntracks       = pv.nTracks();
    if ( recinfo1.ntracks >= 10 ) { ++m_nRec_geq10tr_1; }
    recinfo1.chi2 = pv.chi2();
    recinfo1.nDoF = pv.nDoF();

    if ( debugLevel() )
      debug() << "              " << pv.position().x() << "   " << pv.position().y() << "   " << pv.position().z()
              << "   " << pv.chi2PerDoF() << "   " << pv.nTracks() << endmsg;
    fullVrt1.push_back( recinfo1 );
  } // end of loop over vertices1

  for ( auto const& pv : recoVtx2 ) {
    ++m_nRec2;
    recinfo2.pRECPV        = &pv;
    recinfo2.position      = pv.position();
    recinfo2.covPV         = pv.covMatrix();
    recinfo2.positionSigma = Gaudi::XYZPoint{ std::sqrt( recinfo2.covPV( 0, 0 ) ), std::sqrt( recinfo2.covPV( 1, 1 ) ),
                                              std::sqrt( recinfo2.covPV( 2, 2 ) ) };
    recinfo2.ntracks       = pv.nTracks();
    if ( recinfo2.ntracks >= 10 ) { ++m_nRec_geq10tr_2; }
    recinfo2.chi2 = pv.chi2();
    recinfo2.nDoF = pv.nDoF();

    if ( debugLevel() )
      debug() << "              " << pv.position().x() << "   " << pv.position().y() << "   " << pv.position().z()
              << "   " << pv.chi2PerDoF() << "   " << pv.nTracks() << endmsg;
    fullVrt2.push_back( recinfo2 );
  } // end of loop over vertices2

  // added sorting to mirror the 'multirec' variable implementation in PVChecker
  std::sort( fullVrt1.begin(), fullVrt1.end(), trackcomp );
  std::sort( fullVrt2.begin(), fullVrt2.end(), trackcomp );

  if ( debugLevel() ) debug() << "fullVrt1 size   " << fullVrt1.size() << endmsg;
  if ( debugLevel() ) debug() << "fullVrt2 size   " << fullVrt2.size() << endmsg;
  size_diff = fullVrt1.size() - fullVrt2.size();

  if ( m_produceNtuple.value() ) {
    Tuple myTuple_evt = nTuple( "PV_nTuple_evt", "PV_nTuple_evt", CLID_ColumnWiseTuple );
    myTuple_evt->column( "size_diff", double( size_diff ) ).ignore();
    myTuple_evt->column( "size_1", double( fullVrt1.size() ) ).ignore();
    myTuple_evt->column( "size_2", double( fullVrt2.size() ) ).ignore();
    myTuple_evt->write().ignore();
  }

  std::vector<int> link;
  int              ntracks1   = 0;
  int              ntracks2   = 0;
  int              dtracks    = 0;
  int              size1      = 0;
  int              size2      = 0;
  double           sigx_part1 = -99999.;
  double           sigy_part1 = -99999.;
  double           sigz_part1 = -99999.;
  double           sigx_part2 = -99999.;
  double           sigy_part2 = -99999.;
  double           sigz_part2 = -99999.;
  double           x1         = -99999.;
  double           y1         = -99999.;
  double           z1         = -99999.;
  double           x2         = -99999.;
  double           y2         = -99999.;
  double           z2         = -99999.;
  double           dx         = -99999.;
  double           dy         = -99999.;
  double           dz         = -99999.;
  int              oIt        = 0;
  int              pv_rank    = -99999;

  if ( fullVrt1.size() != 0 && fullVrt2.size() != 0 ) { matchByDistance( fullVrt1, fullVrt2, link ); }

  for ( auto const& [counter, vrtf] : LHCb::range::enumerate( fullVrt1 ) ) {
    while ( ( oIt < int( link.size() - 1 ) ) && ( link.at( oIt ) == -1 ) ) { oIt += 1; }
    if ( vrtf.indexanothervert != -1 ) {
      m_nVtx += 1;
      size1              = fullVrt1.size();
      size2              = fullVrt2.size();
      double m_beamSpotX = beamspot.x();
      double m_beamSpotY = beamspot.y();

      VtxVariables set1 = SetVtxVariables( vrtf, counter, size1, size2, m_beamSpotX, m_beamSpotY );
      VtxVariables set2 = SetVtxVariables( fullVrt2[link.at( oIt )], counter, size1, size2, m_beamSpotX, m_beamSpotY );

      sigx_part1 = set1.covxx;
      sigy_part1 = set1.covyy;
      sigz_part1 = set1.covzz;
      sigx_part2 = set2.covxx;
      sigy_part2 = set2.covyy;
      sigz_part2 = set2.covzz;

      x1 = set1.x;
      y1 = set1.y;
      z1 = set1.z;

      x2 = set2.x;
      y2 = set2.y;
      z2 = set2.z;

      dx = x1 - x2;
      dy = y1 - y2;
      dz = z1 - z2;

      double errx = std::sqrt( sigx_part1 + sigx_part2 );
      double erry = std::sqrt( sigy_part1 + sigy_part2 );
      double errz = std::sqrt( sigz_part1 + sigz_part2 );

      ntracks1 = set1.ntracks;
      ntracks2 = set2.ntracks;
      dtracks  = ntracks1 - ntracks2;
      if ( ntracks1 >= 10 ) { ++m_nVtx_geq10tr_matched1; }
      if ( ntracks2 >= 10 ) { ++m_nVtx_geq10tr_matched2; }

      // check for ntracks pass
      // no pass is ntracks == 1
      // we have reco cut on ntracks >=4
      if ( ntracks1 > 1 ) {
        pv_rank = oIt + 1;
      } else if ( ntracks2 > 1 ) {
        pv_rank = link.at( oIt ) + 1;
      }

      double pullx = dx / errx;
      double pully = dy / erry;
      double pullz = dz / errz;
      if ( m_monitoring.value() ) {
        if ( dz < 2 && size_diff < 3 && pv_rank < 2 && ( ntracks1 + ntracks2 ) / 2 > ntrack_bins[0] ) {
          int binCount = 0;
          for ( size_t i = 1; i < ntrack_bins.size(); ++i ) {
            if ( std::lround( ( ntracks1 + ntracks2 ) / 2 ) <= ntrack_bins[i] ) { break; }
            binCount++;
          }
          auto& monitoringHistos = *m_monitoringHistos.get();
          ++monitoringHistos.m_histo_nTracksBins_dx[binCount][dx];
          ++monitoringHistos.m_histo_nTracksBins_dy[binCount][dy];
          ++monitoringHistos.m_histo_nTracksBins_dz[binCount][dz];
          ++monitoringHistos.m_histo_pullx_Monitoring[pullx];
          ++monitoringHistos.m_histo_pully_Monitoring[pully];
          ++monitoringHistos.m_histo_pullz_Monitoring[pullz];
        }
      }
      if ( m_produceHistogram.value() || m_monitoring.value() ) {
        ++m_nTracks1.value()[ntracks1];
        ++m_nTracks2.value()[ntracks2];
      }
      if ( m_produceHistogram.value() ) {
        ++m_histo_dx.value()[dx];
        ++m_histo_dy.value()[dy];
        ++m_histo_dz.value()[dz];
        ++m_histo_pullx.value()[pullx];
        ++m_histo_pully.value()[pully];
        ++m_histo_pullz.value()[pullz];
        ++m_nTracks_dif.value()[ntracks2 - ntracks1];

        m_stat_dx += dx;
        m_stat_dx_fourth_sum += std::pow( dx, 4 );
        m_stat_dy += dy;
        m_stat_dy_fourth_sum += std::pow( dy, 4 );
        m_stat_dz += dz;
        m_stat_dz_fourth_sum += std::pow( dz, 4 );
        m_stat_pullx += pullx;
        m_stat_pullx_fourth_sum += std::pow( pullx, 4 );
        m_stat_pully += pully;
        m_stat_pully_fourth_sum += std::pow( pully, 4 );
        m_stat_pullz += pullz;
        m_stat_pullz_fourth_sum += std::pow( pullz, 4 );
      } // else end

      if ( m_produceNtuple.value() ) {
        Tuple myTuple = nTuple( "PV_nTuple", "PV_nTuple", CLID_ColumnWiseTuple );
        myTuple->column( "ntracks1", double( ntracks1 ) ).ignore();
        myTuple->column( "ntracks2", double( ntracks2 ) ).ignore();
        myTuple->column( "dtracks", double( dtracks ) ).ignore();
        myTuple->column( "dx", dx ).ignore();
        myTuple->column( "dy", dy ).ignore();
        myTuple->column( "dz", dz ).ignore();
        myTuple->column( "x1", x1 ).ignore();
        myTuple->column( "y1", y1 ).ignore();
        myTuple->column( "z1", z1 ).ignore();
        myTuple->column( "x2", x2 ).ignore();
        myTuple->column( "y2", y2 ).ignore();
        myTuple->column( "z2", z2 ).ignore();
        myTuple->column( "dx1r", set1.dxr ).ignore();
        myTuple->column( "dy1r", set1.dyr ).ignore();
        myTuple->column( "dx2r", set2.dxr ).ignore();
        myTuple->column( "dy2r", set2.dyr ).ignore();
        myTuple->column( "r1", set1.r ).ignore();
        myTuple->column( "r2", set2.r ).ignore();
        myTuple->column( "errx", errx ).ignore();
        myTuple->column( "erry", erry ).ignore();
        myTuple->column( "errz", errz ).ignore();
        myTuple->column( "errx1", set1.errx ).ignore();
        myTuple->column( "erry1", set1.erry ).ignore();
        myTuple->column( "errz1", set1.errz ).ignore();
        myTuple->column( "errx2", set2.errx ).ignore();
        myTuple->column( "erry2", set2.erry ).ignore();
        myTuple->column( "errz2", set2.errz ).ignore();
        myTuple->column( "errr1", set1.err_r ).ignore();
        myTuple->column( "errr2", set2.err_r ).ignore();
        myTuple->column( "covxx1", set1.covxx ).ignore();
        myTuple->column( "covyy1", set1.covyy ).ignore();
        myTuple->column( "covzz1", set1.covzz ).ignore();
        myTuple->column( "covxy1", set1.covxy ).ignore();
        myTuple->column( "covxz1", set1.covxz ).ignore();
        myTuple->column( "covyz1", set1.covyz ).ignore();
        myTuple->column( "covxx2", set2.covxx ).ignore();
        myTuple->column( "covyy2", set2.covyy ).ignore();
        myTuple->column( "covzz2", set2.covzz ).ignore();
        myTuple->column( "covxy2", set2.covxy ).ignore();
        myTuple->column( "covxz2", set2.covxz ).ignore();
        myTuple->column( "covyz2", set2.covyz ).ignore();
        myTuple->column( "chi21", set1.chi2 ).ignore();
        myTuple->column( "chi22", set2.chi2 ).ignore();
        myTuple->column( "nDoF1", set1.nDoF ).ignore();
        myTuple->column( "nDoF2", set2.nDoF ).ignore();
        myTuple->column( "size1", int( size1 ) ).ignore();
        myTuple->column( "size2", int( size2 ) ).ignore();
        myTuple->column( "dsize", set1.dsize ).ignore();
        myTuple->column( "beamSpotX", m_beamSpotX ).ignore();
        myTuple->column( "beamSpotY", m_beamSpotY ).ignore();
        myTuple->column( "single", set1.single ).ignore();
        myTuple->column( "equal_sizes", set1.equal_sizes ).ignore();
        myTuple->column( "pullx", dx / errx ).ignore();
        myTuple->column( "pully", dy / erry ).ignore();
        myTuple->column( "pullz", dz / errz ).ignore();
        myTuple->column( "count", int( counter ) ).ignore();
        myTuple->column( "match", set1.match ).ignore();
        myTuple->column( "isopposite", set1.opposite_container ).ignore();
        myTuple->column( "evt", int( m_nEvt.nEntries() ) ).ignore();
        myTuple->column( "pv_rank", int( pv_rank ) ).ignore();
        myTuple->write().ignore();
      }
      oIt++;
      m_nEvt += 1;
    }
  }

  for ( auto const& [counter1, v1] : LHCb::range::enumerate( fullVrt1 ) ) {

    size1              = fullVrt1.size();
    size2              = fullVrt2.size();
    double m_beamSpotX = beamspot.x();
    double m_beamSpotY = beamspot.y();

    VtxVariables set1 = SetVtxVariables( v1, counter1, size1, size2, m_beamSpotX, m_beamSpotY );

    if ( m_produceNtuple.value() ) {
      Tuple myTuple = nTuple( "PV_nTuple_1", "PV_nTuple_1", CLID_ColumnWiseTuple );
      myTuple->column( "ntracks1", set1.ntracks ).ignore();
      myTuple->column( "x1", set1.x ).ignore();
      myTuple->column( "y1", set1.y ).ignore();
      myTuple->column( "z1", set1.z ).ignore();
      myTuple->column( "dx1r", set1.dxr ).ignore();
      myTuple->column( "dy1r", set1.dyr ).ignore();
      myTuple->column( "r1", set1.r ).ignore();
      myTuple->column( "errx1", set1.errx ).ignore();
      myTuple->column( "erry1", set1.erry ).ignore();
      myTuple->column( "errz1", set1.errz ).ignore();
      myTuple->column( "errr1", set1.err_r ).ignore();
      myTuple->column( "covxx1", set1.covxx ).ignore();
      myTuple->column( "covyy1", set1.covyy ).ignore();
      myTuple->column( "covzz1", set1.covzz ).ignore();
      myTuple->column( "covxy1", set1.covxy ).ignore();
      myTuple->column( "covxz1", set1.covxz ).ignore();
      myTuple->column( "covyz1", set1.covyz ).ignore();
      myTuple->column( "chi21", set1.chi2 ).ignore();
      myTuple->column( "nDoF1", set1.nDoF ).ignore();
      myTuple->column( "size1", int( size1 ) ).ignore();
      myTuple->column( "size2", int( size2 ) ).ignore();
      myTuple->column( "dsize", set1.dsize ).ignore();
      myTuple->column( "beamSpotX", m_beamSpotX ).ignore();
      myTuple->column( "beamSpotY", m_beamSpotY ).ignore();
      myTuple->column( "single", set1.single ).ignore();
      myTuple->column( "equal_sizes", set1.equal_sizes ).ignore();
      myTuple->column( "count", int( counter1 ) ).ignore();
      myTuple->column( "match", set1.match ).ignore();
      myTuple->column( "pv_rank", set1.pv_rank ).ignore();
      myTuple->column( "isopposite", set1.opposite_container ).ignore();
      myTuple->column( "evt", int( m_nEvt.nEntries() ) ).ignore();
      myTuple->write().ignore();
    }
    oIt++;
    m_nEvt += 1;
  }

  for ( auto const& [counter2, v2] : LHCb::range::enumerate( fullVrt2 ) ) {

    size1              = fullVrt1.size();
    size2              = fullVrt2.size();
    double m_beamSpotX = beamspot.x();
    double m_beamSpotY = beamspot.y();

    VtxVariables set2 = SetVtxVariables( v2, counter2, size1, size2, m_beamSpotX, m_beamSpotY );

    if ( m_produceNtuple.value() ) {
      Tuple myTuple = nTuple( "PV_nTuple_2", "PV_nTuple_2", CLID_ColumnWiseTuple );
      myTuple->column( "ntracks2", set2.ntracks ).ignore();
      myTuple->column( "x2", set2.x ).ignore();
      myTuple->column( "y2", set2.y ).ignore();
      myTuple->column( "z2", set2.z ).ignore();
      myTuple->column( "dx2r", set2.dxr ).ignore();
      myTuple->column( "dy2r", set2.dyr ).ignore();
      myTuple->column( "r2", set2.r ).ignore();
      myTuple->column( "errx2", set2.errx ).ignore();
      myTuple->column( "erry2", set2.erry ).ignore();
      myTuple->column( "errz2", set2.errz ).ignore();
      myTuple->column( "errr2", set2.err_r ).ignore();
      myTuple->column( "covxx2", set2.covxx ).ignore();
      myTuple->column( "covyy2", set2.covyy ).ignore();
      myTuple->column( "covzz2", set2.covzz ).ignore();
      myTuple->column( "covxy2", set2.covxy ).ignore();
      myTuple->column( "covxz2", set2.covxz ).ignore();
      myTuple->column( "covyz2", set2.covyz ).ignore();
      myTuple->column( "chi22", set2.chi2 ).ignore();
      myTuple->column( "nDoF2", set2.nDoF ).ignore();
      myTuple->column( "size1", int( size1 ) ).ignore();
      myTuple->column( "size2", int( size2 ) ).ignore();
      myTuple->column( "dsize", set2.dsize ).ignore();
      myTuple->column( "beamSpotX", m_beamSpotX ).ignore();
      myTuple->column( "beamSpotY", m_beamSpotY ).ignore();
      myTuple->column( "single", set2.single ).ignore();
      myTuple->column( "equal_sizes", set2.equal_sizes ).ignore();
      myTuple->column( "count", int( counter2 ) ).ignore();
      myTuple->column( "match", set2.match ).ignore();
      myTuple->column( "pv_rank", set2.pv_rank ).ignore();
      myTuple->column( "isopposite", set2.opposite_container ).ignore();
      myTuple->column( "evt", int( m_nEvt.nEntries() ) ).ignore();
      myTuple->write().ignore();
    }
    oIt++;
    m_nEvt += 1;
  }
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode VertexCompare::finalize() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;

  info() << " ============================================" << endmsg;
  info() << " Efficiencies for reconstructed vertices:    " << endmsg;
  info() << " ============================================" << endmsg;
  info() << " " << endmsg;
  info() << " Total number of PVs in input1: " << m_nRec1.nEntries() << ", input2: " << m_nRec2.nEntries()
         << ", matched " << m_nVtx.nEntries() << endmsg;
  info() << " Efficiency wrt input1 size: " << (double)m_nVtx.nEntries() / (double)m_nRec1.nEntries() * 100 << " % "
         << endmsg;
  info() << " Efficiency wrt input2 size: " << (double)m_nVtx.nEntries() / (double)m_nRec2.nEntries() * 100 << " % "
         << endmsg;
  info() << "      ---------------------------------------" << endmsg;
  info() << " Number of PVs with ntracks>=10: " << endmsg;
  info() << " in input1 " << m_nRec_geq10tr_1.nEntries() << ", matched from input1 "
         << m_nVtx_geq10tr_matched1.nEntries() << endmsg;
  info() << " in input2 " << m_nRec_geq10tr_2.nEntries() << ", matched from input2 "
         << m_nVtx_geq10tr_matched2.nEntries() << endmsg;
  info() << " Efficiency wrt input1, ntracks>=10: "
         << (double)m_nVtx_geq10tr_matched1.nEntries() / (double)m_nRec_geq10tr_1.nEntries() * 100 << " % " << endmsg;
  info() << " Efficiency wrt input2, ntracks>=10: "
         << (double)m_nVtx_geq10tr_matched2.nEntries() / (double)m_nRec_geq10tr_2.nEntries() * 100 << " % " << endmsg;
  info() << " Efficiency wrt input1, ntracks<10:  "
         << ( (double)m_nVtx.nEntries() - (double)m_nVtx_geq10tr_matched1.nEntries() ) /
                ( (double)m_nRec1.nEntries() - (double)m_nRec_geq10tr_1.nEntries() ) * 100
         << " % " << endmsg;
  info() << " Efficiency wrt input2, ntracks<10:  "
         << ( (double)m_nVtx.nEntries() - (double)m_nVtx_geq10tr_matched2.nEntries() ) /
                ( (double)m_nRec2.nEntries() - (double)m_nRec_geq10tr_2.nEntries() ) * 100
         << " % " << endmsg;

  if ( m_produceHistogram.value() ) {
    info() << "dx: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_dx.sum() ) / m_stat_dx.nEntries(), m_stat_dx.meanErr(),
                      m_stat_dx.standard_deviation(), rmsErr( m_stat_dx, m_stat_dx_fourth_sum ) )
           << endmsg;
    info() << "dy: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_dy.sum() ) / m_stat_dy.nEntries(), m_stat_dy.meanErr(),
                      m_stat_dy.standard_deviation(), rmsErr( m_stat_dy, m_stat_dy_fourth_sum ) )
           << endmsg;
    info() << "dz: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_dz.sum() ) / m_stat_dz.nEntries(), m_stat_dz.meanErr(),
                      m_stat_dz.standard_deviation(), rmsErr( m_stat_dz, m_stat_dz_fourth_sum ) )
           << endmsg;
    info() << "      ---------------------------------------" << endmsg;

    info() << "pullx: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_pullx.sum() ) / m_stat_pullx.nEntries(), m_stat_pullx.meanErr(),
                      m_stat_pullx.standard_deviation(), rmsErr( m_stat_pullx, m_stat_pullx_fourth_sum ) )
           << endmsg;

    info() << "pully: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_pully.sum() ) / m_stat_pully.nEntries(), m_stat_pully.meanErr(),
                      m_stat_pully.standard_deviation(), rmsErr( m_stat_pully, m_stat_pully_fourth_sum ) )
           << endmsg;

    info() << "pullz: "
           << format( "mean =  %5.3f +/- %5.3f, RMS =  %5.3f +/- %5.3f",
                      static_cast<double>( m_stat_pullz.sum() ) / m_stat_pullz.nEntries(), m_stat_pullz.meanErr(),
                      m_stat_pullz.standard_deviation(), rmsErr( m_stat_pullz, m_stat_pullz_fourth_sum ) )
           << endmsg;

    info() << "      ---------------------------------------" << endmsg;

    info() << "diff in x: "
           << format( "%5.3f +/- %5.3f", std::sqrt( 1.0 + m_stat_pullx.standard_deviation() ) - 1.0,
                      rmsErr( m_stat_pullx, m_stat_pullx_fourth_sum ) * 0.5 /
                          std::sqrt( 1.0 + m_stat_pullx.standard_deviation() ) )
           << endmsg;

    info() << "diff in y: "
           << format( "%5.3f +/- %5.3f", std::sqrt( 1.0 + m_stat_pully.standard_deviation() ) - 1.0,
                      rmsErr( m_stat_pully, m_stat_pully_fourth_sum ) * 0.5 /
                          std::sqrt( 1.0 + m_stat_pully.standard_deviation() ) )
           << endmsg;

    info() << "diff in z: "
           << format( "%5.3f +/- %5.3f", std::sqrt( 1.0 + m_stat_pullz.standard_deviation() ) - 1.0,
                      rmsErr( m_stat_pullz, m_stat_pullz_fourth_sum ) * 0.5 /
                          std::sqrt( 1.0 + m_stat_pullz.standard_deviation() ) )
           << endmsg;
    info() << "      ============================================" << endmsg;
  }
  return Consumer::finalize(); // Must be called after all other actions
}

//=============================================================================
//  Match vertices by distance
//=============================================================================
void VertexCompare::matchByDistance( std::vector<RecPVInfo<VertexType>>& vert1,
                                     std::vector<RecPVInfo<VertexType>>& vert2, std::vector<int>& link ) const {

  if ( vert2.size() > vert1.size() && debugLevel() ) debug() << "half.size > full.size" << endmsg;
  for ( int imc = 0; imc < (int)vert1.size(); imc++ ) {
    double       mindist         = 999999.;
    int          indexrec        = -1;
    double       sigma1          = vert1[imc].positionSigma.z();
    double       sigma2          = 0.0;
    double       combined_sigma  = 0.0;
    const double sigma_threshold = 0.5;

    for ( int irec = 0; irec < (int)vert2.size(); irec++ ) {
      if ( std::count( link.begin(), link.end(), irec ) != 0 ) continue;
      double dist    = fabs( vert2[irec].position.z() - vert1[imc].position.z() );
      sigma2         = vert2[irec].positionSigma.z();
      combined_sigma = std::sqrt( sigma1 * sigma1 + sigma2 * sigma2 );
      if ( combined_sigma > sigma_threshold ) { combined_sigma = sigma_threshold; }
      if ( dist < mindist ) {
        if ( dist < 5.0 * combined_sigma ) {
          mindist  = dist;
          indexrec = irec;
        }
      }
    }
    vert1[imc].indexanothervert = indexrec;
    if ( indexrec != -1 ) { vert2[indexrec].indexanothervert = imc; }
    if ( debugLevel() ) debug() << "original vertex " << imc << " linked to " << indexrec << " half vertex." << endmsg;
    link.push_back( indexrec );
  }

  for ( int imc = 0; imc < (int)vert2.size(); imc++ ) {
    int count = std::count( link.begin(), link.end(), imc );
    if ( count > 1 && debugLevel() ) debug() << "linked twice to vertex " << imc << endmsg;
  }
}

//=============================================================================
//  printRat
//=============================================================================
void VertexCompare::printRat( std::string mes, int a, int b ) {

  double rat = 0.;
  if ( b > 0 ) rat = 1.0 * a / b;

  // reformat message
  unsigned int len  = 20;
  std::string  pmes = mes;
  while ( pmes.length() < len ) { pmes += " "; }
  pmes += " : ";

  info() << pmes << format( " %6.3f ( %7d / %8d )", rat, a, b ) << endmsg;
}
