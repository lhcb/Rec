/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/FitNode.h"
#include "Event/KalmanFitResult.h"
#include "Event/Measurement.h"
#include "Event/PrFitNode.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackParameters.h"
#include "FTDet/DeFTDetector.h"
#include "FTDet/DeFTMat.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Consumer.h"
#include "Map.h"
#include "TrackKernel/TrackFunctors.h"
#include "TrackMonitorTupleBase.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <fmt/format.h>
#include <yaml-cpp/yaml.h>

namespace FTConstants = LHCb::Detector::FT;
// backward compatibility with Gaudi < v39 where StaticProfileHistogram is just the plain ProfileHistogram
#if GAUDI_MAJOR_VERSION < 39
namespace Gaudi::Accumulators {
  template <unsigned int ND, atomicity Atomicity = atomicity::full, typename Arithmetic = double>
  using StaticProfileHistogram = ProfileHistogram<ND, Atomicity, Arithmetic>;
} // namespace Gaudi::Accumulators
#endif

class FTMatCalibrationMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range& tracks, const DeFT& ),
                                       LHCb::Algorithm::Traits::usesBaseAndConditions<TrackMonitorTupleBase, DeFT>> {

public:
  FTMatCalibrationMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "TracksInContainer", LHCb::TrackLocation::Default },
                    KeyValue{ "FTDetectorLocation", DeFTDetectorLocation::Default } } ){};

  StatusCode initialize() override { return Consumer::initialize(); }

  StatusCode finalize() override {
    return Consumer::finalize().andThen( [&] {
      if ( m_produceYamlFile ) {
        YAML::Node yamlNode;
        // function that subtracts the average from a sequence of optionals
        auto subtractaverage = []( auto begin, auto end ) {
          size_t n{ 0 };
          double sum{ 0 };
          std::for_each( begin, end, [&n, &sum]( const auto& i ) {
            if ( i ) {
              ++n;
              sum += *i;
            };
          } );
          if ( n > 0 ) {
            const double av = sum / n;
            std::for_each( begin, end, [&av]( auto& i ) {
              if ( i ) { ( *i ) -= av; };
            } );
          }
        };

        // Loop over the stations, layers, quarters, modules and mats to make the names of the conditions as strings
        constexpr auto nChannelsPerMat    = FTConstants::nChannels * FTConstants::nSiPM;
        constexpr auto nChannelsPerModule = nChannelsPerMat * FTConstants::nMats;
        for ( const auto stationID : LHCb::Detector::FTChannelID::allStationIDs ) {
          for ( const auto layerID : LHCb::Detector::FTChannelID::allLayerIDs ) {
            for ( const auto quarterID : LHCb::Detector::FTChannelID::allQuarterIDs ) {
              for ( const auto moduleID : LHCb::Detector::FTChannelID::allModuleIDsInStation( stationID ) ) {
                // compute the deltas per module such that we can make a half-module average.
                // use std::optional such that we can keep track of which values were set.
                std::array<std::optional<float>, nChannelsPerModule> deltas;
                deltas.fill( std::optional<float>{} ); // does default initialization work?

                for ( const auto matID : LHCb::Detector::FTChannelID::allMatIDs ) {
                  LHCb::Detector::FTChannelID chanID_mat( stationID, layerID, quarterID, moduleID, matID, 0, 0 );
                  const auto matoffset = nChannelsPerMat * to_unsigned( matID ); //---LoH: TODO: Detector!442
                  // the local and global numbering can be different depending on the quarter

                  // get the histogram for this mat
                  const auto& matHisto = m_histograms_mat_wrt0.at( chanID_mat.globalMatIdx() );

                  if ( m_calibmodel == Model::PerChannel ) {
                    for ( std::size_t ichan = 1; ichan < nChannelsPerMat; ++ichan ) {
                      const auto& bincontents = matHisto.UnbiasedresidualPerChannel.binValue( ichan + 1 );
                      const float sumOfSqs    = std::get<1>( bincontents );
                      const float sum         = std::get<1>( std::get<0>( bincontents ) );
                      const auto  nEntries    = std::get<0>( std::get<0>( bincontents ) );
                      const float val         = sum / nEntries;
                      const auto  variance    = sumOfSqs / nEntries - val * val;
                      const auto  error       = sqrt( variance );
                      // it is not necessary to use the variance: to measure an mean with a precision of 1/sqrt(N) of
                      // the variance of the parent distribution, you need at least N entries. so here we'd like N to
                      // be at least 100 or so.
                      if ( nEntries >= m_minEntries && val / error > m_significanceThreshold ) {
                        deltas[ichan + matoffset] = -1 * val;
                        // initialized[ichan + matoffset ] = true;
                      }
                    }
                  } else {
                    for ( unsigned iasic = 0; iasic < FTConstants::nSiPM; ++iasic ) {
                      // perform a fit to the model y = [0] + [1]*x
                      using Vector    = ROOT::Math::SVector<double, 2>;
                      using SymMatrix = ROOT::Math::SMatrix<double, 2, 2, ROOT::Math::MatRepSym<double, 2>>;
                      Vector    halfDChi2DX;
                      SymMatrix halfD2Chi2DX2;
                      for ( unsigned ichan = 0; ichan < FTConstants::nChannels; ++ichan ) {
                        const auto   ibin        = iasic * FTConstants::nChannels + ichan + 1;
                        const auto&  bincontents = matHisto.UnbiasedresidualPerChannel.binValue( ibin );
                        const double x           = ichan;
                        const double wy          = std::get<1>( std::get<0>( bincontents ) );
                        const auto   w           = std::get<0>( std::get<0>( bincontents ) );
                        halfDChi2DX( 0 ) += -wy;
                        halfDChi2DX( 1 ) += -wy * x;
                        halfD2Chi2DX2( 0, 0 ) += w;
                        halfD2Chi2DX2( 0, 1 ) += w * x;
                        halfD2Chi2DX2( 1, 1 ) += w * x * x;
                      }
                      if ( halfD2Chi2DX2( 0, 0 ) > m_minEntries ) {
                        SymMatrix  C  = halfD2Chi2DX2;
                        const auto ok = C.Invert();
                        if ( ok ) {
                          Vector par = -C * halfDChi2DX;
                          // compute the channel deltas from the model
                          for ( unsigned ichan = 0; ichan < FTConstants::nChannels; ++ichan )
                            deltas[ichan + iasic * FTConstants::nChannels + matoffset] = -( par[0] + ichan * par[1] );
                        }
                      }
                    }
                  }
                  // subtract the average per mat (only if we run separate alignment for mat Tx)
                  if ( m_subtractMatAverage )
                    subtractaverage( deltas.begin() + matoffset, deltas.begin() + matoffset + nChannelsPerMat );
                } // end loop over mats

                // subtract the average per module
                subtractaverage( deltas.begin(), deltas.end() );

                // for each mat write the results to yaml
                for ( const auto matID : LHCb::Detector::FTChannelID::allMatIDs ) {
                  const auto        matoffset = nChannelsPerMat * to_unsigned( matID );
                  const std::string conditionName =
                      fmt::format( "matContraction{matName}",
                                   fmt::arg( "matName", LHCb::Detector::FTChannelID::matStringName(
                                                            stationID, layerID, quarterID, moduleID, matID ) ) );
                  yamlNode["matContraction"][conditionName] =
                      YAML::Load( "[]" ); // is there a better way to make it appear flow style?
                  auto thisnode = yamlNode["matContraction"][conditionName];
                  std::for_each( deltas.begin() + matoffset, deltas.begin() + matoffset + nChannelsPerMat,
                                 [&thisnode]( const auto& d ) { thisnode.push_back( d ? *d : 0.0 ); } );
                } // end loop over mats
              }   // end loop over modules
            }     // end loop over quarters
          }       // end loop over layers
        }         // end loop over stations
        std::ofstream fileOut( m_outputFileName );
        fileOut << yamlNode;
        fileOut.close();
      } // end if
    } );
  }

  void operator()( const LHCb::Track::Range& tracks, const DeFT& ftDet ) const override;

private:
  enum Model { PerChannel = 0, PerAsic = 1 };
  Gaudi::Property<bool>        m_produceYamlFile{ this, "ProduceYamlFile", false };
  Gaudi::Property<std::string> m_outputFileName{ this, "OutputFileName", "matContraction.yaml" };
  Gaudi::Property<float>       m_significanceThreshold{ this, "SignificanceThreshold", -100. };
  Gaudi::Property<unsigned>    m_minEntries{ this, "MinEntriesPerChannel", 100 };
  Gaudi::Property<int>         m_calibmodel{ this, "Model", Model::PerAsic };
  Gaudi::Property<bool>        m_subtractMatAverage{ this, "SubtractMatAverage", false };

  template <typename TFitResult>
  void fillContainers( const TFitResult& track, const DeFT& ftDet ) const;

private:
  // histogram to look at the unbiased residual, considering the current calibration values
  struct DifferencePerChannelHistogram {
    mutable Gaudi::Accumulators::StaticProfileHistogram<1> UnbiasedresidualPerChannel;

    DifferencePerChannelHistogram( const FTMatCalibrationMonitor* owner, std::string const& mat )
        : UnbiasedresidualPerChannel{
              owner,
              "DeltaUnbiasedresidualPerChannel" + mat,
              "Difference in unbiased residual per Channel, " + mat,
              { FTConstants::nSiPM * FTConstants::nChannels, 0, FTConstants::nSiPM * FTConstants::nChannels } } {}
  };

  // histogram to look at the unbiased residual, subtracting the current calibration values (i.e. residual wrt no
  // calibration)
  struct Ref0PerChannelHistogram {
    mutable Gaudi::Accumulators::StaticProfileHistogram<1> UnbiasedresidualPerChannel;

    Ref0PerChannelHistogram( const FTMatCalibrationMonitor* owner, std::string const& mat )
        : UnbiasedresidualPerChannel{
              owner,
              "Ref0UnbiasedresidualPerChannel" + mat,
              "Unbiased residual per Channel wrt 0, " + mat,
              { FTConstants::nSiPM * FTConstants::nChannels, 0, FTConstants::nSiPM * FTConstants::nChannels } } {}
  };

  std::map<unsigned int, DifferencePerChannelHistogram> m_histograms_mat_wrt_current_cond =
      []( const FTMatCalibrationMonitor* parent ) {
        std::map<unsigned int, DifferencePerChannelHistogram> map;
        for ( size_t globalMatIdx = 0; globalMatIdx < LHCb::Detector::FT::nMatsTotal; globalMatIdx++ ) {
          // todo: make naming consistent with the condition file
          map.emplace( std::piecewise_construct, std::forward_as_tuple( globalMatIdx ),
                       std::forward_as_tuple( parent, "GlobalMat" + std::to_string( globalMatIdx ) ) );
        }
        return map;
      }( this );

  std::map<unsigned int, Ref0PerChannelHistogram> m_histograms_mat_wrt0 = []( const FTMatCalibrationMonitor* parent ) {
    std::map<unsigned int, Ref0PerChannelHistogram> map;
    for ( size_t globalMatNumber = 0; globalMatNumber < LHCb::Detector::FT::nMatsTotal; globalMatNumber++ ) {
      // todo: make naming consistent with the condition file
      map.emplace( std::piecewise_construct, std::forward_as_tuple( globalMatNumber ),
                   std::forward_as_tuple( parent, "GlobalMat" + std::to_string( globalMatNumber ) ) );
    }
    return map;
  }( this );
};

DECLARE_COMPONENT_WITH_ID( FTMatCalibrationMonitor, "FTMatCalibrationMonitor" );

void FTMatCalibrationMonitor::operator()( const LHCb::Track::Range& tracks, const DeFT& ftDet ) const {

  for ( const LHCb::Track* track : tracks ) {
    const auto type = track->type();
    if ( !LHCb::Event::Enum::Track::hasT( type ) ) { continue; }
    if ( track->checkFitStatus( LHCb::Track::FitStatus::Fitted ) ) {
      const auto fr    = track->fitResult();
      const auto prkfr = dynamic_cast<const LHCb::PrKalmanFitResult*>( fr );
      if ( prkfr ) {
        fillContainers( *prkfr, ftDet );
      } else {
        const auto tmffr = dynamic_cast<const LHCb::KalmanFitResult*>( fr );
        if ( tmffr ) {
          fillContainers( *tmffr, ftDet );
        } else {
          throw GaudiException( "Fit result is NULL - track was not fitted or uses wrong fit result type", name(),
                                StatusCode::FAILURE );
        }
      }
    }
  }
}

namespace {
  Gaudi::XYZPoint mpos( const LHCb::FitNode& node ) {
    const LHCb::Measurement& meas = node.measurement();
    LHCb::LineTraj<double>   mtraj =
        meas.visit( [&]( const auto& m ) { return m.trajectory; },
                    [&]( const LHCb::Measurement::VP2D& ) {
                      throw std::runtime_error( "Alignment derivatives not implemented for 2D measurements." );
                      LHCb::LineTraj<double> dummy;
                      return dummy;
                    } );
    return mtraj.beginPoint();
  }
  Gaudi::XYZPoint mpos( const LHCb::Pr::Tracks::Fit::Node& node ) {
    return Gaudi::XYZPoint{ node.measurement_pos[0], node.measurement_pos[1], node.measurement_pos[2] };
  }
} // namespace

template <typename TFitResult>
void FTMatCalibrationMonitor::fillContainers( const TFitResult& fitResult, const DeFT& ftDet ) const {

  for ( const auto& node : nodes( fitResult ) ) {
    if ( !node.hasMeasurement() ) continue;
    if ( node.isHitOnTrack() && node.isFT() ) {
      LHCb::LHCbID lhcbID = id( node );
      assert( lhcbID.isFT() );

      LHCb::Detector::FTChannelID chan         = lhcbID.ftID();
      const auto                  globalMatIdx = chan.globalMatIdx();

      // get the histograms for this mat
      auto&       histos_mats_wrt_current_cond = m_histograms_mat_wrt_current_cond.at( globalMatIdx );
      auto&       histos_mats_wrt0             = m_histograms_mat_wrt0.at( globalMatIdx );
      const auto& ftMat                        = ftDet.findMat( chan );
      const auto  ichan =
          chan.channel() + chan.sipm() * FTConstants::nChannels; //---LoH: This is in local orientation (not CtoE)

      // get the unbiased track state
      const auto trkstate = node.unbiasedState();
      // rotate position and direction to the local frame
      const auto localtrkpos = ftMat->toLocal( trkstate.position() );
      const auto localtrkdir = ftMat->toLocal( trkstate.slopes() );
      // compute the local x coordinate at local z = 0
      const auto localtrkx = localtrkpos.x() - localtrkpos.z() * localtrkdir.x() / localtrkdir.z();
      // now do the same for the nodes measurement position: this should come out with z=0
      const auto localmpos = ftMat->toLocal( mpos( node ) );
      // the difference between the two is the residual that we need
      const auto residual = localmpos.x() - localtrkx;
      // You could check that the local z position is (very close) to zero:
      // std::cout << "Local mpos: " << localmpos << std::endl ;

      histos_mats_wrt_current_cond.UnbiasedresidualPerChannel[ichan] += residual;
      // double negative
      histos_mats_wrt0.UnbiasedresidualPerChannel[ichan] += residual - ftMat->getmatContractionParameterVector()[ichan];
    }
  }
}
