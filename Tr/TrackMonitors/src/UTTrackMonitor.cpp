/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "UTDet/DeUTDetector.h"

#include "Event/Measurement.h"
#include "Event/State.h"
#include "Event/Track.h"
// #include "PrKernel/UTHitHandler.h"
#include "Event/UTHitCluster.h"

#include "Event/FitNode.h"
#include "TrackKernel/TrackFunctors.h"

#include "Gaudi/Accumulators/Histogram.h"
#include "Gaudi/Accumulators/HistogramArray.h"
#include "GaudiKernel/PhysicalConstants.h"

#include "Kernel/LHCbID.h"
#include "Kernel/UTNames.h"
#include "UTDAQ/UTInfo.h"

#include "LHCbAlgs/Consumer.h"

using namespace LHCb;
using namespace Gaudi;

namespace {
  const unsigned int NUTLAYERS = 4;
}

namespace LHCb {
  class UTTrackMonitor
      : public LHCb::Algorithm::Consumer<void( LHCb::Track::Range const&, UTHitClusters const&, DeUTDetector const& ),
                                         LHCb::DetDesc::usesConditions<DeUTDetector>> {

  public:
    UTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( LHCb::Track::Range const&, UTHitClusters const&, DeUTDetector const& ) const override;

  private:
    void fillHistograms( LHCb::Track const& track, UTHitClusters const& utHits, DeUTDetector const& deUT ) const;

    double            m_xMax;
    double            m_yMax;
    const std::string sensor_types = "ABCD";

    Gaudi::Property<double> m_refZ{ this,
                                    "ReferenceZ",
                                    2485.0,
                                    [this]( auto& ) {
                                      // guess that the size of the histograms at ref is ~ 0.35 by 0.3
                                      this->m_xMax = 0.35 * this->m_refZ / Gaudi::Units::cm;
                                      this->m_yMax = 0.3 * this->m_refZ / Gaudi::Units::cm;
                                    },
                                    Gaudi::Details::Property::ImmediatelyInvokeHandler{ true },
                                    "midpoint of UT" };

    Gaudi::Property<unsigned int> m_minNumUTHits{ this, "minNumUTHits", 2u };

    mutable Gaudi::Accumulators::Histogram<1> m_histogram_trackTx{
        this, "UTTrackTx", "dx/dz of fit nodes with UT hits; dx/dz;", { 200, -0.2, 0.2 } };
    mutable Gaudi::Accumulators::Histogram<1> m_histogram_trackTy{
        this, "UTTrackTy", "dy/dz of fit nodes with UT hits; dy/dz", { 200, -0.2, 0.2 } };
    mutable Gaudi::Accumulators::Histogram<1> m_histogram_trackP{
        this, "UTTrackP", "|p| of states for UT hits; |p|", { 40, 0.0, 125000 } };

    struct HistogramsPerLayer {
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_stateX;
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_stateY;
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_unbiasedResidual;
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_biasedResidual;
      mutable Gaudi::Accumulators::Histogram<2>        m_histogram_frac_strip_cluster_size;
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_cluster_size;
      mutable Gaudi::Accumulators::Histogram<1>        m_histogram_cluster_size_zoomed;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_size_vs_tx;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_size_vs_ty;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_size_vs_t2;
      mutable Gaudi::Accumulators::Histogram<2>        m_histogram_cluster_charge_cluster_size;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_charge_vs_tx;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_charge_vs_ty;
      mutable Gaudi::Accumulators::ProfileHistogram<1> m_histogram_cluster_charge_vs_t2;
      mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<1>, 4>
          m_histogram_cluster_charge_sensors;
      mutable Gaudi::Accumulators::HistogramArray<Gaudi::Accumulators::Histogram<2>, 4>
                                                m_histogram_cluster_charge_sensors_strips;
      mutable Gaudi::Accumulators::Histogram<1> m_histogram_overlap_residual;
      mutable Gaudi::Accumulators::Histogram<1> m_histogram_overlap_first_residual;
      mutable Gaudi::Accumulators::Histogram<1> m_histogram_overlap_second_residual;
      mutable Gaudi::Accumulators::Histogram<2> m_histogram_stateXY;
      mutable Gaudi::Accumulators::Histogram<1> m_histogram_n_used_cluster;
      HistogramsPerLayer( const UTTrackMonitor* owner, std::string const& uniqueName )
          : m_histogram_stateX( owner, uniqueName + "UTStateX", "x of states for UT hits; x;",
                                { 100, -0.35 * 2500, 0.35 * 2500 } )
          , m_histogram_stateY{ owner,
                                uniqueName + "UTStateY",
                                "y of states for UT hits; y;",
                                { 100, -0.32 * 2500, 0.32 * 2500 } }
          , m_histogram_unbiasedResidual{ owner,
                                          uniqueName + "unbiasedResidual",
                                          "'unbiased' residual of hits on track used in the track fit; 'unbiased' "
                                          "residual;",
                                          { 200, -2, 2. } }
          , m_histogram_biasedResidual{ owner,
                                        uniqueName + "biasedResidual",
                                        "Residual of hits on track *used* in the track fit; biased residual;",
                                        { 200, -2, 2. } }
          , m_histogram_frac_strip_cluster_size{ owner,
                                                 uniqueName + "fracSrip_clusterSize",
                                                 "Fractional strip position and cluster size correlation matrix; frac "
                                                 "strip; cluster size",
                                                 { 100, -0.5, 0.5 },
                                                 { 4, 1.5, 5.5 } }
          , m_histogram_cluster_size{ owner,
                                      uniqueName + "clusterSize",
                                      "Cluster size; cluster size;",
                                      { 50, -0.5, 49.5 } }
          , m_histogram_cluster_size_zoomed{ owner,
                                             uniqueName + "clusterSizeZoomed",
                                             "Cluster size; cluster size;",
                                             { 6, -0.5, 5.5 } }
          , m_histogram_cluster_size_vs_tx{ owner,
                                            uniqueName + "ClusterSizeVsTx",
                                            "cluster size vs dx/dz; dx/dz; cluster size",
                                            { 100, -0.2, 0.2 } }
          , m_histogram_cluster_size_vs_ty{ owner,
                                            uniqueName + "ClusterSizeVsTy",
                                            "cluster size vs dy/dz; dy/dz; cluster size",
                                            { 100, -0.2, 0.2 } }
          , m_histogram_cluster_size_vs_t2{ owner,
                                            uniqueName + "ClusterSizeVsT2",
                                            "cluster size vs. sqrt(dx/dz^2 + dy/dz^2); ds/dz; cluster size",
                                            { 100, 0., 0.4 } }
          , m_histogram_cluster_charge_cluster_size{ owner,
                                                     uniqueName + "clusterCharge_clusterSize",
                                                     "Cluster charge and cluster size correlation matrix; cluster "
                                                     "charge; cluster size",
                                                     { 200, 0., 200. },
                                                     { 6, -0.5, 5.5 } }
          , m_histogram_cluster_charge_vs_tx{ owner,
                                              uniqueName + "ClusterChargeVsTx",
                                              "cluster charge vs dx/dz; dx/dz; cluster charge",
                                              { 100, -0.2, 0.2 } }
          , m_histogram_cluster_charge_vs_ty{ owner,
                                              uniqueName + "ClusterChargeVsTy",
                                              "cluster charge vs dy/dz; dy/dz; cluster charge",
                                              { 100, -0.2, 0.2 } }
          , m_histogram_cluster_charge_vs_t2{ owner,
                                              uniqueName + "ClusterChargeVsT2",
                                              "cluster charge vs. sqrt(dx/dz^2 + dy/dz^2); ds/dz; cluster charge",
                                              { 100, 0., 0.4 } }
          , m_histogram_cluster_charge_sensors{
                owner,
                [&uniqueName]( int n ) {
                  return fmt::format( "{}clusterCharge_Sensor{}", uniqueName, (char)( 'A' + n ) );
                },
                []( int n ) { return fmt::format( "clusterCharge_Sensor{};cluster charge;", (char)( 'A' + n ) ); },
                { 200, 0., 200. } }
          , m_histogram_cluster_charge_sensors_strips{
                owner,
                [&uniqueName]( int n ) {
                  return fmt::format( "{}clusterCharge_clusterSize_Sensor{}", uniqueName, (char)( 'A' + n ) );
                },
                []( int n ) {
                  return fmt::format( "clusterCharge_clusterSize_Sensor{};cluster charge;cluster size",
                                      (char)( 'A' + n ) );
                },
                { 200, 0., 200. },
                { 4, 1.5, 5.5 },
            }
          , m_histogram_overlap_residual{ owner,
                                          uniqueName + "overlapResidual",
                                          "Overlap residuals; overlap residuals;",
                                          { 100, -1.0, 1.0 } }
          , m_histogram_overlap_first_residual{ owner,
                                                uniqueName + "overlapResidual_left",
                                                "Overlap residuals (left); overlap residuals (left);",
                                                { 100, -0.5, 0.5 } }
          , m_histogram_overlap_second_residual{ owner,
                                                 uniqueName + "overlapResidual_right",
                                                 "Overlap residuals (right); overlap residuals (right)",
                                                 { 100, -0.5, 0.5 } }
          , m_histogram_stateXY{ owner,
                                 uniqueName + "stateXY",
                                 "(x,y) position of UT states (interaction of track with layer around UT cluster);x;y",
                                 { 100, -875, 875 },
                                 { 100, -800, 800 } }
          , m_histogram_n_used_cluster{
                owner,
                uniqueName + "fracUsedClusters",
                "Frac. of clusters used on tracks (nClusterOnTrack / nClusterTotal); nClusterOnTrack/nClusterTotal;",
                { 100, -0.001, 1.01 } } {}
    };

    /** this is pretty disgusting */
    std::map<unsigned int, HistogramsPerLayer> m_histograms_per_layer;

    StatusCode initialize() override {
      auto sc = Consumer::initialize();
      if ( sc.isFailure() ) return sc;

      for ( unsigned int i = 0; i < 4; ++i ) {
        m_histograms_per_layer.emplace( std::piecewise_construct, std::forward_as_tuple( i ),
                                        std::forward_as_tuple( this, "Layer" + std::to_string( i ) + "/" ) );
      }

      return sc;
    }
  };
} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UTTrackMonitor, "UTTrackMonitor" )

UTTrackMonitor::UTTrackMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { { "TracksInContainer", LHCb::TrackLocation::Default },
                  { "InputHitHandler", "" },
                  { "DeUT", DeUTDetLocation::location() } } ) {}

void UTTrackMonitor::operator()( LHCb::Track::Range const& tracks, LHCb::UTHitClusters const& clusters,
                                 DeUTDetector const& deUT ) const {

  // tmp container for ids
  std::set<unsigned int> usedIDs;

  // histograms per track
  for ( const auto& track : tracks ) {
    if ( !track ) continue; // pointer types...

    if ( track->hasUT() ) {
      // find the UT hits on the track
      const auto&               ids = track->lhcbIDs();
      std::vector<LHCb::LHCbID> utHits;
      utHits.reserve( 4 );
      std::copy_if( ids.begin(), ids.end(), std::back_inserter( utHits ),
                    []( const LHCb::LHCbID& id ) { return id.isUT(); } );

      if ( utHits.size() < m_minNumUTHits ) continue;

      // insert into tmp container
      std::transform( utHits.begin(), utHits.end(), std::inserter( usedIDs, usedIDs.begin() ),
                      []( const LHCb::LHCbID& id ) { return id.utID(); } );

      fillHistograms( *track, clusters, deUT );
    }
  }

  if ( clusters.size() != 0 ) {
    for ( unsigned int layerId = 0; layerId < NUTLAYERS; ++layerId ) {
      auto& histos_this_layer = m_histograms_per_layer.at( layerId );

      const float nOnTrack = std::count_if( usedIDs.begin(), usedIDs.end(), [&]( const auto& lhcbId ) {
        return LHCbID( lhcbId ).utID().layer() == layerId;
      } );

      const float nTotalOnLayer = std::count_if(
          clusters.begin(), clusters.end(), [&]( const auto& uthit ) { return uthit.channelID().layer() == layerId; } );

      if ( nTotalOnLayer > 0 ) ++histos_this_layer.m_histogram_n_used_cluster[nOnTrack / nTotalOnLayer];
    }
  }
}

void UTTrackMonitor::fillHistograms( LHCb::Track const& track, LHCb::UTHitClusters const& utHits,
                                     DeUTDetector const& deUT ) const {

  // Loop over the nodes to get the hits variables
  std::vector<const LHCb::FitNode*> nodesByUTLayer[4];

  for ( const auto& node : nodes( track ) ) {
    // Only loop on hits with measurement that is UT type
    if ( !node->hasMeasurement() ) continue;

    // monitor overlapping tracks
    const LHCb::Measurement&     measurement = node->measurement();
    const LHCb::Measurement::UT* hit         = node->measurement().getIf<LHCb::Measurement::UT>();
    if ( !hit ) continue;
    LHCb::LHCbID lhcbID = measurement.lhcbID();
    assert( lhcbID.isUT() );

    // unbiased residuals and biased residuals
    Detector::UT::ChannelID chan        = lhcbID.utID();
    unsigned int            uniquelayer = chan.layer();

    auto sector = deUT.findSector( LHCb::Detector::UT::ChannelID( chan ) );
    // track parameters at some reference z
    // state
    const auto& aState = node->state();
    ++m_histogram_trackTx[aState.tx()];
    ++m_histogram_trackTy[aState.ty()];
    ++m_histogram_trackP[track.p()];

    auto& histos_this_layer = m_histograms_per_layer.at( uniquelayer );

    ++histos_this_layer.m_histogram_stateXY[{ aState.x(), aState.y() }];
    ++histos_this_layer.m_histogram_stateX[aState.x()];
    ++histos_this_layer.m_histogram_stateY[aState.y()];

    nodesByUTLayer[uniquelayer].push_back( node );

    ++histos_this_layer.m_histogram_unbiasedResidual[node->unbiasedResidual()];
    ++histos_this_layer.m_histogram_biasedResidual[node->residual()];

    /*
    // 2D plots
      const std::string layerName = UTNames().UniqueLayerToString( chan );
      const unsigned int bin = histoBin( chan );
      plot2D( bin, node->unbiasedResidual(), "unbiasedResSector" + layerName, "unbiasedResSector" + layerName, 99.5,
              500.5, -2., 2., 401, 200 );
      plot2D( bin, node->residual(), "biasedResSector" + layerName, "/biasedResSector" + layerName, 99.5, 500.5, -2.,
              2., 401, 200 );
    */

    const auto& cluster = findCluster( chan, utHits );
    if ( !cluster ) { error() << "Could not find cluster back in hit handler!" << endmsg; }
    // get the measurement and plot UT related quantities
    ++histos_this_layer.m_histogram_frac_strip_cluster_size[{ cluster->fracStrip(),
                                                              (double)( std::min( (int)( cluster->size() ), 5 ) ) }];
    ++histos_this_layer.m_histogram_cluster_size[(double)( std::min( (int)( cluster->size() ), 49 ) )];
    ++histos_this_layer.m_histogram_cluster_size_zoomed[(double)( std::min( (int)( cluster->size() ), 5 ) )];
    ++histos_this_layer.m_histogram_cluster_charge_cluster_size[{
        cluster->clusterCharge(), (double)( std::min( (int)( cluster->size() ), 5 ) ) }];

    // for this one we actually need the component perpendicular to B field.
    Gaudi::XYZVector dir = node->state().slopes();
    histos_this_layer.m_histogram_cluster_size_vs_tx[{ dir.x() }] += (float)( std::min( (int)( cluster->size() ), 5 ) );
    histos_this_layer.m_histogram_cluster_size_vs_ty[{ dir.y() }] += (float)( std::min( (int)( cluster->size() ), 5 ) );
    histos_this_layer.m_histogram_cluster_size_vs_t2[{ std::sqrt( dir.x() * dir.x() + dir.y() * dir.y() ) }] +=
        (float)( std::min( (int)( cluster->size() ), 5 ) );
    histos_this_layer.m_histogram_cluster_charge_vs_tx[{ dir.x() }] += cluster->clusterCharge();
    histos_this_layer.m_histogram_cluster_charge_vs_ty[{ dir.y() }] += cluster->clusterCharge();
    histos_this_layer.m_histogram_cluster_charge_vs_t2[{ std::sqrt( dir.x() * dir.x() + dir.y() * dir.y() ) }] +=
        cluster->clusterCharge();

#ifdef USE_DD4HEP
    unsigned int sensor_type = sensor_types.find( sector.sensor().sensorType() );
#else
    unsigned int sensor_type = sensor_types.find( sector->sensor( 0 ).sensorType() );
#endif
    ++( histos_this_layer.m_histogram_cluster_charge_sensors[sensor_type] )[cluster->clusterCharge()];
    ++( histos_this_layer.m_histogram_cluster_charge_sensors_strips[sensor_type] )[{
        cluster->clusterCharge(), (double)( std::min( (int)( cluster->size() ), 5 ) ) }];

  } // nodes

  // make overlap histograms
  for ( size_t ilay = 0; ilay < 4; ++ilay ) {
    auto& histos_this_layer = m_histograms_per_layer.at( ilay );
    if ( nodesByUTLayer[ilay].size() == 2 ) {
      const LHCb::FitNode* firstnode  = nodesByUTLayer[ilay].front();
      const LHCb::FitNode* secondnode = nodesByUTLayer[ilay].back();

      if ( firstnode->measurement().isSameDetectorElement( secondnode->measurement() ) ) {

        if ( firstnode->measurement().toGlobal( Gaudi::XYZPoint() ).x() >
             secondnode->measurement().toGlobal( Gaudi::XYZPoint() ).x() )
          std::swap( firstnode, secondnode );

        auto   firstTrajectory  = firstnode->measurement().getIf<LHCb::Measurement::UT>()->trajectory;
        auto   secondTrajectory = secondnode->measurement().getIf<LHCb::Measurement::UT>()->trajectory;
        int    firstsign        = firstTrajectory.direction( 0 ).y() > 0 ? 1 : -1;
        int    secondsign       = secondTrajectory.direction( 0 ).y() > 0 ? 1 : -1;
        double firstresidual    = firstsign * firstnode->residual();
        double secondresidual   = secondsign * secondnode->residual();

        double diff = firstresidual - secondresidual;

        // let's still make the correction for the track angle, such that we really look in the wafer plane.
        Gaudi::XYZVector localdir = firstnode->measurement().toLocal( firstnode->state().slopes() );
        double           localTx  = localdir.x() / localdir.z();
        diff *= std::sqrt( 1 + localTx * localTx );

        ++histos_this_layer.m_histogram_overlap_residual[diff];
        ++histos_this_layer.m_histogram_overlap_first_residual[firstresidual * std::sqrt( firstnode->errMeasure2() /
                                                                                          firstnode->errResidual2() )];
        ++histos_this_layer
              .m_histogram_overlap_second_residual[secondresidual *
                                                   std::sqrt( secondnode->errMeasure2() / secondnode->errResidual2() )];
        /*
        // this needs to be fixed: can we somehow can a consecutive ladder ID?
        size_t sectorID = utUniqueSectorID( firstnode->measurement().lhcbID().utID() );
        profile1D( double( sectorID ), diff, prefix + "Overlap residual versus sector ID",
                   "Average overlap residual versus sector ID", -0.5, 47.5, 48 );

        if ( fullDetail() ) {
          std::string firstname  = firstnode->measurement().name().substr( 55, 10 );
          std::string secondname = secondnode->measurement().name().substr( 55, 10 );
          plot( diff, prefix + "Overlap residual for " + firstname + " and " + secondname,
                "Overlap residual for " + firstname + " and " + secondname, -1.0, 1.0, 50 );
        }
        */
      }
    }
  }
}
