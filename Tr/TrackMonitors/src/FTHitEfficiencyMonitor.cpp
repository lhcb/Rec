/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Consumer.h"
#include "LHCbAlgs/Traits.h"

#include "DetDesc/DetectorElement.h"
#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/PrHits.h"
#include "FTDet/DeFTDetector.h"
#include "FTDet/DeFTMat.h"
#include "GaudiKernel/SystemOfUnits.h"

#include "Event/PrHits.h"
#include "Event/Track.h"

#include "Gaudi/Accumulators/Histogram.h"

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackInterpolator.h"

#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"

/**
 * Algorithm based on the track-based efficiencies, in particular the
 * OTHitEfficiencyMonitor
 * conceptualised by Wouter Hulsbergen, modified by Francesco Dettori,
 * and others in the next generations.
 *
 * Written a SciFi-version by Laurent Dufour, Lukas Witola, Zehua Xu.
 *
 * To use this, please take proper care of the hit unbiasing beforehand.
 * This is most adequately done by unbiasing the pattern recognition.
 * In addition, due to the complex 3D histograms, it is not intended
 * to be ran online as-is.
 *
 * A lightweight version, in which only the 1D histograms are
 * created, can be used.
 *
 **/
class FTHitEfficiencyMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, const LHCb::Pr::FT::Hits&,
                                             const DetectorElement&, const DeFT& ),
                                       LHCb::Algorithm::Traits::usesConditions<DetectorElement, DeFT>> {
private:
  Gaudi::Property<unsigned int> m_layerUnderStudy{ this, "LayerUnderStudy" };

  Gaudi::Property<bool> m_expertMode{ this, "ExpertMode", false };

  Gaudi::Property<float> m_maxDoca{ this, "MaxDoca", 2.5 * Gaudi::Units::mm };
  Gaudi::Property<float> m_maxTrackCov{ this, "MaxTrackCov", 0.5 * Gaudi::Units::mm };
  Gaudi::Property<float> m_trackMaxChi2PerDoF{ this, "MaxTrackChi2PerDof", 3. };
  Gaudi::Property<float> m_trackMinP{ this, "MinTrackP", 2000 * Gaudi::Units::MeV };
  Gaudi::Property<float> m_trackMinPt{ this, "MinTrackPT", 400 * Gaudi::Units::MeV };

  ToolHandle<ITrackInterpolator> m_interpolator{ this, "Interpolator", "TrackInterpolator" };
  ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "Extrapolator", "TrackLinearExtrapolator" };

  mutable Gaudi::Accumulators::ProfileHistogram<2> m_efficiencyVsXY{
      this,
      "EfficiencyVsXY",
      "Efficiency vs XY;X [mm];Y [mm]",
      { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm },
      { 124, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsX{
      this,
      "EfficiencyVsX",
      "Efficiency vs X;X [mm];Efficiency",
      { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsModule{
      this,
      "EfficiencyVsModule",
      "Efficiency vs Module;GlobalModuleIdx;Efficiency",
      { FTConstants::nModulesTotal, 0, FTConstants::nModulesTotal } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsMat{
      this,
      "EfficiencyVsMat",
      "Efficiency vs Mat;GlobalMatIdx;Efficiency",
      { FTConstants::nMatsTotal, 0, FTConstants::nMatsTotal } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsSipm{
      this,
      "EfficiencyVsSipm",
      "Efficiency vs Sipm;GlobalSipmIdx;Efficiency",
      { FTConstants::nSiPMsTotal, 0, FTConstants::nSiPMsTotal } };

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_efficiencyVsLocalChannel{
      this,
      "EfficiencyVsLocalChannel",
      "Efficiency vs Channel;LocalChannelIdx;Efficiency",
      { FTConstants::nChannels, 0, FTConstants::nChannels } };

  mutable Gaudi::Accumulators::Histogram<1> m_doca{
      this, "Doca", "DOCA;DOCA [mm];Entries", { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<1> m_docaX{
      this, "DocaX", "DOCA X;DOCA X [mm];Entries", { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<1> m_docaY{
      this, "DocaY", "DOCA Y;DOCA Y [mm];Entries", { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<1> m_docaZ{
      this, "DocaZ", "DOCA Z;DOCA Z [mm];Entries", { 100, -4.0 * Gaudi::Units::mm, 4.0 * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<1> m_matchedHitsPerTrack{
      this, "MatchedHitsPerTrack", "Matched hits per Track;Hits;Entries", { 10, -0.5, 9.5 } };

  mutable Gaudi::Accumulators::ProfileHistogram<2> m_matchedHitsPerTrackVsXY{
      this,
      "MatchedHitsPerTrackVsXY",
      "Matched hits per Track vs XY;X [mm];Y [mm];Hits",
      { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm },
      { 124, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<2> m_allHitsVsXY{ this,
                                                           "AllHitsVsXY",
                                                           "All Hits vs XY;X [mm];Y [mm];Entries",
                                                           { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm },
                                                           { 2, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm } };

  mutable Gaudi::Accumulators::Histogram<2> m_matchedHitsVsXY{
      this,
      "MatchedHitsVsXY",
      "Matched Hit vs XY;X [mm];Y [mm];Entries",
      { 660, -3200. * Gaudi::Units::mm, 3200. * Gaudi::Units::mm },
      { 2, -2500. * Gaudi::Units::mm, 2500. * Gaudi::Units::mm } };

  struct QuarterHistogram {
    QuarterHistogram( FTHitEfficiencyMonitor* parent, const std::string& prefix )
        : efficiencyVsModule{ parent,
                              prefix + "-EfficiencyVsModule",
                              prefix + " Efficiency vs Module;ModuleIdx;Efficiency",
                              { FTConstants::nModulesMax, 0, FTConstants::nModulesMax } }
        , efficiencyVsMat{ parent,
                           prefix + "-EfficiencyVsMat",
                           prefix + " Efficiency vs Mat;MatIdx;Efficiency",
                           { FTConstants::nMaxMatsPerQuarter, 0, FTConstants::nMaxMatsPerQuarter } }
        , efficiencyVsSipm{ parent,
                            prefix + "-EfficiencyVsSipm",
                            prefix + " Efficiency vs Sipm;SipmIdx;Efficiency",
                            { FTConstants::nMaxSiPMsPerQuarter, 0, FTConstants::nMaxSiPMsPerQuarter } }
        , efficiencyVsChannel{ parent,
                               prefix + "-EfficiencyVsChannel",
                               prefix + " Efficiency vs Channel;ChannelIdx;Efficiency",
                               { FTConstants::nMaxChannelsPerQuarter, 0, FTConstants::nMaxChannelsPerQuarter } } {}

    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsModule;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsMat;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsSipm;
    mutable Gaudi::Accumulators::ProfileHistogram<1> efficiencyVsChannel;
  };

  std::map<LHCb::Detector::FTChannelID::QuarterID, QuarterHistogram> m_quarterHistograms;

public:
  FTHitEfficiencyMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  { KeyValue{ "TrackLocation", "" }, KeyValue{ "PrFTHitsLocation", "" },
                    KeyValue{ "LHCbGeoLocation", LHCb::standard_geometry_top },
                    KeyValue{ "FTDetectorLocation", DeFTDetectorLocation::Default } } ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      if ( m_expertMode.value() ) {
        for ( const auto quarterID : LHCb::Detector::FTChannelID::allQuarterIDs ) {
          m_quarterHistograms.emplace( std::piecewise_construct, std::forward_as_tuple( quarterID ),
                                       std::forward_as_tuple( this, to_string( quarterID ) ) );
        }
      }
    } );
  }

  void operator()( const LHCb::Track::Range& tracks, const LHCb::Pr::FT::Hits& hits, const DetectorElement& lhcb,
                   const DeFT& ftDet ) const override {
    // Get the station & layer under study
    const auto& ftStation = ftDet.getStation( m_layerUnderStudy / LHCb::Detector::FT::nLayers );
    if ( !ftStation ) {
      error() << "Failed to find SciFi station " << m_layerUnderStudy / LHCb::Detector::FT::nLayers << endmsg;
      return;
    }

    const auto& ftLayer = ftStation->getLayer( m_layerUnderStudy % LHCb::Detector::FT::nLayers );
    if ( !ftLayer ) {
      error() << "Failed to find SciFi layer " << m_layerUnderStudy % LHCb::Detector::FT::nLayers << endmsg;
      return;
    }

    for ( const auto& track : tracks ) {
      // Chisquare and momentum cuts
      if ( m_trackMaxChi2PerDoF.value() > 0 && track->chi2PerDoF() > m_trackMaxChi2PerDoF.value() ) continue;
      if ( m_trackMinP.value() > 0 && track->p() < m_trackMinP.value() ) continue;
      if ( m_trackMinPt.value() > 0 && track->pt() < m_trackMinPt.value() ) continue;

      // Interpolate the track to the z-position of the layer under study
      LHCb::State state;
      if ( !m_interpolator->interpolate( *track, ftLayer->globalZ(), state, lhcb ).isSuccess() ) {
        debug() << "Failed to interpolate track" << endmsg;
        continue;
      }

      // Only consider if state has reasonably small error
      if ( std::sqrt( state.covariance()( 0, 0 ) ) > m_maxTrackCov.value() ) {
        debug() << "Covariance too large" << endmsg;
        continue;
      }

      // To get a slightly more precise answer, intersect with the plane of the layer.
      // Use a simple linear extrapolation here
      if ( !m_extrapolator->propagate( state, ftLayer->plane(), lhcb ).isSuccess() ) {
        debug() << "Failed to extrapolate track" << endmsg;
        continue;
      }

      // Find the mat that the track intersects with
      const auto& ftMat = ftLayer->findMatWithXY( state.position() );
      if ( !ftMat ) {
        debug() << "Failed to find a corresponding SciFi mat" << endmsg;
        continue;
      }
      // Extrapolate the state to the mat
      if ( !m_extrapolator->propagate( state, ftMat->plane(), lhcb ).isSuccess() ) {
        debug() << "Failed to extrapolate track" << endmsg;
        continue;
      }

      // This is a float to help with the Profile-histogram call...
      // could well be a int/bool
      float matchedHit   = 0;
      int   nMatchedHits = 0;

      // Get the trajectory of the track. This is used to calculate the DOCA
      auto trackLine = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>( state.position(), state.slopes() );

      // What we really want to do here is to find all SciFi ChannelIDs that are within our search window.
      // For now we restrict ourselves to just one fibre mat. This might bias the edges of the modules and mats
      for ( unsigned int index = 0; index < hits.size(); index++ ) {
        auto [y0, y1] = hits.yEnd( index );

        auto yHit = 0.5 * ( y0 + y1 );
        ++m_allHitsVsXY[{ hits.x( index ), yHit }];
        //---LoH: This has to be the ID and not the Idx because some hits are empty (station=0)
        if ( hits.id( index ).globalMatID() != ftMat->elementID().globalMatID() ) continue;

        // Get the trajectory of the SciFi hit
        auto hitLine = Gaudi::Math::Line<Gaudi::XYZPoint, Gaudi::XYZVector>(
            Gaudi::XYZPoint( hits.x( index, y0 ), y0, hits.z( index, y0 ) ),
            Gaudi::XYZPoint( hits.x( index, y1 ), y1, hits.z( index, y1 ) ) );

        // Calculate the distance of closest approach (DOCA) between the track
        // and the line spanned by the hit in the layer under study
        Gaudi::XYZPoint trackPoint;
        Gaudi::XYZPoint hitPoint;
        if ( !Gaudi::Math::closestPoints( trackLine, hitLine, trackPoint, hitPoint ) ) {
          warning() << "Track and hit are parallel" << endmsg;
          continue;
        }

        auto docaVector = ( trackPoint - hitPoint );
        auto signedDoca = ( docaVector.x() / std::abs( docaVector.x() ) ) * docaVector.R();

        if ( std::abs( signedDoca ) <= m_maxDoca ) {
          matchedHit = 1;
          nMatchedHits++;
          ++m_matchedHitsVsXY[{ hits.x( index ), yHit }];
        }

        // DOCA histograms
        ++m_doca[signedDoca];
        ++m_docaX[docaVector.x()];
        ++m_docaY[docaVector.y()];
        ++m_docaZ[docaVector.z()];
      }
      // Matched hits
      ++m_matchedHitsPerTrack[nMatchedHits];
      m_matchedHitsPerTrackVsXY[{ state.x(), state.y() }] += nMatchedHits;

      // Efficiency histograms
      m_efficiencyVsXY[{ state.x(), state.y() }] += matchedHit;
      m_efficiencyVsX[state.x()] += matchedHit;

      if ( m_expertMode.value() ) {
        auto [channelID, fraction] = ftMat->calculateChannelAndFrac( ftMat->toLocal( state.position() ).x() );

        m_efficiencyVsModule[channelID.globalModuleIdx()] += matchedHit;
        m_efficiencyVsMat[channelID.globalMatIdx()] += matchedHit;
        m_efficiencyVsSipm[channelID.globalSipmIdx()] += matchedHit;
        m_efficiencyVsLocalChannel[channelID.localChannelIdx()] += matchedHit;
        auto& quarterHistogram = m_quarterHistograms.at( channelID.quarter() );
        quarterHistogram.efficiencyVsModule[channelID.localModuleIdx()] += matchedHit;
        quarterHistogram.efficiencyVsMat[channelID.localMatIdx_quarter()] += matchedHit;
        quarterHistogram.efficiencyVsSipm[channelID.localSiPMIdx_quarter()] += matchedHit;
        quarterHistogram.efficiencyVsChannel[channelID.localChannelIdx_quarter()] += matchedHit;
      }
    }
  }
};

DECLARE_COMPONENT( FTHitEfficiencyMonitor )
