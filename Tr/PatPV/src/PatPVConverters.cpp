/*****************************************************************************\
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PrVeloTracks.h"
#include "Event/RecVertex.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackPVFinderUtils.h"

// boost includes
#include <boost/container/static_vector.hpp>

// std includes
#include <vector>

using namespace LHCb::Event::PV;
using namespace LHCb::TrackPVFinder;

namespace LHCb::Event::v2 {
  using Tracks = std::vector<Track>;
}

class PVToRecConverterV1 : public LHCb::Algorithm::Transformer<LHCb::RecVertices(
                               const LHCb::Event::PV::PrimaryVertexContainer&, const LHCb::Track::Range& )> {
public:
  PVToRecConverterV1( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "InputVertices", LHCb::Event::PV::DefaultLocation }, KeyValue{ "InputTracks", "" } },
                     KeyValue{ "OutputVertices", LHCb::RecVertexLocation::Primary } ) {}
  LHCb::RecVertices operator()( const LHCb::Event::PV::PrimaryVertexContainer& vertices,
                                const LHCb::Track::Range&                      keyed_tracks ) const override {
    // create the output container
    LHCb::RecVertices recvertexcontainer;
    recvertexcontainer.reserve( vertices.size() );
    const auto pvtracks = vertices.tracks.scalar();
    for ( const auto& vertex : vertices ) {
      auto recvertex = std::make_unique<LHCb::RecVertex>( vertex.key() );
      recvertex->setPosition( vertex.position() );
      recvertex->setChi2( vertex.chi2() );
      recvertex->setNDoF( vertex.nDoF() );
      recvertex->setCovMatrix( vertex.covMatrix() );
      recvertex->setTechnique( LHCb::RecVertex::RecVertexType::Primary );
      // The following relies on the Velo tracks being in the right order ([forward,backward])
      // (Note that when the input comes from Allen vertices.tracks.prvelocontainers is not initialized)
      const auto fwdsize = vertices.tracks.prvelocontainers[0] ? vertices.tracks.prvelocontainers[0]->size() : 0;
      for ( int trkindex = vertex.begin(); trkindex != vertex.end(); ++trkindex ) {
        const auto pvtrack = pvtracks[trkindex];
        const auto weight  = pvtrack.weight().cast();
        if ( weight > 0 ) {
          auto [containerIdx, veloIdx] = pvtrack.field<PVTrackTag::veloindex>().index();
          const int containerindex     = containerIdx.cast();
          const int velotrackindex     = veloIdx.cast() + ( containerindex == 0 ? 0 : fwdsize );
          if ( velotrackindex >= 0 && velotrackindex < int( keyed_tracks.size() ) )
            recvertex->addToTracks( keyed_tracks[velotrackindex], weight );
        }
      }
      recvertexcontainer.add( recvertex.release() );
    }
    return recvertexcontainer;
  }
};

struct PVToRecConverterV1WithoutTracks
    : public LHCb::Algorithm::Transformer<LHCb::RecVertices( const LHCb::Event::PV::PrimaryVertexContainer& )> {
  Gaudi::Property<bool> m_addTrackWeights{ this, "AddTrackWeights", true };
  PVToRecConverterV1WithoutTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, { KeyValue{ "InputVertices", LHCb::Event::PV::DefaultLocation } },
                     KeyValue{ "OutputVertices", LHCb::RecVertexLocation::Primary } ) {}
  LHCb::RecVertices operator()( const LHCb::Event::PV::PrimaryVertexContainer& vertices ) const override {
    // create the output container
    LHCb::RecVertices recvertexcontainer;
    recvertexcontainer.reserve( vertices.size() );
    const auto pvtracks = vertices.tracks.scalar();
    for ( const auto& vertex : vertices ) {
      auto recvertex = std::make_unique<LHCb::RecVertex>( vertex.key() );
      recvertex->setPosition( vertex.position() );
      recvertex->setChi2( vertex.chi2() );
      recvertex->setNDoF( vertex.nDoF() );
      recvertex->setCovMatrix( vertex.covMatrix() );
      recvertex->setTechnique( LHCb::RecVertex::RecVertexType::Primary );
      // Add the weights with empty track pointers. Like this it is still useful for monitoring.
      if ( m_addTrackWeights && pvtracks.size() > 0 ) {
        for ( int trkindex = vertex.begin(); trkindex != vertex.end(); ++trkindex ) {
          const auto pvtrack = pvtracks[trkindex];
          if ( pvtrack.field<PVTrackTag::pvindex>().get() >= 0 ) {
            const auto weight = pvtrack.weight().cast();
            if ( weight > 0 ) recvertex->addToTracks( nullptr, weight );
          }
        }
      }
      recvertexcontainer.add( recvertex.release() );
    }
    return recvertexcontainer;
  }
};

struct PVToLightPVConverter : public LHCb::Algorithm::Transformer<LHCb::PrimaryVertex::Container(
                                  const LHCb::Event::PV::PrimaryVertexContainer& )> {
  PVToLightPVConverter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, { KeyValue{ "InputVertices", LHCb::Event::PV::DefaultLocation } },
                     KeyValue{ "OutputVertices", "" } ) {}
  LHCb::PrimaryVertex::Container operator()( const LHCb::Event::PV::PrimaryVertexContainer& vertices ) const override {
    // create the output container
    LHCb::PrimaryVertex::Container pvcontainer;
    pvcontainer.reserve( vertices.size() );
    for ( const auto& vertex : vertices ) {
      auto pv = std::make_unique<LHCb::PrimaryVertex>( vertex.key() );
      *pv     = vertex;
      pvcontainer.add( pv.release() );
    }
    return pvcontainer;
  }
};

class PVToRecConverterV2
    : public LHCb::Algorithm::Transformer<LHCb::Event::v2::RecVertices(
          const EventContext&, const LHCb::Event::PV::PrimaryVertexContainer&, const LHCb::Event::v2::Tracks& )> {
public:
  PVToRecConverterV2( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "InputVertices", LHCb::Event::PV::DefaultLocation }, KeyValue{ "InputTracks", "" } },
                     KeyValue{ "OutputVertices", LHCb::Event::v2::RecVertexLocation::Primary } ) {}

  LHCb::Event::v2::RecVertices operator()( const EventContext&                            evtCtx,
                                           const LHCb::Event::PV::PrimaryVertexContainer& pvdata,
                                           const LHCb::Event::v2::Tracks&                 v2tracks ) const override {
    auto memResource = LHCb::getMemResource( evtCtx );
    // create the output container and copy the vertex data
    LHCb::Event::v2::RecVertices recvertexcontainer{ memResource };
    recvertexcontainer.reserve( pvdata.vertices.size() );
    const auto pvtracks = pvdata.tracks.scalar();
    const auto fwdsize  = pvdata.tracks.prvelocontainers[0]->size();
    for ( const auto& vertex : pvdata.vertices ) {
      auto& recvertex = recvertexcontainer.emplace_back(
          vertex.position(), vertex.covMatrix(),
          LHCb::Event::v2::Track::Chi2PerDoF{ vertex.chi2() / vertex.nDoF(), vertex.nDoF() } );
      recvertex.setTechnique( LHCb::Event::v2::RecVertex::RecVertexType::Primary );
      recvertex.reserve( vertex.nTracks() );
      for ( int trkindex = vertex.begin(); trkindex != vertex.end(); ++trkindex ) {
        const auto pvtrack = pvtracks[trkindex];
        const auto weight  = pvtrack.weight().cast();
        if ( weight > 0 ) {
          auto [containerIdx, veloIdx] = pvtrack.field<PVTrackTag::veloindex>().index();
          const int containerindex     = containerIdx.cast();
          const int velotrackindex     = veloIdx.cast() + ( containerindex == 0 ? 0 : fwdsize );
          recvertex.addToTracks( &( v2tracks[velotrackindex] ), weight );
        }
      }
    }
    return recvertexcontainer;
  }
};

DECLARE_COMPONENT( PVToRecConverterV1 )
DECLARE_COMPONENT( PVToRecConverterV2 )
DECLARE_COMPONENT( PVToRecConverterV1WithoutTracks )
DECLARE_COMPONENT( PVToLightPVConverter )

class RecV1ToPVConverterWithoutTracks
    : public LHCb::Algorithm::Transformer<LHCb::Event::PV::PrimaryVertexContainer( const LHCb::RecVertex::Range& )> {
public:
  RecV1ToPVConverterWithoutTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, { KeyValue{ "InputVertices", LHCb::RecVertexLocation::Primary } },
                     KeyValue{ "OutputVertices", LHCb::Event::PV::DefaultLocation } ) {}
  LHCb::Event::PV::PrimaryVertexContainer operator()( const LHCb::RecVertex::Range& vertices ) const override {
    // create the output container
    LHCb::Event::PV::PrimaryVertexContainer pvcontainer;
    pvcontainer.vertices.reserve( vertices.size() );
    for ( const auto& vertex : vertices ) {
      auto& newpv = pvcontainer.vertices.emplace_back( vertex->position() );
      newpv.setNDoF( vertex->nDoF() );
      newpv.setChi2( vertex->chi2() );
      newpv.setCovMatrix( vertex->covMatrix() );
    }
    pvcontainer.setIndices();
    return pvcontainer;
  }
};

class RecV1ToPVConverter
    : public LHCb::Algorithm::Transformer<LHCb::Event::PV::PrimaryVertexContainer( const LHCb::RecVertex::Range& )> {
public:
  RecV1ToPVConverter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, { KeyValue{ "InputVertices", LHCb::RecVertexLocation::Primary } },
                     KeyValue{ "OutputVertices", LHCb::Event::PV::DefaultLocation } ) {}
  LHCb::Event::PV::PrimaryVertexContainer operator()( const LHCb::RecVertex::Range& recvertices ) const override {
    // create the output container
    LHCb::Event::PV::PrimaryVertexContainer pvcontainer;
    // fill it
    LHCb::TrackPVFinder::populateFromRecVertices( pvcontainer, recvertices );
    // in debug mode: call the vertex fit
    if ( msgLevel() == MSG::DEBUG ) {
      debug() << "Test: refit and compare result for all vertices" << endmsg;
      for ( auto& pv : pvcontainer.vertices ) {
        const auto pos  = pv.position();
        const auto ndof = pv.nDoF();
        LHCb::Event::PV::fitAdaptive( pv, pvcontainer.tracks, LHCb::Event::PV::AdaptiveFitConfig{} );
        debug() << ndof << " -> " << pv.nDoF() << " : " << pos << " -> " << pv.position() << endmsg;
      }
    }
    return pvcontainer;
  }
};

class RecV1ToLightPVConverter
    : public LHCb::Algorithm::Transformer<LHCb::PrimaryVertex::Container( const LHCb::RecVertex::Range& )> {
public:
  RecV1ToLightPVConverter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, { KeyValue{ "InputVertices", LHCb::RecVertexLocation::Primary } },
                     KeyValue{ "OutputVertices", "" } ) {}
  LHCb::PrimaryVertex::Container operator()( const LHCb::RecVertex::Range& vertices ) const override {
    // create the output container
    LHCb::PrimaryVertex::Container pvcontainer;
    pvcontainer.reserve( vertices.size() );
    for ( const auto& vertex : vertices ) {
      auto pv = std::make_unique<LHCb::PrimaryVertex>( vertex->key() );
      pv->VertexBase::operator=( *vertex );
      pvcontainer.add( pv.release() );
    }
    return pvcontainer;
  }
};

namespace LHCb::Event::PV {
  namespace PVTrackTag {
    struct ip2 : float_field {};
    template <typename T>
    using RecV1ToPVConverter_pvseedingtrack_t =
        SOACollection<T, LHCb::Event::PV::PVTrackTag::pvindex, LHCb::Event::PV::PVTrackTag::veloindex,
                      LHCb::Event::PV::PVTrackTag::ip2>;
  } // namespace PVTrackTag

  struct RecV1ToPVConverter_PVSeedingTracks
      : PVTrackTag::RecV1ToPVConverter_pvseedingtrack_t<RecV1ToPVConverter_PVSeedingTracks> {
    using base_t = typename PVTrackTag::RecV1ToPVConverter_pvseedingtrack_t<RecV1ToPVConverter_PVSeedingTracks>;
    using base_t::base_t;
  };
} // namespace LHCb::Event::PV

namespace {
  struct VertexSeed {
    VertexSeed( const LHCb::RecVertex& v ) : position{ v.position() }, recvtx{ &v } {}
    Gaudi::XYZPoint        position;
    const LHCb::RecVertex* recvtx{ nullptr }; // keep for debugging
    auto                   x() const { return position.x(); }
    auto                   y() const { return position.y(); }
    auto                   z() const { return position.z(); }
  };
} // namespace

class RecV1ToPVConverterWithPrTracks : public LHCb::Algorithm::Transformer<LHCb::Event::PV::PrimaryVertexContainer(
                                           const EventContext&, const LHCb::RecVertex::Range&,
                                           const LHCb::Pr::Velo::Tracks&, const LHCb::Pr::Velo::Tracks& )> {
private:
  Gaudi::Property<uint16_t> m_minNumTracksPerVertex{ this, "MinNumTracksPerVertex", 4 };
  Gaudi::Property<uint16_t> m_maxFitIter{ this, "MaxFitIter", defaultMaxFitIter,
                                          "Maximum number of iterations for vertex fit" };
  Gaudi::Property<float>    m_maxDeltaChi2{ this, "MaxDeltaChi2", defaultMaxDeltaChi2,
                                         "Maximum chi2 contribution of track to vertex fit" };
  Gaudi::Property<float>    m_maxDeltaZConverged{ this, "MaxDeltaZConverged", defaultMaxDeltaZConverged,
                                               "Limit on change in z to determine if vertex fit has converged" };
  Gaudi::Property<float>    m_maxDeltaChi2Converged{ this, "MaxDeltaChi2Converged", defaultMaxDeltaChi2Converged,
                                                  "Limit on change in chi2 to determine if vertex fit has converged" };

public:
  RecV1ToPVConverterWithPrTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     { KeyValue{ "InputVertices", LHCb::RecVertexLocation::Primary },
                       KeyValue{ "TracksBackwardLocation", "Rec/Track/VeloBackward" },
                       KeyValue{ "TracksLocation", "Rec/Track/Velo" } },
                     KeyValue{ "OutputVertices", LHCb::Event::PV::DefaultLocation } ) {}
  LHCb::Event::PV::PrimaryVertexContainer operator()( const EventContext&           evtCtx,
                                                      const LHCb::RecVertex::Range& inputvertices,
                                                      const LHCb::Pr::Velo::Tracks& tracksBackward,
                                                      const LHCb::Pr::Velo::Tracks& tracksForward ) const override {

    // Because anything else has issues as well, we implement the conversion as a seeded search.
    auto memResource = LHCb::getMemResource( evtCtx );

    // allocate the output
    PrimaryVertexContainer pvcontainer{ memResource };
    auto&                  vertices = pvcontainer.vertices;
    auto&                  pvtracks = pvcontainer.tracks;
    pvtracks.prvelocontainers[0]    = &tracksForward;
    pvtracks.prvelocontainers[1]    = &tracksBackward;

    // only do anything if there actually are vertices
    if ( !inputvertices.empty() ) {
      // 1. translate the input vertices into seeds and sort those in z
      boost::container::small_vector<VertexSeed, 16> vertexseeds;
      for ( const auto& ivtx : inputvertices ) vertexseeds.emplace_back( *ivtx );
      std::stable_sort( std::begin( vertexseeds ), std::end( vertexseeds ),
                        []( const auto& lhs, const auto& rhs ) { return lhs.position.z() < rhs.position.z(); } );

      // 2. create a data structure for the input tracks that just contains an to the closest seed
      const size_t                       Nvelo = tracksForward.size() + tracksBackward.size();
      RecV1ToPVConverter_PVSeedingTracks pvseedtracks;
      pvseedtracks.reserve( Nvelo + simd::size ); // reserve one extra for overflow of the padding when filling?
      // make sure to set the padding of the pv index since we need that to be valid for all simd values
      pvseedtracks.simd()[simd::size * ( Nvelo / simd::size )].field<PVTrackTag::pvindex>().set( int_v{ 0 } );
      int icontainer{ 0 };
      for ( const auto& tracks : { &tracksForward, &tracksBackward } ) {
        for ( auto const& track : tracks->simd() ) {
          auto loop_mask = track.loop_mask();
          auto index     = pvseedtracks.size();
          pvseedtracks.resize( index + popcount( loop_mask ) );
          auto pvseedtrack = pvseedtracks.simd()[index];
          pvseedtrack.field<PVTrackTag::veloindex>().set( icontainer, track.indices() );
          pvseedtrack.field<PVTrackTag::pvindex>().set( int_v{ 0 } );
          pvseedtrack.field<PVTrackTag::ip2>().set( std::numeric_limits<float_v>::max() );
          // loop over all PVs. assign it if ip2 is smaller.
          for ( int iseed = 0; iseed < int( vertexseeds.size() ); ++iseed ) {
            // compute ip2
            const auto& seedpos     = vertexseeds[iseed];
            auto        pos         = track.StatePos( LHCb::Event::Enum::State::Location::ClosestToBeam );
            auto        dir         = track.StateDir( LHCb::Event::Enum::State::Location::ClosestToBeam );
            auto const  tx          = dir.x();
            auto const  ty          = dir.y();
            const auto  dz          = seedpos.z() - pos.z();
            const auto  dx          = pos.x() + dz * tx - seedpos.x();
            const auto  dy          = pos.y() + dz * ty - seedpos.y();
            const auto  ip2         = dx * dx + dy * dy;
            const auto  ip2best     = pvseedtrack.field<PVTrackTag::ip2>().get();
            const auto  pvindexbest = pvseedtrack.field<PVTrackTag::pvindex>().get();
            const auto  closer      = ip2 < ip2best;
            pvseedtrack.field<PVTrackTag::ip2>().set( select( closer, ip2, ip2best ) );
            pvseedtrack.field<PVTrackTag::pvindex>().set( select( closer, iseed, pvindexbest ) );
          }
        }
        ++icontainer;
      }

      // This initializes the data-structures and fits the vertices
      const AdaptiveFitConfig fitconfig{ m_maxDeltaChi2, m_maxDeltaZConverged, m_maxDeltaChi2Converged, m_maxFitIter };
      initializeFromSeeds( pvcontainer, vertexseeds, pvseedtracks, fitconfig );

      // remove vertices with too few tracks, just in case
      applyPVSelection( pvcontainer, fitconfig,
                        [this]( const auto& vertex ) { return vertex.nTracks() >= this->m_minNumTracksPerVertex; } );

      // Debugging: check that output vertices have approximately the same number of tracks as the input.
      if ( msgLevel( MSG::VERBOSE ) ) {
        int ivertex = 0;
        for ( const auto& pv1 : vertexseeds ) {
          const auto& pv2 = vertices[ivertex];
          verbose() << ivertex << " " << pv1.position << " " << pv2.position() << " " << pv1.recvtx->tracks().size()
                    << " " << pv2.nTracks() << " " << pv2.size() << endmsg;
          ++ivertex;
        }
      }
      // Set the keys of the PVs. Unfortunately, as long as PrimaryVertex
      // derives from KeyedObject, we can only set the key once, so we
      // really need to do this at the end. Perhaps we should get rid of
      // keys altogether.
      pvcontainer.setIndices();
      // make sure to set up some navigation for the unbiasing
      pvcontainer.updateVeloIDMap();
    }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "h #output=" << vertices.size() << endmsg;
    return pvcontainer;
  }
};

DECLARE_COMPONENT( RecV1ToPVConverter )
DECLARE_COMPONENT( RecV1ToPVConverterWithoutTracks )
DECLARE_COMPONENT( RecV1ToPVConverterWithPrTracks )
DECLARE_COMPONENT( RecV1ToLightPVConverter )
