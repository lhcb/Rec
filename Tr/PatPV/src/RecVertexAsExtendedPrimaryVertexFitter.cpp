/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PrimaryVertices.h"
#include "Event/RecVertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/IPVFitter.h"
#include "TrackPVFinderUtils.h"

using namespace LHCb::Event::PV;

class RecVertexAsExtendedPrimaryVertexFitter : public extends<GaudiTool, IPVFitter> {

private:
  Gaudi::Property<int>   m_maxFitIter{ this, "Iterations", AdaptiveFitConfig::defaultMaxFitIter };
  Gaudi::Property<float> m_maxDeltaChi2{ this, "trackMaxChi2", AdaptiveFitConfig::defaultMaxDeltaChi2 };
  Gaudi::Property<float> m_maxDeltaZConverged{ this, "maxDeltaZ", AdaptiveFitConfig::defaultMaxDeltaZConverged };
  Gaudi::Property<float> m_maxDeltaChi2Converged{ this, "MaxDeltaChi2Converged",
                                                  AdaptiveFitConfig::defaultMaxDeltaChi2Converged };
  Gaudi::Property<float> m_numAnnealingSteps{ this, "NumAnnealingSteps", AdaptiveFitConfig::defaultNumAnnealingSteps };
  Gaudi::Property<float> m_minTrackWeight{
      this, "minTrackWeight", 0,
      "Minimum Tukey's weight to accept a track. Set to negative value if you want to include all tracks." };

public:
  // Standard constructor
  using extends::extends;
  // Fitting
  StatusCode fitVertex( const Gaudi::XYZPoint& seedPoint, LHCb::span<const LHCb::Track* const> tracks,
                        LHCb::RecVertex& recvertex, std::vector<const LHCb::Track*>& tracks2remove,
                        IGeometryInfo const& ) const override {

    // convert the RecVertex to en extendedprimaryvertex

    // copy the relevant track info into a SOA PVTracks object.
    auto       pvtracks       = LHCb::Event::PV::PVTracks{};
    const auto pvtracksscalar = pvtracks.scalar();
    pvtracks.resize( LHCb::TrackPVFinder::paddedsize( tracks.size() ) );
    unsigned itrk = 0;
    for ( const auto& trk : tracks ) {
      const auto& state   = trk->firstState();
      auto        pvtrack = pvtracksscalar[itrk++];
      pvtrack.field<PVTrackTag::z>().set( state.z() );
      pvtrack.field<PVTrackTag::x>().set( state.x() );
      pvtrack.field<PVTrackTag::y>().set( state.y() );
      pvtrack.field<PVTrackTag::tx>().set( state.tx() );
      pvtrack.field<PVTrackTag::ty>().set( state.ty() );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ).set( state.covariance()( 0, 0 ) );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ).set( state.covariance()( 0, 2 ) );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ).set( state.covariance()( 2, 2 ) );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ).set( state.covariance()( 1, 1 ) );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ).set( state.covariance()( 1, 3 ) );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ).set( state.covariance()( 3, 3 ) );
      pvtrack.field<PVTrackTag::weight>().set( 1 );
      pvtrack.field<PVTrackTag::pvindex>().set( 0 );
      // We do not need the veloid for the refit
      // pvtrack.field<PVTrackTag::veloid>().set( uniqueVeloSegmentID( trk->lhcbIDs() ) );
    }
    // make sure to initialize the padding
    while ( itrk < pvtracks.size() ) {
      auto pvtrack = pvtracksscalar[itrk++];
      pvtrack.field<PVTrackTag::z>().set( 0 );
      pvtrack.field<PVTrackTag::x>().set( 0 );
      pvtrack.field<PVTrackTag::y>().set( 0 );
      pvtrack.field<PVTrackTag::tx>().set( 0.1 );
      pvtrack.field<PVTrackTag::ty>().set( 0.1 );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ).set( 1 );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ).set( 0 );
      pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ).set( 1 );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ).set( 1 );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ).set( 0 );
      pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ).set( 1 );
      pvtrack.field<PVTrackTag::weight>().set( 0 );
      pvtrack.field<PVTrackTag::pvindex>().set( 0 );
    }
    // create the extendedpv
    LHCb::Event::PV::ExtendedPrimaryVertex extendedpv;
    extendedpv.setPosition( seedPoint );
    extendedpv.setRange( size_t( 0 ), tracks.size() );
    // create the fitconfig object
    AdaptiveFitConfig fitconfig;
    fitconfig.maxDeltaChi2          = m_maxDeltaChi2;
    fitconfig.maxDeltaZConverged    = m_maxDeltaZConverged;
    fitconfig.maxDeltaChi2Converged = m_maxDeltaChi2Converged;
    fitconfig.maxFitIter            = m_maxFitIter;
    fitconfig.numAnnealingSteps     = m_numAnnealingSteps;
    // call the fit
    const auto status = LHCb::Event::PV::fitAdaptive( extendedpv, pvtracks, fitconfig );
    // fill the RecVertex:
    recvertex.setPosition( extendedpv.position() );
    recvertex.setChi2( extendedpv.chi2() );
    recvertex.setNDoF( extendedpv.nDoF() );
    recvertex.setCovMatrix( extendedpv.covMatrix() );
    recvertex.setTechnique( LHCb::RecVertex::RecVertexType::Primary );
    // add the tracks
    recvertex.clearTracks();
    tracks2remove.reserve( tracks2remove.size() + pvtracks.size() );
    itrk = 0;
    for ( const auto& trk : tracks ) {
      // get the weight:
      const auto weight = pvtracksscalar[itrk++].weight().cast();
      if ( weight > m_minTrackWeight ) {
        recvertex.addToTracks( trk, weight ); // use > on purpose.
        tracks2remove.emplace_back( trk );
      }
    }
    return status == PVFitStatus::Converged ? StatusCode::SUCCESS : StatusCode::FAILURE;
  }
};

DECLARE_COMPONENT( RecVertexAsExtendedPrimaryVertexFitter )
