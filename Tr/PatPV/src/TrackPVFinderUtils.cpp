/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TrackPVFinderUtils.h"

namespace LHCb::TrackPVFinder {

  void populateFromVeloTracks( PrimaryVertexContainer& pvdata ) {
    // we cannot vectorise this, but in the previous version we also had un unvectorised 'permute' loop.
    auto& pvtracks        = pvdata.tracks;
    auto  pvtracks_scalar = pvtracks.scalar();
    // auto  pvtracks_simd   = pvtracks.simd();

    // if we add reasonable values in the padding, we can save time in the addition in the vertex fit.
    reset<false>( pvtracks );

    // We run the loop from the pv tracks. The advantage is that we
    // can allow multiple pvtracks to point to the same velo
    // track.

    // for (auto pvtrack : pvtracks_simd ) {
    for ( auto pvtrack : pvtracks_scalar )
      if ( std::get<1>( pvtrack.template field<PVTrackTag::veloindex>().index() ) <
           pvtracks.prvelocontainers[std::get<0>( pvtrack.template field<PVTrackTag::veloindex>().index() ).cast()]
               ->size() ) {
        // This loop seems >2x slower in simd than in scalar, so leave scalar for now.
        // auto [containerIdx, veloIdx] = pvtrack.field<PVTrackTag::veloindex>().index();
        // const int containerIdx_int = containerIdx.cast() ;
        // const int veloIdx_int = veloIdx.cast() ;
        // onst auto track     = pvtracks.containers<PVTrackTag::veloindex>()[containerIdx_int]->scalar()[veloIdx_int] ;
        populateFromPrVelo( pvtrack );
      }
    // const auto pvtrack = pvtracks.simd()[pvtracks.size()-1] ;
    // std::cout << "Padding check: "
    // 	      << pvtrack.get<PVTrackTag::z>() << " "
    // 	      << pvtrack.get<PVTrackTag::Vx>(0) << std::endl ;
  }

  void populateFromVeloTracks( PrimaryVertexContainer& pvdata, const LHCb::Pr::Velo::Tracks& tracksForward,
                               const LHCb::Pr::Velo::Tracks& tracksBackward ) {
    pvdata.tracks.prvelocontainers[0] = &tracksForward;
    pvdata.tracks.prvelocontainers[1] = &tracksBackward;
    populateFromVeloTracks( pvdata );
  }

  void populateFromRecVertices( PrimaryVertexContainer& pvcontainer, const RecVertex::Range& recvertices ) {
    auto& vertices = pvcontainer.vertices;
    vertices.clear();
    vertices.resize( recvertices.size() );
    size_t N{ 0 };
    size_t ipv{ 0 };
    for ( const auto& recvtx : recvertices ) {
      auto& vtx = vertices[ipv++];
      vtx.VertexBase::operator=( *recvtx );
      const size_t thisN = std::count_if( recvtx->tracks().begin(), recvtx->tracks().end(),
                                          []( const auto& trkp ) { return trkp != nullptr; } );
      vtx.setRange( N, N + thisN );
      N += paddedsize( thisN );
    }
    // Second reserve and fill the tracks
    auto& pvtracks = pvcontainer.tracks;
    pvtracks.resize( N );
    reset( pvtracks );

    // fill the tracks from the v1 tracks in the RecVertex
    ipv = 0;
    for ( const auto& recvtx : recvertices ) {
      auto&  vtx  = vertices[ipv];
      size_t itrk = vtx.begin();
      for ( const auto& trkww : recvtx->tracksWithWeights() ) {
        const auto& weight = trkww.second;
        const auto& trk    = trkww.first;
        if ( trk ) {
          const auto& state   = trk->firstState();
          auto        pvtrack = pvtracks.scalar()[itrk++];
          pvtrack.field<PVTrackTag::z>().set( state.z() );
          pvtrack.field<PVTrackTag::x>().set( state.x() );
          pvtrack.field<PVTrackTag::y>().set( state.y() );
          pvtrack.field<PVTrackTag::tx>().set( state.tx() );
          pvtrack.field<PVTrackTag::ty>().set( state.ty() );
          pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xx ).set( state.covariance()( 0, 0 ) );
          pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::xtx ).set( state.covariance()( 0, 2 ) );
          pvtrack.field<PVTrackTag::Vx>( PVTrackTag::XTxCovMatrixElement::txtx ).set( state.covariance()( 2, 2 ) );
          pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xx ).set( state.covariance()( 1, 1 ) );
          pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::xtx ).set( state.covariance()( 1, 3 ) );
          pvtrack.field<PVTrackTag::Vy>( PVTrackTag::XTxCovMatrixElement::txtx ).set( state.covariance()( 3, 3 ) );
          pvtrack.field<PVTrackTag::weight>().set( weight );
          pvtrack.field<PVTrackTag::veloid>().set( uniqueVeloSegmentID( trk->lhcbIDs() ) );
          pvtrack.field<PVTrackTag::pvindex>().set( ipv );
        }
      }
      ++ipv;
    }
    pvcontainer.updateDerivatives();
    pvcontainer.updateVeloIDMap();
    pvcontainer.setIndices();
  }
} // namespace LHCb::TrackPVFinder
