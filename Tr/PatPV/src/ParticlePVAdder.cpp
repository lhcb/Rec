/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "GaudiKernel/KeyedContainer.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackKernel/PrimaryVertexUtils.h"

using Particles     = LHCb::Particles;
using PrimaryVertex = LHCb::PrimaryVertex;

struct ParticlePVAdder
    : public LHCb::Algorithm::Transformer<Particles( LHCb::Particle::Range const&, LHCb::PrimaryVertices const& )> {
  ParticlePVAdder( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer(
            name, pSvcLocator,
            { KeyValue{ "InputParticles", "" }, KeyValue{ "PrimaryVertices", LHCb::Event::PV::DefaultLocation } },
            KeyValue{ "OutputParticles", "" } ) {}

  Particles operator()( LHCb::Particle::Range const& inputparticles, LHCb::PrimaryVertices const& pvs ) const override {
    Particles outputparticles;
    for ( const auto& inputparticle : inputparticles ) {
      // copy the particle
      auto* outputparticle = new LHCb::Particle{ *inputparticle };
      outputparticles.add( outputparticle );
      const auto& pv = LHCb::bestPV( pvs, outputparticle->referencePoint(), outputparticle->momentum() );
      if ( pv ) outputparticle->setPV( pv );
    }
    return outputparticles;
  }
};

DECLARE_COMPONENT( ParticlePVAdder )
