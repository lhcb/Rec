/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/FTLiteCluster.h"
#include "Event/VPMicroCluster.h"

#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"

#include "Event/Track_v1.h"

/**
 * Tools of this type are responsible for adding the (minimal)
 * detector clusters to the track's <detector>Clusters members.
 * The clusters are found based on the lhcbids, for which
 * either an explicit list can be provided, or it's obtained from the
 * lhcbIDs() of the v1 track object.
 *
 * The methods return StatusCodes. It's up to the caller to check these
 * and stop executing in case it's considered a failure if the clusters are
 * incomplete.
 * By default, a warning is shown with the information about which subdetector
 * is missing.
 *
 * The standard use-case for these are v{2/3} track -> v1 track converters,
 * where first a v1 temporary track is created with just the lhcbID
 * container filled, and this tool is called to find the associated
 * clusters.
 *
 * @author Laurent Dufour
 */
struct ITrackAddClusters : extend_interfaces<IAlgTool> {
  DeclareInterfaceID( ITrackAddClusters, 1, 1 );

  // Fill from LHCbIDs on track
  virtual StatusCode fillClustersFromLHCbIDs( LHCb::Event::v1::Track& ) const = 0;

  // Fill from LHCbIDs provided
  virtual StatusCode fillClustersFromLHCbIDs( LHCb::Event::v1::Track&, const std::vector<LHCb::LHCbID>& ) const = 0;
};
