/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/State.h"
#include "GaudiKernel/Vector4DTypes.h"
#include "Kernel/STLExtensions.h"
#include <algorithm>
#include <cassert>
#include <cstdint>
#include <memory>
#include <numeric>
#include <optional>
#include <vector>

namespace LHCb {

  namespace TrackVertexHelpers {

    class VertexTrack final {

    public:
      using PositionCovariance = Gaudi::SymMatrix3x3;
      using MomentumCovariance = Gaudi::SymMatrix3x3;
      using PositionParameters = ROOT::Math::SVector<double, 3>;
      using MomentumParameters = ROOT::Math::SVector<double, 3>;
      using TrackVector        = ROOT::Math::SVector<double, 5>;
      enum Status : std::uint8_t { IsOK = 0, InitFailure, ProjFailure };

    public:
      VertexTrack( const LHCb::State& state, const PositionCovariance& m_poscov, double weight = 1.0 );

      /// set the state
      void setState( const LHCb::State& state ) {
        m_state = state;
        m_G     = state.covariance();
        m_G.InvertChol();
      }

      /// compute chisq derivatives. returns 1 if fine.
      Status project( const ROOT::Math::SVector<double, 3>& position, ROOT::Math::SVector<double, 3>& halfDChisqDX,
                      Gaudi::SymMatrix3x3& halfD2ChisqDX2 );
      /// same as above but exploiting that A and B are almost empty
      Status projectFast( const ROOT::Math::SVector<double, 3>& position, ROOT::Math::SVector<double, 3>& halfDChisqDX,
                          Gaudi::SymMatrix3x3& halfD2ChisqDX2 );

      /// derivative of momentum to position
      inline const auto& WBGA() const {
        if ( !m_WBGA.has_value() ) { computeWBGA(); }
        return *m_WBGA;
      }

      /// momentum (tx,ty) covariance ('D' in Fruhwirth)
      inline const auto& momcov() const {
        if ( !m_momcov.has_value() ) { computeMomCov(); }
        return *m_momcov;
      }
      inline auto& momcov() {
        if ( !m_momcov.has_value() ) { computeMomCov(); }
        return *m_momcov;
      }

      /// momentum-position covariance ('E' in Fruhwirth)
      inline const auto& momposcov() const {
        if ( !m_momposcov.has_value() ) { computeMomPosCov(); }
        return *m_momposcov;
      }
      inline auto& momposcov() {
        if ( !m_momposcov.has_value() ) { computeMomPosCov(); }
        return *m_momposcov;
      }

      /// chisquare (ignores the weight)
      inline auto chisq( const ROOT::Math::SVector<double, 3>& pos ) const {
        return Similarity( residual( pos ), m_G );
      }

      /// set the weight
      inline void setweight( const double w ) noexcept { m_weight = w; }

      /// retrieve the weight
      inline auto weight() const noexcept { return m_weight; }

      /// retrieve the status
      inline auto status() const noexcept { return m_status; }

      /// update momentum for given change in position
      inline void updateSlopes( const ROOT::Math::SVector<double, 3>& pos ) { m_q += m_WBG * residual( pos ); }

      inline TrackVector prediction( const PositionParameters& pos ) const {
        const auto dz = m_state.z() - pos( 2 );
        return { pos( 0 ) + dz * m_q( 0 ), pos( 1 ) + dz * m_q( 1 ), m_q( 0 ), m_q( 1 ), m_q( 2 ) };
      }

      /// residual for given position
      inline TrackVector residual( const PositionParameters& pos ) const {
        TrackVector res = -prediction( pos );
        res( 0 ) += m_state.x();
        res( 1 ) += m_state.y();
        res( 2 ) += m_state.tx();
        res( 3 ) += m_state.ty();
        res( 4 ) += m_state.qOverP();
        return res;
      }

      /// full state after fitting
      inline void momentum( ROOT::Math::SVector<double, 3>& mom, Gaudi::SymMatrix3x3& momcov,
                            ROOT::Math::SMatrix<double, 3, 3>& posmomcov ) const;

      inline const auto& state() const { return m_state; }
      inline const auto& A() const { return m_A; }
      inline const auto& B() const { return m_B; }
      inline const auto& mom() const { return m_q; }
      inline auto&       mom() { return m_q; }

      inline void setPosCov( const PositionCovariance* poscov ) noexcept { m_poscov = poscov; }

    private:
      void computeWBGA() const {
        assert( !m_WBGA.has_value() );
        m_WBGA = m_WBG * m_A;
      }
      void computeMomCov() const {
        assert( !m_momcov.has_value() );
        m_momcov.emplace();
        ROOT::Math::AssignSym::Evaluate( *m_momcov, -momposcov() * Transpose( WBGA() ) );
        *m_momcov += m_W;
      }
      void computeMomPosCov() const {
        assert( m_poscov );
        assert( !m_momposcov.has_value() );
        m_momposcov = -WBGA() * ( *m_poscov );
      }

    private:
      LHCb::State               m_state;
      const PositionCovariance* m_poscov = nullptr;
      double                    m_weight{ 0 };

      // In fruhwirth's notation
      Gaudi::SymMatrix5x5                                      m_G;   // weight matrix of state p=(x,y,tz,ty)
      ROOT::Math::SMatrix<double, 5, 3>                        m_A;   // projection matrix for vertex position
      ROOT::Math::SMatrix<double, 5, 3>                        m_B;   // projection matrix for slopes (tx,ty, qop)
      ROOT::Math::SVector<double, 3>                           m_q;   // predicted/fitted slope (tx,ty)
      Gaudi::SymMatrix3x3                                      m_W;   // cov matrix of slopes for fixed position
      ROOT::Math::SMatrix<double, 3, 5>                        m_WBG; // derivative dmom/dresidual
      mutable std::optional<ROOT::Math::SMatrix<double, 3, 3>> m_WBGA;
      mutable std::optional<Gaudi::SymMatrix3x3>               m_momcov;
      mutable std::optional<ROOT::Math::SMatrix<double, 3, 3>> m_momposcov;
      Status                                                   m_status{ InitFailure };
    };

  } // namespace TrackVertexHelpers

  /** @class TrackStateVertex TrackStateVertex.h
   *
   * Vertex(ing) class for multitrack vertices
   *
   * @author Wouter Hulsbergen
   * created Wed Dec 12 14:58:51 2007
   *
   */

  class TrackStateVertex {

  private:
    std::size_t             symIndex( const std::size_t i, const std::size_t j ) const { return i * ( i + 1 ) / 2 + j; }
    const Gaudi::Matrix3x3& computeMomMomCov( const std::size_t i, const std::size_t j ) const;
    bool        hasReference() const { return ( m_refweight( 0, 0 ) + m_refweight( 1, 1 ) + m_refweight( 2, 2 ) ) > 0; }
    void        initPos();
    const auto& track( const std::size_t i ) const {
      assert( i < m_tracks.size() );
      return m_tracks[i];
    }
    auto& track( const std::size_t i ) {
      assert( i < m_tracks.size() );
      return m_tracks[i];
    }

  public:
    ///
    enum FitStatus : std::uint8_t { FitSuccess = 0, FitFailure, UnFitted };
    enum ErrCode : std::uint8_t {
      NoError              = 0,
      NotConverged         = 1,
      BadInputTrack        = 2,
      InsufficientTracks   = 4,
      ProjectionFailure    = 8,
      InversionFailure     = 16,
      NotConvergedAdaptive = 32
    };

    /// Sets minimal data content for useable vertex. The rest we do with setters.
    TrackStateVertex() {
      for ( std::size_t i = 0; i < PositionParameters::kSize; ++i ) { m_pos( i ) = 0; }
    }

    /// Construct vertex from set of states. also fits the vertex.
    TrackStateVertex( span<const LHCb::State* const> states, const double maxdchisq = 0.01,
                      const std::size_t maxiterations = 10 ) {
      for ( std::size_t i = 0; i < PositionParameters::kSize; ++i ) { m_pos( i ) = 0; }
      for ( auto& state : states ) { addTrack( *state ); }
      fit( maxdchisq, maxiterations );
    }

    /// Construct vertex from set of states. also fits the vertex.
    TrackStateVertex( span<const LHCb::State> states, double maxdchisq = 0.01, size_t maxiterations = 10 ) {
      for ( size_t i = 0; i < PositionParameters::kSize; ++i ) m_pos( i ) = 0;
      for ( const auto& state : states ) addTrack( state );
      fit( maxdchisq, maxiterations );
    }

    /// Construct vertex from two states. also fits the vertex
    TrackStateVertex( const LHCb::State& state1, const LHCb::State& state2, const double maxdchisq = 0.01,
                      const std::size_t maxiterations = 10 ) {
      for ( std::size_t i = 0; i < PositionParameters::kSize; ++i ) { m_pos( i ) = 0; }
      addTrack( state1 );
      addTrack( state2 );
      fit( maxdchisq, maxiterations );
    }

    /// Construct from a reference vertex. Then add track states
    /// later. If you use inverse of cov matrix, specify 'isweight=true'.
    TrackStateVertex( const Gaudi::XYZPoint& reference, const Gaudi::SymMatrix3x3& refcovariance,
                      const bool isweightmatrix )
        : m_poscov( refcovariance ), m_fitStatus( UnFitted ), m_error( NoError ), m_chi2( -1 ) {
      m_refpos( 0 ) = reference.x();
      m_refpos( 1 ) = reference.y();
      m_refpos( 2 ) = reference.z();
      m_refweight   = refcovariance;
      if ( !isweightmatrix ) { m_refweight.InvertChol(); }
      m_pos = m_refpos;
    }

    /// Copy constructor
    TrackStateVertex( const TrackStateVertex& rhs )
        : m_pos( rhs.m_pos )
        , m_poscov( rhs.m_poscov )
        , m_mommomcov( rhs.m_mommomcov )
        , m_fitStatus( rhs.m_fitStatus )
        , m_error( rhs.m_error )
        , m_chi2( rhs.m_chi2 )
        , m_refpos( rhs.m_refpos )
        , m_refweight( rhs.m_refweight )
        , m_posweight( rhs.m_posweight ) {
      m_tracks.reserve( rhs.m_tracks.size() );
      std::transform( rhs.m_tracks.begin(), rhs.m_tracks.end(), std::back_inserter( m_tracks ),
                      []( const std::unique_ptr<VertexTrack>& t ) { return std::make_unique<VertexTrack>( *t ); } );
    }

    /// assignment
    TrackStateVertex& operator=( const TrackStateVertex& vertex );

    /// add a track. invalidates any existing fit. (call 'fitOneStep' afterwards)
    void addTrack( const LHCb::State& state, const Gaudi::TrackVector& reference, const double weight = 1.0 );

    /// add a track. invalidates any existing fit. (call 'fit' afterwards)
    void addTrack( const LHCb::State& state, const double weight = 1.0 ) {
      addTrack( state, state.stateVector(), weight );
    }

    /// Set the input state for track i. invalidates any existing fit. (call 'fit' afterwards)
    void setTrack( const std::size_t i, const LHCb::State& state ) { setInputState( i, state ); }

    /// fit a single iteration. returns the delta-chisquare.
    double fitOneStep();

    /// fit until converged
    FitStatus fit( const double maxdchisq = 0.01, const std::size_t maxiterations = 10 );

    /// adapative fit. downweight tracks with chi2 contribution larger than maxtrkchi2 using Huber weights.
    /// the '95%' efficient chi2 for 1dof = 1.8. I multiplied that by 2, then rounded.
    /// (see also http://research.microsoft.com/en-us/um/people/zhang/INRIA/Publis/Tutorial-Estim/node24.html)
    FitStatus fitAdaptive( const double maxtrkchi2 = 4, const double maxdchisq = 0.01,
                           const std::size_t maxiterations = 10 );

    /// return the fit status
    auto fitStatus() const { return m_fitStatus; }

    /// errcode gives some information on the type of error for failed fits
    auto error() const noexcept { return m_error; }

    /// calculate the chisquare of the vertex fit
    auto chi2() const {
      if ( m_chi2 < 0 ) {
        m_chi2 = std::accumulate( m_tracks.begin(), m_tracks.end(),
                                  hasReference() ? Similarity( m_refweight, ( m_pos - m_refpos ) ) : 0,
                                  [&]( double chi2, VertexTrackContainer::const_reference t ) {
                                    return chi2 + t->weight() * t->chisq( m_pos );
                                  } );
      }
      return m_chi2;
    }

    /// number of tracks in vertex
    auto nTracks() const { return m_tracks.size(); }

    /// number of dofs in vertex fit
    auto nDoF() const { return nTracks() * 2 - 3 + ( hasReference() ? 3 : 0 ); }

    /// Fitted state vector of track i
    Gaudi::TrackVector stateVector( const std::size_t i ) const { return track( i )->prediction( m_pos ); }
    /// Fitted covariance of track i
    Gaudi::TrackSymMatrix stateCovariance( const std::size_t i ) const;
    /// Correlation matrix between state i and j
    Gaudi::Matrix5x5 stateCovariance( const std::size_t i, const std::size_t j ) const;
    /// Fitted state for track i
    LHCb::State state( const std::size_t i ) const {
      return { stateVector( i ), stateCovariance( i ), track( i )->state().z(), track( i )->state().location() };
    }
    /// Fitted state at vertex z position for track i
    LHCb::State stateAtVertex( const std::size_t i ) const;
    /// Input state for track i
    const LHCb::State& inputState( const std::size_t i ) const { return track( i )->state(); }
    /// Set the input state for track i
    void setInputState( const std::size_t i, const LHCb::State& state );
    /// Position of the vertex
    Gaudi::XYZPoint position() const { return Gaudi::XYZPoint( m_pos( 0 ), m_pos( 1 ), m_pos( 2 ) ); }
    /// Position covariance of the vertex
    const Gaudi::SymMatrix3x3& covMatrix() const { return m_poscov; }
    /// Inverse of position covariance of the vertex
    const Gaudi::SymMatrix3x3& weightMatrix() const { return m_posweight; }
    /// Momentum (tx,ty,q/pc) of track i
    const ROOT::Math::SVector<double, 3>& mom( const std::size_t i ) const { return track( i )->mom(); }
    /// Momentum (tx,ty,q/pc) covariance matrix of track i
    const Gaudi::SymMatrix3x3& momCovMatrix( const std::size_t i ) const { return track( i )->momcov(); }
    /// Covariance matrix for momentum i and position
    const ROOT::Math::SMatrix<double, 3, 3>& momPosCovMatrix( const std::size_t i ) const {
      return track( i )->momposcov();
    }
    /// Covariance matrix for momentum i and momentum j (j<=i)
    const Gaudi::Matrix3x3& momMomCovMatrixFast( const std::size_t i, const std::size_t j ) const {
      return m_mommomcov.empty() ? computeMomMomCov( i, j ) : m_mommomcov[symIndex( i, j )];
    }
    /// Covariance matrix for momentum i and momentum j
    Gaudi::Matrix3x3 momMomCovMatrix( const std::size_t i, const std::size_t j ) const {
      return j <= i ? momMomCovMatrixFast( i, j ) : ROOT::Math::Transpose( momMomCovMatrixFast( j, i ) );
    }
    /// Get the total p4 given the mass hypotheses
    Gaudi::LorentzVector p4( const std::vector<double>& masshypos ) const;
    /// Get the total p4 given the mass hypotheses
    Gaudi::SymMatrix4x4 p4CovMatrix( const std::vector<double>& masshypos ) const;
    /// Get the cov7x7
    Gaudi::SymMatrix7x7 covMatrix7x7( const std::vector<double>& masshypos ) const;
    /// Get the mass given a set of mass hypotheses
    double mass( const std::vector<double>& masshypos ) const { return p4( masshypos ).M(); }
    /// Get the error on the mass given a set of mass hypotheses
    double massErr( const std::vector<double>& masshypos ) const;
    /// Add a mass constraint
    FitStatus constrainMass( const std::vector<double>& masshypos, const double constrainedmass,
                             const double naturalwidth = 0. );

    /// Projection matrix for state i to vertex position (matrix A in Fruhwirth)
    const ROOT::Math::SMatrix<double, 5, 3>& matrixA( const std::size_t i ) const { return track( i )->A(); }
    /// Projection matrix for state i to momentum i (matrix B in Fruhwirth)
    const ROOT::Math::SMatrix<double, 5, 3>& matrixB( const std::size_t i ) const { return track( i )->B(); }

    /// the biased chisquare contribution of a single track. (the
    /// unbiased one I still need to compute)
    double biasedChi2( const std::size_t i ) const { return track( i )->chisq( m_pos ); }

    /// get the weight of a track after the adaptive fit
    double weight( const std::size_t i ) const { return track( i )->weight(); }

    /// set the weight of a track
    void setWeight( const std::size_t i, const double w ) { return track( i )->setweight( w ); }

    /// move the tracks from another vertex to this vertex
    void takeTracks( TrackStateVertex&& rhs );

  private:
    using VertexTrack          = TrackVertexHelpers::VertexTrack;
    using VertexTrackContainer = std::vector<std::unique_ptr<VertexTrack>>;
    using MomentumParameters   = VertexTrack::MomentumParameters;
    using PositionParameters   = VertexTrack::PositionParameters;
    using PositionCovariance   = VertexTrack::PositionCovariance;
    using MomentumCovariance   = VertexTrack::MomentumCovariance;

  private:
    VertexTrackContainer                  m_tracks;
    PositionParameters                    m_pos;
    PositionCovariance                    m_poscov;
    mutable std::vector<Gaudi::Matrix3x3> m_mommomcov;
    FitStatus                             m_fitStatus = UnFitted;
    std::uint8_t                          m_error     = NoError;
    mutable double                        m_chi2      = -1;
    PositionParameters                    m_refpos;    // position of reference position
    PositionCovariance                    m_refweight; // weight (inverse cov) of reference position
    PositionCovariance                    m_posweight; // inverse of the position covariance matrix
  };

} // namespace LHCb
