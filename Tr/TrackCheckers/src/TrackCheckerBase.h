/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// from Gaudi
#include "Gaudi/Algorithm.h"

// interfaces
#include "GaudiKernel/IMagneticFieldSvc.h"
#include "MCInterfaces/IIdealStateCreator.h"
#include "MCInterfaces/IMCReconstructible.h"
#include "MCInterfaces/ITrackGhostClassification.h"
#include "MCInterfaces/IVisPrimVertTool.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"

#include <string>

/** @class TrackCheckerBase TrackCheckerBase.h "TrackCheckers/TrackCheckerBase"
 *
 *  Base class for track monitoring: essentially a 'box' of common tools

 *  @author M. Needham.
 *  @date   7-5-2007
 */

class TrackCheckerBase : public Gaudi::Algorithm {

public:
  /** Standard construtor */
  using Algorithm::Algorithm;

  /** Algorithm initialization */
  StatusCode initialize() override;

public:
  /** small struct for link info */
  struct LinkInfo {
    LinkInfo( const LHCb::Track* t, unsigned int c, double p ) : track( t ), clone( c ), purity( p ) {}
    LinkInfo() = default;
    const LHCb::Track* track{ nullptr };
    unsigned int       clone{ 0 };
    double             purity{ -1 };
  };

public:
  /** Get a pointer to Magnetic field service
   *  @return field service
   */
  auto fieldSvc() const { return m_pIMF.get(); }

  /** Get a pointer to the track selection tool
   *  @return field service
   */
  auto selector() const { return m_selector.get(); }

  /** Get a pointer to the idealStateCreator
   *  @return IdealStateCreator
   */
  auto idealStateCreator() const { return m_stateCreator.get(); }

  /** Get a pointer to the track extrapolator
   *  @return extrapolator
   */
  auto extrapolator() const { return m_extrapolator.get(); }

  /** link to truth
   * @param  aTrack track
   * @return linked particle
   */
  const LHCb::MCParticle* mcTruth( const LHCb::Track&, const LHCb::MCParticles&, const LHCb::LinksByKey& ) const;

  /** Selected as type
   *
   * @return bool
   */
  bool selected( const LHCb::MCParticle* particle ) const {
    return selector()->isReconstructibleAs( m_recCat, particle );
  }

  /** Whether to split by algorithm
   *  @return splitByAlgorithm true or false
   */
  bool splitByAlgorithm() const { return m_splitByAlgorithm.value(); }

  /** Whether to split by algorithm
   *  @return splitByType true or false
   */
  bool splitByType() const { return m_splitByType.value(); }

  /** Pointer to the visible primary vertex tool
   *  @return IVisPrimVertTool
   */
  auto visPrimVertTool() const { return m_visPrimVertTool.get(); }

  /** Pointer to ghost classification tool
   *  @return ITrackGhostClassification
   */
  auto ghostClassification() const { return m_ghostClassification.get(); }

  /** Is a b child ? ie has b quark somewhere in history
   * @param  mcPart MC particle
   * @return bool true/false
   */
  bool bAncestor( const LHCb::MCParticle* mcPart ) const;

  /** Is a lambda/ks
   * @param  mcPart MC particle
   * @return bool true/false
   */
  bool ksLambdaAncestor( const LHCb::MCParticle* mcPart ) const;

  /** Are all stable daughters of this particle reconstructible?
   * @param  mcPart MC particle
   * @return bool true/false
   */
  bool allDaughtersReconstructible( const LHCb::MCParticle* mcPart ) const;

  /** Are all stable daughters of this particle reconstructible *and* is a b child ?
   * @param  mcPart MC particle
   * @return bool true/false
   */
  bool bAncestorWithReconstructibleDaughters( const LHCb::MCParticle* mcPart ) const;

private:
  Gaudi::Property<std::string>    m_selectionCriteria{ this, "SelectionCriteria", "ChargedLong" };
  IMCReconstructible::RecCategory m_recCat;

  ServiceHandle<const IMagneticFieldSvc>      m_pIMF{ this, "MagneticFieldService", "MagneticFieldSvc" };
  ToolHandle<const ITrackGhostClassification> m_ghostClassification{ this, "GhostTool",
                                                                     "LongGhostClassification/GhostTool" };
  ToolHandle<const IMCReconstructible>        m_selector{ this, "Selector", "MCReconstructible/Selector" };
  ToolHandle<const ITrackExtrapolator>        m_extrapolator{ this, "Extrapolator", "TrackMasterExtrapolator" };
  ToolHandle<const IIdealStateCreator>        m_stateCreator{ this, "StateCreator", "IdealStateCreator" };
  ToolHandle<const IVisPrimVertTool>          m_visPrimVertTool{ this, "VisPrimVertTool", "VisPrimVertTool" };

  Gaudi::Property<bool> m_splitByAlgorithm{ this, "SplitByAlgorithm", false };
  Gaudi::Property<bool> m_splitByType{ this, "SplitByType", false };
};
