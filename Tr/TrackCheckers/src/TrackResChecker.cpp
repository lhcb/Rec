/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/Measurement.h"
#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators/StaticHistogram.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiUtils/HistoStats.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedTo.h"
#include "TrackCheckerBase.h"
#include "TrackInterfaces/ITrackProjector.h"
#include "TrackInterfaces/ITrackProjectorSelector.h"
#include "TrackKernel/TrackFunctors.h"

#include <mutex>
#include <string>
#include <unordered_map>

class TrackResChecker;

namespace {
  /// little wrapper around a map of histograms indexed by type/location
  /// implementing thread safe insertion of new histograms on demand
  template <int N, typename... Axis>
  struct HistoMapImpl : std::map<std::string, Gaudi::Accumulators::StaticHistogram<N>> {
    std::mutex          m{}; // thread safety of map insertion
    TrackResChecker*    algo;
    std::string         name, title;
    std::tuple<Axis...> axis;

    HistoMapImpl( TrackResChecker* _algo, std::string _name, std::string _title, Axis... _axis )
        : algo( _algo ), name( _name ), title( _title ), axis( _axis... ) {}

    static std::string typeToString( LHCb::Track::Types t ) {
      return t == LHCb::Track::Types::Unknown ? "ALL" : toString( t );
    }
    auto& get( LHCb::Track::Types t ) { return get( typeToString( t ) ); }
    auto& get( LHCb::Track::Types t, std::string const& dir ) { return get( typeToString( t ) + "/" + dir ); }
    auto& get( std::string const& t ) {
      if ( !this->contains( t ) ) {
        std::scoped_lock lock{ m };
        if ( !this->contains( t ) ) {
          // now we are alone and there is still nothing, let's create the histogram
          this->emplace( std::piecewise_construct, std::forward_as_tuple( t ),
                         std::forward_as_tuple( algo, fmt::format( "{}/{}", t, name ), title, axis ) );
        }
      }
      return this->at( t );
    }
  };
  template <int N>
  struct HistoMap;
  template <>
  struct HistoMap<1> : HistoMapImpl<1, Gaudi::Accumulators::Axis<double>> {
    using HistoMapImpl::HistoMapImpl;
  };
  template <>
  struct HistoMap<2> : HistoMapImpl<2, Gaudi::Accumulators::Axis<double>, Gaudi::Accumulators::Axis<double>> {
    using HistoMapImpl::HistoMapImpl;
  };
} // namespace

/** @class TrackResChecker TrackResChecker.h
 *
 * Class for track monitoring
 *  @author M. Needham.
 *  @date   6-5-2007
 */

class TrackResChecker
    : public LHCb::Algorithm::Consumer<
          void( LHCb::Track::Range const&, LHCb::MCParticles const&, LHCb::LinksByKey const&, DetectorElement const& ),
          LHCb::Algorithm::Traits::usesBaseAndConditions<TrackCheckerBase, DetectorElement>> {

public:
  /** Standard constructor */
  TrackResChecker( const std::string& name, ISvcLocator* pSvcLocator );

  /** Algorithm execute */
  void operator()( LHCb::Track::Range const&, LHCb::MCParticles const&, LHCb::LinksByKey const&,
                   DetectorElement const& ) const override;

private:
  void resolutionHistos( LHCb::Track::Types type, LHCb::Track const& track, LHCb::MCParticle const& mcPart,
                         IGeometryInfo const& ) const;

  void pullplots( LHCb::Track::Types type, const LHCb::State& trueState, const LHCb::State& recState,
                  const std::string& location ) const;

  void plotsByMeasType( LHCb::Track::Types type, LHCb::Track const& track, LHCb::MCParticle const& mcPart,
                        IGeometryInfo const& ) const;

private:
  Gaudi::Property<bool> m_plotsByMeasType{ this, "PlotsByMeasType", false };
  Gaudi::Property<bool> m_fullDetail{ this, "FullDetail", false };

  ToolHandle<ITrackProjectorSelector> m_projectorSelector{ this, "ProjectorSelector",
                                                           "TrackProjectorSelector/Projector" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_differentFitHistory{ this,
                                                                               "Tracks have different fit history" };

  mutable HistoMap<1> m_chi2PerDof{ this, "chi2PerDof", "chi2PerDof", { 1000, 0., 100. } };
  mutable HistoMap<1> m_probChi2{ this, "probChi2", "probChi2", { 50, 0., 1. } };
  mutable HistoMap<1> m_fitStatus{ this, "fitStatus", "fit status", { 5, -0.5, 4.5 } };
  mutable HistoMap<1> m_truemom{ this, "truemom", "true p [GeV]", { 100, 0, 50 } };
  mutable HistoMap<1> m_truept{ this, "truept", "true pT [GeV]", { 100, 0, 10 } };
  mutable HistoMap<1> m_correctcharge{ this, "correctcharge", "correct charge", { 2, -0.5, 1.5 } };

  mutable HistoMap<1> m_xres{ this, "xres", "x resolution / mm", { 101, -0.4, 0.4 } };
  mutable HistoMap<1> m_yres{ this, "yres", "y resolution / mm", { 101, -0.4, 0.4 } };
  mutable HistoMap<1> m_txres{ this, "txres", "tx resolution", { 101, -0.0025, 0.0025 } };
  mutable HistoMap<1> m_tyres{ this, "tyres", "ty resolution", { 101, -0.0025, 0.0025 } };

  mutable HistoMap<1> m_xpull{ this, "xpull", "x pull", { 101, -5., 5. } };
  mutable HistoMap<1> m_ypull{ this, "ypull", "y pull", { 101, -5., 5. } };
  mutable HistoMap<1> m_txpull{ this, "txpull", "tx pull", { 101, -5., 5. } };
  mutable HistoMap<1> m_typull{ this, "typull", "ty pull", { 101, -5., 5. } };

  mutable HistoMap<1> m_qopres{ this, "qop_res", "qop", { 101, -0.02, 0.02 } };
  mutable HistoMap<1> m_qoppull{ this, "qoppull", "qop pull", { 101, -5., 5. } };
  mutable HistoMap<1> m_ppull{ this, "ppull", "p pull", { 101, -5., 5. } };
  mutable HistoMap<1> m_dpoverp{ this, "dpoverp", "dp/p", { 101, -0.05, 0.05 } };
  mutable HistoMap<1> m_expecteddpoverp{ this, "expecteddpoverp", "expected dp/p", { 101, 0., 0.01 } };

  mutable HistoMap<1> m_measres{ this, "/meas_res", " Measurement resolution", { 100, -0.5, 0.5 } };
  mutable HistoMap<1> m_measpull{ this, "/meas_pull", " Measurement pull", { 100, -5., 5. } };
  mutable HistoMap<1> m_measchi2{ this, "/meas_chi2", " Measurement chi2", { 200, 0., 10. } };
  mutable HistoMap<2> m_measchi2ndf{
      this, "/meas_chi2ndf", "Measurement chi2 vs ndf", { 200, 0., 10. }, { 2, 0., 3. } };

  mutable HistoMap<2> m_vdopp{ this, "vertex/dpoverp_vs_p", "dp/p vs p", { 25, 0., 50. }, { 50, -0.1, 0.1 } };
  mutable HistoMap<2> m_vdopeta{ this, "vertex/dpoverp_vs_eta", "dp/p vs eta", { 20, 2., 5. }, { 50, -0.05, 0.05 } };
  mutable HistoMap<2> m_vpullp{ this,
                                "vertex/p_pull_vs_p",
                                "p pull vs p",
                                {
                                    25,
                                    0.,
                                    50.,
                                },
                                { 50, -10., 10. } };
  mutable HistoMap<2> m_vpulleta{ this, "vertex/p_pull_vs_eta", "p pull vs eta", { 20, 2., 5. }, { 50, -10., 10. } };
};

DECLARE_COMPONENT( TrackResChecker )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackResChecker::TrackResChecker( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                { KeyValue{ "TracksInContainer", LHCb::TrackLocation::Default },
                  KeyValue{ "MCParticleInContainer", LHCb::MCParticleLocation::Default },
                  KeyValue{ "LinkerInTable", "Link/" + LHCb::TrackLocation::Default },
                  KeyValue{ "StandardGeometryTop", LHCb::standard_geometry_top } } ) {}

//=============================================================================
// Execute
//=============================================================================
void TrackResChecker::operator()( LHCb::Track::Range const& tracks, LHCb::MCParticles const& mcParts,
                                  LHCb::LinksByKey const& links, DetectorElement const& lhcb ) const {

  if ( std::adjacent_find( tracks.begin(), tracks.end(), []( const auto& t1, const auto& t2 ) {
         return t1->fitHistory() != t2->fitHistory();
       } ) != tracks.end() ) {
    ++m_differentFitHistory;
  }
  // loop over them
  for ( const auto* track : tracks ) {
    // Get the associated true particle
    const LHCb::MCParticle* mcparticle = mcTruth( *track, mcParts, links );

    if ( mcparticle
         // we actually just want to know if it passes the 'selector' inside the IMCReconstructible
         // && int(selector()->reconstructible(mcparticle)) > int(IMCReconstructible::NotReconstructible)
    ) {
      auto type = splitByType() ? track->type() : LHCb::Track::Types::Unknown;

      // resolutions at predefined z.
      resolutionHistos( type, *track, *mcparticle, *lhcb.geometry() );

      // prob chi^2
      ++m_chi2PerDof.get( type )[track->chi2PerDoF()];
      ++m_probChi2.get( type )[track->probChi2()];

      // fit status
      ++m_fitStatus.get( type )[static_cast<int>( track->fitStatus() )];

      ++m_truemom.get( type )[mcparticle->p() / Gaudi::Units::GeV];
      ++m_truept.get( type )[mcparticle->pt() / Gaudi::Units::GeV];

      // Resolutions and pulls per Measurement type
      if ( m_plotsByMeasType.value() && nMeasurements( *track ) > 0 )
        plotsByMeasType( type, *track, *mcparticle, *lhcb.geometry() );
    }
  }
}

//=============================================================================
//
//=============================================================================
void TrackResChecker::resolutionHistos( LHCb::Track::Types type, LHCb::Track const& track,
                                        LHCb::MCParticle const& mcPart, IGeometryInfo const& geometry ) const {
  // pulls at vertex
  LHCb::State trueStateVertex;
  idealStateCreator()->createStateVertex( &mcPart, trueStateVertex ).ignore();
  LHCb::State vtxState;
  StatusCode  sc = extrapolator()->propagate( track, trueStateVertex.z(), vtxState, geometry );
  if ( sc.isSuccess() ) pullplots( type, trueStateVertex, vtxState, "vertex" );

  // for vertex also make some 2-d plots
  if ( track.type() == LHCb::Track::Types::Long || track.type() == LHCb::Track::Types::Upstream ||
       track.type() == LHCb::Track::Types::Downstream || track.type() == LHCb::Track::Types::Ttrack ) {
    const double invp  = std::abs( track.firstState().qOverP() );
    const double ptrue = mcPart.p();
    const double eta   = mcPart.pseudoRapidity(); // track.pseudoRapidity();

    ++m_vdopp.get( type )[{ ptrue / Gaudi::Units::GeV, invp * ptrue - 1 }];
    ++m_vdopeta.get( type )[{ eta, invp * ptrue - 1 }];

    const double invperr2 = track.firstState().covariance()( 4, 4 );
    if ( invperr2 > 0 ) {
      const double ppull = ( invp - 1 / ptrue ) / std::sqrt( invperr2 );
      ++m_vpullp.get( type )[{ ptrue / Gaudi::Units::GeV, ppull }];
      ++m_vpulleta.get( type )[{ eta, ppull }];
    }
  }

  // fraction of tracks with correct charge
  bool correctcharge = track.firstState().qOverP() * mcPart.particleID().threeCharge() > 0;
  ++m_correctcharge.get( type )[correctcharge];

  if ( m_fullDetail.value() ) {
    for ( const LHCb::State* state : track.states() ) {
      // skip the closest to beam, since we already have it
      if ( state->location() == LHCb::State::Location::ClosestToBeam ) continue;
      double state_z = state->z();
      if ( state_z > mcPart.originVertex()->position().Z() && state_z < mcPart.endVertices().back()->position().Z() ) {
        LHCb::State trueState;
        StatusCode  sc = idealStateCreator()->createState( &mcPart, state_z, trueState, geometry );
        if ( sc.isSuccess() ) {
          std::string location = state->location() != LHCb::State::Location::LocationUnknown
                                     ? toString( state->location() )
                                     : format( "state_%d_mm", int( state_z ) );
          pullplots( type, trueState, *state, location );
        }
      }
    }
  }
}

//=============================================================================
//
//=============================================================================
void TrackResChecker::pullplots( LHCb::Track::Types type, const LHCb::State& trueState, const LHCb::State& recState,
                                 const std::string& location ) const {

  // save some typing
  const Gaudi::TrackVector&    vec     = recState.stateVector();
  const Gaudi::TrackVector&    trueVec = trueState.stateVector();
  const Gaudi::TrackSymMatrix& cov     = recState.covariance();
  const Gaudi::TrackSymMatrix& trueCov = trueState.covariance();
  const double                 dx      = vec( 0 ) - trueVec( 0 );
  const double                 dy      = vec( 1 ) - trueVec( 1 );
  const double                 dtx     = vec( 2 ) - trueVec( 2 );
  const double                 dty     = vec( 3 ) - trueVec( 3 );

  // fill the histograms
  ++m_xres.get( type, location )[dx];
  ++m_yres.get( type, location )[dy];
  ++m_txres.get( type, location )[dtx];
  ++m_tyres.get( type, location )[dty];

  ++m_xpull.get( type, location )[dx / sqrt( cov( 0, 0 ) + trueCov( 0, 0 ) )];
  ++m_ypull.get( type, location )[dy / sqrt( cov( 1, 1 ) + trueCov( 1, 1 ) )];
  ++m_txpull.get( type, location )[dtx / sqrt( cov( 2, 2 ) + trueCov( 2, 2 ) )];
  ++m_typull.get( type, location )[dty / sqrt( cov( 3, 3 ) + trueCov( 3, 3 ) )];

  if ( std::abs( cov( 4, 4 ) ) > 1e-20 ) { // test that there was a momentum measurement
    const double qop      = vec( 4 );
    const double qoptrue  = trueVec( 4 );
    const double invp     = std::abs( qop );
    const double invptrue = std::abs( qoptrue );
    const double qoperr   = std::sqrt( cov( 4, 4 ) + trueCov( 4, 4 ) );
    // make two pulls, to be sensitive to both a curvature and a momentum bias
    ++m_qopres.get( type, location )[( qop - qoptrue ) / invptrue];
    ++m_qoppull.get( type, location )[( qop - qoptrue ) / qoperr];
    ++m_ppull.get( type, location )[( invp - invptrue ) / qoperr];
    ++m_dpoverp.get( type, location )[invp / invptrue - 1];
    if ( invp > 0 ) ++m_expecteddpoverp.get( type, location )[std::sqrt( cov( 4, 4 ) ) / invp];
  }
}

//=============================================================================
//
//=============================================================================
void TrackResChecker::plotsByMeasType( LHCb::Track::Types type, LHCb::Track const& track,
                                       LHCb::MCParticle const& mcPart, IGeometryInfo const& geometry ) const {
  for ( const auto& measure : measurements( track ) ) {

    LHCb::State trueStateAtMeas;
    StatusCode  sc = idealStateCreator()->createState( &mcPart, measure.z(), trueStateAtMeas, geometry );
    if ( sc.isSuccess() ) {

      const std::string dir = Gaudi::Utils::toString( measure.type() );
      LHCb::State       stateAtMeas;
      StatusCode        sc = extrapolator()->propagate( track, measure.z(), stateAtMeas, geometry );
      if ( sc.isSuccess() ) {
        // make pull plots as before
        pullplots( type, trueStateAtMeas, stateAtMeas, dir );
      }

      // Monitor unbiased measurement resolutions
      ITrackProjector* proj = m_projectorSelector->projector( measure );
      if ( proj != 0 ) {

        auto [sc, projectResult] = proj->project( trueStateAtMeas, measure );

        if ( sc ) {

          struct Result {
            double res, errMeasure, chi2;
            int    ndf;
          };

          const auto [res, errMeasure, chi2, ndf] = std::visit(
              Gaudi::overload(
                  [&]( ITrackProjector::Project1DResult& projResult ) -> Result {
                    auto res        = projResult.residual[0];
                    auto errMeasure = projResult.errMeasure[0];
                    return { res, errMeasure, ( errMeasure > 0 ? std::pow( res / errMeasure, 2 ) : 0. ), 1 };
                  },
                  [&]( ITrackProjector::Project2DResult& projResult ) -> Result {
                    auto residual   = projResult.residual;
                    auto errMeasure = projResult.errMeasure;
                    return { sqrt( pow( residual[0], 2 ) + pow( residual[1], 2 ) ),
                             sqrt( pow( errMeasure[0], 2 ) + pow( errMeasure[1], 2 ) ),
                             ( errMeasure > 0 ? std::pow( residual[0] / errMeasure[0], 2 ) +
                                                    std::pow( residual[1] / errMeasure[1], 2 )
                                              : 0. ),
                             2 };
                  } ),
              projectResult );

          ++m_measres.get( type, dir )[res];
          ++m_measpull.get( type, dir )[res / errMeasure];
          ++m_measchi2.get( type, dir )[chi2];
          ++m_measchi2ndf.get( type, dir )[{ chi2, ndf }];
        }
      } else {
        warning() << "could not get projector for measurement" << endmsg;
      }
    }
  } // iterate measurements
}
