###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from cppyy import gbl
from LHCbMath.VectorizedML import Sequence

GhostProb_Long_noUT = Sequence(gbl.LHCb.GhostProbability.Long_noUT())

GhostProb_Long = Sequence(gbl.LHCb.GhostProbability.Long())
GhostProb_Downstream = Sequence(gbl.LHCb.GhostProbability.Downstream())
GhostProb_Upstream = Sequence(gbl.LHCb.GhostProbability.Upstream())
GhostProb_Ttrack = Sequence(gbl.LHCb.GhostProbability.Ttrack())
GhostProb_Velo = Sequence(gbl.LHCb.GhostProbability.Velo())
