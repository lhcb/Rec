/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FitNode.h"
#include "Event/KalmanFitResult.h"
#include "Event/PrFitNode.h"
#include "Event/PrKalmanFitResult.h"
#include "Event/Track.h"
#include "Event/TrackUnitsConverters.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbMath/MatrixManip.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackInterpolator.h"
#include "TrackKernel/TrackFunctors.h"
#include <type_traits>

using namespace Gaudi;
using namespace Gaudi::Math;
using namespace LHCb;

/** @class TrackInterpolator TrackInterpolator.h
 *
 *  This tool finds the two nearest nodes and interpolates between the nodes
 *  to get the best estimate of an intermediate state at the given z-position.
 *  It extrapolates the two filtered states to the intermediate z-position and
 *  calculated the weighted mean.
 *  The current implemtation also applies the Kalman filter step because only
 *  the result from the prediction step is stored in the node (not the result
 *  of the filtered step).
 *
 *  @author Jeroen van Tilburg
 *  @date   2006-10-06
 */

class TrackInterpolator : public extends<GaudiTool, ITrackInterpolator> {
public:
  /// Standard constructor
  using extends::extends;

  /// Interpolate between the two nearest nodes to get a state
  StatusCode interpolate( const LHCb::Track& track, double z, LHCb::State& state,
                          IGeometryInfo const& geometry ) const override;

  template <typename TFitResult>
  StatusCode Interpolator( const TFitResult& fr, const LHCb::Track& track, double z, LHCb::State& state,
                           IGeometryInfo const& geometry ) const;

private:
  /// extrapolator
  ToolHandle<ITrackExtrapolator> m_extrapolator{ this, "Extrapolator", "TrackMasterExtrapolator" };

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_inverse_failure{ this, "Failure inverting matrix in smoother",
                                                                           0 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_propagate_upstream_failure{
      this, "Failure propagating upstream state", 0 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_propagate_downstream_failure{
      this, "Failure propagating downstream state", 0 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_out_of_range{
      this, "Failure extrapolating outside measurement range", 0 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_locating_fitnodes_failure{
      this, "Logic failure in locating fitnodes", 0 };
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nodes_size_zero{ this, "The size of fitnodes is zero", 0 };
};

//-----------------------------------------------------------------------------
// Implementation file for class : TrackInterpolator
//
// 2006-10-06 : Jeroen van Tilburg
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( TrackInterpolator )

//=============================================================================
// Interpolate between the nearest nodes
//=============================================================================
StatusCode TrackInterpolator::interpolate( const Track& track, double z, State& state,
                                           IGeometryInfo const& geometry ) const {
  if ( !track.fitResult() ) {
    throw GaudiException( "TrackInterpolator", "Track has no fit result", StatusCode::FAILURE );
  }

  if ( fitResult( track ) ) {
    return Interpolator( *fitResult( track ), track, z, state, geometry );
  } else {
    const auto kf_fitresult = dynamic_cast<const PrKalmanFitResult*>( track.fitResult() );
    if ( kf_fitresult ) {
      return Interpolator( *kf_fitresult, track, z, state, geometry );
    } else
      throw GaudiException( "TrackInterpolator", "Track has unknown fit result type", StatusCode::FAILURE );
  }
}

template <typename TFitResult>
StatusCode TrackInterpolator::Interpolator( const TFitResult& fitResult, const Track& track, double z, State& istate,
                                            IGeometryInfo const& geometry ) const {
  // Check that track was actally fitted. Otherwise quietly call
  // extrapolator.
  const auto& fitnodes = nodes( fitResult );
  if ( fitnodes.size() <= 0 ) {
    ++m_nodes_size_zero;
    return m_extrapolator->propagate( track, z, istate, geometry );
  }

  // If we are between the first and last node with a measurement, we
  // interpolate. If not, we extrapolate from the closest 'inside'
  // node. (That's more stable than interpolation.) In the end this
  // needs to work both for upstream and downstream fits. I am not
  // sure that it works for either now.

  // first find the pair of iterators such that z is between 'prevnode' and 'nextnode'
  auto nextnode = fitnodes.begin();

  if ( fitnodes.front().z() < fitnodes.back().z() ) {
    nextnode = std::find_if( nextnode, fitnodes.end(), [z]( const auto& n ) { return n.z() >= z; } );
  } else {
    nextnode = std::find_if( nextnode, fitnodes.end(), [z]( const auto& n ) { return n.z() <= z; } );
  }

  // then determine where we are wrt to nodes with (active) measurements
  // is there measurement in nodes < nextnode?
  bool foundprecedingmeasurement =
      std::any_of( fitnodes.begin(), nextnode, []( const auto& n ) { return ( n.isHitOnTrack() ); } );
  // is there a measurement in nodes >= nextnode?
  bool foundprocedingmeasurement =
      std::any_of( nextnode, fitnodes.end(), []( const auto& n ) { return ( n.isHitOnTrack() ); } );

  // we must find either of the two (there must be measurement nodes!)
  if ( !foundprecedingmeasurement && !foundprocedingmeasurement )
    throw GaudiException( "TrackInterpolator", "Did not find any measurement fitnodes on track!", StatusCode::FAILURE );

  // interpolate only if we have measurements on both sides
  if ( !foundprecedingmeasurement || !foundprocedingmeasurement ) {
    const auto& extrapolationnode = foundprocedingmeasurement ? *nextnode : *std::prev( nextnode );
    istate                        = state( extrapolationnode );
    return m_extrapolator->propagate( istate, z, geometry ).orElse( [&] {
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Failure with normal extrapolator: z_target = " << z << " track type = " << track.type() << std::endl
                << "state = " << state( extrapolationnode ) << endmsg;
      ++m_out_of_range;
    } );
  }

  auto prevnode = std::prev( nextnode );

  if ( ( z - ( *nextnode ).z() ) * ( z - ( *prevnode ).z() ) > 0 ) {
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Logic failure in locating fitnodes: " << z << ", " << ( *prevnode ).z() << "," << ( *nextnode ).z()
              << endmsg;
    ++m_locating_fitnodes_failure;
    return StatusCode::FAILURE;
  }

  // bail out if we have actually reached our destination
  for ( const auto& node : { *nextnode, *prevnode } ) {
    if ( std::abs( node.z() - z ) < TrackParameters::propagationTolerance ) {
      istate = state( node );
      return StatusCode::SUCCESS;
    }
  }

  // downstream propagation
  const auto  Down_refvector_at_origin = ( *prevnode ).refVector();
  auto        Down_refvector_at_target = Down_refvector_at_origin;
  TrackMatrix F_Down;
  auto        sc = m_extrapolator->propagate( Down_refvector_at_target, z, geometry, &F_Down );
  if ( sc.isFailure() ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Error propagating downstream state to z = " << z << " tracktype = " << track.type() << std::endl
              << "state = " << state( *prevnode ) << endmsg;
    }
    ++m_propagate_downstream_failure;
    return sc;
  }
  auto stateDown = filteredStateForward( *prevnode );
  // propagate the filtered state using the reference
  stateDown.stateVector() = Down_refvector_at_target.parameters() +
                            F_Down * ( stateDown.stateVector() - Down_refvector_at_origin.parameters() );
  stateDown.covariance() = ROOT::Math::Similarity( F_Down, stateDown.covariance() );
  stateDown.setZ( z );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "propagating downstream state:" << std::endl
            << state( *prevnode ) << std::endl
            << "to new state at z = " << z << ":" << std::endl
            << stateDown << endmsg;
  }

  // upstream propagation
  const auto  Up_refvector_at_origin = ( *nextnode ).refVector();
  auto        Up_refvector_at_target = Up_refvector_at_origin;
  TrackMatrix F_Up;
  sc = m_extrapolator->propagate( Up_refvector_at_target, z, geometry, &F_Up );
  if ( sc.isFailure() ) {
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Error propagating upstream state to z = " << z << " tracktype = " << track.type() << std::endl
              << "state = " << state( *nextnode ) << endmsg;
    }
    ++m_propagate_upstream_failure;
    return sc;
  }
  auto stateUp = filteredStateBackward( *nextnode );
  // propagate the filtered state using the reference
  stateUp.stateVector() =
      Up_refvector_at_target.parameters() + F_Up * ( stateUp.stateVector() - Up_refvector_at_origin.parameters() );
  stateUp.covariance() = ROOT::Math::Similarity( F_Up, stateUp.covariance() );
  stateUp.setZ( z );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "propagating upstream state:" << std::endl
            << state( *nextnode ) << std::endl
            << "to new state at z = " << z << ":" << std::endl
            << stateUp << endmsg;
  }

  // Get the predicted downstream state and invert the covariance matrix
  const TrackVector& stateDownX    = stateDown.stateVector();
  TrackSymMatrix     invStateDownC = stateDown.covariance();
  if ( !invStateDownC.InvertChol() ) {
    ++m_inverse_failure;
    return StatusCode::FAILURE;
  }

  // Get the predicted upstream state and invert the covariance matrix
  const TrackVector& stateUpX    = stateUp.stateVector();
  TrackSymMatrix     invStateUpC = stateUp.covariance();
  if ( !invStateUpC.InvertChol() ) {
    ++m_inverse_failure;
    return StatusCode::FAILURE;
  }

  // Add the inverted matrices
  TrackSymMatrix& stateC = istate.covariance();
  stateC                 = invStateDownC + invStateUpC;
  if ( !stateC.InvertChol() ) {
    ++m_inverse_failure;
    return StatusCode::FAILURE;
  }

  // Get the state by calculating the weighted mean
  TrackVector& stateX = istate.stateVector();
  stateX              = stateC * ( ( invStateDownC * stateDownX ) + ( invStateUpC * stateUpX ) );
  istate.setZ( z );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "filteredstate A: " << stateUpX << std::endl
            << "filteredstate B: " << stateDownX << std::endl
            << "smoothed state A: " << state( *prevnode ) << "smoothed state B: " << state( *nextnode )
            << "interpolated state: " << istate << endmsg;

  return StatusCode::SUCCESS;
}
