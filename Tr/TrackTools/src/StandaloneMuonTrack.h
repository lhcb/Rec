/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Core/FloatComparison.h"
#include "MuonDAQ/CommonMuonHit.h"

#include "Detector/Muon/MuonConstants.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

#include <boost/container/small_vector.hpp>
#include <string>

/** @class StandaloneMuonTrack StandaloneMuonTrack.h
 *
 *  @author Alessia Satta
 *  @date   2004-10-08
 *
 * Removed from Hlt/HltMuon and ported to Tr/TrackTools
 *
 *  @author Paul Seyfert
 *  @date   2011-03-03
 */
namespace {

  enum class FitMode { XZ, YZ };

  struct FitResult {
    float slope     = -1e6;
    float trunc     = -1e6;
    float chi2ndof  = -1e6;
    float err_slope = -1e6;
    float err_trunc = -1e6;
    float cov       = -1e6;
  };

  template <typename MuonPositionClass, FitMode fitMode>
  FitResult linearFitImpl( boost::container::small_vector<MuonPositionClass, LHCb::Detector::Muon::nStations>& points,
                           float yAt0Err = std::numeric_limits<float>::max() ) {
    float sz2, sz, s0, sxz, sx, sx2;
    sz2 = sz = s0 = sxz = sx = sx2 = 0;

    FitResult result;

    for ( const auto& point : points ) {
      const auto z    = point.z();
      auto       x    = point.x();
      auto       xerr = 2. * point.dx();
      if ( fitMode == FitMode::YZ ) {
        x    = point.y();
        xerr = 2 * point.dy();
      }
      sz2 += z * z / xerr / xerr;
      sz += z / xerr / xerr;
      s0 += 1.0 / xerr / xerr;
      sxz += z * x / xerr / xerr;
      sx += x / xerr / xerr;
      sx2 += x * x / xerr / xerr;
    }

    if ( yAt0Err < std::numeric_limits<float>::max() )
      s0 += 1.0 / ( yAt0Err * yAt0Err ); // this applies a constraint to 0/0/0

    const float xdet = sz2 * s0 - sz * sz;
    if ( !LHCb::essentiallyZero( xdet ) ) {
      result.slope = ( sxz * s0 - sx * sz ) / xdet;
      result.trunc = ( sx * sz2 - sxz * sz ) / xdet;

      result.err_trunc = sqrt( sz2 / xdet );
      result.err_slope = sqrt( s0 / xdet );
      result.cov       = -sz / xdet;

      result.chi2ndof =
          ( sx2 + result.slope * result.slope * sz2 + result.trunc * result.trunc * s0 - 2. * result.slope * sxz -
            2. * result.trunc * sx + 2 * result.slope * result.trunc * sz ) /
          ( points.size() - 2 + ( yAt0Err < std::numeric_limits<float>::max() ) ); // add one degree of freedom if we
                                                                                   // add the "point" at 0/0/0
    }

    return result;
  }
} // namespace

template <typename MuonPositionClass>
class StandaloneMuonTrack final {
public:
  /// Standard constructor
  StandaloneMuonTrack() {
    m_clone = false;
    m_points.reserve( 4 );
  };

  virtual ~StandaloneMuonTrack(){}; ///< Destructor
  void setPoint( unsigned int station, MuonPositionClass point ) { m_points[station] = point; };
  void addPoint( MuonPositionClass point ) { m_points.push_back( point ); };

  MuonPositionClass point( unsigned int station ) const { return m_points[station]; };

  void setClone() { m_clone = true; };
  bool isClone() const { return m_clone; }

  // -- used to constrain the straight line fit in y to 0/0/0
  void setYAt0Err( const float yAt0Err ) { m_yAt0Err = yAt0Err; }

  // Fit with a min chi^2 in the 2 projections xz and yz
  bool linearFit() {
    const double dof = nHits() - 2.;
    if ( dof < 0 ) return false;
    m_resultXZ = linearFitImpl<MuonPositionClass, FitMode::XZ>( m_points );
    m_resultYZ = linearFitImpl<MuonPositionClass, FitMode::YZ>( m_points, m_yAt0Err );
    return m_resultXZ.chi2ndof > -1.f && m_resultYZ.chi2ndof > -1.f;
  };

  inline double chi2x() const { return m_resultXZ.chi2ndof; } /// chi2/dof XZ
  inline double chi2y() const { return m_resultYZ.chi2ndof; } /// chi2/dof YZ
  // slope XZ
  inline double sx() const { return m_resultXZ.slope; }
  // intercept XZ
  inline double bx() const { return m_resultXZ.trunc; }
  // slope YZ
  inline double sy() const { return m_resultYZ.slope; }
  // intercept YZ
  inline double by() const { return m_resultYZ.trunc; }

  // errors on the above parameters
  inline double errsx() const { return m_resultXZ.err_slope; }
  inline double errbx() const { return m_resultXZ.err_trunc; }
  inline double covbsx() const { return m_resultXZ.cov; }
  inline double errsy() const { return m_resultYZ.err_slope; }
  inline double errby() const { return m_resultYZ.err_trunc; }
  inline double covbsy() const { return m_resultYZ.cov; }

  inline int nHits() const { return m_points.size(); }

private:
  boost::container::small_vector<MuonPositionClass, LHCb::Detector::Muon::nStations> m_points;
  bool                                                                               m_clone;
  float m_yAt0Err = std::numeric_limits<float>::max();

  FitResult m_resultXZ;
  FitResult m_resultYZ;
};
