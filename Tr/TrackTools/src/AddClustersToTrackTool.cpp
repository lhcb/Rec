/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/FTLiteCluster.h"
#include "Event/Track_v1.h"
#include "Event/UTHitCluster.h"
#include "Event/VPMicroCluster.h"

#include "GaudiAlg/FunctionalTool.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IBinder.h"

#include "TrackInterfaces/ITrackAddClusters.h"

#include <vector>

namespace {
  using VPMicroClusters = LHCb::VPMicroClusters;
  using FTLiteClusters  = LHCb::FTLiteCluster::FTLiteClusters;
  using UTClusters      = LHCb::UTHitClusters;

  using TrackVPClusters = LHCb::VPMicroClusterVector;
  using TrackFTClusters = std::vector<LHCb::FTLiteCluster>;
  using TrackUTClusters = std::vector<LHCb::UTHitCluster>;

  /**
   * Note:
   *
   * These searches are done cluster-by-cluster, as typically there is an index present
   * which nullifies the gain you'd have from looking in the entire container for
   * all IDs.
   */
  auto copy_clusters = []( auto const& in, auto& out, auto const& channel_ids ) {
    bool all_found = true;

    std::for_each( channel_ids.begin(), channel_ids.end(), [&in, &out, &all_found]( auto id ) {
      auto cluster = findCluster( id, in );

      if ( cluster ) {
        out.push_back( *cluster );
      } else {
        all_found = false;
      }
    } );

    return all_found;
  };

  /**
   * These numbers are used for initial memory allocations
   * for the number of hits on a track.
   *
   * They are chosen based on 100k (hlt1 filtered) 2024 events,
   * in which no radiation damage was simulated.
   * (note that the nVeloHits is lower than what is used in the PR,
   *  as there it's likely used as a hard limit. We don't see tracks
   *  going on both sides of the detector simultaneous and going through
   *  all backward and forward modules at the same time)
   *
   * If any trakc is encountered in which more hits are on the track
   * than this limit, the vector size is just extended, and the
   * application will not crash. It will, however, impact throughput.
   */
  size_t SOFT_MAX_N_VELO_HITS_ON_TRACK = 26;
  size_t SOFT_MAX_N_UT_HITS_ON_TRACK   = 8;
  size_t SOFT_MAX_N_FT_HITS_ON_TRACK   = 12;

} // namespace

namespace LHCb {
  /**
   * GaudiTool to fill the cluster attributes of the v1 Track object with the
   * corresponding minimal clusters.
   *
   * This has to be provided with the data handles of these clusters.
   * If operating in a situation in which part of the detectors are absent,
   * or no clusters are present, it's possible to pass empty containers
   * instead (see the empty container producers).
   *
   * The main implementation is done through the copy_clusters function
   * detailed in the anonymous namespace above. The findCluster functions
   * are provided in the cluster class' files themselves.
   *
   * The typical use-case for this is the v3/v2 track -> v1 track conversions,
   * along with the pr track -> v1 track conversions.
   *
   * @authors Laurent Dufour, with help from Gerhard Raven
   */
  class AddClustersToTrackTool final : public extends<GaudiTool, ITrackAddClusters> {
  public:
    /// Standard constructor
    using extends::extends;

  private:
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_could_not_find_ut{ this, "Could not find all UT clusters" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_could_not_find_vp{ this, "Could not find all VP clusters" };
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_could_not_find_ft{ this, "Could not find all FT clusters" };

    DataObjectReadHandle<VPMicroClusters> m_vpClusters{ this, "VPMicroClusterLocation", "",
                                                        "Location of VP micro clusters" };
    DataObjectReadHandle<FTLiteClusters>  m_ftClusters{ this, "FTLiteClusterLocation", "",
                                                       "Location of FT lite clusters" };
    DataObjectReadHandle<UTHitClusters>   m_utClusters{ this, "UTHitClusterLocation", "", "Location of UT clusters" };

  public:
    StatusCode fillClustersFromLHCbIDs( LHCb::Event::v1::Track&          track,
                                        const std::vector<LHCb::LHCbID>& lhcbIDs ) const override {
      TrackVPClusters& trackVPClusters = track.vpClusters();
      TrackFTClusters& trackFTClusters = track.ftClusters();
      TrackUTClusters& trackUTClusters = track.utClusters();

      trackVPClusters.clear();
      trackFTClusters.clear();
      trackUTClusters.clear();

      std::vector<Detector::VPChannelID>   vp_ids;
      std::vector<Detector::FTChannelID>   ft_ids;
      std::vector<Detector::UT::ChannelID> ut_ids;

      const auto& allVPClusters = m_vpClusters.get();
      const auto& allUTClusters = m_utClusters.get();
      const auto& allFTClusters = m_ftClusters.get();

      if ( track.hasVelo() ) vp_ids.reserve( SOFT_MAX_N_VELO_HITS_ON_TRACK );
      if ( track.hasT() ) ft_ids.reserve( SOFT_MAX_N_FT_HITS_ON_TRACK );
      if ( track.hasUT() ) ut_ids.reserve( SOFT_MAX_N_UT_HITS_ON_TRACK );

      for ( const auto& id : lhcbIDs ) {
        if ( id.isVP() )
          vp_ids.push_back( id.vpID() );
        else if ( id.isUT() )
          ut_ids.push_back( id.utID() );
        else if ( id.isFT() )
          ft_ids.push_back( id.ftID() );
      }

      trackVPClusters.reserve( vp_ids.size() );
      trackFTClusters.reserve( ft_ids.size() );
      trackUTClusters.reserve( ut_ids.size() );

      bool vp_ok = copy_clusters( *allVPClusters, trackVPClusters, vp_ids );
      bool ut_ok = copy_clusters( *allUTClusters, trackUTClusters, ut_ids );
      bool ft_ok = copy_clusters( *allFTClusters, trackFTClusters, ft_ids );

      if ( !vp_ok ) ++m_could_not_find_vp;

      if ( !ft_ok ) ++m_could_not_find_ft;

      if ( !ut_ok ) ++m_could_not_find_ut;

      return StatusCode( vp_ok && ut_ok && ft_ok );
    }

    StatusCode fillClustersFromLHCbIDs( LHCb::Event::v1::Track& track ) const override {
      return fillClustersFromLHCbIDs( track, track.lhcbIDs() );
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::AddClustersToTrackTool, "AddClustersToTrackTool" )
