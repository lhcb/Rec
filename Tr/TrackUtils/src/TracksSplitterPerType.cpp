/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  template <typename TracksOut, typename OutData = std::tuple<TracksOut, TracksOut, TracksOut, TracksOut, TracksOut>>
  class TracksSplitterPerType : public Algorithm::MultiTransformer<OutData( Track::Range const& tracks )> {
  public:
    using KeyValue = typename TracksSplitterPerType<TracksOut>::KeyValue;

    TracksSplitterPerType( const std::string& name, ISvcLocator* pSvc )
        : TracksSplitterPerType<TracksOut>::MultiTransformer(
              name, pSvc, { KeyValue( "InputTracks", "" ) },
              { KeyValue( "LongTracks", "" ), KeyValue( "DownstreamTracks", "" ), KeyValue( "UpstreamTracks", "" ),
                KeyValue( "Ttracks", "" ), KeyValue( "VeloTracks", "" ) } ) {}

    OutData operator()( Track::Range const& tracks ) const override {
      OutData out_tracks;
      auto& [long_tks, down_tks, up_tks, t_tks, velo_tks] = out_tracks;

      for ( auto track : tracks ) {
        switch ( track->type() ) {
        case LHCb::Track::Types::Long:
          insert_track( long_tks, track );
          break;
        case LHCb::Track::Types::Downstream:
          insert_track( down_tks, track );
          break;
        case LHCb::Track::Types::Upstream:
          insert_track( up_tks, track );
          break;
        case LHCb::Track::Types::Ttrack:
          insert_track( t_tks, track );
          break;
        case LHCb::Track::Types::Velo:
          insert_track( velo_tks, track );
          break;
        default:
          break;
        }
      }

      return out_tracks;
    }

  private:
    virtual void insert_track( TracksOut&, LHCb::Track const* ) const = 0;
  };

  class TracksSharedSplitterPerType final : public TracksSplitterPerType<LHCb::Track::Selection> {
  public:
    TracksSharedSplitterPerType( const std::string& name, ISvcLocator* pSvc )
        : TracksSplitterPerType<LHCb::Track::Selection>::TracksSplitterPerType( name, pSvc ){};

  private:
    void insert_track( LHCb::Track::Selection& tracks, LHCb::Track const* track ) const override {
      tracks.insert( track );
    }
  };

  class TracksCopySplitterPerType final : public TracksSplitterPerType<LHCb::Tracks> {
  public:
    TracksCopySplitterPerType( const std::string& name, ISvcLocator* pSvc )
        : TracksSplitterPerType<LHCb::Tracks>::TracksSplitterPerType( name, pSvc ){};

  private:
    void insert_track( LHCb::Tracks& tracks, LHCb::Track const* track ) const override {
      tracks.insert( new LHCb::Track( *track ) );
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::TracksSharedSplitterPerType, "TracksSharedSplitterPerType" )
DECLARE_COMPONENT_WITH_ID( LHCb::TracksCopySplitterPerType, "TracksCopySplitterPerType" )
