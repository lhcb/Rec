/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <algorithm>
#include <limits>
#include <string>
#include <vector>

#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ToolHandle.h"

#include "LHCbAlgs/Transformer.h"

#include "Event/PrSeedTracks.h"
#include "Event/RelationTable.h"
#include "Event/States.h"
#include "Event/Track.h"

using PrTracks    = LHCb::Pr::Seeding::Tracks;
using V1Tracks    = LHCb::Event::v1::Tracks;
using PrSelection = LHCb::Event::RelationTable1D<PrTracks>;

using base_t2   = LHCb::Algorithm::Transformer<V1Tracks( const V1Tracks&, const V1Tracks& )>;
using KeyValue2 = base_t2::KeyValue;

class TrackAncestorAssociatorByKey : public base_t2 {
public:
  TrackAncestorAssociatorByKey( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t2( name, pSvcLocator, { KeyValue2{ "InputTracks", {} }, KeyValue2{ "AncestorTracks", {} } },
                 { KeyValue2{ "OutputLocation", {} } } ) {}

  V1Tracks operator()( const V1Tracks& tracksin, const V1Tracks& unfittedTracks ) const override {
    V1Tracks tracksout;
    for ( std::size_t i{ 0 }; i < unfittedTracks.size(); i++ ) {
      if ( !tracksin.object( i ) ) continue; // skip if track didn't pass fit
      if ( tracksin.object( i )->ancestors().size() > 0 ) {
        throw GaudiException( "Track already has ancestors, downstream algorithms expect ancestor index to be 0",
                              this->name(), StatusCode::FAILURE );
      }
      tracksin.object( i )->addToAncestors( unfittedTracks.object( i ) );
      tracksout.insert( tracksin.object( i ), i );
    }

    return tracksout;
  }
};

DECLARE_COMPONENT( TrackAncestorAssociatorByKey )

using base_t   = LHCb::Algorithm::Transformer<V1Tracks( const V1Tracks&, const PrSelection&, const V1Tracks& )>;
using KeyValue = base_t::KeyValue;

class TtrackAncestorAssociatorByTableIndex : public base_t {
public:
  TtrackAncestorAssociatorByTableIndex( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t( name, pSvcLocator,
                { KeyValue{ "InputTracks", {} }, KeyValue{ "InputTracksTable", {} }, KeyValue{ "AncestorTracks", {} } },
                { KeyValue{ "OutputLocation", {} } } ) {}

  V1Tracks operator()( const V1Tracks& tracksin, const PrSelection& trackSelection,
                       const V1Tracks& unfittedTracks ) const override {
    if ( tracksin.size() != trackSelection.size() ) {
      verbose() << "Input tracks and track selection have differing numbers of tracks" << endmsg;
    }

    V1Tracks tracksout;
    for ( std::size_t i{ 0 }; i < trackSelection.scalar().size(); i++ ) {
      if ( !tracksin.object( i ) ) continue; // skip if track didn't pass fit
      if ( tracksin.object( i )->ancestors().size() > 0 ) {
        throw GaudiException( "Track already has ancestors, downstream algorithms expect ancestor index to be 0",
                              this->name(), StatusCode::FAILURE );
      }
      const int fromTrackIndex = trackSelection.scalar()[i].fromIndex().cast();
      tracksin.object( i )->addToAncestors( unfittedTracks.object( fromTrackIndex ) );
      tracksout.insert( tracksin.object( i ), i );
    }

    return tracksout;
  }
};

DECLARE_COMPONENT( TtrackAncestorAssociatorByTableIndex )
