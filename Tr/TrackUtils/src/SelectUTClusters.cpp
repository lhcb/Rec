/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class selectUTClustersForTracks
 *
 *  Filters UTHitClusters to only select
 *  those clusters that were used on the given track,
 *  or particle list.
 *
 *
 *  @see UTHitCluster.h
 *  @author Laurent Dufour
 */

#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "Event/UTHitCluster.h"

#include "GaudiKernel/ISvcLocator.h"

#include "LHCbAlgs/MergingTransformer.h"

#include "LoKi/PhysExtract.h"

#include <UTDAQ/UTDAQHelper.h>

using UTClusters    = LHCb::UTHitClusters;
using UTClusterList = std::vector<LHCb::UTHitCluster>;

namespace {
  /** Changes the ids and clusters, determining the VP clusters on the track **/
  void addUTClusters( LHCb::Track const* track, std::set<unsigned int>& ids, UTClusterList& clusters ) {
    for ( const auto& cluster : track->utClusters() ) {
      auto [_, is_new_cluster] = ids.insert( cluster.channelID() );
      if ( is_new_cluster ) { clusters.push_back( cluster ); }
    }
  }

  /** Changes the ids and clusters, determining the vpIDs on the proto **/
  void addUTClusters( LHCb::ProtoParticle const* proto, std::set<unsigned int>& ids, UTClusterList& clusters ) {
    if ( !( proto->track() ) ) return;

    addUTClusters( proto->track(), ids, clusters );
  }

  /** Changes the ids and clusters, determining the vpIDs on the particle **/
  void addUTClusters( LHCb::Particle const* particle, std::set<unsigned int>& ids, UTClusterList& clusters ) {
    if ( !particle->isBasicParticle() || !( particle->proto() ) || !( particle->proto()->track() ) ) return;

    addUTClusters( particle->proto(), ids, clusters );
  }

  template <class InputType>
  InputType& getBasics( InputType& a ) {
    return a;
  } // for protos & tracks

  LHCb::Particle::Selection getBasics( LHCb::Particle::Range const& particles ) {
    // make sure the list of particles includes the children
    LHCb::Particle::Selection flat_particles;

    for ( const auto& rawParticle : particles ) {
      LoKi::Extract::particles( rawParticle, std::back_inserter( flat_particles ),
                                [&]( const auto& p ) { return p->isBasicParticle() && p->proto(); } );
    }

    return flat_particles;
  }
} // namespace

namespace LHCb {

  template <typename InputListType>
  struct SelectUTClustersFromTrackT final
      : Algorithm::MergingTransformer<UTHitClusters( Gaudi::Functional::vector_of_const_<InputListType> const& )> {
    mutable Gaudi::Accumulators::AveragingCounter<> m_clusterPerEvent{ this, "Average clusters copied per event" };

    SelectUTClustersFromTrackT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<UTHitClusters( Gaudi::Functional::vector_of_const_<InputListType> const& )>{
              name, pSvcLocator, { "Inputs", {} }, { "OutputLocation", {} } } {}

    UTHitClusters operator()( Gaudi::Functional::vector_of_const_<InputListType> const& lists ) const override {
      std::set<unsigned int> ids;
      UTClusterList          all_clusters;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;

        const auto& flat_particles = getBasics( list );

        for ( const auto* track : flat_particles ) { addUTClusters( track, ids, all_clusters ); }
      }

      // this takes care of sorting the clusters
      UTHitClusters outputClusters = LHCb::hitClustersFromUTHitClusterVector( all_clusters );

      m_clusterPerEvent += outputClusters.size();

      return outputClusters;
    }
  };

  using SelectUTClustersFromParticles      = SelectUTClustersFromTrackT<LHCb::Particle::Range>;
  using SelectUTClustersFromProtoParticles = SelectUTClustersFromTrackT<LHCb::ProtoParticle::Range>;
  using SelectUTClustersFromTracks         = SelectUTClustersFromTrackT<LHCb::Track::Range>;

  DECLARE_COMPONENT_WITH_ID( SelectUTClustersFromParticles, "SelectUTClustersFromParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectUTClustersFromTracks, "SelectUTClustersFromTracks" )
  DECLARE_COMPONENT_WITH_ID( SelectUTClustersFromProtoParticles, "SelectUTClustersFromProtoParticles" )
} // namespace LHCb
