/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class SelectVPClustersFromContainerForTracks
 *
 *  Filters a VP(Light|Micro)Clusters container to only select
 *  those clusters that were used on the given track/particle list.
 *
 *  Example use-case is in the persistency, in which
 *  (for reasons of applying better alignments) one wants to persist
 *  sufficient information to refit.
 *
 *  The search of clusters with a cetain LHCb ID in the container
 *  is relatively slow, but unfortunately the container is not
 *  perfectly sorted nor an indexed container.
 *
 *  These algorithms support their application on
 *  tracks, particles, protoparticles, via their ::Range
 *  which means there are 9 algorithms defined here.
 *
 *  @author Laurent Dufour
 */

#include "Detector/VP/VPChannelID.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/VPLightCluster.h"
#include "Event/VPMicroCluster.h"
#include "GaudiKernel/ISvcLocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "LoKi/PhysExtract.h"

namespace {
  /** Changes the ids, determining the vpIDs on the track **/
  void addVPIds( LHCb::Track const* track, std::set<unsigned int>& ids ) {
    for ( const auto& id : track->lhcbIDs() ) {
      if ( id.isVP() ) ids.insert( id.lhcbID() );
    }
  }

  /** Changes the ids, determining the vpIDs on the proto **/
  void addVPIds( LHCb::ProtoParticle const* proto, std::set<unsigned int>& ids ) {
    if ( !( proto->track() ) ) return;

    addVPIds( proto->track(), ids );
  }

  /** Changes the ids, determining the vpIDs on the particle **/
  void addVPIds( LHCb::Particle const* particle, std::set<unsigned int>& ids ) {
    if ( !particle->isBasicParticle() || !( particle->proto() ) || !( particle->proto()->track() ) ) return;

    addVPIds( particle->proto(), ids );
  }

  /** Changes the ids and clusters, determining the VP clusters on the track **/
  void addVPClusters( LHCb::Track const* track, std::set<unsigned int>& ids, LHCb::VPMicroClusters& clusters ) {
    for ( const auto& vpCluster : track->vpClusters() ) {
      auto [_, is_new_cluster] = ids.insert( vpCluster.channelID() );
      if ( is_new_cluster ) clusters.insert( &vpCluster, &vpCluster + 1, vpCluster.channelID().module() );
    }
  }

  /** Changes the ids and clusters, determining the vpIDs on the proto **/
  void addVPClusters( LHCb::ProtoParticle const* proto, std::set<unsigned int>& ids, LHCb::VPMicroClusters& clusters ) {
    if ( !( proto->track() ) ) return;

    addVPClusters( proto->track(), ids, clusters );
  }

  /** Changes the ids and clusters, determining the vpIDs on the particle **/
  void addVPClusters( LHCb::Particle const* particle, std::set<unsigned int>& ids, LHCb::VPMicroClusters& clusters ) {
    if ( !particle->isBasicParticle() || !( particle->proto() ) || !( particle->proto()->track() ) ) return;

    addVPClusters( particle->proto(), ids, clusters );
  }

  template <class InputType>
  InputType& getBasics( InputType& a ) {
    return a;
  } // for protos & tracks

  LHCb::Particle::Selection getBasics( LHCb::Particle::Range const& particles ) {
    // make sure the list of particles includes the children
    LHCb::Particle::Selection flat_particles;

    for ( const auto& rawParticle : particles ) {
      LoKi::Extract::particles( rawParticle, std::back_inserter( flat_particles ),
                                [&]( const auto& p ) { return p->isBasicParticle() && p->proto(); } );
    }

    return flat_particles;
  }

  template <class ClusterContainer>
  ClusterContainer getClusterSublist( std::set<unsigned int> const& ids, ClusterContainer const& inputClusters ) {
    ClusterContainer outputClusters;
    outputClusters.reserve( ids.size() );

    for ( const auto& lhcbid : ids ) {
      const auto& channel = LHCb::LHCbID( lhcbid ).vpID();

      for ( const auto& cluster : inputClusters ) {
        if ( cluster.channelID() == channel ) {
          outputClusters.push_back( cluster );
          break;
        }
      }
    }

    return outputClusters;
  }

  template <>
  LHCb::VPMicroClusters getClusterSublist( std::set<unsigned int> const& ids,
                                           LHCb::VPMicroClusters const&  inputClusters ) {
    LHCb::VPMicroClusters outputClusters;
    outputClusters.reserve( ids.size() );

    for ( const auto& lhcbid : ids ) {
      const auto& channel = LHCb::LHCbID( lhcbid ).vpID();

      for ( const auto& cluster : inputClusters.range( channel.module() ) ) {
        if ( cluster.channelID() == channel ) {
          outputClusters.addHit( std::forward_as_tuple( cluster ), channel.module() );
          break;
        }
      }
    }

    sortClusterContainer( outputClusters );

    return outputClusters;
  }
} // namespace

namespace LHCb {
  template <typename InputListType, typename ClusterContainerType>
  struct SelectVPClustersFromContainerT final : Algorithm::MergingTransformer<ClusterContainerType(
                                                    Gaudi::Functional::vector_of_const_<InputListType> const& )> {
    DataObjectReadHandle<ClusterContainerType> m_inputClusters{ this, "InputClusters", "" };

    mutable Gaudi::Accumulators::AveragingCounter<> m_clusterPerEvent{ this, "Average clusters copied per event" };

    SelectVPClustersFromContainerT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<ClusterContainerType(
              Gaudi::Functional::vector_of_const_<InputListType> const& )>{
              name, pSvcLocator, { "Inputs", {} }, { "OutputLocation", {} } } {}

    ClusterContainerType operator()( Gaudi::Functional::vector_of_const_<InputListType> const& lists ) const override {
      ClusterContainerType* const pInputClusters = m_inputClusters.get();

      if ( !pInputClusters ) {
        this->error() << "Could not find the input cluster container (a Retina/SP mismatch?)." << endmsg;

        return ClusterContainerType();
      }

      ClusterContainerType const& inputClusters = *pInputClusters;

      // first, scout for LHCbIDs
      // these should be unique
      // and sorted
      // get the unique LHCb IDs for a track list
      // candidate for refactoring to a standard function
      std::set<unsigned int> ids;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;

        const auto& flat_particles = getBasics( list );

        for ( const auto* track : flat_particles ) { addVPIds( track, ids ); }
      }

      auto outputClusters = getClusterSublist( ids, inputClusters );

      if ( outputClusters.size() < ids.size() ) this->error() << "Missing clusters from container" << endmsg;
      m_clusterPerEvent += outputClusters.size();

      return outputClusters;
    }
  };

  template <typename InputListType>
  struct SelectVPClustersFromTrackT final : Algorithm::MergingTransformer<LHCb::VPMicroClusters(
                                                Gaudi::Functional::vector_of_const_<InputListType> const& )> {
    mutable Gaudi::Accumulators::AveragingCounter<> m_clusterPerEvent{ this, "Average clusters copied per event" };

    SelectVPClustersFromTrackT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<LHCb::VPMicroClusters(
              Gaudi::Functional::vector_of_const_<InputListType> const& )>{
              name, pSvcLocator, { "Inputs", {} }, { "OutputLocation", {} } } {}

    LHCb::VPMicroClusters operator()( Gaudi::Functional::vector_of_const_<InputListType> const& lists ) const override {
      std::set<unsigned int> ids;
      LHCb::VPMicroClusters  outputClusters;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;

        const auto& flat_particles = getBasics( list );

        for ( const auto* track : flat_particles ) { addVPClusters( track, ids, outputClusters ); }
      }

      m_clusterPerEvent += outputClusters.size();

      return outputClusters;
    }
  };

  // these 'usings' are needed as the macro doesn't like template arguments too much...
  using SelectVPLightClustersFromContainerForParticles =
      SelectVPClustersFromContainerT<LHCb::Particle::Range, LHCb::VPLightClusters>;
  using SelectVPMicroClustersFromContainerForParticles =
      SelectVPClustersFromContainerT<LHCb::Particle::Range, LHCb::VPMicroClusters>;
  using SelectVPLightClustersFromContainerForTracks =
      SelectVPClustersFromContainerT<LHCb::Track::Range, LHCb::VPLightClusters>;
  using SelectVPMicroClustersFromContainerForTracks =
      SelectVPClustersFromContainerT<LHCb::Track::Range, LHCb::VPMicroClusters>;
  using SelectVPLightClustersFromContainerForProtos =
      SelectVPClustersFromContainerT<LHCb::ProtoParticle::Range, LHCb::VPLightClusters>;
  using SelectVPMicroClustersFromContainerForProtos =
      SelectVPClustersFromContainerT<LHCb::ProtoParticle::Range, LHCb::VPMicroClusters>;

  using SelectVPMicroClustersForTracks         = SelectVPClustersFromTrackT<LHCb::Track::Range>;
  using SelectVPMicroClustersForParticles      = SelectVPClustersFromTrackT<LHCb::Particle::Range>;
  using SelectVPMicroClustersForProtoParticles = SelectVPClustersFromTrackT<LHCb::ProtoParticle::Range>;

  DECLARE_COMPONENT_WITH_ID( SelectVPLightClustersFromContainerForParticles,
                             "SelectVPLightClustersFromContainerForParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectVPLightClustersFromContainerForProtos,
                             "SelectVPLightClustersFromContainerForProtos" )
  DECLARE_COMPONENT_WITH_ID( SelectVPLightClustersFromContainerForTracks,
                             "SelectVPLightClustersFromContainerForTracks" )

  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersFromContainerForParticles,
                             "SelectVPMicroClustersFromContainerForParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersFromContainerForProtos,
                             "SelectVPMicroClustersFromContainerForProtos" )
  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersFromContainerForTracks,
                             "SelectVPMicroClustersFromContainerForTracks" )

  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersForParticles, "SelectVPMicroClustersFromParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersForProtoParticles, "SelectVPMicroClustersFromProtoParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectVPMicroClustersForTracks, "SelectVPMicroClustersFromTracks" )
} // namespace LHCb
