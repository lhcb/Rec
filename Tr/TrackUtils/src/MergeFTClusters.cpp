/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"

#include "Event/FTLiteCluster.h"

#include "LHCbAlgs/MergingTransformer.h"

using FTLiteClusters    = LHCb::FTLiteCluster::FTLiteClusters;
using LHCbIDsPerQuarter = std::array<std::set<LHCb::Detector::FTChannelID>, LHCb::Detector::FT::nQuartersTotal>;

/**
 *  Merge a list of "FTLiteClusters"
 *  to a new container of the same type, in which only one cluster
 *  per FTChannelID is allowed.
 *
 *  @author Laurent Dufour
 */
namespace {
  template <class ClusterContainerType>
  void mergeFTClusters( ClusterContainerType& merge_to, LHCbIDsPerQuarter& encountered_channel_ids_per_quarter,
                        const ClusterContainerType& merge_from ) {
    //  probably there is something more elegant than this
    for ( unsigned int quarterIdx = 0; quarterIdx < LHCb::Detector::FT::nQuartersTotal; ++quarterIdx ) {
      for ( const auto& cluster : merge_from.range( quarterIdx ) ) {
        auto [_, is_new_cluster] = encountered_channel_ids_per_quarter[quarterIdx].insert( cluster.channelID() );

        if ( is_new_cluster ) merge_to.insert( &cluster, &cluster + 1, cluster.channelID().globalQuarterIdx() );
      }
    }
  }
}; // namespace

namespace LHCb {
  template <class ClusterContainerType>
  struct FTClustersMergerT final : Algorithm::MergingTransformer<ClusterContainerType(
                                       Gaudi::Functional::vector_of_const_<ClusterContainerType> const& )> {

    mutable Gaudi::Accumulators::Counter<> m_n_clusters_input{ this, "No. of input clusters (in total)" };
    mutable Gaudi::Accumulators::Counter<> m_n_clusters_output{ this, "No. of output clusters" };

    FTClustersMergerT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<ClusterContainerType(
              Gaudi::Functional::vector_of_const_<ClusterContainerType> const& )>(
              name, pSvcLocator, { "InputClusters", {} }, { "OutputLocation", {} } ) {}

    ClusterContainerType
    operator()( Gaudi::Functional::vector_of_const_<ClusterContainerType> const& cluster_lists ) const override {
      ClusterContainerType outputClusters;

      LHCbIDsPerQuarter ids;

      for ( const auto& list : cluster_lists ) {
        if ( !list.size() ) continue;

        m_n_clusters_input += list.size();
        mergeFTClusters( outputClusters, ids, list );
      }

      m_n_clusters_output += ids.size();

      return outputClusters;
    }
  };
} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::FTClustersMergerT<FTLiteClusters>, "MergeFTLiteClusters" )
