/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class RandomVeloTrackContainerSplitter RandomVeloTrackContainerSplitter.h
 *
 *  Split Velo container to two (passed and rejected) wrt to given selection criteria
 *
 *  @author Andrii Usachov
 *  @date   30/04/2020
 *  @author Bogdan Kutsenko
 *  @date   25/05/2024
 */

#include "Event/PrVeloTracks.h"
#include "Functors/Function.h"
#include "Functors/with_functors.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  /// The output data
  using OutConts     = std::tuple<LHCb::Pr::Velo::Tracks, LHCb::Pr::Velo::Tracks>;
  using PrTrackProxy = decltype( *std::declval<const LHCb::Pr::Velo::Tracks>().scalar().begin() );
  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Pr::Velo::Tracks& );
    static constexpr auto PropertyName = "Code";
  };
} // namespace

bool makePseudoRandomDecision( const PrTrackProxy& track ) {
  const auto lhcb_id = track.lhcbIDs()[0];
  return static_cast<int>( lhcb_id.lhcbID() ) % 2;
}

class RandomVeloTrackContainerSplitter final
    : public with_functors<LHCb::Algorithm::MultiTransformer<OutConts( const LHCb::Pr::Velo::Tracks& )>,
                           TrackPredicate> {
public:
  RandomVeloTrackContainerSplitter( const std::string& name, ISvcLocator* pSvcLocator )
      : with_functors( name, pSvcLocator, { KeyValue{ "TracksInContainer", "" } },
                       { KeyValue{ "PassedContainer", "" }, KeyValue{ "RejectedContainer", "" } } ) {}

  OutConts operator()( const LHCb::Pr::Velo::Tracks& input_tracks ) const override {
    OutConts out;
    auto& [passed, rejected] = out;
    m_inputs += input_tracks.size();

    for ( auto const& trk : input_tracks.scalar() ) {
      auto&     container = ( makePseudoRandomDecision( trk ) ? passed : rejected );
      const int t         = trk.offset();
      container.copy_back<SIMDWrapper::scalar::types>( input_tracks, t, true );
    }
    m_passed += passed.size();

    return out;
  }

private:
  mutable Gaudi::Accumulators::StatCounter<> m_inputs{ this, "#inputs" };
  mutable Gaudi::Accumulators::StatCounter<> m_passed{ this, "#passed" };
};

DECLARE_COMPONENT( RandomVeloTrackContainerSplitter )
