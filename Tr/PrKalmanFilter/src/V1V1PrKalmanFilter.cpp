/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PrKalmanFilter/KF.h"
#include "TrackInterfaces/IPrFitterTool.h"

#include "Event/PrHits.h"
#include "Event/Track_v1.h"

#include "LHCbAlgs/Transformer.h"

namespace {
  using V1Tracks     = LHCb::Event::v1::Tracks;
  using V1TrackRange = LHCb::Event::v1::Track::Range;
} // namespace

namespace LHCb {

  /** @class V1V1PrKalmanFilter
   *
   *  Gaudi Algorithm that takes care of fitting v1 tracks on the TES
   * (using a ::Range) as input using a PrKalmanFilterTool.
   *
   * At the moment, only the configuration in which the track has UT hits,
   * FT hits, or velo hits are supported.
   *
   * Inspired by the PrKalmanFilterToolExamples
   */
  class V1V1PrKalmanFilter
      : public LHCb::Algorithm::Transformer<V1Tracks( V1TrackRange const&, const Pr::Hits<Pr::HitType::VP>&,
                                                      const Pr::Hits<Pr::HitType::FT>&,
                                                      const Pr::Hits<Pr::HitType::UT>&, const IPrFitterTool& )> {
  public:
    using base_t = LHCb::Algorithm::Transformer<V1Tracks( const V1TrackRange&, const Pr::Hits<Pr::HitType::VP>&,
                                                          const Pr::Hits<Pr::HitType::FT>&,
                                                          const Pr::Hits<Pr::HitType::UT>&, const IPrFitterTool& )>;

    V1V1PrKalmanFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : base_t( name, pSvcLocator,
                  { typename base_t::KeyValue{ "Input", "" }, typename base_t::KeyValue{ "HitsVP", "" },
                    typename base_t::KeyValue{ "HitsFT", "" }, typename base_t::KeyValue{ "HitsUT", "" },
                    typename base_t::KeyValue{ "TrackFitter", "" } },
                  { "OutputTracks", "" } ) {}

    V1Tracks operator()( V1TrackRange const&, const Pr::Hits<Pr::HitType::VP>&, const Pr::Hits<Pr::HitType::FT>&,
                         const Pr::Hits<Pr::HitType::UT>&, const IPrFitterTool& ) const override;
  };

  V1Tracks V1V1PrKalmanFilter::operator()( V1TrackRange const&                          tracks,
                                           const LHCb::Pr::Hits<LHCb::Pr::HitType::VP>& hits_vp,
                                           const LHCb::Pr::Hits<LHCb::Pr::HitType::FT>& hits_ft,
                                           const LHCb::Pr::Hits<LHCb::Pr::HitType::UT>& hits_ut,
                                           const IPrFitterTool&                         fit ) const {
    if ( tracks.empty() ) return V1Tracks{};

    return std::get<V1Tracks>( fit( tracks, hits_vp, hits_ut, hits_ft ) );
  }

  DECLARE_COMPONENT_WITH_ID( V1V1PrKalmanFilter, "V1V1PrKalmanFilter" )
} // namespace LHCb
