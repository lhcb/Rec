/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/**
 *  This algorithm splits a track container in 3 parts:
 *   - those containing ghosts
 *   - those containing tracks belonging to MCParticles passing
 *     the configured functor (which can be F.ALL to accept everything)
 *   - those containing tracks beloning to MCParticles failing
 *     the configured functor
 *
 *  The intended use-case of this is for the study of algorithms
 *  which take tracks, allowing the study of the impact of ghosts
 *  and specific MCParticle's tracks, without extending these
 *  algorithms with MC-truth matching functionality.
 *
 *  An example of this use-case can be found in the FT hit efficiencies.
 *
 *  In case of multiple MC matches for the track, only those passing
 *  the configured weight treshold are used. If either one of those
 *  pass the cut, the track is kept.
 *
 *  @author Laurent Dufour <laurent.dufour@cern.ch>
 *  @author Mark Waterlaat <mark.waterlaat@cern.ch>
 */

#include "Event/Track.h"

#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"

#include "Functors/with_functors.h"

#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  struct MCPredicate {
    using Signature                    = bool( const LHCb::MCParticle& );
    static constexpr auto PropertyName = "MCCut";
  };

  using Split_track_collection_t = std::tuple<LHCb::Track::Selection, LHCb::Track::Selection, LHCb::Track::Selection>;
  using Truth_match_relations_t  = LHCb::LinksByKey;
} // namespace

class TrackListMCTruthRefiner
    : public with_functors<LHCb::Algorithm::MultiTransformer<Split_track_collection_t(
                               LHCb::Track::Range const&, LHCb::MCParticles const&, Truth_match_relations_t const& )>,
                           MCPredicate> {
public:
  TrackListMCTruthRefiner( const std::string& name, ISvcLocator* pSvcLocator )
      : with_functors(
            name, pSvcLocator,
            { KeyValue{ "InputTracks", {} }, KeyValue{ "MCParticles", {} }, KeyValue{ "TrackToMCPRelations", {} } },
            { KeyValue{ "OutputGhostTracks", {} }, KeyValue{ "OutputPassTracks", {} },
              KeyValue{ "OutputFailTracks", {} } } ) {}

  Split_track_collection_t operator()( const LHCb::Track::Range& inputTracks, LHCb::MCParticles const& mcParticles,
                                       Truth_match_relations_t const& relations ) const override {

    assert( relations.sourceClassID() == LHCb::Track::classID() &&
            "Incompatible link table in TrackListMCTruthRefiner. Source should be Track" );

    m_nInputTracks += inputTracks.size();
    Split_track_collection_t output{};
    auto&                    outputGhostTracks = std::get<0>( output );
    auto&                    outputPassTracks  = std::get<1>( output );
    auto&                    outputFailTracks  = std::get<2>( output );

    unsigned int nGhostsInEvent = 0;
    for ( auto const& track : inputTracks ) {
      std::vector<LHCb::MCParticle const*> mcParticlesForTrack;

      relations.applyToLinks(
          track->key(), [&mcParticles, &mcParticlesForTrack,
                         thresholdWeight = m_thresholdWeight.value()]( auto, auto tgtKey, auto weight ) {
            const auto mcp = dynamic_cast<const LHCb::MCParticle*>( mcParticles.containedObject( tgtKey ) );

            if ( mcp && weight > thresholdWeight ) { mcParticlesForTrack.push_back( mcp ); }
          } );

      if ( mcParticlesForTrack.empty() ) {
        outputGhostTracks.insert( track );
        ++nGhostsInEvent;
        continue;
      }

      auto const& pred       = getFunctor<MCPredicate>();
      bool        filterPass = false;

      for ( const auto& mcParticle : mcParticlesForTrack ) {
        if ( pred( *mcParticle ) ) {
          filterPass = true;
          break;
        }
      }

      if ( filterPass )
        outputPassTracks.insert( track );
      else
        outputFailTracks.insert( track );
    }

    m_nPassed += outputPassTracks.size();
    m_nGhosts += nGhostsInEvent;

    return output;
  }

private:
  mutable Gaudi::Accumulators::StatCounter<> m_nInputTracks{ this, "# input tracks" };
  mutable Gaudi::Accumulators::StatCounter<> m_nGhosts{ this, "# ghosts" };
  mutable Gaudi::Accumulators::StatCounter<> m_nPassed{ this, "# passed" };

  Gaudi::Property<float> m_thresholdWeight{ this, "ThresholdWeight", 0.7,
                                            "Minimum weight for the MCParticle associated to the track" };
};

DECLARE_COMPONENT( TrackListMCTruthRefiner )
